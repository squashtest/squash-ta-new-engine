/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.xml.functions.library.utils.TransformerTuner;

/**
 * This class defines an XSLT transform.
 * @author edegenetais
 *
 */
public class Transform {
	private static final Logger LOGGER = LoggerFactory.getLogger(Transform.class);
	
	private static final byte[] identitySheet;
	
	static{
		URL identityUrl=Transform.class.getResource("identity.xslt");
		if(identityUrl==null){
			throw new RuntimeException("Failed to load resource 'identity.xslt' for normalization.");
		}
		ByteArrayOutputStream identityBuffer=new ByteArrayOutputStream();
		byte[] buffer=new byte[4096];
		try(
				InputStream identityLoadStream=identityUrl.openStream();
				){
			int nb=identityLoadStream.read(buffer);
			while(nb>=0){
				identityBuffer.write(buffer, 0, nb);
				nb=identityLoadStream.read(buffer);
			}
		} catch (IOException e) {
			throw new RuntimeException("Failed to load identity stylesheet (for normalization)",e);
		}
		identitySheet=identityBuffer.toByteArray();
	}
	
	/** Reference (automatically grabbed) to the XSLT SPI of the runtime environment */
	private TransformerFactory tFactory = TransformerFactory.newInstance();
	/** Reference to the applied styleheet */
	private File stylesheet=null;
	/** if we indent XML on output */
	private boolean normalize=true;
	/** Applies the wanted tuning to the automatically fetched {@link org.squashtest.ta.xml.functions.library.utils.Transformer}*/
	private TransformerTuner tuner = new TransformerTuner();
	/**
	 * Creates an identity transform - may be used to normalize the file representation of an XML sample.
	 */
	public Transform() {
		LOGGER.debug("Instantiating identity transform.");
	}
	
	/**
	 * Creates an XSL transform using a specific XSL stylesheet.
	 * @param stylesheet the stylesheet to apply
         * @param normalize set to true if output ought to be normalized.
	 */
	public Transform(File stylesheet, boolean normalize){
		this.stylesheet=stylesheet;
		this.normalize=normalize;
		LOGGER.debug("Instatiating transform on stylesheet {}, {} XML identation", stylesheet==null?"{identity}":stylesheet.getAbsolutePath(),normalize?"with":"without");
	}
	
	/**
	 * Apply to XSLT to the input file and write the result to the output file.
	 * @param input reference to the input file from which to get the input.
	 * @param destination reference to the destination file to which the resulting output must be written.
	 */
	public void transform(File input, File destination){
		LOGGER.debug("Apply {} XSLT on {} into {}",stylesheet,input,destination);
		try {
			Transformer transformer;
			if(stylesheet==null && normalize){
				transformer=tFactory.newTransformer(new StreamSource(new ByteArrayInputStream(identitySheet)));
			}else if(stylesheet==null){
				transformer=tFactory.newTransformer();
			}else{
				transformer=tFactory.newTransformer(new StreamSource(stylesheet));
			}
			
			transformer.setOutputProperty(OutputKeys.INDENT, normalize?"yes":"no");
			transformer.setErrorListener(new TransformErrorListener());
			
			tuner.tune(transformer);
			
			StreamResult result=new StreamResult(destination);
			transformer.transform(new StreamSource(input), result);
			
		} catch (TransformerException e) {
			throw new TransformFailure("Transformation failed: "+e.getLocalizedMessage(), e);
		}
	}
}

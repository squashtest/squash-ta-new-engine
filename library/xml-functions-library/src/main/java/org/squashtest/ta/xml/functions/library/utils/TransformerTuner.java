/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils;

import javax.xml.transform.Transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.xml.functions.library.utils.saxon.SaxonTransformerMessageTweaker;

/**
 * Tunes the provided {@link Transformer} according to specified {@link org.squashtest.ta.xml.functions.library.utils.TransformerTweaker}s.
 * @author fgautier
 *
 */
public class TransformerTuner {
	
	private static final TransformerTweaker TWEAKERS[] = {new SaxonTransformerMessageTweaker()};
	
	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TransformerTuner.class);
	
	/**
	 * Tune a transformer according to its implementation. If no tweaks available the initial transformer has
	 * to be left as is.
	 * In the case of saxon implementation this will use <ul>
	 * <li>SaxonTransformerMessageTweaker which will redirect xslt messages to either debug or error. </li>
	 * </ul>  
	 * @param initialTransformer the transformer about to be tuned.
	 */
	public void tune(Transformer initialTransformer) {
		
		int tweakCounter = 0;
		for (TransformerTweaker tweaker : TWEAKERS) {
			if (tweaker.accept(initialTransformer)) {
				tweaker.apply(initialTransformer);
				tweakCounter++;
			}
		}
		
		if (tweakCounter == 0) {
			LOGGER.debug("No tweaker available, leaving Transformer " 
							+ initialTransformer.getClass().getSimpleName()
							+ " as factory made it");
		} else {
			LOGGER.debug("Applied " + tweakCounter + " tweaks to Transformer" 
							+ initialTransformer.getClass().getSimpleName());
		}
		

	}

}

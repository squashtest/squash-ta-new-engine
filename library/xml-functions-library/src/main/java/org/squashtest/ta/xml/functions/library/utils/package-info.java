/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * This package provides utilities regarding xml transformation. 
 * 
 * It defines a {@link org.squashtest.ta.xml.functions.library.utils.TransformerTuner} which tunes a {@link javax.xml.transform.Transformer}
 * to adapt it to the user liking. For that purpose it uses an array of {@link org.squashtest.ta.xml.functions.library.utils.TransformerTweaker}s 
 * used to tweak specific {@link javax.xml.transform.Transformer} implementations on specific features. 
 * 
 * Since Jaxb implementation providers may to bad things to the class loader and registry to be the first picked
 * we need to avoid as much as we can hard linkage to those. Hence the objects defined in this pacakge and their implementation
 * should avoid at all cost direct linkage to a Jaxb specific implementation. One way to deal with this constraint is to 
 * use Reflection. If for any reason one does not want or can't use reflection please find another clever way to avoid the direct 
 * dependency. The ultimate goal is to let the user choose is specific Jaxb implementation not us. 
 * 
 * @author fgautier
 *
 */
package org.squashtest.ta.xml.functions.library.utils;
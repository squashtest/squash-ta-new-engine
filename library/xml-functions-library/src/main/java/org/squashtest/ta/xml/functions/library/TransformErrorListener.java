/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransformErrorListener implements ErrorListener {

	private static final Logger LOGGER=LoggerFactory.getLogger(TransformErrorListener.class);
	
	@Override
	public void warning(TransformerException exception)
			throws TransformerException {
		
                if(LOGGER.isDebugEnabled()){
                    LOGGER.warn("Continuable error during transform",exception);
                }else{
                    LOGGER.warn("Continuable error during transform: {}\nYou may see this warning's stack trace by switching the {} logger to debug level.",exception.getMessage(),LOGGER.getName());
                }               
	}

	@Override
	public void error(TransformerException exception)
			throws TransformerException {
		throw exception;
	}

	@Override
	public void fatalError(TransformerException exception)
			throws TransformerException {
		throw exception;
	}

}

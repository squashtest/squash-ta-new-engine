/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.process

import java.util.concurrent.TimeoutException;

import org.squashtest.ta.core.tools.io.SimpleLinesData
import org.squashtest.ta.local.process.library.process.LocalProcessClient;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;

import spock.lang.Specification

class LocalProcessClientTest extends Specification {
	LocalProcessClient testee
	
	def setup(){
		testee=new LocalProcessClient()
	}
	
	def "should execute with no timeout"(){
		given:
			def command = "ping"
			int timeout = 0
			int streamlength = 100
		when:
			def result = testee.runLocalProcessCommand(command, null, timeout, streamlength)
		then:
			result instanceof ShellResult
			result.getExitValue() > 0
	}
	
	def "should execute with parametrized timeout"(){
		given:
			def command = "ping localhost"
			int timeout = 30
			int streamlength = 100
		when:
			def result = testee.runLocalProcessCommand(command, null, timeout, streamlength)
		then:
			thrown(TimeoutException);
	}
	
	def "should transmit command correctly"(){
		given:
			def command = "ping"
			int timeout = 0
			int streamlength = 100
		when:
			def result = testee.runLocalProcessCommand(command, null, timeout, streamlength)
		then:
			result instanceof ShellResult
			result.getCommand() == command
	}
	
	def "should construct an outputstream"(){
		given:
			def command = "java -version"
			int timeout = 0
			int streamlength = 100
		when:
			def result = testee.runLocalProcessCommand(command, null, timeout, streamlength)
		then:
                        result.getExitValue() != 127
			def length
			if(result.getStdout().length() > 0 ){
				length =  result.getStdout().length()
			}else{
				length =  result.getStderr().length()
			}
			
			length > 0
	}
	
	def "wrong command should rant"(){
		given:
			def command = "wrong"
			int timeout = 0
			int streamlength = 100
		when:
			def result = testee.runLocalProcessCommand(command, null, timeout, streamlength)
		then:
			thrown(IOException)
	}
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.process;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Some static logic used by the process management subsystem.
 * @author branchet
 */
public class CommandLine {
	
	private enum Status {
		
		OUTSIDE(""), IN_QUOTE("\'"), IN_DOUBLE_QUOTE("\"");
                
                String token;
		
		private Status(String token) {
			this.token=token;
		}
	
	}
	
	/**
	 * Utility method to split a command line given as a unique string to an array of string 
	 * 
	 * @param commandLine The command line to parse
	 * 
	 * @return The splitted command line
	 * @throws ParseException Exception thrown when an unbalanced quote is found in the command line to process.
	 */
	public static String[] parse( String commandLine) throws ParseException{
		
		if (commandLine == null || commandLine.length() == 0) {
			return new String[0];
		}
		
		Status state = Status.OUTSIDE;
		
		StringTokenizer tokenized = new StringTokenizer(commandLine, "\"\' ", true);
		ArrayList<String> parsedCommand = new ArrayList<String>();
		StringBuilder argument = new StringBuilder();
		
		boolean lastTokenHasBeenQuoted = false;

		while (tokenized.hasMoreTokens()) {
			String nextToken = tokenized.nextToken();	
			switch (state) {
			case IN_QUOTE:
				if ("\'".equals(nextToken)) {
					lastTokenHasBeenQuoted = true;
					state = Status.OUTSIDE;
				} else {
					argument.append(nextToken);
				}
				break;
			case IN_DOUBLE_QUOTE:
				if ("\"".equals(nextToken)) {
					lastTokenHasBeenQuoted = true;
					state = Status.OUTSIDE;
				} else {
					argument.append(nextToken);
				}
				break;
			default:
				if ("\'".equals(nextToken)) {
					state = Status.IN_QUOTE;
				} else if ("\"".equals(nextToken)) {
					state = Status.IN_DOUBLE_QUOTE;
				} else if (" ".equals(nextToken)) {
					if (lastTokenHasBeenQuoted || argument.length() != 0) {
						parsedCommand.add(argument.toString());
						argument.setLength(0);
					}
				} else {
					argument.append(nextToken);
				}
				lastTokenHasBeenQuoted = false;
				break;
			}
		}
		if (lastTokenHasBeenQuoted || argument.length() != 0) {
			parsedCommand.add(argument.toString());
		}
		if (!state.equals(Status.OUTSIDE)) {
			throw new ParseException("Unable to parse command \""+commandLine+"\". Unbalanced \""+state.token+"\"", commandLine.length());
		}
		return parsedCommand.toArray(new String[parsedCommand.size()]);
		
	}
	
	 

}

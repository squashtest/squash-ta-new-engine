/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.shell;

/**
 * {@link Platform} used fully POSIX complient platforms.
 * 
 * @author edegenetais
 */
public class PosixPlatform extends Platform{

    @Override
    public String getPlatformStatementSeparator() {
        return ";";
    }

    protected String changeIntoInSHellCommand(String localCommand) {
        String escapedInnerCommand = localCommand.replace("\"", "\\\"");
        return "sh -c \"" + escapedInnerCommand + "\"";
    }

    protected String prepareStatement(String query) {
        String preparedQuery = query;
        //we delete the commentaries...
        if (query.contains("#")) {
            //... but first check the character is not escaped
            if (query.indexOf('#') > 0) {
                if (query.charAt(query.indexOf('#') - 1) != '\\') {
                    preparedQuery = query.substring(0, query.indexOf('#'));
                }
            } else {
                preparedQuery = "";
            }
        }
        //we supress the blank
        preparedQuery = preparedQuery.trim();
        return preparedQuery;
    }

    @Override
    protected boolean needsShellOnPlatform(String command) {
        return false;
    }
    
}

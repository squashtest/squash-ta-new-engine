/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.core.tools.AmnesicStringBuffer;


/**
 * This class is used to monitor streams.
 * @author bsiri
 * @author edegenetais
 *
 */
public final class StreamMuncher implements Runnable {
	
        private static final Logger LOGGER = LoggerFactory.getLogger(StreamMuncher.class);
        
	private String streamName="anonymous";
	private AmnesicStringBuffer buffer;
	private BufferedReader reader;
	private boolean shouldRun = true;
	 
	
	public StreamMuncher(String streamName,InputStream stream, int bufferSize){
		this.streamName=streamName;
		buffer = new AmnesicStringBuffer(bufferSize);
		reader = new BufferedReader(new InputStreamReader(stream));
	}
	
	
	public void requestStop(){
		shouldRun = false;
	}

	public void start(){
		Thread errMonitor=new Thread(this,streamName+" tail.");
    	errMonitor.setDaemon(true);
    	errMonitor.start();
	}
	
	@Override
	public void run() {
		try{
			String buf;
			
			while(shouldRun){
				
				buf = reader.readLine();
				
				if (buf != null) {
                                    buffer.append(buf).append("\n"); 
                                }
			}
			if(reader.ready() && reader.read()>=0){
                            buffer.append("<<stream truncated here!>>");
                        }
			reader.close();
			
		}catch(IOException ex){
                        LOGGER.warn("Failed to access {} contents.",streamName,ex);
			try{
                            buffer.append(streamName+" : could not access contents.");
			}catch(IOException e){
                            LOGGER.warn("Swallowing IOException, since we know our buffer is RAM based. If you see this message there is definitely something rotten in the realm of Denmark !",e);
				/*
				 * swallow: we known our specific buffer is RAM based, so it
				 * cannot throw IOExceptions
				 */
			}
		}
		
	}
	
	public String getStreamContent(){
		return buffer.toString();
	}
	
}
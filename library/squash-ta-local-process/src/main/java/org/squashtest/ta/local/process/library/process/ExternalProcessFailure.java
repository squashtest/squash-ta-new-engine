/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.process;

/**
 * Exception thrown when an external process fails.
 * @author edegenetais
 *
 */
@SuppressWarnings("serial")
public class ExternalProcessFailure extends RuntimeException{
	
	private String errorStreamContent;
	private Integer returnCode;
	
	public ExternalProcessFailure(String message, String errorStreamContent) {
		super(message+"\n"+errorStreamContent);
		this.errorStreamContent=errorStreamContent;
	}
	
	public ExternalProcessFailure(String message, String errorStreamContent, Integer errorCode) {
		super(message+"\nStatus code: "+(errorCode==null?"null":errorCode.toString())+"\n"+errorStreamContent);
		this.errorStreamContent=errorStreamContent;
		this.returnCode=errorCode;
	}
	
	/**
	 * @return Error stream from the failed external process.
	 */
	public String getErrorStreamContent() {
		return errorStreamContent;
	}

	/**
	 * @return Error code from the failed external process (if known).
	 */
	public Integer getReturnCode() {
		return returnCode;
	}
}

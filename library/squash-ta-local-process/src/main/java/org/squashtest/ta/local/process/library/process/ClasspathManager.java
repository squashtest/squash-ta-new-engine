/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.process;

import java.net.MalformedURLException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides functionalities to work on classpaths.
 * @author edegenetais
 */
public class ClasspathManager {
    private static final Logger LOGGER=LoggerFactory.getLogger(ClasspathManager.class);
    
    public URL getClassCProotURL(final Class<?> targetClass) throws ClasspathException{
        String name = targetClass.getSimpleName() + ".class";
        URL u = targetClass.getResource(name);
        if("jar".equals(u.getProtocol())){
            try {
                String classURL=u.getFile();
                String jarURL=classURL.split("!")[0];
                LOGGER.debug("Adding jar {} from the Junit plugin classpath to support default junit resource execution.",jarURL);
                return new URL(jarURL);
            } catch (MalformedURLException ex) {
                throw new ClasspathException("Failed to register plugin jar URL", ex);
            }
        }else{
            throw new ClasspathException("The plugin should be packaged as a JAR, other packagings are unsupported (found at "+u.toExternalForm()+")");
        }
    }
}

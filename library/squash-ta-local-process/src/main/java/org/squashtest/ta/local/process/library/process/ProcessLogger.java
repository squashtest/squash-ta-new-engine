/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.local.process.library.shell.OutputStream;

/**
 * This class is intended to log stdout from external processes on demand.
 * 
 * @author edegenetais
 * 
 */
public class ProcessLogger extends ProcessStreamListener implements Runnable {
	private final Logger logger;
	private static int counter;
	/**
	 * Creates the stdout logger thread.
	 * 
	 * @param processClass
	 *            an arbitrary string to describe what kind of process is logged
	 *            (will be part of the logger name)
	 * @param process
	 *            the process which stdout we want to log
	 */
	public ProcessLogger(String processClass, Process process) {
		super(process,OutputStream.out);
		logger = LoggerFactory.getLogger("stdout." + processClass + "."
				+ (ProcessLogger.counter++));
		
		if (logger.isDebugEnabled()) {
			logger.info("stdout logger daemon started");
		} else if(logger.isInfoEnabled()){
			String name = logger.getName();
			logger.info("stdout logger daemon started (switch the '"
					+ name.substring(0, name.lastIndexOf('.'))
					+ "' logger category to level DEBUG to see more)");
		}
	}
	
	/**
	 * Creates the stdout logger thread.
	 * 
	 * @param processClass
	 *            an arbitrary string to describe what kind of process is logged
	 *            (will be part of the logger name)
	 * @param process
	 *            the process which stdout we want to log
	 */
	public ProcessLogger(String processClass, Process process, OutputStream targetStream) {
		super(process,targetStream);
		logger = LoggerFactory.getLogger("std"+targetStream.name()+"." + processClass + "."
				+ (ProcessLogger.counter++));
		
		if (logger.isDebugEnabled()) {
			logger.info("std"+targetStream.name()+" logger daemon started");
		} else if(logger.isInfoEnabled()){
			String name = logger.getName();
			logger.info("stdout logger daemon started (switch the '"
					+ name.substring(0, name.lastIndexOf('.'))
					+ "' logger category to level DEBUG to see more)");
		}
	}
	
	@Override
	protected void commitOutputLine(String osString) {
		logger.debug(osString);
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.shell;

import java.util.Iterator;
import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * For testability, we encapsulate plateform-dependant behavior in a strategy family.
 * @author edegenetais
 */
public abstract class Platform {
    private static final Logger LOGGER=LoggerFactory.getLogger(Platform.class);
    /** @deprecated  This field is package-accessible FOR UNIT TESTS ONLY, and should NEVER be used in business code */
    @Deprecated
    static Platform INSTANCE;
    
    static{
        if(SystemUtils.IS_OS_WINDOWS){
            INSTANCE=new WindowsPlatform();
        }else{
            INSTANCE=new PosixPlatform();
        }
    }
    
    /**
     * This function returns a {@link Platform} instance tuned to the local platform.
     * @return the {@link Platform} instance.
     */
    public static final Platform getLocalPlatform(){
        /** This is the Only Legitimate Use Business-Side (R)(c)(tm) ! */
        return INSTANCE;
    }
    
    public abstract String getPlatformStatementSeparator();
    /**
     * Depending on what works or doesn't on the platform, may decide wether to open a shell.
     * Opening a shell may be done for example in order to obtain shell interpreter functions that are unavailable when
     * we are merely creating a new process wiht the first command part as executable name. Shell opening is performed by
     * adding platform specific parts to the commandline before executing it.
     * @param localCommand the local command string.
     * @return the localCommand string with shell-opening part added.
     */
    public String addShellSpawnCommandIfNeeded(String localCommand) {
        if(
                isMultistatement(localCommand) | 
                needsShellOnPlatform(localCommand)
          ){
            return changeIntoInSHellCommand(localCommand);
        }else{
            return localCommand;
        }
    }

    protected boolean isMultistatement(String localCommand) {
        return localCommand.indexOf(getPlatformStatementSeparator())>0;
    }
    
     protected abstract boolean needsShellOnPlatform(String command);
    
    protected abstract String prepareStatement(String query);

    /**
     * Perform platform-related changes to prepare the command string for execution.
     * @param lineIterator
     * @param command 
     */
    public void addStatementFromLine(Iterator<String> lineIterator, StringBuffer command) {
        final String line = lineIterator.next();
        final String preparedStatement = prepareStatement(line);
        if (!preparedStatement.isEmpty()) {
            LOGGER.debug("Command file line: {}", line);
            command.append(preparedStatement);
        }
    }

    protected abstract String changeIntoInSHellCommand(String localCommand);
}

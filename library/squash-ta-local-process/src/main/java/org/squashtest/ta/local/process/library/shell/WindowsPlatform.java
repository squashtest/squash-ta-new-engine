/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.local.process.library.shell;

import java.io.File;

/**
 * {@link Platform} implementation used in windows.
 * @author edegenetais
 */
public class WindowsPlatform extends Platform{

    @Override
    public String getPlatformStatementSeparator() {
        return " && ";
    }

    protected String changeIntoInSHellCommand(String localCommand) {
            String escapedInnerCommand=localCommand.replace("\"", "^\"");
            return "cmd /C \""+escapedInnerCommand+"\"";
    }
     
    protected String prepareStatement(String query) {
        //we delete the commentaries...
        if (query.trim().startsWith("REM ")) {
            return "";
        }else{
            //we supress the blank
            return query.trim();
        }
    }

    @Override
    protected boolean needsShellOnPlatform(String command) {
        String executable;
        String trimmedCommand=command.trim();
        if(trimmedCommand.trim().startsWith("\"")){
            int closingDoubleQuote=trimmedCommand.indexOf('\"',1);
            while(closingDoubleQuote>0 && trimmedCommand.charAt(closingDoubleQuote-1)=='\\'){
                closingDoubleQuote=trimmedCommand.indexOf('\"',closingDoubleQuote+1);
            }
            if(closingDoubleQuote>0){
                executable = trimmedCommand.substring(1, closingDoubleQuote);
                
            }else{
                executable=trimmedCommand;//most probably executable not found, but let's let the shell decide and report.
            }
        }else{
            int firstSpace=trimmedCommand.indexOf(" ");
            if(firstSpace>0){
                executable=trimmedCommand.substring(0,firstSpace);
            }else{
                executable=trimmedCommand;
            }
        }
        return !new File(executable).exists();
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.metadata;

import org.squashtest.ta.framework.test.result.MetadataType;

/**
 * @author qtran
 */
public class MonolineMetadata extends AbstractTestMetadata{
    private String text;
     
    private String key;
    
    private String value;
    
    public MonolineMetadata() {
        super.setType(MetadataType.MONOLINE);
    }

    public MonolineMetadata(String key, String value) {
        this.key = key;
        this.value = value;
    }
    
    public MonolineMetadata(String text, String key, String value) {
        this.text = text;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void visit(TestMetadataVisitor visitor) {
        visitor.accept(this);
    }
    
    @Override
    public String toText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return toText();
    }
    
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.result;

import org.squashtest.ta.framework.test.metadata.TestMetadata;

/**
 * An InformationDetails is mainly relevant to a metadata element. 
 * 
 * @author qtran
 */
public interface InformationDetails {
        /** 
	 * @return the type of metadata {@link MetadataType}. 
	 */
	MetadataType getMetadataType();
        
        /**
	 * @return the index of the metadata within its section + 1;
	 */
	int getMetadataNumberInSection();
	
	/** 
	 * @return the absolute position of the metadata in test file
	 */
	int getMetadataNumberInFile();
	
	/**
	 * @return the textual representation of the metadata 
         * (see {@link TestMetadata})
	 */
	String getMetadataAsText();
        
        /**
	 * 
	 * @return the exception that triggered the metadata failure, or null if everything went fine.
	 */
	Exception caughtException();

}

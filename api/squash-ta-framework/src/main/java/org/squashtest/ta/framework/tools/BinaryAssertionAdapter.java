/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 *
 * @author edegenetais
 */
class BinaryAssertionAdapter implements ComponentAdapter {
    
    private final BinaryAssertion<?, ?> asser;

    public BinaryAssertionAdapter(BinaryAssertion<?, ?> asser) {
        this.asser = asser;
    }

    @Override
    public String getNature() {
        TABinaryAssertion annotation = asser.getClass().getAnnotation(TABinaryAssertion.class);
        if (annotation == null) {
            EngineComponent component = asser.getClass().getAnnotation(EngineComponent.class);
            if (component == null) {
                throw new BrokenTestException("BinaryAssertion class " + asser.getClass() + " is not annotated @TABinaryAssertion and will not work!");
            } else {
                LoggerFactory.getLogger(BinaryAssertionAdapter.class).warn("BinaryAssertion "+asser.getClass()+" uses the deprecated @EngineComponent annotation.");
                return component.value();
            }
        } else {
            return annotation.value();
        }
    }

    @Override
    public String getType() {
        return "Assertion";
    }
    
}

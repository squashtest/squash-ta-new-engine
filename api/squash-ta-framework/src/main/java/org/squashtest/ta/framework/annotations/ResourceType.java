/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * see {@link EngineComponent}. A ResourceType annotation is meant for {@link org.squashtest.ta.framework.components.Resource}s, but also {@link org.squashtest.ta.framework.components.Target} and {@link org.squashtest.ta.framework.components.ResourceRepository}.
 * 
 * <p>Elements annotated with ResourceType won't be created directly by the engine, they usually need factories for that. See the 
 * aforementioned classes for more instructions.</p> 
 * 
 *  <p>There are some major differences between @ResourceType and @EngineComponent.</p>
 *  
 *  <p>First, the difference is semantic :</p>
 *  
 *  <ul>
 *  	<li>ResourceType means that instances will be created by user code explicitly : for instance a {@link org.squashtest.ta.framework.components.ResourceRepository} 
 *  is annotated @ResourceType because it is meant to be created by a {@link org.squashtest.ta.framework.components.RepositoryCreator}. </li>
 *  	<li>EngineComponent means that instances will be created by the engine itself <strong>when it needs it</strong>: for instance a {@link org.squashtest.ta.framework.components.RepositoryCreator} 
 *  is annotated @EngineComponent then the engine will know it must create one when it needs it.</li>  
 *  </ul>
 *  
 *  <p>It ensue that classes @EngineComponent MUST declare a constructor accepting zero parameters or the engine won't know how to 
 *  handle them. @ResourceType classes, however, need not to do so.</p>
 *  
 *  <p>Second, they have different rules for occurrence of a single value : </p> 
 *  
 *  <ul>
 *  	<li>the value of each ResourceType must be unique (in other words each Resource or Target implementation have their own, unique ResourceType)</li>
 *  	<li>the value of EngineComponent may be shared among multiple classes implementing ResourceConverter, Command, BinaryAssertion etc.
 *  </ul>
 *  
 *  <p>Addendum : do not assume that @EngineComponent means that the annotated class will be a singleton. For instance a {@link org.squashtest.ta.framework.components.RepositoryCreator} will actually be 
 *  created once, while a new {@link org.squashtest.ta.framework.components.BinaryAssertion} will be created any time the engine processes an instruction needing one. What classes will be singletons and 
 *  what will not is strictly a matter of which interfaces the said classes implement.</p>
 *  
 * 
 * @author bsiri
 * @deprecated in 1.5.0. Use relevant {@link TAResource}, {@link TATarget}, {@link TARepository} instead.
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface ResourceType {
String value();
}

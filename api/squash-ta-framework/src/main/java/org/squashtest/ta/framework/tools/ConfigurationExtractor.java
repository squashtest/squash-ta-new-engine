/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * This component provides standard behvior for component
 * configuration.
 * 
 * @author edegenetais
 */
public class ConfigurationExtractor {
    
    private ComponentRepresentation representation;
    private final Logger COMPONENT_LOGGER;
    
    public ConfigurationExtractor(BinaryAssertion<?,?> c){
        COMPONENT_LOGGER=componentLoggerReference(c);
        representation=new ComponentRepresentation(c);
    }

    
    
    public ConfigurationExtractor(Command<?,?> c){
        COMPONENT_LOGGER=componentLoggerReference(c);
        representation=new ComponentRepresentation(c);
    }
    
    public ConfigurationExtractor(ResourceConverter<?,?> c){
        COMPONENT_LOGGER=componentLoggerReference(c);
        representation=new ComponentRepresentation(c);
    }
    
    public ConfigurationExtractor(UnaryAssertion<?> c){
        COMPONENT_LOGGER=componentLoggerReference(c);
        representation=new ComponentRepresentation(c);
    }

    public class ExtractionResult<CfgType>{
        private Collection<Resource<?>> remaningConfiguration;
        private CfgType extractedData;

        public ExtractionResult(Collection<Resource<?>> remaningConfiguration, CfgType extractedData) {
            this.remaningConfiguration = remaningConfiguration;
            this.extractedData = extractedData;
        }

        public Collection<Resource<?>> getRemaningConfiguration() {
            return remaningConfiguration;
        }

        public CfgType getExtractedData() {
            return extractedData;
        }
        
    }
    
    private Logger componentLoggerReference(Object c) {
        if(c==null){
            throw new NullPointerException("The ConfigurationExtractor component reference cannot be null");
        }
        return LoggerFactory.getLogger(c.getClass());
    }
    
    /**
     * This service checks that components that expect no configuration indeed receive none.
     * If configuration elements are transmitted nonetheless, a useless configuration warning will be logged.
     * <p>If a component expects no configuration, its <code>{@literal #addConfiguration(Collection<Resource<?>> cfg)}</code></p>
     * <pre><code>
     * </code>
     * 
     * </pre>
     * @param configuration configuration argument received by the components <code>{@literal #addConfiguration(Collection<Resource<?>> cfg)}</code>
     * method.
     * @see BinaryAssertion#addConfiguration(java.util.Collection) 
     * @see Command#addConfiguration(java.util.Collection) 
     * @see ResourceConverter#addConfiguration(java.util.Collection) 
     * @see UnaryAssertion#addConfiguration(java.util.Collection) 
     */
    public void expectNoConfiguration(Collection<Resource<?>> configuration){
        if(configuration!=null && !configuration.isEmpty()){
            COMPONENT_LOGGER.warn("Ignoring {} unexpected configuration resources. {} expects none!",configuration.size(),representation);
        }
    }
    
    public Properties extractSingleOptionalPropertiesResourceFromConfiguration(Properties previousValue,Collection<Resource<?>> configuration) {
        return extractPropertyResource(previousValue, configuration, true).getExtractedData();
    }
    
    public ExtractionResult<Properties> extractOptionalPropertiesResourceFromConfiguration(Properties previousValue,Collection<Resource<?>> configuration) {
        return extractPropertyResource(previousValue, configuration,false);
    }

    private ExtractionResult<Properties> extractPropertyResource(Properties previousValue, Collection<Resource<?>> configuration,boolean single) {
        Properties conf=previousValue;
        Collection<Resource<?>> otherConf=new ArrayList<>(configuration.size()-1);
        for(Resource<?> r:configuration){
            if(r instanceof PropertiesResource){
                if(conf==null){
                    conf=((PropertiesResource)r).getProperties();
                    COMPONENT_LOGGER.debug("Extracting property configuration {} for {}",conf,representation);
                }else{
                    COMPONENT_LOGGER.warn("Only one PropertiesResource was expected in {} to define global parameters. Ignoring unexpected additional configuration resource.",representation);
                }
            }else if(single){
                COMPONENT_LOGGER.warn("Ignoring unexpected configuration resource {} for {}",r,representation);
            }else{
                otherConf.add(r);
            }
        }
        return new ExtractionResult<Properties>(otherConf,conf);
    }
    
    public String extractUniqueMandatoryOptionValue(Collection<Resource<?>> configuration,String key){
        String foundValue=extractOptionValue(configuration,key);
        if(foundValue==null){
            throw new IllegalConfigurationException("Missing options file with key "+key);
        }
        return foundValue;
    }

    public String extractOptionValue(Collection<Resource<?>> configuration, String key) {
        String foundValue=null;
        for(Resource<?> r:configuration){
            foundValue = getValueIfResourceIsOption(r, key, foundValue);
        }
        return foundValue;    
    }
    
    private String getValueIfResourceIsOption(Resource<?> r, String key, String foundValue) {
        if(r instanceof FileResource){
            foundValue = getValueFromOption(r, key, foundValue);
        }else{
            COMPONENT_LOGGER.warn("Ignoring unexpected resource {} (only a single option from a single Resource:file is expected)",new ComponentRepresentation(r));
        }
        return foundValue;
    }

    private String getValueFromOption(Resource<?> r, String key, String foundValue) {
        FileResource file=(FileResource)r;
        try {
            if(OptionsReader.BASIC_READER.isOptions(file.getFile())){
                Map<String,String> options=OptionsReader.BASIC_READER.getOptions(file.getFile());
                final String candidate = options.get(key);
                foundValue = setIfNull(foundValue, candidate, key);
            }else{
                COMPONENT_LOGGER.warn("Ignored an unexpected non-options resource.");
            }
        } catch (IOException ex) {
            throw new InstructionRuntimeException("Failed to retrieve the contents of FileResource from "+file.getFile()+" storage.",ex);
        }
        return foundValue;
    }

    private String setIfNull(String foundValue, final String candidate, String key) {
        if(foundValue==null){
            foundValue=candidate;
        }else if(candidate!=null){
            COMPONENT_LOGGER.warn("Ignoring unexpected options file carrying redudant value {}={}, because {} has already been found.",key,candidate,foundValue);
        }
        return foundValue;
    }
}

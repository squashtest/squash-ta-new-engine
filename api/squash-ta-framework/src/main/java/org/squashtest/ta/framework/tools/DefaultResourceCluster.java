/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceCluster;


/**
 * Unique implementation of the {@link ResourceCluster}. 
 * 
 * This wrapper holds a collection of clustered resources. 
 * 
 * @author fgautier
 * @param <R> The type of the clustered resources
 */
public class DefaultResourceCluster<R extends Resource<R>> implements ResourceCluster<R> {
   
    /**
     * Holds the grouped resources. 
     */
    private final List<R> clusteredResources;

    /**
     * Noarg ctor. Creates an empty shell that holds an empty collection of clustered
     * resources. 
     */
    public DefaultResourceCluster() {
        this(new ArrayList<R>());
    }

    /**
     * Arbitrary number of args constuctor. Cluster one or more {@link Resource}. 
     * 
     * @param clusteredResource the resource to cluster. 
     */
    public DefaultResourceCluster(R ... clusteredResource) {
        this(Arrays.asList(clusteredResource));
    }
    
    /**
     * Standard constructor. Cluster a {@link  List} of {@link Resource}s;
     * 
     * @param clusteredResources the resources to cluster
     */
    public DefaultResourceCluster(List<R> clusteredResources) {
        this.clusteredResources = Collections.unmodifiableList(clusteredResources);
    }
       
    @Override
    public ResourceCluster<R> copy() {
        return new DefaultResourceCluster<>(clusteredResources);
    }

    @Override
    public void cleanUp() { /* NO-OP */}

    @Override
    public Iterator<R> iterator() {
        return clusteredResources.iterator();
    }
}

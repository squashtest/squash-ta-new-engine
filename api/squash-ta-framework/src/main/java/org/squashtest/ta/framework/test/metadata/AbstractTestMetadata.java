/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.metadata;

import org.squashtest.ta.framework.test.result.MetadataType;

/**
 *
 * @author qtran
 */
public abstract class AbstractTestMetadata implements TestMetadata{
    
    /**
    * OPTIONAL : May be used to store useful informations like a human readable representation of the instruction,
    * comments etc.
    */
    private String lineNumber = "0";
    
    private MetadataType type;

    @Override
    public String getLine() {
        return lineNumber;
    }

    @Override
    public void setLine(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    @Override
    public MetadataType getType() {
        return type;
    }
    
    void setType(MetadataType type){
	this.type = type;
    }
    
}

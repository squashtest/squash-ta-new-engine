/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.squashtest.ta.framework.tools;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.EngineComponent;
import org.squashtest.ta.framework.annotations.TATargetCreator;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * 
 * @author edegenetais
 */
class TargetCreatorAdapter implements ComponentAdapter{
    
    private TargetCreator<?> creator;

    public TargetCreatorAdapter(TargetCreator<?> creator) {
        this.creator = creator;
    }
    
    @Override
    public String getNature() {
        TATargetCreator annotation=creator.getClass().getAnnotation(TATargetCreator.class);
        if(annotation==null){
            EngineComponent deprecatedAnnotation=getClass().getAnnotation(EngineComponent.class);
            if(deprecatedAnnotation==null){
                throw new BrokenTestException("Resource class " + creator.getClass() + " is not annotated @TATargetCreator and will not work!");
            }else{
                LoggerFactory.getLogger(TargetCreatorAdapter.class).warn("TargetCreator type {}, uses the deprecated EngineComponent annotation",creator.getClass());
                return deprecatedAnnotation.value();
            }
        }else{
            return annotation.value();
        }
    }

     @Override
    public String getType() {
        return "TargetCreator";
    }
}

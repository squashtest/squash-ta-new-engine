/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.metadata;

import java.util.LinkedList;
import java.util.List;
import org.squashtest.ta.framework.test.result.MetadataType;

/**
 * @author qtran
 */
public class  MultilineMetadata extends AbstractTestMetadata{

    private String text;
    
    private String key;
    
    private List<String> values = new LinkedList<>();
    
    public MultilineMetadata() {
        super.setType(MetadataType.MULTILINE);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public void visit(TestMetadataVisitor visitor) {
        visitor.accept(this);
    }

    @Override
    public String toText() {
        StringBuilder builder = new StringBuilder();
        for (String str : values){
            builder.append(str).append(",");
        }
        builder.deleteCharAt(builder.length()-1);
        this.text = key + " : [" + builder.toString() + ']';
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }    
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 *
 * @author edegenetais
 */
class ResourceAdapter implements ComponentAdapter {
    
    final Resource<?> res;

    public ResourceAdapter(Resource<?> res) {
        this.res = res;
    }

    @Override
    public String getNature() {
        TAResource annotation = res.getClass().getAnnotation(TAResource.class);
        if (annotation == null) {
            ResourceType resourceType = res.getClass().getAnnotation(ResourceType.class);
            LoggerFactory.getLogger(ComponentRepresentation.class).warn("Resource type {} used the depreacated @ResourceType annotation.", res.getClass());
            if (resourceType == null) {
                throw new BrokenTestException("Resource class " + res.getClass() + " is not annotated @TAResource and will not work!");
            } else {
                return resourceType.value();
            }
        } else {
            return annotation.value();
        }
    }
    
     @Override
    public String getType() {
        return "Resource";
    }
}

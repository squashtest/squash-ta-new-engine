/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Reads options given from a file. 
 * Here an option is a key:value pair. 
 * It's a kind of a Properties file except that all the options are comma-separated and written on the same line, 
 * instead of one per line.</p>
 * 
 *  <p>The options are separated using commas ',' 
 * and within an option the key and value are separated by a column ':'.
 * Trailing spaces will be stripped.</p>
 *  
 *  <p>{@literal Example : " name : bob , buddy : mike , hobby : barfly   " -> { "name=bob"; "buddy=mike"; "hobby=barfly"}}</p>
 *  
 * 
 * 
 * @author bsiri
 *
 */
public abstract class OptionsReader {
	
	//this defines a "pair" : "<key> : <value>".
	// the legit separator is ':', not '='. If one encounters a '=' before a ':' is met, then the match fails.
	private static final String PAIR_PATTERN = "[^=]*?:[^,]*";
	//alternate pair pattern to be able to use comma separated lists as values (uses semi-colon as option separator)
	private static final String LIST_HAPPY_PAIR_PATTERN = "[^=]*?:[^;]*";
	
	// a string represents options if it has at least one "<key> : <value>" optionally followed by more of them.
	private static final Pattern OPTIONS_PATTERN = Pattern.compile("^"+PAIR_PATTERN+"(,\\s*"+PAIR_PATTERN+")*$");
	// alternate options pattern to be able to use comma separated lists as values (uses semi-colon as option separator)
	private static final Pattern LIST_HAPPY_OPTIONS_PATTERN = Pattern.compile("^"+LIST_HAPPY_PAIR_PATTERN+"(;\\s*"+LIST_HAPPY_PAIR_PATTERN+")*$");
	
	/**
	 * A basic option reader instance with no key filtering if you don't want to define it.
	 */
	public static final OptionsReader BASIC_READER=new OptionsReader(){
		private Logger logger=LoggerFactory.getLogger(OptionsReader.class.getName()+".BASIC_READER");
		@Override
		protected Set<String> supportedKeys(Map<String, String> source) {return source.keySet();}
		@Override
		protected Logger getLogger() {
			return logger;
		}
	};

	private final Pattern selectedOptionsPattern;
	private final String optionsSeparator;
	
	/**
	 * Default option reader (uses commas to separate options)
	 */
	protected OptionsReader(){
		this(',');
	}
	
	/**
	 * Cusotimzed option reader (parametrize options separator)
	 */
	protected OptionsReader(char optionsSeparator){
		switch(optionsSeparator){
		case ',':
			selectedOptionsPattern=OPTIONS_PATTERN;
			this.optionsSeparator = ",";
			break;
		case ';':
			selectedOptionsPattern=LIST_HAPPY_OPTIONS_PATTERN;
			this.optionsSeparator = ";";
			break;
		default:
			throw new IllegalArgumentException(optionsSeparator+ " is not a supported option separator (choose , or ; )");
		}
	}	
	
	/**
	 * Returns the list of supported keys present in a given option map.
	 * @param source the option map to filter.
	 * @return the {@link Set} of keys from the option set that are supported keys.
	 */
	protected abstract Set<String> supportedKeys(Map<String,String> source);
	protected abstract Logger getLogger();
	
	/**
	 * Returns the list of unsupported keys present in a given option map.
	 * @param source the option map to filter.
	 * @return the {@link Set} of keys from the option set that are NOT supported keys.
	 */
	protected final Set<String> unSupportedKeys(Map<String,String> source){
		Set<String> keys=new HashSet<String>(source.keySet());
		keys.removeAll(supportedKeys(source));
		return keys;
	}
	
	public Map<String, String> getOptions(File file) throws IOException {
		
		
		BufferedReader reader = null;
		try{
			reader = new BufferedReader(new FileReader(file));
			String opts = reader.readLine();
			Map<String, String> emptyMap = Collections.emptyMap();
			return opts==null?emptyMap:getOptions(opts);
		}
		finally{
			if(reader!=null){
				reader.close();
			}
		}
	}
	
	public boolean isOptions(File file) throws IOException{
		
		SimpleLinesDataImpl data = new SimpleLinesDataImpl(file.toURI().toURL());
		
		if (data.getLines().size()!=1) {
			return false;
		}
		
		return isOptions(data.getLines().get(0));
		
	}
	
	public Map<String, String> getOptions(String options){
		
		Map<String, String> opts = new HashMap<String, String>();
		
		String[] pairs = readPairs(options);
		
		if (pairs.length==0) {
			return opts;
		}
		
		for (String option : pairs){
			String trimmed = option.trim();
			String key = readKey(trimmed);
			String value = readValue(trimmed);
			if(opts.containsKey(key)){
				throw new IllegalArgumentException("Parameter '"+key+"' is defined more than once in configuration.");
			}
			opts.put(key, value);
		}
		
		return opts;	
		
	}
	
	public boolean isOptions(String line){
		return selectedOptionsPattern.matcher(line).matches();
	}
	
	private String[] readPairs(String line){
		String[] results = line.split(optionsSeparator);
		for (int i=0;i<results.length;i++){
			results[i] = results[i].trim();
		}
		return results;
	}
	
	private String readKey(String pair){
		int index = pair.indexOf(':');
		if (index == -1) {
			throw new IllegalArgumentException("option '"+pair+"' is invalid");
		}
		
		return pair.substring(0,index).trim();		
	}
	
	private String readValue(String pair){
		int index = pair.indexOf(':');
		if (index == -1){
			throw new IllegalArgumentException("option '"+pair+"' is invalid");
		}
		
		return pair.substring(index+1).trim();		
	}
	
}

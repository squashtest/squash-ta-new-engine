/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Core implementation of the {@link org.squashtest.ta.core.tools.io.SimpleLinesData} functionality.
 * This is package visible because it is NOT part of the Squash TA framework API.
 * For framework components internals only. This is used to have OptionsReader as a framework tool
 * without adding new dependencies to the framework jar.
 * @author edegenetais
 */
class SimpleLinesDataImpl {
private static final Logger LOGGER=LoggerFactory.getLogger(SimpleLinesDataImpl.class);
	
	/** Data lines. */
	private List<String> dataLines=new LinkedList<String>();
	
	/**
	 * Create from raw data.
	 * @param data
	 */
	public SimpleLinesDataImpl(byte[] data){
		this(toLineTable(data, null));
	}
	
	/**
	 * Create from raw data.
	 * @param data
	 */
	public SimpleLinesDataImpl(byte[] data, Charset charset){
		this(toLineTable(data, charset));
	}
	
        
	/**
	 * Load text data from an URL with default encoding.
	 * @param url the URL.
	 * @throws IOException in case the URL can't be opened for reading, or an error occurs while reading from it.
	 */
	public SimpleLinesDataImpl(URL url) throws IOException{
		Reader reader=new InputStreamReader(url.openStream());
		load(url.toExternalForm(), reader);
	}

	/**
	 * Load text data from an URL.
	 * @param url the URL.
	 * @param encoding encoding to enforce
	 * @throws IOException in case the URL can't be opened for reading, or an error occurs while reading from it.
	 */
	public SimpleLinesDataImpl(URL url, String encoding) throws IOException{
		Reader reader=new InputStreamReader(url.openStream(),encoding);
		load(url.toExternalForm(), reader);
	}
	
	/**
	 * Load a text file.
	 * @param path file path.
	 * @throws IOException in case the file can't be opened or an error occurs while reading it.
	 */
	public SimpleLinesDataImpl(final String path) throws IOException{
		FileReader fileReader=new FileReader(path);
		load(path, fileReader);
	}
	
	/**
	 * Load a text file.
	 * @param path file path.
	 * @throws IOException in case the file can't be opened or an error occurs while reading it.
	 */
	public SimpleLinesDataImpl(final String path, String encoding) throws IOException{
		InputStreamReader fileReader=new InputStreamReader(new FileInputStream(path),encoding);
		load(path, fileReader);
	}

	/**
	 * Create from in memory data.
	 * @param data the text lines data.
	 */
	public SimpleLinesDataImpl(Iterable<String> data){
		for(String line:data){
			if(line.endsWith("\n")){
				//filtering
				line=line.substring(0, line.length()-"\n".length());
			}
			dataLines.add(line);
		}
	}
        
        
	private static List<String> toLineTable(byte[] content, Charset charset){
		String contentString;
		if(charset==null){
			contentString=new String(content);
		}else{
			contentString=new String(content, charset);
		}
		String[] lines=contentString.split("\n");
		return Arrays.asList(lines);
	}
        
        
	private void load(final String location, Reader reader)
			throws IOException {
		try{
			BufferedReader lineReader=new BufferedReader(reader);
			String line=lineReader.readLine();
			while(line!=null){
				dataLines.add(line);
				line=lineReader.readLine();
			}
		}finally{
			try{
				reader.close();
			}catch(IOException e){
				LOGGER.warn("Error while closing file "+location+" after loading it.", e);
			}
		}
	}
	
	/**
	 * Get the excluded element list.
	 * @return
	 */
	public List<String> getLines(){
		return dataLines;
	}
	
	/**
	 * Write the data to file. This uses the platform default encoding.
	 * @param path path to the file.
	 * @throws IOException if an I/O error occurs during writing.
	 */
	public void write(String path) throws IOException{
		writeInternal(new FileWriter(path));
	}
	
        /**
         * Write the data to file with the specified encoding.
         * @param path path to the file.
         * @param encoding the required encoding. @see java.nio.Charset documentation for hints on supperted encoding names.
         * @throws IOException if an I/O error occurs during writing.
         */
        public void write(String path, String encoding) throws IOException{
            FileOutputStream outStream=new FileOutputStream(path);
            writeInternal(new OutputStreamWriter(outStream, encoding));
        }
        
	/**
	 * Write the data to file. This uses the platform default encoding.
	 * @param destination file reference.
	 * @throws IOException if an I/O error occurs during writing.
	 */
	public void write(File destination) throws IOException{
		writeInternal(new FileWriter(destination));
	}

        /**
	 * Write the data to file. This uses the platform default encoding.
         * @param encoding the required encoding. @see java.nio.Charset documentation for hints on supperted encoding names.
	 * @param destination file reference.
	 * @throws IOException if an I/O error occurs during writing.
	 */
        public void write(File destination,String encoding) throws IOException{
                FileOutputStream outStream=new FileOutputStream(destination.getAbsoluteFile());
                writeInternal(new OutputStreamWriter(outStream, encoding));
        }
        
        /**
         * Writes data to the file.
         * @param writer file writer. <strong>This method closes the writer when complete.</strong>
         * @throws IOException 
         */
	private void writeInternal(Writer writer)
			throws IOException {
            
		try(BufferedWriter lineWriter=new BufferedWriter(writer);){
			
			for(String line:dataLines){
				lineWriter.write(line);
				lineWriter.newLine();
			}
		}
	}
}

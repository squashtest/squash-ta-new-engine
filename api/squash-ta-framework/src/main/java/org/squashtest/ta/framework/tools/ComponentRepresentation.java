/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.components.UnaryAssertion;

/**
 * Reporting helper to help output TA component references in messages.
 * @author edegenetais
 */
public class ComponentRepresentation {
    
    private final ComponentAdapter componentAdapter;

    public ComponentRepresentation(final Resource<?> res) {
        this.componentAdapter=new ResourceAdapter(res);
    }
    
    public ComponentRepresentation(final BinaryAssertion<?,?> asser){
        this.componentAdapter=new BinaryAssertionAdapter(asser);
    }

    public ComponentRepresentation(final UnaryAssertion<?> asser){
        this.componentAdapter=new UnaryAssertionAdapter(asser);
    }

    public ComponentRepresentation(final ResourceConverter<?,?> convert){
        this.componentAdapter=new ResourceConverterAdapter(convert);
    }

    public ComponentRepresentation(final TargetCreator<?> creator){
        this.componentAdapter=new TargetCreatorAdapter(creator);
    }
    
    public ComponentRepresentation(final Command<?,?> command) {
        this.componentAdapter=new CommandAdapter(command);
    }
    
    @Override
    public String toString(){
        return componentAdapter.getType()+" "+componentAdapter.getNature();
    }

}

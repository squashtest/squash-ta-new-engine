/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.facade;

import java.util.Map;
import org.squashtest.ta.framework.components.Resource;

/**
 * Enhanced interface to make room for new functionalities used by TF Runners. 
 * Old runners and exploitation layer components will still load, but obviously won't
 * benefit from new functionalitites for not being compiled against this interface.
 * This design was preferred to java 8 default methods to avoid loosing JDK7 compatibility
 * too soon.
 * Further extensions to this interface, if needed, may be designed by using default methods when
 * we've had decent deprecation time before dropping JDK7 compatibility.
 * 
 * @author ericdegenetais
 */
public interface TFTestWorkspaceBrowser extends TestWorkspaceBrowser{
            /**
         * This method alows the exploitation layer (project and runner implementations) to create and provide builtin resources 
         * in addition to the builtin resource provided by the Squash TA engine.
         * <div>The following resource names are reserved by the framework, and will be rejected:
         * <ul>
         * <li><code>void</code> : the void resource, used to call commands without INPUT resource.</li>
         * <li><code>context_global_params</code> : global Properties resource defined for the whole test suite by
         * {@link org.squashtest.ta.framework.test.definition.TestSuite#getGlobalParams() }</li>
         * <li><code>context_script_params</code> : Properties resource defined for each test 
         * by {@link org.squashtest.ta.framework.test.definition.Test#getScriptParams()  }.</li>
         * </ul>
         * Global and scripts parameters may be set by the exploitation layer when parsing teh test suite and tests.
         * </div>
         * @return a map of resources. The keys are resource names (with usual resource name syntactic restrictions) 
         * which will be defined in the {@link org.squashtest.ta.framework.test.instructions.ResourceName.Scope#SCOPE_BUILTIN} scope.
         * NB : sonar "don't use wildcard generic return types" rule overriden here 
         * because we KNWO who'll be using the collection (i.e. : the engine code), 
         * and that it will only read it, never try to add anything.
         * So yeap, this map can contain any type of resource, 
         * and the Squash TA engine only needs to know that they are Squash TA test resources.
         */
        Map<String,Resource<?>> getExploitationBuiltinResources();//NOSONAR : the resource type HAS to be generic because we may add any resource type AND this Map is read-only
}

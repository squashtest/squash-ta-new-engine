/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.metadata;

import org.squashtest.ta.framework.test.result.MetadataType;

/**
 * An informative line that holds the metadata used in the METADATA section of the current test case.
 * There are two types of metadata: 
 * <ul>
 *      <li>KEY</li>
 *      <li>KEY : VALUE</li>
 * </ul>
 * A KEY must respect this regex [0-9a-zA-Z_-.]+
 * A VALUE must respect this regex [0-9a-zA-Z_-./]+
 * 
 * No spaces are allowed before KEY.
 * 
 * A KEY can be associated with one or many VALUES.
 * Comment line(s) (but NOT empty line(s)) are allowed inside a multi-line metadata.
 * 
 * @author qtran
 */
public interface TestMetadata {
       	
	/**
	 * May be used as a commentary for instance, and has no impact on the job itself.
	 * 
	 * @return a textual representation of the metadata 
	 */
	String toText();
	
	/**
	 * Set the textual representation
	 * 
	 * @param text
	 */
	void setText(String text);
	
	/**
	 * Get the line number of the original text metadata in the TA script 
         * which leads to this {@link TestMetadata}.
	 * 
	 * @return Return the metadata line number 
	 */
	String getLine();
	
	/**
	 * Set the line number of the text metadata in the original TA script
	 * @param lineNumber The metadata line number
	 */
	void setLine(String lineNumber);
	
	/**
	 *  
	 * @return The type of the current metadata
	 */
	MetadataType getType();
        
        /**
	 * Visitor hook.
	 * 
	 * @param visitor
	 */
	void visit(TestMetadataVisitor visitor);
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.instructions

import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import spock.lang.Specification;

class ResourceNameTest extends Specification{
	def "same object, equals"(){
		given:
			def tst1=new ResourceName(Scope.SCOPE_TEST, "name");
		when:
			def result=tst1.equals(tst1)
		then:
			result==true
	}
	
	def "same scope, same name, equals"(){
		given:
			def tst1=new ResourceName(Scope.SCOPE_TEST, "name");
		and:
			def tst2=new ResourceName(Scope.SCOPE_TEST, "name");
		when:
			def result=tst1.equals(tst2);
		then:
			result==true
	}
	
	def "same scope, different names, not equals"(){
		given:
			def tst1=new ResourceName(Scope.SCOPE_TEST, "name1");
		and:
			def tst2=new ResourceName(Scope.SCOPE_TEST, "name2");
		when:
			def result=tst1.equals(tst2);
		then:
			result==false
	}
	
	def "same name, different scopes, not equals"(){
		given:
			def tst1=new ResourceName(Scope.SCOPE_TEST, "name");
		and:
			def tst2=new ResourceName(Scope.SCOPE_TEMPORARY, "name");
		when:
			def result=tst1.equals(tst2);
		then:
			result==false
	}
	
	def "same object, same hashcode"(){
		given:
		def tst1=new ResourceName(Scope.SCOPE_TEST, "name");
		def tst2=new ResourceName(Scope.SCOPE_TEMPORARY, "name");
	when:
		def result1=tst1.hashCode();
		def result2=tst1.hashCode();
		def result3=tst2.hashCode();
		def result4=tst2.hashCode();
	then:
		result1==result2
		result3==result4
	}
	
	def "equals, same hashcode"(){
		given:
		def tst1v1=new ResourceName(Scope.SCOPE_TEST, "name");
		def tst1v2=new ResourceName(Scope.SCOPE_TEST, "name");
		def tst2v1=new ResourceName(Scope.SCOPE_TEMPORARY, "name2");
		def tst2v2=new ResourceName(Scope.SCOPE_TEMPORARY, "name2");
	when:
		def result1=tst1v1.hashCode();
		def result2=tst1v2.hashCode();
		def result3=tst2v1.hashCode();
		def result4=tst2v2.hashCode();
	then:
		result1==result2
		result3==result4
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools

import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import spock.lang.Specification;

class TempDirTest extends Specification{
	
	
	def "constructExecutionTempDir (1)"(){
		given:
			TempDir.executionTempDir = Mock(File)
		when:
			def result = TempDir.getExecutionTempDir()
		then:
			result.getAbsolutePath() != TempDir.defaultTempDir.getAbsolutePath()
		cleanup:
			TempDir.getMainTempDir().deleteDir()
			TempDir.executionTempDir = null
	}
		
	def "constructExecutionTempDir (2)"(){
		when:
			def result = TempDir.getExecutionTempDir()
		then:
			result.getAbsolutePath() == TempDir.defaultTempDir.getAbsolutePath()
		cleanup:
			TempDir.getMainTempDir().deleteDir()
	}
	
	def "constructMacroTempDir (1)"(){
		given:
			TempDir.macroTempDir = Mock(File)
		when:
			def result = TempDir.getMacroTempDir()
		then:
			result.getAbsolutePath() != TempDir.defaultTempDir.getAbsolutePath()
		cleanup:
			TempDir.getMainTempDir().deleteDir()
			TempDir.macroTempDir = null
	}
		
	def "constructMacroTempDir (2)"(){
		when:
			def result = TempDir.getMacroTempDir()
		then:
			result.getAbsolutePath() == new File(TempDir.defaultTempDir.getAbsolutePath(), TempDir.MACRO_DIR_NAME).getAbsolutePath()
		cleanup:
			TempDir.getMainTempDir().deleteDir()
	}
	
	def "constructDefaultTempDir"(){
		when:
			def result = TempDir.buildDefaultTempDir("target/mockPath")
		then:
			result.getPath().replace("\\", "/") == "target/mockPath/Squash_TA"
		cleanup:
			new File("target/mockPath").deleteDir()
	}
}

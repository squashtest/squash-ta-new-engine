/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.definition

import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import spock.lang.Specification;

class TestTest extends Specification{
	
	def testee = new Test()
	
	def "look for targets (1)"(){
		given:
			def command1 = new ExecuteCommandInstruction()
			command1.setTargetName("pseudotarget1")
			def command2 = new ExecuteCommandInstruction()
			command2.setTargetName("pseudotarget2")
		when:
			testee.addToTests(command1)
			testee.addToTests(command2)
			testee.addToTests(command2)
			def result = testee.getTargetsNames()
		then:
			result.size() == 2
			result.contains("pseudotarget1")
			result.contains("pseudotarget2")
	}
	
	def "look for targets (2)"(){
		given:
			def command1 = new ExecuteCommandInstruction()
			command1.setTargetName("pseudotarget1")
			def command2 = new ExecuteCommandInstruction()
			command2.setTargetName("pseudotarget2")
		when:
			testee.addToSetup(command1)
			testee.addToTests(command2)
			testee.addToTeardown(command2)
			def result = testee.getTargetsNames()
		then:
			result.size() == 2
			result.contains("pseudotarget1")
			result.contains("pseudotarget2")
	}
}

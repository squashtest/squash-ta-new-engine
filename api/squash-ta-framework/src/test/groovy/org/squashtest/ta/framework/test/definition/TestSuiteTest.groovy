/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.test.definition

import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import spock.lang.Specification;

class TestSuiteTest extends Specification{
	
	def testee = new TestSuite()
	
	def "look for targets (1)"(){
		given:
			def command1 = new ExecuteCommandInstruction()
			command1.setTargetName("pseudotarget1")
			def command2 = new ExecuteCommandInstruction()
			command2.setTargetName("pseudotarget2")
		and:
			def test = new Test()
			test.addToTests(command1)
			test.addToTests(command2)
			test.addToTests(command2)
		and:
			def list = new ArrayList<Test>()
			list.add(test)
		and:
			def ecosystem = new Ecosystem()
			ecosystem.setTestPopulation(list)
			def environment = Mock(Environment)
			environment.getSetUp() >> new Test()
			environment.getTearDown() >> new Test()
			ecosystem.setEnvironment(environment)
			
			
		and:
			testee.addEcosystem(ecosystem)
		when:

			def result = testee.getTargetsNames()
		then:
			result.size() == 2
			result.contains("pseudotarget1")
			result.contains("pseudotarget2")
	}
	
	def "look for targets (2)"(){
		given:
			def command1 = new ExecuteCommandInstruction()
			command1.setTargetName("pseudotarget1")
			def command2 = new ExecuteCommandInstruction()
			command2.setTargetName("pseudotarget2")
		and:
			def test1 = new Test()
			test1.addToSetup(command1)
			def test2 = new Test()
			test2.addToTests(command2)
		and:
			def list1 = new ArrayList<Test>()
			list1.add(test1)
			def list2 = new ArrayList<Test>()
			list2.add(test2)
		and:
			def ecosystem1 = new Ecosystem()
			ecosystem1.setTestPopulation(list1)
			def environment = Mock(Environment)
			environment.getSetUp() >> new Test()
			environment.getTearDown() >> new Test()
			ecosystem1.setEnvironment(environment)
			def ecosystem2 = new Ecosystem()
			ecosystem2.setTestPopulation(list2)
			ecosystem2.setEnvironment(environment)
		and:
			testee.addEcosystem(ecosystem1)
			testee.addEcosystem(ecosystem2)
		when:
			def result = testee.getTargetsNames()
		then:
			result.size() == 2
			result.contains("pseudotarget1")
			result.contains("pseudotarget2")
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.test.toolkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Creates various collections to code for expectations.
 * @author edegenetais
 */
public class ExpectedCollections {
    private ExpectedCollections(){/* Static class no instances*/}
    
    public static <E> List<E> expectedList(E... expectedElements){
        List<E> expectedList=new ArrayList<>(expectedElements.length);
        expectedList.addAll(Arrays.asList(expectedElements));
        return Collections.unmodifiableList(expectedList);
    }
    
    public static <E> Set<E> expectedSet(E... expectedElements){
        Set<E> expectedSet=new HashSet<>(expectedElements.length);
        expectedSet.addAll(Arrays.asList(expectedElements));
        return Collections.unmodifiableSet(expectedSet);
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.test.toolkit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.tools.TempDir;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Utility class to test file-dependant components while easily deploying the test files from test resources,
 * AND cleaning them up after test.
 * 
 * This class is intended for people who don't want their project to get ear-pointy, cold and over-refined ;).
 * If you're not one of them, look at <code>org.squashtest.ta.api.test.toolkit.ResourceExtractorSpockBase</code> instead.
 * @author Éric Dégenètais
 *
 */
public class ResourceExtractorTestBase {
        private static final Logger LOGGER = LoggerFactory.getLogger(ResourceExtractorTestBase.class);

        private static final FailedClassFileCleanup FAILED_CLASS_FILE_CLEANUP = new FailedClassFileCleanup();
        private static final SuccessfulClassFileCleanup SUCCESSFUL_CLASS_CLEANUP = new SuccessfulClassFileCleanup();
            
	private final Logger extractorLogger = LoggerFactory.getLogger(getClass());
        
        private static final Set<File> fileErasingFailedAtTestCleanup=new HashSet<>();
        private static final Set<File> classRemovedFiles=new HashSet<>();
	private final Set<File> extractedResources=new HashSet<>();
        private final Set<File> testRemovedFiles=new HashSet<>();
        private final Set<InputStream> streamsToClose=new HashSet<>();
        private final Class<?> classWeDelegateFor;

        public ResourceExtractorTestBase() {
            //This noop default constructor exists because we nedd both a default AND non-*default constructor, see below
            classWeDelegateFor=null;
        }
        
        
        public ResourceExtractorTestBase(Class<?> classWeDelegateFor){
            try {
                Class<?> spockSpecificationClass=Class.forName("spock.lang.Specification");
                if(spockSpecificationClass.isAssignableFrom(classWeDelegateFor)){
                    this.classWeDelegateFor=classWeDelegateFor;
                }else{
                    throw new LinkageError("This constructor is intended to build a delegate for ResourceExtractorSpockBase only. Your are obviously not in a spock context, forget about it.");
                }
            } catch (ClassNotFoundException ex) {
                throw new LinkageError("This constructor is intended to build a delegate for ResourceExtractorSpockBase only. Your are obviously not in a spock context, forget about it.",ex);
            }
        }
        
        
        
        /**
         * This method creates an empty temporary file and adds it to the list of files to wipe.
         * This can be used to create desintaion files when the tested component creates a result file.
         * @return the created file path as a File object.
         * @throws IOException 
         */
        public File createNtrackFile() throws IOException{
            final File tempFile = createTempFile();
            extractedResources.add(tempFile);
            return tempFile;
        }
        
        public File createNtrackDir() throws IOException{
            final File tempDir = createTempDir();
            extractedResources.add(tempDir);
            return tempDir;
        }
        
        /**
         * This is the static version of {@link #createNtrackFile() }
         * @return the created empty temporary file.
         * @throws IOException 
         */
        public static File createFile4Class() throws IOException{
            File f=new ResourceExtractorTestBase().createNtrackFile();
            fileErasingFailedAtTestCleanup.add(f);
            return f;
        }
        
        /**
         * Call this method from your daughter test class when it needs to deploy
         * a packaged resource (eg : part of src/test/resource) as a temporary file.
         * @param resource the name of the resource to deploy.
         * @return the reference of the temporary file.
         * @throws IOException 
         */
	protected File createFile(String resource) throws IOException{
		File f = createTempFile();
		extractToFile(f,resource);
		return f;
	}

        /**
         * Call this method from your daughter class when it needs to deploy a packaged resource 
         * AND choose the location and/or name of the created file.
         * @param f the destination file.
         * @param resource the resource name.
         * @throws IOException
         * @throws FileNotFoundException if the parent directory of the specified destination , file does not exist.
         */
	protected void extractToFile(File f, String resource) throws IOException {
		extractedResources.add(f);
            
		try(
				InputStream rsc=openResourceStream(resource);
				OutputStream os=new FileOutputStream(f);
				){
                        
			byte[]buffer=new byte[4096];
			int read=rsc.read(buffer);
			while(read>=0){
				os.write(buffer, 0, read);
				read=rsc.read(buffer);
			}
		}
	}
        
        /**
         * Call this method to open a stream and forget about it, 
         * with a nice exception message if the resource is not found.
         * @param resource the name of the resource.
         * @return 
         */
        protected InputStream openResourceStream(String resource) {
            final Class<?> aClass;
            if(classWeDelegateFor==null){
                LOGGER.debug("No delegate user class registered, we'll suppose this is called from a ResourceExtractorTestBase subclass");
                aClass= getClass();
            }else{
                LOGGER.debug("Delegatinng for class {}.",classWeDelegateFor.getName());
                aClass=classWeDelegateFor;
            }
            final InputStream rsc = aClass.getResourceAsStream(resource);
            if(rsc==null){
               throw new MissingResourceException("Resource "+resource+" not found, it cannot be extracted.", getClass().getName(), resource);
            }
            return rsc;
        }
	
        /**
         * This version of the targeted file extractor method may be called without fuss from a static context 
         * AND allow the file to be automatically swept at the end of the test.
         * @see ResourceExtractorTestBase#extractToFile(java.io.File, java.lang.String) 
         * @param f the destination file
         * @param resource the resource name
         * @throws FileNotFoundException
         * @throws IOException 
         */
	protected static void extractToFile4Class(File f, String resource) throws FileNotFoundException, IOException{
		new ResourceExtractorTestBase().extractToFile(f, resource);
	}
	
         /**
         * This version of the temporary file extractor method may be called without fuss from a static context 
         * AND allow the file to be automatically swept at the end of the test.
         * @see ResourceExtractorTestBase#createFile(java.lang.String)
         * @param resource the resource name
         * @return the reference of the temporary file
         * @throws FileNotFoundException
         * @throws IOException 
         */
	protected static File createFile4Class(String resource) throws IOException{
		File f=new ResourceExtractorTestBase().createFile(resource);
		fileErasingFailedAtTestCleanup.add(f);
		return f;
	}
	
        /**
         * This method is called by the junit framework after each test to remove the files it created through this resource manager instance.
         */
	@After
	public void removeTestFiles(){                
		for(File f:extractedResources){
			singleDelete(f, new FailedTestFileCleanup(), new SuccessfullTestFileCleanup());
		}
                extractedResources.removeAll(testRemovedFiles);
	}
        
        /**
         * This method is called by the junit framework after each test to ensure that streams are closed.
         */
        @After
        public void closeAllStreams(){
            for(InputStream is:streamsToClose){
                try{
                    is.close();
                    LOGGER.debug("Registered InputStream closed.");
                }catch(IOException e){
                    LOGGER.warn("Closing registered InputStream failed.", e);
                }
            }
        }
        
        /**
         * This method is called by the junit framework after each test to remove the files it created through this resource manager class.
         * It will also try to reclaim files that were not successfully destroyed by the {@link ResourceExtractorTestBase#removeTestFiles()}
         */
	@AfterClass
	public static void removeFailedTestFiles(){
		for(File f:fileErasingFailedAtTestCleanup){
                    singleDelete(f, FAILED_CLASS_FILE_CLEANUP, SUCCESSFUL_CLASS_CLEANUP);
		}
                synchronized(classRemovedFiles){
                    fileErasingFailedAtTestCleanup.removeAll(classRemovedFiles);
                    classRemovedFiles.clear();
                }
	}
        
        /**
         * This method creates a file derived from the input file by replacing its OEL marks
         * by the current platform standard EOL mark.
         * Use this with care, because it might hide tested class defacts. However, if the class
         * under test is supposed to create files with the default EOL, this will make using static
         * expected file contents easier by adjusting them to the current platform default. 
         * One caveat : this will add a final EOL mark even if the input file had none.
         * @param f input file
         * @return a file with the same content as the f input file, except EOL which are replaced by the platform current EOL.
         * @throws IOException 
         */
        protected File toPlatformEndOfLine(File f) throws IOException{
            File dest=createTempFile();
            extractedResources.add(dest);
            try(
                    BufferedReader reader=new BufferedReader(new FileReader(f));
                    PrintWriter writer=new PrintWriter(dest);
                    ){
                String line=reader.readLine();
                while(line!=null){
                    writer.println(line);
                    line=reader.readLine();
                }
            }
            return dest;
        }
        
        /**
         * This static function allows the use of {@link #toPlatformEndOfLine(java.io.File)} from a static context.
         * @param f the input file
         * @see #toPlatformEndOfLine(java.io.File) 
         * @return the result of EOL replacement.
         * @throws IOException 
         */
        protected static File toPlatformEndOfLine4Class(File f) throws IOException{
            File dest=new ResourceExtractorTestBase().toPlatformEndOfLine(f);
            fileErasingFailedAtTestCleanup.add(dest);
            return dest;
        }
        
        /**
         * This method asserts that actual and expected contents match.
         * The first detected line difference is shown in the test failure message with the line number.
         * @see #checkActualContentAgainstExpected(java.io.File, java.io.File) 
         * @param actual actual file to test.
         * @param expectedResourceName name of a test resource which defines the expected file content.
         * @throws IOException 
         */
        protected void checkActualContentAgainstExpected(File actual, String expectedResourceName) throws IOException {
            File expected = createFile(expectedResourceName);
	    checkActualContentAgainstExpected(actual,expected);
	}

        /**
         * This method asserts that actual and expected contents match.
         * The first detected line difference is shown in the test failure message with the line number.
         * @see #checkActualContentAgainstExpected(java.io.File, java.lang.String) 
         * @param actual actual file to test.
         * @param expected file with the expected contents.
         * @throws IOException 
         */
        protected void checkActualContentAgainstExpected(File actual, File expected) throws IOException {
            try (final BufferedReader actualReader = new BufferedReader(new FileReader(actual));final BufferedReader expectedReader = new BufferedReader(new FileReader(expected))) {
                String actualLine = actualReader.readLine();
                List<String> actualContent = new ArrayList<>();
                while (actualLine != null) {
                    LOGGER.debug(actualLine);
                    actualContent.add(actualLine);
                    actualLine = actualReader.readLine();
                }
                int line=1;
                for (String currentLine : actualContent) {
                    assertEquals("Line "+line+" diff:",expectedReader.readLine(), currentLine);
                    line++;
                }
                assertEquals("Line "+line+" diff:",expectedReader.readLine(), actualLine);
                assertNull("Actual is shorter than expected", expectedReader.readLine());
            }
        }
        
        private File createTempFile() throws IOException {
            return File.createTempFile("rsc", "tmp",TempDir.getExecutionTempDir());
        }
        
        private File createTempDir() throws IOException{
            return Files.createTempDirectory("tmp").toFile();
        }
        
        private static void singleDelete(File f, FileAction failure,FileAction success) {
            if(f.isDirectory()){
                for(File child:f.listFiles()){
                    singleDelete(child, failure,success);
                }
            }
            if(f.delete()){
                success.invoke(f);
            }else{
                failure.invoke(f);
            }
	}
        
        private static interface FileAction{
            void invoke(File f);
        }
        
        private static class SuccessfulClassFileCleanup implements FileAction {
            @Override
            public void invoke(File f) {
                synchronized(classRemovedFiles){
                    classRemovedFiles.add(f);
                }
                LOGGER.info("No deletion for {} at test cleanup, but successfull delete at class cleanup.",f.getAbsolutePath());
            }
        }

        private static class FailedClassFileCleanup implements FileAction {

            @Override
            public void invoke(File f) {
                if(f.exists()){
                    LOGGER.warn("{} deletion failed definively.",f.getAbsolutePath());
                }else{
                    LOGGER.debug("File {} was still registered for deletion, but does not exist anymore.",f.getAbsolutePath());
                }
            }
        }

        private class FailedTestFileCleanup implements FileAction {

            @Override
            public void invoke(File f) {
                if(f.exists()){
                    extractorLogger.warn("{} deletion failed after test.",f.getAbsolutePath());
                    fileErasingFailedAtTestCleanup.add(f);
                }else{
                    LOGGER.debug("File {} was still registered for deletion, but does not exist anymore.",f.getAbsoluteFile());
                }
            }
        }

        private class SuccessfullTestFileCleanup implements FileAction {

            @Override
            public void invoke(File f) {
                testRemovedFiles.add(f);
                extractorLogger.debug("{} deleted OK.",f.getAbsolutePath());
            }
        }
}

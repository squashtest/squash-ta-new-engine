/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.test.toolkit;

import spock.lang.Specification;

/**
 * This class should be used as base class when writing spock tests for components that need files.
 * @author edegenetais
 */
public class ResourceExtractorSpockBase extends Specification{
    
    private ResourceExtractorTestBase delegate;
    
    /**
     * @see ResourceExtractorTestBase#createNtrackFile(java.lang.String)
     */
    protected File createNtrackFile(){
        return delegate.createNtrackFile();
    }

    /**
     * @see ResourceExtractorTestBase#createNtrackDir()
     */
    protected File createNtrackDir(){
        return delegate.createNtrackDir();
    }
    
    /**
     * @see ResourceExtractorTestBase#createFile4Class()
     */
    protected static File createFile4Class(){
        return ResourceExtractorTestBase.createFile4Class();
    }
    
    /**
     * @see ResourceExtractorTestBase#createFile(java.lang.String)
     */    
    protected File createFile(String resourceName){
        return delegate.createFile(resourceName);
    }
    
    /**
     * @see ResourceExtractorTestBase#createFile4Class(java.lang.String)
     */
    protected static File createFile4Class(String resourceName){
        return ResourceExtractorTestBase.createFile4Class(resourceName);
    }
    
     /**
     * @see ResourceExtractorTestBase#extractToFile(java.io.File,java.lang.String)
     */
    protected void extractToFile(File f, String resourceName){
        delegate.extractToFile(f,resourceName);
    }
    /**
     * @see ResourceExtractorTestBase#extractToFile4Class(java.io.File,java.lang.String)
     */
    protected void extractToFile4Class(File f, String resourceName){
        delegate.extractToFile4Class(f,resourceName);
    }
    
    /**
     * @see ResourceExtractorTestBase#openResourceStream(java.lang.String)
     */
    protected InputStream openResourceStream(String resourceName){
        return delegate.openResourceStream(resourceName);
    }
    
    /**
     * @see ResourceExtractorTestBase#toPlatformEndOfLine(java.io.File)
     */
    protected File toPlatformEndOfLine(File f){
        return delegate.toPlatformEndOfLine(f);
    }
    
     /**
     * @see ResourceExtractorTestBase#toPlatformEndOfLine4Class(java.io.File)
     */
    protected static File toPlatformEndOfLine4Class(File f){
        return ResourceExtractorTestBase.toPlatformEndOfLine4Class(f);
    }

     /**
     * @see ResourceExtractorTestBase#checkActualContentAgainstExpected(java.io.File,java.lang.String)
     */
    protected void checkActualContentAgainstExpected(File actual, String expectedResourceName){
        delegate.checkActualContentAgainstExpected(actual, expectedResourceName);
    }

     /**
     * @see ResourceExtractorTestBase#checkActualContentAgainstExpected(java.io.File,java.lang.String)
     */
    protected void checkActualContentAgainstExpected(File actual, File expected){
        delegate.checkActualContentAgainstExpected(actual, expected);
    }

    def setup(){
        delegate=new ResourceExtractorTestBase(getClass());
    }
    
    def cleanup(){
        delegate.removeTestFiles();
        delegate.closeAllStreams();
    }
    
    def cleanupSpec(){
        ResourceExtractorTestBase.removeFailedTestFiles();
    }
}

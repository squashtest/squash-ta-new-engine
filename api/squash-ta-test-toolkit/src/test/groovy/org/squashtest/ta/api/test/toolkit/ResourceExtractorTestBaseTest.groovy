/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.test.toolkit;

import org.squashtest.ta.core.tools.io.SimpleLinesData;

import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.api.test.toolkit.testhelpers.ResourceExtractorTestee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spock.lang.Specification;

/**
 * 
 * @author edegenetais
 */
class ResourceExtractorTestBaseTest extends Specification{
    private static final Logger LOGGER=LoggerFactory.getLogger(ResourceExtractorTestBaseTest.class);
    
    ResourceExtractorTestee testee;

    def setup(){
        testee=new ResourceExtractorTestee();
    }
    
    def teardown(){
        testee.removeTestFiles();
        ResourceExtractorTestBase.removeFailedTestFiles();
    }
    
    def "Should create the expected test method resource file with expected content."(){
        when:
            File f=testee.createFile("/extractor/target-resource.txt");
        then:
            f.exists()
            SimpleLinesData extractedData=new SimpleLinesData(f.getAbsolutePath());
            "This is target resource line 1".equals(extractedData.getLines().get(0));
            "This is line 2".equals(extractedData.getLines().get(1));
            extractedData.getLines().size()==2;
    }
    
    def "Should create the expected test class resource file with expected content."(){
        when:
            File f=ResourceExtractorTestBase.createFile4Class("/extractor/target-resource.txt");
        then:
            f.exists()
            SimpleLinesData extractedData=new SimpleLinesData(f.getAbsolutePath());
            "This is target resource line 1".equals(extractedData.getLines().get(0));
            "This is line 2".equals(extractedData.getLines().get(1));
            extractedData.getLines().size()==2;
            ResourceExtractorTestBase.removeFailedTestFiles();
    }
    
    def "should create test method file where told to with expected content"(){
        given:
        
            File f=File.createTempFile("test",".txt");
            
            if(f.delete()){
                LOGGER.debug("Temporary file properly deleted before test - will KNOW if the class is able to create files.");
            }else{
                LOGGER.warn("Temporary file was not properly deleted before test - will KNOW if the class is able to create files.");
            }
            
        when:
            testee.extractToFile(f,"/extractor/target-resource.txt")
        then:
            f.exists()
            SimpleLinesData extractedData=new SimpleLinesData(f.getAbsolutePath());
            "This is target resource line 1".equals(extractedData.getLines().get(0));
            "This is line 2".equals(extractedData.getLines().get(1));
            extractedData.getLines().size()==2;
    }
    
     def "should create test class file where told to with expected content"(){
        given:
        
            File f=File.createTempFile("test",".txt");
            
            if(f.delete()){
                LOGGER.debug("Temporary file properly deleted before test - will KNOW if the class is able to create files.");
            }else{
                LOGGER.warn("Temporary file was not properly deleted before test - will KNOW if the class is able to create files.");
            }
            
        when:
                ResourceExtractorTestBase.extractToFile4Class(f,"/extractor/target-resource.txt");
        then:
            f.exists()
            SimpleLinesData extractedData=new SimpleLinesData(f.getAbsolutePath());
            "This is target resource line 1".equals(extractedData.getLines().get(0));
            "This is line 2".equals(extractedData.getLines().get(1));
            extractedData.getLines().size()==2;
    }
    
    def "test method temp files should not exist anymore after cleanup is called"(){
        given:
            File f=testee.createFile("/extractor/target-resource.txt");
        and:
            File f2=testee.createFile("/extractor/target-resource2.txt");
        when:
            testee.removeTestFiles();
        then:
            !f.exists()
            !f2.exists()
    }
    
     def "class temp files should not exist anymore after cleanup is called"(){
        given:
            File f=testee.createFile4Class("/extractor/target-resource.txt");
        and:
            File f2=testee.createFile4Class("/extractor/target-resource2.txt");
        when:
            testee.removeFailedTestFiles()
        then:
            !f.exists()
            !f2.exists()
    }
    
    def "should accept files when identical"(){
        given:
            File firstCopy=testee.createFile("/extractor/target-resource.txt")
        when:
            testee.checkActualContentAgainstExpected(firstCopy,"/extractor/target-resource.txt")
        then:
            true
    }
    
    def "should fail if line content differs"(){
        given:
            File firstCopy=testee.createFile("/extractor/target-resource.txt")
        when:
            testee.checkActualContentAgainstExpected(firstCopy,"/extractor/target-resource-same-size-line1-diff.txt")
        then:
            thrown AssertionError
    }
    
    def "should fail if actuel shorter than expected"(){
        given:
            File firstCopy=testee.createFile("/extractor/target-resource.txt")
        when:
            testee.checkActualContentAgainstExpected(firstCopy,"/extractor/target-resource-longer.txt")
        then:
            thrown AssertionError
    }
    
    def "should fail if actuel longer than expected"(){
        given:
            File firstCopy=testee.createFile("/extractor/target-resource.txt")
        when:
            testee.checkActualContentAgainstExpected(firstCopy,"/extractor/target-resource-shorter.txt")
        then:
            thrown AssertionError
    }
}

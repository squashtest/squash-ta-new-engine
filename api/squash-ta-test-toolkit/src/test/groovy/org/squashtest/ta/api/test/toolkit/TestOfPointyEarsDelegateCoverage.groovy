/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.test.toolkit

import spock.lang.Specification;

import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.List;
import java.util.ArrayList;

/**
 * Best effort at checking delegate coverage.
 * @author ericdegenetais
 */
class TestOfPointyEarsDelegateCoverage extends Specification{
    /* These signatures are excluded from test base because they are not part of the testbase interface */
    List<String> excludedSignatures=[
        "public void org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase.removeTestFiles()",
        "public static void org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase.removeFailedTestFiles()",
        "public void org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase.closeAllStreams()"
    ];
    
    def setup(){
        for(Method m:Object.class.getMethods()){
            excludedSignatures.add(m.toGenericString());
        }
    }
    
    def "allResourceExtractorTestBaseMethodsMustBeDelegated"(){
        given:
            Logger LOGGER = LoggerFactory.getLogger(TestOfPointyEarsDelegateCoverage.class);
        and:
            Class<?> honestClass=ResourceExtractorTestBase.class;
            List<Method> honestMethods=new ArrayList(Arrays.asList(honestClass.getMethods()))
            
            for(Method m:honestClass.getDeclaredMethods()){
                if(Modifier.isProtected(m.getModifiers())){
                    honestMethods.add(m);
                }
            }
            
        and:
            Class<?> pointyEarsClass=ResourceExtractorSpockBase.class;
        when:
            StringBuilder missingMethods=new StringBuilder();
            LOGGER.info("Begin checking method coverage.");
            for(Method honestMethod:honestMethods){
                if(excludedSignatures.contains(honestMethod.toGenericString())){
                    LOGGER.info("Method {} is excluded (not part of the ResourceExtractorTestBase interface).",honestMethod.toGenericString())
                }else{
                    try{
                        Method m=pointyEarsClass.getDeclaredMethod(honestMethod.getName(),honestMethod.getParameterTypes());
                        LOGGER.info("Method {} is covered in the Spock delegate. You won't get the 'pinch' !",m.getName());
                    }catch(NoSuchMethodException nsme){
                        LOGGER.error("Uncovered protected method {} in delegate",honestMethod.toGenericString(),nsme);
                        missingMethods.append(honestMethod.toGenericString()).append(" is missing\n");
                    }
                }
            }
            def missingMethodString=missingMethods.toString();
        then:
            missingMethodString=="";
    }
}


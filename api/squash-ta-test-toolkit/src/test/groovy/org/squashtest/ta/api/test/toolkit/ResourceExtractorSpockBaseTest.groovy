/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.test.toolkit

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test that a specification derived from the base works in itself, and gets resource files.
 * @author ericdegenetais
 */
class ResourceExtractorSpockBaseTest extends ResourceExtractorSpockBase{
        static File testResource;
	def "If the specification is functional at all this should succeed"(){
            when:
                LoggerFactory.getLogger(ResourceExtractorSpockBaseTest.class).info("Testing if this spec works even slightly")
            then:
                true
        }
        
        def "here we shall create a file and see if it exists when we cleanup the spec"(){
            when:
                testResource=createFile("/extractor/target-resource.txt")
            then:
                testResource.exists()
        }
        
        /** 
         * This cleanupSpec is a bit subverted, 
         * as it is misused as a test for an after test cleanup feature 
         * of the base specification.
         * I found no other way. 
         * */
        def cleanupSpec(){
            if(testResource.exists()){
                Assert.fail("yeaks ! testResource still exists at specCleanup time, it should have been wiped at test cleanup time!")
            }else{
                LoggerFactory.getLogger(ResourceExtractorSpockBaseTest.class).info("Temporary file {} has been created by a test and auto-removed sucessfully",testResource.getAbsolutePath())
            }
        }
}


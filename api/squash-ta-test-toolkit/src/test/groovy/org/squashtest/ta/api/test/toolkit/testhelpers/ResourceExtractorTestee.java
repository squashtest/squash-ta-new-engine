/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * This class is used as a testee real type to test class ResourceExtractor's logic.
 * It makes its protected methods public as they are part of its API as Superclass
 * from which to derive test classes that use resources deployed as temporary files
 * as input or as expected output.
 * 
 * @author edegenetais
 */
package org.squashtest.ta.api.test.toolkit.testhelpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

public class ResourceExtractorTestee extends ResourceExtractorTestBase{

    @Override
    public File createFile(String resource) throws IOException {
        return super.createFile(resource);
    }

    @Override
    public void extractToFile(File f, String resource) throws IOException, FileNotFoundException {
        super.extractToFile(f, resource); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeTestFiles() {
        super.removeTestFiles(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void checkActualContentAgainstExpected(File actual, File expected) throws IOException {
        super.checkActualContentAgainstExpected(actual, expected); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void checkActualContentAgainstExpected(File actual, String expectedResourceName) throws IOException {
        super.checkActualContentAgainstExpected(actual, expectedResourceName); //To change body of generated methods, choose Tools | Templates.
    }
    
}

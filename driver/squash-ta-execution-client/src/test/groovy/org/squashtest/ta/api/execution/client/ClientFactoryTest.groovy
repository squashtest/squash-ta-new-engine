/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.execution.client

import org.apache.cxf.jaxrs.client.WebClient;

import spock.lang.Specification
import spock.lang.Unroll

class ClientFactoryTest extends Specification {
	
	ClientFactory testee
	
	def setup(){
		testee = new ClientFactory()
	}
	
	@Unroll
	def "Test searchBaseKey method with various value in the property file"(){
		given: 
			Properties prop = new Properties()
			prop.put(baseKey1, confUrl1)
			prop.put(baseKey2, confUrl2)


		when:
			def result = testee.searchBaseKey(testee.cleanUrl(endpointUrl),prop)
		then:
			result == resultValue
			
		where:
			endpointUrl                     | confUrl1                        | baseKey1                             | confUrl2                        | baseKey2                              || resultValue
			"http://localhost:8080/squash"  | "http://localhost:8080/squash"  | "endpoint.1"                         | "http://localhost"              | "endpoint.2"                          || "endpoint.1"
			"http://localhost:8080/squash"  | "http://localhost:8080/squash/" | "endpoint.1"                         | "http://localhost"              | "endpoint.2"                          || "endpoint.1"
			"http://localhost:8080/squash/" | "http://localhost:8080/squash"  | "endpoint.1"                         | "http://localhost"              | "endpoint.2"                          || "endpoint.1"
			"http://localhost:8080/squash/" | "http://localhost:8080/squash/" | "endpoint.1"                         | "http://localhost"              | "endpoint.2"                          || "endpoint.1"
			"http://localhost:8080/squash"  | "http://localhost:8080/squash"  | "endpoint.20"                        | "http://localhost:8080/squash"  | "endpoint.5"                          || "endpoint.5"
			"http://localhost:8080/squash"  | "http://localhost:8080/squash"  | "endpoint.20"                        | "http://localhost:8080/squash/" | "endpoint.5"                          || "endpoint.5"
			"http://localhost:8080/squash/" | "http://localhost:8080/squash"  | "endpoint.20"                        | "http://localhost:8080/squash"  | "endpoint.5"                          || "endpoint.5"
			"http://localhost:8080/squash/" | "http://localhost:8080/squash"  | "endpoint.20"                        | "http://localhost:8080/squash/" | "endpoint.5"                          || "endpoint.5"
			"http://localhost:8080/squash/" | "myLogin"                       | "http://localhost:8080/squash.login" | "myLogin"                       | "http://myComputer:8080/squash.login" || "http://localhost:8080/squash"
			"http://localhost:8080/squash/" | "myLogin"                       | "http://localhost:8080/squash.login" | "http://localhost:8080/squash/" | "endpoint.5"                          || "endpoint.5"
			"http://localhost:8080/squash/" | "http://localhost:8080/squash"  | "endpoint.20"                        | "myLogin"                       | "http://myComputer:8080/squash.login" || "endpoint.20"
	}
	

	def "The method getCredentials should return null when a null endpointLoginConfFile is given"(){
		when:
			def res = testee.getCredentials("", null)
			
		then:
			res == null
	}
	
	def "When getClient is call with a null endpointLoginConfFile argument then the client shoudn't contain the \"Authorization\" key"(){
		when:
			def StatusUpdate res = testee.getClient("", null)
		then:
			res!=null
			WebClient.client(res).getHeaders().containsKey("Authorization") == false
	}
	
	def "When getClient is call with correct argument then the client shoud contain the \"Authorization\" key"(){
		when:
			File confFile = new File(getClass().getResource("conf.properties").toURI());
			def StatusUpdate res = testee.getClient("localhost:9000", confFile)
		then:
			res!=null
			WebClient.client(res).getHeaders().containsKey("Authorization") == true
	}
	
	def "Test generateAuthorizationHeader method with new configuration format"(){
		given:
			Properties prop = new Properties()
			prop.put(baseKey1, confUrl1)
			prop.put(baseKey1+".login", confLogin1)
			prop.put(baseKey1+".password", confPwd1)
			prop.put(baseKey2, confUrl2)
			prop.put(baseKey2+".login", confLogin2)
			prop.put(baseKey2+".password", confPwd2)


		when:
			def authorizationHeader = testee.generateAuthorizationHeader(baseKey,prop)
		then:
			authorizationHeader == "Basic " + org.apache.cxf.common.util.Base64Utility.encode((login+":"+password).getBytes());
			
		where:
			baseKey      | confUrl1                       | baseKey1      | confLogin1 | confPwd1 | confUrl2           | baseKey2     | confLogin2 | confPwd2 || login    | password 
			"endpoint.1" | "http://localhost:8080/squash" | "endpoint.1"  | "login1"   | "pwd1"   | "http://localhost" | "endpoint.2" | "login2"   | "pwd2"   || "login1" | "pwd1"

	}
	
	def "Test generateAuthorizationHeader method with old configuration format"(){
		given:
			Properties prop = new Properties()
			prop.put(baseKey1+".login", confLogin1)
			prop.put(baseKey1+".password", confPwd1)
			prop.put(baseKey2+".login", confLogin2)
			prop.put(baseKey2+".password", confPwd2)

		when:
			def authorizationHeader = testee.generateAuthorizationHeader(baseKey,prop)
		then:
			authorizationHeader == "Basic " + org.apache.cxf.common.util.Base64Utility.encode((login+":"+password).getBytes());
			
		where:
			baseKey                        | baseKey1                        | confLogin1 | confPwd1 | baseKey2           | confLogin2 | confPwd2 || login    | password
			"http://localhost:8080/squash" | "http://localhost:8080/squash"  | "login1"   | "pwd1"   | "http://localhost" | "login2"   | "pwd2"   || "login1" | "pwd1"

	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.tf.rest;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.execution.client.ClientFactory;
import org.squashtest.ta.core.library.properties.DefaultPropertiesSource;

/**
 *
 * @param <ClientType> : type of the produced client.
 * @author edegenetais
 */
public abstract class ClientFactoryBase<ClientType> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ClientFactory.class);
    /* marshalling configuration under there! */
    protected static final List<Object> PROVIDERS = new ArrayList<Object>();

    static {
		JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
		PROVIDERS.add(provider);
    }
    
    /**
     * Get a configured client for the specified endpoint. Questions?
     *
     * @param endPoint
     *            endpoint URL to connect to.
     * @return the client.
     */
    public ClientType getClient(String endPoint, File endpointLoginConfFile) {
        ClientType client = null;
        client = JAXRSClientFactory.create(endPoint, getBindingClass(), PROVIDERS);
        String authorizationHeader = getCredentials(endPoint, endpointLoginConfFile);
        if (authorizationHeader != null) {
            WebClient.client(client).header("Authorization", authorizationHeader);
        }
        return client;
    }

    protected abstract <E extends ClientType> Class<E> getBindingClass();
    
    /**
     * This method provides the credential by using the given configuration file. The file is a properties file. This
     * file is initially load then process to retrieve the login and password for the given endpoint. Finally the
     * authorization header string is build. If the data for the endpoint are not found then the methos return
     * <code>null</code> and no credentials would be used.
     *
     * @param endpoint
     *            The url of the endpoint
     * @param endpointLoginConfFile
     *            The configuration file
     * @return the authorization header
     */
    protected String getCredentials(String endpoint, File endpointLoginConfFile) {
        String authorizationHeader = null;
        if (endpointLoginConfFile != null) {
            FileInputStream inStream = null;
            try {
                Properties prop = new Properties();
                inStream = new FileInputStream(endpointLoginConfFile);
                prop.load(inStream);
                String propertyBaseKey = searchBaseKey(cleanUrl(endpoint), prop);
                authorizationHeader = generateAuthorizationHeader(propertyBaseKey, prop);
            } catch (FileNotFoundException e) {
                LOGGER.warn("Rest client configurer : Squash TA configuration file for TM credentials was not found at : {}, no credentials will be used ", endpointLoginConfFile.getPath());
                //in some cases, the details will come in usefull, but most of the time they won't
                LOGGER.debug("TM credential file not found details", e);
            } catch (IOException e) {
                LOGGER.warn("Rest client configurer : An IO error occured while using Squash TA configuration file for TM credentials, no credentials will be used.", e);
            } finally {
                closeStream(inStream);
            }
        } else {
            LOGGER.warn("Rest client configurer : No Squash TA configuration file for TM credentials was given in maven command line argument. No credentials will be used");
        }
        return authorizationHeader;
    }

    protected void closeStream(FileInputStream inStream) {
        if (inStream != null) {
            try {
                inStream.close();
            } catch (IOException e) {
                // Nothing else todo
                /* save a tweeny littlie thingy : that error might indicate some kind of rot in the system, let's log it !*/
                LOGGER.warn("Failed to close input stream. No data loss, but WTF?", e);
            }
        }
    }

    /**
     * If the String given in argument (which is supposed to represent an URL) ends with a character "/", then this "/"
     * is removed.
     *
     * @param urlToClean
     *            The url to clean
     * @return The cleaned url
     */
    protected String cleanUrl(String urlToClean) {
        return urlToClean.endsWith("/") ? urlToClean.substring(0, urlToClean.lastIndexOf("/")) : urlToClean;
    }

    /**
     * This method generate the String for the authorization header. It searches the login and the password in the list
     * of configuration property and used them to generate the string. If the property for the login and the password is
     * not found then the method return null
     *
     * @param propertyBaseKey
     *            The base key to buold the key to search the login and the password
     * @param propertieslist
     *            The list of properties
     * @return The authorization header
     */
    protected String generateAuthorizationHeader(String propertyBaseKey, Properties propertieslist) {
        String authorizationHeader = null;
        String key = propertyBaseKey + ".login";
        String login = propertieslist.getProperty(key);
        if (login == null) {
            LOGGER.warn("Rest client configurer : login key {} not found, no credentials will be used", key);
        } else if (login.equals("")) {
            LOGGER.warn("Rest client configurer : Login credentials is empty, no credentials will be used");
        } else {
            key = propertyBaseKey + ".password";
            String pwd = propertieslist.getProperty(key);
            if (pwd == null) {
                LOGGER.warn("Rest client configurer : password key {} not found, no credentials will be used", key);
            } else {
                String concat = login + ":" + pwd;
                authorizationHeader = "Basic " + Base64Utility.encode(concat.getBytes());
            }
        }
        return authorizationHeader;
    }

    /**
     * This method retrieve the base key of the property linked to the endpoint given in argument
     *
     * @param endPoint
     *            : The url of the server to contact
     * @param propertiesList
     *            : The list of properties
     * @return The base key to use in order to retrieve the properties linked to the endpoint given in argument
     */
    protected String searchBaseKey(String endPoint, Properties propertiesList) {
        String baseKey = null;
        DefaultPropertiesSource defaultPropertiesSource = new DefaultPropertiesSource(propertiesList);
        List<String> keyList = defaultPropertiesSource.findKeysMatchingPattern("^endpoint\\.(\\d)+$");
        if (!keyList.isEmpty()) {
            Map<String, String> urlKey = new HashMap<String, String>();
            for (String key : keyList) {
                String url = defaultPropertiesSource.getValue(key);
                url = cleanUrl(url);
                String previousValue = urlKey.put(url, key);
                if (previousValue != null) {
                    StringBuilder msg = new StringBuilder("Credentials for the URL {} was defined twice in the endPointConfFile.");
                    msg.append("Keys \"{}\" and \"{}\" are involved, the data associated to the last one have been kept");
                    String[] arg = {url, previousValue, key};
                    LOGGER.warn(msg.toString(), arg);
                }
            }
            baseKey = urlKey.get(endPoint);
        } else {
            baseKey = endPoint;
        }
        return baseKey;
    }

}

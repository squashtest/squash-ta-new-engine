/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.execution.client;

import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;
import org.squashtest.tm.api.testautomation.execution.dto.TestSuiteExecutionStatus;

/**
 * This interface defines operations for the execution API of Squash TM.
 * 
 * @author edegenetais
 * 
 */
public interface StatusUpdate {
	/**
	 * Method to update a test suite execution status.
	 * 
	 * @param testId id for the TestSuite (initiated by the caller when the TestSuite execution order was posted)
	 * @param status
	 *            new status of the test suite.
	 */
	void updateTestSuiteExecutionStatus(String testId, TestSuiteExecutionStatus status);

	/**
	 * Method to update a test execution status.
	 * @param externalId
	 *            external id given to the execution process when execution was
	 *            required.
	 * @param status new status of the test.
	 */
	void updateTestExecutionStatus(	String externalId, TestExecutionStatus status);
}

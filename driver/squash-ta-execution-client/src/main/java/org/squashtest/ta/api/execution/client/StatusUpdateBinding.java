/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.api.execution.client;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;
import org.squashtest.tm.api.testautomation.execution.dto.TestSuiteExecutionStatus;

/**
 * RESTful binding specification.
 * @see ClientFactory code for complete configuration code.
 * @author edegenetais
 *
 */
@Path("/automated-executions")
interface StatusUpdateBinding extends StatusUpdate{
	@Override
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{testId}/test-status")
	public void updateTestExecutionStatus(@PathParam("testId")String testId, TestExecutionStatus status);
	
	@Override
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{externalId}/test-suite-status")
	public void updateTestSuiteExecutionStatus(@PathParam("externalId")String externalId,TestSuiteExecutionStatus status);
}

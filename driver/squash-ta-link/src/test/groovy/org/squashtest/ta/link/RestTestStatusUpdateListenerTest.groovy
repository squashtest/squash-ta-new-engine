/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link

import org.squashtest.ta.api.execution.client.StatusUpdate
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus

import spock.lang.Specification;

class RestTestStatusUpdateListenerTest extends Specification{
	StatusUpdate mockService
	RestTestStatusUpdateListener testee
	TestStatusUpdate event
	TestResult mockResult
	Date startTime
	ReportUrl mockReportUrl

	final String MOCK_TEST="mockTest"
	final String MOCK_TEST_GROUP="mockTestGroup"
	
	def setup(){
		mockService=Mock()
		mockReportUrl=Mock()
		mockReportUrl.getUrl() >> "fakeReportUrl"
                ExecutionEnvironment mockExecutionEnvironment=Mock()
                mockExecutionEnvironment.getExecutionHostname() >> "testHost"
                mockExecutionEnvironment.getJobName() >> "testJob"
		testee=new RestTestStatusUpdateListener(mockService,mockExecutionEnvironment, "testId",mockReportUrl)
		event=Mock()
		
		startTime=new Date(2385)
		
		mockResult=Mock()
		mockResult.getName()>>MOCK_TEST
		mockResult.startTime()>>startTime
		mockResult.getTestId()>>"testExternalId"
		event.getPayload() >> mockResult
		event.getEcosystemName()>>MOCK_TEST_GROUP
	}

	def "should transmit external ID"(){
                given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus("testExternalId", _)
	}

        def "should transmit start time"(){
                given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStartTime().equals(startTime)
			})
	}

        def "should transmit end time"(){
                given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
                and:
                        Date endTime=new Date(3392)
                        mockResult.endTime()>>endTime
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getEndTime().equals(endTime)
			})
	}

        def "should transmit test name"(){
                given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getTestName().equals(MOCK_TEST)
			})
	}

        def "should transmit test group name"(){
                given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getTestGroupName().equals(MOCK_TEST_GROUP)
			})
	}
	
        def "should transmit SUCCESS event"(){
		given:
			Date endTime=new Date(3392)
		and:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
			mockResult.endTime()>>endTime
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatus().equals(ExecutionStatus.SUCCESS)
			})
	}

	def "should transmit FAILURE event"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
                        detail.getChildrens()>>[]
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>>GeneralStatus.FAIL
			mockResult.getFirstFailure()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatus().equals(ExecutionStatus.FAILURE)
			})
	}

        def "should transmit FAILURE event with message"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
                        detail.getChildrens()>>[]
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>>GeneralStatus.FAIL
			mockResult.getFirstFailure()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatusMessage().indexOf(expectdMessage)>=0
			})
	}
	
        def "should transmit ERROR event"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
                        detail.getChildrens()>>[]
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>>GeneralStatus.ERROR
			mockResult.getFirstFailure()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatus().equals(ExecutionStatus.ERROR)
			})
	}

        def "should transmit ERROR event with message"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
                        detail.getChildrens()>>[]
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>>GeneralStatus.ERROR
			mockResult.getFirstFailure()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatusMessage().indexOf(expectdMessage)>=0
			})
	}

	def "should transmit WARNING event"(){
		given:
			Date endTime=new Date(3325)
		and:
			ExecutionDetails detail=Mock()
			String expectdMessage="expected error message"
			detail.caughtException()>>new Exception(expectdMessage)
		and:
			mockResult.endTime()>>endTime
			mockResult.getStatus()>> GeneralStatus.WARNING
			mockResult.getFirstFailure()>>detail
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatus().equals(ExecutionStatus.WARNING)
			})
	}
	
	def "should transmit RUNNING event"(){
		given:
			mockResult.getStatus()>>GeneralStatus.RUNNING
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && it.getStatus().equals(ExecutionStatus.RUNNING)
			})
	}

        def "should not format message if hasHtmlFormattedFailureDetails not set (default)"(){
                given:
                    mockResult.getStatus()>>GeneralStatus.FAIL
                and:
                    ExecutionDetails detail=Mock()
                    String expectdMessage="expected error message"
                    detail.caughtException()>>new Exception(expectdMessage)
                    detail.getChildrens()>>[]
                    mockResult.getFirstFailure()>>detail
                when:
                    testee.handle(event)
                then:
                    1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && !(it.getStatusMessage() =~ /.*<[a-zA-Z]+>.*/) // want no HTML in default mode
                    })
               
        }


        def "should not format message if hasHtmlFormattedFailureDetails false"(){
                given:
                    mockResult.getStatus()>>GeneralStatus.FAIL
                and:
                    ExecutionDetails detail=Mock()
                    String expectdMessage="expected error message"
                    detail.caughtException()>>new Exception(expectdMessage)
                    detail.getChildrens()>>[]
                    mockResult.getFirstFailure()>>detail
                and:
                    ExecutionEnvironment mockExecutionEnvironment=Mock()
                    mockExecutionEnvironment.hasHtmlFormattedFailureDetails() >> false
                when:
                    testee.handle(event)
                then:
                    1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && !(it.getStatusMessage() =~ /.*<[a-zA-Z]+>.*/) // want no HTML if setting hasHtmlFormattedFailureDetails false
                    })
               
        }

        def "should format message if hasHtmlFormattedFailureDetails true"(){
                given:
                    mockResult.getStatus()>>GeneralStatus.FAIL
                and:
                    ExecutionDetails detail=Mock()
                    String expectdMessage="expected error message"
                    detail.caughtException()>>new Exception(expectdMessage)
                    detail.getChildrens()>>[]
                    mockResult.getFirstFailure()>>detail
                and:
                    ExecutionEnvironment mockExecutionEnvironment=Mock()
                    mockExecutionEnvironment.hasHtmlFormattedFailureDetails() >> true
                    def testee2=new RestTestStatusUpdateListener(mockService,mockExecutionEnvironment, "testId",mockReportUrl)
                when:
                    testee2.handle(event)
                then:
                    1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && (it.getStatusMessage() =~ /.*<[a-zA-Z]+>.*/) // DO want HTML if setting hasHtmlFormattedFailureDetails true
                    })
               
        }

        def "should escape pre-existing angle brackets if hasHtmlFormattedFailureDetails true"(){
                given:
                    mockResult.getStatus()>>GeneralStatus.FAIL
                and:
                    ExecutionDetails detail=Mock()
                    String expectdMessage="expected error with angle bracket here: > < "
                    detail.caughtException()>>new Exception(expectdMessage)
                    detail.getChildrens()>>[]
                    mockResult.getFirstFailure()>>detail
                and:
                    ExecutionEnvironment mockExecutionEnvironment=Mock()
                    mockExecutionEnvironment.hasHtmlFormattedFailureDetails() >> true
                    def testee2=new RestTestStatusUpdateListener(mockService,mockExecutionEnvironment, "testId",mockReportUrl)
                when:
                    testee2.handle(event)
                then:
                    1 * mockService.updateTestExecutionStatus(_,{
				it instanceof TestExecutionStatus && (it.getStatusMessage() =~ /.* &gt; &lt; .*/) // DO want HTML if setting hasHtmlFormattedFailureDetails true
                    })

        }
}

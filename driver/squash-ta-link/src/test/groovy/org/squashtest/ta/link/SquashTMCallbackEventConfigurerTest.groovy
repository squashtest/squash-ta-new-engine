/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link

import org.squashtest.ta.api.execution.client.StatusUpdate
import org.squashtest.ta.api.execution.client.ClientFactory
import org.squashtest.ta.framework.facade.Engine

import spock.lang.Specification;

class SquashTMCallbackEventConfigurerTest extends Specification{
	SquashTMCallbackEventConfigurer testee
	ClientFactory mockFactory
	Engine engineMock
	URL url
	File confFile
	
	def setup(){
		mockFactory=Mock()
		testee=new SquashTMCallbackEventConfigurer()
		testee.clientFactory=mockFactory
		testee.setActivateTestSuiteStatusUpdate(true)
		engineMock=Mock()
		
		url=new URL("http://app.example.com/squashTM/services/rest")
		confFile=new File(getClass().getResource("conf.properties").toURI())
	}
	
	def "should transmit given URL"(){
		given:
			testee.setEndpointURL(url)
			testee.setEndpointLoginConfFile(confFile)
		when:
			testee.apply(engineMock)
		then:
			1 * mockFactory.getClient(url.toExternalForm(),confFile)>>Mock(StatusUpdate)
	}
	
	def "should add test listener to engine"(){
		given:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				it instanceof RestTestStatusUpdateListener
			})
	}
	
	def "should transmit test listener host name"(){
		given:
			String hostName="jenkins.example.com"
			testee.setHostName(hostName)
		and:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				boolean result
				if(it instanceof RestTestStatusUpdateListener){
					RestTestStatusUpdateListener listener=it
					result=listener.getExecutionHostname()==hostName
				}else{
					result=false
				}
				result
			})
	}
	
	def "should transmit test listener job name"(){
		given:
			String jobName="testJob"
			testee.setJobName(jobName)
		and:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				boolean result
				if(it instanceof RestTestStatusUpdateListener){
					RestTestStatusUpdateListener listener=it
					result=listener.getJobName()==jobName
				}else{
					result=false
				}
				result
			})
	}
	
	def "should transmit test listener external id"(){
		given:
			String externalId="testId"
			testee.setExecutionExternalId(externalId)
		and:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				boolean result
				if(it instanceof RestTestStatusUpdateListener){
					RestTestStatusUpdateListener listener=it
					result=listener.getExternalId()==externalId
				}else{
					result=false
				}
				result
			})
	}
	
	def "should add suite listener to engine"(){
		given:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				it instanceof RestTestSuiteStatusUpdateListener
			})
	}
	
	def "should transmit suite listener host name"(){
		given:
			String hostName="jenkins.example.com"
			testee.setHostName(hostName)
		and:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				boolean result
				if(it instanceof RestTestSuiteStatusUpdateListener){
					RestTestSuiteStatusUpdateListener listener=it
					result=listener.getExecutionHostname()==hostName
				}else{
					result=false
				}
				result
			})
	}
	
	def "should transmit suite listener job name"(){
		given:
			String jobName="testJob"
			testee.setJobName(jobName)
		and:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				boolean result
				if(it instanceof RestTestSuiteStatusUpdateListener){
					RestTestSuiteStatusUpdateListener listener=it
					result=listener.getJobName()==jobName
				}else{
					result=false
				}
				result
			})
	}
	
	def "should transmit suite listener external id"(){
		given:
			String externalId="testId"
			testee.setExecutionExternalId(externalId)
		and:
			testee.setEndpointURL(url)
		when:
			testee.apply(engineMock)
		then:
			1 * engineMock.addEventListener({
				boolean result
				if(it instanceof RestTestSuiteStatusUpdateListener){
					RestTestSuiteStatusUpdateListener listener=it
					result=listener.getExternalId()==externalId
				}else{
					result=false
				}
				result
			})
	}

}

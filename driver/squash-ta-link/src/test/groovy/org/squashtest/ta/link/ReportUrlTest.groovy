/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link

import spock.lang.Specification
import spock.lang.Unroll;

class ReportUrlTest extends Specification {

	ReportUrl testee
	
	@Unroll
	def "Test produced Url with various input" (){
		
		given:
			testee = new ReportUrl(baseReportUrl, jobName, jobExecutionId, reportName);
		
		when:
			def result = testee.getUrl();
			
		then:
			result == expectedResult
			
		where:
		    baseReportUrl                    | jobName     | jobExecutionId | reportName     || expectedResult
			"http://myHost:9080/jenkins/job" | "demo-full" | "10"           | "html_report"  || "http://myHost:9080/jenkins/job/demo-full/10/html_report"
			"http://myHost:9080/jenkins/job" | "demo-full" | "10"           | "html_report/" || "http://myHost:9080/jenkins/job/demo-full/10/html_report/"
			""                               | "demo-full" | "10"           | "html_report"  || "/demo-full/10/html_report"
			null                             | "demo-full" | "10"           | "html_report"  || ""
			"http://myHost:9080/jenkins/job" | ""          | "10"           | "html_report"  || "http://myHost:9080/jenkins/job/10/html_report"
			"http://myHost:9080/jenkins/job" | null        | "10"           | "html_report"  || "http://myHost:9080/jenkins/job/10/html_report"
			"http://myHost:9080/jenkins/job" | "demo-full" | ""             | "html_report"  || "http://myHost:9080/jenkins/job/demo-full/html_report"
			"http://myHost:9080/jenkins/job" | "demo-full" | null           | "html_report"  || "http://myHost:9080/jenkins/job/demo-full/html_report"
			"http://myHost:9080/jenkins/job" | "demo-full" | "10"           | ""             || "http://myHost:9080/jenkins/job/demo-full/10/"
			"http://myHost:9080/jenkins/job" | "demo-full" | "10"           | null           || "http://myHost:9080/jenkins/job/demo-full/10/"
	}  
	
}

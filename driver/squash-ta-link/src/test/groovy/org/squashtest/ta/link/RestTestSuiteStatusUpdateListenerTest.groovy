/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link

import java.util.Date;

import org.squashtest.ta.api.execution.client.StatusUpdate
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.tm.api.testautomation.execution.dto.ExecutionStatus
import org.squashtest.tm.api.testautomation.execution.dto.TestSuiteExecutionStatus


import spock.lang.Specification;

class RestTestSuiteStatusUpdateListenerTest extends Specification{
	StatusUpdate mockService
	RestTestSuiteStatusUpdateListener testee
	TestSuiteStatusUpdate event
	SuiteResult mockResult
	Date startTime
	
	final SUITE_NAME="testSuite"
	
	def setup(){
	mockService=Mock()
                ExecutionEnvironment mockExecutionEnvironment=Mock()
                mockExecutionEnvironment.getExecutionHostname() >> "testHost"
                mockExecutionEnvironment.getJobName() >> "testJob"
		testee=new RestTestSuiteStatusUpdateListener(mockService,mockExecutionEnvironment, "testId")
		event=Mock()
		
		startTime=new Date(2385)
		
		mockResult=Mock()
		mockResult.startTime()>>startTime
		mockResult.getName() >> SUITE_NAME 
		
		event.getPayload() >> mockResult
	}

	def "should transmit external ID"(){
		given:
			mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus("testId", _)
	}

        def "should transmit start time"(){
		given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,{
				it instanceof TestSuiteExecutionStatus && startTime.equals(it.getStartTime())
			})
	}

        def "should transmit end time"(){
		given:
			Date endTime=new Date(9654)
                        mockResult.endTime()>>endTime
		and:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,{
				it instanceof TestSuiteExecutionStatus && endTime.equals(it.getEndTime())
			})
	}

        def "should transmit suite name"(){
		given:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,{
				it instanceof TestSuiteExecutionStatus && it.getSuiteName()==SUITE_NAME
			})
	}

	def "should transmit SUCCESS event"(){
		given:
			Date endTime=new Date(9654)
                        mockResult.endTime()>>endTime
		and:
                        mockResult.getStatus()>>GeneralStatus.SUCCESS
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,{
				it instanceof TestSuiteExecutionStatus && it.getStatus()==ExecutionStatus.SUCCESS
			})
	}
	
	def "should transmit RUNNING event"(){
		given:
			Date endTime=new Date(9654)
                        mockResult.endTime()>>endTime
		and:
                        mockResult.getStatus()>>GeneralStatus.RUNNING
		when:
			testee.handle(event)
		then:
			1 * mockService.updateTestSuiteExecutionStatus(_,{
				it instanceof TestSuiteExecutionStatus && it.getStatus()==ExecutionStatus.RUNNING
			})
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.execution.client.ClientFactory;
import org.squashtest.ta.api.execution.client.StatusUpdate;
import org.squashtest.ta.framework.facade.Configurer;
import org.squashtest.ta.framework.facade.Engine;

/**
 * {@link Configurer} implementation to configure the handling of status update events for callback to Squash TM through
 * the RESTful execution update services.
 * 
 * @author edegenetais
 * 
 */
public class SquashTMCallbackEventConfigurer implements Configurer {
	private static final Logger LOGGER = LoggerFactory.getLogger(SquashTMCallbackEventConfigurer.class);

	private static final String DEFAULT_URL_1 = "file://dev/null";
	private static final String DEFAULT_URL_2 = "file:/dev/null";

	/**
	 * Endpoint URL for the destination service. Set by default to file://dev/null
	 */
	private URL endpointURL = setDefaultUrl();

	/**
	 * External id attributed to the present automated test execution.
	 */
	private String executionExternalId;

	/**
	 * Name of the job used to serve execution requests.
	 */
	private String jobName;

	/**
	 * Name of the server (server host name would be a good candidate in most cases, but any unique (in your context)
	 * name will do).
	 */
	private String hostName;

	/**
	 * Configuration file which contains the mapping callback url / credentials for post
	 */
	private File endpointLoginConfFile;

	/**
	 * Boolean to activate / deactivate the <b>test</b> status update feature. Activated by default
	 */
	private boolean activateTestStatusUpdate = true;

	/**
	 * Boolean to activate / deactivate the <b>test suite</b> status update feature. Deactivated by default
	 */
	private boolean activateTestSuiteStatusUpdate = false;

	/**
	 * @parameter expression="${ta.tmCallBack.baseReportUrl}"
	 */
	private String reportBaseUrl;

	/**
	 * @parameter expression="${ta.tmCallBack.jobExecutionId}"
	 */
	private String jobExecutionId;

	/**
	 * @parameter expression="${ta.tmCallBack.reportName}"
	 */
	private String reportName;

        /**
         * @parameter expression="${tm.tmCallBack.htmlFormattedFailureDetailsMsg}"
         */
        private boolean htmlFormattedFailureDetailsMsg;
    
	/** For testability */
	private ClientFactory clientFactory = new ClientFactory();

	@Override
	public void apply(Engine engine) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Configuring events for client " + endpointURL + " on execution " + jobName + "#"
					+ executionExternalId + "@" + hostName);
		}
		if (endpointURL == null) {
			throw new IllegalArgumentException("The mandatory enpointURL parameter is null");
		}
		/*
		 * It seems there is a mistake in default url between the TA server job template and the pom template on fwk
		 * version 1.6.0 one use file:/dev/null and the other file://dev/null. So we consider the both URL as
		 * deactivation url
		 */
		if (endpointURL.toString().equals(DEFAULT_URL_1) || endpointURL.toString().equals(DEFAULT_URL_2)) {
			LOGGER.warn("The endpoint URL is set to its default value: \"file://dev/null\", so the call back is not activated");
		} else {
			StatusUpdate updateService = clientFactory.getClient(endpointURL.toExternalForm(), endpointLoginConfFile);
			startTestStatusUpdateListener(updateService, engine);
			startTestSuiteStatusUpdateListener(updateService, engine);
		}
	}

	private void startTestSuiteStatusUpdateListener(StatusUpdate updateService, Engine engine) {
		if (activateTestSuiteStatusUpdate) {
			LOGGER.debug("Test suite status update is activated");
                        ExecutionEnvironment env=new ExecutionEnvironment(hostName, jobName,htmlFormattedFailureDetailsMsg);
			RestTestSuiteStatusUpdateListener testSuiteStatusListener = new RestTestSuiteStatusUpdateListener(
					updateService, env, executionExternalId);
			engine.addEventListener(testSuiteStatusListener);
		} else {
			LOGGER.debug("Test suite status update is deactivated");
		}

	}

	private void startTestStatusUpdateListener(StatusUpdate updateService, Engine engine) {
		if (activateTestStatusUpdate) {
			LOGGER.debug("Test status update is activated");
			ReportUrl reportUrl = new ReportUrl(reportBaseUrl, jobName, jobExecutionId, reportName);
                        ExecutionEnvironment env=new ExecutionEnvironment(hostName, jobName,htmlFormattedFailureDetailsMsg);
			RestTestStatusUpdateListener testStatusListener = new RestTestStatusUpdateListener(updateService, env, 
                                executionExternalId, reportUrl);
			engine.addEventListener(testStatusListener);
		} else {
			LOGGER.debug("Test status update is deactivated");
		}

	}

	private URL setDefaultUrl() {
		URL defaultUrl;
		try {
			defaultUrl = new URL(DEFAULT_URL_1);
		} catch (MalformedURLException e) {
			defaultUrl = null;
		}
		return defaultUrl;
	}

	/**
	 * Getter to retrieve the URL of the server to notify for the status update
	 * 
	 * @return The endpoint URL
	 */
	public URL getEndpointURL() {
		return endpointURL;
	}

	/**
	 * Setter to define the URL of the server to notify for the status update
	 * 
	 * @param endpointURL
	 *            The new endpoint URL
	 */
	public void setEndpointURL(URL endpointURL) {
		this.endpointURL = endpointURL;
	}

	/**
	 * Getter for the execution external id
	 * 
	 * @return The id of the current automated test execution
	 */
	public String getExecutionExternalId() {
		return executionExternalId;
	}

	/**
	 * Setter for the execution external id
	 * 
	 * @param executionExternalId
	 *            The new id of the current automated test execution
	 */
	public void setExecutionExternalId(String executionExternalId) {
		this.executionExternalId = executionExternalId;
	}

	/**
	 * Getter to retrieve the job name
	 * 
	 * @return The job name
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * Setter for the job name
	 * 
	 * @param jobName
	 *            The new job name
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * Getter to retrieve the name of the execution host
	 * 
	 * @return The current host name
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * Setter for the host name
	 * 
	 * @param hostName 
         *          The execution server host name
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	/**
	 * Getter for the configuration file located on the automated server. This file contains the login / password of
	 * user authorized to post status update to the associated endpointURL
	 * 
	 * @return The authorization configuration file
	 */
	public File getEndpointLoginConfFile() {
		return endpointLoginConfFile;
	}

	/**
	 * Setter for the authorization configuration file
	 * 
	 * @param endpointLoginConfFile
	 *            The new confguration file
	 */
	public void setEndpointLoginConfFile(File endpointLoginConfFile) {
		this.endpointLoginConfFile = endpointLoginConfFile;
	}

	/**
	 * Getter for the state of the <b>test</b> status update feature
	 * 
	 * @return <code>true</code> if the feature is activated
	 */
	public boolean isActivateTestStatusUpdate() {
		return activateTestStatusUpdate;
	}

	/**
	 * Setter to activate/ deactivate the <b>test</b> status update feature. Give <code>true</code> as argument to
	 * activate the feature, give <code>false</code> as argument to deactivate the feature.
	 * 
	 * @param activateTestStatusUpdate
	 *            The new state
	 */
	public void setActivateTestStatusUpdate(boolean activateTestStatusUpdate) {
		this.activateTestStatusUpdate = activateTestStatusUpdate;
	}

	/**
	 * Getter for the state of the <b>test suite</b> status update feature
	 * 
	 * @return <code>true</code> if the feature is activated
	 */
	public boolean isActivateTestSuiteStatusUpdate() {
		return activateTestSuiteStatusUpdate;
	}

	/**
	 * Setter to activate/ deactivate the <b>test suite</b> status update feature. Give <code>true</code> as argument to
	 * activate the feature, give <code>false</code> as argument to deactivate the feature.
	 * 
	 * @param activateTestSuiteStatusUpdate
	 *            The new state
	 */
	public void setActivateTestSuiteStatusUpdate(boolean activateTestSuiteStatusUpdate) {
		this.activateTestSuiteStatusUpdate = activateTestSuiteStatusUpdate;
	}

	/**
	 * Setter for the reprotBaseUrl attribute
	 * 
	 * @param reportBaseUrl
	 *            Url of the server which host the report file
	 */
	public void setBaseReportUrl(String reportBaseUrl) {
		this.reportBaseUrl = reportBaseUrl;
	}

	/**
	 * Setter for the jobExecutionId attribute
	 * 
	 * @param jobExecutionId
	 *            The id of the current job execution
	 */
	public void setJobExecutionId(String jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	/**
	 * Setter for the reportName attribute
	 * 
	 * @param reportName
	 *            The name of the report
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

        /**
         * Getter for the state of the HTML formatted failure detail message feature.
         * NB: this is a workaround feature for overly verbose plugins (details message summarizes the reason of the failure and should
         * be kept as close as possible to a one-liner; the complete report should be attached as a file resource 
         * to the failure resource and context, which gets attached to the the detailed report produced by Squash TA fater test completion.
         * @return <code>true</code> if failure detail messages should be HTML formatted. If not explicitely set, defaults to <code>false</code>.
         */
        public boolean hasHtmlFormattedFailureDetailsMsg() {
                return htmlFormattedFailureDetailsMsg;
        }

        /**
         * Setter for the htmlFormattedFailureDetailsMsg attribute
         *
         * @param htmlFormattedFailureDetailsMsg 
         *          Weather the failure detail message
         * sent back to Squash TM should be html formatted. Defaults to false.
         */
        public void setHtmlFormattedFailureDetailsMsg(boolean htmlFormattedFailureDetailsMsg) {
            this.htmlFormattedFailureDetailsMsg = htmlFormattedFailureDetailsMsg;
        }
}

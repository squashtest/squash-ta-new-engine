/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link;

/**
 * This class represents the Url of the report provided to the endpoint.
 * 
 * @author bfranchet
 * 
 */
public class ReportUrl {

	/** Url of the server which host the report. By default it should be JENKINS_URL/job */
	private String reporBaseUrl;

	/** The job name */
	private String jobName;

	/** The id of the current job execution */
	private String jobExecutionId;

	/** The name of the report */
	private String reportName;

	/**
	 * Full constructor
	 * 
	 * @param baseReportUrl
	 *            The url of the server which host the report
	 * @param jobName
	 *            The job name
	 * @param jobExecutionId
	 *            The id of the current job execution
	 * @param reportName
	 *            The name of the report
	 */
	public ReportUrl(String baseReportUrl, String jobName, String jobExecutionId, String reportName) {
		this.reporBaseUrl = baseReportUrl;
		this.jobExecutionId = jobExecutionId;
		this.reportName = reportName;
		this.jobName = jobName;
	}

	/**
	 * This method generate the url of the report. If the reportBaseUrl is null then an empty String is return. Else the
	 * method concatenates the reprotBaseUrl, the jobName, the jobExecutionId and the reportName and adding a "/"
	 * between each element.
	 * 
	 * @return The url of the report
	 */
	public String getUrl() {
		if (reporBaseUrl != null) {
			StringBuilder url = new StringBuilder(reporBaseUrl).append("/");
			if (jobName != null && !jobName.isEmpty()) {
				url.append(jobName).append("/");
			}
			if (jobExecutionId != null && !jobExecutionId.isEmpty()) {
				url.append(jobExecutionId).append("/");
			}
			if (reportName != null && !reportName.isEmpty()) {
				url.append(reportName);
			}
			return url.toString();
		} else {
			return "";
		}

	}

}

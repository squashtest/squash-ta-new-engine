/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import org.squashtest.ta.link.SquashTMCallbackEventConfigurer;

/**
 * This class only extends {@link SquashTMCallbackEventConfigurer} in order to simplify TA projects pom. With a class
 * define in the same package as the MOJO it's possible to use directly the class name as configuration tag for the
 * plugin and not use the awful "implementation" attribute which required the full qualified class name. (@see maven
 * plugin configuration documentation)
 * 
 * @author bfranchet
 * 
 */
public class TmCallBack extends SquashTMCallbackEventConfigurer {
	
}

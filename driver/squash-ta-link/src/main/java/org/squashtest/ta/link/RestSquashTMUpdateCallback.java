/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link;

import org.squashtest.ta.api.execution.client.StatusUpdate;


public abstract class RestSquashTMUpdateCallback {

	private StatusUpdate updateClient;
	private String executionHostname;
	private String jobName;
	private String externalId;

	/**
	 * @param updateClient
	 * @param env
	 * @param externalId
	 */
	public RestSquashTMUpdateCallback(StatusUpdate updateClient,
			ExecutionEnvironment env, String externalId) {
		this.updateClient = updateClient;
		this.executionHostname = env.getExecutionHostname();
		this.jobName = env.getJobName();
		this.externalId = externalId;
	}

	public StatusUpdate getUpdateClient() {
		return updateClient;
	}

	public String getExecutionHostname() {
		return executionHostname;
	}

	public String getJobName() {
		return jobName;
	}

	public String getExternalId() {
		return externalId;
	}

}
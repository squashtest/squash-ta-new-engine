/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.link;

import javax.ws.rs.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.execution.client.StatusUpdate;
import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.tm.api.testautomation.execution.dto.TestExecutionStatus;

/**
 * Event listener to post {@link TestStatusUpdate} events back to a Squash TM API- compliant RESTful service.
 * @author edegenetais
 *
 */
public class RestTestStatusUpdateListener extends RestSquashTMUpdateCallback implements StatusUpdateListener{
        private static final Logger LOGGER = LoggerFactory.getLogger(RestTestStatusUpdateListener.class);	
	
        /** The url of the report */
	private ReportUrl reportUrl;
        /** Activation flag for the failure detail message HTML formatting feature */
        private boolean htmlFormattedFailureDetailsMsg;
	
	public RestTestStatusUpdateListener(StatusUpdate updateClient,
			ExecutionEnvironment env, String externalId, ReportUrl reportUrl) {
		super(updateClient, env, externalId);
		this.reportUrl=reportUrl;
                this.htmlFormattedFailureDetailsMsg=env.hasHtmlFormattedFailureDetails();
	}

	@Override
	public void handle(TestSuiteStatusUpdate event) {/* This listener only manages test level updates. */}

	@Override
	public void handle(EcosystemStatusUpdate event) {/* This listener only manages test level updates. */}

	@Override
	public void handle(TestStatusUpdate event) {
            TestExecutionStatus status=null;
            try{
		TestResult testResult = event.getPayload();
                
		String testId = testResult.getTestId();
		if(testId!=null){
			 status=new TestExecutionStatus();
			status.setTestName(testResult.getName());
			status.setTestGroupName(event.getEcosystemName());
			setStatusData(status, testResult);
			status.setStartTime(testResult.startTime());
			status.setEndTime(testResult.endTime());
			status.setResultUrl(reportUrl.getUrl());
			getUpdateClient().updateTestExecutionStatus(testId, status);
		}else{
                    LOGGER.debug("Ignoring test "+testResult.getName()+" (no id)");
                }
            }catch(BadRequestException bre){
                TestResult result=event.getPayload();
                String testID=result.getTestId();           
                throw new BadRequestException("Callback for test "+status.getTestGroupName()+"/"+status.getTestName()+" with test id "+testID+" failed with message: "+bre.getMessage(), bre.getResponse(), bre);
            }
	}
    
	private void setStatusData(TestExecutionStatus status, TestResult testResult) {
		GeneralStatus taStatus = testResult.getStatus();
		TaTmStatusMapping statusMapping=TaTmStatusMapping.getMapping(taStatus);
		status.setStatus(statusMapping.getTmStatus());
		if(taStatus.isFailOrError()){
			ExecutionDetails firstFailed = testResult.getFirstFailure();		
			while(!firstFailed.getChildrens().isEmpty()){
				firstFailed = firstFailed.getErrorOrFailedChildrens().get(0);
			}
			if(firstFailed!=null && firstFailed.caughtException()!=null){
                            
                            String toString=createFailureDetailMessage(firstFailed);

                            status.setStatusMessage(toString);
			}
		}
		if(taStatus.isNotFound()){
			status.setStatusMessage("Script not found");
		}
		if(taStatus.isNotRun()){
			status.setStatusMessage("Script not runned");
		}
	}

        /**
         * Create failure detail message in a separate method as [Feat 0006393] has made
         * message building more complex.
         * @param firstFailed execution detail object for the first failed test instruction.
         * @return 
         */
        private String createFailureDetailMessage(ExecutionDetails firstFailed) {
            final StringBuilder msgBuilder=new StringBuilder("First caught exception: ");
            msgBuilder.append(firstFailed.caughtException().getMessage());
            
            if(htmlFormattedFailureDetailsMsg){
                performEscape(msgBuilder, "<", "&lt;");
                performEscape(msgBuilder, ">", "&gt;");
                msgBuilder.insert(0,"<pre>");
                msgBuilder.append("</pre>");
            }
            
            return msgBuilder.toString();
        }

        private void performEscape(final StringBuilder contentBuilder, String target, String escapedForm) {
            int lastPosition=contentBuilder.indexOf(target);
            while(lastPosition>=0){
                contentBuilder.replace(lastPosition, lastPosition+1, escapedForm);
                lastPosition=contentBuilder.indexOf(target);
            }
        }
	
}

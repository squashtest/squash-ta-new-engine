/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.regex.Pattern;

import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;

/**
 * File filter to find files by combining a regex with an optional base directory.
 * @author edegenetais
 *
 */
public class RegexFileFilter implements FilenameFilter{
	
	private static final Pattern ABSOLUTE_REGEX=Pattern.compile("(/|\\[a-zA-Z\\]:\\\\|[a-zA-Z]:\\\\).*");
	
	private Pattern pattern;
	private boolean relativeRegex;
	private File baseDirectory;
	
	public RegexFileFilter(File baseDir,String regex) {
		baseDirectory=baseDir;
		pattern=Pattern.compile(regex);
		relativeRegex=!ABSOLUTE_REGEX.matcher(regex).matches();
	}
	
	/**
	 * Checks if the file <code>{@literal <dir>/name}</code> is coherent with the base
	 * directory + name regex combination. Due to limits in the relative path
	 * computation, you may have false negative with regular expressions that do
	 * not explicitely define an absolute path (either they define a relative
	 * path, or the degrees of liberty could make them match relative paths).
	 */
	@Override
	public boolean accept(File dir, String name) {
		boolean accept;
		if(relativeRegex){
			try {
				String relativePath=FILE_TREE.getRelativePath(baseDirectory, new File(dir,name));
				if(relativePath==null){
					accept=false;//not in the ascribed base directory ==> condition not fulfilled
				}else{//otherwise, let's see if the path relative to the base directory matches the regex.
					accept=pattern.matcher(relativePath).matches();
				}
			} catch (IOException e) {
				throw new TestFilteringException("Relative path checking exception.", e);
			}
		}else{
			//if the regex is without ambiguity an absolute path, we'll treat it as such and ignore the base directory
			String absolutePath=new File(dir,name).getAbsolutePath();
			accept=pattern.matcher(absolutePath).matches();
		}
		return accept;
	}

}

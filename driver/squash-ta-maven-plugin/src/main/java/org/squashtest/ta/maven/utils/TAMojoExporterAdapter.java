/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.utils;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.maven.AbstractSquashTaMojo;

/**
 * This helper class provides utility methods to tweak a {@link AbstractSquashTaMojo} runner.
 * 
 * It can register new {@link ResultExporter}s without discarding previous ones. 
 * 
 * @author fgautier
 */
public class TAMojoExporterAdapter {
    
    /**
     * Used for class related messages.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TAMojoExporterAdapter.class);
    
    /**
     * The {@link AbstractSquashTaMojo} runner about to be adapted.
     */
    private final AbstractSquashTaMojo adaptedRunner; 

    /**
     * Standard constructor. 
     * 
     * @param adaptedRunner the {@link AbstractSquashTaMojo} runner about to be adapted.
     * Not checked, but to work properly this parmater should be non null. 
     */
    public TAMojoExporterAdapter(AbstractSquashTaMojo adaptedRunner) {
        this.adaptedRunner = adaptedRunner;
    }
   
    /**
     * Register a new {@link ResultExporter}s to the adapted runner without discarding 
     * previous ones. Nonetheless it fails to do so and warns about it if another 
     * similar exporter is already regitered. Similar is ot be understood as "already registered exporter" 
     * is "instance of" "to be registered exporter".
     * 
     * @param exporter the exporter to be registered. 
     * @return the adapter runner.
     * 
     */
    public AbstractSquashTaMojo registerResultExporter(ResultExporter exporter) {
        final Class<? extends ResultExporter> exporterClazz = exporter.getClass();
        
        List<ResultExporter> newExporters = new ArrayList<>(); 
        ResultExporter[] oldExporters = adaptedRunner.getExporters();
        
        boolean oldExportersContainsReporter = false;
        if(oldExporters != null) {
            for(ResultExporter oldExporter : oldExporters) {
                newExporters.add(oldExporter);
                oldExportersContainsReporter = oldExportersContainsReporter 
                                                    || ( exporterClazz.isInstance(oldExporter) );
            }
        }
        
        if(!oldExportersContainsReporter) {
            newExporters.add(exporter);
        } else {
            LOGGER.warn("Failed to register exporter '{}' as another '{}' exporter is already registered. Reports may be missing.", exporter, exporterClazz.getName());
        }
        
        ResultExporter[] exportersTable = newExporters.toArray(new ResultExporter[]{});
        
        adaptedRunner.setExporters(exportersTable);
        
        return adaptedRunner;
    }
  
    /**
     * Register a collection of exporter. Registrations are treated sequentially.
     * Each registration is treated as in the "unit" case. 
     * 
     * @param exporters the exporters to be registered. 
     * @return the adapter runner. 
     */
    public AbstractSquashTaMojo registerResultExporters(ResultExporter ... exporters) {
        
        for (ResultExporter resultExporter : exporters) {
            registerResultExporter(resultExporter);
        }
        
        return adaptedRunner;
    }
    
}

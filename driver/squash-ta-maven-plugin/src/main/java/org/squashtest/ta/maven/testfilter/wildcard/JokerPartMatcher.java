/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter.wildcard;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Matches path element names with one or more * inside.
 * 
 * @author edegenetais
 * 
 */
public class JokerPartMatcher extends AbstractMatcherBase {

	private Pattern matchExpression;

	/**
	 * Full initialization constructor.
	 * @param jokerName the name, must be an a-zA-Z0-9 string with * inside.
	 */
	public JokerPartMatcher(String jokerName) {
			parseComplexJokerExpression(jokerName);
	}

	/**
	 * Change the "joker expression" into a regex pattern.
	 * @param jokerName
	 */
	protected void parseComplexJokerExpression(String jokerName) {
		String jokerNameEscaped=jokerName.replaceAll("\\.", "\\\\.");
		String jokerRegex=jokerNameEscaped.replaceAll("\\*", ".*");
		matchExpression=Pattern.compile(jokerRegex);
	}

	@Override
	public MatchState matches(List<String> path) {
		MatchState state;
		if (!path.isEmpty()) {
			List<String> leftOver = getSubPath(path, 1);
			String targetElement = path.get(0);
			Matcher matcher = matchExpression.matcher(targetElement);
			if(leftOver.isEmpty()){
				state=new MatchState(matcher.matches() ? true: false, emptyList());
			}else{
				state = new MatchState(matcher.matches() ? null : false, leftOver);
			}
		} else {
			state = new MatchState(false, emptyList());
		}
		return state;
	}

	@Override
	public String toPattern() {
		return matchExpression.pattern();
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.metadata.MetadataSyntaxChecker;

/**
 * Squash TA goal in order to validate the syntax of all metadata located in the test root directory of the test project.
 *  
 * @author qtran
 * 
 * @goal check-metadata
 * @phase process-sources
 */
public class SquashTACheckMetadataMojo extends AbstractBaseSquashTaMojo{
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashTACheckMetadataMojo.class);
    
    private static final String TEST_DIR = "tests";
      
    /**
     * The base directory where the test project is defined.
     * 
     * @parameter property="ta.src" default-value="src/squashTA"
     */
    private File mainDirectory;
        
    /**
     * The base directory where tests are found.
     * 
     * @parameter property="ta.src.tests"
     */
    private File testsDirectory;
    
    /**
     * 
     * @parameter property="tf.metadata.check"
     */
    private String metadataCheckParam;
    
    /**
     * 
     * @parameter property="tf.metadata.check.keys"
     */
    private String metadataCheckKeyListParam;
            
    @Override
    protected void execution() throws MojoExecutionException, MojoFailureException {
        if (testsDirectory == null) {
            testsDirectory = new File(mainDirectory, TEST_DIR);
        }
        
        //********* execution ********
        MetadataSyntaxChecker checker = new MetadataSyntaxChecker(testsDirectory);
        logPhase("checking metadata...");
        if (null == metadataCheckParam){
            //default metadata syntax validation
            checker.checkMetadataSyntaxByDefault(true);
        } else {
            LOGGER.debug("User input for tf.metadata.check: \'"+metadataCheckParam+"\'.");
            String userInputForMetadataCheckParam = metadataCheckParam.trim();
                        
            if (isWrappedByBrackets(userInputForMetadataCheckParam)){
                treatUserInputListForMetadataCheckParam(userInputForMetadataCheckParam, checker);
            } else {
                raiseNonCoveredByBracketsArgExcptForMetadataCheckParam();
            }
        }
        logPhase("Metadata checking successfully completed");
    }

    private boolean isWrappedByBrackets(String userInputForMetadataCheckParam) {
        String regex = "\\[.*\\]";
        Pattern pat = Pattern.compile(regex);
        return pat.matcher(userInputForMetadataCheckParam).matches();
    }

    private void treatUserInputListForMetadataCheckParam(String userInputForMetadataCheckParam, MetadataSyntaxChecker checker) {
        String removedBracketsInput = removeBracketsOfInputString(userInputForMetadataCheckParam);
        String trimmedRemovedBracketsInput = removedBracketsInput.trim();
        if ("".equals(trimmedRemovedBracketsInput)){
            invokeEmptyUserInputListForMetadataCheckParamException();
        } else {
            treatNotEmptyUserInputListForMetadataCheckParam(removedBracketsInput, checker);
        }
    }

    private String removeBracketsOfInputString(String userInputForMetadataCheckParam) {
        return userInputForMetadataCheckParam.substring(1, userInputForMetadataCheckParam.length()-1);
    }
    
    private void invokeEmptyUserInputListForMetadataCheckParamException() {
        LOGGER.error("\'"+metadataCheckParam+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input list MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void treatNotEmptyUserInputListForMetadataCheckParam(String removedBracketsInput, MetadataSyntaxChecker checker) {
        String[] inputList = removedBracketsInput.split(",");
        for (String input :  inputList){
            String trimmedInput =  input.trim();
            if ("".equals(trimmedInput)){
                invokeEmptyUserInputForMetadataCheckParamException(trimmedInput);
            } else {
                treatUserInputForMetadataCheckParam(trimmedInput, checker);
            }
        }
    }
    
    private void invokeEmptyUserInputForMetadataCheckParamException(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }

    private void treatUserInputForMetadataCheckParam(String trimmedInput, MetadataSyntaxChecker checker) {
        switch (trimmedInput) {
            case "valueUnicity":
                logPhase("checking metadata with value unicity...");
                checker.checkMetadataSyntaxWithUnicityChecking(metadataCheckKeyListParam);
                break;
            case "keyUnicity":
                logPhase("checking metadata with key unicity...");
                break;
            default:
                raiseIllegalArgExcptForMetadataCheckParam(trimmedInput);                
        }
    }
    
    private void raiseIllegalArgExcptForMetadataCheckParam(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input is NOT defined.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }

    private void raiseNonCoveredByBracketsArgExcptForMetadataCheckParam() {
        LOGGER.error("\'"+metadataCheckParam+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input(s) must be wrapped in brackets, ex: \"[valueUnicity]\" or \"[valueUnicity, keyUnicity]\".");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter to explicitly match files by name.
 * @author edegenetais
 *
 */
class IncludeTestFilter implements FilenameFilter {
	
	private static Logger logger = LoggerFactory.getLogger(IncludeTestFilter.class);

	private File baseTestDirectory ;
	private List<File> includedFiles = new LinkedList<File>();
	
	/**
	 * Creates an empty filter that never matches... 
	 * @param baseDirectory the base directory to use.
	 */
	IncludeTestFilter(File baseDirectory)
	{
		baseTestDirectory=baseDirectory;
	}
	
	/**
	 * Create a filter to match a list of file names.
	 * @param baseDirectory
	 * @param fileNames
	 */
	IncludeTestFilter(File baseDirectory, List<String> fileNames)
	{
		this(baseDirectory);
		setIncludedFiles(fileNames);
	}
	
	void setIncludedFiles(List<String> fileNames)
	{		
		for (String name : fileNames)
		{
			File file = new File(name);
			
			if (! file.isAbsolute())
			{
				file = new File(baseTestDirectory, name);
			}
			
			if (! file.exists())
			{
				throw logAndThrow("cannot build test suite : file "+file.getPath()+" does not exist",null);
			}
			
			includedFiles.add(file);			
		}
	}
	
	
	@Override
	public boolean accept(File arg0, String arg1)
	{
		
		File testedFile=null;
		
		try
		{
			testedFile = new File(arg0, arg1);
			String testedName = testedFile.getCanonicalPath();

			
			boolean accepted = false;
			
			for (File acceptedFile : includedFiles)
			{
				String acceptedName = acceptedFile.getCanonicalPath();
				
				if (FilenameUtils.equals(testedName, acceptedName))
				{
					accepted = true;
					break;
				}
			}
			
			return accepted;
			
		}
		catch (IOException ex)
		{
			throw logAndThrow("Squash TA : an error occured while building the test suite, could not process file '"+ testedFile.getPath()+"'", ex);
		}
		
	}

	
	private RuntimeException logAndThrow(String message, Exception ex){
		if (logger.isErrorEnabled()){
			logger.error(message, ex);
		}
		throw new TestFilteringException(message, ex);
	}
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import org.squashtest.ta.framework.facade.Configurer;

/**
 * Exception class to encapsulate (thus better report) any exception thrown when applying an engine configurer within the Mojo.
 * @author edegenetais
 *
 */
@SuppressWarnings("serial")
public class ConfigurationException extends RuntimeException{
	private final Class<?> configurerClass;
	private String firstLevelFrameDescription="the configuration method";
	public ConfigurationException(Exception cause, Configurer configurer) {
		super(cause);
		if(configurer==null){
			configurerClass=Void.class;
		}else{
			configurerClass=configurer.getClass();
			StackTraceElement lastFrame=null;
			for(StackTraceElement frame:cause.getStackTrace()){
				if (lastFrame != null
						&& configurerClass.getName().equals(
								frame.getClassName())
						&& "apply".equals(frame.getMethodName())) {
					firstLevelFrameDescription=lastFrame.toString();
				}
				lastFrame=frame;
			}
		}
	}
	
	@Override
		public String getMessage() {
			return "In configurer "+configurerClass.getName()+", "+firstLevelFrameDescription+" through the following: "+super.getMessage();
		}	
}

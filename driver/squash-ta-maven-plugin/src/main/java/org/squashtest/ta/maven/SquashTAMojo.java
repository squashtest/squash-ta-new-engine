/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.factories.TestSuiteDescription;
import org.squashtest.ta.commons.factories.dsl.DSLTestSuiteFactory;
import org.squashtest.ta.commons.factories.macros.TestSuiteMacro;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.init.DefaultTestProjectWorkspaceBrowser;
import org.squashtest.ta.commons.init.TestSuiteDescriptionFactory;
import org.squashtest.ta.commons.json.listing.JsonTestTree;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TFTestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.maven.param.json.JsonTestSuite;
import org.squashtest.ta.maven.testfilter.FileFilterFactory;
import org.squashtest.ta.plugin.commons.resources.ThirdPartyJavaCodeBundle;

/**
 * That mojo is a maven way to start Squash TA. It will perform the following
 * action in that order :
 * 
 * <ol>
 * <li>Build a {@link TestSuite} using the Squash TA project this build is bound
 * to,</li>
 * <li>Initialize the test context,</li>
 * <li>Initialize the engine,</li>
 * <li>Perform the actual tests,</li>
 * <li>Execute the exporters.</li>
 * </ol>
 * 
 * @author bsiri
 * 
 * @goal run
 * @requiresDependencyResolution test
 * @phase integration-test
 * 
 */
public class SquashTAMojo extends AbstractSquashTaMojo {

	private static final String BUILT_IN_MACROS = "builtin/macros/";
        private static final String TEST_CODE_BUNDLE_BUILTIN_NAME = "testCode";
        private static final String MAIN_CODE_BUNDLE_BUILTIN_NAME = "mainCode";

	/* ****************** configuration section *************************** */

	/**
	 * 
	 * Tells the mojo where to find the project sources.
	 * 
	 * @parameter property="ta.src"
	 * 
	 */
	private File mainDirectory = new File("src/squashTA");

	/**
	 * The base directory where lie the definition for the additional
	 * repositories used by Squash TA. Defaults to {@link #mainDirectory}
	 * /repositories.
	 * 
	 * @parameter property="ta.src.repositories"
	 * 
	 */
	private File repositoriesDirectory;

	/**
	 * The base directory where lie the definition for the target that Squash TA
	 * will hit. Defaults to {@link #mainDirectory}/targets.
	 * 
	 * @parameter property="ta.src.targets"
	 * 
	 */
	private File targetsDirectory;

	/**
	 * The base directory where lie the different test resources that the engine
	 * may have to load. Defaults to {@link #mainDirectory}/resources.
	 * 
	 * @parameter property="ta.src.resources"
	 * 
	 */
	private File resourcesDirectory;

	/**
	 * The base directory where lie the different test scripts. Defaults to
	 * {@link #mainDirectory}/tests.
	 * 
	 * @parameter property="ta.src.tests"
	 */
	private File testsDirectory;

	/**
	 * The base directory where lie the user-defined shortcuts definitions.
	 * Defaults to {@link #mainDirectory} /shortcuts.
	 * 
	 * @parameter property="ta.src.shortcuts"
	 * 
	 */
	private File shortcutsDirectory;

	/**
	 * @parameter property="project.build.directory"
	 */
	private File buildDirectory;

	/**
	 * @parameter default-value="${plugin.artifacts}"
	 */
	protected List<Artifact> pluginArtifacts;

	/**
	 * If set, defines the list of specific tests that should be executed. The
	 * format is &lt;testname1&gt;, [ &lt;testname2&gt; etc ], Where a test name
	 * may be absolute, or relative to the test directory. If not set, all the
	 * tests in the test directory will be included.
	 * 
	 * @parameter property="ta.test.suite"
	 * 
	 */
	private String testSuiteComposition;

	/**
	 * @parameter property="ta.delete.json.file"
	 */
	private boolean deleteJsonFile = false;
        
        /**
         * @parameter property=project.compileClasspathElements
         */
        private List<String> compileCP;

        /**
         * @parameter property=project.testClasspathElements
         */
        private List<String> testCP;

        /**
         * @parameter property=project.build.outputDirectory
         */
        private File mainClasses;

        /**
         * @parameter property=project.build.testOutputDirectory
         */
        private File testClasses;

	/*
	 * This mehod is a dirty fix for maven 3.3.x bug : slf4j-api is missing in
	 * the classPath since maven is effectively using slf4j-api as Logger.
	 * REMOVE THIS METHOD AS SOON AS MAVEN FIXED HIS BUG !
	 */
	private void setMissingMojoClassToThread() throws MojoExecutionException {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Log logger = getLogger();
			// getting List<URL> to compare
			List<URL> manifestURLs = Collections.list(contextClassLoader.getResources("META-INF/MANIFEST.MF"));
			List<URL> mojoArtifactUrls = new ArrayList<>();
			for (Artifact artifact : pluginArtifacts) {
				URL artifactUrl = artifact.getFile().toURI().toURL();

				if (logger.isDebugEnabled()) {
					logger.debug("Enumerated artifact URL: " + artifactUrl.toExternalForm());
				}
				mojoArtifactUrls.add(artifactUrl);
			}

			// Process ClassLoader List for comparing to mojo Artifacts
			List<String> processedContextClassesURLs = new ArrayList<>();
			for (URL url : manifestURLs) {
				String classpathUrl = url.toString().replace("jar:", "").replace("!/META-INF/MANIFEST.MF", "");
				if (logger.isDebugEnabled()) {
					logger.debug("Enumerated ContextClassLoader URL: " + classpathUrl);
				}
				processedContextClassesURLs.add(classpathUrl);
			}
			
			// Building list of missing artifacts filtered (excluding) maven proper jars.
			List<URL> missingUrlList = new ArrayList<>();
			for (URL url : mojoArtifactUrls) {
				if (!processedContextClassesURLs.contains(url.toString()) &&
					!url.toString().contains("org/apache/maven/maven-plugin-") &&
					!url.toString().contains("org/apache/maven/maven-artifact") &&
					!url.toString().contains("org/apache/maven/maven-model") &&
					!url.toString().contains("plexus-classworlds/") &&
					!url.toString().contains("org/eclipse/sisu/org.eclipse.sisu.plexus")) {
					if (logger.isWarnEnabled()) {
						logger.warn("Per maven bug #MNG5787, re-adding filtered artifact URL: " + url.toExternalForm());
					}
					missingUrlList.add(url);
				}
			}

			// Add missing artifact to the ClassRealm
			URL[] missingUrlArray = new URL[missingUrlList.size()];
			missingUrlList.toArray(missingUrlArray);
			try {
				ClassLoader classcl = getClass().getClassLoader();
				if (logger.isDebugEnabled()) {
					logger.debug("Mojo loaded by: " + classcl.toString());
				}
				Class<?> realmClass = Class.forName("org.codehaus.plexus.classworlds.realm.ClassRealm");
				if (realmClass.isInstance(classcl)) {
					Method m = realmClass.getMethod("addURL", new Class[] { URL.class });
					for (URL missingUrl : missingUrlArray) {
						m.invoke(classcl, new Object[] { missingUrl });
					}
					logger.warn("Applied Maven 3.3.x fix");
				} else {
					// If he class name is wrong, try to fix the class thread context.
					logger.warn("Maven 3.3.x fix does not apply, trying to fix thread context classloader.");
					Thread.currentThread()
							.setContextClassLoader(new URLClassLoader(missingUrlArray, getClass().getClassLoader()));
				}
			} catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException pbe) {
				logger.warn("Maven 3.3.x fix does not apply, trying to fix thread context classloader.", pbe);
				Thread.currentThread()
						.setContextClassLoader(new URLClassLoader(missingUrlArray, getClass().getClassLoader()));

			}

		} catch (IOException | IllegalAccessException | IllegalArgumentException | SecurityException e) {
			throw new MojoExecutionException("Couldn't complete the Thread classloader with missing jars.", e);
		}
	}

	private void init() throws MojoExecutionException {
		
		// As mentionned before this is a quick and dirty fix for a maven 3.3.x
		// bug related to slf4j-api.
		setMissingMojoClassToThread();
                
                File absoluteMainDirectory=getAbsoluteMainDirectory();
                
		if (repositoriesDirectory == null) {
			repositoriesDirectory = new File(absoluteMainDirectory, "repositories");
		}

		if (targetsDirectory == null) {
			targetsDirectory = new File(absoluteMainDirectory, "targets");
		}

		if (resourcesDirectory == null) {
			resourcesDirectory = new File(absoluteMainDirectory, "resources");
		}

		if (testsDirectory == null) {
			testsDirectory = new File(absoluteMainDirectory, "tests");
		}

		if (shortcutsDirectory == null) {
			shortcutsDirectory = new File(absoluteMainDirectory, "shortcuts");
		}

		if (getOutputDirectory() == null) {
			setOutputDirectory(new File(buildDirectory, "squashTA"));
		}

		if (getLogger().isDebugEnabled()) {
			getLogger().debug("Using effective configuration : \n" + "\tmainDirectory : " + absoluteMainDirectory.getAbsolutePath()
					+ "\n" + "\trepositoriesDirectory : " + repositoriesDirectory.getPath() + "\n"
					+ "\ttargetsDirectory : " + targetsDirectory.getPath() + "\n" + "\tresourcesDirectory : "
					+ resourcesDirectory.getPath() + "\n" + "\ttestsDirectory : " + testsDirectory.getPath() + "\n"
					+ "\tshortcutsDirectory : " + shortcutsDirectory.getPath() + "\n" + "\toutputDirectory : "
					+ getOutputDirectory().getPath() + "\n");
		}
	}
	
	private void teardown(){
            //noop
	}
	
        @Override
	public SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException {

		init();
		resetOutputDirectory();
		listTest();

		// ***************** test suite generation and outer context
		// preparation*
		logPhase("compiling tests...");
		SuiteSpecification specification = createSuiteSpecification();
		TestSuiteDescription description = createSuiteDescription(specification);
                
                //metadata syntax checking in build test suite step
		TestSuite suite = buildTestSuite(description);
		TFTestWorkspaceBrowser browser = makeWorkspaceBrowser();

		// ***************** engine init
		logPhase("initializing context...");
		EngineLoader loader = new EngineLoader();
		Engine engine = loader.load();
		applyConfigurers(engine);

		// ***************** execution
		logPhase("testing...");
		SuiteResult results = engine.execute(suite, browser);
		teardown();
		return results;
	}

	private void listTest() {
		JsonTestTree listing = new JsonTestTree(testsDirectory, getOutputDirectory());
		listing.generate(true);
	}

	private SuiteSpecification createSuiteSpecification() {
		if (getLogger().isDebugEnabled()) {
			getLogger().debug(new StringBuilder("Squash TA: test suite specification is ").append(testSuiteComposition));
		}
		SuiteSpecification specification = null;
		if (testSuiteComposition != null && testSuiteComposition.trim().startsWith("{")) {
			JsonTestSuite jsonTestsuite = new JsonTestSuite(testsDirectory);
			/*
			 * The pattern below will match testSuiteComposition with the format
			 * : {file:pathToJsonFile}. When the testSuiteComposition has a such
			 * format that means a file containing the json is provided and
			 * located at pathToJsonFile
			 */
			Pattern jsonFilePattern = Pattern.compile("^\\{file:(.*)\\}$");
			Matcher matcher = jsonFilePattern.matcher(testSuiteComposition.trim());
			if (matcher.matches()) {
				/*
				 * When the testSuitComposition match, pathToJsonFile correspond
				 * to the capturing group at index 1.
				 */
				File jsonFile = new File(matcher.group(1));
				specification = jsonTestsuite.parse(jsonFile);
				deleteJsonFile(jsonFile);
			} else {
				specification = jsonTestsuite.parse(testSuiteComposition);
			}
		} else {
			specification = new SuiteSpecification();
			FilenameFilter filter = new FileFilterFactory().createFilter(testsDirectory, testSuiteComposition);
			specification.setFilter(filter);
		}
		return specification;
	}

	private void deleteJsonFile(File jsonFile) {
		if (deleteJsonFile) {
			jsonFile.delete();
		}
	}

	private TestSuiteDescription createSuiteDescription(SuiteSpecification specification) {
		if (getLogger().isDebugEnabled()) {
			getLogger().debug(new StringBuilder("Squash TA: test suite description based on composition=").append(testSuiteComposition));
		}
		TestSuiteDescription testSuiteDescription = TestSuiteDescriptionFactory.getInstance(testsDirectory,
				specification);
		testSuiteDescription.setProjectDescription(projectGroupId, projectArtifactId, projectVersion);
		return testSuiteDescription;
	}

	private TestSuiteMacro retrieveAvailableMacro() {

		TestSuiteMacro availableMacro = new TestSuiteMacro();
		availableMacro.addMacrosFromClasspath(BUILT_IN_MACROS);

		try {
			if ((shortcutsDirectory != null) && (shortcutsDirectory.exists())) {
				availableMacro.addMacrosFromURL(shortcutsDirectory.toURI().toURL());
			} else {
				logPhase("could not find the shortcuts directory in the test project, skipping");
			}
		} catch (MalformedURLException e) {
			if (getLogger().isErrorEnabled()) {
				getLogger().error("Squash TA : shortcuts directory '" + shortcutsDirectory.getPath()
						+ "' could not be translated in a valid url, skipping", e);
			}
		}
		return availableMacro;
	}

	private TFTestWorkspaceBrowser makeWorkspaceBrowser() {
                
                File absoluteMaindirectory = getAbsoluteMainDirectory();
                
                Map<String,Resource<?>> exploitationResources=new HashMap<>(2,1.0f);
                exploitationResources.put(MAIN_CODE_BUNDLE_BUILTIN_NAME, new ThirdPartyJavaCodeBundle(this.compileCP, this.mainClasses));
                exploitationResources.put(TEST_CODE_BUNDLE_BUILTIN_NAME, new ThirdPartyJavaCodeBundle(this.testCP, this.testClasses));
                
		DefaultTestProjectWorkspaceBrowser browser = new DefaultTestProjectWorkspaceBrowser(absoluteMaindirectory,exploitationResources);

		browser.setRepositoriesDirectory(repositoriesDirectory);

		browser.setResourcesDirectory(resourcesDirectory);

		browser.setTargetsDirectory(targetsDirectory);

		browser.setConfigProperties(absoluteMaindirectory);

		return browser;
	}

    private File getAbsoluteMainDirectory() {
        return mainDirectory.isAbsolute()?mainDirectory:new File(getBaseDir(),mainDirectory.getPath());
    }

	private TestSuite buildTestSuite(TestSuiteDescription description) {
		TestSuiteMacro availableMacro = retrieveAvailableMacro();
		DSLTestSuiteFactory testFactory = new DSLTestSuiteFactory(availableMacro);
		return testFactory.buildTestSuite(description);
	}

	public void setMainDirectory(File mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

	public void setRepositoriesDirectory(File repositoriesDirectory) {
		this.repositoriesDirectory = repositoriesDirectory;
	}

	public void setTargetsDirectory(File targetsDirectory) {
		this.targetsDirectory = targetsDirectory;
	}

	public void setResourcesDirectory(File resourcesDirectory) {
		this.resourcesDirectory = resourcesDirectory;
	}

	public void setTestsDirectory(File testsDirectory) {
		this.testsDirectory = testsDirectory;
	}

	public void setBuildDirectory(File buildDirectory) {
		this.buildDirectory = buildDirectory;
	}

	@Override
	protected ReportType getReportType() {
		return ReportType.EXECUTION;
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Date;
import java.util.Iterator;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.ta.backbone.test.DefaultEcosystemResult;
import org.squashtest.ta.backbone.test.DefaultSuiteResult;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.factories.TestSuiteDescription;
import org.squashtest.ta.commons.factories.dsl.list.DSLTestListFactory;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.init.TestSuiteDescriptionFactory;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.maven.list.ListTestResult;
import org.squashtest.ta.maven.testfilter.FileFilterFactory;

/**
 * <p>
 * That mojo creates a surefire-like list of the Squash TA tests in the project. It is used to integrate the engine to
 * client tools and give them a list of available tests for a given project.
 * </p>
 * 
 * @author bsiri
 * 
 * @goal test-list
 * @requiresDependencyResolution test
 * @phase integration-test
 * 
 */
public class SquashTATestListMojo extends AbstractSquashTaMojo {

	/**
	 * The base directory where the test project is defined.
	 * 
	 * @parameter property="ta.src" default-value="src/squashTA"
	 */
	private File mainDirectory;

	/**
	 * The base directory where tests are found.
	 * 
	 * @parameter property="ta.src.tests"
	 */
	private File testsDirectory;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		initLogging();

		SuiteResult result = executeImpl();
		logResult(result);
		exportResults(result);
	}

	private void logResult(SuiteResult result) {
		String newLine = "\r\n";
		StringBuilder builder = new StringBuilder("List of available test in this project :");
		builder.append(newLine);
		builder.append(newLine);
		for (EcosystemResult ecosystemResult : result.getSubpartResults()) {
			builder.append("Ecosystem : ");
			builder.append(ecosystemResult.getName());
			builder.append(newLine);
			for (TestResult testResult : ecosystemResult.getSubpartResults()) {
				builder.append("   ");
				builder.append("Test : ");
				builder.append(testResult.getName());
				builder.append(newLine);
			}
			builder.append(newLine);
		}
		logPhase(builder.toString());

	}

	@Override
	protected SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException {
		init();

		logPhase("listing tests...");
		SuiteSpecification specification = new SuiteSpecification();
		FilenameFilter filter = new FileFilterFactory().createFilter(testsDirectory, null);
		specification.setFilter(filter);
		TestSuiteDescription description = TestSuiteDescriptionFactory.getInstance(testsDirectory, specification);
		TestSuite testSuite = new DSLTestListFactory().buildTestSuite(description);

		logPhase("exporting list.");

		return  buildListingResult(testSuite);
	}

	protected void init() {
		if (testsDirectory == null) {
			testsDirectory = new File(mainDirectory, "/tests");
		}
	}

	protected SuiteResult buildListingResult(TestSuite testSuite) {

		Date now = new Date();
		DefaultSuiteResult result = new DefaultSuiteResult(testSuite.getName());
		result.setProjectDescription(projectGroupId, projectArtifactId, projectVersion);
		Iterator<Ecosystem> ecosystemIterator = testSuite.iterator();
		while (ecosystemIterator.hasNext()) {
			Ecosystem ecosystem = ecosystemIterator.next();
			DefaultEcosystemResult ecosystemResult = new DefaultEcosystemResult();
			ecosystemResult.setName(ecosystem.getName());
			ecosystemResult.setStartTime(now);
			ecosystemResult.setEndTime(now);
			ecosystemResult.setStatus(GeneralStatus.SUCCESS);
			for (Test test : ecosystem.getTestPopulation()) {
				TestResult description = new ListTestResult(test);
				ecosystemResult.addTestResult(description);
			}
			result.addTestEcosystemResult(ecosystemResult);
		}
		return result;
	}

	protected File getTestsDirectory() {
		return testsDirectory;
	}

	protected void setTestsDirectory(File testsDirectory) {
		this.testsDirectory = testsDirectory;
	}

	protected File getMainDirectory() {
		return mainDirectory;
	}

	protected void setMainDirectory(File mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

	@Override
	protected ReportType getReportType() {
		return ReportType.LIST;
	}

}

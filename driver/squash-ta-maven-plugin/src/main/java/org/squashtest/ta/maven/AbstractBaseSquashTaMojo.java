/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;

public abstract class AbstractBaseSquashTaMojo extends AbstractMojo {
    
        protected static final String INVALID_METADATA_CHECK_PARAM = "Invalid value for \'tf.metadata.check\' parameter";
        protected static final String FREEMARKER_LOGGER_LIBRARY_REF = "org.freemarker.loggerLibrary";

	// refers to
	// http://maven.apache.org/plugin-developers/common-bugs.html#Retrieving_the_Mojo_Logger
	private Log logger;
	private String freemarkerLoggingProperty;
	
        /**
         * Reference to the project's base directory.
         * This was added to make multi-module execution work (cf [Issue 0007584]).
         * The field is magicked to life by maven during the mojo's initialization.
         * 
         * @parameter property="project.basedir"
         */
        @Parameter(
                name="baseDir",
                property="project.basedir",
                defaultValue = "${user.dir}"
        )
        private File baseDir;
    
	/**
	 * Where to dump exporter output. Defaults to
	 * ${project.build.directory}/squashTF.
	 * 
	 * @parameter property="project.build.directory/squashTF"
	 */
        @Parameter(
                property = "project.build.directory/squashTF"
        )
	private File outputDirectory;
	
	/**
	 * @parameter property="project.groupId"
	 */
        @Parameter(
                property = "project.groupId",
                readonly = true
        )
	protected String projectGroupId;

	/**
	 * @parameter property="project.artifactId"
	 */
        @Parameter(
                property = "project.artifactId",
                readonly = true
        )
	protected String projectArtifactId;

	/**
	 * @parameter property="project.version"
	 */
        @Parameter(
                property = "project.version",
                readonly = true
        )
	protected String projectVersion;
        

	protected Log getLogger(){
		return logger;
	}
        
	/* Mojo core methods */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		initLogging();
                
                warnOnNonRelease();
                
		execution();
        
                cleanupLogging();
	}

    protected void cleanupLogging() {
        //reset the freemarker logging thingy
        if(freemarkerLoggingProperty==null){
            System.getProperties().keySet().remove(FREEMARKER_LOGGER_LIBRARY_REF);
        }else{
            System.setProperty(FREEMARKER_LOGGER_LIBRARY_REF, freemarkerLoggingProperty);
        }
    }

    protected void warnOnNonRelease() {
        final VersionName versionName = new VersionName(logger);
        if(versionName.isRelease()){
            logger.info("Launching Squash TA "+versionName.getVersionCodename()+" edition.");
        }else{
            LoggerFactory.getLogger("JarJar_warcry").warn("Oops! Voussa utiliser JarJar version "+versionName.getVersionData()+" noussa pas sur que voussa avoir bons résulats !!!");
            logger.warn("Using non-official version "+versionName.getVersionData()+" may bring you more functionalities, but also more trouble! Of course, you are welcome to it if you see fit.");
        }
    }
	
	protected abstract void execution() throws MojoExecutionException, MojoFailureException;
	
	/* end Mojo core methods */
	
	
	/* logging related methods. */
	protected void initLogging() {
                freemarkerLoggingProperty=System.getProperty(FREEMARKER_LOGGER_LIBRARY_REF);
                System.setProperty(FREEMARKER_LOGGER_LIBRARY_REF, "slf4j");
                SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();
                
                logger=getLog();
                
	}

	protected void logPhase(String message){
		if (logger.isInfoEnabled()){
			logger.info("Squash TF : "+message);
		}
	}

	/* Output directory-related methods */
	protected File getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	protected void resetOutputDirectory() throws MojoExecutionException{
        if (getOutputDirectory().exists()){
        	try {
				FILE_TREE.clean(getOutputDirectory());
			} catch (IOException e) {
				throw new MojoExecutionException("Could not clean output directory", e);
			}
        }
        
        boolean success=getOutputDirectory().mkdirs();
        if(!success && !getOutputDirectory().exists()){
        	throw new MojoExecutionException("Failed to create output directory "+getOutputDirectory().getAbsolutePath());
        }
	}
        
        protected File getBaseDir() {
            return baseDir;
        }

        public void setBaseDir(File baseDir) {
            this.baseDir = baseDir;
        }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link TokenInterpreter} for tokens that specify a regex.
 * @author edegenetais
 *
 */
public class RegexFilterFactory implements TokenInterpreter {
	/** Pattern to recognize the beginning of a regex */
	private static final Pattern INITIAL_REGEX_TOKEN = Pattern
			.compile("^regex('.*'?)$");

	@Override
	public boolean isEligible(File baseDirectory, List<String> availableTokens) {
		return extractRegex(availableTokens)!=null;
	}

	/**
	 * Tries to extract the beginning of a regex from a token.
	 * 
	 * @param availableTokens
	 *            the list of tokens available for treatment.
	 * @return the beginning of the regex if the first token is an initial regex
	 *         token, <code>null</code> if not. The initial quote ' is kept, and
	 *         if the whole regex fits in a token (ie, the regex does not use
	 *         any comma), the final quote is also kept.
	 */
	protected String extractRegex(List<String> availableTokens) {
		String regex=null;
		if (!availableTokens.isEmpty()) {
			Matcher matcher=INITIAL_REGEX_TOKEN.matcher(availableTokens.get(0));
			if(matcher.matches()){
				regex=matcher.group(1);
			}
		}
		return regex;
	}

	/**
	 * Build a regex based filter from one or more tokens, beginning with the first available token.
	 */
	@Override
	public FilenameFilter interprete(File baseDirectory,
			List<String> availableTokens) {
		if(availableTokens.isEmpty()){
			throw new IllegalArgumentException("Need at least one token!");
		}
		String regexPrimer=extractRegex(availableTokens);
		if(regexPrimer==null){//here case where a non eligible token list is tranmitted
			throw new IllegalArgumentException("The following part of the ta.test.suite parameter is not a valid regex : '"+availableTokens.get(0)+"'.");
		}
		FilenameFilter filter;
		availableTokens.remove(0);
		if(regexPrimer.endsWith("'")){
			filter=createFilter(baseDirectory, regexPrimer);
		}else{
			StringBuilder regexBuilder=new StringBuilder(regexPrimer);
			boolean searching=true;
			while(searching){
				String currentRegexPart = availableTokens.remove(0);
				regexBuilder.append(currentRegexPart).append(",");
				if(currentRegexPart.endsWith("'")){
					searching=false;
				}
				if(availableTokens.isEmpty() && !searching){
					throw new IllegalArgumentException("ta.test.suite parameter analyser : "+regexBuilder.substring(0, regexBuilder.length()-1)+" is not a complete regex (missing terminal quote)");
				}
			}
			regexBuilder.setLength(regexBuilder.length()-1);
			filter=createFilter(baseDirectory, regexBuilder.toString());
		}
		return filter;
	}

	protected RegexFileFilter createFilter(File baseDirectory, String regexSpec) {
		return new RegexFileFilter(baseDirectory, regexSpec.substring(1, regexSpec.length()-1));
	}

}

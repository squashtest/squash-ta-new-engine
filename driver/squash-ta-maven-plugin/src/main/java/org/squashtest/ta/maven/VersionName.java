/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Enumeration;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugin.descriptor.PluginDescriptorBuilder;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.configuration.PlexusConfigurationException;

/**
 * Implementing version codenames.
 * 
 * @author edegenetais
 */
class VersionName {
    
    private static enum CodeName{
        Chewie("1.10"),C3PO("1.11"),Ackbar("1.12"),
        HuckleberryFinn("1.13"),Odysseus("1.14");
        private final String majorVersion;

        private CodeName(String majorVersion) {
            this.majorVersion = majorVersion;
        }
        
        public static CodeName getFromVersion(String version){
            if(version.matches("[0-9]+\\.[0-9]+\\..*")){
                for(CodeName candidate:values()){
                    if(version.startsWith(candidate.majorVersion+".")){
                        return candidate;
                    }
                }
            }else{
                throw new IllegalArgumentException(version+" is not a supported version number");
            }
            return null;
        }
    }
    
    private static final String PLUGIN_MANIFEST_RESOURCENAME = "META-INF/maven/plugin.xml";
    
    private String versionData;

    private Log logger;
    
    /*
     * Compute the absolute URL of a class'es bytecode.
     */
    public VersionName(Log log) {
        this.logger=log;
        try {
            String jarURL = getClasspathRootPath();
            final String metainFmavenpluginxml = PLUGIN_MANIFEST_RESOURCENAME;
            Enumeration<URL> descriptors = AbstractBaseSquashTaMojo.class.getClassLoader().getResources(metainFmavenpluginxml);
            if (!descriptors.hasMoreElements()) {
                log.debug("No plugin descriptor found!");
            }
            URL ourDescriptorURL = null;
            while (descriptors.hasMoreElements()) {
                URL descriptorURL = descriptors.nextElement();
                String descriptorJarURL = descriptorURL.getFile().split("!")[0].replace(metainFmavenpluginxml, "");
                if (descriptorJarURL.equals(jarURL)) {
                    ourDescriptorURL = descriptorURL;
                    log.debug("Descripteur trouvé = " + descriptorURL.toExternalForm());
                } else {
                    log.debug("Descripteur tiers écarté = " + descriptorURL.toExternalForm());
                }
            }
            try (final Reader r = new InputStreamReader(ourDescriptorURL.openStream())) {
                PluginDescriptor descriptor = new PluginDescriptorBuilder().build(r);
                versionData = descriptor.getVersion();
            }
        } catch (PlexusConfigurationException | IOException ex) {
            log.debug("Failed to get hold of plugin descriptor.", ex);
        }
    }
    
    public String getVersionData() {
        return versionData;
    }   

    public boolean isRelease() {
        return versionData.matches(".*-RELEASE");
    }

    public String getVersionCodename() {
        if (isRelease()) {
            CodeName codeName=CodeName.getFromVersion(versionData);
            if(codeName==null){
                if(versionData.matches("1\\.[0-9]\\..*")){
                    /* Will of course never be found, as it predates this code, but I was so sorely tempted 0:p */
                    return "Old_Republic";
                }else{
                    logger.warn("No registered codename for that major version. Sooo sad ! :(");
                    return "John_Doe";
                }
            }else{
                return codeName.name();
            }
        } else {
            return "JarJar";
        }
    }

    protected static String getClasspathRootPath() {
        URL url = AbstractBaseSquashTaMojo.class.getResource(AbstractBaseSquashTaMojo.class.getSimpleName() + ".class");
        if ("jar".equals(url.getProtocol())) {
            //jar protocol : classpath root is the jar path, on the right of the ! separator
            return url.getFile().split("!")[0];
        } else {
            //otherwise, classpath root is the rootpath, it can be extracted by cutting out the absoolute resource name of the bytecode file
            final String bytecodeFileResourceName = AbstractBaseSquashTaMojo.class.getName().replace('.', '/') + ".class";
            return url.getFile().replace(bytecodeFileResourceName, "");
        }
    }
    
}

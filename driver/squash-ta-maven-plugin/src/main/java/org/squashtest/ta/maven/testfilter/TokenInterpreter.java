/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/**
 * Interface of subcomponent filter factories that consume tokens to create
 * filters.
 * 
 * @author edegenetais
 */
interface TokenInterpreter {
	/**
	 * See if the next token(s) can be used by this filter factory.
	 * @param baseDirectory base directory for the file set.
	 * @param availableTokens tokens available for interpretation.
	 * @return
	 */
	boolean isEligible(File baseDirectory, List<String> availableTokens);

	/**
	 * Consume some tokens to create a filter.
	 * @param baseDirectory base directory for the file set.
	 * @param availableTokens
	 *            available tokens to consume. Used tokens are taken from
	 *            the beginning of the list.
	 * @return
	 */
	FilenameFilter interprete(File baseDirectory,List<String> availableTokens);
}
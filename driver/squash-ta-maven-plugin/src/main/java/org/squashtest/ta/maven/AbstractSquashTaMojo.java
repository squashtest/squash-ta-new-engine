/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.facade.Configurer;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;

public abstract class AbstractSquashTaMojo extends AbstractBaseSquashTaMojo {
	
	/**
	 * List of all used result exporters.
	 * @parameter
	 */
        @Parameter
	private ResultExporter[] exporters = new ResultExporter[]{ new SurefireSuiteResultExporter() };

	/**
	 * List of external configurer definitions. The Mojo will apply them to the engine.
	 * @parameter
	 */
        @Parameter
	private Configurer[] configurers;
	
	/**
	 * That variable will tell whether the mojo should exit with status failure when a test failed, or if 
	 * it should exit with success no matter what. That later option could be useful if the TA build 
	 * is part of a multimodule project and you don't want it to break. Default is false.
	 * 
	 * @parameter property="ta.always.success"
         * @deprecated There are three possible final statusses : they will now be managed as a failure threshold.
         */
        @Parameter(
                property = "ta.always.success"
        )
        @Deprecated
	private Boolean alwaysSuccess;
	
        /**
         * The mojo will exit with a success code if this variable is defined and the {@link TestSuite} global {@link GeneralStatus} is as severe as this threshold, or better.
         * A {@link GeneralStatus} equal to the threshold is considered success. 
         * <ul><li>If no threshold is defined, the mojo will fail on {@link GeneralStatus#FAIL or worse. This is the old <code>alwaysSuccess=false</code> or alwaysSucess default behavior.</li>
         *     <li>If the threshold is defined as {@link GeneralStatus#ERROR}, the mojo will never fail. This is the old <code>alwaysSucess=true</code> behavior.</li>
         * </ul>
         * @parameter property="ta.success.threshold"
         * @see GeneralStatus#moreSevere(org.squashtest.ta.framework.test.result.GeneralStatus) this order is used to determine if a status reaches threshold or not.
         */
        @Parameter(
                property = "ta.success.threshold"
        )
        private GeneralStatus mojoSuccessThreshold;
        
	/**
	 * @parameter property="ta.debug.mode"
	 */
        @Parameter(
                property="ta.debug.mode"
        )
	protected String debug;
	
	/**
	 * @parameter property="ta.temp.directory"
	 */
        @Parameter(
                property="ta.temp.directory"
        )
	protected String tempDirectory;
	
	public AbstractSquashTaMojo() {
	}

	/* Mojo core methods */
	@Override
	public void execution() throws MojoExecutionException, MojoFailureException {
	
            try {
                validateConfiguration();
                
                TempFileUtils.init(tempDirectory, debug);
                     
                SuiteResult result=executeImpl();
                
                getLogger().info("Exporting results");
                exportResults(result);
                
                getLogger().info("Cleaning resources");
                result.cleanUp();
                
                //***************** done
                
                logPhase("build complete.");
                
                deleteTempFiles();
                
                ifTestsFailedThenFail(result);
                
            } catch (IOException ex) {
                throw new MojoExecutionException("An unexpected IO problem crashed the Squash TA mojo", ex);
            }
        
	}
        
        private void validateConfiguration() throws MojoExecutionException{
            if(alwaysSuccess!=null && mojoSuccessThreshold!=null){
                throw new MojoExecutionException("The alwaysSuccessfull and mojoSuccessThreshold parameters may clash, and should NOT be both defined.");
            }
        }
        
	protected abstract SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException;
	
	protected void applyConfigurers(Engine engine){
		if(configurers!=null){
			for(Configurer configurer:configurers){
				try{
					configurer.apply(engine);
				}catch(Exception e){
					throw new ConfigurationException(e,configurer);
				}
			}
		}
	}
	
	public void setConfigurers(Configurer[] configurerTable){
		configurers=Arrays.copyOf(configurerTable, configurerTable.length);
	}
	
	public Configurer[] getConfigurers(){
		return configurers;
	}
	
	/* end Mojo core methods */
	
	/* Result export-related methods */
	public ResultExporter[] getExporters() {
		return exporters;
	}

	public void setExporters(ResultExporter[] exporters) {
		this.exporters = Arrays.copyOf(exporters,exporters.length);
	}

        public void setDebug(String debug) {
         this.debug = debug;
        }
 	
	protected void exportResults(SuiteResult results) {
		initLogging();
		
		Map<String,String> usedOutputDirectories=new HashMap<>();
	    for (ResultExporter exporter : exporters){
                if(exporter==null){
                    getLogger().warn("Null exporter reference detected. Some reporting data will be missing."+
                            "\nYou may have used an SKF exporter reference element (such as html) with a runner mojo that won't recognize it."+
                            "In this case, try using the whole implementation class with the 'implementation' attribute of the 'exporter' element instead.");
                }else{
                    applyExporterToResults(exporter, usedOutputDirectories, results);
                }
	    }
	}

    private void applyExporterToResults(ResultExporter exporter, Map<String, String> usedOutputDirectories, SuiteResult results) {
        try{
            exporter.setReportType(getReportType());
            String outputSubdirectory = exporter.getOutputDirectoryName();
            if (outputSubdirectory == null) {
                outputSubdirectory = ".";
            }
            if (usedOutputDirectories.containsKey(outputSubdirectory)) {
                getLogger().warn("Subdirectory '" + outputSubdirectory
                        + "' used by both " + exporter.getClass().getName()
                        + " and "
                        + usedOutputDirectories.get(outputSubdirectory));
            }
            usedOutputDirectories.put(outputSubdirectory, exporter
                    .getClass().getName());

            File reportTarget=new File(getOutputDirectory(),outputSubdirectory);
            reportTarget.mkdirs();
            exporter.write(reportTarget, results);

        }catch(RuntimeException | IOException ex){ //Here we WANT to catch wide on RuntimeExceptions
            /* because we have no clue about Exporter's technology
            * And want to prevent une Exporter from failing all others.
            */
            getLogger().error("Squash TA : a result exporter failed, some result data may be missing.",ex);
        }
    }
	
	protected abstract ReportType getReportType();

	/* Test status accouting methods: should the execution fail? */
	protected void ifTestsFailedThenFail(SuiteResult results) throws MojoFailureException{
		
		GeneralStatus status = results.getStatus();
                
		if (statusFailsMojo(status) ){			               
			throw new MojoFailureException(buildFailedMessage(results));
		}
	}

        protected String buildFailedMessage(final SuiteResult results) {			
			StringBuilder builder = new StringBuilder();			
			builder.append("Build failure : there are tests failures\n");
			builder.append("Test statistics : "+results.getTotalTests()+" test runs, "+results.getTotalPassed()+" passed, "+results.getTotalNotPassed()+" tests didn't pass\n");
			builder.append("Tests failed / crashed : \n");
			builder.append(getFailedTestNames(results));
                        return builder.toString();
        }

    protected boolean statusFailsMojo(GeneralStatus status) throws MojoFailureException {
        GeneralStatus effectifeSuccessThreshold;
		if(alwaysSuccess==null && mojoSuccessThreshold==null){
                    effectifeSuccessThreshold=null;
                }else if(alwaysSuccess==null){
                    effectifeSuccessThreshold=mojoSuccessThreshold;
                }else if(mojoSuccessThreshold==null){
                    effectifeSuccessThreshold=alwaysSuccess.booleanValue()?GeneralStatus.ERROR:GeneralStatus.SUCCESS;
                }else{
                    throw new MojoFailureException("This is a fall through failure condition, configuration validation should have rejected it. alwaysSuccess(ta.always.success) and mojoSuccessThreshold(ta.sucess.threshold) cannot be both defined.");
                }
                if(effectifeSuccessThreshold==null){
                    return !status.isPassed();
                }else{
                    return status.moreSevere(effectifeSuccessThreshold);
                }
    }
	
	protected String getFailedTestNames(SuiteResult results){
		
		StringBuilder builder = new StringBuilder();
		
		Iterator<? extends EcosystemResult> ecoIterator = results.getSubpartResults().iterator();
		
		while (ecoIterator.hasNext()){
			
			EcosystemResult ecoResult = ecoIterator.next();
			
			String testNames = getFailedTestNames (ecoResult);
			
			builder.append(testNames);
			
		}
		
		return builder.toString();
	}

	private String getFailedTestNames(EcosystemResult results){
		String failedTestNames;
		if (results.getStatus().isPassed()){	
			failedTestNames = "";	
		}else if (results.getSetupResult()!=null && !results.getSetupResult().getStatus().isPassed()){			
			failedTestNames =  "-----------\nall tests in '"+results.getName()+"' (setup failed)\n";		
		}else{			
			Iterator<? extends TestResult> testIterator = results.getSubpartResults().iterator();		
			failedTestNames =  "-----------\n"+results.getName()+ ": \n" + collectFailedTestNames(testIterator);
		}
		return failedTestNames;
	}
	
	private String collectFailedTestNames(Iterator<? extends TestResult> iterator){
		
		StringBuilder builder = new StringBuilder();
		
		while ( iterator.hasNext() ){
			
			TestResult result = iterator.next();
			
			if (! result.getStatus().isPassed()){
				builder.append(" ==> "+result.getName()+"\n");
			}
			
		}
		
		return builder.toString();
		
	}
	
	protected void deleteTempFiles() {
		TempFileUtils.cleanUpTempDirectory();
	}
}

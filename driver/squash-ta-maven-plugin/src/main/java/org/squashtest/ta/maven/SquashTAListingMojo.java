/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.ta.commons.json.listing.JsonTestTree;
import org.squashtest.ta.commons.metadata.MetadataSyntaxChecker;

/**
 * Squash TA goal in order to list Squash TA test located in the test root directory. This goal produce a json file which is by default located in :
 * target/squashTF/test-tree/testTree.json
 * 
 * @author bfranchet
 * 
 * @goal list
 * @phase process-sources
 */
public class SquashTAListingMojo extends AbstractBaseSquashTaMojo {
        /**
	 * The base directory where the test project is defined.
	 * 
	 * @parameter property="ta.src" default-value="src/squashTA"
	 */
	private File mainDirectory;

	/**
	 * The base directory where tests are found.
	 * 
	 * @parameter property="ta.src.tests"
	 */
	private File testsDirectory;

        /**
         * 
         * @parameter property="tf.disableMetadata"
         */
        private boolean disableMetadata;
        
	/** Default tests */
	private static final String TEST_DIRECTORY = "tests";
        
        @Override
	protected void execution() throws MojoExecutionException, MojoFailureException {
            
                if (testsDirectory == null) {
                    testsDirectory = new File(mainDirectory, TEST_DIRECTORY);
                }
                JsonTestTree listing = new JsonTestTree(testsDirectory, getOutputDirectory());
                
                // ***************** execution
                logPhase("listing...");
                if (disableMetadata){
                    listing.generate(false);
                } else{
                    //metadata validation
                    logPhase("Metadata checking...");
                    new MetadataSyntaxChecker(testsDirectory).checkMetadataSyntaxByDefault(false);
                    logPhase("Metadata checking successfully completed");
                    //if no syntax error found, let's write out the json file
                    listing.generateWithMetadata();
                }
                
        }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * {@link TokenInterpreter} for litteral regular file paths.
 * @author edegenetais
 *
 */
class IncludeTestFactory implements TokenInterpreter {
	/** Regex pattern used to recognized litteral file path tokens. */
	private static final Pattern FILENAME_PATTERN = Pattern
			.compile("^([a-zA-Z]:)?[/\\\\]?([a-zA-Z0-9_\\-.][a-z A-Z0-9_\\-.]*[/\\\\])*[a-zA-Z0-9_\\-.][a-z A-Z0-9_\\-.]*$");
	
	@Override
	public FilenameFilter interprete(File baseDirectory, List<String> availableTokens) {
		List<String> fileNames = new ArrayList<String>();
		while (isEligible(baseDirectory,availableTokens)) {
			fileNames.add(availableTokens.remove(0));
		}
		if(fileNames.isEmpty()){
			throw new IllegalArgumentException("The following part of the ta.test.suite parameter is not a valid filename :'"+availableTokens.get(0)+"'.");
		}
		return new IncludeTestFilter(baseDirectory, fileNames);
	}

	@Override
	public boolean isEligible(File baseDirectory, List<String> availableTokens) {
		return !availableTokens.isEmpty() && FILENAME_PATTERN.matcher(availableTokens.get(0)).matches();
	}
}
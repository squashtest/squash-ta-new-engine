/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.param.json;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.factories.specification.TestSpecification;
import org.squashtest.ta.maven.testfilter.FileFilterFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTestSuite {

	private static final Logger LOGGER = LoggerFactory.getLogger(JsonTestSuite.class);

	private static final String FILTER = "filter";
	private static final String TEST = "test";
	private static final String SCRIPT = "script";
	private static final String ID = "id";
	private static final String PARAM = "param";

	private File testsDirectory;

	public JsonTestSuite(File testDirectory) {
		this.testsDirectory = testDirectory;
	}

	public SuiteSpecification parse(File jsonFileToParse) {
		ObjectMapper mapper = initMapper();
		try {
			JsonNode rootNode = mapper.readTree(jsonFileToParse);
			return processJsonTree(rootNode);
		} catch (JsonProcessingException e) {
			StringBuilder msg = new StringBuilder("An error occurs while processing the json file (");
			msg.append(jsonFileToParse.getPath());
			msg.append(")");
			LOGGER.error(msg.toString(), e);
			throw new IllegalArgumentException(msg.toString(), e);
		} catch (IOException e) {
			StringBuilder msg = new StringBuilder("An error occurs while processing the json file (");
			msg.append(jsonFileToParse.getPath());
			msg.append(")");
			LOGGER.error(msg.toString(), e);
			throw new IllegalArgumentException(msg.toString(), e);
		}
	}

	public SuiteSpecification parse(String jsonStringToParse) {

		ObjectMapper mapper = initMapper();
		try {
			JsonNode rootNode = mapper.readTree(jsonStringToParse);
			return processJsonTree(rootNode);
		} catch (JsonProcessingException e) {
			StringBuilder msg = new StringBuilder(
					"An error occurs while processing the ta.test.list parameter (json type)");
			LOGGER.error(msg.toString(), e);
			throw new IllegalArgumentException(msg.toString(), e);
		} catch (IOException e) {
			StringBuilder msg = new StringBuilder(
					"An error occurs while processing the ta.test.list parameter (json type)");
			LOGGER.error(msg.toString(), e);
			throw new IllegalArgumentException(msg.toString(), e);
		}
	}

	private ObjectMapper initMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
		return mapper;
	}

	private SuiteSpecification processJsonTree(JsonNode rootNode) {

		SuiteSpecification specification = new SuiteSpecification();
		JsonNode filterNode = rootNode.get(FILTER);
		JsonNode testListNode = rootNode.get(TEST);

		if (testListNode != null && filterNode != null) {
			throw new IllegalArgumentException(
					"Json test suite definition : \"test\" and \"filter\" parameter can't be used together");
		}
		if (testListNode == null && filterNode == null) {
			throw new IllegalArgumentException(
					"Json test suite definition: At least one of \"test\" and \"filter\" parameter should be defined.");
		}
		if (filterNode != null) {
			generateSpecificationForFilter(filterNode, specification);
		} else {
			generateSpecificationForOrdered(testListNode, specification);
		}
		JsonNode globalParamNode = rootNode.get(PARAM);
		if (globalParamNode != null) {
			specification.setGlobalParams(retrieveParam(globalParamNode));
		}
		return specification;
	}

	private void generateSpecificationForOrdered(JsonNode testListNode, SuiteSpecification specification) {
		if (testListNode.isArray()) {
			if (testListNode.size() > 0) {
				specification.setTests(getTests(testListNode));
			} else {
				throw new IllegalArgumentException(
						"Json test suite definition: The value for the \"test\" parameter can't be an empty array");
			}
		} else {
			throw new IllegalArgumentException(
					"Json test suite definition: The value for the \"test\" parameter should be a json array");
		}
	}

	private List<TestSpecification> getTests(JsonNode testListNode) {
		JsonNode node = testListNode.get(0);
		List<TestSpecification> tests = new LinkedList<TestSpecification>();
		if (node.isObject()) {
			boolean testIdSet = node.get(ID) != null ? true : false;
			for (JsonNode jsonNode : testListNode) {
				JsonNode idNode = jsonNode.get(ID);
				boolean currentIdSet = idNode != null ? true : false;
				if (currentIdSet == testIdSet) {
					JsonNode scriptNode = jsonNode.get(SCRIPT);
					JsonNode param = jsonNode.get(PARAM);
					tests.add(generateTest(scriptNode, idNode, param));
				} else {
					throw new IllegalArgumentException(
							"Json test suite definition : All the test for an ordered execution should be define in the same way; The \"id\" attribute should be define for all or for none");
				}
			}
		} else {
			for (JsonNode jsonNode : testListNode) {
				tests.add(new TestSpecification(jsonNode.asText()));
			}
		}
		return tests;
	}

	private TestSpecification generateTest(JsonNode scriptNode, JsonNode idNode, JsonNode paramNode) {
		if (scriptNode == null) {
			throw new IllegalArgumentException(
					"Json test suite definition: \"script\" attribute is mandatory to define a test in an ordered execution");
		}
		String scriptPath = scriptNode.asText();
		if (scriptPath.isEmpty()) {
			throw new IllegalArgumentException("Json test suite definition: \"script\" attribute can't be empty to define a test in an ordered execution");
		}
		String id = null;
		if (idNode != null) {
			id = idNode.asText();
		}
		Map<String, String> param = null;
		if (paramNode != null) {
			param = retrieveParam(paramNode);
		}
		return new TestSpecification(scriptPath, id, param);
	}

	private Map<String, String> retrieveParam(JsonNode paramNode) {
		Map<String, String> paramMap = new HashMap<String, String>();
		for (Iterator<Map.Entry<String, JsonNode>> iterator = paramNode.fields(); iterator.hasNext();) {
			Map.Entry<String, JsonNode> elt = iterator.next();
			String previousValue = paramMap.put(elt.getKey(), elt.getValue().asText());
			if (previousValue != null) {
				throw new IllegalArgumentException("Json test suite definition: A \"param\" attribute contains twice the same key : \""+elt.getKey()+"\"");
			}
		}
		return paramMap;
	}

	private void generateSpecificationForFilter(JsonNode filterNode, SuiteSpecification specification) {
		FilenameFilter filter = new FileFilterFactory().createFilter(testsDirectory, filterNode.asText());
		specification.setFilter(filter);
	}

}

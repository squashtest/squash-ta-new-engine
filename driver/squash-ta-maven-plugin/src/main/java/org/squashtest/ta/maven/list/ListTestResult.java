/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.list;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * This class is a model of the project's test list as a 'TestResult' implementation.
 * This is used by the XML test list mojo.
 * 
 * @author ericdegenetais
 */
public class ListTestResult implements TestResult {

	private static final ListExecutionDetails LIST_EXECUTION_DETAILS = new ListExecutionDetails();

      	private static final class ListExecutionDetails implements ExecutionDetails {
		@Override
		public InstructionType instructionType() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int instructionNumberInPhase() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int instructionNumber() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String instructionAsText() {
			return "test list";
		}

		@Override
		public GeneralStatus getStatus() {
			return GeneralStatus.SUCCESS;
		}

		@Override
		public Collection<ResourceAndContext> getResourcesAndContext() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Exception caughtException() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void addResourceAndContext(ResourceAndContext contextResource) {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<ExecutionDetails> getChildrens() {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<ExecutionDetails> getErrorOrFailedChildrens() {
 			return null;
 		}
		
		@Override
		public int getErrorOrFailedWithContinue() {
 			return 0;
 		}
		
	}

	private Test support;
	private Date timeStamp=new Date();
	
	public ListTestResult(Test test){
		this.support=test;
	}
	
	@Override
	public String getName() {
		return support.getName();
	}

	@Override
	public Date startTime() {
		return timeStamp;
	}

	@Override
	public Date endTime() {
		return timeStamp;
	}

	@Override
	public GeneralStatus getStatus() {
		return LIST_EXECUTION_DETAILS.getStatus();
	}

	@Override
	public void cleanUp() {
		//noop: no resource held.
	}

	@Override
	public int getTotalInstructions() {
		return -1;
	}

	@Override
	public ExecutionDetails getFirstFailure() {
		return LIST_EXECUTION_DETAILS;
	}

	@Override
	public int getTotalAssertions() {
		return -1;
	}

	@Override
	public int getTotalCommands() {
		return -1;
	}
        
	@Override
	public PhaseResult getSetupPhaseResult() {
		return null;
	}

	@Override
	public PhaseResult getTestPhaseResult() {
		return null;
	}

	@Override
	public PhaseResult getTeardownPhaseResult() {
		return null;
	}

	@Override
	public boolean hasOnlyFailOrErrorWithContinue() {
		return false;
	}

	@Override
	public String getTestId() {
		return "";
	}
	
}

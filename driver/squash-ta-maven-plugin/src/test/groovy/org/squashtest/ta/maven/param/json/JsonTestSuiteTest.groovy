/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.param.json

import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.factories.specification.TestSpecification;
import org.squashtest.ta.maven.testfilter.FileFilterFactory;

import spock.lang.Specification

class JsonTestSuiteTest extends Specification {

	JsonTestSuite testee
	
	def "Orderer execution with id, test params and global params"(){
		given:
			testee = new JsonTestSuite(new File("fake"))

		when:
			SuiteSpecification spec = testee.parse("{\"test\":[{\"script\":\"essai/placeHolder.ta\",\"id\":\"5\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-me\":\"Joey\",\"date\":\"today\"}},{\"script\":\"essai2/placeHolder.ta\",\"id\":\"6\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-mes\":\"Marguerite\",\"date\":\"yesterday\"}}],\"param\":{\"blah\":\"foo\",\"ow_ner.Na-me\":\"bar\",\"date\":\"monday\"}}")
		
		then:
			spec.getFilter() == null
			spec.filtered==false
			spec.ordered==true
			List<TestSpecification> tests = spec.getTests()
			tests.size()==2
			TestSpecification test1 = tests[0]
			test1.script=="essai/placeHolder.ta"
			test1.id=="5"
			test1.param.get("id")=="1\u20AC"
			test1.param.get("ow_ner.Na-me")=="Joey"
			test1.param.get("date")=="today"
			TestSpecification test2 = tests[1]
			test2.script=="essai2/placeHolder.ta"
			test2.id=="6"
			test2.param.get("id")=="1\u20AC"
			test2.param.get("ow_ner.Na-mes")=="Marguerite"
			test2.param.get("date")=="yesterday"
			spec.globalParams.get("blah")=="foo"
			spec.globalParams.get("ow_ner.Na-me")=="bar"
			spec.globalParams.get("date")=="monday"
			
	}
	
	def "Ordered execution with id and test params"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		
		when:
			SuiteSpecification spec = testee.parse("{\"test\":[{\"script\":\"essai/placeHolder.ta\",\"id\":\"5\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-me\":\"Joey\",\"date\":\"today\"}},{\"script\":\"essai2/placeHolder.ta\",\"id\":\"6\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-mes\":\"Marguerite\",\"date\":\"yesterday\"}}]}")
		
		then:
			spec.getFilter() == null
			List<TestSpecification> tests = spec.getTests()
			tests.size()==2
			TestSpecification test1 = tests[0]
			test1.script=="essai/placeHolder.ta"
			test1.id=="5"
			test1.param.get("id")=="1\u20AC"
			test1.param.get("ow_ner.Na-me")=="Joey"
			test1.param.get("date")=="today"
			TestSpecification test2 = tests[1]
			test2.script=="essai2/placeHolder.ta"
			test2.id=="6"
			test2.param.get("id")=="1\u20AC"
			test2.param.get("ow_ner.Na-mes")=="Marguerite"
			test2.param.get("date")=="yesterday"
			spec.globalParams.size()==0
			
	}
	
	def "Ordered execution with id and one test with params and another test without params"(){
		given:
			testee = new JsonTestSuite(new File("fake"))

		when:
			SuiteSpecification spec = testee.parse("{\"test\":[{\"script\":\"essai/placeHolder.ta\",\"id\":\"5\"},{\"script\":\"essai2/placeHolder.ta\",\"id\":\"6\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-mes\":\"Marguerite\",\"date\":\"yesterday\"}}]}")
		
		then:
			spec.getFilter() == null
			List<TestSpecification> tests = spec.getTests()
			tests.size()==2
			TestSpecification test1 = tests[0]
			test1.script=="essai/placeHolder.ta"
			test1.id=="5"
			test1.param.size()==0
			TestSpecification test2 = tests[1]
			test2.script=="essai2/placeHolder.ta"
			test2.id=="6"
			test2.param.get("id")=="1\u20AC"
			test2.param.get("ow_ner.Na-mes")=="Marguerite"
			test2.param.get("date")=="yesterday"
			spec.globalParams.size()==0			
	}
	
	def "Ordered execution without id and with test params"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		
		when:
			SuiteSpecification spec = testee.parse("{\"test\":[{\"script\":\"essai/placeHolder.ta\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-me\":\"Joey\",\"date\":\"today\"}},{\"script\":\"essai2/placeHolder.ta\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-mes\":\"Marguerite\",\"date\":\"yesterday\"}}]}")
		
		then:
			spec.getFilter() == null
			List<TestSpecification> tests = spec.getTests()
			tests.size()==2
			TestSpecification test1 = tests[0]
			test1.script=="essai/placeHolder.ta"
			test1.id==null
			test1.param.get("id")=="1\u20AC"
			test1.param.get("ow_ner.Na-me")=="Joey"
			test1.param.get("date")=="today"
			TestSpecification test2 = tests[1]
			test2.script=="essai2/placeHolder.ta"
			test2.id==null
			test2.param.get("id")=="1\u20AC"
			test2.param.get("ow_ner.Na-mes")=="Marguerite"
			test2.param.get("date")=="yesterday"
			spec.globalParams.size()==0
			
	}
	
	def "Ordered execution should throw an exception when one test has an id while another hasn't"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		when:
			testee.parse("{\"test\":[{\"script\":\"essai/placeHolder.ta\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-me\":\"Joey\",\"date\":\"today\"}},{\"script\":\"essai2/placeHolder.ta\",\"id\":\"6\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-mes\":\"Marguerite\",\"date\":\"yesterday\"}}]}")
		then:
			thrown IllegalArgumentException			
	}
	
	def "Filtered execution"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		when:
			SuiteSpecification spec = testee.parse("{\"filter\":\"**/*.ta\"}")
		then:
			spec.filter!=null
			spec.filtered==true
			spec.ordered==false
	}
	
	def "Should thrown an exception if filtered and ordered are defined"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		when:
			testee.parse("{\"filter\":\"**/*.ta\",\"test\":[]}")
		then:
			thrown IllegalArgumentException
	}
	
	def "Should thrown an exception if ordered is defined with an empty array"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		when:
			testee.parse("{\"test\":[]}")
		then:
			thrown IllegalArgumentException
	}
	
	def "Should thrown an exception if a is defined without a \"script\" attribute"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		when:
			testee.parse("{\"test\":[{\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-me\":\"Joey\",\"date\":\"today\"}}]}")
		then:
			thrown IllegalArgumentException
	}
	
	def "Should thrown an exception if a is defined with an empty String for \"script\" attribute value"(){
		given:
			testee = new JsonTestSuite(new File("fake"))
		when:
			testee.parse("{\"test\":[{\"script\":\"\",\"param\":{\"id\":\"1\u20AC\",\"ow_ner.Na-me\":\"Joey\",\"date\":\"today\"}}]}")
		then:
			thrown IllegalArgumentException
	}
	
}

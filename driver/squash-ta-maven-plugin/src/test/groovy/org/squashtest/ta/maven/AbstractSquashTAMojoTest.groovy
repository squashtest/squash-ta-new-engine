/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven

import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException
import org.squashtest.ta.commons.exporter.ReportType
import org.squashtest.ta.framework.facade.Configurer
import org.squashtest.ta.framework.facade.Engine
import org.squashtest.ta.framework.test.result.GeneralStatus
import org.squashtest.ta.framework.test.result.SuiteResult

import spock.lang.Specification

class AbstractSquashTAMojoTest extends Specification{
	AbstractSquashTaMojo testee
	def "should apply configurers"(){
		given:
			Engine engineMock=Mock()
		and:
			SuiteResult result=Mock()
			result.getStatus()>>GeneralStatus.SUCCESS
		and:
			testee=new AbstractSquashTaMojo(){
				@Override
				protected SuiteResult executeImpl() throws MojoExecutionException,
				MojoFailureException {
					applyConfigurers(engineMock)
					return result
				}
				@Override
				protected ReportType getReportType(){
					return ReportType.EXECUTION
				}	
				@Override
				protected void deleteTempFiles() {
					//is not tested
				}			
			}
		and:
			Configurer configurerMock=Mock()
		and:
			testee.configurers=[configurerMock]
			testee.exporters=[]
		when:
			testee.execute()
		then:
			1 * configurerMock.apply(engineMock)
	}
}

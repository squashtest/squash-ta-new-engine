/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter

import org.squashtest.ta.maven.testfilter.WildcardFilterFactory;

import spock.lang.Specification;

class WildcardFilterFactoryTest extends Specification{
	WildcardFilterFactory testee
	def _nixBaseDir
	def windozBaseDir
	
	def setup(){
		testee=new WildcardFilterFactory()
		_nixBaseDir = new File("/home/developper/workspace/squashTA-Engine//test/squash-ta-integration-test/src/squashTA/tests")
		windozBaseDir = new File("D:\\developper\\workspace\\squashTA-Engine\\test\\squash-ta-integration-test\\src\\squashTA\\tests")
	}
		
	def "single file is NOT eligible"(){
		when:
			def eligible=testee.isEligible(null, ["test.txt"])
		then:
			!eligible
	}
	
	def "absolute windoz file is NOT eligible"(){
		when:
			def eligible=testee.isEligible(null, ["C:\\test.txt"])
		then:
			!eligible
	}
	
	def "absolute *nix file is NOT eligible"(){
		when:
			def eligible=testee.isEligible(null, ["/home/dev/test.txt"])
		then:
			!eligible
	}
	
	def "windoz relative file + base is NOT eligible"(){
		when:
			def eligible=testee.isEligible(windozBaseDir, ["test.txt"])
		then:
			!eligible
	}
	
	def "*nix relative file + base is NOT eligible"(){
		when:
			def eligible=testee.isEligible(_nixBaseDir, ["test.txt"])
		then:
			!eligible
	}
	
	def "wildcard is eligible in *nix"(){
		when:
			def eligible=testee.isEligible(_nixBaseDir, ["*.txt"])
		then:
			eligible
	}
	
	def "wildcard is eligible in windoz"(){
		when:
			def eligible=testee.isEligible(windozBaseDir, ["*.txt"])
		then:
			eligible
	}
	
	def "regex is NOT eligible in *nix"(){
		when:
			def eligible=testee.isEligible(_nixBaseDir, ["regex'test\\.txt'"])
		then:
			!eligible
	}
	
	def "regex is NOT eligible in windoz"(){
		when:
			def eligible=testee.isEligible(windozBaseDir, ["regex'test\\.txt'"])
		then:
			!eligible
	}
	
	def "absolute windoz wildcard is eligible"(){
		given:
			String wildcard=
			"D:\\developper\\workspace\\squashTA-Engine\\test\\squash-ta-integration-test\\src\\squashTA\\tests\\ssh/*"
		when:
			def accept=testee.isEligible(windozBaseDir, [wildcard])
		then:
			accept
	}
	
	def "absolute *nix wildcard is eligible"(){
		when:
			def accept=testee.isEligible(_nixBaseDir, ["/home/developper/workspace/squashTA-Engine/test/squash-ta-integration-test/src/squashTA/tests/ssh/*"])
		then:
			accept
	}
	
	def "relative windoz wildcard is eligible"(){
		when:
			def accept=testee.isEligible(windozBaseDir, ["ssh\\*"])
		then:
			accept
	}
	
	def "relative *nix wildcard is eligible"(){
		when:
			def accept=testee.isEligible(_nixBaseDir, ["ssh/*"])
		then:
			accept
	}
	
	def "case of issue 1014 should be eligible in *nix"(){
		when:
			def accept=testee.isEligible(_nixBaseDir, ["foo/**/*.*"])
		then:
			accept
	}
	
	def "case of issue 1014 should be eligible in windoz"(){
		when:
			def accept=testee.isEligible(windozBaseDir, ["foo/**/*.*"])
		then:
			accept
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.testfilter

import java.io.File;
import java.util.List;

import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.Unzipper;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.maven.testfilter.FileFilterFactory;

import spock.lang.Specification;

class FileFilterFactoryTest extends Specification {
	FileFilterFactory testee
	File testTree
	def setup(){
		testee=new FileFilterFactory()
		testTree=new Unzipper().unzipInTemp(getClass().getResourceAsStream("FileFilterTestTree.zip"))
	}
	
	def "empty takes all"(){
		given:
			List<File> expected=new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)
			Set<String> expectedNames=new HashSet<String>()
			for(File file:expected){
				expectedNames.add(file.getName())
			}
		when:
			FilenameFilter filter=testee.createFilter(testTree, "")
			Set<String>actualNames=new HashSet<String>()
			for(String name:expectedNames){
				if(filter.accept(testTree, name)){
					actualNames.add(name)
				}
			}
		then:
			actualNames.equals(expectedNames)
	}
	
	def "name -> file at first level"(){
		given:
			List<String>expected=[new File(testTree,"test2.txt").getCanonicalPath()]
		when:
			FilenameFilter filter=testee.createFilter(testTree, "test2.txt")
			List<String>actualNames=new ArrayList<String>()
			for(String name:new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)){
				def candidateFile = new File(name)
				if(filter.accept(candidateFile.getParentFile(), candidateFile.getName())){
					actualNames.add(candidateFile.getCanonicalPath())
				}
			}
		then:
			actualNames.equals(expected)
	}
	
	def "full path to first level file -> OK "(){
		given:
			List<String>expected=[new File(testTree,"test2.txt").getCanonicalPath()]
		when:
			FilenameFilter filter=testee.createFilter(testTree, new File(testTree,"test2.txt").getCanonicalPath())
			List<String>actualNames=new ArrayList<String>()
			for(String name:new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)){
				def candidateFile = new File(name)
				if(filter.accept(candidateFile.getParentFile(), candidateFile.getName())){
					actualNames.add(candidateFile.getCanonicalPath())
				}
			}
		then:
			actualNames.equals(expected)
	}
	
	def "full path to deeper level file -> OK "(){
		given:
			List<String>expected=[new File(testTree,"dir2/test2.txt").getCanonicalPath()]
		when:
			FilenameFilter filter=testee.createFilter(testTree, new File(testTree,"dir2/test2.txt").getCanonicalPath())
			List<String>actualNames=new ArrayList<String>()
			for(String name:new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)){
				def candidateFile = new File(name)
				if(filter.accept(candidateFile.getParentFile(), candidateFile.getName())){
					actualNames.add(candidateFile.getCanonicalPath())
				}
			}
		then:
			actualNames.equals(expected)
	}
	
	def "test with two files"(){
		given:
			Set<String>expected=[new File(testTree,"dir2/test2.txt").getCanonicalPath(),new File(testTree,"test1.txt").getCanonicalPath()]
		when:
			String element1=new File(testTree,"dir2/test2.txt").getCanonicalPath()
			String element2="test1.txt"
			String spec=element1+","+element2
			FilenameFilter filter=testee.createFilter(testTree,spec)
			Set<String>actualNames=new HashSet<String>()
			for(String name:new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)){
				def candidateFile = new File(name)
				if(filter.accept(candidateFile.getParentFile(), candidateFile.getName())){
					actualNames.add(candidateFile.getCanonicalPath())
				}
			}
		then:
			actualNames.equals(expected)
	}
	
	def "test with four files"(){
		given:
			Set<String>expected=[
				new File(testTree,"dir2/test2.txt").getCanonicalPath(),
				new File(testTree,"test1.txt").getCanonicalPath(),
				new File(testTree,"dir1/test2.txt").getCanonicalPath(),
				new File(testTree,"test2.txt").getCanonicalPath()
				]
		when:
			String element1=new File(testTree,"dir2/test2.txt").getCanonicalPath()
			String element2="test1.txt"
			String element3=new File(testTree,"dir1/test2.txt").getCanonicalPath()
			String element4="test2.txt"
			String spec=element1+","+element2+","+element3+","+element4
			FilenameFilter filter=testee.createFilter(testTree,spec)
			Set<String>actualNames=new HashSet<String>()
			for(String name:new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)){
				def candidateFile = new File(name)
				if(filter.accept(candidateFile.getParentFile(), candidateFile.getName())){
					actualNames.add(candidateFile.getCanonicalPath())
				}
			}
		then:
			actualNames.equals(expected)
	}
	
	def "properly handle wildcard filter"(){
		given:
			List<String>expected=[new File(testTree,"test2.txt").getCanonicalPath()]
		when:
			FilenameFilter filter=testee.createFilter(testTree, "t*2.txt")
			List<String>actualNames=new ArrayList<String>()
			for(String name:new FileTree().enumerate(testTree, EnumerationMode.FILES_ONLY)){
				def candidateFile = new File(name)
				if(filter.accept(candidateFile.getParentFile(), candidateFile.getName())){
					actualNames.add(candidateFile.getCanonicalPath())
				}
			}
		then:
			actualNames.equals(expected)
	}
	
	def "worspace"(){
		given:
			File mockFile = new File("directoryPath")
			List<String> mockTokens = new ArrayList<String>()
			mockTokens.add("token1")
			mockTokens.add(mockFile.getAbsolutePath()+"token2")
		when:
			def e = testee.convertToRelativePath(mockFile, mockTokens)
		then:
			e == ["token1", "token2"]
	}
	
	def cleanup(){
		new Unzipper().clean(testTree)		
	}
}

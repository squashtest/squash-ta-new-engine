<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.squashtest.ta</groupId>
        <artifactId>squash-ta-parent</artifactId>
        <version>1.8.2020-RELEASE</version>
    </parent>

    <artifactId>squash-ta-umbrella</artifactId>
    <version>1.15.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <!-- ============================ distribution ======================== -->

    <scm>
        <connection>scm:git:${project.basedir}</connection>
        <developerConnection>scm:git:${project.basedir}</developerConnection>
      <tag>HEAD</tag>
  </scm>

    
    <!-- ============================ Modules ======================== -->
    
    <modules>
        <module>dependencies</module>
        <module>api</module>
        <module>core</module>
        <module>driver</module>
        <module>plugin</module>
        <module>library</module>
    </modules>
    
    
    <!-- =============================== main properties ======================= -->
    
    <properties>
        
        <maven.remote.resources.version>1.4</maven.remote.resources.version>

        <saxon.version>9.6.0-7</saxon.version>
        <apache.lang.version>2.6</apache.lang.version>
        <apache.digester.version>1.8.1</apache.digester.version>
        <apache.io.version>2.4</apache.io.version>
        <apache.logging.version>1.2</apache.logging.version>
        <maven.mojo.api>3.6.0</maven.mojo.api>
        <log4j.version>2.5</log4j.version>
        <javax.inject.version>1</javax.inject.version>
        <spring.version>4.2.2.RELEASE</spring.version>
        
        <!-- Html template library used for the html reporter -->
        <freemarker.version>2.3.23</freemarker.version>
        
        <!-- json processor -->
        <jackson.version>2.5.4</jackson.version>

        <!-- Plugin Commons component -->
        <opencsv.version>2.3</opencsv.version>
        <xmlunit.version>1.6</xmlunit.version>

        <!-- Plugin db -->
        <dbunit.version>2.4.9</dbunit.version>
        <c3p0.version>0.9.1.2</c3p0.version>

        <!-- Plugin filechecker -->
        <filechecker.library.version>1.6.1-RELEASE</filechecker.library.version>

        <!-- Plugin ftp -->
        <apache.commons.net>3.3</apache.commons.net>

        <!-- Plugin sahi -->
        <sahi.version>5.0</sahi.version>

        <!-- Plugin selenium -->
        <selenium.version>3.141.59</selenium.version>
        
        <!-- Plugin selenium legacy-->
        <selenium.legacy.version>2.53.1</selenium.legacy.version>
                
        <!-- Plugin ssh -->
        <schmizz.sshj.version>0.13.0</schmizz.sshj.version>

        <!-- Plugin soapUi -->
        <soapui.version>5.5.0</soapui.version>
        
        <!--  local process -->
        <commons.exec.version>1.3</commons.exec.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- To put in the parent pom -->
            <dependency>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-remote-resources-plugin</artifactId>
                 <version>${maven.remote.resources.version}</version>
            </dependency>
        
            <dependency>
                <groupId>org.squashtest.ta</groupId>
                <artifactId>squash-ta-core-utils</artifactId>
                <version>${project.version}</version>
            </dependency>
            
            <dependency>
                <groupId>org.squashtest.ta</groupId>
                <artifactId>squash-ta-framework</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
        	<artifactId>log4j-slf4j-impl</artifactId>
               	<version>${log4j.version}</version>
               	<scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>jcl-over-slf4j</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
                
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>log4j-over-slf4j</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
                
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>jul-to-slf4j</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
                
            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-core</artifactId>
                <version>${log4j.version}</version>
                <scope>test</scope>
            </dependency>
            
            <dependency>
                <groupId>commons-digester</groupId>
                <artifactId>commons-digester</artifactId>
                <version>${apache.digester.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${apache.io.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>${apache.lang.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
                <version>${apache.logging.version}</version>
                <scope>provided</scope><!-- vade retro, satanas -->
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-core</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>${spring.version}</version>
            </dependency>
            
            <dependency>
		<groupId>javax.inject</groupId>
		<artifactId>javax.inject</artifactId>
		<version>${javax.inject.version}</version>
            </dependency>

            <dependency>
                <groupId>net.sf.saxon</groupId>
                <artifactId>Saxon-HE</artifactId>
                <version>${saxon.version}</version>
            </dependency>

            <dependency>
                <groupId>net.sourceforge.saxon</groupId>
                <artifactId>saxon</artifactId>
                <version>${saxon.version}</version>
                <classifier>dom</classifier>
            </dependency>

            <!-- For Html reporter -->
            <dependency>
                <groupId>org.freemarker</groupId>
                <artifactId>freemarker</artifactId>
                <version>${freemarker.version}</version>
            </dependency>
            
            <!-- Json processor -->
            <dependency>
              <groupId>com.fasterxml.jackson.core</groupId>
              <artifactId>jackson-databind</artifactId>
              <version>${jackson.version}</version>
            </dependency>
            <dependency>
              <groupId>com.fasterxml.jackson.core</groupId>
              <artifactId>jackson-core</artifactId>
              <version>${jackson.version}</version>
            </dependency>
            
            <!-- ========= Plugin commons component ========= -->
            <!-- dependency for CSV parsing purposes -->
            <dependency>
                <groupId>net.sf.opencsv</groupId>
                <artifactId>opencsv</artifactId>
                <version>${opencsv.version}</version>
            </dependency>
            <dependency>
                <groupId>xmlunit</groupId>
                <artifactId>xmlunit</artifactId>
                <version>${xmlunit.version}</version>
            </dependency>


            <!-- ========= Plugin db ========= -->
            <dependency>
                <groupId>org.dbunit</groupId>
                <artifactId>dbunit</artifactId>
                <version>${dbunit.version}</version>
            </dependency>

            <dependency>
                <groupId>c3p0</groupId>
                <artifactId>c3p0</artifactId>
                <version>${c3p0.version}</version>
            </dependency>


            <!-- ========= Plugin filechecker ========= -->
            <dependency>
                <groupId>org.squashtest.ta</groupId>
                <artifactId>filechecker-lib</artifactId>
                <version>${filechecker.library.version}</version>
            </dependency>


            <!-- ========= Plugin ftp ========= -->
            <dependency>
                <groupId>commons-net</groupId>
                <artifactId>commons-net</artifactId>
                <version>${apache.commons.net}</version>
            </dependency>


            <!-- ========= Plugin Sahi ========== -->
            <dependency>
                <groupId>net.sf.sahi</groupId>
                <artifactId>sahi</artifactId>
                <version>${sahi.version}</version>
            </dependency>
            <dependency>
                <groupId>net.sf.sahi</groupId>
                <artifactId>ant-sahi</artifactId>
                <version>${sahi.version}</version>
            </dependency>
            
            
            <!-- ========= Plugin ssh ========== -->
            <dependency>
                <groupId>com.hierynomus</groupId>
                <artifactId>sshj</artifactId>
                <version>${schmizz.sshj.version}</version>
            </dependency>


            <!-- ========= Plugin SoapUi ========== -->
            <dependency>
                <groupId>com.smartbear.soapui</groupId>
                <artifactId>soapui</artifactId>
                <version>${soapui.version}</version>
                <exclusions>
                    <!-- 
                        These artifacts might trigger conflicts with jdk15on
                        elsewhere in the dependency tree => we exclude them
                    -->
                    <exclusion>
                        <groupId>bouncycastle</groupId>
                        <artifactId>bcprov-jdk15</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>bouncycastle</groupId>
                        <artifactId>bcpkix-jdk15</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.slf4j</groupId>
                        <artifactId>slf4j-log4j12</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            
            <!-- ========= Plugin Selenium ========== -->
            
            <!-- For the selenium plugin, we had to remove the dependency management because the selenium plugin and the selenium legacy plugin both have to manage their own dependencies -->

            
            <!-- === Compatibilité Selenium/SoapUI -->
                        <!-- For some library soapui (in 5.1.3) used older version than selenium (in 2.48.2)-->
            <!-- As a consequence some selenium test failed and that's why need the version management below -->
            <!-- NB : excluding like we did before is discouraged by the maven documentation (should be used as a last resort)
            , so we use dependency management instead -->
            <dependency>
                <groupId>net.sourceforge.htmlunit</groupId>
                <artifactId>htmlunit</artifactId>
		<version>2.18</version>
            </dependency>
            <dependency>
                <groupId>commons-codec</groupId>
                <artifactId>commons-codec</artifactId>
                <version>1.4</version>
            </dependency>
            <dependency>
                <groupId>net.sourceforge.htmlunit</groupId>
                <artifactId>htmlunit-core-js</artifactId>
		<version>2.17</version>
            </dependency>
            <dependency>
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis</artifactId>
		<version>1.4.01</version>
            </dependency>
            <dependency>
                <groupId>net.sourceforge.nekohtml</groupId>
                <artifactId>nekohtml</artifactId>
		<version>1.9.22</version>
            </dependency>
            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpclient</artifactId>
		<version>4.5.1</version>
            </dependency>
            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpcore</artifactId>
		<version>4.4.3</version>
            </dependency>     
            <!-- ========= Local process ========= -->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-exec</artifactId>
                <version>${commons.exec.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            
            <plugins>
                <plugin>
                    <groupId>com.mycila</groupId>
                    <artifactId>license-maven-plugin</artifactId>
                    <configuration>
                        <excludes combine.children="append">
                            <!-- TODO : look into how we may include headers in the core's plugin manifests -->
                            <exclude>**/*.mf</exclude>
                            <!-- 
                                TODO Legacy exclusions we'll probably HAVE to take a look at because
                                they seem WAYYYYY overwide !!
                            -->
                            <exclude>**/squash-ta-integration-test/src/**</exclude>
                            <exclude>**/squash-ta-legacy-integration-test/src/**</exclude>
                            <exclude>**/squash-ta-vcs-interaction-test/src/squashTA/tests/**</exclude>
                            <exclude>**/squash-ta-failure-test/src/**</exclude>
                            <exclude>**/squash-ta-legacy-failure-test/src/**</exclude>
                            <exclude>**/archetype-resources/**</exclude>
                            <exclude>**/squash-ta-maven-plugin/src/main/resources/**</exclude>
                            <!-- 
                                These table-ordering files are plain text files which can only hold a list of tables, one per line. 
                                No comment mechanisme, so no license header.
                            -->
                            <exclude>**/src/squashTA/resources/**/*/table-ordering.txt</exclude>
                            <!-- 
                                These files are plain text field fixed files used for FFF tests. 
                                No comment mechanisme, so no license header.
                                TODO : make sure these are all FFF FOR REAL
                            -->
                            <exclude>**/src/squashTA/resources/**/filechecker/**/*.txt</exclude>
                            <exclude>**/src/squashTA/resources/**/*/FFF_txt_test.txt</exclude>
                            <!-- 
                                This file is a plain text field fixed file which is used for table ordering specifications in the db plugin tests.
                                Here again, no comment mechanisme, so no license header.
                            -->
                            <exclude>plugin/squash-ta-plugin-db/src/test/resources/org/squashtest/ta/plugin/db/converter/CSVDir/table-ordering.txt</exclude>
                            <!--
                                This file is part of a test to check that resource extract for unit tests works proprely.
                                Here again, there is no tool to ignore comments, so no license header.
                            -->      
                            <exclude>api/squash-ta-test-toolkit/src/test/resources/extractor/target-resource.txt</exclude>
                            <!--
                                This file is part of a test to check that the engine loader includes exclusion records properly
                                Here again, there is no tool to ignore comments, so no license header.
                            -->
                            <exclude>core/squash-ta-engine/src/test/resources/org/squashtest/ta/backbone/init/testExcludeFile.txt</exclude>
                            <!--
                                This file is part of a test to check that the engine loader includes exclusion records properly
                                Here again, there is no tool to ignore comments, so no license header.
                            -->
                            <exclude>plugin/squash-ta-plugin-xml-functions/xml-plugin/src/test/resources/expected*Result.xml</exclude>
                            
                            <!-- That one's not ours, so we should keep our snatchy fingers from it ! -->
                            <exclude>**/src/main/resources/html-report/stylesheets/knacss.css</exclude>

                            <!-- Here is a fork, let's manage licenses properly -->
                            <exclude>plugin/squash-ta-plugin-men-xml-checker/**</exclude>
                        </excludes>
                        <mapping combine.children="append">
                            <!-- TODO : map the .macro extensions to manage the header for core-built-in macros -->
			    <xslt>XML_STYLE</xslt>
                        </mapping>
                    </configuration>
                </plugin>
            </plugins>

        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>com.mycila</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <inherited>false</inherited>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <!-- our components -->
        <repository>
            <id>org.squashtest.repo.release</id>
            <name>Squashtest repository for releases</name>
            <url>http://repo.squashtest.org/maven2/release</url>
        </repository>
        <!-- useful when fetching spock 0.6-groovy-1.8 -->
        <repository>
            <id>repo1.maven.org</id>
            <name>Central Repository</name>
            <url>http://repo1.maven.org/maven2/</url>
        </repository>

        <repository>
            <id>eviware.repository</id>
            <url>http://www.soapui.org/repository/maven2</url>
        </repository>
  		
  		<repository>
    		<id>central</id>
    		<url>http://repo1.maven.org/maven2/</url>
  		</repository>
    </repositories>
    <profiles>
       <profile>
          <id>docbuild-linux</id>
          <activation>
              <os>
                  <family>linux</family>
              </os>
              <file>
                  <exists>/usr/bin/sphinx-build</exists>
              </file>
          </activation>
          <build>
            <plugins>
              <plugin>
                  <groupId>org.codehaus.mojo</groupId>
                  <artifactId>exec-maven-plugin</artifactId>
                  <version>1.6.0</version>
                  <inherited>false</inherited>
                  <executions>
                      <execution>
                          <id>doc-clean</id>
                          <goals>
                              <goal>exec</goal>
                          </goals>
                          <phase>clean</phase>
                          <configuration>
                              <basedir>documentation/docs</basedir>
                              <executable>make</executable>
                              <arguments>
                                  <argument>clean</argument>
                              </arguments>
                          </configuration>
                      </execution>
                      <execution>
                          <id>doc-build</id>
                          <goals>
                              <goal>exec</goal>
                          </goals>
                          <phase>compile</phase>
                          <configuration>
                            <basedir>documentation/docs</basedir>
                            <executable>make</executable>
                            <arguments>
                              <argument>html</argument>
                            </arguments>
                          </configuration>
                      </execution>
                      <execution>
                         <id>index-deploy</id>
                         <goals>
                            <goal>exec</goal>
                         </goals>
                         <phase>package</phase>
                         <configuration>
                            <basedir>.</basedir>
                            <executable>cp</executable>
                            <arguments>
                               <argument>src/main/site/index.html</argument>
                               <argument>documentation/docs/_build/index.html</argument>
                            </arguments>
                         </configuration>
                      </execution>
                </executions>
              </plugin>
            </plugins>
          </build>
        </profile>
        <profile>
            <id>add-windows-hg-hooks</id>
            <activation>
                <os>
                    <family>windows</family>
                </os>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.gmaven</groupId>
                        <artifactId>groovy-maven-plugin</artifactId>
                        <version>2.1.1</version>
                        <executions>
                            <execution>
                                <id>add-windows-hg-hooks</id>
                                <goals>
                                    <goal>execute</goal>
                                </goals>
                                <phase>process-resources</phase>
                                <inherited>false</inherited>
                                <configuration>
                                    <source>${project.basedir}/src/main/scripts/AddWindowsGitHooks.groovy</source>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>add-unix-like-hg-hooks</id>
            <activation>
                <os>
                    <family>unix</family>
                </os>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.gmaven</groupId>
                        <artifactId>groovy-maven-plugin</artifactId>
                        <version>2.1.1</version>
                        <executions>
                            <execution>
                                <id>aadd-unix-like-hg-hooks</id>
                                <goals>
                                    <goal>execute</goal>
                                </goals>
                                <phase>process-resources</phase>
                                <inherited>false</inherited>
                                <configuration>
                                    <source>${project.basedir}/src/main/scripts/Add_NixGitHooks.groovy</source>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>self-contained-integration-tests</id>
            <activation>
                <property>
                    <name>!skipSelfContainedIntegrationTests</name>
                </property>
            </activation>
            <modules>
                <module>test</module>
            </modules>
        </profile>
    </profiles>
    <distributionManagement>
        <repository>
          <id>squash-release-deploy-repo</id>
          <name>Squash releases deployment repo</name>
          <url>${deploy-repo.release.url}</url>
        </repository>
        <snapshotRepository>
          <id>squash-snapshot-deploy-repo</id>
          <name>Squash snapshot deployment repo</name>
          <url>${deploy-repo.snapshot.url}</url>
        </snapshotRepository>
    </distributionManagement>
    
</project>

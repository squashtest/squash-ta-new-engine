
import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;

public class SeleniumRConTargetExample extends SeleneseTestCase {
	@Before
	public void setUp() throws Exception {
		//here we lokk for the Selenium Server on port 4444, which is managed by a pseudo-target
		selenium = new DefaultSelenium("localhost", 4444, "*firefox", "http://squash-int:9080/webSUT-1.0.0/webui/textarea.html");
		selenium.start();
	}

	@Test
	public void testSeleniumRConTargetExample() throws Exception {
		selenium.open("/webSUT-1.0.0/webui/textarea.html");
		selenium.type("name=comments", "Toto");
		selenium.click("css=input[type=\"SUBMIT\"]");
		selenium.waitForPageToLoad("30000");
		assertEquals("Toto", selenium.getText("//td[@id='content']/pre"));
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

/**
 * This file was taken from the selenium tutorial
 * TODO Complete disclaimer
 */

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import java.io.File;
import com.csvreader.CsvReader;
import java.nio.charset.Charset;
import java.io.IOException;

public class Selenium2Example {

	@Test
	public void runSeleniumCommands() throws IOException{
		String url=null;
		String targetValue=null;
		/** No *real* use: this is just to demonstrate dependency management for compilation and execution */
		
		CsvReader reader=new CsvReader(getClass().getResourceAsStream("/config.csv"),Charset.forName("utf-8"));
		reader.readHeaders();
		int keyIndex=reader.getIndex("key");
		int valueIndex=reader.getIndex("value");
		reader.readRecord();
		String key=reader.get(keyIndex);
		if("url".equals(key)){
			url=reader.get(valueIndex);
		}else if("targetValue".equals(key)){
			targetValue=reader.get(valueIndex);
		}
		reader.readRecord();
		key=reader.get(keyIndex);
		if("url".equals(key)){
			url=reader.get(valueIndex);
		}else if("targetValue".equals(key)){
			targetValue=reader.get(valueIndex);
		}
		reader.close();
		
		WebDriver driver = new HtmlUnitDriver(true);
		
		driver.get(url);
		WebElement input = driver.findElement(By.name("title"));
		input.sendKeys("Cheese!");
		WebElement form = driver.findElement(By.id("form"));
		form.submit();
		
		System.out.println("Page title is: " + driver.getTitle());
		
		final String targetValueFinal=targetValue;
        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith(targetValueFinal);
            }
        });
        
        System.out.println("Page title is: " + driver.getTitle());
        
        driver.quit();
        
	}
	
}

-- creating target table --
CREATE TABLE IF NOT EXISTS `dbunit_insert_target`(
    `ID` BIGINT NOT NULL,    
    `CONTENT` varchar(100) NULL
)ENGINE=InnoDB;

-- inserting preliminary content --
INSERT INTO `dbunit_insert_target`(`ID`,`CONTENT`) values(1,'First row');
INSERT INTO `dbunit_insert_target`(`ID`,`CONTENT`) values(3,'Second row');
INSERT INTO `dbunit_insert_target`(`ID`,`CONTENT`) values(10,'Third row');
INSERT INTO `dbunit_insert_target`(`ID`,`CONTENT`) values(12,'Fourth row');

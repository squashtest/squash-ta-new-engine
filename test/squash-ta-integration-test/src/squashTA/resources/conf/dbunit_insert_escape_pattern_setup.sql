CREATE TABLE IF NOT EXISTS person (
  id varchar(30) NOT NULL,
  name varchar(30) NOT NULL,
  firstname varchar(30),
  `unique`  boolean,
  PRIMARY KEY  (id)
);

INSERT INTO person (id, name, firstname, `unique`) VALUES
('01', 'Martin', 'Jean', true),
('02', 'Dupont', 'Isabelle', false);
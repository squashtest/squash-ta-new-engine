--
--     This file is part of the Squashtest platform.
--     Copyright (C) 2011 - 2020 Henix
--
--     See the NOTICE file distributed with this work for additional
--     information regarding copyright ownership.
--
--     This is free software: you can redistribute it and/or modify
--     it under the terms of the GNU Lesser General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
--
--     this software is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU Lesser General Public License for more details.
--
--     You should have received a copy of the GNU Lesser General Public License
--     along with this software.  If not, see <http://www.gnu.org/licenses />.
--

DROP SCHEMA IF EXISTS persondb;
CREATE SCHEMA IF NOT EXISTS persondb;

SET INITIAL SCHEMA persondb;

DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS department;

CREATE TABLE IF NOT EXISTS person (
  id varchar(30) NOT NULL,
  name varchar(30) NOT NULL,
  firstname varchar(30),
  secondname varchar(30),
  age varchar(30),
  PRIMARY KEY  (id)
);

INSERT INTO person (id, name, firstname, secondname, age) VALUES
('01', 'Martin', 'Jean', 'Paul', '35'),
('02', 'Dupont', 'Isabelle', 'Lise', '25');
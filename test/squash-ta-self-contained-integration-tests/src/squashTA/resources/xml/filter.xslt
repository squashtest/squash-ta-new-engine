<?xml version="1.0" encoding="utf-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="utf-8"/>
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="root">
        <tronc>
            <xsl:apply-templates />
        </tronc>
    </xsl:template>
    <xsl:template match="limb">
        <branche>
            <xsl:apply-templates />
        </branche>
    </xsl:template>
    <xsl:template match="limb2">
        <xsl:if test="leaf = green">
            <xsl:apply-templates />
        </xsl:if>
    </xsl:template>
    <xsl:template match="leaf">
        <feuille><xsl:value-of select="." /></feuille>
    </xsl:template>
</xsl:stylesheet>
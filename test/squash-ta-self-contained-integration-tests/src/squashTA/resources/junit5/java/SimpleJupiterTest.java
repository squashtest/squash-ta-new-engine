/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.tf.toy.test.project.jupiter;
/**
 * toto!!!
 */
import java.io.File;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeAll;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fgautier
 */
public class SimpleJupiterTest {
    
    public SimpleJupiterTest() {
    }
  
    @BeforeAll
    public static void clean(){
        final File targetFile = new File("target/coucou.txt");
        if(targetFile.delete()){
            LoggerFactory.getLogger(SimpleJupiterTest.class).info("target/coucou.txt deleted pre test");
        }else if(targetFile.exists()){
            throw new RuntimeException("Failed to delete target/coucou.txt");
        }
    }
    
    @Test
    public void jupiterTest() {
        assertEquals(2, 1+1);
    }
    
    @Test
    public void desappointe(){
        fail("Je suis ... DÉSAPPOINTÉ !!!");
    }
    
    @Test
    public void hereMyFriendThereSaRopeLetsHang(){
        throw new RuntimeException("Well, obviously the guys didn't expect that one !");
    }
    
}

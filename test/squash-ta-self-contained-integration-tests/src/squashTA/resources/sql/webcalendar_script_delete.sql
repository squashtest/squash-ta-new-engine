--
--     This file is part of the Squashtest platform.
--     Copyright (C) 2011 - 2020 Henix
--
--     See the NOTICE file distributed with this work for additional
--     information regarding copyright ownership.
--
--     This is free software: you can redistribute it and/or modify
--     it under the terms of the GNU Lesser General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
--
--     this software is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU Lesser General Public License for more details.
--
--     You should have received a copy of the GNU Lesser General Public License
--     along with this software.  If not, see <http://www.gnu.org/licenses />.
--

DELETE from webcal_access_function;
DELETE from webcal_access_user;
DELETE from webcal_asst;
DELETE from webcal_blob;
DELETE from webcal_categories;
DELETE from webcal_config;
DELETE from webcal_entry;
DELETE from webcal_entry_categories;
DELETE from webcal_entry_ext_user;
DELETE from webcal_entry_log;
DELETE from webcal_entry_repeats;
DELETE from webcal_entry_repeats_not;
DELETE from webcal_entry_user;
DELETE from webcal_group;
DELETE from webcal_group_user;
DELETE from webcal_import;
DELETE from webcal_import_data;
DELETE from webcal_nonuser_cals;
DELETE from webcal_reminders;
DELETE from webcal_report;
DELETE from webcal_report_template;
DELETE from webcal_site_extras;
DELETE from webcal_timezones;
DELETE from webcal_user;
DELETE from webcal_user_layers;
DELETE from webcal_user_pref;
DELETE from webcal_user_template;
DELETE from webcal_view;
DELETE from webcal_view_user;
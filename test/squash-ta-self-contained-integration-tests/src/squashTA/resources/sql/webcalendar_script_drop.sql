--
--     This file is part of the Squashtest platform.
--     Copyright (C) 2011 - 2020 Henix
--
--     See the NOTICE file distributed with this work for additional
--     information regarding copyright ownership.
--
--     This is free software: you can redistribute it and/or modify
--     it under the terms of the GNU Lesser General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
--
--     this software is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU Lesser General Public License for more details.
--
--     You should have received a copy of the GNU Lesser General Public License
--     along with this software.  If not, see <http://www.gnu.org/licenses />.
--

DROP TABLE IF EXISTS webcal_access_function;
DROP TABLE IF EXISTS webcal_access_user;
DROP TABLE IF EXISTS webcal_asst;
DROP TABLE IF EXISTS webcal_blob;
DROP TABLE IF EXISTS webcal_categories;
DROP TABLE IF EXISTS webcal_config;
DROP TABLE IF EXISTS webcal_entry;
DROP TABLE IF EXISTS webcal_entry_categories;
DROP TABLE IF EXISTS webcal_entry_ext_user;
DROP TABLE IF EXISTS webcal_entry_log;
DROP TABLE IF EXISTS webcal_entry_repeats;
DROP TABLE IF EXISTS webcal_entry_repeats_not;
DROP TABLE IF EXISTS webcal_entry_user;
DROP TABLE IF EXISTS webcal_group;
DROP TABLE IF EXISTS webcal_group_user;
DROP TABLE IF EXISTS webcal_import;
DROP TABLE IF EXISTS webcal_import_data;
DROP TABLE IF EXISTS webcal_nonuser_cals;
DROP TABLE IF EXISTS webcal_reminders;
DROP TABLE IF EXISTS webcal_report;
DROP TABLE IF EXISTS webcal_report_template;
DROP TABLE IF EXISTS webcal_site_extras;
DROP TABLE IF EXISTS webcal_timezones;
DROP TABLE IF EXISTS webcal_user;
DROP TABLE IF EXISTS webcal_user_layers;
DROP TABLE IF EXISTS webcal_user_pref;
DROP TABLE IF EXISTS webcal_user_template;
DROP TABLE IF EXISTS webcal_view;
DROP TABLE IF EXISTS webcal_view_user;
--
--     This file is part of the Squashtest platform.
--     Copyright (C) 2011 - 2020 Henix
--
--     See the NOTICE file distributed with this work for additional
--     information regarding copyright ownership.
--
--     This is free software: you can redistribute it and/or modify
--     it under the terms of the GNU Lesser General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
--
--     this software is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU Lesser General Public License for more details.
--
--     You should have received a copy of the GNU Lesser General Public License
--     along with this software.  If not, see <http://www.gnu.org/licenses />.
--

INSERT into webcal_user(cal_login, cal_passwd, cal_lastname, cal_firstname, cal_is_admin, cal_email, cal_enabled, cal_telephone, cal_address, cal_title, cal_birthday, cal_last_login) values 
 ('aatienza', '325c4d90e7f5865cd9452582d4d6de84', 'Atienza', 'AurÃ©lie', 'N', 'aatienza@test.com', 'Y', null, null, null, null, null), 
 ('acayol', '6e8c28457f72ba4ce819a09d81704e3d', 'Cayol', 'Annick', 'N', 'acayol@test.com', 'Y', null, null, null, null, null), 
 ('admin', '21232f297a57a5a743894a0e4a801fc3', 'par dÃ©faut', 'Administrateur', 'Y', null, 'Y', null, null, null, null, null);
INSERT into webcal_categories(cat_id, cat_owner, cat_name, cat_color) values 
 (1, null, 'Travail', '#0000FF'), 
 (2, null, 'Loisirs', '#00FF00'), 
 (3, null, 'Sport', '#FF00FF'), 
 (4, null, 'Formation', '#FF0000'), 
 (5, null, 'Autre', '#000000');
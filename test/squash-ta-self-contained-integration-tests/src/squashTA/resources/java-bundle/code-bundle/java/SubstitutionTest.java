/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package selenium;

import java.io.IOException;
import java.io.InputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Properties;

import static org.junit.Assert.*;

public class SubstitutionTest{
    
    @Test
    public void correctInSituSubstitution(){
        assertEquals("from-substitution","${substitution.key}");
    }
    
    @Test
    public void correctPropFileSubstAndAccess() throws IOException{
        Properties prop=new Properties();
        try(InputStream in=getClass().getResourceAsStream("/conf-variabilized.properties")){
            if(in==null){
                throw new RuntimeException("Not found : /conf-variabilized.properties");
            }
            prop.load(in);
            assertEquals("from-substitution",prop.get("variabilized.key"));
        }
    }
    
    @Test
    public void correctPropXmlSubstAndAccess() throws IOException{
        Properties prop=new Properties();
        try(InputStream in=getClass().getResourceAsStream("/variabilized.xml")){
            if(in==null){
                throw new RuntimeException("Not found : /variabilized.xml");
            }
            prop.loadFromXML(in);
            assertEquals("from-substitution",prop.get("variabilized.key"));
        }
    }

    @Test
    public void correctPropXmlSubstAndAccessFromResourceDir() throws IOException{
        Properties prop=new Properties();
        try(InputStream in=getClass().getResourceAsStream("/tagazoc.xml")){
            if(in==null){
                throw new RuntimeException("Not found : /tagazoc.xml");
            }
            prop.loadFromXML(in);
            assertEquals("from-substitution-rsc",prop.get("variabilized.key"));
        }
    }
}
/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest;

import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Selenium1JUnit4WithMvnDependency extends SeleneseTestCase {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(Selenium1JUnit4WithMvnDependency.class);
	
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*chrome", "http://localhost/");
		selenium.start();
	}

	@Test
	public void testSelenium1JUnit4() throws Exception {
		selenium.open("/");
		selenium.click("link=test_robot");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=PageIntro.php");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=mot_de_passe", "henix");
		selenium.click("css=input.sub");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest;

import com.thoughtworks.selenium.*;
import java.util.regex.Pattern;

public class Selenium1JUnit3 extends SeleneseTestCase {
	public void setUp() throws Exception {
		setUp("http://localhost/", "*chrome");
	}
	public void testSelenium1JUnit3() throws Exception {
		//selenium.open("/");
		selenium.open("/test_robot/PageIntro.php");
		//selenium.click("link=test_robot");
		selenium.waitForPageToLoad("30000");
		//selenium.click("link=PageIntro.php");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=mot_de_passe", "henix");
		selenium.click("css=input.sub");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img");
		selenium.waitForPageToLoad("30000");
	}
}

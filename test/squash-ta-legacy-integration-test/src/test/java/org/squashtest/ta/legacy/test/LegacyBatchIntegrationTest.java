/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2011 Squashtest TA, Squashtest.org
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.legacy.test;

import org.squashtest.cats.runner.test.AbstractBatchTestCase;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class LegacyBatchIntegrationTest extends AbstractBatchTestCase{
	@BeforeClass
	public void globalSetup(){
		
	}
	
	@Before
	public void testSetup(){
		
	}
	
	@Test
	public void yesWeEvenHaveATest(){
		
	}
	
	@After
	public void testTeardown(){
		
	}
	
	@AfterClass
	public void globalTearDown(){
		
	}
}

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


##############################################
Introduction to Squash Keyword Framework (SKF)
##############################################

.. contents:: Contents |_|:
    :local:

|

In order to create and maintain your SKF test cases you need several tools. To simplify the installation process, we have bundled these tools into |squashTA| |_| **Toolbox**. The installation of |squashTA| |_| **Toolbox** is described :ref:`here <create.project.toolbox.anchor>`.

In order to *execute* your test cases you need **Squash Keyword Framework**, which is NOT part of the toolbox. No need to install it though as it will be automatically downloaded and installed the first time you will try to run an SKF test (**Squash Keyword Framework** is integrated as a maven plugin, as we will see later on).

|

----

***************************
Automated Project Structure
***************************

An SKF automated project is a **Maven Project** and must have the following structure |_|:

.. figure:: ../_static/introduction-to-skf/squash-ta-structure.jpg

The root of the |squashTF| files is the ``src/squashTA`` directory. It is subdivided in 5 subdirectories |_|:

    * The ``tests`` directory contains SKF test scripts (Files '**.ta**').
    * The ``targets`` directory contains configuration files to define the tested systems (Files '**.properties**').
    * The ``shortcuts`` directory is used to define macros (Files '**.macro**').
    * The ``resources`` directory contains all resources used by test scripts (test data, third party tools, configuration files, ...).
    * The ``repositories`` directory contains definitions of the resources library of the automated project (Files '**.properties**').

'**pom.xml**' (Project Object Model) is the configuration file of a Maven Project.

|

----

*********
Test Case
*********

In an SKF automated project all test cases must be in the ``tests`` directory of the project (or in a subdirectory of this directory). 

A test case is described within a file named SKF script. The extension of the file to use is '**.ta**'. An SKF script represents a test case.

The script names '*setup.ta*' and '*teardown.ta*' are reserved names for the ecosystem setup and teardown phases (see :ref:`here <writing.test.ecosystem.anchor>`).

A test case can contain 3 phases |_|:

    * **A setup phase (optional)** |_|: Used to initiate the test case and to be sure that all necessary conditions to execute the test are gathered.
    * **A test phase (mandatory)** |_|: Contains the different test steps of the test case.
    * **A teardown phase (optional)** |_|: Generally used to clean-up the actions of the test case and so to prepare the environement for the next test case.


|

----

******
Macros
******

Each phase of a test case is comprised of discreet instruction lines.

Most often these instuctions are written as **macros** that contain a sequence of instructions functionally linked.

Macro files all have the extension '**.macro**'. Some are provided with the SKF but you can always write your own custom macros as described :ref:`here <writing.test.macros.anchor>`. User created macros are to be placed in the ``shortcuts`` directory or its subdirectories.

Macros are parametrized for each use by the user with inputs, and depending on the instructions, an output and other resources needed to execute their instructions set.

For example, the following macro is used to execute an SQL query (written on a '**.sql**' file in the ``resources`` directory) on a target database **db**, with the resource **result** defined as the output of the process.

.. figure:: ../_static/introduction-to-skf/example-macro-01.png
    :align: center

In this example the black parts of the macro are fixed and the green parts are parameters filled by the user |_|:

    * The red parameter is an **input**. It is the relative path (to the 'resources' folder) to the '**.sql**' file which will be executed by the macro.

    * The yellow parameter is also an **input**. This resource represents a **database** which will be the target of the SQL script.

    * The blue parameter is the **output**. The resource will contain the result of the **SQL query** written in the SQL script.

|

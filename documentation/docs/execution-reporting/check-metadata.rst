..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. role:: blue
.. role:: red
.. role:: green

.. raw:: html

    <style> .red {color:red} </style>
    <style> .blue {color:blue} </style>
    <style> .green {color:green} </style>

############################
Check TF metadata in project
############################

.. contents:: Contents:
    :local:

************************************************
'check-metadata' goal (squash-ta:check-metadata)
************************************************
As goal **"list"**, the goal **"check-metadata"** searches and checks if all metadata in a SKF project respect the conventions for writing and using |squashTF| metadata (see :ref:`metadata-section` for more information about Metadata syntax conventions).

.. code-block:: shell

     mvn squash-ta:check-metadata

The goal will check through the project, collect all the metadata error(s) if any and lead to a :red:`FAILURE`. Otherwise, a :green:`SUCCESS` result will be obtained (however, no JSON report will be created with a successful **check-metadata** goal).

.. image:: ../_static/execution-reporting-execution/check-metadata-success.png
   :align: center
   :alt: Check-metadata build SUCCESS

Metadata error(s), if found, will be grouped by test names.

.. image:: ../_static/execution-reporting-execution/check-metadata-failure.png
   :align: center
   :alt: Check-metadata build FAILURE

When a SKF project has duplicate values ​​in a multi-value key on a given test, the 'check-metadata' goal will create a **WARNING** message in the console.

.. image:: ../_static/execution-reporting-execution/check-metadata-warning.png
   :scale: 89%
   :align: center
   :alt: Check-metadata build WARNING

|

'check-metadata' goal with Unicity checking
*******************************************
In addition to the normal syntax checking, you can insert the :guilabel:`tf.metadata.check` property after the goal "check-metadata" to check the unicity of each Metadata Key - Value pair.

.. code-block:: shell

     mvn squash-ta:check-metadata -Dtf.metadata.check=[valueUnicity]

If there are metadata Key - Value duplicate(s) in the SKF project (even if the syntax is OK), a :red:`FAILURE` result will be obtained.

.. image:: ../_static/execution-reporting-execution/check-metadata-with-unicity-failure.png
   :align: center
   :alt: Check-metadata with Unicity checking build FAILURE

|

'check-metadata' goal with Unicity checking for specific Keys
*************************************************************
You can even check the unicity of each Metadata Key - Value pair with just some specific Keys by inserting the second property :guilabel:`tf.metadata.check.key` after the first one mentioned above.

.. code-block:: shell

     mvn squash-ta:check-metadata -Dtf.metadata.check=[valueUnicity] -Dtf.metadata.check.keys=[xxx,yyy,zzz]

.. important::

     In the bracket, the key list MUST be a string of characters composed by the concatenation of 1 to n keys separated by commas |_|: -Dtf.metadata.check.keys=[xxx,yyy,zzz]

     If the list is surrounded by double quotes, spaces are allowed |_|: -Dtf.metadata.check.keys="[xxx, yyy, zzz]"

     It is NOT allowed to have two commas without any key OR only spaces/tabulations between them (ex: -Dtf.metadata.check.keys="[xxx, |_| |_| |_|,yyy,,zzz]").

     Key list is NOT allowed to be either uninitiated or empty (ex: -Dtf.metadata.check.keys= OR -Dtf.metadata.check.keys=[]).

For each searched metadata key, if there are Key - Value duplicate(s) in the SKF project, a :red:`FAILURE` result will be obtained.

.. image:: ../_static/execution-reporting-execution/check-metadata-with-unicity-limited-failure.png
   :align: center
   :alt: Check-metadata with Unicity checking for specific Keys build FAILURE

.. note::
   If searched metadata key(s) are not found in any Test files, a **WARNING** message will be raised in the console.

   .. image:: ../_static/execution-reporting-execution/missing-key.png
      :align: center
      :alt: Missing Key

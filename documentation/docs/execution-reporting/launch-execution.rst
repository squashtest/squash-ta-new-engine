..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

*****************
Execute SKF tests
*****************


'run' goal (squash-ta:run)
**************************


The ``run`` goal of the SKF maven plugin is used to execute one or several tests. By default this goal is associated with the ``Integration-test`` phase of the maven build life cycle.

To execute ALL tests, you can use one of the command bellow (in the root directory of your project) |_|::

        mvn integration-test

        mvn squash-ta:run

.. _run.execution.specify.testlist.anchor:

Specify the test list to execute
********************************

The SKF maven plugin defines a maven property which allows to specify the test list to execute. This option is |_|: ``ta.test.suite``.

It’s possible to specify its value by modifying the **pom.xml** of an SKF project |_|:

.. code-block:: xml

      ...
      <modelVersion>4.0.0</modelVersion>
      <groupId>fr.mycompany</groupId>
      <artifactId>my-app-automated-tests</artifactId>
      <packaging>pom</packaging>
      <version>0.0.1-SNAPSHOT</version>
      <properties>
        <ta.test.suite>tc1.txt,tc2.txt</ta.test.suite>
      </properties>
      <build>
      ...



It’s also possible to specify its value in the **command line** |_|:

.. code-block:: shell

   mvn integration-test -Dta.test.suite=tc1.txt,tc2.txt

.. note::

   * If a property is defined in **'pom.xml'** AND via **command line**, the command line value predominates.
   * If a property has an empty value, all tests are executed.

There are many ways to define the test list to execute through ``ta.test.suite`` parameter. More details :ref:`here <run.define.testsuite.perimeter.anchor>`.

|

Manage temporary files
**********************

The SKF maven plugin also defines two maven properties which allow to manage temporary files created during execution.

  * ta.temp.directory |_|: Defines where temporary files should be stored.
  * ta.debug.mode |_|: Defines if temporary files are deleted or not after the execution (its value must be "true" or "false").

Same as ``ta.test.suite``, it’s possible to define them in the **pom.xml**.

**Example**\ |_|\ **:**

.. list-table::

   * - mvn integration-test -Dta.temp.directory=C:\Squash_TF_temp

       mvn integration-test -Dta.debug.mode=true

       mvn integration-test -Dta.temp.directory=C:\Squash_TF_temp -Dta.debug.mode=true

.. note::

        - If a property is defined in **'pom.xml'** AND via **command line**, the command line value predominates.
        - ``ta.temp.directory`` default value is the system temporary directory.
        - ``ta.debug.mode`` default value is “false”.


.. attention::
   If there are syntax-error-metadata in the running test script(s), **warning** message(s) will be displayed in the console.
   (See :ref:`metadata-section` for more information about Metadata syntax conventions)
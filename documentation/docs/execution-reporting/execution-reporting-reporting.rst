..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _execution.reporting.anchor:

#########
Reporting
#########

.. contents:: Contents |_|:
    :local:

*************
Configuration
*************

All configurations are done in the ``<exporters>`` section of the pom.xml of your project.

A report is defined by the ``implementation`` attribute of an ``<exporter>``. Child tags (of <exporter>) are report-dependant.


.. code-block:: xml

    <configuration>
        ...
        <exporters>
            <exporter implementation="org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter">
                <exportAttached>true</exportAttached>
                <jenkinsAttachmentMode>true</jenkinsAttachmentMode>
            </exporter>
            <exporter implementation="org.squashtest.ta.commons.exporter.html.HtmlSuiteResultExporter"></exporter>
        </exporters>
        ...
    </configuration>

|

----

.. _execution.reporting.squash.tf.workflow.anchor:

*************
Report Status
*************

.. list-table::
        :header-rows: 1

        * - Element :
          - Status :
        * - **Instruction :**
          - *  **NOT RUN :**    Instruction was not executed.
            *  **SUCCESS :**    Instruction was successfully executed.
            *  **FAILURE :**    An assertion has failed.
            *  **ERROR :**      An instruction raised an unexpected error
        * - **Test case :**
          - * **NOT_RUN :**      A phase of a test case was not executed.
            * Otherwise its status is the severest one among status of its instructions.
        * - **Script TF :**

            **(Test)**
          - * **NOT_RUN :**     Script was not executed.
            * Otherwise its status is the severest one among status of its instructions.

        * - **Ecosystem :**
          - * Its status is the severest one among status of its scripts.

|

----

***************
SKF HTML Report
***************

HTML Report Configuration
*************************

In our maven archetype, HTML reports are already enabled. Otherwise, you can do it with the following implementation |_|: 

.. code-block:: none

    org.squashtest.ta.commons.exporter.html.HtmlSuiteResultExporter


**HTML report configuration sample** |_|:

.. code-block:: xml

    <configuration>
        ...
        <exporters>
            <exporter implementation="org.squashtest.ta.commons.exporter.html.HtmlSuiteResultExporter"></exporter>
        </exporters>
        ...
    </configuration>

**Since 1.7.0 version, it’s also possible to enable it with the following configuration** |_|:

.. code-block:: xml

    <configuration>
        ...
        <exporters>
            <html/>
        </exporters>
        ...
    </configuration>

|

.. note::

    Both solutions are equivalent and work since 1.7.0.

|

HTML Report overview
********************

The HTML Report (squash-tf-report.html) is created post-execution in directory |_|: ``my.squash.ta.project/target/SquashTA/html-reports``.

Here are some screenshots of an html report |_|:

* **Test suite summary** |_|:

  .. figure:: ../_static/execution-reporting-reporting/suite_summary.png

* **Ecosystem summary** |_|:

  .. figure:: ../_static/execution-reporting-reporting/eco_summary.png

* **Target Initialisation summary** |_|: (@since Squash-TA framework 1.7.0)

  .. figure:: ../_static/execution-reporting-reporting/target.png

* **Test script result** |_|:

  .. figure:: ../_static/execution-reporting-reporting/script_report.png

A full sample is also available here |_|: `Execution report - execution-report-test-sample.pdf <./../_static/execution-reporting-reporting/sample-report.pdf>`_

|

----

************
HTML Summary
************

The HTML Summary is a less detailed version of the HTML report.

Normal HTML Summary
*******************

If you want to have an HTML Summary report, you have to add the following lines in ``<exporters>`` |_|:

.. code-block:: xml

    <exporters>
        <htmlSummary>
            <outputDirectoryName>directoryOfHtmlSummary</outputDirectoryName>
        </htmlSummary>
    </exporters>

.. figure:: ../_static/execution-reporting-reporting/suite_summary_with_header.png

|

Lightened HTML Summary
**********************

You can lighten the report if you add to the ``<htmlSummary>`` configuration the tag |_|:

.. code-block:: xml

     <includeHeader>false</includeHeader>

Which gives |_|:

.. code-block:: xml

    <exporters>
        <htmlSummary>
            <outputDirectoryName>directoryOfHtmlSummary</outputDirectoryName>
            <includeHeader>false</includeHeader>
        </htmlSummary>
    </exporters>

.. figure:: ../_static/execution-reporting-reporting/html_summary_without_header.png

|

.. note:: It's possible to have both **HTML report** and **HTML Summary** together |_|: Put the two tags in <exporters>...</exporters>

  .. code-block:: xml

      <exporters>
        <htmlSummary>
          <outputDirectoryName>directoryOfHtmlSummary</outputDirectoryName>
          <includeHeader>false</includeHeader>
        </htmlSummary>
        <html/>
      </exporters>

|

---------

***************
Surefire Report
***************

Surefire Report Configuration
*****************************

In our maven archetype, Surefire reports are already enabled. Otherwise, you can enable it with the following implementation |_|: 

.. code-block:: none

    org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter

The Surefire report uses 2 properties |_|:

    * ``exportAttached`` |_|: If you want to generate the attachments in the surefire-reports-directory |_|:

        * Default value |_|: true (since 1.7.0).
        * Values accepted |_|: true/false.
    * ``jenkinsAttachmentMode`` |_|: Used with the |squashTF| server for the Jenkins JUnit attachment plugin.


Surefire report configuration sample |_|:

.. code-block:: xml

    <configuration>
        ...
        <exporters>
            <exporter implementation="org.squashtest.tf.commons.exporter.surefire.SurefireSuiteResultExporter">
                <exportAttached>true</exportAttached>
                <jenkinsAttachmentMode>true</jenkinsAttachmentMode>
            </exporter>
        </exporters>
        ...
    </configuration>

Since 1.7.0 version, there is an easy solution to activate the Surefire report |_|:

.. code-block:: xml

    <configuration>
        ...
        <exporters>
            <surefire>
                <exportAttached>true</exportAttached>
                <jenkinsAttachmentMode>true</jenkinsAttachmentMode>
            </surefire>
        </exporters>
        ...
    </configuration>

.. note::

  Both solutions are equivalent and works since SKF 1.7.0.

|

Surefire report overview
************************

A Surefire report is an .xml file which contains all results of the scripts execution.

Each ``<testsuite>`` element represents an ecosystem and each ``<testcase>`` element a .ta script.

Surefire reports are created in directory |_|: ``my.squash.ta.project/target/squashTF/surefire-reports``


.. figure:: ../_static/execution-reporting-reporting/report.jpg

**Here is an example of a Surefire report** |_|:

.. figure:: ../_static/execution-reporting-reporting/surefire_report.png


Three kind of results are possible |_|:

    * **Passed** |_|: In this case the Surefire report provides only the name and the execution time of the test case |_|::

        <testcase time="0.017" classname="tests.F01.ScriptsTF.Set1" name="UC01.01_Success.ta" />

    * **Failed (because of a false assertion)** |_|: In this case the Surefire report provides the name and the execution time of the test case. It also indicates that a **'failure'** occured and gives the associated trace.

    .. code-block:: none

        <testcase time="0.0090" classname="tests.Set1" name="Failure_in_setup.ta" >
          <failure message="The pattern rubbish was not found in the file." 
          type="org.squashtest.ta.framework.exception.BinaryAssertionFailedException" >
        Summary
        SETUP :
        [SUCCESS] LOAD file.txt AS file
        [FAIL] ASSERT file DOES contain WITH $(rubbish)
             [SUCCESS]     $(rubbish) --> {{__temp306}}
             [FAIL] ASSERT file DOES contain WITH {{__temp306}}

        Trace here

        [NOT_RUN] ASSERT file DOES contain WITH $(Hello)
             [NOT_RUN]     $(Hello) --> {{__temp152}}
             [NOT_RUN]     ASSERT file DOES contain WITH {{__temp152}}
        TEST :
        [NOT_RUN] ASSERT file DOES contain WITH $(Hello)
             [NOT_RUN]     $(Hello) --> {{__temp597}}
             [NOT_RUN]     ASSERT file DOES contain WITH {{__temp597}}
        TEARDOWN :
        [SUCCESS] ASSERT file DOES contain WITH $(Hello)
             [SUCCESS]     $(Hello) --> {{__temp529}}
             [SUCCESS]     ASSERT file DOES contain WITH {{__temp529}}
          </failure>
        </testcase>

    * **Failed (because of a technical error)** |_|: In this case the Surefire report provides the name and the execution time of the test case. It also indicates that an **'error'** occurs and give the associated trace.

    .. code-block:: none

        <testcase time="0.013" classname="tests.Set2" name="Error_in_setup.ta" >
          <error message="ASSERT wrongId DOES contain WITH {{__temp238}}: Cannot apply assertion :  
          SCOPE_TEST:wrongId does not exist in this test context: you must load it first." 
          type="org.squashtest.tf.backbone.exception.ResourceNotFoundException" >
        Summary
        SETUP :
        [SUCCESS] LOAD file.txt AS file
        [ERROR] ASSERT wrongId DOES contain WITH $(Hello)
             [SUCCESS]     $(Hello) --> {{__temp238}}
             [ERROR] ASSERT wrongId DOES contain WITH {{__temp238}}

        Trace was here

        [NOT_RUN] ASSERT file DOES contain WITH $(Hello)
             [NOT_RUN]     $(Hello) --> {{__temp362}}
             [NOT_RUN]     ASSERT file DOES contain WITH {{__temp362}}
        TEST :
        [NOT_RUN] ASSERT file DOES contain WITH $(Hello)
             [NOT_RUN]     $(Hello) --> {{__temp345}}
             [NOT_RUN]     ASSERT file DOES contain WITH {{__temp345}}
        TEARDOWN :
        [SUCCESS] ASSERT file DOES contain WITH $(Hello)
             [SUCCESS]     $(Hello) --> {{__temp875}}
             [SUCCESS]     ASSERT file DOES contain WITH {{__temp875}}
          </error>
        </testcase>


Inside the target directory, there are also attached documents produced by SKF scripts to make easier diagnosis when an error occurs (snapshots, diff-reports, detailed logs...).

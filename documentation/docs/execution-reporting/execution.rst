..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


#######################
Execution and Reporting
#######################

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Logging <logging.rst>
    Run tests <run.rst>
    List tests <list.rst>
    Check TF metadata <check-metadata.rst>

SKF is a maven plugin. So an SKF automation project is a maven project. You'll need **maven** and a **jdk** installed on your system.

To execute your tests, list your tests, etc ... , you will have to use the maven goals we create to handle them. Currently 3 goals are defined |_|:

* ``run`` |_|: this goal handles the execution of your tests.
* ``list`` |_|: this goal handles the listing of your tests.
* ``check-metadata`` |_|: this goal handles some checks on TF metadata in SKF test scripts.

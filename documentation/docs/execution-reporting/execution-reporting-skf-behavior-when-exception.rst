..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.




########################################################
SKF behaviour if an exception is raised during execution
########################################################

|

.. list-table::
        :header-rows: 1

        * - Phases |_|:
          - SKF bevahiour if an exception⁽¹⁾ is raised |_|:

        * - **Ecosystem**

            **SETUP**

            **(setup.ta)**

          - **General behaviour :**

            * Stops the execution of this setup.ta script.

            * Launches the teardown.ta script.
            
            **Using VERIFY⁽²⁾ instruction :**      

            * Executes the next instruction in the setup.ta script.

            * When setup.ta script is finished, launches test cases of the ecosystem.

        * - **Test case**

            **SETUP**

          - **General behaviour :**

            * Stops the execution of this test case setup phase.

            * Launches the test case teardown phase.

            * When test case teardown phase is finished, launches next SKF script⁽³⁾.

            **Using VERIFY⁽²⁾ instruction :**

            * Executes the next instruction in this test case setup phase.

            * When test case setup phase is finished, launches test case test phase.

        * - **Test case**

            **TEST**

          - **General behaviour :**

            * Stops the execution of this test case test phase.

            * Launches the test case teardown phase.

            * When test case teardown phase is finished, launches next SKF script⁽³⁾.

            **Using VERIFY⁽²⁾ instruction :**

            * Executes the next instruction in this test case test phase.

            * When test case test phase is finished, launches test case teardown phase.

        * - **Test case**

            **TEARDOWN**

          - **General behaviour :**

            * Stops the execution of this test case teardown phase.

            * Launches the next SKF script (test case or teardown.ta).

            **Using VERIFY⁽²⁾ instruction:**

            * Executes the next instruction in this test case teardown phase.

            * When test case teardown phase is finished, launches the next SKF script⁽³⁾.

        * - **Ecosystem**

            **TEARDOWN**

            **(teardown.ta)**

          - **General behaviour :**

            * Stops the execution of this teardown.tf script.

            * Launches the next ecosystem.

            **Using VERIFY⁽²⁾ instruction:**

            * Executes the next instruction in this teardown.ta script.

            * When this teardown.ta Script is finished, launches the next ecosystem.


⁽¹⁾ Exception could be an assertion **failure** or an unexpected **error**.

⁽²⁾ For more information about VERIFY instruction, please check the following :ref:`page <assertion.instruction.anchor>`.

⁽³⁾ test case or teardown.ta


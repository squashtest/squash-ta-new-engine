..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. role:: red
.. role:: green

.. raw:: html

    <style> .red {color:red} </style>
    <style> .green {color:green} </style>

############################
List tests in an SKF project
############################

.. contents:: Contents |_|:
    :local:

'list' goal (squash-ta:list)
****************************

The  ``list`` goal generates a json file representing the test tree of the current project. To generate this list, run at the root of your project (where the pom.xml of your project is located) the command |_|:

.. code-block:: shell

     mvn squash-ta:list

The generated json file is named ``testTree.json`` and is created in ``<root_project_path>/target/squashTF/test-tree`` directory.

Listing test JSON report
========================

.. code-block:: json

   {
     "timestamp": "2014-06-17T09:48:19.733+0000",
     "name": "tests",
     "contents": [
       {
         "name": "sample",
         "contents": [
           {
             "name": "test-OK.tf",
             "contents": null
           }
         ]
       },
       {
         "name": "sample2",
         "contents": [
           {
             "name": "placeHolder.tf",
             "contents": null
           },
           {
             "name": "test-OK.tf",
             "contents": null
           }
         ]
       },
       {
         "name": "placeHolder.tf",
         "contents": null
       },
       {
         "name": "test-KO-db-verification.tf",
         "contents": null
       },
       {
         "name": "test-KO-sahi.tf",
         "contents": null
       },
       {
         "name": "test-OK.tf",
         "contents": null
       }
     ]
   }

|

'list' goal with Metadata
*************************
If there are Squash metadata in the current test project, the goal **"list"** searches and checks if all metadata in a SKF project respect the conventions for writing and using |squashTF| metadata. (See :ref:`metadata-section` for more information about Metadata syntax conventions)

The goal will check through the project, collect all the metadata error(s) if any and lead to a :red:`FAILURE`. Otherwise, a :green:`SUCCESS` result will be obtained.

.. image:: ../_static/execution-reporting-execution/list-success.png
   :align: center
   :alt: List build SUCCESS

Metadata error(s), if found, will be grouped by test names.

.. image:: ../_static/execution-reporting-execution/list-failure.png
   :align: center
   :alt: List build FAILURE

|

Listing test JSON report with Metadata
======================================
If the build is successful, the generated report (JSON file) will contain the metadata associated with each of the test scripts.

.. code-block:: json

   {
     "timestamp": "2014-06-17T09:48:19.733+0000",
     "name": "tests",
     "contents": [
       {
         "name": "sample",
         "metadata" : {},
         "contents": [
           {
             "name": "test-OK.ta",
             "metadata" : {
               "linked-TC": ["guid-1", "guid-2"],
               "key2": null,
               "key3": ["value"]
             },
             "contents": null
           }
         ]
       },
       {
         "name": "test-KO.ta",
         "metadata" : {},
         "contents": null
       }
     ]
   }

|

Disable Metadata when test listing
==================================
If there are Metadata in your project but you want to ignore them during the project test listing, then insert :guilabel:`tf.disableMetadata` property after the goal "list"

.. code-block:: shell

     mvn squash-ta:list -Dtf.disableMetadata=true
	 
or as a property in the **pom.xml** file
   
.. code-block:: xml

   <properties>
      <tf.disableMetadata>true</tf.disableMetadata>
   </properties>

The generated report (JSON file) will then NO LONGER contain the metadata.

.. code-block:: json

   {
     "timestamp": "2014-06-17T09:48:19.733+0000",
     "name": "tests",
     "contents": [
       {
         "name": "sample",
         "contents": [
           {
             "name": "test-OK.ta",
             "contents": null
           }
         ]
       },
       {
         "name": "test-KO.ta",
         "contents": null
       }
     ]
   }

|

.. note:: SKF has also a deprecated ``test-list`` goal. It generates the test list in the console / log and through the exporters configured in pom.xml (html, surefire)

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _execution.reporting.context.parameters.anchor:

##################
Context Parameters
##################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros
.. role:: phases
.. role:: comments

.. contents:: Contents |_|:
    :local:

The purpose of context parameters is |_|:

    * To provide a list of key/value through :ref:`json data <reporting.testsuite.json.data.anchor>` (at script or at global level).

        **For example** |_|:

             .. code-block:: none

                {
                "test" : [{
                        "script" : "pathToMyscript1",
                        "param" : {                              // Script context parameters
                            "my_cuf" : "value1",
                            "property2" : "value2"
                        }
                    }
                ],
                "param" : {                                      // Global context parameters
                    "property2" : "value13",
                    "property6" : "value14"
                }

    * To transform the parameters as a properties resource and then use it in test script through :ref:`file to file (using param) converter <libraries.common.component.converters.from.file.to.file.using.param.anchor>` (in :keywords:`USING` clause).

            * For script context parameters the resource is available in the test with |_|: ``context_script_params``.

            * For global context parameters the resource is available in the test with |_|: ``context_global_params``.
            

In the sample below, in processedCommandFile, ${my_cuf} is replaced by "value1" |_|:

.. code-block:: none

    {
        "test" : [{
                "script" : "pathToMyscript1",              
                "param" : {                                         
                    "my_cuf" : "value1",
                    "property2" : "value2"
                }
            }
        ],
        "param" : {                                                 
            "property2" : "value13",
            "property6" : "value14"
        }
    }

.. list-table::

    * - :keywords:`DEFINE` $(monShell.sh -param1=${my_cuf}) :keywords:`AS` :resources:`commandFile`

        :keywords:`CONVERT` :resources:`commandFile` :keywords:`TO` :resource-type:`file` (:converter-name:`param`) :keywords:`USING` :resources:`context_script_params` :keywords:`AS` :resources:`processedCommandFile`
    
        :keywords:`CONVERT` :resources:`processedCommandFile` :keywords:`TO` :resource-type:`query.shell` (:converter-name:`query`) :keywords:`AS` :resources:`commandLine`

        :keywords:`EXECUTE` :converter-name:`local` :keywords:`WITH` :resources:`commandLine` :keywords:`AS` :resources:`result`

``context_script_params`` and ``context_global_params`` can be used together but be wary of multiple definitions of the same parameter. 

Only the latest parameter sent will be used.

**For example** |_|:

.. code-block:: none

    {
        "test" : [{
                "script" : "pathToMyscript1",              
                "param" : {                                         
                    "my_cuf" : "value1",
                    "property2" : "value2"
                }
            }
        ],
        "param" : {                                                 
            "property2" : "value13",
            "property6" : "value14"
        }
    }

If you send the parameters in the following order |_|:

.. list-table::

    * - :keywords:`CONVERT` :resources:`xmlResource` :keywords:`TO` :resource-type:`file` (:converter-name:`param`) :keywords:`USING` :resources:`context_global_params`, :resources:`context_script_params` :keywords:`AS` :resources:`convertedXml`

Then |_|: **property2** will be replaced by **value2**.

On the other hand, if you send them in the reverse order |_|:

.. list-table::

    * - :keywords:`CONVERT` :resources:`xmlResource` :keywords:`TO` :resource-type:`file` (:converter-name:`param`) :keywords:`USING` :resources:`context_script_params`, :resources:`context_global_params` :keywords:`AS` :resources:`convertedXml`

Then |_|: **property2** will be replaced by **value13**.

.. note::

    The framework doesn't prevent you from defining your own SKF resource with those context names. If you ever do it, your context parameters will be overwritten (and a warn is logged).
    
In the sample below, ``context_script_params`` corresponds to ``sample.properties`` |_|:

.. list-table::

    * - :keywords:`LOAD` sample.properties :keywords:`AS` :resources:`sample.file`

        :keywords:`CONVERT` :resources:`sample.file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`context_script_params`

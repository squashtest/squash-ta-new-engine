..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _run.define.testsuite.perimeter.anchor:

###########################
Define Test suite Perimeter
###########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros
.. role:: phases
.. role:: comments

.. contents:: Contents |_|:
    :local:

When you execute your test through the ``run`` goal, you define the test suite to execute through the ``ta.test.suite`` parameter (more details on the ``run`` goal :ref:`here <run.execution.specify.testlist.anchor>`).

You can define your test suite by providing to ``ta.test.suite`` **filters** (for filtered execution) or **data structured in .json** (for filtered or ordedered execution).

************************
"ta.test.suite": filters
************************

Definition
**********

The ``ta.test.suite`` parameter can be a list of filters separated by comma.

A filter can be |_|:

* The test case path (absolute or relative to the automation project "tests" directory).

  .. list-table::

    * - The file path :keywords:`D:/myProject/src/squashTA/tests/foo/bar/baz.ta` can be matched as :

          - D:/myProject/src/squashTA/tests/foo/bar/baz.ta
          - foo/bar/baz.ta

* A path using a wildcard characters which selects matching test script inside "tests" directory. Wildcard characters can be used |_|:

  * ``**`` to replace directories path (one or many levels).
  * ``*`` to replace 0,1 or many characters.

  .. list-table::

    * - Examples of file paths matching foo/bar/baz.ta using wildcard characters :

          - \**/baz.ta
          - \**/bar/baz.ta
          - foo/\**/baz.ta
          - f\*o/b\*/baz.ta
          - \**/b*z.ta
          - etc.

* A regular expression, using regex'<myRegex>' format, which selects matching test script inside "tests" directory.

  .. list-table::

     * - regex'<regular_expression>'

           **regular_expression** : The regular expression to use to select tests.

Usage
*****

In the example below, ta.test.suite is composed of two filters |_|:

.. code-block:: shell

   "foo/bar/baz.ta"

will select for execution foo/bar/baz.ta file in "tests" directory (if it exists).

.. code-block:: shell

   "sample/**/*.ta"

will select for execution all files in "tests/sample" directory and its subdirectories which name finish by ".ta".

.. code-block:: shell

   mvn squash-tf:run -Dta.test.suite=foo/bar/baz.tf,sample/**/*.ta

|

----

.. _reporting.testsuite.json.data.anchor:

**************************
"ta.test.suite": json data
**************************

Through json data you can do a filtered execution or an ordered execution.

Filtered execution
******************

In the json data you can provide filters (as defined in the previous section) by using the syntax below |_|:

.. code-block:: json

  {
    "filter" : "**/*.ta"
  }

In addition, you can provide some global parameters |_|:

.. code-block:: json

  {
    "filter" : "**/*.ta",
    "param" : {
      "property5" : "value13",
      "property6" : "value14"
    }
  }

|

Ordered execution
*****************

The other possibility, in json format, is to provide the list of tests to execute |_|:

// Path to the test script to execute

// Test execution identifier

// Script parameters


.. list-table::

  * - .. code-block:: json

        {
          "test": [
            {
              "script": "pathToMyscript1",
              "id": "TestCase1",
              "param": {
                "property1": "value1",
                "property2": "value2"
              }
            },
            {
              "script": "pathToMyscript2",
              "id": "TestCase2",
              "param": {
                "property3": "value7",
                "property4": "value8"
              }
            },
            {
              "script": "pathToMyscript1",
              "id": "TestCase3",
              "param": {
                "property1": "value3",
                "property2": "value4"
              }
            }
          ],
          "param": {
            "property5": "value13",
            "property6": "value14"
          }
        }

    - .. code-block:: none

        -
        -
        -
        <---- Path to the test script to execute
        <---- Test execution identifier
        <---- Script parameters
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        -
        <---- Global parameters
        -
        -
        -
        -

Where for each test |_|:

  * ``script`` is the the path to the test to execute relative to the "tests" directory. This property is mandatory.
  * ``id`` is the test execution identifier. Only useful for |squashTM| |_| - |_| |squashTF| link. However, if "id" is defined for one test then it should be defined for all tests of test suite.
  * ``param`` is a list of parameters (key/value) associated to the test. This property is optional.

As for json filtered execution, global parameters are optionals.

When no ``param`` and ``id`` properties are defined for test, it's possible to use a simplier syntax |_|:

.. code-block:: json

  {
    "test": [
      "script1Path",
      "script2Path",
      "script3Path"
    ],
    "param": {
      "property5": "value13",
      "property6": "value14"
    }
  }

|

Usage
*****

Json data can be provided through a String or a file.


    **> Json provided through a String**

      .. code-block:: shell

        mvn squash-ta:run -Dta.test.suite={'test':[{'script':'pathToMyscript1','param':{'property1':'value1','property2':'value2'}},{'script':'pathToMyscript2'}]}

      .. Note:: Note that the double quote surrounding properties and values of the json data has been replaced. You can |_|:

                  * replace them by simple quote (as it's done in the example)
                  * escape the double quote \\"

|

    **> Json provided through a file**
        
      .. code-block:: shell

         mvn squash-ta:run -Dta.test.suite={file:pathToMyJsonFile}

      Where *pathToMyJsonFile* is the path to the json data file. This path can be absolute or relative to the root directory of the automation project.

|

----

***************************************
Filtered execution vs Ordered execution
***************************************

Filtered execution
******************

When you do a filtered execution you provide filters. The list of test to execute is composed of all the tests in "tests" directory whose path matches the filter. With this kind of execution |_|:

    * A test can only be executed once during an execution.

    * There is no execution order.

    * You can't provide specific parameters to the script however you can provide global parameters through json data.

Ordered execution
*****************

When you do an ordered execution you provide the list of tests to execute through json format. With this kind of execution |_|:

    * A test can be executed as many times as needed.

    * The tests are executed in the order that they were declared in the json data **if the tests are in the same ecosystem**. If the tests are not in the same ecosystem they are executed ecosystem by ecosystem. That means we execute all the tests of the first ecosystem used (in the order they are declared) then the tests of the second ecosytem are executed, etc.

      **Given** the test tree below |_|:

      .. code-block:: shell

            tests
              |--foo
              |    |--test21.ta
              |    `--test22.ta
              |--test01.ta
              `--test02.ta

      **When** this json data is given as input |_|:

      .. code-block:: json

         {
           "test" : [{
             "script" : "foo/test22.ta"
           }, {
             "script" : "test01.ta"
           }, {
             "script" : "test02.ta"
           }, {
             "script" : "foo/test21.ta"
           }]
         }

      **Then**\ |_|\ **:** test22.ta and test21.ta of ecosystem foo will be executed, then test01.ta and test02.ta will be executed.

    * Parameters can be specified for each test.


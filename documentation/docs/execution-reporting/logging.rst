..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _logging.anchor:

#######
Logging
#######

We recommand to patch your maven by using the procedure below for a better logging with our runners |_|:

.. Note:: In all the procedure ``$MVN_HOME`` is your maven installation directory, and ``$MVN_VERSION`` your maven version.

* Add in ``$MVN_HOME/lib/ext/`` the jars |_|:

  * `log4j-slf4j-impl-2.5.jar <http://central.maven.org/maven2/org/apache/logging/log4j/log4j-slf4j-impl/2.5/log4j-slf4j-impl-2.5.jar>`_
  * `log4j-core2.5.jar <http://central.maven.org/maven2/org/apache/logging/log4j/log4j-core/2.5/log4j-core-2.5.jar>`_
  * `log4j-api-2.5.jar <http://central.maven.org/maven2/org/apache/logging/log4j/log4j-api/2.5/log4j-api-2.5.jar>`_


- Create a logging configuration file called ``log4j2.xml`` in ``$MVN_HOME/conf/logging/`` and fill it with |_|:

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8" ?>
    <Configuration>
      <Properties>
        <Property name="maven.logging.root.level">INFO</Property>
      </Properties>
      <Appenders>
        <Console name="console" target="SYSTEM_OUT">
          <PatternLayout pattern="[%p] %msg%n%throwable" />
        </Console>
      </Appenders>
      <Loggers>
        <Root level="${sys:maven.logging.root.level}">
          <Appender-ref ref="console"/>
        </Root>
    <!-- <logger name="[USER_MESSAGE]" level="DEBUG"/> -->
      </Loggers>
    </Configuration>

* Remove if exists |_|:

  * In the directory ``$MVN_HOME/lib`` the file ``maven-sl4j-provider-$MVN_VERSION.jar``
  * In the directory ``$MVN_HOME/conf/logging/`` the file ``deletesimpleLogger.properties``



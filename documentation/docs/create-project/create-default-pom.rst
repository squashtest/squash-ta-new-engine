..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################################
Default SKF automation project pom.xml
######################################

|

Default pom
***********

Here is an example of SKF‘s default pom. It’s the one generated with the maven archetype (for more information, please consult the page about :ref:`creating a new project <getting.started.create.new.project.anchor>`).

.. code-block:: xml

    <project
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
        xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

        <modelVersion>4.0.0</modelVersion>

        <groupId>squash-project</groupId>
        <artifactId>test</artifactId>
        <packaging>pom</packaging>
        <version>1.0-SNAPSHOT</version>

        <!-- Properties definition -->
        <properties>
            <!-- Squash-TA framework version used by the project -->
            <ta.framework.version>1.12.0-RELEASE</ta.framework.version>
        </properties>

        <build>
            <plugins>
                <!-- Configuration of the Squash TA framework used by the project -->
                <plugin>
                    <groupId>org.squashtest.ta</groupId>
                    <artifactId>squash-ta-maven-plugin</artifactId>
                    <version>${ta.framework.version}</version>

                    <!-- Here you can add libraries to the engine classpath, using the <dependencies></dependencies> tag -->
                    <!-- A sample with the mySql jdbc driver -->
                    <!-- <dependencies> -->
                    <!--     <dependency> -->
                    <!--        <groupId>mysql</groupId> -->
                    <!--        <artifactId>mysql-connector-java</artifactId> -->
                    <!--        <version>5.1.19</version> -->
                    <!--     </dependency> -->
                    <!-- </dependencies> -->

                    <!-- Under here is the Squash TA framework default configuration -->
                    <configuration>

                        <!--
                            Uncomment the line below in order to the build finish in success even if a test failed
                            (functional (assertion) failure), but fail the build if an ERROR (technical failure) occurred.
                        -->
                        <!-- <mojoSuccessThreshold>FAIL</mojoSuccessThreshold> -->

                        <!-- Define a log configuration file (at log4j format) to override the one defined internally -->
                        <!-- If the given file can't be found the engine switch to the internal configuration-->
                        <logConfiguration>${logConfFile}</logConfiguration>

                        <!-- Define exporters -->
                        <exporters>
                            <surefire>
                                <jenkinsAttachmentMode>${ta.jenkins.attachment.mode}</jenkinsAttachmentMode>
                            </surefire>
                            <html/>
                        </exporters>

                        <!-- Define configurers -->
                        <configurers>
                            <tmCallBack>
                                <endpointURL>${status.update.events.url}</endpointURL>
                                <executionExternalId>${squash.ta.external.id}</executionExternalId>
                                <jobName>${jobname}</jobName>
                                <hostName>${hostname}</hostName>
                                <endpointLoginConfFile>${squash.ta.conf.file}</endpointLoginConfFile>
                                <reportBaseUrl>${ta.tmcallback.reportbaseurl}</reportBaseUrl>
                                <jobExecutionId>${ta.tmcallback.jobexecutionid}</jobExecutionId>
                                <reportName>${ta.tmcallback.reportname}</reportName>
                            </tmCallBack>
                        </configurers>
                    </configuration>

                    <!-- Bind the Squash TA "run" goal to the maven integration-test phase and reuse the default configuration -->
                    <executions>
                        <execution>
                            <goals>
                                <goal>run</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>

        <!-- Squash TA maven repository -->
        <repositories>
            <repository>
                <id>org.squashtest.ta.release</id>
                <name>squashtest test automation - releases</name>
                <url>http://repo.squashtest.org/maven2/releases</url>
            </repository>
        </repositories>

        <!-- Squash TA maven plugin repository -->
        <pluginRepositories>
            <pluginRepository>
                <id>org.squashtest.plugins.release</id>
                <name>squashtest.org</name>
                <url>http://repo.squashtest.org/maven2/releases</url>
                <snapshots>
                    <enabled>false</enabled>
                </snapshots>
                <releases>
                    <enabled>true</enabled>
                </releases>
            </pluginRepository>
        </pluginRepositories>

    </project>


By default, the following configuration has been set |_|:

    * Generation of an html report at the end of the execution.
    * Generation of Junit reports at the end of execution with attachment and jenkinsAttachmentMode deactivated.
    * |squashTM| events callback for **Squash** |_| **TF-TM** link is declared but deactivated.

|

----------

<exporters>
***********

For more information on the |squashTF| exporters configuration, please read :ref:`this <execution.reporting.anchor>`.

|

----------

<configurer>
************

Currently, there is only one configurer existing in SKF. It is used to configure |squashTM| events callback for the **TF-TM** link. When activated, this component send progression events to |squashTM| during the execution of a test suite. If you don't use **TF-TM** link, then you don't need this configurer.

To declare the |squashTM| events callback in your project |_|:

.. code-block:: xml

    <configuration>
      ...
      <configurers>
        <tmCallBack>
          <endpointURL>${status.update.events.url}</endpointURL>
          <executionExternalId>${squash.ta.external.id}</executionExternalId>
          <jobName>${jobname}</jobName>
          <hostName>${hostname}</hostName>
          <endpointLoginConfFile>${squash.ta.conf.file}</endpointLoginConfFile>
          <reportBaseUrl>${ta.tmcallback.reportbaseurl}</reportBaseUrl>
          <jobExecutionId>${ta.tmcallback.jobexecutionid}</jobExecutionId>
          <reportName>${ta.tmcallback.reportname}</reportName>
        </tmCallBack>
      </configurers>
      ...
    </configuration>


For automation project using a version before **1.7.0**, you have to use |_|:

.. code-block:: xml

    <configuration>
      ...
      <configurers>
        <configurer implementation="org.squashtest.ta.link.SquashTMCallbackEventConfigurer">
          <endpointURL>${status.update.events.url}</endpointURL>
          <executionExternalId>${squash.ta.external.id}</executionExternalId>
          <jobName>${jobname}</jobName>
          <hostName>${hostname}</hostName>
          <endpointLoginConfFile>${squash.ta.conf.file}</endpointLoginConfFile>
          <reportBaseUrl>${ta.tmcallback.reportbaseurl}</reportBaseUrl>
          <jobExecutionId>${ta.tmcallback.jobexecutionid}</jobExecutionId>
          <reportName>${ta.tmcallback.reportname}</reportName>
        </configurer>
      </configurers>
      ...
    </configuration>

.. note::

  Since |squashTA| |_| **1.7.0**, the endpointURL has a default value |_|: file://dev/null. Moreover this default value has for effect to deactivate the send event mecanism. A valid URL should be given to activate it.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


###################################################
Create a Squash |_| TF Project using a command line
###################################################

|

You can create the |squashTF| project with a maven command-line, then import it into your preferred IDE.

Open a shell window at the location where you want your project created and execute the following command line |_|:

.. code-block:: none

    mvn archetype:generate -DarchetypeGroupId=org.squashtest.ta -DarchetypeArtifactId=squash-ta-project-archetype -DarchetypeVersion={SKF version} -DarchetypeRepository=http://repo.squashtest.org/maven2/releases

**Note** |_|: Do not forget to replace the **{SKF version}** with the last version of the Squash Keyword Framework. You can check it on our `website <https://www.squashtest.com/telechargements>`_
 
    * At the prompt, enter the desired **groupId** (in our example, **org.squashtest.tutorial**).
    * Next, enter the desired **artifactId** (here, **my.squash.ta.project**).
    * Next, enter the **version** (here, **1.0**).
    * Skip the next, irrelevant, prompt about java packages and you can check the parameters and confirm them with **y** if it's OK.

    .. figure:: ../_static/create-project/config-ta-eclipse-new-project-CLI-validate.png
 
After the execution of the archetype, you should have the following |_|:
 
    .. figure:: ../_static/create-project/config-ta-eclipse-new-project-CLI-build-success.png
 
Now, you can close the shell window and import the project into your IDE (Eclipse in the following example) |_|:
 
    * Select menu **File > Import...**, then **Maven > Existing Maven Projects**. Click on **Browse** and go to the newly created project directory (in our example C:\\Workspace\\my.squash.tf.project) |_|:
 
        .. figure:: ../_static/create-project/config-ta-eclipse-new-project-CLI-import-01.png
 
        .. figure:: ../_static/create-project/config-ta-eclipse-new-project-CLI-import-02.png
 
    * Click on **OK**, then **Finish** to complete the project import operation |_|:
 
        .. figure:: ../_static/create-project/config-ta-eclipse-new-project-CLI-import-03.png
 
    * The newly created Test project now appears in the TA Navigator |_|:
 
        .. figure:: ../_static/create-project/config-ta-eclipse-new-project-CLI-import-04.png

|

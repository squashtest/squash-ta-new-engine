..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


#########################################
Create a |squashTF| Project with IntelliJ
#########################################

.. contents:: Contents |_|:
    :local:

|

**************
Pre-requisites
**************

If you are using a Maven |_| > |_| 3.0.0, you’ll need to add the following section to your **settings.xml** (located in the **conf** directory of your Maven folder, or in **\\plugins\\maven\\lib\\maven3\\conf** in your IntelliJ directory for the bundled version) |_|: ::

    <profiles>
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>archetype</id>
                    <name>squashTA</name>
                    <url>http://repo.squashtest.org/maven2/releases/</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>squashTA</id>
                    <name>squashTA</name>
                    <url>http://repo.squashtest.org/maven2/releases/</url>
                </pluginRepository>
            </pluginRepositories>
            <id>squashTA</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>squashTA</activeProfile>
    </activeProfiles>

.. figure:: ../_static/create-project/create-project-intellij-maven-config.png

Add the following to your file  |_|::

    <?xml version="1.0" encoding="UTF-8"?>
    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

        <profiles>
            <profile>
                <repositories>
                    <repository>
                        <snapshots>
                            <enabled>false</enabled>
                        </snapshots>
                        <id>archetype</id>
                        <name>squashTA</name>
                        <url>http://repo.squashtest.org/maven2/releases/</url>
                    </repository>
                </repositories>
                <pluginRepositories>
                    <pluginRepository>
                        <snapshots>
                            <enabled>false</enabled>
                        </snapshots>
                        <id>squashTA</id>
                        <name>squashTA</name>
                        <url>http://repo.squashtest.org/maven2/releases/</url>
                    </pluginRepository>
                </pluginRepositories>
                <id>squashTA</id>
            </profile>
        </profiles>
        <activeProfiles>
            <activeProfile>squashTA</activeProfile>
        </activeProfiles>
    </settings>

.. figure:: ../_static/create-project/create-project-intellij-bundled-maven-config.png

|

----

***********************************
Creating your Squash |_| TF project
***********************************

Launch IntelliJ IDEA then click on **“Create New Project"**  |_|:

.. figure:: ../_static/create-project/create-new-project-intellij.png

Select in the left column the Maven type then check the **“Create from archetype”** box.

Click on **“Add Archetype...”** to add the one needed.

.. figure:: ../_static/create-project/create-project-intellij-add-archetype.png

Insert the following informations :

    * **GroupId :** org.squashtest.ta
    * **ArtifactId :** squash-ta-project-archetype
    * **Version :** You can check the last version of the Squash Keyword Framework on our `website <https://www.squashtest.com/telechargements>`_
    * **Repository :** \http://repo.squashtest.org/maven2/releases (Only if your Maven |_| is |_| < |_| 3.0.0)

.. figure:: ../_static/create-project/create-project-intellij-archetype-info.png

Select the newly created archetype and click on **“Next”**.

.. figure:: ../_static/create-project/create-project-intellij-add-archetype-2.png

Insert your **groupId**, **ArtifactId**, **Version**, and click on **"Next"**.

.. figure:: ../_static/create-project/create-new-project-intellij-2.png

Select your Maven. It should be the one with the repository in the **settings.xml**. Click on **"Next"**.

.. figure:: ../_static/create-project/create-project-intellij-select-maven.png

Select a project name and location, and click on **"Finish"**.

.. figure:: ../_static/create-project/create-new-project-intellij-3.png

You may need to wait a little bit.

You should have a build success and the following structure :

.. figure:: ../_static/create-project/create-new-project-intellij-4.png

|

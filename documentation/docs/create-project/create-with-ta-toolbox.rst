..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _create.project.toolbox.anchor:

#####################################################
Create a Squash |_| TF Project with Squash TA Toolbox
#####################################################

.. contents:: Contents |_|:
    :local:

|

**************
Pre-requisites
**************

You need to have the |squashTA| |_| **toolbox** installed on your workstation. The toolbox is packaged with Eclipse and the m2eclipse plugin.

You can download and install it, as described `here <https://squash-tf.readthedocs.io/en/latest/development-tools/squash-ta-toolbox/squash-ta-toolbox-install.html>`_.

|

***********************************
Creating your Squash |_| TF project
***********************************

Let's start a dedicated Eclipse instance by clicking on the **"Squash-TA Eclipse"** icon on your desktop.

Confirm that you want to use your newly created workspace.

It is important to use this workspace because it contains specific archetypes for your projects and run configurations for your test scripts.

.. figure:: ../_static/create-project/config-ta-eclipse-workspace-selection.png


When Eclipse is started, we will create a new |squashTF| project as follows |_|:

    * Select **File > New >  Maven Project** |_|:

    .. figure:: ../_static/create-project/config-ta-eclipse-new-maven-project-01.png

    * In the "New Maven Project" dialog window that opens, click on **Next** |_|:

    .. figure:: ../_static/create-project/config-ta-eclipse-new-maven-project-02.png

    * Select the Squash-TF project maven archetype with Artifact id **squash-ta-project-archetype** |_|:

        .. figure:: ../_static/create-project/config-ta-eclipse-Archetype-choice.png

    * If you can't find this archetype or wish to create your own you can instead add it from scratch as follows |_|:
    
        * Click on the **Add Archetype** button |_|:

        .. figure:: ../_static/create-project/config-ta-eclipse-4.jpg

        * Enter the following values |_|:

            * **Archetype Group ID** |_|: org.squashtest.ta
            * **Archetype Artifact ID** |_|: squash-ta-project-archetype
            * **Archetype Version** |_|: You can check the last version of the Squash Keyword Framework on our `website <https://www.squashtest.com/telechargements>`_
            * **Repository URL** |_|: \http://repo.squashtest.org/maven2/releases/

        * Confirm by clicking on the **OK** button |_|:

    .. figure:: ../_static/create-project/config-ta-eclipse-5.jpg

    * Then, select the archetype (it has appeared in the archetype list) and uncheck the **Show the last version of Archetype only** option. In the Catalog list at the top of the window, choose **Default local**. Click on **Next** to go to the next page |_|:

    .. figure:: ../_static/create-project/config-ta-eclipse-6.jpg

    * On the next screen, (wether you used a provided or custom archetype), you are describing your new test project |_|:

        * The **Group Id** is the name of the broader software package your test automation project belongs to. In our example we will use **org.squashtest.tutorial** as group Id.
        * The **Artifact Id** is the specific name of your test project. Let's use **my.squash.tf.project** as artifact id.
        * Finally, we will use the **1.0** version number, and ignore the field **package** which is not relevant for Squash-TF projects.

    * Now just launch project creation by clicking on the **Finish** button |_|:

    .. figure:: ../_static/create-project/config-ta-eclipse-project-IDs.png

    * The newly created test project appears in the explorer |_|:

    .. figure:: ../_static/create-project/config-ta-eclipse-new-project-explorer.png

|

----------

.. Note::
    If you encounter some difficulties to create your new TF project through the |squashTF| project archetype in Eclipse, please try the command-line method.

|

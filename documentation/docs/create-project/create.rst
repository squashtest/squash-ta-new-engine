..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _getting.started.create.new.project.anchor:

###################################
Create a new SKF automation Project
###################################

|

As SKF is a maven plugin, the best way to create a new SKF automation project is to use the SKF project archetype. When you do so, Maven creates a new SKF automation project for you. Later on, when you first run your tests, Maven will download the SKF execution engine and its dependencies.

.. toctree::
    :maxdepth: 2

    Create a project with IntelliJ <create-with-intellij.rst>
    Create a project with Squash TA Toolbox <create-with-ta-toolbox.rst>
    Create a project using a command line <create-with-cli.rst>
    SKF automation project pom.xml <create-default-pom.rst>

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########
Glossary
########

|

:guilabel:`Capture/Playback tool` |_|: A type of test execution tool where inputs are recorded during manual testing in order to generate automated test scripts that can be executed later (i.e. replayed). These tools are often used to support automated regression testing.

:guilabel:`Driver` |_|: A software component or test tool that replaces a component that takes care of the control and/or the calling of a component or system.

:guilabel:`DSL` |_|: Domain Specific Language. It's a language specifically created for a domain area.

:guilabel:`Expected result` |_|: The behavior predicted by the specification, or another source, of the component or system under specified conditions.

:guilabel:`Failure` |_|: Deviation of the component or system from its expected delivery, service or result.

:guilabel:`False-fail (false-positive) result` |_|: A test result in which a defect is reported although no such defect actually exists in the test object.

:guilabel:`Project Automation Modules` |_|: |squashTF| components are grouped together as functionnal modules (Sahi module, Selenium module...). This modules are plugins, they can be added or removed of the framework based on functionnal needs.

:guilabel:`Post-condition` |_|: Environmental and state conditions that must be fulfilled after the execution of a test or test procedure.

:guilabel:`Pre-condition` |_|: Environmental and state conditions that must be fulfilled before the component or system can be executed with a particular test or test procedure.

:guilabel:`Regression testing` |_|: Testing sof a previously tested program following modification to ensure that defects have not been introduced or uncovered in unchanged  areas of the software, as a result of the changes made. It is performed when the software or its environment is changed.
  
:guilabel:`Requirement` |_|: Extracted from the design documentation and business administration rules they come from, the requirements describe the application expected behaviors.

:guilabel:`Step` |_|: Phase of the functional path set up in a script. Each step verifies an expected result.

:guilabel:`SUT` |_|: System Under Test.

:guilabel:`Test case` |_|: Functional path to execute in order to verify the conformity of functions. The test case is defined by a data set to determine, a script to execute and expected detailed results.

:guilabel:`Test suite` |_|: A set of several test cases for a component or system under test, where the post condition of one test is often used as the precondition for the next one.

:guilabel:`Test execution automation` |_|: The use of software, e.g. capture/playback tools, to control the execution of tests, the comparison of actual results to expected results, the setting up of test preconditions, and other test control and reporting functions.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################################
Proceeding a Test Case / a Test Campagne
########################################

.. contents:: Contents |_|:
    :local:

|

**************
Pre-conditions
**************

Just as manual tests, automated tests generally begin with a step of setting up  pre-requisites before execution of test steps. Nevertheless the way to do it is slightly different between manual tests and automated tests.

In the case of a manual test, the acceptance tester often needs to use the tested application to set-up the pre-requisites of the test. In the example of a Man/Machine Interface permitting to create and consult users accounts |_|: before being able to test the consultation function, we need to create a user account.
This method has a major inconvenient |_|: the qualification of a function depends directly of the good functioning of another function.

In the previous example, if the creation function has a blocking issue, consultation function can't be tested.

Unlike manual tests, automated tests allow to set-up pre-requisites of the test case without going through the tested application. In the previous example, it's possible to create the account directly in the database before consulting it with the application. So, the consultation function can be tested, even if the creation function doesn't work.

|

----

**********
Test Steps
**********

Test steps progress is similar for manual tests and automated tests.
For each test step, the acceptance tester or automaton interact with the SUT (System Under Test) and compare obtained result with expected result.

|

----

************************
Post-conditions Checking
************************

In some test cases, the execution of test steps is not enough to verify the good functioning of the SUT. The state of the system after the test steps progress must be verified too.

Most of the time it consists in verifying persistent data test inside a database or inside a file.

During a manual test, postconditions are often difficult to verify. Just as the setting-up of pre-requisites, the acceptance tester must use the tested application. In the previous example, the only way for the acceptance tester to verify the account creation is using the tested Man/Machine Interface.

In an automated test, verification of post-conditions can be done independently of the tested application. The account creation will be verified consulting directly the database. It is then possible to test a creation function even if the consultation function doesn't work.

In this way, automated tests never use the tested application to verify post-conditions.

|

----

***********
Cleaning Up
***********

In some cases, the test can also have a step of system cleaning after post-conditions verification. It allows to be sure that the tested system is reseted before the execution of the next test case.

This step can be omitted when the step of setting-up pre-requisites is enough to guarantee the state of the SUT. When this step exists, it is executed whatever the test result is (success, failure, error).

|

----

***************
Results Storage
***************

The result of each test case is saved after the execution.

3 kind of results are possible for an automated test |_|: 

        * :guilabel:`Success`

        * :guilabel:`Failure` |_|: an assertion step failed (it obtained a result different than the expected result)

        * :guilabel:`Error` |_|:  an error occured during the test execution.

In the two last cases, saved result has a short explanatory message that allows to identify where the test crashed and if possible, the reason of the crash.

|

----

*************
Test Campaign
*************

Some test preconditions are common to all test cases and don't need to be implemented between each test. Those conditions are set-up once for all at the begining of a campaign. After that all test cases are executed. And after the campaign execution, it can be necessary to clean-up the test environment (clean-up the database, stop server programs needed for the tests execution...).

At the end of an execution campaign, an execution report is created from tests results. This report describe the result of each test case |_|: success, failure or error with an explanatory message in the two last cases.

Here is a figure showing the different steps of an automated tests campaign execution |_|: 

.. figure:: ../../_static/automated-tests-rudiments/automation-tests-steps.jpg

|

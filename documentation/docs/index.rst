..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
Squash Keyword Framework
########################

.. toctree::
   :maxdepth: 2
   :hidden:

   < SKF Doc Homepage > <https://skf.readthedocs.io>
   Getting started <getting-started/getting-started.rst>
   Introduction to Squash Keyword Framework (SKF) <introduction-to-skf/introduction-to-skf.rst>
   Create a new Squash TF Project <create-project/create.rst>
   Writing Tests <writing-tests/writing-tests.rst>
   Execution and Reporting <execution-reporting/execution.rst>
   Libraries <libraries/plugins.rst>
   Tutorials <tutorials/tutorials.rst>


.. contents::
   :local:

SKF (aka Squash Keyword Framework) is a Keyword oriented framework for test automation.

********
Overview
********

Here's a quick overview of our framework :

* A **test script** is written with a composition of **keywords**
* A **keyword** is an action written in (nearly) natural language. It's implemented by a **macro**
* A **macro** (also known as "shortcut" in SKF) is made of :

  * A **hook** : It's the signature of the macro. It describes how other macros and test scripts can call it
  * An **expansion** : It contains a list of **instructions** that'll be executed by the macro.

* An **instruction** can refer to different elements.

  * It can be :

    * An user-defined macro
    * A framework's builtin macro
    * (If needed a framework's low level instruction)

  * It can also have some inputs and/or outputs that corresponds to :

    * **Input** : The target / SUT definition, test data, testing scripts ( ex : SQL query, SoapUI project, Selenium Java project, ...)
    * **Output** : An element used for assertion or as input for other instructions

*********************************
A small example to illustrate
*********************************

Let's have a look at a small example

**>>** Here is a ``SKF test script`` :

.. figure:: _static/intro/script_2.png
   :align: center
   :alt: SKF script

This SKF test script is composed of three steps and use two keywords :

   * ``# I have {number_of_pets} black cats in stock`` (Used two times)
   * ``# User logs in with login {user_login} and password {user_password}, buy {number_of_cats} black cats and logs out``

In order to make this SKF test script work we have to implement two macros.

**>>** The ``# I have {number_of_pets} black cats in stock`` macro :

.. figure:: _static/intro/macro_black_cat_in_stock_2.png
   :align: center
   :alt: SKF script

This macro has two instructions in its expansion :

* The first instruction is a macro provided by the framework in the database plugin (builtin macro).
  It has 2 input parameters and 1 output parameter:

  * Input :

    * db/query_black_cat_stock.sql : A file with a SQL query
    * myDb : A database named "myDb"

  * Output :

    * query_result : Wrapped result of the query

* The second instruction is also a macro but it needs to be implemented. This instruction has 2 inputs :

  * query_result : Resource produced by the previous instruction
  * number_of_pets : Test data

**>>** The ``# User logs in with login {user_login} and password {user_password}, buy {number_of_cats} black cats and logs out`` macro :

.. figure:: _static/intro/macro_buy_black_cat_2.png

In this second macro the expansion is composed of two macros provided by the framework.


.. note:: Screenshots come from IntelliJ IDEA combined with our plugin for coloration and autocompletion

************
SKF benefits
************


* The framework has been built with the **separation of concerns** principle in mind. This leads us to a **multi layer** approach :

  * **Top level** : Test script in natural language
  * **Middle level(s)** : Test implementation
  * **Ground level** : The framework with its modular architecture and resources

* Separating test scripts from their implementation brings more test **robustness** : test implementation changes whereas test script doesn't.

* The use of **natural language** for test scripts makes them more **readable** and easy to write. This makes the writing of SKF test scripts by QA tester possible.

* Implementation of keywords still requires technical skills (and you need to know how to use the targeted robot). However this aspect is **reduced** with the builtin macros provided by the framework.

* SKF is built on a **modular architecture** using plugins : one plugin for each type of test / robot. Each plugin brings the macros (and their associated low level instructions) needed to ease the implementation work. Our aim is to provide the widest set of builtin macro so that the user never have to use low level instructions.

* Its modular architecture gives the possibility to **extend** the capacity of the framework by creating new plugins.

* The writing, for either test scripts or macros, is eased with our IDE plugins (coloration and completion):

  * `IntelliJ IDEA plugin <https://squash-tf.readthedocs.io/projects/intellij-plugin/en/doc-stable/>`_
  * `Eclipse tools <https://squash-tf.readthedocs.io/en/latest/development-tools/toolbox-eclipse.html>`_

.. note:: Squash Keyword Framework (SKF) had a first life as Squash TA framework. The rebranding decision was taken when we decided to focus on the keyword approach.
          (You will surely find some reference to TA at some places). The changes accompanying this new approach is still a work in progress.
          Our first focus is a new IntelliJ IDEA plugin to ease the writing of test scripts.
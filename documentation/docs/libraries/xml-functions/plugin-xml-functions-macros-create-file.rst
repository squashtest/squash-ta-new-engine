..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###########################################
XML Functions Plugin - Macros - Create File
###########################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

****************************************************************
# CREATE FILE {output} FROM {input} BY APPLYIN XSLT {stylesheet}
****************************************************************

.. admonition:: What |_|?

        This macro applies a stylesheet to a xml file in order to create a file resource and logs the result in the console at DEBUG level.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {input} AS __input{%%r1}.xml.file
    CONVERT __input{%%r1}.xml.file TO xml(structured) AS __input{%%r1}.xml

    LOAD {stylesheet} AS __stylesheet{%%r1}.xslt.file
    CONVERT __stylesheet{%%r1}.xslt.file TO xml(structured) AS __stylesheet{%%r1}.xslt.xml
    CONVERT __stylesheet{%%r1}.xslt.xml TO xslt AS __stylesheet{%%r1}.xslt

    CONVERT __input{%%r1}.xml TO file(xslt) USING __stylesheet{%%r1}.xslt AS {output}
    EXECUTE log WITH {output} USING $(logLevel: DEBUG,multiline: yes) AS $()

**> Input** |_|:

        * ``{input}`` |_|: The name of the file to convert (``xml`` type file).

        * ``{stylesheet}`` |_|: The name of the stylesheet file (``xslt`` type file).

**> Output** |_|:

        * ``{output}`` |_|: The name of the converted resource (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# CREATE FILE sample-dataset-resource.xml FROM sample-dataset.xml BY APPLYIN XSLT table1n2sorted.xslt`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-1-resources.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-1-script.png

    **Console output in DEBUG mode** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-1-stacktrace.png

|

----

************************************************************************************
# CREATE FILE {output} FROM {input} BY APPLYIN XSLT {stylesheet} USING {xslt_config}
************************************************************************************

.. admonition:: What |_|?

        This macro applies a stylesheet and a config resource to a xml file in order to create a file resource and logs the result in the console at DEBUG level. Additional configurations can be done with a config file.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {input} AS __input{%%r1}.xml.file
    CONVERT __input{%%r1}.xml.file TO xml(structured) AS __input{%%r1}.xml

    LOAD {stylesheet} AS __stylesheet{%%r1}.xslt.file
    CONVERT __stylesheet{%%r1}.xslt.file TO xml(structured) AS __stylesheet{%%r1}.xslt.xml
    CONVERT __stylesheet{%%r1}.xslt.xml TO xslt AS __stylesheet{%%r1}.xslt

    CONVERT __input{%%r1}.xml TO file(xslt) USING __stylesheet{%%r1}.xslt,{xslt_config} AS {output}
    EXECUTE log WITH {output} USING $(logLevel: DEBUG,multiline: yes) AS $()

**> Input** |_|:

        * ``{input}`` |_|: The name of the file to convert (``xml`` type file).

        * ``{stylesheet}`` |_|: The name of the stylesheet file (``xslt`` type file).

        * ``{xslt_config}`` |_|: The name of the loaded configuration resource (``file`` type resource, from a ``properties`` type file). It can be used to normalize the output (normalize |_| = |_| true).

**> Output** |_|:

        * ``{output}`` |_|: The name of the converted resource (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# CREATE FILE sample-dataset-resource.xml FROM sample-dataset.xml BY APPLYIN XSLT table1n2sorted.xslt USING config-resource.file`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-2-resources.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-2-script.png

    **Console output in DEBUG mode** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-2-stacktrace.png

|

----

*****************************************************************************
# CREATE FILE {output} FROM XML RESOURCE {input} BY APPLYIN XSLT {stylesheet}
*****************************************************************************

.. admonition:: What |_|?

   This macro applies a stylesheet to a loaded xml resource in order to create a file resource and logs the result in the console at DEBUG level.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {stylesheet} AS __stylesheet{%%r1}.xslt.file
    CONVERT __stylesheet{%%r1}.xslt.file TO xml(structured) AS __stylesheet{%%r1}.xslt.xml
    CONVERT __stylesheet{%%r1}.xslt.xml TO xslt AS __stylesheet{%%r1}.xslt

    CONVERT {input} TO file(xslt) USING __stylesheet{%%r1}.xslt AS {output}
    EXECUTE log WITH {output} USING $(logLevel: DEBUG,multiline: yes) AS $()

**> Input** |_|:

        * ``{input}`` |_|: The name of the loaded resource to convert (``xml`` type resource).
        * ``{stylesheet}`` |_|: The name of the stylesheet file (``xslt`` type file).

**> Output** |_|:

        * ``{output}`` |_|: The name of the converted resource (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# CREATE FILE sample-dataset-resource-output.xml FROM XML RESOURCE sample-dataset-resource.xml BY APPLYIN XSLT table1n2sorted.xslt`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-3-resources.png

    **SKF script** |_|:

        In the **SETUP** phase, we load the *sample-dataset.xml* and replace all the occurrences of **'${hello}'** by **'Goodbye'** 
        with a :ref:`SUBSTITUTE KEYS macro <libraries.commons.component.macros.substitute.keys.anchor>`.
        The resulting resource is then converted to a `xml` type resource and used in the CREATE FILE macro.

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-3-script.png

    **Console output in DEBUG mode** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-3-stacktrace.png

|

----

*************************************************************************************************
# CREATE FILE {output} FROM XML RESOURCE {input} BY APPLYIN XSLT {stylesheet} USING {xslt_config}
*************************************************************************************************

.. admonition:: What |_|?

        This macro applies a stylesheet and a config resource to a loaded xml resource in order to create a file resource and logs the result in the console at DEBUG level. Additional configurations can be done with a config file.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {stylesheet} AS __stylesheet{%%r1}.xslt.file
    CONVERT __stylesheet{%%r1}.xslt.file TO xml(structured) AS __stylesheet{%%r1}.xslt.xml
    CONVERT __stylesheet{%%r1}.xslt.xml TO xslt AS __stylesheet{%%r1}.xslt

    CONVERT {input} TO file(xslt) USING __stylesheet{%%r1}.xslt,{xslt_config} AS {output}
    EXECUTE log WITH {output} USING $(logLevel: DEBUG,multiline: yes) AS $()

**> Input** |_|:

        * ``{input}`` |_|: The name of the loaded resource to convert (``xml`` type resource).

        * ``{stylesheet}`` |_|: The name of the stylesheet resource (``xslt`` type file).

        * ``{xslt_config}`` |_|: The name of the loaded configuration resource (``file`` type resource, from a ``properties`` type file). It can be used to normalize the output (normalize |_| = |_| true).

**> Output** |_|:

        * ``{output}`` |_|: The name of the converted resource (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# CREATE FILE sample-dataset-resource-output.xml FROM XML RESOURCE sample-dataset-resource.xml BY APPLYIN XSLT table1n2sorted.xslt USING config-resource.file`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-4-resources.png

    **SKF script** |_|:

        In the **SETUP** phase, we load the *sample-dataset.xml* and replace all the occurrences of **'${hello}'** by **'Goodbye'** 
        with a :ref:`SUBSTITUTE KEYS macro <libraries.commons.component.macros.substitute.keys.anchor>`.
        The resulting resource is then converted to a `xml` type resource and used in the CREATE FILE macro.

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-4-script.png

    **Console output in DEBUG mode** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/create-file/xml-functions-macros-create-file-4-stacktrace.png

|

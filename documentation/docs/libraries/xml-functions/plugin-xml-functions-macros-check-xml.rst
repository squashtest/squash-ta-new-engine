..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################################
XML Functions Plugin - Macros - Check XML
#########################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

************************************************************************
# CHECK IF XML FILE {actual} FILTERED BY {xslt_filter} EQUALS {expected}
************************************************************************

.. admonition:: What |_|?

        This macro apply a xslt filter to an actual and an expected xml file and then checks if the resulting resources match.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {xslt_filter} AS __xslt_filter{%%r1}.file
    CONVERT __xslt_filter{%%r1}.file TO xml(structured) AS __xslt_filter{%%r1}.xml
    CONVERT __xslt_filter{%%r1}.xml TO xslt AS __xslt_filter{%%r1}.xslt

    LOAD {actual} AS __actual{%%r1}.file
    CONVERT __actual{%%r1}.file TO xml(structured) AS __actual{%%r1}.xml
    CONVERT __actual{%%r1}.xml TO xml(xslt) USING __xslt_filter{%%r1}.xslt,$(normalize:true) AS __filtered_actual{%%r1}.xml

    LOAD {expected} AS __expected{%%r1}.file
    CONVERT __expected{%%r1}.file TO xml(structured) AS __expected{%%r1}.xml
    CONVERT __expected{%%r1}.xml TO xml(xslt) USING __xslt_filter{%%r1}.xslt,$(normalize:true) AS __filtered_expected{%%r1}.xml

    ASSERT __filtered_expected{%%r1}.xml IS similaire WITH __filtered_actual{%%r1}.xml USING $(comparateur:xmlunit)

**> Input** |_|:

        * ``{xslt_filter}`` |_|: The name of the filter to apply (``xslt`` type file).

        * ``{actual}`` |_|: The name of the file to compare (``xml`` type file).

        * ``{expected}`` |_|: The name of the file to be compared to (``xml`` type file).

**Example** |_|:

    .. list-table::

        * - :macros:`# CHECK IF XML FILE sample-dataset-1.xml FILTERED BY table1n2sorted.xslt EQUALS sample-dataset-2.xml`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-1-resources.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-1-script.png

|

----

*******************************************************************
# CHECK IF XML {actual} FILTERED BY {xslt_filter} EQUALS {expected}
*******************************************************************

.. admonition:: What |_|?

    This macro apply a xslt filter to an actual and an expected loaded xml resources and then checks if the resulting resources match.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {xslt_filter} AS __xslt_filter{%%r1}.file
    CONVERT __xslt_filter{%%r1}.file TO xml(structured) AS __xslt_filter{%%r1}.xml
    CONVERT __xslt_filter{%%r1}.xml TO xslt AS __xslt_filter{%%r1}.xslt

    CONVERT {expected} TO xml(xslt) USING __xslt_filter{%%r1}.xslt,$(normalize:true) AS __filtered_expected{%%r1}.xml

    CONVERT {actual} TO xml(xslt) USING __xslt_filter{%%r1}.xslt,$(normalize:true) AS __filtered_actual{%%r1}.xml

    ASSERT __filtered_actual{%%r1}.xml IS similaire WITH __filtered_expected{%%r1}.xml USING $(comparateur:xmlunit)

**> Input** |_|:

        * ``{xslt_filter}`` |_|: The name of the filter to apply (``xslt`` type file).
        * ``{actual}`` |_|: The name of the loaded resource to compare (``xml`` type resource).
        * ``{expected}`` |_|: The name of the loaded resource to be compared to (``xml`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# CHECK IF XML sample-dataset-1-modified-resource.xml FILTERED BY table1n2sorted.xslt EQUALS sample-dataset-2-resource.xml`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-2-resources.png

    **SKF script** |_|:

        In the **SETUP** phase, we load the *sample-dataset-1.xml* and replace all the occurrences of **'${hello}'** by **'Goodbye'** 
        with a :ref:`SUBSTITUTE KEYS macro <libraries.commons.component.macros.substitute.keys.anchor>`.
        The resulting resource is then converted to a `xml` type resource and used in the CHECK IF XML macro.

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-2-script.png

|

----

**********************************************************************************
# CHECK IF XML {actual} FILTERED BY {xslt_filter} EQUALS {expected} USING {config}
**********************************************************************************

.. admonition:: What |_|?

        This macro apply a xslt filter to an actual and an expected loaded xml resources and then checks if the resulting resources match. Additional configurations can be done with a config file.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {xslt_filter} AS __xslt_filter{%%r1}.file
    CONVERT __xslt_filter{%%r1}.file TO xml(structured) AS __xslt_filter{%%r1}.xml
    CONVERT __xslt_filter{%%r1}.xml TO xslt AS __xslt_filter{%%r1}.xslt

    CONVERT {expected} TO xml(xslt) USING __xslt_filter{%%r1}.xslt,$(normalize:true) AS __filtered_expected{%%r1}.xml

    CONVERT {actual} TO xml(xslt) USING __xslt_filter{%%r1}.xslt,$(normalize:true) AS __filtered_actual{%%r1}.xml

    LOAD {config} AS config.file
    CONVERT config.file TO properties(structured) AS config.properties

    DEFINE $(comparateur=xmlunit) AS default_comparator
    CONVERT default_comparator TO properties(structured) AS default_comparator.properties
    DEFINE $(comparateur:${comparateur}) AS comparateur.pattern
    CONVERT comparateur.pattern TO file(param) AS comparateur.conf.in USING config.properties
    CONVERT comparateur.conf.in TO file(param) AS comparateur.conf USING default_comparator.properties

    ASSERT __filtered_actual{%%r1}.xml IS similaire WITH __filtered_expected{%%r1}.xml USING comparateur.conf,config.properties

**> Input** |_|:

        * ``{xslt_filter}`` |_|: The name of the filter to apply (``xslt`` type resource).

        * ``{actual}`` |_|: The name of the loaded resource to compare (``xml`` type resource).

        * ``{expected}`` |_|: The name of the loaded resource to be compared to (``xml`` type resource).

        * ``{config}`` |_|: The name of the configuration file. It can be used to change the default comparison engine from xmlunit to jxb, through a 'comparateur |_| = |_| jaxb' entry, or to give a name to the actual and expected resource (actualName |_| = |_| <put |_| name |_| here>, expectedName |_| = |_| <put |_| name |_| here>).

**Example** |_|:

    .. list-table::

        * - :macros:`# CHECK IF XML sample-dataset-1-resource.xml FILTERED BY table1n2sorted.xslt EQUALS sample-dataset-2-resource.xml USING config.properties`

    **Resources** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-3-resources.png

    **SKF script** |_|:

        In the **SETUP** phase, we load the *sample-dataset-1.xml* and replace all the occurrences of **'${hello}'** by **'Goodbye'** 
        with a :ref:`SUBSTITUTE KEYS macro <libraries.commons.component.macros.substitute.keys.anchor>`.
        The resulting resource is then converted to a `xml` type resource and used in the CHECK IF XML macro.

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-3-script.png

    **Console output in DEBUG mode** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-xml-functions/check-xml/xml-functions-macros-check-xml-3-stacktrace.png

|

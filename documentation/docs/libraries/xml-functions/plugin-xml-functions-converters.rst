..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################################
XML Functions Plugin - Converters
#################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents:
   :local:

|

***************
From XML |_|...
***************

|

... |_| to XSLT
***************

**Category-Name** |_|: :guilabel:`structured.xslt`

.. admonition:: What |_|?

    This :guilabel:`structured.xslt` converter will convert a ``xml`` type resource to a ``xslt`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:xml>}` :keywords:`TO` :resource-type:`xslt` (:converter-name:`structured.xslt`) :keywords:`AS` :resources:`{converted<Res:xslt>}`

**> Input** |_|:

    * ``resourceToConvert<Res:xml>`` |_|: The name of the resource to convert (``xml`` type resource).

**> Output** |_|:

    * ``converted<Res:xslt>`` |_|: The name of the converted resource (``xslt`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` stylesheet.xslt :keywords:`AS` :resources:`stylesheet-resource.file`

        :keywords:`CONVERT` :resources:`stylesheet-resource.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`stylesheet-resource.xml`

        :keywords:`CONVERT` :resources:`stylesheet-resource.xml` :keywords:`TO` :resource-type:`xslt` (:converter-name:`structured.xslt`) :keywords:`AS` :resources:`stylesheet-resource.xslt`

|

-----------------

... |_| to File
***************

**Category-Name** |_|: :guilabel:`xslt`

.. admonition:: What |_|?

    This :guilabel:`xslt` converter will convert a ``xml`` type resource to a ``file`` type resource. A stylesheet can be applied to the ``xml`` resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:xml>}` :keywords:`TO` :resource-type:`file` (:converter-name:`xslt`) [:keywords:`USING` :resources:`{config<Res:xslt>}`] :keywords:`AS` :resources:`{converted<Res:file>}`

**> Input** |_|:

    * ``resourceToConvert<Res:xml>`` |_|: The name of the resource to convert (``xml`` type resource).

    * ``config<Res:xslt>`` |_|: The name of the configuration resource (``xslt`` type resource).

**> Output** |_|:

    * ``converted<Res:file>`` |_|: The name of the converted resource (``file`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` sample.xml :keywords:`AS` :resources:`sample-resource.file`

        :keywords:`CONVERT` :resources:`sample-resource.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`sample-resource.xml`

        :keywords:`LOAD` stylesheet.xslt :keywords:`AS` :resources:`stylesheet-resource.file`

        :keywords:`CONVERT` :resources:`stylesheet-resource.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`stylesheet-resource.xml`

        :keywords:`CONVERT` :resources:`stylesheet-resource.xml` :keywords:`TO` :resource-type:`xslt` (:converter-name:`structured.xslt`) :keywords:`AS` :resources:`stylesheet-resource.xslt`

        :keywords:`CONVERT` :resources:`sample-resource.xml` :keywords:`TO` :resource-type:`file` (:converter-name:`xslt`) :keywords:`USING` :resources:`stylesheet-resource.xslt` :keywords:`AS` :resources:`final-sample-resource.file`

|

-----------------

... |_| to XML
**************

**Category-Name** |_|: :guilabel:`xslt`

.. admonition:: What |_|?

    This :guilabel:`xslt` converter will convert a ``xml`` type resource to another ``xml`` type resource. A stylesheet can be applied to the ``xml`` resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:xml>}` :keywords:`TO` :resource-type:`xml` (:converter-name:`xslt`) [:keywords:`USING` :resources:`{config<Res:xslt>}`] :keywords:`AS` :resources:`{converted<Res:xml>}`

**> Input** |_|:

    * ``resourceToConvert<Res:xml>`` |_|: The name of the resource to convert (``xml`` type resource).

    * ``config<Res:xslt>`` |_|: The name of the configuration resource (``xslt`` type resource).

**> Output** |_|:

    * ``converted<Res:xml>`` |_|: The name of the converted resource (``xml`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` sample.xml :keywords:`AS` :resources:`sample-resource.file`

        :keywords:`CONVERT` :resources:`sample-resource.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`sample-resource.xml`

        :keywords:`LOAD` stylesheet.xslt :keywords:`AS` :resources:`stylesheet-resource.file`

        :keywords:`CONVERT` :resources:`stylesheet-resource.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`stylesheet-resource.xml`

        :keywords:`CONVERT` :resources:`stylesheet-resource.xml` :keywords:`TO` :resource-type:`xslt` (:converter-name:`structured.xslt`) :keywords:`AS` :resources:`stylesheet-resource.xslt`

        :keywords:`CONVERT` :resources:`sample-resource.xml` :keywords:`TO` :resource-type:`xml` (:converter-name:`xslt`) :keywords:`USING` :resources:`stylesheet-resource.xslt` :keywords:`AS` :resources:`final-sample-resource.xml`

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


###########
SKF Plugins
###########

.. toctree::
   :maxdepth: 2

    Commons component Plugin <commons-component/plugin-commons-component.rst>
    Database Plugin <database/plugin-database.rst>
    Filechecker Plugin <filechecker/plugin-filechecker.rst>
    FTP Plugin <ftp/plugin-ftp.rst>
    JUnit Plugin <junit/plugin-junit.rst>
    Local Process Plugin <local-process/plugin-local-process.rst>
    MEN XML Checker <men-xml-checker/plugin-men-xml-checker.rst>
    Sahi Plugin <sahi/plugin-sahi.rst>
    Selenium Plugin <selenium/plugin-selenium.rst>
    Selenium Plugin Legacy <selenium-legacy/plugin-selenium-legacy.rst>
    SoapUI Plugin <soapui/plugin-soapui.rst>
    SSH - SFTP Plugin <ssh/plugin-ssh-sftp.rst>
    XML Functions Plugin <xml-functions/plugin-xml-functions.rst>
..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



###################################################
Commons component plugin - Macros - Substitute keys
###################################################

.. contents:: Contents |_|:
   :local:

.. _libraries.commons.component.macros.substitute.keys.anchor:

|

************************************************************************
# SUBSTITUTE KEYS IN {files} USING {key_value_list} AS {processed_files}
************************************************************************

.. admonition:: What |_|?

    This macro allows to replace specific keys by selected values in a bundle of files.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {key_value_list} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param) USING placeholder{%%rand3}.properties AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{key_value_list}`` |_|: It can either be a path to a properties file or an inline command with keys and values. 
    
        .. list-table::
        
            * - **Example of inline command** : $(key1=value1 \\n key2=value2).
      
 
**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE KEYS IN resources_folder USING resources_folder/file.properties AS result_bundle`

**Or**

    .. list-table::

        * - :macros:`# SUBSTITUTE KEYS IN resources_folder USING $(oneKey=oneValue \\n anotherKey=anotherValue) AS result_bundle`

    **First file to process** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-example-file.png

    **Second file to process** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-example-file2.png

    **.properties File** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-properties-file.png

    **The folder containing files to process which corresponds to** ``{files}`` |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-resources-folder.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro.png

    In order to check that the macro worked properly, we added in this example, the instruction **"LOAD result AS resultOutput"** to be able to see the result output.

    You can access to the result output in the following folder which contains temporary files |_|:

    .. list-table::

        * - C:\\Users\\*user name*\\AppData\\Local\\Temp\\Squash_TA\\20190909_121048_957615127627872437436\\tests\\tests\\substitute_keys.ta\\myResources2594181966007652622639979916temp

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-temporary-file-folder.png

    **Result output for first file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-example-file-output.png

    **Result output for second file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-macro-example-file-output2.png

| 

--------------------

.. _libraries.commons.component.macros.substitute.keys.matching.anchor:

***************************************************************************************************************
# SUBSTITUTE KEYS IN {files} FOR FILES WHOSE NAMES MATCHING {regex} USING {key_value_list} AS {processed_files}
***************************************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace specific keys by selected values in a bundle of files whose names are matching a selected regular expression.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {key_value_list} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.include:{regex}) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

        * ``{key_value_list}`` |_|: It can either be a path to a properties file or an inline command with keys and values. 
    
        .. list-table::
        
            * - **Example of inline command** : $(key1=value1 \\n key2=value2).
      
 
**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE KEYS IN resources_folder FOR FILES WHOSE NAMES MATCHING .xml USING resources_folder/file.properties AS result_bundle`

    **Or**

    .. list-table::

        * - :macros:`# SUBSTITUTE KEYS IN resources_folder FOR FILES WHOSE NAMES MATCHING .xml USING $(oneKey=oneValue \\n anotherKey=anotherValue) AS result_bundle`

    This example is based on the previous one. For more details, please check :ref:`here <libraries.commons.component.macros.substitute.keys.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-matching-regex-macro2.png

    **Or**

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-matching-regex-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-matching-regex-macro-result.png

    The **.txt** and **.properties** files which don't match the selected regex are properly excluded.

    **Result output for first file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-matching-regex-macro-exemple-file-output1.png

    **Result output for second file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-matching-regex-macro-exemple-file-output2.png

    The **.xml** file is the only one that has been processed.

| 

--------------------

.. _libraries.commons.component.macros.substitute.keys.not.matching.anchor:

*******************************************************************************************************************
# SUBSTITUTE KEYS IN {files} FOR FILES WHOSE NAMES NOT MATCHING {regex} USING {key_value_list} AS {processed_files}
*******************************************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace specific keys by selected values in a bundle of files whose names are not matching a selected regular expression.
 
**Underlying instruction** |_|:

.. code-block:: html

    LOAD {key_value_list} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.exclude:{regex}) AS {processed_files}

**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

        * ``{key_value_list}`` |_|: It can either be a path to a properties file or an inline command with keys and values. 
    
        .. list-table::
        
            * - **Example of inline command** : $(key1=value1 \\n key2=value2).
      
 
**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE KEYS IN resources_folder FOR FILES WHOSE NAMES NOT MATCHING .xml USING resources_folder/file.properties AS result_bundle`

    **Or**

    .. list-table::

        * - :macros:`# SUBSTITUTE KEYS IN resources_folder FOR FILES WHOSE NAMES NOT MATCHING .xml USING $(oneKey=oneValue,anotherKey \\n anotherValue) AS result_bundle`

    This example is based on the previous one. For more details, please check :ref:`here <libraries.commons.component.macros.substitute.keys.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-not-matching-regex-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-not-matching-regex-macro-result.png

    The **.xml** file which match the selected regex is properly excluded.

    **Result output for first file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-not-matching-regex-macro-exemple-file-output1.png

    **Result output for second file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-keys/substitute-keys-not-matching-regex-macro-exemple-file-output2.png

    The **.xml** file is not processed whereas the **.txt** is.

|

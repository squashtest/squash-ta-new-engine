..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################################
Commons component plugin - Converters
#####################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

Since converters create resource of ``B`` type using a resource of ``A`` type, the documentation on converters follows a pattern 'from ``A`` to ``B`` ' (e.g. from ``file`` to ``query.sql``). Most of the time knowing the category of the resources you have and/or you want to obtain will be enough to find which converter you need using the following list. However remember that a converter is fully qualified by his signature |_|: the only way to disambiguate situations where multiple converters consuming and producing the same categories could apply is to specify the name of the converter too.

Like the other engine components, a converter is configurable. It means that you can pass parameters and tweak the operation.

|

****************
From file |_|...
****************

A test project is mostly made of files, including the test resources. This is why the ``file`` category is so important and overly processed by converters.

|

... |_| to bundle
*****************

**Category-Name** |_|: :guilabel:`unchecked`

.. admonition:: What |_|?

        This :guilabel:`unchecked` converter will convert a ``file`` type resource to a ``bundle`` type resource. It checks during the conversion that the resource to convert is really pointing to a directory.

+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|:keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`bundle` (:converter-name:`unchecked`) :keywords:`AS` :resources:`{converted<Res:bundle>}` [ :keywords:`USING` :resources:`{mainPath<Res:file>}` ]|
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references the root directory. This root directory should contains the whole files and directories of the bundle (``file`` type resource).

        * ``mainPath<Res:file>`` |_| (OPTIONAL) |_|: The name of the configuration resource. The content of the file should be: mainpath:relativePathToMainFile (Note |_|: you could use an inline definition). This path to main file should be relative to the directory given as the root directory.

> **Output** |_|:

        * ``converted<Res:bundle>`` |_|: The name of the converted resource (``bundle`` type resource).

**Example** |_| (with the :keywords:`USING` clause and an inline definition) |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/rootDirectory :keywords:`AS` :resources:`bundle.file`

        :keywords:`CONVERT` :resources:`bundle.file` :keywords:`TO` :resource-type:`bundle` (:converter-name:`unchecked`) :keywords:`AS` :resources:`bundle.bundle` :keywords:`USING` $(mainpath:relative/path/to/mainFile.txt)

|

--------------------

... |_| to csv
**************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|? 

        This :guilabel:`structured` converter will convert a ``file`` type resource to a ``csv`` type resource. It checks during the conversion that the resource to convert is really pointing to a csv file.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`csv` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:csv>}` [ :keywords:`USING` :resources:`{mainPath<Res:file>}` ]

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references the csv file (``file`` type resource).
        
        * ``mainPath<Res:file>`` |_| (OPTIONAL) |_|: The name of the configuration resource. given as the root directory.

> **Output** |_|:

        * ``converted<Res:csv>`` |_|: The name of the converted resource (``csv`` type resource).
  
**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` csv1/mycsv.csv :keywords:`AS` :resources:`mycsv.file`

        :keywords:`CONVERT` :resources:`mycsv.file` :keywords:`TO` :resource-type:`csv` (:converter-name:`structured`) :keywords:`AS` :resources:`mycsv.csv`

|

--------------------

... |_| to directory
********************

**Category-Name** |_|: :guilabel:`filesystem`

.. admonition:: What |_|?

        This :guilabel:`filesystem` converter will convert a ``file`` type resource to a ``directory`` type resource. It checks during the conversion that the resource to convert is really pointing to a directory.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`directory` (:converter-name:`filesystem`) :keywords:`AS` :resources:`{converted<Res:directory>}`

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a directory (``file`` type resource).

> **Output** |_|:

        * ``converted<Res:directory>`` |_|: The name of the converted resource (``directory`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/Directory :keywords:`AS` :resources:`directory.file`

        :keywords:`CONVERT` :resources:`directory.file` :keywords:`TO` :resource-type:`directory` (:converter-name:`filesystem`) :keywords:`AS` :resources:`directory.directory`

.. _param.relativedate Overview:

|

--------------------

... |_| to file (using param.relativedate)
******************************************

**Category-Name** |_|: :guilabel:`param.relativedate`

.. admonition:: What |_|?

        This :guilabel:`param.relativedate` converter will convert a ``file`` type resource to another ``file`` type resource. In the context of the aging factor a mechanism has been set-up in SKF to manage dates. It consists in replacing dates of the data set with formulas of type |_|: 

+---------------+
|**${function}**|
+---------------+

where function is |_|:

    * ``now().format(f)`` |_|: return the execution date at the 'f' format.
    * ``now().addDay(n).format(f)`` |_|: return the execution date + 'n' days (positive or negative) at the 'f' format.
    * ``now().addMonth(n).format(f)`` |_|: return the execution date + 'n' months (positive or negative) at the 'f' format.
    * ``now().addYear(n).format(f)`` |_|: return the execution date + 'n' years (positive or negative) at the 'f' format.

.. note |_|::

        Date and time formats are specified by date and time pattern strings, as described `here <https://docs.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html>`_.

.. list-table:: **Examples with an SKF script run on 16/05/2012** |_|:
   :header-rows: 1

   * - Function
     - Result 
   * - now().format(yyyyMMdd)
     - 20120516 
   * - now().addDay(1).format(dd/MM/yyyy)
     - 17/05/2012 
   * - now().addDay(-2).addMonth(1).addYear(-3).format(dd MMMM yyyy)
     - 14 June 2009 

Since **1.7.0**, you can overwrite the default locale of your date, with a language code or a language and a country |_|:

        * now().[...].format(f, l) |_|: return the date at the 'f' format in the 'l' language, where 'l' is a lower-case, two-letter code as defined by ISO-639.
        * now().[...].format(f, l, C) |_|: return the date at the 'f' format in the 'l' language of the 'C' country, where 'l' is a lower-case, two-letter code as defined by ISO-639 and 'C' is an upper-case, two-letter code as defined by ISO-3166.

.. list-table::
   :header-rows: 1

   * - Function
     - Result
   * - now().addMonth(1).format(dd MMMM yyyy, fr) 
     - 16 juin 2012  
   * - now().addMonth(1).format(dd MMMM yyyy, de, DE)
     - 16 Juni 2012
 
Since **1.10.0**, you can manipulate the date in the ${function} with 3 new options |_|:

* The ensureWorkingDay($param) function adjusts the computed date to the nearest working day before or after the input date ($param must be replaced by AFTER or BEFORE).
* The addWorkingDay(n) function allows to add a given number of working days to its input date (n is a positive or negative integer).
* If you want to specify a date which is different from now(), you can use the following method |_|:
    
    **Example** |_|:
        
    .. code-block:: html
        
        LOAD folder/file AS templateData
        DEFINE $(date-key=05051978) AS rawProperties
        CONVERT rawProperties TO properties(structured) AS prop
        CONVERT templateData TO file(param.relativedate) USING prop AS data

        Written in the file to process : ${date(date-key,ddMMyyyy).addDay(-1).format(yyyy-MM-dd)}

.. list-table::
   :header-rows: 1

   * - Function
     - Result
   * - now().addMonth(4).ensureWorkingDay(AFTER).format(dd MMM yyyy, fr) 
     - 17 Septembre 2012 
   * - now().addMonth(4).ensureWorkingDay(BEFORE).format(dd MMM yyyy, fr)
     - 14 Septembre 2012
   * - now().addWorkingDay(10).format(dd MMM yyyy, fr)
     - 30 Mai 2012

The Working days are read from component configuration through the :keywords:`USING` clause.

**Example of file** |_|:

     * org.squashtest.tf.plugin.commons.parms.data.WorkedSunday=false
   
     * org.squashtest.tf.plugin.commons.parms.data.WorkedSaturday=false

     * org.squashtest.tf.plugin.commons.parms.data.WorkedMonday=true

     * org.squashtest.tf.plugin.commons.parms.data.nonWorkingDays=2018-05-01,2018-12-25,2019-01-01

The **fourth** parameter list all the non working days, you must declare the date like yyyy-MM-dd and separate them with a comma.

.. note |_|::

        The :guilabel:`to file(param)` converter must only use text files. If you want to use it on a bundle that contains binary files, you must exclude them in the converter. You can use either squashtest.tf.param.exclude OR squashtest.tf.param.include parameters.

The converter transforms formulas ``${function}`` in valid dates at the execution |_|:

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`file` 

        (:converter-name:`param.relativedate`) [:keywords:`USING` :resources:`{properties<Res:properties>}`]  :keywords:`AS` :resources:`{converted<Res:file>}` 

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a file. All the dates of the file must have been replaced by formulas like '${function}'.
        * ``properties<Res:properties>`` |_|: If the resourceToConvert is a bundle containing binary file, this properties file must contain either the squashtest.tf.param.exclude OR the squashtest.tf.param.include parameter. Both of them use regular expressions. According to the selected parameter, the file(s) matching the regex will or will not be treated by this converter.

> **Output** |_|:

        * ``converted<Res:file>`` |_|: The name of the converted resource (``file`` type resource). The finale resource is the same than the input resource, the difference is that formulas have been replaced with valid dates.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/myfile :keywords:`AS` :resources:`myfile.file`

        :keywords:`CONVERT` :resources:`myfile.file` :keywords:`TO` :resource-type:`file` (:converter-name:`param.relativedate`) :keywords:`AS` :resources:`myfile2.file`


.. _libraries.common.component.converters.from.file.to.file.using.param.anchor:

|

--------------------

... |_| to file (using param)
*****************************

**Category-Name** |_|: :guilabel:`param`

**Since 1.6.0** |_|:

.. admonition:: What |_|?

        This :guilabel:`param` converter will convert a ``file`` type resource to another ``file`` type resource. After the conversion all the placeholder, whose key was found in the properties resource given in the USING clause, should have been replace by it's valid value (the value associate to the key in the property resource file). The initial File resource could be a file or a directory. If it's a directory then all the file contained in this directory should be process.

**Here are the rule used** |_|:

* Placeholder syntax |_|: ${key}
* Authorized characters for properties key |_|: letters (a-z;A-Z), digits (0-9), underscore (_), dot (.) and dash (-)
* The convert instruction could take one or many properties file AND one or many inline statements
* If a property key is defined many times, then it's the last stated which is take into account
* If a placeholder is surrounding with character @, then the placeholder is escaped. For example if we have in the file to process |_|: @${test}@, then we will have in the final file |_|: ${test}
* If a placeholder key is not found in the properties key, then the placeholder is escaped.

.. Note |_|:: 

        The :guilabel:`to file(param)` converter must be used only text files. If you use it on a bundle that contains binary files then you must exclude them. To do this you can use either squashtest.tf.param.exclude OR squashtest.tf.param.include parameters.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`file` (:converter-name:`param`) :keywords:`USING` :resources:`{properties<Res:properties>}` :keywords:`AS` :resources:`{converted<Res:file>}` 

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a file.
        * ``properties<Res:properties>`` |_|: This properties file contains the mapping key-value. If the resourceToConvert is a bundle containing binary file, this properties file must also contain either the squashtest.tf.param.exclude OR the squashtest.tf.param.include parameter. Both of them are regular expression the file contained in the bundle must match to be or not to be treated by this converter.

> **Output** |_|:

        * ``converted<Res:file>`` |_|: The name of the converted resource (``file`` type resource). The finale resource is the same than the input resource, the difference is that the placeholder ${...} have been replaced with their valid values.

**Example** |_|:

.. list-table::

    - * :keywords:`LOAD` sahi/placeholder.properties :keywords:`AS` :resources:`placeholder.file`

        :keywords:`CONVERT` :resources:`placeholder.file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`placeholder.properties`

        |

        :keywords:`LOAD` sahi/main/simple-script.sah :keywords:`AS` :resources:`sahiFile`

        :keywords:`CONVERT` :resources:`sahiFile` :keywords:`TO` :resource-type:`file` (:converter-name:`param`) :keywords:`USING` :resources:`placeholder.properties` :keywords:`AS` :resources:`processedSahiFile`

        |

        :keywords:`CONVERT` :resources:`processedSahiFile` :keywords:`TO` :resource-type:`script.sahi` (:converter-name:`script`) :keywords:`AS` :resources:`suite`

::

        Where :

    * placeholder.properties contains : c3p0.data=Using c3p0
    * simple-script.sah contains : _click(_link("${c3p0.data}"));

        Then :

    * processedSahiFile should contains : _click(_link("Using c3p0")); 

|

--------------------

... |_| to properties
*********************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

        This :guilabel:`structured` converter will convert a ``file`` type resource to a ``properties`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:properties>}`

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references the '.properties' file (list of properties key / value) where the used separator is '='.

> **Output** |_|:

        * ``converted<Res:properties>`` |_|: The name of the converted resource (``properties`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` :resources:`path/myfile.properties` :keywords:`AS` :resources:`myfile.file`
    
        :keywords:`CONVERT` :resources:`myfile.file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`myfile.prop`

|

--------------------

... |_| to script.java
**********************

**Category-Name** |_|: :guilabel:`compile`

.. admonition:: What |_|?

        This :guilabel:`compile` converter will convert a ``file`` type resource to a ``script.java`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`{converted<Res:script.java>}` [ :keywords:`USING` :resources:`{mainPath<Res:file>}` ]

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references the root directory of the java code bundle which contains.resources and the java's source code.
        * ``mainPath<Res:file>`` |_| (OPTIONAL) |_|: The name of the configuration resource. It represents a configuration file containing java compilation options.(Possible options are those of the Java compiler present on the machine). In this file options can be written:

                * In line separated with a space character
                * One option per line
                * A mix of both

> **Output** |_|:

        * ``converted<Res:script.java>``: The name of the converted resource (Resource of type script.java). It contains the compiled java code.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/javaBundle :keywords:`AS` :resources:`bundleJava.file`

        :keywords:`CONVERT` :resources:`bundleJava.file` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`bundleJava.compiled` :keywords:`USING` $(mainpath:relative/path/to/compileOptions)

|

--------------------

... |_| to xml
**************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

        This :guilabel:`structured` converter will convert a ``file`` type resource to a ``xml`` type resource. It checks during the conversion that the resource to convert is really xml category.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:xml>}`

> **Input** |_|:

        * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references the xml file.

> **Output** |_|:

        * ``converted<Res:xml>`` |_|: The name of the converted resource (``xml`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` myfile.xml :keywords:`AS` :resources:`myfile.file`
    
        :keywords:`CONVERT` :resources:`myfile.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`myXMLfile`

|

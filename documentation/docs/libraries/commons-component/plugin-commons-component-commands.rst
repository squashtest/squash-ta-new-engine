..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###################################
Commons component plugin - Commands
###################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

*******
cleanup
*******

**@See** |_|: Since |squashTA| |_| **1.7.0**, this command moved to the :ref:`Local Process Plugin <local.process.commands.cleanup.anchor>`.

|

----

*****
pause
*****

.. admonition:: Description |_|: 

        Makes the test execution sleep for a given time (expressed in milliseconds).
        
.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`pause` :keywords:`WITH` $(<n>) :keywords:`AS` $()

> **Input** |_|:

        * ``<n>`` |_|: an integer. It represents the time in milliseconds.

|

----

***
log
***

.. admonition:: Description |_|:

        This instruction allows writing a message in the log console.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`log` :keywords:`WITH` $(<message>) :keywords:`USING` $(logLevel: <level>) :keywords:`AS` $()

> **Input** |_|:

        * ``<message>`` |_|: The message you want to display in the log console.
        * ``<level>`` |_|: The log level to use : DEBUG, INFO, WARN or ERROR.

|

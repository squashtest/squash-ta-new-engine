..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
Commons component plugin
########################

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Repositories <plugin-commons-component-repositories.rst>
    Target <plugin-commons-component-target.rst>
    Resources <plugin-commons-component-resources.rst>
    Macros <plugin-commons-component-macros.rst>
    Advanced Users <plugin-commons-component-advanced-users.rst>

In an SKF script, the tokens are the spine and the engine components are the muscles. The package commons-components is a basic plugin shipped with SKF. It provides the platform with some basic Resources, Repositories, Targets, and Engine Components.

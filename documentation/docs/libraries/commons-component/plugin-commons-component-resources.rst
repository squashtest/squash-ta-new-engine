..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

####################################
Commons component plugin - Resources
####################################

.. contents:: Contents |_|:
   :local:

This subsection introduces you to the Resources shipped with the commons-components basic plugin. The most nitpicking of you will notice that the ``file`` resource category is part of the core of SKF and doesn't need the plugin to be available. Nevertheless it is stated here for convenience.

Most of the resources here will start their careers as ``file`` resource, then will be converted to their type using converters. Some of them can also be produced as the result of a command (for instance ``result.sql``).

Since most of the resources will be materialized as a ``file`` in your test project, you might be wondering what ``file`` corresponds to complex resources (``conf.dbunit`` for instance). You will NOT find that information here, because there are potentially unlimited ways to create that resource, not just one file format. In other words, one could imagine that a given resource could be constructed from other resources involving completely different material. A good example is the ``dataset.dbunit`` resource |_|: the **Database Plugin** provides three ways of creating one of these.

In fact, the question of how to obtain a resource, given another resource, is typically the job of the converters. If you're interested in what you should supply as inputs to the system in order to produce the resources you want, you should check the '**Converters**' section, located in the '**Advanced Users**' section of each plugin. In the case of our ``dataset.dbunit`` example, the answer you look for is in :ref:`this section <database.plugin.converters.anchor>`.

|

****
file
****

**Category-name** |_|: :guilabel:`file`

.. admonition:: What |_|?

        :guilabel:`file` is a resource type representing a plain file or a directory, no assumption made on the content of either of them.

|

----

******
bundle
******

**Category-name** |_|: :guilabel:`bundle` 

.. admonition:: What |_|?

        :guilabel:`bundle` is a resource type representing a set of files. Basically it is a directory containing more directories or files. The root directory is called the 'base'. However it also have additional attributes. Those attributes give clues on what is the relationships between the files it embeds. 

The followig attribute is available |_|: 

        * ``mainpath`` |_|: denotes which file is considered to be the main file, and the other files represent its dependencies. The mainpath is the path relative to the base of the bundle. When used, the context will decide what to do with that main file, typically when using commands.

|

----

*********
directory
*********

**Category-name** |_|: :guilabel:`directory`

.. admonition:: What |_|?

        :guilabel:`directory` is a resource type that represents a whole directory.

|

----

*******
process
*******

**Category-name** |_|: :guilabel:`process`

**@See** |_|: Since |squashTA| |_| **1.7.0**, this resource moved to the :ref:`Local Process Plugin <local.process.resources.process.anchor>`.

|

----

**********
properties
**********

**Category-name** |_|: :guilabel:`properties`

.. admonition:: What |_|?

        :guilabel:`properties` is a resource type that represents properties, in other words a set of pairs of <key - value >.

|

----

***********
script.java
***********

**Category-name** |_|: :guilabel:`script.java`

.. admonition:: What |_|? 

        This resource encapsulates a java code bundle, including resources and compiled java classes.

|

----

***
xml
***

**Category-name** |_|: :guilabel:`xml`

.. admonition:: What |_|?

        :guilabel:`xml` is a resource type that represents a file or a folder, like :guilabel:`file`. The difference is that the content is trusted to be of XML nature.

|

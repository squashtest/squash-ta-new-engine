..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#############################################################
Commons component plugin - Macros - Substitute dates and keys
#############################################################

.. contents:: Contents |_|:
   :local:

|

**********************************************************************************
# SUBSTITUTE DATES AND KEYS IN {files} USING {key_value_list} AS {processed_files}
**********************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates and keys by values in a bundle of files. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.
 
**Underlying instruction** |_|:

.. code-block:: html

    LOAD {key_value_list} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING placeholder{%%rand3}.properties AS __bundle{%%rand2}
    CONVERT __bundle{%%rand2} TO file(param) USING placeholder{%%rand3}.properties AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{key_value_list}`` |_|: It can either be a path to a properties file or an inline command with keys and values. 
    
        .. list-table::
        
            * - **Example of inline command** : $(key1=value1 \\n key2=value2).
      
 
**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

.. list-table::

    * - :macros:`# SUBSTITUTE DATES AND KEYS IN resources_folder USING resources_folder/file.properties AS result_bundle`

**Or**

.. list-table::

    * - :macros:`# SUBSTITUTE DATES AND KEYS IN resources_folder USING $(oneKey=oneValue \\n anotherKey=anotherValue) AS result_bundle`

For more information please check the following sections |_|: :ref:`substitute dates macro <commons.component.substitute.dates.macro.anchor>` and :ref:`substitute keys macro <libraries.commons.component.macros.substitute.keys.anchor>`.

| 

--------------------

*************************************************************************************************************************
# SUBSTITUTE DATES AND KEYS IN {files} FOR FILES WHOSE NAMES MATCHING {regex} USING {key_value_list} AS {processed_files}
*************************************************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates and keys by values in a bundle of files whose names match a selected regular expression. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.
 
**Underlying instruction** |_|:

.. code-block:: html

    LOAD {key_value_list} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.include:{regex}) AS __bundle{%%rand2}
    CONVERT __bundle{%%rand2} TO file(param) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.include:{regex}) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

        * ``{key_value_list}`` |_|: It can either be a path to a properties file or an inline command with keys and values. 
    
        .. list-table::
        
            * - **Example of inline command** : $(key1=value1 \\n key2=value2).
      
 
**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

.. list-table::

    * - :macros:`# SUBSTITUTE DATES AND KEYS IN resources_folder FOR FILES WHOSE NAMES MATCHING .xml USING resources_folder/file.properties AS result_bundle`

**Or**

.. list-table::

    * - :macros:`# SUBSTITUTE DATES AND KEYS IN resources_folder FOR FILES WHOSE NAMES MATCHING .xml USING $(oneKey=oneValue \\n anotherKey=anotherValue) AS result_bundle`


For more information please check the following sections |_|: :ref:`substitute dates macro <commons.component.macros.substitute.dates.files.matching.anchor>` and :ref:`substitute keys macro <libraries.commons.component.macros.substitute.keys.matching.anchor>` for files whose names are matching the given regular expression.

| 

--------------------

*****************************************************************************************************************************
# SUBSTITUTE DATES AND KEYS IN {files} FOR FILES WHOSE NAMES NOT MATCHING {regex} USING {key_value_list} AS {processed_files}
*****************************************************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates and keys by values in a bundle of files whose names are not matching a selected regular expression. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.
 
**Underlying instruction** |_|:

.. code-block:: html

    LOAD {key_value_list} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.exclude:{regex}) AS __bundle{%%rand2}
    CONVERT __bundle{%%rand2} TO file(param) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.exclude:{regex}) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

        * ``{key_value_list}`` |_|: It can either be a path to a properties file or an inline command with keys and values. 
    
        .. list-table::
        
            * - **Example of inline command** : $(key1=value1 \\n key2=value2).
      
 
**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

.. list-table::

    * - :macros:`# SUBSTITUTE DATES AND KEYS IN resources_folder FOR FILES WHOSE NAMES NOT MATCHING .txt USING resources_folder/file.properties AS result_bundle`

**Or**

.. list-table::

    * - :macros:`# SUBSTITUTE DATES AND KEYS IN resources_folder FOR FILES WHOSE NAMES NOT MATCHING .txt USING $(oneKey=oneValue \\n anotherKey=anotherValue) AS result_bundle`

For more information please check the following sections |_|: :ref:`substitute dates macro <commons.component.macros.substitute.dates.files.not.matching.anchor>` and :ref:`substitute keys macro <libraries.commons.component.macros.substitute.keys.not.matching.anchor>` for files whose names are not matching the given regular expression.

|

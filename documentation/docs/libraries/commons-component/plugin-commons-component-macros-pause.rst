..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################################
Commons component plugin - Macros - Pause
#########################################

.. contents:: Contents |_|:
   :local:

|

*********************************
# PAUSE {time_in_ms} MILLISECONDS
*********************************

.. admonition:: What |_|?

        This macro makes the test execution sleep for a given time (expressed in milliseconds).

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE pause WITH $({time_in_ms}) AS $()

**> Input** |_|:

        * ``time_in_ms`` |_|: Time in milliseconds.

**Example** |_|:

        .. list-table::

            * - :macros:`# PAUSE 3000 MILLISECONDS`

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/pause/pause-millisecond-macro.png

    **Console Output** |_|:

    .. figure:: ../../_static/plugin-commons-component/pause/pause-millisecond-macro-result.png

|

-----

***************************
# PAUSE {time_in_s} SECONDS
***************************

.. admonition:: What |_|?

        This macro makes the test execution sleep for a given time (expressed in seconds).

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE pause WITH $({time_in_s}000) AS $()

**> Input** |_|:

        * ``time_in_s`` |_|: Time in seconds.

**Example** |_|:

    .. list-table::

        * - :macros:`# PAUSE 3 SECONDS`

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/pause/pause-second-macro.png

    **Console Output** |_|:

    .. figure:: ../../_static/plugin-commons-component/pause/pause-second-macro-result.png

|
..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#######################################
Commons component plugin - Repositories
#######################################

.. contents:: Contents |_|:
   :local:

The Repositories give you access to your test data from your script using the LOAD instruction. The present subsection will teach you how to configure and use them.

.. admonition:: IMPORTANT |_|: 

        By default you can always access the Resources in your 'resources' directory (a default Repository pointing to that directory always exists). So if all you need is this unique directory you don't need to declare any Repository.

|

*********
Classpath
*********

**Category-name** |_|: :guilabel:`classpath`

.. admonition:: What |_|?

        Retrieves files present in the classpath of SKF and is helpful to load Resources embedded in other SKF plugins. If you don't know what it means, then you probably don't need it.

**Configuration** |_|: A simple .properties file dropped in the 'repositories' directory of your test project. It must contain EXACTLY '**squashtest.ta.classpath**', with any values you like (it doesn't matter). Any other properties present in this file will disqualify it. 

**Example of valid configuration file** |_|::

        squashtest.ta.classpath=whatever

|

----

***
URL
***

**Category-name** |_|: :guilabel:`url`

.. admonition:: What |_|?

        A generic repository for **files** (and **ONLY FILES**) accessible using an URL. An ideal choice for http or file-system based repositories. Technical note |_|: the supported protocols depends on the protocol handlers available in the jvm at runtime, so adding your own handlers will naturally increase the range of addresses accessible from SKF (For more informations, please consult the `Java Documentation <https://docs.oracle.com/javase/6/docs/api/java/net/URL.html>`_).

**Configuration** |_|: A simple .properties file dropped in the 'repositories' directory of your test project. The file must contain AT LEAST 'squashtest.ta.url.base'.

Available properties are |_|:

        * **squashtest.ta.url.base** |_|: The base url representing your repository.
        * **squashtest.ta.url.useCache** |_|: Whether to use a cache, to speed up future references to recurrent resources.
        * **squashtest.ta.url.login** |_|: Specifies a username for http authentication (special, see below).
        * **squashtest.ta.url.password** |_|: Specifies the password for the username above (special, see below).

**HTTP Authentication** |_|: The login and password options above only hold for http authentication (protocols listed `here <https://docs.oracle.com/javase/6/docs/technotes/guides/net/http-auth.html>`_). It may also fail if the http server implements a custom protocol.

.. note:: 

        You can also use the URL repository as a cheap replacement for the FTP repository. You must then format the url with respect to the standard |_|: ftp://user:password@host:port/path/to/basedir. In that case the login and password properties are useless (since they're inlined in the url itself).

**Example of valid configuration file pointing to a .txt file** |_|::

        # note that the space in 'Program Files' is url encoded.

        squashtest.ta.url.base = file:///C:/Program%20Files/fileTest.txt
        squashtest.ta.ftp.useCache = false
..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################################
Commons component plugin - Macros - Logs
########################################

.. contents:: Contents |_|:
   :local:

|

******************
# DEBUG $(message)
******************

.. admonition:: What |_|?

        This macro allows to write a message in the console with the DEBUG status.

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE log WITH $({message}) USING $(logLevel:DEBUG) AS whatever

**> Input** |_|:

        * ``message`` |_|: The message to display.

**Example** |_|:

    .. list-table::

        * - :macros:`# DEBUG $(This is a debug message logged with a macro)`

    **SKF Script** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-debug-macro.png

    **To be able to see the message in the console, you need to activate maven debug output.**

    .. figure:: ../../_static/plugin-commons-component/log/log-debug-macro-debug-output.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-debug-macro-result.png

|

----

******************
# ERROR $(message)
******************

.. admonition:: What |_|?

        This macro allows to write a message in the console with the ERROR status.

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE log WITH $({message}) USING $(logLevel:ERROR) AS whatever

**> Input** |_|:

        * ``message`` |_|: The message to display.

**Example** |_|:

    .. list-table::

        * - :macros:`# ERROR $(This is an error message logged with a macro)`

    **SKF Script** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-error-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-error-macro-result.png

|

----

*****************
# INFO $(message)
*****************

.. admonition:: What |_|?

        This macro allows to write a message in the console with the INFO status.

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE log WITH $({message}) USING $(logLevel:INFO) AS whatever

**> Input** |_|:

        * ``message`` |_|: The message to display.

**Example** |_|:

    .. list-table::

        * - :macros:`# INFO $(This is an info message logged with a macro)`

    **SKF Script** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-info-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-info-macro-result.png

|

----

*****************
# WARN $(message)
*****************

.. admonition:: What |_|?

        This macro allows to write a message in the console with the WARN status.

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE log WITH $({message}) USING $(logLevel:WARN) AS whatever

**> Input** |_|:

        * ``message`` |_|: The message to display.

**Example** |_|:

    .. list-table::

        * - :macros:`# WARN $(This is a warn message logged with a macro)`

    **SKF Script** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-warn-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-warn-macro-result.png

|

----

*************************************************
# LOG FILE CONTENT FROM {file} WITH LEVEL {level}
*************************************************

.. admonition:: What |_|?

        This macro allows to write the content of a file in the console with the status of your choice (DEBUG,INFO,WARN,ERROR).

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {file} AS __target{%%r1}
    EXECUTE log WITH __target{%%r1} USING $(logLevel:{level},multiline:yes) AS $()

**> Input** |_|:

        * ``file`` |_|: The file which you want to display the content.

**Example** |_|:

    .. list-table::

        * - :macros:`# LOG FILE CONTENT FROM folder/example.txt WITH LEVEL INFO`

    **File to log** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-multi-file-macro-first-content-file.png

    **SKF Script** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-file-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-file-macro-result.png

|

----

**********************************************************************
# LOG FILE CONTENT FROM {content1} , {otherContent} WITH LEVEL {level}
**********************************************************************

.. admonition:: What |_|?

        This macro allows to write the content of multiple files in the console with the status of your choice (DEBUG,INFO,WARN,ERROR).

**Underlying instruction** |_|:

.. code-block:: html

    # LOG FILE CONTENT FROM {content1} WITH LEVEL {level}
    # LOG FILE CONTENT FROM {otherContent} WITH LEVEL {level}

**> Input** |_|:

        * ``content1`` |_|: The selected file which you want to display the content.
        * ``otherContent`` |_|: Another file which you want to display the content.

**Example** |_|:

    .. list-table::

        * - :macros:`# LOG FILE CONTENT FROM folder/example.txt , folder/example2.txt WITH LEVEL WARN`

    **First file to log** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-multi-file-macro-first-content-file.png

    **Second file to log** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-multi-file-macro-second-content-file.png

    **Resource folder where files  to log are stored** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-multi-file-macro-resources-folder.png

    **SKF Script** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-multi-file-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/log/log-multi-file-macro-result.png

|

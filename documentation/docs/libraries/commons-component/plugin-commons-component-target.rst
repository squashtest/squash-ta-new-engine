..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################################
Commons component plugin - Target
#################################

.. admonition:: Remark

        All '.properties' file corresponding to a target file must contain a shebang on the first line (example for a database target the shebang is |_|: **#!db**).

|

****
http
****

**Category-name** |_|: :guilabel:`http`

.. admonition:: What |_|?

        The http target represents a basic http endpoint.

**Configuration** |_|: A simple .properties file dropped in the 'targets' directory of your test project. The file must contain EXACTLY '**squashtest.tf.endpoint.url**'. It must also include a shebang on the very first line |_|: '**#!http**'.

**Example of valid configuration file** |_|::

        #!http

        squashtest.ta.http.endpoint.url = http://www.google.com/pacman/
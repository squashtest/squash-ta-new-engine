..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

####################################################
Commons component plugin - Macros - Substitute dates
####################################################

.. contents:: Contents |_|:
   :local:
   
.. _commons.component.substitute.dates.macro.anchor:

|

**************************************************
# SUBSTITUTE DATES IN {files} AS {processed_files}
**************************************************

.. admonition:: What |_|?

    This macro allows to replace dates in a bundle of files. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of files that have been processed.

**Example** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE DATES IN resources_folder AS result_bundle`

    **First file to process** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro-example-file.png

    **Second file to process** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro-example-file2.png

    **The folder containing files to process which corresponds to** ``{files}`` |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro-resources-folder.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro.png

    In order to check that the macro worked properly, we added in this example the instruction **"LOAD result AS resultOutput"** to be able to see the result output.

    You can access to the result output in the following folder which contains temporary files |_|:

    .. list-table::

        * - C:\\Users\\*user name*\\AppData\\Local\\Temp\\Squash_TA\\20190909_121048_957615127627872437436\\tests\\tests\\substitute_dates.ta\\date88302692580700934357655997463584302temp

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro-temporary-file-folder.png

    **Result output for first file (script executed on 09/09/2019)** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro-example-file-output.png

    **Result output for second file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-macro-example-file-output2.png

| 

--------------------

.. _commons.component.macros.substitute.dates.files.matching.anchor:

*****************************************************************************************
# SUBSTITUTE DATES IN {files} FOR FILES WHOSE NAMES MATCHING {regex} AS {processed_files}
*****************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates in a bundle of files whose names match a specific regular expression. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING $(squashtest.ta.param.include:{regex}) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Example** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE DATES IN resources_folder FOR FILES WHOSE NAMES MATCHING .xml AS result_bundle`

    This example is based on the previous one. For more details, please check :ref:`here <commons.component.substitute.dates.macro.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-matching-regex-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-matching-regex-macro-result.png

    The **.txt** file which does not match the selected regex is properly excluded.

    **Result output for first file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-matching-regex-macro-exemple-file-output1.png

    **Result output for second file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-matching-regex-macro-exemple-file-output2.png

    The **.txt** file is not processed whereas the **.xml** is.


| 

-------------------

.. _commons.component.macros.substitute.dates.files.not.matching.anchor:

*********************************************************************************************
# SUBSTITUTE DATES IN {files} FOR FILES WHOSE NAMES NOT MATCHING {regex} AS {processed_files}
*********************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates in a bundle of files whose names don't match a specific regular expression. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING $(squashtest.ta.param.exclude:{regex}) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Example** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE DATES IN resources_folder FOR FILES WHOSE NAMES NOT MATCHING .xml AS result_bundle`

    This example is based on the first one. For more details, please check :ref:`here <commons.component.substitute.dates.macro.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-not-matching-regex-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-not-matching-regex-macro-result.png

    The **.xml** file which match the selected regex is properly excluded.

    **Result output for first file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-not-matching-regex-macro-exemple-file-output1.png

    **Result output for second file** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-not-matching-regex-macro-exemple-file-output2.png

    The **.xml** file is not processed whereas the **.txt** is.


| 

--------------------

.. _commons.component.macros.substitute.dates.using.anchor:

****************************************************************
# SUBSTITUTE DATES IN {files} USING {dates} AS {processed_files}
****************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates in a bundle of files with dates you have specified. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {dates} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING placeholder{%%rand3}.properties AS {processed_files}


**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{dates}`` |_|: It can either be a path to a properties file or an inline command with keys and values for the dates you want to specify. 
    
        .. list-table::
        
            * - **Example of inline command** : $(date1=01/01/2001 \\n date2=31/12/2018).

            * - **Example of date in file to process** : ${date(date1,dd/MM/yyyy).format(dd-MM-yyyy)}

**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of files that have been processed.

**Examples** |_|:

    .. list-table::

        * - :macros:`# SUBSTITUTE DATES IN resources_folder USING resources_folder/dates.properties AS result_bundle`

    **Or**

    .. list-table::

        * - :macros:`# SUBSTITUTE DATES IN resources_folder USING $(date1=01/01/2001 \\n date2=31/12/2018) AS result_bundle`

    
    This example is based on the first one. For more details, please check :ref:`here <commons.component.substitute.dates.macro.anchor>`.

    
    **File to process** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-using-properties-macro-example-file.png

    **.properties File** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-using-properties-macro-properties-file.png

    **.properties File Location** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-using-properties-macro-properties-file-location.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-using-properties-macro.png

    **Console output** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-using-properties-macro-result.png

    **Result output** |_|:

    .. figure:: ../../_static/plugin-commons-component/substitute-dates/substitute-dates-using-properties-macro-example-file-output.png 

    
| 

--------------------

*******************************************************************************************************
# SUBSTITUTE DATES IN {files} USING {dates} FOR FILES WHOSE NAMES MATCHING {regex} AS {processed_files}
*******************************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates (with dates you have specified) in a bundle of files whose names match a specific regular expression. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {dates} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.include:{regex}) AS {processed_files}
 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{dates}`` |_|: It can either be a path to a properties file or an inline command with keys and values for the dates you want to specify.

        .. list-table::
        
            * - **Example of inline command** : $(date1=01/01/2001 \\n date2=31/12/2018).

            * - **Example of date in file to process** : ${date(date1,dd/MM/yyyy).format(dd-MM-yyyy)}

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

.. list-table::

    * - :macros:`# SUBSTITUTE DATES IN resources_folder USING resources_folder/dates.properties FOR FILES WHOSE NAMES MATCHING .xml AS result_bundle`

**Or**

.. list-table::

    * - :macros:`# SUBSTITUTE DATES IN resources_folder USING $(date1=01/01/2001 \\n date2=31/12/2018) FOR FILES WHOSE NAMES MATCHING .xml AS result_bundle`

For more infomation, please check this :ref:`example <commons.component.macros.substitute.dates.using.anchor>` and this :ref:`one <commons.component.macros.substitute.dates.files.matching.anchor>`.

| 

--------------------

***********************************************************************************************************
# SUBSTITUTE DATES IN {files} USING {dates} FOR FILES WHOSE NAMES NOT MATCHING {regex} AS {processed_files}
***********************************************************************************************************

.. admonition:: What |_|?

    This macro allows to replace dates (with dates you have specified) in a bundle of files whose names don't match a specific regular expression. For more information about the formulas to use in order to replace dates, please check this :ref:`page<param.relativedate Overview>`.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {dates} AS placeholder{%%rand2}.file
    CONVERT placeholder{%%rand2}.file TO properties(structured) AS placeholder{%%rand3}.properties
    LOAD {files} AS __bundle{%%rand1}
    CONVERT __bundle{%%rand1} TO file(param.relativedate) USING placeholder{%%rand3}.properties,$(squashtest.ta.param.exclude:{regex}) AS {processed_files}

 
**> Input** |_|:
   
        * ``{files}`` |_|: The bundle of files where you want to apply the substitution.

        * ``{dates}`` |_|: It can either be a path to a properties file or an inline command with keys and values for the dates you want to specify.

        .. list-table::
        
            * - **Example of inline command** : $(date1=01/01/2001 \\n date2=31/12/2018).

            * - **Example of date in file to process** : ${date(date1,dd/MM/yyyy).format(dd-MM-yyyy)}

        * ``{regex}`` |_|: The regular expression used to filter the files in the bundle.

**> Output** |_|:

        * ``{processed_files}`` |_|: The bundle of filtered files that have been processed.

**Examples** |_|:

.. list-table::

    * - :macros:`# SUBSTITUTE DATES IN resources_folder USING resources_folder/dates.properties FOR FILES WHOSE NAMES NOT MATCHING .txt AS result_bundle`

**Or**

.. list-table::

    * - :macros:`# SUBSTITUTE DATES IN resources_folder USING $(date1=01/01/2001 \\n date2=31/12/2018) FOR FILES WHOSE NAMES NOT MATCHING .txt AS result_bundle`

For more infomation, please check this :ref:`example <commons.component.macros.substitute.dates.using.anchor>` and this :ref:`one <commons.component.macros.substitute.dates.files.not.matching.anchor>`.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###################################
MEN XML Checker Plugin - Converters
###################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

****************
From File to XSD
****************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

    This :guilabel:`structured` converter will convert a ``file`` type resource to a ``xsd`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`xsd` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:xsd>}`

**> Input** |_|:

    * ``resourceToConvert<Res:file>``: The name of the resource to convert (``file`` type resource).

**> Output** |_|:

    * ``converted<Res:xsd>``: The name of the converted resource (``xsd`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` schema.xsd :keywords:`AS` :resources:`schema-resource.file`

        :keywords:`CONVERT` :resources:`schema-resource.file` :keywords:`TO` :resource-type:`xsd` (:converter-name:`structured`) :keywords:`AS` :resources:`schema-resource.xsd`

|

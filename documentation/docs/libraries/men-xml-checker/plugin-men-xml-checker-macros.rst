..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############################
MEN XML Checker Plugin - Macros
###############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

*****************************************************
# ASSERT_XML {xml_path} IS VALID USING XSD {xsd_path}
*****************************************************

.. admonition:: What |_|?

    This macro will verify if an xml file is valid according to a schema (xsd type file).

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {xsd_path} AS __{%%r1}.xsdfile

    LOAD {xml_path} AS __xml_{%%r2}.file
    CONVERT __xml_{%%r2}.file TO file(param.relativedate) AS __temp_{%%r3}.file
    CONVERT __temp_{%%r3}.file TO xml (structured) AS __temp_{%%r4}.xmlfile

    ASSERT __temp_{%%r4}.xmlfile IS valid USING __{%%r1}.xsdfile

**> Input** |_|:

        * ``{xsd_path}`` |_|: The name of the schema (``xsd`` type file).
        * ``{xml_path}`` |_|: The name of the xml file to verify (``xml`` type file).

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT_XML sample-dataset.xml IS VALID USING XSD reference-file.xsd`

    **First file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-1-sample-dataset.png

    **Second file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-1-reference-file.png

    **The folder containing the resources to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-1-tree.png

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-1-script.png

|

----

***************************************************************************
# ASSERT_XML {actual_file} SIMILAIRE TO {expected_file_path} USING {config}
***************************************************************************

.. admonition:: What |_|?

    This macro will verify if an xml file matches another xml. A config file can be used to change the comparison engine.

**Underlying instructions** |_|:

.. code-block:: html
    
    LOAD {actual_file} AS __actual_{%%r1}.file
    LOAD {expected_file_path} AS __expected_{%%r1}.file
    CONVERT __expected_{%%r1}.file TO file(param.relativedate) AS __expected_{%%r2}.file
    CONVERT __expected_{%%r2}.file TO xml (structured) AS __expected_{%%r3}.xmlfile

    CONVERT __actual_{%%r1}.file TO xml (structured) AS __actual_{%%r4}.xmlfile

    ASSERT __expected_{%%r3}.xmlfile IS similaire WITH  __actual_{%%r4}.xmlfile USING {config}

**> Input** |_|:

        * ``{actual_file}`` |_|: The name of the file to compare (``xml`` type file).
        * ``{expected_file_path}`` |_|: The name of the file to be compared to (``xml`` type file).
        * ``{config}`` |_|: The name of the loaded configuration resource (``file`` type resource). It can be used to change the default comparison engine from jaxb to xmlunit, through a ‘comparateur:xmlunit’ entry. The default comparator can also be changed directly with $(comparateur:xmlunit). A ``xsd`` resource can be specified here if using the jaxb comparator.

        If you are using jaxb (not xmlunit), which is also the default comparator used by the macro if you have not defined the USING clause, a few more steps are necessary.

        You need to load and convert an .xsd file and add it to the using clause :

        .. code-block:: html

            TEST:
            LOAD schema.xsd AS schema
            CONVERT schema TO xsd(structured) AS convertedSchema
            #ASSERT_XML base.xml SIMILAIRE TO to_be_compared.xml USING $(comparateur:jaxb),convertedSchema

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT_XML sample-dataset-1.xml SIMILAIRE TO sample-dataset-2.xml USING config-resource.file`

    **First file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-2-sample-dataset-1.png

    **Second file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-2-sample-dataset-2.png

    **Third file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-2-config.png

    **The folder containing the resources to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-2-tree.png

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-2-script.png

    **Console output in DEBUG mode** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-men-xml-checker/assert-xml/men-xml-checker-macros-assert-xml-2-stacktrace.png

|

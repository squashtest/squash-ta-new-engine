..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################################
MEN XML Checker Plugin - Asserts
################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

**************************
'file.xml' is 'valid' 
**************************

.. admonition:: What |_|?

    This assertion verifies if a xml file is valid. If the assertion is verified the test continues, else the test fails.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{resourceToAssert<Res:xml>}` :keywords:`IS` :converter-name:`valid`

**> Input** |_|:

    * ``{resourceToAssert<Res:xml>}`` |_|: The name of the resource to assert (``xml`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` schema.xsd :keywords:`AS` :resources:`schema-resource.file`

        :keywords:`LOAD` sample.xml :keywords:`AS` :resources:`sample-resource.file`

        :keywords:`CONVERT` :resources:`sample-resource.file` :keywords:`TO` :resource-type:`file` (:converter-name:`param.relativedate`) :keywords:`AS` :resources:`sample-resource-relative-date.file`

        :keywords:`CONVERT` :resources:`sample-resource-relative-date.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`sample-resource-relative-date.xml`

        :keywords:`ASSERT` :resources:`sample-resource-relative-date.xml` :keywords:`IS` :converter-name:`valid` :keywords:`USING` :resources:`schema-resource.file`

|

----------

*******************************************
'file1.xml' is 'similaire' with 'file2.xml'
*******************************************

.. admonition:: What |_|?

    This assertion verifies if a xml file matches another xml file. If the assertion is verified the test continues, else the test fails.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{resourceToAssert<Res:xml>}` :keywords:`IS` :converter-name:`similaire` :keywords:`WITH` :resources:`{resourceToCompareTo<Res:xml>}` :keywords:`USING` :resources:`{config}`

**> Input** |_|:

    * ``{resourceToAssert<Res:xml>}`` |_|: The name of the resource to assert (``xml`` type resource).

    * ``{resourceToCompareTo<Res:xml>}`` |_|: The name of the resource to compare to (``xml`` type resource).

    * ``{config}`` |_|:  The name of the loaded configuration resource (``file`` type resource). It can be used to change the default comparison engine from jaxb to xmlunit, through a ‘comparateur:xmlunit’ entry. The defalut comparator can also be changed directly with $(comparateur:xmlunit). A ``xsd`` resource can be specified here if using the jaxb comparator.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` actual.xml :keywords:`AS` :resources:`actual-resource.file`

        :keywords:`CONVERT` :resources:`actual-resource.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`actual-resource.xml`

        :keywords:`LOAD` expected.xml :keywords:`AS` :resources:`expected-resource.file`

        :keywords:`CONVERT` :resources:`expected-resource.file` :keywords:`TO` :resource-type:`file` (:converter-name:`param.relativedate`) :keywords:`AS` :resources:`expected-resource-relative-date.file`

        :keywords:`CONVERT` :resources:`expected-resource-relative-date.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`expected-resource-relative-date.xml`

        :keywords:`ASSERT` :resources:`actual-resource.xml` :keywords:`IS` :converter-name:`similaire` :keywords:`WITH` :resources:`expected-resource-relative-date.xml` :keywords:`USING` $(comparateur:xmlunit)

|

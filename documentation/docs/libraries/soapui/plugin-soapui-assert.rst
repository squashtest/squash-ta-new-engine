..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################
SoapUI Plugin - Assert
######################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

****************************
'result.soapui' is 'success'
****************************

.. admonition:: What |_|?

    This assertion verifies if a soapUI execution is a success. If the assertion is verified then the test continues. In the other case, the test fails.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{soapuiResult<Res:result.soapui>}` :keywords:`IS` :converter-name:`success`

        :keywords:`VERIFY` :resources:`{soapuiResult<Res:result.soapui>}` :keywords:`IS` :converter-name:`success`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{soapuiResult<Res:result.soapui>}`` |_|: The name of the resource which contains the result of a soapUI execution command (``result.soapui`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/soapui_script.xml :keywords:`AS` :resources:`soapui_script.file`

        :keywords:`CONVERT` :resources:`soapui_script.file` :keywords:`TO` :resource-type:`script.soapui` (:converter-name:`structured`) :keywords:`AS` :resources:`soapui_script.soapui`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`soapui_script.soapui` :keywords:`USING` $(soapui.test.suites=suite1,suite2;soapui.test.cases=tc1,tc2) :keywords:`AS` :resources:`soapuiResult`

        :keywords:`ASSERT` :resources:`soapuiResult` :keywords:`IS` :converter-name:`success`

|

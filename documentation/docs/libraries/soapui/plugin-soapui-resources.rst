..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################
SoapUI Plugin - Resources
#########################

.. contents:: Contents |_|:
   :local:

|

*************
script.soapui 
*************

**Category-name** |_|: :guilabel:`script.soapui`

.. admonition:: What |_|?

    :guilabel:`script.soapui` is a resource type that represents a SoapUI workspace. This resource can reference either a single xml workspace file as produced by SoapUI or (since **1.7**) a bundle that contains an xml workspace.

| 

--------------------

*************
result.soapui  
*************

**Category-name** |_|: :guilabel:`result.soapui`

.. admonition:: What |_|?

    :guilabel:`result.soapui` is a resource type that represents the result of the execution of SoapUI tests. It is produced by the :ref:`'execute' 'script.soapui'<soapui.command.execute.script.soapui.anchor>` command.

|

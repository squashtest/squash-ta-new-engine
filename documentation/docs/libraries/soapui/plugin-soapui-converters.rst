..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##########################
SoapUI Plugin - Converters
##########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

******************************
From 'file' to 'script.soapui' 
******************************

**Category-name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

    This :guilabel:`structured` script converter will convert xml ``file`` resources to ``script.soapui`` resources. The converted resource is then ready for execution through the :ref:`'execute' 'script.soapui'<soapui.command.execute.script.soapui.anchor>` command.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{xmlSoapUI<Res:file>}` :keywords:`TO` :resource-type:`script.soapui` (:converter-name:`script`) [:keywords:`USING` :resources:`$(soapui.project.path:<soapui.project.path>)`] :keywords:`AS` :resources:`{converted<Res:script.soapui>}`

**> Input** |_|:

    * ``{xmlSoapui<Res:file>}`` |_|: The name of the resource to convert (``file`` type resource). This resource references a single xml workspace file as produced by SoapUI or (since **1.7**) a bundle containing such a file.
    * ``soapui.project.path`` (since **1.7**) |_|: In case xmlSoapui is a bundle, we can indicate here the path to the xml workspace file relatively to the bundle's root.

**> Output** |_|:

    * ``{converted<Res:script.soapui>}`` |_|: The name of the converted resource (``script.soapui`` type resource).

**Note** |_|:

    * If ``script.soapui`` is a bundle, we must indicate the path of the xml workspace file either in the convert or the command instruction.
    * If the path is indicated in both, the command instruction prevails.
    * If it is not indicated, the test will fail.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/soapui_script.xml :keywords:`AS` :resources:`soapui_script.file`

        :keywords:`CONVERT` :resources:`soapui_script.file` :keywords:`TO` :resource-type:`script.soapui` (:converter-name:`structured`) :keywords:`AS` :resources:`soapui_script.soapui`

|

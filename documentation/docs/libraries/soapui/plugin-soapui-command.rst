..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#######################
SoapUI Plugin - Command
#######################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

.. _soapui.command.execute.script.soapui.anchor:

*************************
'execute' 'script.soapui' 
*************************

.. admonition:: What |_|?

    This command executes the test suite defined by the ``script.soapui`` resource given as input. It is executed on the (implicit) void target because the SUT target is defined by the SoapUI workspace and cannot be overriden.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{soapuiScript<Res:script.soapui>}` :keywords:`AS` :resources:`{soapuiResult<Res:result.soapui>}` [ :keywords:`USING` :resources:`$(<soapui.test.suites>;<soapui.test.cases>;<soapui.project.path>)`]

**> Input** |_|:

    * ``{soapuiScript<Res:script.soapui>}`` |_|: This resource references a single xml workspace file as produced by SoapUI, or (since **1.7**) a bundle containing such a file (``script.soapui`` type resource).
    * ``soapui.test.suites`` |_|: Expected value is the comma separated list of test suite names to execute. If this key is not defined or if it is an empty string, then all test suites are selected.
    * ``soapui.test.cases`` |_|: Expected value is the comma separated list of the test case names to execute in the selected test suites. If this key is not defined or if its value is an empty string, then all test cases are selected.
    * ``soapui.project.path`` (since **1.7**) |_|: In case ``script.soapui`` is a bundle, we can indicate here the path to the xml workspace file relatively to the bundle's root.

**> Output** |_|:

    * ``{soapuiResult<Res:result.soapui>}`` |_|: The name of the resource which contains the result of the SoapUI command execution (``soapui.result``  type resource).

**Note** |_|:

    * If ``script.soapui`` is a bundle, we must indicate the path of the xml workspace file either in the convert or the command instruction.
    * If the path is indicated in both, the command instruction prevails.
    * If it is not indicated, the test will fail.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/soapui_script.xml :keywords:`AS` :resources:`soapui_script.file`

        :keywords:`CONVERT` :resources:`soapui_script.file` :keywords:`TO` :resource-type:`script.soapui` (:converter-name:`structured`) :keywords:`AS` :resources:`soapui_script.soapui`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`soapui_script.soapui` :keywords:`USING` $(soapui.test.suites=suite1,suite2;soapui.test.cases=tc1,tc2) :keywords:`AS` :resources:`result`

|

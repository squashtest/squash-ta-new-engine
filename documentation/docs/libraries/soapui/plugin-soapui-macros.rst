..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################
SoapUI Plugin - Macros
######################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

.. _libraries.macros.execute.soapui.anchor:

********************************
# EXECUTE_SOAPUI {soapui_script}
********************************

.. admonition:: What |_|?

    This macro loads and executes a {soapui_script}. It then verifies the success of the execution.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {soapui_script} AS __soapui_script{%%rand1}.file
    CONVERT __soapui_script{%%rand1}.file TO script.soapui(structured) AS __soapui_script{%%rand2}.soapui

    EXECUTE execute WITH __soapui_script{%%rand2}.soapui AS __exec{%%rand3}.result
    ASSERT __exec{%%rand3}.result IS success

**> Input** |_|:

        * ``{soapui_script}`` |_|: path to a SoapUI xml workspace file. It will be converted to a ``soapui.script`` resource.

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE_SOAPUI path/to/soapui-script.xml`

    **SoapUI Project as a .xml file in the resources directory** |_|:

    .. figure:: ../../_static/plugin-soapui/doc-soapui-project.png
        :align: center
        :target: ../../_static/plugin-soapui/doc-soapui-project.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-soapui/doc-soapui-macro-execute.png
        :align: center
        :target: ../../_static/plugin-soapui/doc-soapui-macro-execute.png

    **Result output on error in the SKF script** |_|:

        In this example we misspelled the name of the SoapUI project in the macro.

    .. figure:: ../../_static/plugin-soapui/doc-soapui-macro-execute-skf-error.png
        :align: center
        :target: ../../_static/plugin-soapui/doc-soapui-macro-execute-skf-error.png

    **Result output on failure of a test case in the SoapUI project** |_|:

    .. figure:: ../../_static/plugin-soapui/doc-soapui-macro-execute-soap-fail.png
        :align: center
        :target: ../../_static/plugin-soapui/doc-soapui-macro-execute-soap-fail.png

| 

--------------------

*************************************************************
# EXECUTE_SOAPUI {soapui_script} WITH TEST_SUITE {testsuites}
*************************************************************

.. admonition:: What |_|?

    This macro loads the {soapui_script} and executes the given {testsuites}. It then verifies the success of the execution.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {soapui_script} AS __soapui_script{%%rand1}.file
    CONVERT __soapui_script{%%rand1}.file TO script.soapui(structured) AS __soapui_script{%%rand2}.soapui

    EXECUTE execute WITH __soapui_script{%%rand2}.soapui USING $(soapui.test.suites:{testsuites};soapui.test.cases:{testcases}) AS __exec{%%rand5}.result
    ASSERT __exec{%%rand5}.result IS success

**> Input** |_|:

        * ``{soapui_script}`` |_|: path to a SoapUI xml workspace file. It will be converted to a ``soapui.script`` resource.
        * ``{testsuites}`` |_|: names of the test suites of the SoapUI workspace to execute. It could be one test suite or a list of comma separated test suites to execute.

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE_SOAPUI path/to/soapui-script.xml WITH TEST_SUITE testsuite_name`

    **SKF script with 2 test suites** |_|:

    .. figure:: ../../_static/plugin-soapui/doc-soapui-macro-execute-with.png
        :align: center
        :target: ../../_static/plugin-soapui/doc-soapui-macro-execute-with.png

| 

--------------------

***************************************************************************************
# EXECUTE_SOAPUI {soapui_script} WITH TEST_SUITE {testsuites} AND TEST_CASE {testcases}
***************************************************************************************

.. admonition:: What |_|?

    This macro loads a SoapUI xml workspace and executes the given test case(s). The macro then verifies the success of the execution.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {soapui_script} AS __soapui_script{%%rand1}.file
    CONVERT __soapui_script{%%rand1}.file TO script.soapui(structured) AS __soapui_script{%%rand2}.soapui

    EXECUTE execute WITH __soapui_script{%%rand2}.soapui USING $(soapui.test.suites:{testsuites};soapui.test.cases:{testcases}) AS __exec{%%rand5}.result
    ASSERT __exec{%%rand5}.result IS success

**> Input** |_|:

        * ``{soapui_script}`` |_|: path to a SoapUI xml workspace file. It will be converted to a ``soapui.script`` resource.
        * ``{testsuites}`` |_|: names of test suites of SoapUI workspace to execute. It could be one test suite or a list of comma separated test suites to execute.
        * ``{testcases}`` |_|: names of test cases to execute in the test suite. It could be only one test case or a comma separated list of test cases.

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE_SOAPUI path/to/soapui-script.xml WITH TEST_SUITE issueServiceTest AND TEST_CASE retrieveIssue,issueExists`

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-soapui/doc-soapui-macro-execute-with-and.png
        :align: center
        :target: ../../_static/plugin-soapui/doc-soapui-macro-execute-with-and.png

| 

--------------------

******************************************************************
# EXECUTE_SOAPUI_BUNDLE {soapui_bundle} WITH PROJECT {projectpath}
******************************************************************

.. admonition:: What |_|?

    This macro executes a SoapUI script embedded in a bundle. The macro then verifies the success of the execution.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {soapui_bundle} AS __soapui_bundle{%%rand1}.file
    CONVERT __soapui_bundle{%%rand1}.file TO script.soapui(structured) AS __soapui_bundle{%%rand2}.soapui

    EXECUTE execute WITH __soapui_bundle{%%rand2}.soapui USING $(soapui.project.path:{projectpath}) AS __exec{%%rand3}.result
    ASSERT __exec{%%rand3}.result IS success

**> Input** |_|:

        * ``{soapui_bundle}`` |_|: path to the SoapUI bundle to load.
        * ``{projectpath}`` |_|: path to the SoapUI xml workspace file (relative to the root of the bundle).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SOAPUI_BUNDLE soapui WITH PROJECT soapui-integration-tests.xml`

| 

--------------------

**********************************************************************************************
# EXECUTE_SOAPUI_BUNDLE {soapui_bundle} WITH PROJECT {projectpath} AND TEST_SUITE {testsuites}
**********************************************************************************************

.. admonition:: What |_|?

    This macro executes the given test suites of the loaded SoapUI bundle. The macro then verifies the success of the execution.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {soapui_bundle} AS __soapui_bundle{%%rand1}.file
    CONVERT __soapui_bundle{%%rand1}.file TO script.soapui(structured) AS __soapui_bundle{%%rand2}.soapui

    EXECUTE execute WITH __soapui_bundle{%%rand2}.soapui USING $(soapui.project.path:{projectpath};soapui.test.suites:{testsuites}) AS __exec{%%rand5}.result
    ASSERT __exec{%%rand5}.result IS success

**> Input** |_|:

        * ``{soapui_bundle}`` |_|: path to SoapUI bundle to load.
        * ``{projectpath}`` |_|: path to SoapUI xml workspace file (relative to the root of the bundle).
        * ``{testcases}`` |_|: names of testsuites of soapui workspace to execute. It could be one test suite or a list of comma separated test suites to execute.

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SOAPUI_BUNDLE soapui WITH PROJECT soapui-integration-tests.xml AND TEST_SUITE issueServiceTest`

| 

--------------------

************************************************************************************************************************
# EXECUTE_SOAPUI_BUNDLE {soapui_bundle} WITH PROJECT {projectpath} AND TEST_SUITE {testsuites} AND TEST_CASE {testcases}
************************************************************************************************************************

.. admonition:: What |_|?

    This macro loads a SoapUI bundle and executes the given test cases. The macro then verifies the success of the execution.

**Underlying instructions** |_|:
    
.. code-block:: html

    LOAD {soapui_bundle} AS __soapui_bundle{%%rand1}.file
    CONVERT __soapui_bundle{%%rand1}.file TO script.soapui(structured) AS __soapui_bundle{%%rand2}.soapui

    EXECUTE execute WITH __soapui_bundle{%%rand2}.soapui USING $(soapui.project.path:{projectpath};soapui.test.suites:{testsuites};soapui.test.cases:{testcases}) AS __exec{%%rand5}.result
    ASSERT __exec{%%rand5}.result IS success

**> Input** |_|:

        * ``{soapui_bundle}`` |_|: path to the SoapUI bundle to load.
        * ``{projectpath}`` |_|: path to SoapUI xml workspace file (relative to the root of the bundle).
        * ``{testsuites}`` |_|: names of test suites of SoapUI workspace to execute. It could be one test suite or a list of comma separated test suites to execute.
        * ``{testcases}`` |_|: names of test cases to execute in the test suite. It could be only one test case or a comma separated list of test cases.

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SOAPUI_BUNDLE soapui WITH PROJECT soapui-integration-tests.xml AND TEST_SUITE issueServiceTest AND TEST_CASE retrieveIssue,issueExists`

|

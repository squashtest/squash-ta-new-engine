..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
SSH/SFTP Plugin - Macros
########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

**********************************************************
# EXECUTE_SSH $({command_content}) ON {target} AS {result}
**********************************************************

.. admonition:: What |_|?

   This macro will execute an inline command on a SSH server

**Underlying instructions** |_|:

.. code-block:: html

    DEFINE $({command_content}) AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell AS __commandLine{%%rand2}
    EXECUTE execute WITH __commandLine{%%rand2} ON {target} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: It corresponds to the text of the shell command to execute.
        * ``{target}`` |_|: The name (in the context) of the SSH server to use. (``ssh.target`` type target).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command.(``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SSH $(echo "hello world") ON ssh-server AS result`

|

----

******************************************************************************
# EXECUTE_SSH $({command_content}) ON {target} AS {result} WITHIN {timeout} ms
******************************************************************************

.. admonition:: What |_|?

        This macro will execute an inline command on a SSH server.

**Underlying instructions** |_|:

.. code-block:: html

    DEFINE $({command_content}) AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell AS __commandLine{%%rand2}
    EXECUTE execute WITH __commandLine{%%rand2} ON {target} USING $(timeout:{timeout}) AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{target}`` |_|: The name (in the context) of the SSH server to use (``ssh.target`` type target).

        * ``{command_content}`` |_|: It corresponds to the text of the shell command to execute.

        * ``{timeout}`` |_|: Maximal time authorized for the command execution (in milliseconds).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command(``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SSH $(echo "hello world") ON ssh-server AS result WITHIN 15000 ms`

|

----

*****************************************************
# EXECUTE_SSH_SCRIPT {script} ON {target} AS {result}
*****************************************************

.. admonition:: What |_|?

        This macro will execute a shell script on a SSH server.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {script} AS __{%%r1}.file
    CONVERT __{%%r1}.file TO file(param.relativedate) AS __{%%r2}.file
    CONVERT __{%%r2}.file TO query.shell AS __{%%r3}.script
    EXECUTE execute WITH __{%%r3}.script ON {target} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{target}`` |_|: The name (in the context) of the SSH server to use (``ssh.target`` type target).

        * ``{script}`` |_|: It corresponds to the path of the shell script to execute.

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SSH_SCRIPT shell/shell-script.txt ON ssh-server AS result`

|

----

*************************************************************************
# EXECUTE_SSH_SCRIPT {script} ON {target} AS {result} WITHIN {timeout} ms
*************************************************************************

.. admonition:: What |_|?

        This macro will execute a shell script on a SSH server.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {script} AS __{%%r1}.file
    CONVERT __{%%r1}.file TO file(param.relativedate) AS __{%%r2}.file
    CONVERT __{%%r2}.file TO query.shell AS __{%%r3}.script
    EXECUTE execute WITH __{%%r3}.script ON {target} USING $(timeout:{timeout}) AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{target}`` |_|: The name (in the context) of the SSH server to use (``ssh.target`` type target).

        * ``{script}`` |_|: It corresponds to the path of the shell script to execute.

        * ``{timeout}`` |_|: Maximal time authorized for the command execution (in milliseconds).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command(``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SSH_SCRIPT shell/shell-script.txt ON ssh-server AS result WITHIN 15000 ms`

|

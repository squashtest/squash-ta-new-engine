..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################
SSH/SFTP Plugin - Asserts
#########################

.. contents:: Contents |_|:
   :local:

|

***************************
'result.shell' is 'success'  
***************************

**@See** |_|: Since |squashTA| |_| **1.7.0**, this resource moved to :ref:`Local Process Plugin <result.shell.is.success.anchor>`.

*******************************************************
'result.shell' is 'failure' with {expected return code}  
*******************************************************

**@See** |_|: Since |squashTA| |_| **1.7.0**, this resource moved to :ref:`Local Process Plugin <result.shell.is.failure.anchor>`.

*************************************
'result.shell' does 'contain' {regex}
*************************************

**@See** |_|: Since |squashTA| |_| **1.7.0**, this resource moved to :ref:`Local Process Plugin <result.shell.does.contain.anchor>`.

*****************************************
'result.shell' does 'not.contain' {regex} 
*****************************************

**@See** |_|: Since |squashTA| |_| **1.7.0**, this resource moved to :ref:`Local Process Plugin <result.shell.does.not.contain.anchor>`.

|

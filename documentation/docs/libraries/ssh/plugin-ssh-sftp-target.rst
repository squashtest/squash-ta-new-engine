..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
SSH/SFTP Plugin - Target
########################

|

***
SSH
***

**Category-name** |_|: :guilabel:`ssh.target`

.. admonition:: What |_|?

        The :guilabel:`ssh.target` represents a ssh server used for the execution of commands. This is mainly used as SUT specification for batch testing.

**Configuration** |_|: A simple .properties file dropped in the targets directory of your test project. To tag the file as an ssh configuration file, the first line must have the following shebang mark |_|: **#!ssh**. All keys in this file begin with the prefix squashtest.ta.ssh.

**Available parameters** |_|:

    * squashtest.ta.ssh.hostname (mandatory) |_|: Host to connect to.
    * squashtest.ta.ssh.port |_|: Port to connect to. This parameter is optional, if it is ommitted or empty the default SSH port will be used.
    * squashtest.ta.ssh.username (mandatory) |_|: Username to use for connection.
    * squashtest.ta.ssh.password (mandatory) |_|: Password to use for connection.

**Example of valid configuration file** |_|:

.. code-block:: html

    #!ssh
    squashtest.ta.ssh.hostname=integrationBox
    squashtest.ta.ssh.username=tester
    squashtest.ta.ssh.password=tester

|

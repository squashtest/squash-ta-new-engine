..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##########################
SSH/SFTP Plugin - Commands
##########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

********************************
'execute' 'query.shell' on 'ssh'
********************************

.. admonition:: What |_|?

    It allows to execute a command line over SSH.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{query<Res:query.shell>}` :keywords:`ON` :resources:`{<Tar:ssh.target>}` :keywords:`AS` :resources:`{result<Res:Result.shell>}` [ :keywords:`USING` $(timeout : <n>) ]

**> Input** |_|:

    * ``{query<Res:query.shell>}`` |_|: The name of the resource referencing a file which includes one or several shell command lines (``query.shell`` type resource).
    * ``{<Tar:ssh.target>}`` |_|: The name (in the context) of the SSH server to use (``ssh.target`` type target).
    * ``<n>`` |_|: An integer that represents time in milliseconds. It's the time to wait before the command execution times out. It can be defined via an inline instruction |_|: $(timeout |_|: |_|...)

**Note** |_|:  If the timeout property is not defined here, we use the timeout property of ``query.shell`` resource (set to 5s by default).

**> Output** |_|:

    * ``{result<Res:result.shell>}`` |_|: The name of the resource which contains the shell commands result (``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` shell/shell_command.txt :keywords:`AS` :resources:`command.file`

        :keywords:`CONVERT` :resources:`command.file` :keywords:`TO` query.shell :keywords:`USING` $(timeout:15000) :keywords:`AS` :resources:`commandLine`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`commandLine` :keywords:`ON` :resources:`ssh_server` :keywords:`AS` :resources:`result`


| 

--------------------

**********************
'put' 'file' on 'SFTP'
**********************

.. admonition:: What |_|?

    This command allows to put a file on a SFTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`put` :keywords:`WITH` :resources:`{<Res:file>}` :keywords:`ON` :resources:`{<Tar:ssh.target>}` :keywords:`AS` :resources:`$()` :keywords:`USING` $(remotepath : <distantPath> )

**> Input** |_|:

    * ``{<Res:file>}`` |_|: The name of the resource which references the file to put on the SFTP server (``file`` type resource).
    * ``{<Tar:ssh.target>}`` |_|: The name (in the context) of the SFTP server to use (``ssh.target`` type target).
    * ``<distantPath>`` |_|: It corresponds to the file path on the SFTP server, relatively to the home directory.

**Remark** |_|: If in ``<distantPath>`` some directories don't exist on the server, they are then created.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/toto.xml :keywords:`AS` :resources:`toto`

        :keywords:`EXECUTE` :converter-name:`put` :keywords:`WITH` :resources:`toto` :keywords:`ON` :resources:`SFTP-server` :keywords:`USING` $(remotepath : toto.xml) :keywords:`AS` $()

| 

--------------------

**********************
'get' 'file' on 'SFTP'
**********************

.. admonition:: What |_|?

    This command allows to get a file from a SFTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get` :keywords:`WITH` $() :keywords:`ON` :resources:`{<Tar:ssh.target>}` :keywords:`AS` :resources:`{result<Res:file>}` :keywords:`USING` $(remotepath : <distantPath> )

**> Input** |_|:

    * ``{<Tar:ssh.target>}`` |_|: The name (in the context) of the SFTP server to use (``ssh.target`` type target).
    * ``<distantPath>`` |_|: It corresponds to the file path on the SFTP server, relatively to the home directory of the file you want to get.

**> Output** |_|:

    * ``{result<Res:file>}`` |_|: The name of the resource which references the file you get from the SFTP server (``file`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get` :keywords:`WITH` $() :keywords:`ON` :resources:`SFTP-server` :keywords:`USING` $(remotepath :sample.zip) :keywords:`AS` :resources:`zip`

|

--------------------

*************************
'delete' 'file' on 'SFTP'
*************************

.. admonition:: What |_|?

    This command allows to delete a file on a SFTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`delete` :keywords:`WITH` $() :keywords:`ON` :resources:`{<Tar:ssh.target>}` :keywords:`AS` $() :keywords:`USING` $(remotepath : <distantPath> [,failIfDoesNotExist : false])

**> Input** |_|:

    * ``{<Tar:ssh.target>}`` |_|: The name (in the context) of the SFTP server to use (``ssh.target`` type target).
    * ``<distantPath>`` |_|: It corresponds to the file path on the SFTP server, relatively to the home directory of the file you want to delete.
    * ``'failIfDoesNotExist : false'`` |_|: It allows to specify to SKF that the test must not fail if the resource we're trying to delete doesn't exist.

**> Output** |_|:

    * ``{result<Res:file>}`` |_|: The name of the resource which references the file you get from the SFTP server (``file`` type resource).

**Remarks** |_|:

    * ``<distantPath>`` can indicate a file OR a directory. To represent a directory, the path should end with the character '/'.
    * The deletion of a directory is recursive |_|: deletion of all sub-directories and files.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`delete` :keywords:`WITH`  $() :keywords:`ON` :resources:`SFTP-server` :keywords:`USING` $( remotepath : path/to/myfile.txt, failIfDoesNotExist: false) :keywords:`AS` $()

|

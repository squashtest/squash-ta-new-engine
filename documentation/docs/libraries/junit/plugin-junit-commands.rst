..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#######################
JUnit Plugin - Commands
#######################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

*******
execute
*******

.. admonition:: What |_|?

    Command to execute any Junit test using the JUnit5 framework.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{<Res:script.junit5>}` :keywords:`AS` :resources:`{<Res:result.junit5>}` :keywords:`USING` $({configuration})

**> Input** |_|:

    * ``{<Res:script.junit5>}`` |_|: The name (in the context) of the resource which references the java code bundle, including resources and compiled java classes (``script.junit5`` type resource).
    * ``{configuration}`` |_|:

        * The qualified display name of the test to execute, using :guilabel:`qualifiedClassName:<Specify name>,displayName:<Specify name>`.
        * Or the unique Id of the test, provided by the JUnit engine, using :guilabel:`uniqueId:<specify Id>`.

**Note 1** |_|: These two possible configuration contents are mutually exclusive. Meaning that one has to choose how one wants to select the test to execute. Either using its qualified display name or using its unique Id, but not both.

**Note 2** |_|: If you use the qualified display name, make sure it is unique in the class being tested, or the execution will result in a failure.

**> Output** |_|:

    * ``{<Res:result.junit5>}`` |_|: The name (in the context) of the resource holding the result of the command (``script.junit5`` type resource).

**Exemple with the qualified display name** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/commands/junit-commands-test-class.png

|

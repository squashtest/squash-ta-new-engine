..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################
JUnit Plugin - Converters
#########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

*********************************
From script.java to script.junit5
*********************************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

    This :guilabel:`structured` converter will convert a ``script.java`` type resource to a ``script.junit5`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:script.java>}` :keywords:`TO` :resource-type:`script.junit5` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:script.junit5>}`

**> Input** |_|:

    * ``resourceToConvert<Res:script.java>`` |_|: The name of the resource which references a java code bundle, including resources and compiled java classes (``script.java`` type resource).

**> Output** |_|:

    * ``converted<Res:script.junit5>`` |_|: The name of the converted resource (``script.junit5`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/java-bundle :keywords:`AS` :resources:`java-bundle.file`

        :keywords:`CONVERT` :resources:`java-bundle.file` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`java-bundle.script.java`

        :keywords:`CONVERT` :resources:`java-bundle.script.java` :keywords:`TO` :resource-type:`script.junit5` (:converter-name:`structured`) :keywords:`AS` :resources:`java-bundle.script.junit5`

|

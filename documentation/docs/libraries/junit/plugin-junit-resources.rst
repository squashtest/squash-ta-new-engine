..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
JUnit Plugin - Resources
########################

.. contents:: Contents |_|:
   :local:

|

*************
script.junit5
*************

**Category-name** |_|: :guilabel:`script.junit5`

.. admonition:: What |_|?

        :guilabel:`script.junit5` is a resource type used by JUnit related components. It references a java code bundle, including resources and compiled java classes.

|

--------------------------

*************
result.junit5
*************

**Category-name** |_|: :guilabel:`result.junit5`

.. admonition:: What |_|?

        :guilabel:`result.junit5` is a resource holding the result of any JUnit command.

|

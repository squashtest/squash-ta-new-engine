..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################
JUnit Plugin - Asserts
######################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

****************************
'result.junit5' is 'success' 
****************************

.. admonition:: What |_|?

    This assertion verifies that the result of the execution of a JUnit test is a success. If the assertion is verified the test continues, else the test fails.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{resourceToAssert<Res:result.junit5>}` :keywords:`IS` :converter-name:`success`

**> Input** |_|:

    * ``{resourceToAssert<Res:result.junit5>}`` |_|: The name of the resource to assert (``result.junit5`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium :keywords:`AS` :resources:`bundle.file`

        :keywords:`CONVERT` :resources:`bundle.file` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`bundle.script.java`

        :keywords:`CONVERT` :resources:`bundle.script.java` :keywords:`TO` :resource-type:`script.junit5` (:converter-name:`structured`) :keywords:`AS` :resources:`bundle.script.junit5`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`bundle.script.junit5` :keywords:`AS` :resources:`bundle.result.junit5` :keywords:`USING` $(qualifiedClassName:org.squashtest.Selenium2JUnit4,displayName:testSelenium2JUnit4)

        :keywords:`ASSERT` :resources:`bundle.result.junit5` :keywords:`IS` :converter-name:`success`

|

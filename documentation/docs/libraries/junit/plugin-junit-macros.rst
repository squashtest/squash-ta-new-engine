..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################
JUnit Plugin - Macros
#####################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

************************************************************************
# EXECUTE_JUNIT_TEST {displayName} FROM {qualifiedClass} IN {bundlePath}
************************************************************************

.. admonition:: What |_|?

    This macro will execute a JUnit test and verify that the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {bundlePath} AS junit5{%%r1}.file
    CONVERT junit5{%%r1}.file TO script.java(compile) AS junit5{%%r1}.bundle
    CONVERT junit5{%%r1}.bundle TO script.junit5(structured) AS junit5{%%r1}.script
    EXECUTE execute WITH junit5{%%r1}.script AS junit5{%%1}.result USING $(qualifiedClassName:{qualifiedClass},displayName:{displayName})
    ASSERT junit5{%%1}.result IS success

**> Input** |_|:

        * ``{qualifiedClass}`` |_|: The qualified name of the class containing the test to execute.
        * ``{displayName}`` |_|: The name of the test to execute.

            * For a JUnit4 test, specify the name of the method being tested (**without** parentheses).
            * For a JUnit5 test, you can also specify the name of the method being tested (**with** parentheses), 
              but you **have** to use the display name of the method if you tagged it with *'@DisplayName'*.
              Make sure the display name of the test is unique in the class being tested.
        * ``{bundlePath}`` |_|: The path to the java code bundle, including resources and compiled java classes.

**Example** |_|:

    .. list-table::

        * - JUnit4 |_|: :macros:`# EXECUTE_JUNIT_TEST testSelenium2JUnit4 FROM org.squashtest.Selenium2JUnit4 IN selenium`
        * - JUnit5 |_|: :macros:`# EXECUTE_JUNIT_TEST testSelenium2JUnit5() FROM org.squashtest.Selenium2JUnit5 IN selenium`
        * - JUnit5 (DisplayName) |_|: :macros:`# EXECUTE_JUNIT_TEST Test_Selenium_2_JUnit_5 FROM org.squashtest.Selenium2JUnit5 IN selenium`

    **File to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-1-test-class.png

    **The folder containing the resources to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-1-tree.png

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-1-script.png

|

----

*******************************************************************************************************
# EXECUTE_JUNIT_TEST {displayName} FROM {qualifiedClass} IN {bundlePath} WITH COMPILE OPTIONS {options}
*******************************************************************************************************

.. admonition:: What |_|?

    This macro will execute a JUnit test, with compile options, and verify that the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {bundlePath} AS junit5{%%r1}.file
    CONVERT junit5{%%r1}.file TO script.java(compile) USING {options} AS junit5{%%r1}.bundle
    CONVERT junit5{%%r1}.bundle TO script.junit5(structured) AS junit5{%%r1}.script
    EXECUTE execute WITH junit5{%%r1}.script AS junit5{%%1}.result USING $(qualifiedClassName:{qualifiedClass},displayName:{displayName})
    ASSERT junit5{%%1}.result IS success

**> Input** |_|:

        * ``{qualifiedClass}`` |_|: The qualified name of the class containing the test to execute.
        * ``{displayName}`` |_|: The name of the test to execute.

            * For a JUnit4 test, specify the name of the method being tested (**without** parentheses).
            * For a JUnit5 test, you can also specify the name of the method being tested (**with** parentheses), 
              but you **have** to use the display name of the method if you tagged it with *'@DisplayName'*.
              Make sure the diplay name of the test is unique in the class being tested.
        * ``{bundlePath}`` |_|: The path to the java code bundle, including resources and compiled java classes.
        * ``{options}`` |_|: The name of the configuration resource. It represents a configuration file containing java compilation options (possible options are those of the Java compiler present on the machine). In this file options can be written |_|:

                * In line separated with a space character
                * One option per line
                * A mix of both

**Example** |_|:

    .. list-table::

        * - JUnit4 |_|: :macros:`# EXECUTE_JUNIT_TEST testSelenium2JUnit4 FROM org.squashtest.Selenium2JUnit4 IN selenium WITH COMPILE OPTIONS compile.options.file`
        * - JUnit5 |_|: :macros:`# EXECUTE_JUNIT_TEST testSelenium2JUnit5() FROM org.squashtest.Selenium2JUnit5 IN selenium WITH COMPILE OPTIONS compile.options.file`
        * - JUnit5 (DisplayName) |_|: :macros:`# EXECUTE_JUNIT_TEST Test_Selenium_2_JUnit_5 FROM org.squashtest.Selenium2JUnit5 IN selenium WITH COMPILE OPTIONS compile.options.file`

    **First file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-1-test-class.png

    **Second file to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-2-options.png

    **The folder containing the resources to process** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-2-tree.png

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-junit/execute/junit-macros-execute-2-script.png

|

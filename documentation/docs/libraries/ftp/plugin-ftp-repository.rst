..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#######################
FTP Plugin - Repository
#######################

.. admonition:: What |_|?

    Will download your resources from a FTP.

**Category-name** |_|: :guilabel:`ftp.repository`

**Configuration** |_|: A simple .properties file dropped in the 'repositories' directory of your test project. It must contain AT LEAST |_|: 'squashtest.ta.ftp.host'.

**Available properties** |_|:

    * squashtest.ta.ftp.host |_|: Supply the host name (mandatory).
    * squashtest.ta.ftp.username |_|: The username to log to.
    * squashtest.ta.ftp.password |_|: The corresponding password.
    * squashtest.ta.ftp.port |_|: An alternate command port.
    * squashtest.ta.ftp.filetype |_|: The default files type. Currently supported |_|: ascii or binary (either uppercase or lowercase).
    * squashtest.ta.ftp.system |_|: The host system type. Currently supported |_|: unix, vms, windows, os/2, os/400, as/400, mvs, l8, netware, macos (either uppercase or lowercase).
    * squashtest.ta.ftp.useCache |_|: Tells if the repository must cache its resource to increase performances. Default is false.

**Example**\ |_|\ **: valid configuration file**

.. code-block:: html
    
    squashtest.ta.ftp.host = myhost
    squashtest.ta.ftp.username = tester
    squashtest.ta.ftp.password = _tester
    squashtest.ta.ftp.port = 50000
    squashtest.ta.ftp.filetype = ascii
    squashtest.ta.ftp.system = os/400
    squashtest.ta.ftp.useCache = true

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################
FTP Plugin - Commands
#####################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

*********************
'put' 'file' on 'ftp'
*********************

.. admonition:: What |_|?

    This command allows to put a file on a FTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`put` :keywords:`WITH` :resources:`{<Res:file>}` :keywords:`ON` :resources:`{<Tar:ftp.target>}` :keywords:`AS` $() :keywords:`USING` $(remotepath : <distantPath> [,filetype : <FileType>] )

**> Input** |_|:

    * ``{<Res:file>}`` |_|: The name of the resource (in the context) which references the file to put on the FTP server (``file`` type resource).
    * ``{<Tar:ftp.target>}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).
    * ``{<distantPath>}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory.
    * ``{<fileType>}`` |_|: It allows to decribe the type of your file. 2 values are possible |_|: 'ascii' or 'binary'.

**Remarks** |_|:

    * If in the path ``{<distantPath>}`` some directories don't exist on the server so they are created.
    * If the property ``{<filetype>}`` is indicated in the configuration file of the FTP target and via the instruction, the value defined in the instruction prime.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/my_file_to_put.txt :keywords:`AS` :resources:`my_file_to_put.file`

        :keywords:`EXECUTE` :converter-name:`put` :keywords:`WITH` :resources:`my_file_to_put.file` :keywords:`ON` :resources:`myFTP-server` :keywords:`USING` $( remotepath : path/to/put/distant_name.txt) :keywords:`AS` $()

| 

***********************
'put' 'folder' on 'ftp'
***********************

.. admonition:: What |_|?

    This command allows to put a folder on a FTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`putFolder` :keywords:`WITH` :resources:`{<Res:directory>}` :keywords:`ON` :resources:`{<Tar:ftp.target>}` :keywords:`AS` {void} :keywords:`USING` $(remotepath : <distantPath>)

**> Input** |_|:

    * ``{<Res:directory>}`` |_|: The name of the resource (in the context) which references the folder to put on the FTP server (``directory`` type resource).
    * ``{<Tar:ftp.target>}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).
    * ``{<distantPath>}`` |_|: It corresponds to the folder path on the FTP server, relatively to the home directory.

**Remarks** |_|:

    * If in the path ``{<distantPath>}`` some directories don't exist on the server so they are created.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/my_folder_to_put :keywords:`AS` :resources:`my_folder_to_put.file`
	
        :keywords:`CONVERT` :resources:`my_folder_to_put.file` :keywords:`TO` :converter-name:`directory` :keywords:`AS` :resources:`my_folder_to_put.dir`

        :keywords:`EXECUTE` :converter-name:`putFolder` :keywords:`WITH` :resources:`my_folder_to_put.dir` :keywords:`ON` :resources:`myFTP-server` :keywords:`USING` $( remotepath : path/to/put/distant_folder_name) :keywords:`AS` {void}

|

*********************
'get' 'file' on 'ftp'
*********************

.. admonition:: What |_|?

    This command allows to get a file from a FTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get` :keywords:`WITH` $() :keywords:`ON` :resources:`{<Tar:ftp.target>}` :keywords:`AS` :resources:`{result<Res:file>}` :keywords:`USING` $(remotepath : <distantPath> [,filetype : <FileType>] )

**> Input** |_|:

    * ``{<Tar:ftp.target>}`` |_|: The name (in the context) of the FTP server to use. (``ftp.target`` type target)
    * ``{<distantPath>}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to get.
    * ``{<fileType>}`` |_|: It allows to decribe the type of your file. 2 values are possible |_|: 'ascii' or 'binary'.
    * ``{result<Res:file>}`` |_|: The name of the resource which references the file you get from the FTP server (``file`` type resource).

**Remark** |_|: If the property ``{<fileType>}`` is indicated in the configuration file of the FTP target and via the instruction, the value defined in the instruction predominates.

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get` :keywords:`WITH` $() :keywords:`ON` :resources:`myFTP-server` :keywords:`USING` $( remotepath : path/to/get/FileToGet) :keywords:`AS` :resources:`getFile.file`

| 

***********************
'get' 'folder' on 'ftp'
***********************

.. admonition:: What |_|?

    This command allows to get a folder (with all its content) from a FTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`getFolder` :keywords:`WITH` {void} :keywords:`ON` :resources:`{<Tar:ftp.target>}` :keywords:`AS` :resources:`{result<Res:directory>}` :keywords:`USING` $(remotepath : <distantPath>)

**> Input** |_|:

    * ``{<Tar:ftp.target>}`` |_|: The name (in the context) of the FTP server to use. (``ftp.target`` type target)
    * ``{<distantPath>}`` |_|: It corresponds to the folder path on the FTP server, relatively to the home directory of the folder you want to get.
    * ``{result<Res:directory>}`` |_|: The name of the resource which references the folder you get from the FTP server (``directory`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`getFolder` :keywords:`WITH` {void} :keywords:`ON` :resources:`myFTP-server` :keywords:`USING` $( remotepath : path/to/get/FolderToGet) :keywords:`AS` :resources:`getFolder.dir`

|

************************
'delete' 'file' on 'ftp'
************************

.. admonition:: What |_|?

    This command allows to delete a file located on a FTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`delete` :keywords:`WITH` $() :keywords:`ON` :resources:`{<Tar:ftp.target>}` :keywords:`AS` $() :keywords:`USING` $(remotepath : :resources:`<distantPath>` [,failIfDoesNotExist : false] )

**> Input** |_|:

    * ``{<Tar:ftp.target>}`` |_|: The name (in the context) of the FTP server to use. (``ftp.target`` type target)
    * ``{<distantPath>}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to delete.
    * ``'failIfDoesNotExist: false'`` |_|: It allows to specify to |squashTF| that the test must not fail if the file we're trying to delete doesn't exist.

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`delete` :keywords:`WITH`  $() :keywords:`ON` :resources:`myFTP-server` :keywords:`USING` $( remotepath : path/to/myfile.txt, failIfDoesNotExist: false) :keywords:`AS` $()

|

**************************
'delete' 'folder' on 'ftp'
**************************

.. admonition:: What |_|?

    This command allows to delete a directory with its content located on a FTP server.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`deleteFolder` :keywords:`WITH` {void} :keywords:`ON` :resources:`{<Tar:ftp.target>}` :keywords:`AS` {void} :keywords:`USING` $(remotepath : :resources:`<distantPath>` [,failIfDoesNotExist : false] )

**> Input** |_|:

    * ``{<Tar:ftp.target>}`` |_|: The name (in the context) of the FTP server to use. (``ftp.target`` type target)
    * ``{<distantPath>}`` |_|: It corresponds to the folder's path on the FTP server, relatively to the home directory of the folder you want to delete.
    * ``'failIfDoesNotExist: false'`` |_|: It allows to specify to |squashTF| that the test must not fail if the folder we're trying to delete doesn't exist.

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`deleteFolder` :keywords:`WITH`  {void} :keywords:`ON` :resources:`myFTP-server` :keywords:`USING` $( remotepath : path/to/myFolder, failIfDoesNotExist: false) :keywords:`AS` {void}

|

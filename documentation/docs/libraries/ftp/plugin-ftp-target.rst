..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###################
FTP Plugin - Target
###################

.. admonition:: What |_|?

    A ftp target is exactly what you think it is.

**Category-name** |_|: :guilabel:`ftp.target`

**Configuration** |_|: A simple .properties file dropped in the 'targets' directory of your test project. The file must include the shebang on the very first line |_|: '#!ftp'. It must also contain AT LEAST |_|: 'squashtest.ta.ftp.host'.

**Available properties** |_|:

    * squashtest.ta.ftp.host |_|: Supply the host name (mandatory).
    * squashtest.ta.ftp.username |_|: The username to log to.
    * squashtest.ta.ftp.password |_|: The corresponding password.
    * squashtest.ta.ftp.port |_|: An alternate command port.
    * squashtest.ta.ftp.filetype |_|: The default files type. Currently supported |_|: ascii or binary (either uppercase or lowercase).
    * squashtest.ta.ftp.system |_|: The host system type. Currently supported |_|: unix, vms, windows, os/2, os/400, as/400, mvs, l8, netware, macos (either uppercase or lowercase).

**Example of valid configuration file** |_|:

.. code-block:: html
    
    #!ftp
    squashtest.ta.ftp.host = myhost
    squashtest.ta.ftp.username = tester
    squashtest.ta.ftp.password = _tester
    squashtest.ta.ftp.port = 50000
    squashtest.ta.ftp.filetype = ascii
    squashtest.ta.ftp.system = os/400

.. admonition:: Remark

    During the download from / upload on the FTP server |_|:

        * If the property 'squashtest.ta.ftp.filetype' has the value 'binary', the file is identicaly transfered.
        * If the property has the 'ascii' value, the file is converted during the transfer (encoding, end-lines and end-file characters). The transfer mode 'ascii' allows to transfer files between heterogeneous systems. The server converts the file from its original format to a standard '8-bit NVT-ASCII' format. The client then converts the '8-bit NVT-ASCII' format file to the output format. Consequently when a binary file is transferred in 'ascii' mode, generally it's corrupted during the transfer.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###################
FTP Plugin - Macros
###################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents:
   :local:

|

.. _ftp.download.macro.anchor:

******************************************************************************
# FTP_DOWNLOAD {remotePath} FROM {FTPserverTarget} AS {downloadedResourceName}
******************************************************************************

.. admonition:: What |_|?

    This macro will download a resource from a FTP server.

**Underlying instruction** |_|:

.. code-block:: html

        EXECUTE get WITH $() ON {FTPserverTarget} USING $(remotepath : {remotePath}) AS {downloadedResourceName}

**> Input** |_|:

        * ``{remotePath}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to get.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).
        * ``{downloadResourceName}`` |_|: The name of the resource which references the file you download on the FTP server (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_DOWNLOAD path/to/example.txt FROM my-ftp-server AS example.file`

   **.properties file which contains FTP information** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-macro-ftp-properties-file.png

   **.properties must be in "targets" folder of your project** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-macro-ftp-properties-file-location.png

   **SKF script** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-download-macro.png

   **FTP confirms that the example.txt has been downloaded** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-download-macro-ftp-interface.png

   **You can see the downloaded file in SQUASH_TA temporary files folder** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-download-macro-result.png

   **Here is the downloaded file** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-download-macro-temp-file.png
    
| 

--------------------

*****************************************************************************************
# FTP_DOWNLOAD ASCII FILE {remotePath} FROM {FTPserverTarget} AS {downloadedResourceName}
*****************************************************************************************

.. admonition:: What |_|?

    This macro will download an ASCII type resource from a FTP server.

**Underlying instruction** |_|:

.. code-block:: html

    EXECUTE get WITH $() ON {FTPserverTarget} USING $(remotepath : {remotePath},filetype : ascii) AS {downloadedResourceName}

**> Input** |_|:

        * ``{remotePath}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to get.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).
        * ``{downloadResourceName}`` |_|: The name of the resource which references the file you download on the FTP server (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_DOWNLOAD ASCII FILE path/to/example.txt FROM my-ftp-server AS example.file`

    This example is based on the first one. For more details, please check :ref:`here <ftp.download.macro.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-download-ascii-macro.png

    **FTP transfer mode is set to ASCII** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-ftp/ftp-download-ascii-macro-result.png

| 

--------------------

******************************************************************************************
# FTP_DOWNLOAD BINARY FILE {remotePath} FROM {FTPserverTarget} AS {downloadedResourceName}
******************************************************************************************

.. admonition:: What |_|?

    This macro will download a binary type resource from a FTP server.


**Underlying instruction** |_|:

.. code-block:: html

    EXECUTE get WITH $() ON {FTPserverTarget} USING $(remotepath : {remotePath},filetype : binary) AS {downloadedResourceName}

**> Input** |_|:

        * ``{remotePath}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to get.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).
        * ``{downloadResourceName}`` |_|: The name of the resource which references the file you download on the FTP server (``file`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_DOWNLOAD BINARY FILE path/to/example.bin FROM my-ftp-server AS example.file`

    This example is based on the first one. For more details, please check :ref:`here <ftp.download.macro.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-download-binary-macro.png


| 

--------------------

.. _ftp.upload.macro.anchor:

**************************************************************************************
# FTP_UPLOAD {localResourcePath} ONTO {FTPserverTarget} USING REMOTE PATH {remotePath}
**************************************************************************************

.. admonition:: What |_|?

    This macro will upload a resource on a FTP server.

**Underlying instructions** |_|:

.. code-block:: html

        LOAD {localResourcePath} AS __temp_{%%rand1}.file
        EXECUTE put WITH __temp_{%%rand1}.file ON {FTPserverTarget} USING $(remotepath : {remotePath}) AS {{whocares}}

**> Input** |_|:

        * ``{localResourcePath}`` |_|: The path of the file you want to upload on the FTP server.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target)
        * ``{remotePath}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to put.

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_UPLOAD path/to/example.txt ONTO my-ftp-server USING REMOTE PATH abc/name.txt`

    **File to upload** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-upload-macro-example-file.png

    **File location** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-upload-macro-example-file-location.png

    **.properties file which contains FTP information** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-macro-ftp-properties-file.png

    **.properties must be in "targets" folder of your project** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-macro-ftp-properties-file-location.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-upload-macro.png

   **FTP confirms that the uploadExample.txt has been uploaded** |_|:

   .. figure:: ../../_static/plugin-ftp/ftp-upload-macro-ftp-interface.png

   .. figure:: ../../_static/plugin-ftp/ftp-upload-macro-ftp-interface2.png

| 

--------------------

*************************************************************************************************
# FTP_UPLOAD ASCII FILE {localResourcePath} ONTO {FTPserverTarget} USING REMOTE PATH {remotePath}
*************************************************************************************************

.. admonition:: What |_|?

    This macro will upload an ASCII type resource on a FTP server.


**Underlying instruction** |_|:

.. code-block:: html

    LOAD {localResourcePath} AS __temp_{%%rand1}.file
    EXECUTE put WITH __temp_{%%rand1}.file ON {FTPserverTarget} USING $(remotepath : {remotePath},filetype : ascii) AS {{whocares}}

**> Input** |_|:

        * ``{localResourcePath}`` |_|: The path of the file you want to upload on the FTP server.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target)
        * ``{remotePath}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to put.

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_UPLOAD ASCII FILE path/to/example.txt ONTO my-ftp-server USING REMOTE PATH abc/name.txt`

    This example is based on the previous one. For more details, please check :ref:`here <ftp.upload.macro.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-upload-ascii-macro.png

    **FTP transfer mode is set to ASCII** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-ftp/ftp-upload-ascii-macro-result.png

| 

--------------------

**************************************************************************************************
# FTP_UPLOAD BINARY FILE {localResourcePath} ONTO {FTPserverTarget} USING REMOTE PATH {remotePath}
**************************************************************************************************

.. admonition:: What |_|?

    This macro will upload a binary type resource on a FTP server.

**Underlying instruction** |_|:

.. code-block:: html

    LOAD {localResourcePath} AS __temp_{%%rand1}.file
    EXECUTE put WITH __temp_{%%rand1}.file ON {FTPserverTarget} USING $(remotepath : {remotePath},filetype : binary) AS {{whocares}}


**> Input** |_|:

        * ``{localResourcePath}`` |_|: The path of the file you want to upload on the FTP server.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target)
        * ``{remotePath}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to put.

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_UPLOAD BINARY FILE path/to/example.bin ONTO my-ftp-server USING REMOTE PATH abc/name.bin`

    This example is based on the previous one. For more details, please check :ref:`here <ftp.upload.macro.anchor>`.

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-upload-binary-macro.png

| 

--------------------

**************************************************************
# FTP_DELETE {remotePathOfFileToDelete} FROM {FTPserverTarget}
**************************************************************

.. admonition:: What |_|?

    This macro will delete a file on a FTP server.

**Underlying instruction** |_|:

.. code-block:: html

    EXECUTE delete WITH $() ON {FTPserverTarget} USING $(remotepath : {remotePathOfFileToDelete}) AS {{whocares}}

**> Input** |_|:

        * ``{remotePathOfFileToDelete}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to delete.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_DELETE distant/path/example.txt FROM my-ftp-server`

    **.properties file which contains FTP information** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-macro-ftp-properties-file.png

    **.properties must be in "targets" folder of your project** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-macro-ftp-properties-file-location.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-delete-macro.png

    **FTP confirms that the file has been deleted** |_|:

    .. figure:: ../../_static/plugin-ftp/ftp-delete-macro-ftp-interface.png

| 

--------------------

.. _ftp.delete.macro.anchor:

************************************************************************
# FTP_DELETE_IF_EXISTS {remotePathOfFileToDelete} FROM {FTPserverTarget}
************************************************************************

.. admonition:: What |_|?

    This macro will delete a resource on a FTP server. If the file doesn't exist, the macro doesn't fail.

**Underlying instruction** |_|:

.. code-block:: html

    EXECUTE delete WITH $() ON {FTPserverTarget} USING $(remotepath : {remotePathOfFileToDelete},failIfDoesNotExist:false) AS {{whocares}}

**> Input** |_|:

        * ``{remotePathOfFileToDelete}`` |_|: It corresponds to the file path on the FTP server, relatively to the home directory of the file you want to delete.
        * ``{FTPserverTarget}`` |_|: The name (in the context) of the FTP server to use (``ftp.target`` type target).

**Example** |_|:

    .. list-table::

        * - :macros:`# FTP_DELETE_IF_EXISTS distant/path/example.txt FROM my-ftp-server`

    This example is similar to the previous one. For more details, please check :ref:`here <ftp.delete.macro.anchor>`.

|

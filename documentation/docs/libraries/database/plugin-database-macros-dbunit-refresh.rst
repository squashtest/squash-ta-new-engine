..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################################
Database Plugin - Macros - Refresh DbUnit
#########################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
    :local:

|

******************************************
# REFRESH_DBUNIT {dataset} INTO {database}
******************************************

.. admonition:: What |_|?

    This macro will refresh all the data listed in the 'dataset file' into the 'database'. Refresh operation means that data of existing rows are updated and non-existing row get inserted. Any rows which exist in the database but not in dataset stay unaffected.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp{%%rand1}.file

    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp_{%%rand2}.file
    CONVERT __temp_{%%rand2}.file TO xml(structured) AS __temp_{%%rand3}.xml
    CONVERT __temp_{%%rand3}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand4}.dbu

    EXECUTE insert WITH __temp_{%%rand4}.dbu ON {database} USING $(operation:refresh) AS __temp_{%%rand5}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).

**Remark** |_|: The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).

**Example** |_|:

    .. list-table::

        * - :macros:`# REFRESH_DBUNIT path/to/dataset.xml INTO my_database`

    **Database overview** |_|:

    .. figure:: ../../_static/plugin-database/refresh-dbunit/refresh-dbunit-database-overview.png

    **In the dataset, we update employees' information and add a new one** |_|:

    .. figure:: ../../_static/plugin-database/refresh-dbunit/refresh-dbunit-dataset.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/refresh-dbunit/refresh-dbunit-macro.png

    **The employees are updated and the new one is inserted in the database** |_|:

    .. figure:: ../../_static/plugin-database/refresh-dbunit/refresh-dbunit-database-result.png

| 

--------------------

***************************************************************
# REFRESH_DBUNIT {dataset} INTO {database} WITH CONFIG {config}
***************************************************************

.. admonition:: What |_|?

    This macro will refresh all the data listed in the 'dataset file' into the 'database' using a DbUnit configuration file. Refresh operation means that data of existing rows are updated and non-existing row get inserted. Any rows which exist in the database but not in dataset stay unaffected.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    LOAD {dataset} AS __temp{%%rand3}.file

    CONVERT __temp{%%rand3}.file TO file(param.relativedate) AS __temp_{%%rand4}.file
    CONVERT __temp_{%%rand4}.file TO xml(structured) AS __temp_{%%rand5}.xml
    CONVERT __temp_{%%rand5}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand6}.dbu

    EXECUTE insert WITH __temp_{%%rand6}.dbu ON {database} USING $(operation:refresh),__temp{config}{%%rand2}.conf AS __temp_{%%rand7}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').

**Remarks** |_|:

    #. The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).
    #. The file designed by ``{config}`` must respect the same rules than a file which would serve to create an SKF ``conf.dbunit`` type resource via the converter (:ref:`From file to conf.dbunit<from.file.to.conf.dbunit.anchor>`).

**Example** |_|:

.. list-table::

    * - :macros:`# REFRESH_DBUNIT path/to/dataset.xml INTO my_database WITH CONFIG  path/to/my_config_dbunit.properties`

| 

--------------------

************************************************************
# REFRESH_DBUNIT {dataset} INTO {database} USING {ppkfilter}
************************************************************

.. admonition:: What |_|?

    This macro will refresh all datas listed in the 'dataset file' into the 'database' using a DbUnit filter. Refresh operation means that data of existing rows are updated and non-existing row get inserted. Any rows which exist in the database but not in dataset stay unaffected.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp{%%rand1}.file
    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO xml(structured) AS __temp{%%rand3}.xml
    CONVERT __temp{%%rand3}.xml TO dataset.dbunit(dataset) AS __temp{%%rand4}.dbu

    LOAD {ppkfilter} AS __temp{%%rand5}.file
    CONVERT __temp{%%rand5}.file TO properties(structured) AS __temp{%%rand6}.props
    CONVERT __temp{%%rand6}.props TO conf.dbunit.ppk(from.properties) AS __temp{%%rand7}.ppk

    EXECUTE insert WITH __temp{%%rand4}.dbu ON {database} USING __temp{%%rand7}.ppk,$(operation:refresh) AS __temp_{%%rand8}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{ppkfilter}`` |_|: A DbUnit filter referring to pseudo primary keys ('.properties').

**Example** |_|:

.. list-table::

    * - :macros:`# REFRESH_DBUNIT path/to/dataset.xml INTO my_database USING path/to/my_filter_dbunit.properties`

| 

--------------------

*********************************************************************************
# REFRESH_DBUNIT {dataset} INTO {database} WITH CONFIG {config} USING {ppkfilter}
*********************************************************************************

.. admonition:: What |_|?

    This macro will refresh all the data listed in the 'dataset file' into the 'database' using a DbUnit configuration file and a DbUnit filter. Refresh operation means that data of existing rows are updated and non-existing row get inserted. Any rows which exist in the database but not in dataset stay unaffected.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    LOAD {dataset} AS __temp{%%rand3}.file
    CONVERT __temp{%%rand3}.file TO file(param.relativedate) AS __temp_{%%rand4}.file
    CONVERT __temp_{%%rand4}.file TO xml(structured) AS __temp_{%%rand5}.xml
    CONVERT __temp_{%%rand5}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand6}.dbu

    LOAD {ppkfilter} AS __temp{%%rand7}.file
    CONVERT __temp{%%rand7}.file TO properties(structured) AS __temp{%%rand8}.props
    CONVERT __temp{%%rand8}.props TO conf.dbunit.ppk(from.properties) AS __temp{%%rand9}.ppk

    EXECUTE insert WITH __temp_{%%rand6}.dbu ON {database} USING __temp{%%rand9}.ppk,$(operation:refresh),__temp{config}{%%rand2}.conf AS __temp_{%%rand10}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').
        * ``{ppkfilter}`` |_|: A DbUnit filter referring to pseudo primary keys ('.properties').

**Example** |_|:

.. list-table::

    * - :macros:`# REFRESH_DBUNIT path/to/dataset.xml INTO my_database WITH CONFIG path/to/my_config_dbunit.properties USING path/to/my_filter_dbunit.properties`

|

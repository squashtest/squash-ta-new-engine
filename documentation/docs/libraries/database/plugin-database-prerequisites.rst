..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############################
Database Plugin - Prerequisites
###############################

.. contents:: Contents:
    :local:

To connect to a database and so to use database targets, an automation |squashTF| project need an adequat JDBC driver (the driver depends on the database type |_|: mysql, oracle...).

The driver is provided in the form of a maven artifact that we add in the automation project. To do so, we modify the 'squash-ta-maven-plugin' inside the *pom.xml* file |_|:

.. code-block:: html

    …
    <build>
        <plugins>
            <plugin>
                <groupId>org.squashtest.ta</groupId>
                <artifactId>squash-ta-maven-plugin</artifactId>
                <version>squash-ta-maven-plugin version</version>

                <dependencies>
                    <dependency>
                        <groupId>JDBC driver groupId</groupId>
                        <artifactId>JDBC driver artifact ID</artifactId>
                        <version>JDBC driver version</version>
                    </dependency>
                <dependencies>
     …


**Example of JDBC Driver for MySql** |_|:

    .. code-block:: html

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.17</version>
        </dependency>

**Project's POM File** |_|:

.. figure:: ../../_static/plugin-database/database-pom.png

**.properties File to connect to database** |_|:

.. figure:: ../../_static/plugin-database/database-properties.png

For more information, please check this :ref:`section <database.target.anchor>`.

|
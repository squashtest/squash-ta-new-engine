..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _database.plugin.converters.anchor:

############################
Database Plugin - Converters
############################

.. contents:: Contents |_|:
   :local:

|

.. _from.file.to.conf.dbunit.anchor:

****************
From file |_|...
****************

|

... |_| to conf.dbunit
**********************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

    This :guilabel:`structured` converter will convert a ``file`` type resource to a ``conf.dbunit`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`conf.dbunit` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:conf.dbunit>}`

**> Input** |_|:

    * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a configuration file for DbUnit. This file must be a ``.properties`` file (list of properties key / value using '=' like separator).

**> Output** |_|:

    * ``converted<Res:conf.dbunit>`` |_|: The name of the converted resource (``conf.dbunit`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/dbunit-configuration.properties :keywords:`AS` :resources:`dbunit-conf.file`

        :keywords:`CONVERT` :resources:`dbunit-conf.file` :keywords:`TO` :resource-type:`conf.dbunit` (:converter-name:`structured`) :keywords:`AS` :resources:`conf`

The DbUnit "features & properties" supported are |_|: (`See DbUnit documentation <http://www.dbunit.org/properties.html>`_)

**Batched statements** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.batchedStatements
    * - DbUnit name
      - http://www.dbunit.org/features/batchedStatements
    * - Default value
      - false
    * - Meaning
      - Enable or disable the use of batch JDBC requests.

**Case sensitive table names** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.caseSensitiveTableNames
    * - DbUnit name
      - http://www.dbunit.org/features/caseSensitiveTableNames
    * - Default value
      - false
    * - Meaning
      - Enable or disable the case sensitivity of table names. When this property is activ, table names are considered case sensitive.

**Qualified table names** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.qualifiedTableNames
    * - DbUnit name
      - http://www.dbunit.org/features/qualifiedTableNames
    * - Default value
      - false
    * - Meaning
      - Enable or disable the possibility of taking in charge simultaneously several schemas. When this property is enabled tables names are preceded by the schema name to which they belong : SCHEME.TABLE.

**Table types** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.tableType
    * - DbUnit name
      - http://www.dbunit.org/properties/tableType
    * - Default value
      - String[]{"TABLE"}
    * - Meaning
      - Permits to configure the type of known tables.

**Datatype factory (Cf. issue 789)** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.datatypeFactory
    * - DbUnit name
      - http://www.dbunit.org/properties/datatypeFactory
    * - Default value
      - org.dbunit.dataset.datatype.DefaultDataTypeFactory
    * - Meaning
      - Some datas types are specific to the management of relational database.
    
        To allow DbUnit to manage this kind of datas, it's necessary to specify the "Datas types Factory" he must use.
        
        The following factories are available in DbUnit : 

        * org.dbunit.ext.db2.Db2DataTypeFactory
        * org.dbunit.ext.h2.H2DataTypeFactory
        * org.dbunit.ext.hsqldb.HsqldbDataTypeFactory
        * org.dbunit.ext.mckoi.MckoiDataTypeFactory
        * org.dbunit.ext.mssql.MsSqlDataTypeFactory
        * org.dbunit.ext.mysql.MySqlDataTypeFactory
        * org.dbunit.ext.oracle.OracleDataTypeFactory
        * org.dbunit.ext.oracle.Oracle10DataTypeFactory
        * org.dbunit.ext.postgresql.PostgresqlDataTypeFactory
        * org.dbunit.ext.netezza.NetezzaDataTypeFactory

**Batch size** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.batchSize
    * - DbUnit name
      - http://www.dbunit.org/properties/batchSize
    * - Default value
      - 100
    * - Meaning
      - Integer representing the requests number in a batch requests 

        (Only when the property ``batchedStatements`` is active)

**Metadata handler** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.metadataHandler
    * - DbUnit name
      - http://www.dbunit.org/properties/metadataHandler
    * - Default value
      - org.dbunit.database.DefaultMetadataHandler
    * - Meaning
      - The way of metadatas management of the base can differ according to the SGBDR.

        The following handlers are available :

        * org.dbunit.ext.db2.Db2MetadataHandler
        * org.dbunit.ext.mysql.MySqlMetadataHandler
        * org.dbunit.ext.netezza.NetezzaMetadataHandler

        For others SGBDR, default handler is enough.

**Escape pattern** |_|:

.. list-table::

    * - SKF name
      - squashtest.ta.dbunit.escapePattern
    * - DbUnit name
      - http://www.dbunit.org/properties/escapePattern
    * - Default value
      - none
    * - Meaning
      - Allows schema, table and column names escaping.
    * - Example
      - **squashtest.tf.dbunit.escapePattern=`**

        The property above will permit to escape the table name and column names in the following query.

        **insert into `person` (`id`, `name`, `unique`) values (1, 'Doe', true);**

        This query will succeed even though "unique" is a SQL key word and is not normally allowed.

.. admonition:: Remark

    The DbUnit property: http://www.dbunit.org/properties/primaryKeyFilter exist via the category of SKF resource: ``conf.dbunit.ppk``.

|

------

... |_| to conf.dbunit.ppk
**************************

**Category-Name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

    This :guilabel:`structured` converter will convert a ``file`` type resource to a``conf.dbunit.ppk`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`conf.dbunit.ppk` (:converter-name:`structured`) :keywords:`AS` :resources:`{converted<Res:conf.dbunit.ppk>}`

**> Input** |_|:

    * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a configuration file to define the pseudo primary keys. This configuration file must be of type ``.properties`` (for each property, the key is the name of a Table, the value is the name of a column or a list of columns separated with comma and the '=' character is used like separator).

.. note |_|::

    For a table whose name is used as key, its pseudo primary key is the column or the list of columns defined in the value part of the property.

**> Output** |_|:

    * ``converted<Res:conf.dbunit.ppk>`` |_|: The name of the converted resource (Resource of type ``conf.dbunit.ppk``).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/valid_ppk.properties :keywords:`AS` :resources:`ppk.file`

        :keywords:`CONVERT` :resources:`ppk.file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`ppk.properties`

        :keywords:`CONVERT` :resources:`ppk.properties` :keywords:`TO` :resource-type:`conf.dbunit.ppk` (:converter-name:`from.properties`) :keywords:`AS` :resources:`ppk`

|

------

... |_| to parameter.indexed.sql
********************************

**Category-Name** |_|: :guilabel:`from.text`

.. admonition:: What |_|?

    This :guilabel:`from.text` converter will convert a ``file`` type resource to a ``parameter.indexed.sql`` type resource. 

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`parameter.indexed.sql` (:converter-name:`from.text`) :keywords:`AS` :resources:`{converted<Res:parameter.indexed.sql>}`

**> Input** |_|:

    * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a file which each line defines the value of a sql query parameter. Each line contains two character strings separated with the character '=' |_|:
        
        * The first character string corresponds to the parameter position in the SQL query.

        * The Second one corresponds to the value.

.. admonition:: Remark

    None of the two character strings can be empty.

**> Output** |_|:

    * ``converted<Res:parameter.indexed.sql>`` |_|: The name of the converted resource (``parameter.indexed.sql`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/parameter-indexed_value.properties :keywords:`AS` :resources:`value.file`

        :keywords:`CONVERT` :resources:`value.file` :keywords:`TO` :resource-type:`parameter.indexed.sql` (:converter-name:`from.text`) :keywords:`AS` :resources:`value.properties`

|

------

... |_| to parameter.named.sql
******************************
 
**Category-Name** |_|: :guilabel:`from.text`

.. admonition:: What |_|?

    This :guilabel:`from.text` converter will convert a ``file`` type resource to a ``parameter.named.sql`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`parameter.named.sql` (:converter-name:`from.text`) :keywords:`AS` :resources:`{converted<Res:parameter.named.sql>}`

**> Input** |_|:

    * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a file which each line defines the value of a sql query parameter. Each line contains two character strings separated with the character '=' |_|:

        * The first character string corresponds to the parameter name in the SQL query.

        * The Second one corresponds to the value.

.. admonition:: Remark

    None of the two character strings can be empty but the name can be constituted with only space characters.

**> Output** |_|:

    * ``converted<Res:parameter.named.sql>`` |_|: The name of the converted resource (``parameter.named.sql``  type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/parameter-named_value.properties :keywords:`AS` :resources:`value.file`

        :keywords:`CONVERT` :resources:`value.file` :keywords:`TO` :resource-type:`parameter.named.sql` (:converter-name:`from.text`) :keywords:`AS` :resources:`value.properties`

.. _from.file.to.query.sql.anchor:

|

------

... |_| to query.sql
********************

**Category-Name** |_|: :guilabel:`query`

.. admonition:: What |_|?

    This :guilabel:`query` converter will convert a ``file`` type resource to a ``query.sql`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`query.sql` (:converter-name:`query`) :keywords:`AS` :resources:`{converted<Res:query.sql>}`

**> Input** |_|:

    * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a file which respects the following rules |_|:
        
        * The file must contain only one query.

        * The query can be written on one or several lines.

        * The query end with the character ';'.

        * Comments at SQL format can be inserted in the file.

**> Output** |_|:

    * ``converted<Res:query.sql>`` |_|: The name of the converted resource (``query.sql`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` sql/my_query.sql :keywords:`AS` :resources:`my.file`

        :keywords:`CONVERT` :resources:`my.file` :keywords:`TO` :resource-type:`query.sql` (:converter-name:`query`) :keywords:`AS` :resources:`my.query`

.. _from.file.to.script.sql.anchor:

|

------

... |_| to script.sql
*********************

**Category-Name** |_|: :guilabel:`script`

.. admonition:: What |_|?

    This :guilabel:`script` converter will convert a ``file`` type resource to a ``script.sql`` type resource. It is possible to add an option for the encoding as well as the SQL block delimiter.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`script.sql` (:converter-name:`script`) :keywords:`AS` :resources:`{converted<Res:script.sql>}` [:keywords:`USING` :resources:`{encoding}, {delimiter}`]

**> Input** |_|:

    * ``resourceToConvert<Res:file>`` |_|: The name (in the context) of the resource which references a file whose content is an SQL script.

    * ``Optional - encoding`` |_|: Parameter representing the query file encoding. Default value |_|: "UTF-8".

    * ``Optional - delimiter`` |_|: Parameter representing the SQL block delimiter. Default value |_|: "@@". It can be used in conjunction with encoding or by itself - in which case encoding will take its value by default.

**> Output** |_|:

    * ``converted<Res:script.sql>`` |_|: The name of the converted resource (``script.sql`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/my_script.sql :keywords:`AS` :resources:`my_script.file`

        :keywords:`CONVERT` :resources:`my_script.file` :keywords:`TO` :resource-type:`script.sql` (:converter-name:`script`) :keywords:`AS` :resources:`script` [:keywords:`USING` :resources:`str_encoding, str_delimiter`]

.. admonition:: Remarks

    * In your SQL script the delimiter should enclose any block of code that should be stored and passed in it's entirety to the database server.

    **Example** |_|:

        * Creating a MySQL procedure |_|::

            DROP PROCEDURE IF EXISTS `some_procedure`;
            @@
            CREATE PROCEDURE `some_procedure`(...)
            BEGIN
             ...
            END;
            @@

        * Creating a PL/pgSQL function |_|::

            @@
            CREATE OR REPLACE FUNCTION somefunc() RETURNS ... AS $$
            DECLARE
             ...
            BEGIN
             ...
                DECLARE
                ...
                BEGIN
                ...
                END;

                RETURN ...;
    
            END;
            $$ LANGUAGE plpgsql;@@

        * Calling a stored PL/SQL procedure with delimiter set to "<DELIMITER>" |_|::

            <DELIMITER>
            BEGIN
                some_procedure;
            END;
            <DELIMITER>

    * In case of nested SQL blocks you only need to englobe the top level block with the delimiter.

    * **Comments** |_|: refrain from using comments at the end of a line of code because it might induce a malfunction if it contains certain characters.

|

----

********************************
From directory to dataset.dbunit
********************************

**Category-Name** |_|: :guilabel:`dataset`

.. admonition:: What |_|?

    This :guilabel:`dataset` converter will convert a ``directory`` type resource to a ``dataset.dbunit`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:directory>}` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`{converted<Res:dataset.dbunit>}`

**> Input** |_|:

    ``resourceToConvert<Res:directory>`` |_|: The name (in the context) of the resource which references a directory (``directory`` type resource). This directory must contain at the root a file named ``table-ordering.txt`` which contains an ordered list of tables to add to the dataset. Each line of the file is a relative path to the root directory towards the csv file containing the table.

.. note |_|::

    The relative path of the file is expected WITHOUT the ``.csv`` extension .

**> Output** |_|:

    * ``converted<Res:dataset.dbunit>`` |_|: The name of the converted resource (``dataset.dbunit`` type resource).

**Example** |_|:

.. list-table::

    - * :keywords:`LOAD` csv/csv1 :keywords:`AS` :resources:`csv1.file`
    
        :keywords:`CONVERT` :resources:`csv1.file` :keywords:`TO` :resource-type:`directory` (:converter-name:`filesystem`) :keywords:`AS` :resources:`csv1.dir`
   
        :keywords:`CONVERT` :resources:`csv1.dir` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`csv1.dataset`

|

----

**********************************
From properties to conf.dbunit.ppk
**********************************

**Category-Name** |_|: :guilabel:`from.properties`

.. admonition:: What |_|?

    This :guilabel:`from.properties` converter will convert a ``properties`` type resource to a ``conf.dbunit.ppk`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:properties>}` :keywords:`TO` :resource-type:`conf.dbunit.ppk` (:converter-name:`from.properties`) :keywords:`AS` :resources:`{converted<Res:conf.dbunit.ppk>}`

**> Input** |_|:

    * ``resourceToConvert<Res:properties>`` |_|: The name (in the context) of the resource which references a ``.properties`` file (``properties`` type resource). For each property, the key is a Table name, the value is a column or columns list separated with comma.

.. note |_|::

    For a table whose name is used as key, its pseudo primary key is the column or the list of columns defined in the value part of the property.

**> Output** |_|:

    * ``converted<Res:conf.dbunit.ppk>`` |_|: The name of the converted resource (``conf.dbunit.ppk`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/valid-ppk.properties :keywords:`AS` :resources:`ppk.file`
    
        :keywords:`CONVERT` :resources:`ppk.file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`ppk.properties`
    
        :keywords:`CONVERT` :resources:`ppk.properties` :keywords:`TO` :resource-type:`conf.dbunit.ppk` (:converter-name:`from.properties`) :keywords:`AS` :resources:`ppk`

|

----

*********************************
From result.sql to dataset.dbunit
*********************************

**Category-Name** |_|: :guilabel:`dataset`

.. admonition:: What |_|?

    This :guilabel:`dataset` converter will convert a ``result.sql`` type resource to a ``dataset.dbunit`` type resource.

.. list-table::
    
    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:result.sql>}` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`{converted<Res:dataset.dbunit>}` :keywords:`USING` :resources:`{config<Res:file>}`

**> Input** |_|:

    * ``resourceToConvert<Res:result.sql>`` |_|: The name (in the context) of the resource which references a ``result.sql`` resource. It corresponds to the result of a SQL query ``SELECT``.

    * ``config<Res:file>`` |_|: The name of the complementary resource which references a configuration file which contains only one key / value |_|: ``tablename`` separated of the value with the character ':'. It's mandatory and can be define with an inline instruction. A ``result.sql`` resource does not have Table name and to transform a ``result.sql`` in ``dataset.dbunit`` we need a Table name.

**> Output** |_|:

    * ``converted<Res:dataset.dbunit>`` |_|: The name of the converted resource (Resource of type ``dataset.dbunit``).

**Example** |_|:

.. list-table::

    * - :keywords:`CONVERT` :resources:`insertion_query.resultset` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`USING` $(tablename : <name_Table>) :keywords:`AS` :resources:`dataset`

|

----

***************
From xml |_|...
***************

|

.. _from.xml.to.dataset.dbunit.anchor:

... |_| to dataset.dbunit
*************************

**Category-Name** |_|: :guilabel:`dataset`

.. admonition:: What |_|?

    This :guilabel:`dataset` converter will convert an ``xml`` type resource to a ``dataset.dbunit`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:xml>}` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`{converted<Res:dataset.dbunit>}`

**> Input** |_|:

    * ``resourceToConvert<Res:xml>`` |_|: The name (in the context) of the resource which references an ``xml`` file. The content of the file must be at the format of `FlatXMLDataset of DbUnit <http://www.dbunit.org/apidocs/org/dbunit/dataset/xml/FlatXmlDataSet.html>`_. Each line of a table is represented by an XML element |_|:

        * The tag name corresponds of the name table.

        * Each column of the table is represented by an attribut |_|:

            * The attribute name corresponds to the column name.

            * The attribute value corresponds to the column value in the represented line.

**> Output** |_|:

    * ``converted<Res:dataset.dbunit>`` |_|: The name of the converted resource (``dataset.dbunit`` type resource).

**Example of XML file** |_|::

    <?xml version="1.0" encoding="UTF-8"?>
    <dataset>
        <table1 colonne0="row 0 col 0" colonne1="row 0 col 1" />
        <table1 colonne0="row 1 col 0" colonne1="row 1 col 0" />
        <table1 colonne0="row 2 col 0" colonne1="row 2 col 0" />
        <table2 colonne0="row 0 col 0" />
        <table3 />
    </dataset>

.. admonition:: Remarks

    * If in the initial resource the attribute value is the sring character ``[NULL]``, the corresponding column will have the value ``null`` in the converted ressource (``dataset.dbunit`` type resource).

    * During the conversion, table columns are determined from the attributs of the first element corresponding to this table in the initial ressource (``xml`` type resource). For instance, if an xml resource contains 'T' elements |_|:

        * **Case 1** |_|: Attribute of the first 'T' element not present but present after |_|:

        If |_|:

            * The first element 'T' doesn't contain 'C' attribute and

            * A 'T' element following contains a 'C' attribute

        Then |_|:

            * The 'C' attribute will be ignored during the conversion. That means no 'C' column in the converted resource.

        * **Case 2** |_|:

        If |_|:

            * The first 'T' element contains a 'C' attribute and

            * A 'T' element following doesn't contain a 'C' attribute

        Then |_|:

            * There will be a 'C' column in the converted resource. In lines corresponding to the elements which doesn't contain the 'C' attribute, the 'C' column will have the value 'null'.

        **Example** |_|:

        .. list-table::

            * - :keywords:`LOAD` path/to/dataset.xml :keywords:`AS` :resources:`dataset.file`
            
                :keywords:`CONVERT` :resources:`dataset.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`my_xml_file`
           
                :keywords:`CONVERT` :resources:`my_xml_file` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset.dbu` 

.. _from.xml.to.filter.dbunit.anchor:

|

------

... |_| to filter.dbunit
************************

**Category-Name** |_|: :guilabel:`filter`

.. admonition:: What |_|?

    This :guilabel:`filter` converter will convert an ``xml`` type resource to a ``filter.dbunit`` type resource.

.. list-table::

   * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:xml>}` :keywords:`TO` :resource-type:`filter.dbunit` (:converter-name:`filter`) :keywords:`AS` :resources:`{converted<Res:filter.dbunit>}`

**> Input** |_|:

    * ``resourceToConvert<Res:xml>`` |_|: The name (in the context) of the resource which references a ``xml`` file. This ``xml`` file looks like |_|:

        * For a Table exclusion |_|::

            <?xml version="1.0" encoding="UTF-8"?>
            <filter>
             <tableExclude tableRegex="table_name"/>  
            </filter>

        * For a column exclusion |_|::

            <?xml version="1.0" encoding="UTF-8"?>
            <filter>
            <tableInclude tableRegex="table_name">
                  <columnExclude>column_name</columnExclude>
             </tableInclude>
            </filter>

.. note |_|::

     It's not allowed to have tableInclude and tableExclude in the same filter.

**> Output** |_|:

    * ``converted<Res:dataset.dbunit>`` |_|: The name of the converted resource (``dataset.dbunit`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/column_exclude.xml :keywords:`AS` :resources:`filter_dbunit.file`

        :keywords:`CONVERT` :resources:`filter_dbunit.file` :keywords:`TO` :resource-type:`filter.dbunit` (:converter-name:`filter`) :keywords:`AS` :resources:`filter_dbunit`

|

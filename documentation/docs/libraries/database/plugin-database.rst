..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############
Database Plugin
###############

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Prerequisites <plugin-database-prerequisites.rst>
    Target <plugin-database-target.rst>
    Resources <plugin-database-resources.rst>
    Macros <plugin-database-macros.rst>
    Advanced Users <plugin-database-advanced-users.rst>

This plugin provides all the elements needed to interact with a database.
..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##########################
Database Plugin - Commands
##########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

***********************************
'execute' 'query.sql' on 'database'
***********************************

.. admonition:: What |_|?

    This command executes a SQL query represented by a ``query.sql`` resource on the specified database target.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{query<Res:query.sql>}` :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` :resources:`{result<Res:result.sql>}`

**> Input** |_|:

    * ``query<Res:query.sql>`` |_|: The name (in the context) of the resource which references a SQL query (``query.sql`` type resource).

    * ``<Tar:database>`` |_|: The name (in the context) of the database to use (``database`` type target).

**> Output** |_|:

    * ``result<Res:result.sql>`` |_|: The name of the resource which contains the result of the SQL query (``result.sql`` type resource).

**Example** |_|:

.. list-table::

    - * :keywords:`LOAD` path/to/my_query.sql :keywords:`AS` :resources:`query.file`

        :keywords:`CONVERT` :resources:`query.file` :keywords:`TO` :resource-type:`query.sql` (:converter-name:`query`) :keywords:`AS` :resources:`query1`

        |

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`query1` :keywords:`ON` :resources:`mydatabase-db` :keywords:`AS` :resources:`my_query_result`

|

----

***************************************************************
'execute' 'query.sql' on 'database' via 'parameter.indexed.sql'
***************************************************************

.. admonition:: What |_|?

    This command executes a SQL query represented by a ``query.sql`` resource on the specified database target via indexed parameters.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{query<Res:query.sql>}` :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` :resources:`{result<Res:result.sql>}` :keywords:`USING` :resources:`{config<Res:parameter.indexed.sql>}`

**> Input** |_|:

    * ``query<Res:query.sql>`` |_|: The name (in the context) of the resource which references a SQL query (``query.sql`` type resource).

    * ``<Tar:database>`` |_|: The name (in the context) of the database to use (``database`` type target).

    * ``config<Res:parameter.indexed.sql>`` |_|: The name of the resource which contains indexed parameters for the SQL query (``parameter.indexed.sql`` type resource).

**> Output** |_|:

    * ``result<Res:result.sql>`` |_|: The name of the resource which contains the result of the SQL query (``result.sql`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/my_query.sql :keywords:`AS` :resources:`query.file`
    
        :keywords:`CONVERT` :resources:`query.file` :keywords:`TO` :resource-type:`query.sql` (:converter-name:`query`) :keywords:`AS` :resources:`query1`

        |

        :keywords:`LOAD` path/to/parameter-indexed_value.properties :keywords:`AS` :resources:`value.file`
  
        :keywords:`CONVERT` :resources:`value.file` :keywords:`TO` :resource-type:`parameter.indexed.sql` (:converter-name:`from.text`) :keywords:`AS` :resources:`value.properties`

        |

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`query1` :keywords:`ON` :resources:`mydatabase-db` :keywords:`AS` :resources:`my_query_result` :keywords:`USING` :resources:`value.properties`

|

----

*************************************************************
'execute' 'query.sql' on 'database' via 'parameter.named.sql'
*************************************************************

.. admonition:: What |_|?

    This command executes a SQL query represented by a ``query.sql`` resource on the specified database target via named parameters.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{query<Res:query.sql>}` :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` :resources:`{result<Res:result.sql>}` :keywords:`USING` :resources:`{config<Res:parameter.named.sql>}`

**> Input** |_|:

    * ``query<Res:query.sql>`` |_|: The name (in the context) of the resource which references a SQL query (``query.sql`` type resource).

    * ``<Tar:database>`` |_|: The name (in the context) of the database to use (``database`` type target).

    * ``config<Res:parameter.named.sql>`` |_|: The name of the resource which contains named parameters for the SQL query (``parameter.named.sql`` type resource).

**> Output** |_|:

    * ``result<Res:result.sql>`` |_|: The name of the resource which contains the result of the SQL query (``result.sql`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/my_query.sql :keywords:`AS` :resources:`query.file`
  
        :keywords:`CONVERT` :resources:`query.file` :keywords:`TO` :resource-type:`query.sql` (:converter-name:`query`) :keywords:`AS` :resources:`query1`


  
        :keywords:`LOAD` path/to/parameter-named_value.properties :keywords:`AS` :resources:`value.file`
  
        :keywords:`CONVERT` :resources:`value.file` :keywords:`TO` :resource-type:`parameter.named.sql` (:converter-name:`from.text`) :keywords:`AS` :resources:`value.properties`


        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`query1` :keywords:`ON` :resources:`mydatabase-db` :keywords:`AS` :resources:`my_query_result` :keywords:`USING` :resources:`value.properties`

|

----

************************************
'execute' 'script.sql' on 'database'
************************************

.. admonition:: What |_|?

    This command executes a SQL script represented by a ``script.sql`` resource on the specified database target.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{script<Res:script.sql>}` :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` $() [ :keywords:`USING` $(keep.separator: <keepSeparator>)]

**> Input** |_|:

    * ``script<Res:script.sql>`` |_|: The name (in the context) of the resource which references a SQL script (``script.sql`` type resource).

    * ``<Tar:database>`` |_|: The name (in the context) of the database on which the SQL script should be used (``database`` type target).

    * ``<keepSeparator>`` |_|: Indicate to the command to keep or remove the separator (";") at the end of each SQL query of the script. This parameter can take one of two values |_|: "true" or "false". By default this parameter is set to "true".

**Example 1** |_|:

.. list-table::
    
    * - :keywords:`LOAD` path/to/my_script.sql :keywords:`AS` :resources:`script.file`
    
        :keywords:`CONVERT` :resources:`script.file` :keywords:`TO` :resource-type:`script.sql` (:converter-name:`script`) :keywords:`AS` :resources:`script1`

        |

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`script1` :keywords:`ON` :resources:`mydatabase-db` :keywords:`AS` $()

**Example 2** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`script1` :keywords:`ON` :resources:`mydatabase-db` :keywords:`AS` $() :keywords:`USING` $(keep.separator:false)

|

----

***********************
'get.all' on 'database'
***********************

.. admonition:: What |_|?

    This command allows to create a DbUnit dataset from a specific database.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get.all` :keywords:`WITH` $() :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` :resources:`{result<Res:dataset.dbunit>}` [:keywords:`USING` [:resources:`{<Res:conf.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`] ]

**> Input** |_|:

    * ``<Tar:database>`` |_|: The name (in the context) of the database to use (``database`` type target).

    * ``<Res:conf.dbunit>`` |_|: This resource contains DbUnit configuration properties.

    * ``<Res:conf.dbunit.ppk>`` |_|: The name of the resource which references a configuration file to define the pseudo primary keys.

.. admonition:: Remarks

    1. If for a table a primary key and a pseudo primary key are defined, the pseudo primary key override the primary key.

    2. If for a table a pseudo primary key is defined with one or more non existent columns, the command fails.

**> Output** |_|:

    * ``result<Res:dataset.dbunit>`` |_|: The name of the resource which contains the DbUnit dataset of all the database.

**Example 1** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get.all` :keywords:`WITH` $() :keywords:`ON` :resources:`myDatabase-db` :keywords:`AS` :resources:`myDataset`

**Example 2** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/dbunit-conf.properties :keywords:`AS` :resources:`conf.file`
    
        :keywords:`CONVERT` :resources:`conf.file` :keywords:`TO` :resource-type:`conf.dbunit` (:converter-name:`structured`) :keywords:`AS` :resources:`conf.dbu`

        |

        :keywords:`EXECUTE` :converter-name:`get.all` :keywords:`WITH` $() :keywords:`ON` :resources:`my_Database-db` :keywords:`USING` :resources:`conf.dbu` :keywords:`AS` :resources:`my_dataset`

|

----

***************************************
'insert' 'dataset.dbunit' on 'database'
***************************************

.. admonition:: What |_|?

    This command insert a DbUnit dataset on the specified database target.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`insert` :keywords:`WITH` :resources:`{dataset<Res:dataset.dbunit>}` :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` $() [:keywords:`USING` [$(operation : <type>)],[:resources:`{<Res:conf.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`]]

**> Input** |_|:

    * ``dataset<Res:dataset.dbunit>`` |_|: The name of the resource which references a DbUnit dataset (Resource of type ``dataset.dbunit``).

    * ``<Tar:database>`` |_|: The name (in the context) of the database to use (``database`` type target).

    * ``<type>`` |_|: 4 values are possible for this parameter |_|:

        * ``INSERT`` |_|: for a simple operation of insert. This operation assumes that table data does not exist in the target database and fails if this is not the case.
    
        * ``CLEAN_INSERT`` |_|: a 'delete all' is realised before the 'insert' operation.
    
        * ``UPDATE`` |_|: this operation assumes that table data already exists in the target database and fails if this is not the case.
  
        * ``REFRESH`` |_|: data of existing rows are updated and non-existing row get inserted. Any rows which exist in the database but not in dataset stay unaffected.

    .. admonition:: Remark

        If "$(operation |_|: |_| <type>)" is not defined, property is by default ``CLEAN_INSERT``.

    * ``<Res:conf.dbunit>`` |_|: This resource contains DbUnit configuration properties.
    
    * ``<Res:conf.dbunit.ppk>`` |_|: The name of the resource which references a configuration file to define the pseudo primary keys.

    .. admonition:: Remark

            1. If for a table a primary key and a pseudo primary key are defined, the pseudo primary key override the primary key.
    
            2. If for a table a pseudo primary key is defined with one or more non existents columns, the command fails.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/dataset_to_insert.xml :keywords:`AS` :resources:`dataset_file`
 
        :keywords:`CONVERT` :resources:`dataset_file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`dataset_xml`
 
        :keywords:`CONVERT` :resources:`dataset_xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset_dbu`

        |

        :keywords:`LOAD` path/to/dbunit-conf.properties :keywords:`AS` :resources:`conf_file`
 
        :keywords:`CONVERT` :resources:`conf_file` :keywords:`TO` :resource-type:`conf.dbunit` (:converter-name:`structured`) :keywords:`AS` :resources:`conf_dbu`

        |

        :keywords:`EXECUTE` :converter-name:`insert` :keywords:`WITH` :resources:`dataset_dbu` :keywords:`ON` :resources:`my_database-db` :keywords:`USING` :resources:`conf_dbu`,$( operation : INSERT ) :keywords:`AS` $()

|

----

***************************************
'delete' 'dataset.dbunit' on 'database'
***************************************

.. admonition:: What |_|?

    This command delete a DbUnit Dataset on the specified database target.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`delete` :keywords:`WITH` :resources:`{dataset<Res:dataset.dbunit>}` :keywords:`ON` :resources:`{<Tar:database>}` :keywords:`AS` $() [:keywords:`USING` [$(operation : <type>)],[:resources:`{<Res:conf.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`]]

**> Input** |_|:

    * ``dataset<Res:dataset.dbunit>`` |_|: The name of the resource which references a DbUnit dataset (``dataset.dbunit`` type resource).

    * ``<Tar:database>`` |_|: The name (in the context) of the database to use (``database`` type target).

    * ``<type>`` |_|: 2 values are possible for this parameter |_|:

        * ``DELETE`` |_|: This operation deletes only the dataset contents from the database. This operation does not delete the entire table contents but only data that are present in the dataset.

        * ``DELETE_ALL`` |_|: Deletes all rows of tables present in the specified dataset. If the dataset does not contains a particular table, but that table exists in the database, the database table is not affected. Table are truncated in reverse sequence.

    .. admonition:: Remark

         If "$(operation |_|: |_| <type>)" is not defined, property is by default ``DELETE_ALL``.

    * ``<Res:conf.dbunit>`` |_|: This resource contains DbUnit configuration properties.

    * ``<Res:conf.dbunit.ppk>`` |_|: The name of the resource which references a configuration file to define the pseudo primary keys.

    .. admonition:: Remarks

        1. If for a table a primary key and a pseudo primary key are defined, the pseudo primary key override the primary key.

        2. If for a table a pseudo primary key is defined with one or more non existents columns, the command fails.

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/dataset_to_insert.xml :keywords:`AS` :resources:`dataset_file`
 
        :keywords:`CONVERT` :resources:`dataset_file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`dataset_xml`
 
        :keywords:`CONVERT` :resources:`dataset_xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset_dbu`

        |

        :keywords:`LOAD` path/to/dbunit-conf.properties :keywords:`AS` :resources:`conf_file`
 
        :keywords:`CONVERT` :resources:`conf_file` :keywords:`TO` :resource-type:`conf.dbunit` (:converter-name:`structured`) :keywords:`AS` :resources:`conf_dbu`

        |

        :keywords:`EXECUTE` :converter-name:`delete` :keywords:`WITH` :resources:`dataset_dbu` :keywords:`ON` :resources:`my_database-db` :keywords:`USING` :resources:`conf_dbu`,$( operation : DELETE ) :keywords:`AS` $()

|

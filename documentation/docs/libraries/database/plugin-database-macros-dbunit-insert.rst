..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################################
Database Plugin - Macros - Insert DbUnit
########################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
    :local:

|

*****************************************
# INSERT_DBUNIT {dataset} INTO {database}
*****************************************

.. admonition:: What |_|?

    This macro will insert all the data listed in the 'dataset file' into the 'database'.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp{%%rand1}.file

    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp_{%%rand2}.file
    CONVERT __temp_{%%rand2}.file TO xml(structured) AS __temp_{%%rand3}.xml
    CONVERT __temp_{%%rand3}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand4}.dbu

    EXECUTE insert WITH __temp_{%%rand4}.dbu ON {database} USING $(operation:insert) AS __temp_{%%rand5}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).

**Remark** |_|: The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).

**Example** |_|:

    .. list-table::

        * - :macros:`# INSERT_DBUNIT path/to/dataset.xml INTO my_database`

    **DbUnit dataset** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-dataset.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-macro.png

    **The employees have been inserted in the database** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-result.png

| 

--------------------

**************************************************************
# INSERT_DBUNIT {dataset} INTO {database} WITH CONFIG {config}
**************************************************************

.. admonition:: What |_|?

    This macro will insert all the data listed in the 'dataset file' into the 'database' using a DbUnit configuration file.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    LOAD {dataset} AS __temp{%%rand3}.file

    CONVERT __temp{%%rand3}.file TO file(param.relativedate) AS __temp_{%%rand4}.file
    CONVERT __temp_{%%rand4}.file TO xml(structured) AS __temp_{%%rand5}.xml
    CONVERT __temp_{%%rand5}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand6}.dbu

    EXECUTE insert WITH __temp_{%%rand6}.dbu ON {database} USING $(operation:insert),__temp{config}{%%rand2}.conf AS __temp_{%%rand7}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').

**Remarks** |_|:

    #. The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).
    #. The file designed by ``{config}`` must respect the same rules than a file which would serve to create an SKF ``conf.dbunit`` type resource via the converter (:ref:`From file to conf.dbunit<from.file.to.conf.dbunit.anchor>`).

**Example** |_|:

    .. list-table::

        * - :macros:`# INSERT_DBUNIT path/to/dataset.xml INTO my_database WITH CONFIG  path/to/my_config_dbunit.properties`

    **DbUnit configuration file** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-using-config-conf-file.png

    The table name is now case sensitive.

    **In dataset, we put capital letters in the table name** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-using-config-dataset.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-using-config-macro.png

    **The execution raises an error** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-using-config-error.png

    **Now we switch the property to "false" in the configuration file** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-using-config-conf-file2.png

    **There is no error this time and users have been inserted in the database** |_|:

    .. figure:: ../../_static/plugin-database/insert-dbunit/insert-dbunit-result.png

| 

--------------------

***********************************************************
# INSERT_DBUNIT {dataset} INTO {database} USING {ppkfilter}
***********************************************************

.. admonition:: What |_|?

    This macro will insert all datas listed in the 'dataset file' into the 'database' using a DbUnit filter.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp{%%rand1}.file
    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO xml(structured) AS __temp{%%rand3}.xml
    CONVERT __temp{%%rand3}.xml TO dataset.dbunit(dataset) AS __temp{%%rand4}.dbu

    LOAD {ppkfilter} AS __temp{%%rand5}.file
    CONVERT __temp{%%rand5}.file TO properties(structured) AS __temp{%%rand6}.props
    CONVERT __temp{%%rand6}.props TO conf.dbunit.ppk(from.properties) AS __temp{%%rand7}.ppk

    EXECUTE insert WITH __temp{%%rand4}.dbu ON {database} USING __temp{%%rand7}.ppk,$(operation:insert) AS __temp_{%%rand8}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{ppkfilter}`` |_|: A DbUnit filter referring to pseudo primary keys ('.properties').

**Example** |_|:

.. list-table::

    * - :macros:`# INSERT_DBUNIT path/to/dataset.xml INTO my_database USING path/to/my_filter_dbunit.properties`

| 

--------------------

********************************************************************************
# INSERT_DBUNIT {dataset} INTO {database} WITH CONFIG {config} USING {ppkfilter}
********************************************************************************

.. admonition:: What |_|?

    This macro will insert all the data listed in the 'dataset file' into the 'database' using a DbUnit configuration file and a DbUnit filter.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    LOAD {dataset} AS __temp{%%rand3}.file
    CONVERT __temp{%%rand3}.file TO file(param.relativedate) AS __temp_{%%rand4}.file
    CONVERT __temp_{%%rand4}.file TO xml(structured) AS __temp_{%%rand5}.xml
    CONVERT __temp_{%%rand5}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand6}.dbu

    LOAD {ppkfilter} AS __temp{%%rand7}.file
    CONVERT __temp{%%rand7}.file TO properties(structured) AS __temp{%%rand8}.props
    CONVERT __temp{%%rand8}.props TO conf.dbunit.ppk(from.properties) AS __temp{%%rand9}.ppk

    EXECUTE insert WITH __temp_{%%rand6}.dbu ON {database} USING __temp{%%rand9}.ppk,$(operation:insert),__temp{config}{%%rand2}.conf AS __temp_{%%rand10}.result

**> Input** |_|:

        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').
        * ``{ppkfilter}`` |_|: A DbUnit filter referring to pseudo primary keys ('.properties').

**Example** |_|:

.. list-table::

    * - :macros:`# INSERT_DBUNIT path/to/dataset.xml INTO my_database WITH CONFIG path/to/my_config_dbunit.properties USING path/to/my_filter_dbunit.properties`

|

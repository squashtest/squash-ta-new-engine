..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################################
Database Plugin - Macros - Delete DbUnit
########################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
    :local:

|

*****************************************
# DELETE_DBUNIT {dataset} FROM {database}
*****************************************

.. admonition:: What |_|?

    This macro will delete all the data listed in the 'dataset file' from the 'database'.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp_{%%rand1}.file

    CONVERT __temp_{%%rand1}.file TO xml(structured) AS __temp_{%%rand2}.xml
    CONVERT __temp_{%%rand2}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand3}.dbu

    EXECUTE delete WITH __temp_{%%rand3}.dbu ON {database} USING $(operation : delete) AS __temp_{%%rand4}.result

**> Input** |_|:

        * ``database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.

**Remark** |_|: The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).

**Example** |_|:

    .. list-table::

        * - :macros:`# DELETE_DBUNIT path/to/dataset.xml FROM my_database`

    **Database overview** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-database-overview.png


    **Dataset .xml File** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-dataset.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-macro.png

    **There is only one employee left in the database** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-database-result.png

| 

--------------------

**************************************************************
# DELETE_DBUNIT {dataset} FROM {database} WITH CONFIG {config}
**************************************************************

.. admonition:: What |_|?

    This macro will delete all the data listed in the 'dataset file' from the 'database' using a DbUnit configuration file.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    LOAD {dataset} AS __temp_{%%rand3}.file

    CONVERT __temp_{%%rand3}.file TO xml(structured) AS __temp_{%%rand4}.xml
    CONVERT __temp_{%%rand4}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand5}.dbu

    EXECUTE delete WITH __temp_{%%rand5}.dbu USING $(operation : delete),__temp{config}{%%rand2}.conf ON {database} AS __temp_{%%rand6}.result

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').

**Remarks** |_|:

    #. The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).
    #. The file designed by ``{config}`` must respect the same rules than a file which would serve to create an SKF ``conf.dbunit`` type resource via the converter (:ref:`From file to conf.dbunit<from.file.to.conf.dbunit.anchor>`).

**Example** |_|:

    .. list-table::

        * - :macros:`# DELETE_DBUNIT path/to/dataset.xml FROM my_database WITH CONFIG path/to/my_config_dbunit.properties`

| 

--------------------

***********************************************************
# DELETE_DBUNIT {dataset} FROM {database} USING {ppkfilter}
***********************************************************

.. admonition:: What |_|?

    This macro will load the specified xml dataset and delete datas listed in from the 'database' using a filter DbUnit.

**Underlying instructions** |_|:

.. code-block:: html

    // Load and convert the dbunit dataset
    LOAD {dataset} AS __{%%r1}.file
    CONVERT __{%%r1}.file TO file(param.relativedate) AS __{%%r2}.file
    CONVERT __{%%r2}.file TO xml(structured) AS __{%%r3}.xml
    CONVERT __{%%r3}.xml TO dataset.dbunit(dataset) AS __{%%r4}.dbu

    // Load and convert the pseudo primary key filter
    LOAD {ppkfilter} AS __{%%r5}.file
    CONVERT __{%%r5}.file TO properties(structured) AS __{%%r6}.props
    CONVERT __{%%r6}.props TO conf.dbunit.ppk(from.properties) AS __{%%r7}.ppk

    // Execute delete operation using the pseudo primary key filter
    EXECUTE delete WITH __{%%r4}.dbu ON {database} USING __{%%r7}.ppk,$(operation : delete) AS __{%%r8}.result

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{ppkfilter}`` |_|: A DbUnit filter referring to pseudo primary keys ('.properties').

**Example** |_|:

    .. list-table::

        * - :macros:`# DELETE_DBUNIT path/to/dataset.xml FROM my_database USING path/to/my_filter_dbunit.properties`


    **For this example, we set the table employee with no primary key** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-table-properties.png

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-database-overview.png

    **We set "username" as pseudo primary key in properties file** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-ppkfile.png

    **Dataset .xml file** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-dataset.png

    **We execute the macro without using ppk properties file** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-macro.png

    **The following error occurs** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-error.png

    **We execute the macro with the ppk properties file** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-macro2.png

    **The operation succeeds and all employees are deleted** |_|:

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-success.png

    .. figure:: ../../_static/plugin-database/delete-dbunit/delete-dbunit-using-ppk-database-result.png



| 

--------------------

*********************************************
# DELETE_ALL_DBUNIT {dataset} FROM {database}
*********************************************

.. admonition:: What |_|?

    This macro will load the specified xml dataset and truncate every table listed in from the 'database'.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp_{%%rand1}.file

    CONVERT __temp_{%%rand1}.file TO xml(structured) AS __temp_{%%rand2}.xml
    CONVERT __temp_{%%rand2}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand3}.dbu

    EXECUTE delete WITH __temp_{%%rand3}.dbu ON {database} AS __temp_{%%rand4}.result

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.

**Remark** |_|: The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).

**Example** |_|:

.. list-table::

    * - :macros:`# DELETE_ALL_DBUNIT path/to/dataset.xml FROM my_database`

| 

--------------------

******************************************************************
# DELETE_ALL_DBUNIT {dataset} FROM {database} WITH CONFIG {config}
******************************************************************

.. admonition:: What |_|?

    This macro will load the specified xml dataset and truncate every table listed in from the 'database' using a DbUnit configuration file.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    LOAD {dataset} AS __temp_{%%rand3}.file

    CONVERT __temp_{%%rand3}.file TO xml(structured) AS __temp_{%%rand4}.xml
    CONVERT __temp_{%%rand4}.xml TO dataset.dbunit(dataset) AS __temp_{%%rand5}.dbu

    EXECUTE delete WITH __temp_{%%rand5}.dbu USING __temp{config}{%%rand2}.conf ON {database} AS __temp_{%%rand6}.result

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').

**Remarks** |_|:

    #. The file designed by ``{dataset}`` must respect the same rules than a file which would serve to create an SKF ``dataset.dbunit`` type resource via the converter (:ref:`From xml to dataset.dbunit<from.xml.to.dataset.dbunit.anchor>`).
    #. The file designed by ``{config}`` must respect the same rules than a file which would serve to create an SKF ``conf.dbunit`` type resource via the converter (:ref:`From file to conf.dbunit<from.file.to.conf.dbunit.anchor>`).

**Example** |_|:

.. list-table::

    * - :macros:`# DELETE_ALL_DBUNIT path/to/dataset.xml FROM my_database WITH CONFIG path/to/my_config_dbunit.properties`

|

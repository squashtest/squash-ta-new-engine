..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
Database Plugin - Macros
########################

.. toctree::
   :maxdepth: 2

    Execute SQL and SQL script <plugin-database-macros-execute-sql-sql_script.rst>
    Insert DbUnit <plugin-database-macros-dbunit-insert.rst>
    Delete DbUnit <plugin-database-macros-dbunit-delete.rst>
    Refresh DbUnit <plugin-database-macros-dbunit-refresh.rst>
    Update DbUnit <plugin-database-macros-dbunit-update.rst>
    Assert DbUnit <plugin-database-macros-dbunit-assert.rst>
    Verify DbUnit <plugin-database-macros-dbunit-verify.rst>

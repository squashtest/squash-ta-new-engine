..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _database.target.anchor:

########################
Database Plugin - Target
########################

|

********
Database
********

**Category-name** |_|: :guilabel:`database`

.. admonition:: What |_|?

        A :guilabel:`database` target represents, well, a database. The file ``(.properties)`` which allows to define this target to test contains all needed informations to connect to the database.

**Configuration** |_| (basic) |_|: A simple ``.properties`` file dropped in the 'targets' directory of your test project. The file must include the shebang on the very first line |_|: ``#!db`` and it must contain AT LEAST ``squashtest.tf.database.driver`` and ``squashtest.tf.database.url``.

**Available properties** |_|:

        * squashtest.ta.database.driver |_|: the jdbc driver supporting your database
        * squashtest.ta.database.url |_|: aka connection string, this is the url of your database
        * squashtest.ta.database.username |_|: the username to connect with
        * squashtest.ta.database.password |_|: the corresponding password

.. admonition:: Remark

        If properties ``squashtest.tf.database.username`` and ``squashtest.tf.database.password`` are not indicated, then the user name and the user password must be indicated in the ``squashtest.tf.database.url property``. If they are indicated in both, then datas from the properties ``squashtest.tf.database.username`` and ``squashtest.tf.database.password`` prime.

**Configuration** |_| (advanced) |_|: Using the same ``.properties`` file you can also specify options related to the pooling of the datasource. As for version 1.0.x SKF will create its own datasource - no support for JNDI references for instance (yet).

To this end, SKF is backed by the `c3p0 <https://www.mchange.com/projects/c3p0/index.html>`_ technology. SKF will transmit to c3p0 its regular configuration properties (see `here <https://www.mchange.com/projects/c3p0/index.html#configuration_properties>`_). For the sake of consistency with the rest of the file, each key must be prefixed with ``squashtest.ta.database.pool.`` + property. For instance ``squashtest.ta.database.pool.maxPoolSize`` will configure the property ``maxPoolSize``.

The only exception to this are the c3p0 properties ``user`` and ``password``, that already exist as basic configuration. Consequently they will be ignored |_|: namely, ``squashtest.ta.database.pool.user`` and ``squashtest.ta.database.pool.password`` will be shunted. Please use the basic keys instead.

**Example of valid configuration file** |_|::

    #!db

    # basic configuration
    squashtest.ta.database.driver = com.mysql.jdbc.Driver
    squashtest.ta.database.url = jdbc:mysql://localhost:3306/my_database
    squashtest.ta.database.username = tester
    squashtest.ta.database.password = _tester

    # advanced configuration
    squashtest.ta.database.pool.acquireRetryDelay = 3000
    squashtest.ta.database.pool.maxPoolSize = 40

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################################
Database Plugin - Macros - Verify DbUnit
########################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
    :local:

|

****************************************************
# VERIFY_DBUNIT TARGET {database} CONTAINS {dataset}
****************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database'. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    EXECUTE get.all WITH $() ON {database} AS __actual_{%%rand1}.dbu

    LOAD {dataset} AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO file(param.relativedate) AS __temp_{%%rand3}.file
    CONVERT __temp_{%%rand3}.file TO xml(structured) AS __temp_{%%rand4}.xml
    CONVERT __temp_{%%rand4}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand5}.dbu

    VERIFY __actual_{%%rand1}.dbu DOES contain THE __expected_{%%rand5}.dbu

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database CONTAINS path/to/my_dataset.xml`

This macro is very similar to the :keywords:`ASSERT` macro. For more information, please check the following :ref:`page <database.assert.contains.macro.anchor>`.

| 

--------------------

*************************************************************************
# VERIFY_DBUNIT TARGET {database} CONTAINS {dataset} WITH CONFIG {config}
*************************************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' using a DbUnit configuration file. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    EXECUTE get.all WITH $() ON {database} USING __temp{config}{%%rand2}.conf AS __actual_{%%rand3}.dbu

    LOAD {dataset} AS __temp{%%rand4}.file
    CONVERT __temp{%%rand4}.file TO file(param.relativedate) AS __temp_{%%rand5}.file
    CONVERT __temp_{%%rand5}.file TO xml(structured) AS __temp_{%%rand6}.xml
    CONVERT __temp_{%%rand6}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand7}.dbu

    VERIFY __actual_{%%rand3}.dbu DOES contain THE __expected_{%%rand7}.dbu

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database CONTAINS path/to/my_dataset.xml WITH CONFIG path/to/my_config_dbunit.properties`

| 

--------------------

*************************************************************************
# VERIFY_DBUNIT TARGET {database} CONTAINS {dataset} WITH FILTER {filter}
*************************************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' using a DbUnit filter. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {dataset} AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO file(param.relativedate) AS __temp_{%%rand3}.file
    CONVERT __temp_{%%rand3}.file TO xml(structured) AS __temp_{%%rand4}.xml
    CONVERT __temp_{%%rand4}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand5}.dbu

    LOAD {filter} AS __filter_{%%rand6}.file
    CONVERT __filter_{%%rand6}.file TO filter.dbunit(filter) AS __filter_{%%rand7}.filter

    VERIFY __actual_{%%rand1}.dbu DOES contain THE __expected_{%%rand5}.dbu USING __filter_{%%rand7}.filter

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{filter}`` |_|: A Dbunit filter ( filter.dbunit TA resource).

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database CONTAINS path/to/my_dataset.xml WITH FILTER path/to/my_dbunit_filter.xml`

| 

--------------------

*********************************************************************************************
# VERIFY_DBUNIT TARGET {database} CONTAINS {dataset} WITH CONFIG {config} AND FILTER {filter}
*********************************************************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' using a DbUnit configuration file and a DbUnit filter. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    EXECUTE get.all WITH $() ON {database} USING __temp{config}{%%rand2}.conf AS __actual_{%%rand3}.dbu

    LOAD {dataset} AS __temp{%%rand4}.file
    CONVERT __temp{%%rand4}.file TO file(param.relativedate) AS __temp_{%%rand5}.file
    CONVERT __temp_{%%rand5}.file TO xml(structured) AS __temp_{%%rand6}.xml
    CONVERT __temp_{%%rand6}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand7}.dbu

    LOAD {filter} AS __filter_{%%rand8}.file
    CONVERT __filter_{%%rand8}.file TO filter.dbunit(filter) AS __filter_{%%rand9}.filter

    VERIFY __actual_{%%rand3}.dbu DOES contain THE __expected_{%%rand7}.dbu USING __filter_{%%rand9}.filter

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties'). It should be a 'conf.dbunit' SKF resource.
        * ``{filter}`` |_|: A Dbunit filter xml file. It should be a 'filter.dbunit' SKF resource.

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database CONTAINS path/to/my_dataset.xml WITH CONFIG path/to/my_dbunit_config.properties AND FILTER path/to/my_dbunit_filter.xml`

| 

--------------------

**************************************************
# VERIFY_DBUNIT TARGET {database} EQUALS {dataset}
**************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' and the reverse. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    EXECUTE get.all WITH $() ON {database} AS __actual_{%%rand1}.dbu

    LOAD {dataset} AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO file(param.relativedate) AS __temp_{%%rand3}.file
    CONVERT __temp_{%%rand3}.file TO xml(structured) AS __temp_{%%rand4}.xml
    CONVERT __temp_{%%rand4}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand5}.dbu

    VERIFY __actual_{%%rand1}.dbu IS equal THE __expected_{%%rand5}.dbu

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# ASSERT_DBUNIT TARGET my_database EQUALS path/to/my_dataset.xml`

This macro is very similar to the :keywords:`ASSERT` macro. For more information, please check the following :ref:`page <database.assert.equals.macro.anchor>`.

| 

--------------------

***********************************************************************
# VERIFY_DBUNIT TARGET {database} EQUALS {dataset} WITH CONFIG {config}
***********************************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' and the reverse using a DbUnit configuration file. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    EXECUTE get.all WITH $() ON {database} USING __temp{config}{%%rand2}.conf AS __actual_{%%rand3}.dbu

    LOAD {dataset} AS __temp{%%rand4}.file
    CONVERT __temp{%%rand4}.file TO file(param.relativedate) AS __temp_{%%rand5}.file
    CONVERT __temp_{%%rand5}.file TO xml(structured) AS __temp_{%%rand6}.xml
    CONVERT __temp_{%%rand6}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand7}.dbu

    VERIFY __actual_{%%rand3}.dbu IS equal THE __expected_{%%rand7}.dbu

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties').

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database EQUALS path/to/my_dataset.xml WITH CONFIG path/to/my_config_dbunit.properties`

| 

--------------------

***********************************************************************
# VERIFY_DBUNIT TARGET {database} EQUALS {dataset} WITH FILTER {filter}
***********************************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' and the reverse using a DbUnit filter. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    EXECUTE get.all WITH $() ON {database} AS __actual_{%%rand1}.dbu

    LOAD {dataset} AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO file(param.relativedate) AS __temp_{%%rand3}.file
    CONVERT __temp_{%%rand3}.file TO xml(structured) AS __temp_{%%rand4}.xml
    CONVERT __temp_{%%rand4}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand5}.dbu

    LOAD {filter} AS __filter_{%%rand6}.file
    CONVERT __filter_{%%rand6}.file TO filter.dbunit(filter) AS __filter_{%%rand7}.filter

    VERIFY __actual_{%%rand1}.dbu IS equal THE __expected_{%%rand5}.dbu USING __filter_{%%rand7}.filter

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{filter}`` |_|: A DbUnit filter xml file. It should be a 'filter.dbunit' SKF resource.

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database EQUALS path/to/my_dataset.xml WITH FILTER path/to/my_dbunit_filter.xml`

| 

--------------------

*******************************************************************************************
# VERIFY_DBUNIT TARGET {database} EQUALS {dataset} WITH CONFIG {config} AND FILTER {filter}
*******************************************************************************************

.. admonition:: What |_|?

    This macro will check that all the data listed in the 'dataset file' exist in the 'database' and the reverse using a DbUnit configuration file and a DbUnit filter. For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {config} AS __temp{config}{%%rand1}.file
    CONVERT __temp{config}{%%rand1}.file TO conf.dbunit AS __temp{config}{%%rand2}.conf

    EXECUTE get.all WITH $() ON {database} USING __temp{config}{%%rand2}.conf AS __actual_{%%rand3}.dbu

    LOAD {dataset} AS __temp{%%rand4}.file
    CONVERT __temp{%%rand4}.file TO file(param.relativedate) AS __temp_{%%rand5}.file
    CONVERT __temp_{%%rand5}.file TO xml(structured) AS __temp_{%%rand6}.xml
    CONVERT __temp_{%%rand6}.xml TO dataset.dbunit(dataset) AS __expected_{%%rand7}.dbu

    LOAD {filter} AS __filter_{%%rand8}.file
    CONVERT __filter_{%%rand8}.file TO filter.dbunit(filter) AS __filter_{%%rand9}.filter

    VERIFY __actual_{%%rand3}.dbu IS equal THE __expected_{%%rand7}.dbu USING __filter_{%%rand9}.filter

**> Input** |_|:

        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * ``{dataset}`` |_|: A flat xml dbunit dataset file.
        * ``{config}`` |_|: A configuration file for DbUnit ('.properties'). It should be a 'conf.dbunit' TA resource.
        * ``{filter}`` |_|: A DbUnit filter xml file. It should be a 'filter.dbunit' TA resource.

**Remark** |_|: If the file designed by ``{dataset}`` contains formulas of date calculation (See the converter :ref:`From file to file via param.relativedate<param.relativedate Overview>`), those ones are calculated and replaced by the value.

**Example** |_|:

.. list-table::

    * - :macros:`# VERIFY_DBUNIT TARGET my_database EQUALS path/to/my_dataset.xml WITH CONFIG path/to/my_dbunit_config.properties AND FILTER path/to/my_dbunit_filter.xml`

|

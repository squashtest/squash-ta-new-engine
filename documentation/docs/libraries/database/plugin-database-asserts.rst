..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#########################
Database Plugin - Asserts
#########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

************************************************
'dataset.dbunit' does 'contain' 'dataset.dbunit' 
************************************************

.. admonition:: What |_|?

    Asserts that the first dataset contains the second one.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{dataset1<Res:dataset.dbunit>}` :keywords:`DOES` :converter-name:`contain` :keywords:`THE` :resources:`{dataset2<Res:dataset.dbunit>}` [:keywords:`USING` [:resources:`{<Res:filter.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`] ]

        :keywords:`VERIFY` :resources:`{dataset1<Res:dataset.dbunit>}` :keywords:`DOES` :converter-name:`contain` :keywords:`THE` :resources:`{dataset2<Res:dataset.dbunit>}` [:keywords:`USING` [:resources:`{<Res:filter.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`] ]

.. note |_|::

    For differences between ASSERT and VERIFY assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``dataset1<Res:dataset.dbunit>`` |_|: The name of the resource which references the first DbUnit dataset (``dataset.dbunit`` type resource).
    
    * ``dataset2<Res:dataset.dbunit>`` |_|: The name of the resource which references the second DbUnit dataset (``dataset.dbunit`` type resource).
    
    * ``<Res:filter.dbunit>`` |_| (Optional) |_|: This resource contains a filter DbUnit (``filter.dbunit`` type resource).
    
    * ``<Res:conf.dbunit.ppk>`` |_| (Optional) |_|: The name of the resource which references a configuration file to define the pseudo primary keys (``conf.dbunit.ppk`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/dataset1.xml :keywords:`AS` :resources:`dataset1_file`
  
        :keywords:`CONVERT` :resources:`dataset1_file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`dataset1_xml`
 
        :keywords:`CONVERT` :resources:`dataset1_xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset1_dbu`

        |

        :keywords:`LOAD` path/to/dataset1.xml :keywords:`AS` :resources:`dataset2_file`
  
        :keywords:`CONVERT` :resources:`dataset2_file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`dataset2_xml`
 
        :keywords:`CONVERT` :resources:`dataset2_xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset2_dbu`

        |

        // Get the pseudo primary keys
  
        :keywords:`LOAD` path/to/my_ppk.properties :keywords:`AS` :resources:`ppk_file`
  
        :keywords:`CONVERT` :resources:`ppk_file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`ppk_properties`
  
        :keywords:`CONVERT` :resources:`ppk_properties` :keywords:`TO` :resource-type:`conf.dbunit.ppk` (:converter-name:`from.properties`) :keywords:`AS` :resources:`ppk_dbu`

        |

        // Load the DbUnit filter
  
        :keywords:`LOAD` path/to/filter-name.xml :keywords:`AS` :resources:`filter_file`
 
        :keywords:`CONVERT` :resources:`filter_file` :keywords:`TO` :resource-type:`filter.dbunit` :keywords:`AS` :resources:`filter_dbu`

        |

        // Compare the two datasets
  
        :keywords:`ASSERT` :resources:`dataset1_dbu` :keywords:`DOES` :converter-name:`contain` :keywords:`THE` :resources:`dataset2_dbu` :keywords:`USING` :resources:`ppk_dbu,filter_dbu` 
    
.. admonition:: Remarks

        * During the assertion, if the first or the second dataset contains primary keys they are used for the assertion.
        
        * If for a given table, a primary key and a pseudo primary key are defined, pseudo primary key overrides the primary key.
        
        * If for a given table, a pseudo primary key has one or several columns excluded from the assertion by a DbUnit filter, the command fails.

|

----

********************************************
'dataset.dbunit' is 'equal' 'dataset.dbunit'
********************************************

.. admonition:: What |_|?

    Asserts that the first dataset is equal to the second one (same number of tables, for each table same number of lines / columns and same data).

.. list-table::

    * - :keywords:`ASSERT` :resources:`{dataset1<Res:dataset.dbunit>}` :keywords:`IS` :converter-name:`equal` :keywords:`THE` :resources:`{dataset2<Res:dataset.dbunit>}` [:keywords:`USING` [:resources:`{<Res:filter.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`] ]

        :keywords:`VERIFY` :resources:`{dataset1<Res:dataset.dbunit>}` :keywords:`IS` :converter-name:`equal` :keywords:`THE` :resources:`{dataset2<Res:dataset.dbunit>}` [:keywords:`USING` [:resources:`{<Res:filter.dbunit>}`],[:resources:`{<Res:conf.dbunit.ppk>}`] ]

.. note |_|::

    For differences between ASSERT and VERIFY assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``dataset1<Res:dataset.dbunit>`` |_|: The name of the resource which references the first DbUnit dataset.(``dataset.dbunit`` type resource).
  
    * ``dataset2<Res:dataset.dbunit>`` |_|: The name of the resource which references the second DbUnit dataset.(``dataset.dbunit`` type resource).
    
    * ``<Res:filter.dbunit>`` |_| (Optional) |_|: This resource contains a filter DbUnit (``filter.dbunit`` type resource).
    
    * ``<Res:conf.dbunit.ppk>`` |_| (Optional) |_|: The name of the resource which references a configuration file to define the pseudo primary keys.(``conf.dbunit.ppk`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/dataset1.xml :keywords:`AS` :resources:`dataset1_file`
  
        :keywords:`CONVERT` :resources:`dataset1_file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`dataset1_xml`
 
        :keywords:`CONVERT` :resources:`dataset1_xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset1_dbu`

        |

        :keywords:`LOAD` path/to/dataset1.xml :keywords:`AS` :resources:`dataset2_file`
  
        :keywords:`CONVERT` :resources:`dataset2_file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`dataset2_xml`
 
        :keywords:`CONVERT` :resources:`dataset2_xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`dataset2_dbu`

        |

        // Get the pseudo primary keys
  
        :keywords:`LOAD` path/to/my_ppk.properties :keywords:`AS` :resources:`ppk_file`
  
        :keywords:`CONVERT` :resources:`ppk_file` :keywords:`TO` :resource-type:`properties` (:converter-name:`structured`) :keywords:`AS` :resources:`ppk_properties`
  
        :keywords:`CONVERT` :resources:`ppk_properties` :keywords:`TO` :resource-type:`conf.dbunit.ppk` (:converter-name:`from.properties`) :keywords:`AS` :resources:`ppk_dbu`

        |

        // Load the DbUnit filter
  
        :keywords:`LOAD` path/to/filter-name.xml :keywords:`AS` :resources:`filter_file`
 
        :keywords:`CONVERT` :resources:`filter_file` :keywords:`TO` :resource-type:`filter.dbunit` :keywords:`AS` :resources:`filter_dbu`

        |

        // Compare the two datasets
  
        :keywords:`ASSERT` :resources:`dataset1_dbu` :keywords:`IS` :converter-name:`equal` :keywords:`THE` :resources:`dataset2_dbu` :keywords:`USING` :resources:`ppk_dbu,filter_dbu` 
    
.. admonition:: Remarks

    * During the assertion, if the first or the second dataset contains primary keys they are used for the assertion.
        
    * If for a given table, a primary key and a pseudo primary key are defined, pseudo primary key overrides the primary key.
        
    * If for a given table, a pseudo primary key has one or several columns excluded from the assertion by a DbUnit filter, the command fails.

|

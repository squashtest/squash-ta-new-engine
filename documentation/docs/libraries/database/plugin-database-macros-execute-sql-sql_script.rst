..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################################################
Database Plugin - Macros - Execute SQL and SQL script
#####################################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
    :local:

.. _libraries.database.macros.execute.sql.anchor:

|

**********************************************
# EXECUTE_SQL {file} ON {database} AS {result}
**********************************************

.. admonition:: What |_|?

    This macro will load and execute an SQL query against the database, then the result will be returned under the name you typed for the last parameter.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {file} AS __temp{%%rand1}.file

    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO query.sql(query) AS __temp_{%%rand3}.query
    EXECUTE execute WITH __temp_{%%rand3}.query ON {database} AS {result}

**> Input** |_|:

        * ``{file}`` |_|: A SQL query ('.sql').
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).

**Remark** |_|: The ``{file}`` must respect the same rules than a file which would serve to create an SKF ``query.sql`` type resource via the converter (:ref:`From file to query.sql<from.file.to.query.sql.anchor>`).
    
**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which will contain the result of the SQL query(``result.sql`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE_SQL path/to/query1.sql ON my_database AS result`


**Example with an INSERT query** |_|:

    **Database overview** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-database-overview.png


    **.sql file for the query** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-query.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-macro.png

    **The new employee has been inserted in the database** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-database-result.png

**Example with a SELECT query** |_|:

    **.sql file for the query** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-query2.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-macro2.png

    **To be able to see the result output, we added in this example the following instructions** |_|:

        .. code-block:: html

            CONVERT result TO dataset.dbunit (dataset) USING $(tablename : user) AS dataset
            CONVERT dataset TO xml (dbu.xml) AS my_xml_file

    **You can access to the result output in the following folder which contains temporary files** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-temp-files.png

    .. figure:: ../../_static/plugin-database/execute/execute-sql-temp-files2.png

    **Result output** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-sql-temp-files-result.png  

| 

--------------------

********************************************************************************************************
# EXECUTE_SQL_SCRIPT {file} ON {database} AS {result} WITH ENCODING {encoding} AND DELIMITER {delimiter}
********************************************************************************************************

.. admonition:: What |_|?

    This macro will load and execute an SQL script against the database, then the result will be returned under the name you typed for the last parameter. 

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {file} AS __temp{%%rand1}.file
    DEFINE $(encoding:{encoding}) AS encoding{%%rand1}.opts
    DEFINE $(delimiter:{delimiter}) AS delimiter{%%rand1}.opts

    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp{%%rand2}.file USING encoding{%%rand1}.opts
    CONVERT __temp{%%rand2}.file TO script.sql AS __temp_{%%rand3}.script USING encoding{%%rand1}.opts, delimiter{%%rand1}.opts
    EXECUTE execute WITH __temp_{%%rand3}.script ON {database} AS {result}

**> Input** |_|:

        * ``{file}`` |_|: An SQL script
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).
        * Optional - ``{encoding}`` |_|: Parameter representing the query file encoding. Default value |_|: "UTF-8".
        * Optional - ``{delimiter}`` |_|: Parameter representing the SQL block delimiter. Default value |_|: "@@". It can be used in conjunction with {encoding} or by itself - in which case {encoding} will take its value by default.

**Remark** |_|: The ``{file}`` must respect the same rules as a file used to create an SKF ``script.sql`` type resource via the converter (:ref:`From file to script.sql<from.file.to.script.sql.anchor>`).

**> Output** |_|:

        * ``{result}`` |_|: A free identifier for the result. As the 'execute' command with an sql script return an empty resource, this result resource will also be empty.

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE_SQL_SCRIPT path/to/script.sql ON my_database AS result WITH ENCODING UTF-16 AND DELIMITER $$$`

    **script.sql file** |_|:

    .. code-block:: sql

        DROP TABLE IF EXISTS  `skf`.`employee`;

        CREATE TABLE `skf`.`employee` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `username` VARCHAR(45) NULL,
          `password` VARCHAR(45) NULL,
          `email` VARCHAR(45) NULL,
          PRIMARY KEY (`id`));

        INSERT INTO `skf`.`employee` (`username`, `password`, `email`) VALUES ('bruceW', '?b@Tm@n!_1939', 'brucew@skf.com');
        INSERT INTO `skf`.`employee` (`username`, `password`, `email`) VALUES ('jessicaJ', 'wh1sk3y', 'jessicaJ@skf.com');
        INSERT INTO `skf`.`employee` (`username`, `password`, `email`) VALUES ('homerS', 'd0nuts', 'homers@skf.com');
        INSERT INTO `skf`.`employee` (`username`, `password`, `email`) VALUES ('tonyS', 'tonyIsTheBest', 'tonys@skf.com');
        INSERT INTO `skf`.`employee` (`username`, `password`, `email`) VALUES ('çàééééééèèèèèè', 'ççççççééééééé', 'test');

    **We encode the file in ISO-8859-1 and use special characters :**

    .. figure:: ../../_static/plugin-database/execute/execute-script-sql-database-script-overview.png

 
    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-script-sql-macro2.png

    **Database overview without specifying encoding in macro** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-script-sql-database-result2.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-script-sql-macro.png

    **Database overview with encoding** |_|:

    .. figure:: ../../_static/plugin-database/execute/execute-script-sql-database-result.png

| 

--------------------

***************************************************************************
#EXECUTE_SQL_SCRIPT_BY_REMOVING_SEPARATOR {file} ON {database} AS {result} 
***************************************************************************

.. admonition:: What |_|?

    This macro will load and execute an SQL script against the database, then the result will be returned under the name you typed for the last parameter. The separator (";") at the end of each SQL query of the script will be removed.

**Underlying instructions:**

.. code-block:: html

    LOAD {file} AS __temp{%%rand1}.file

    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO script.sql AS __temp_{%%rand3}.script
    EXECUTE execute WITH __temp_{%%rand3}.script ON {database} USING $(keep.separator:false) AS {result}

**> Input** |_|:

        * ``{file}`` |_|: An SQL script.
        * ``{database}`` |_|: The name (in the context) of the database to use (``database`` type target).

**Remark** |_|: The ``{file}`` must respect the same rules than a file which would serve to create an SKF ``script.sql`` type resource via the converter (:ref:`From file to script.sql<from.file.to.script.sql.anchor>`).

**> Output** |_|:

        * ``{result}`` |_|: A free identifier for the result. As the 'execute' command with an sql script returns an empty resource, this result resource will also be empty.

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SQL_SCRIPT_BY_REMOVING_SEPARATOR path/to/my_script.sql ON my_database AS result`

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###########################
Database Plugin - Resources
###########################

.. contents:: Contents |_|:
   :local:

|

***********
conf.dbunit
***********

**Category-name** |_|: :guilabel:`conf.dbunit`

.. admonition:: What |_|?

    :guilabel:`conf.dbunit` is a resource type whose role is to configure DbUnit transactions.


:ref:`How to use it ?<from.file.to.conf.dbunit.anchor>`


|

----

***************
conf.dbunit.ppk
***************

**Category-name** |_|: :guilabel:`conf.dbunit.ppk`

.. admonition:: What |_|?

    :guilabel:`conf.dbunit.ppk` is a resource type that represents a pseudo-primary key filter for DbUnit transactions. The file format is as follows |_|: for each table, define a property with the name of the table. The value of the property is the comma-separated list of the names of the columns that make up the primary key.

**Here is an example of definition file content** |_|::

    employee=employee_id
    company=company_id
    contract=contract_employee_id,contract_company_id

.. admonition:: Why |_|?

    Usually DbUnit reads directly from the database, information about the tables it needs to know, including their primary keys. However some tables simply have no primary key, which can lead DbUnit to failures for a few operations. The :guilabel:`conf.dbunit.ppk` is a way to provide DbUnit with these extra information.

.. note |_|::

    If a primary key exists whereas a pseudo primary key is defined, the pseudo primary key overrides the primary key.

|

----

**************
dataset.dbunit
**************

**Category-name** |_|: :guilabel:`dataset.dbunit`

.. admonition:: What |_|?

    :guilabel:`dataset.dbunit` is a resource type that represents a DbUnit DataSet.

:ref:`How to use it ?<from.xml.to.dataset.dbunit.anchor>`

|

----

*************
filter.dbunit
*************

**Category-name** |_|: :guilabel:`filter.dbunit`

.. admonition:: What |_|?

    :guilabel:`filter.dbunit` is a resource type that represents a Dbunit Filter. These filters are used in assertions for comparison between DbUnit datasets (:guilabel:`dataset.dbunit`). Their purpose is to exclude / include from the comparison some tables or some columns if you need to narrow the scope of your assertion.


:ref:`How to use it ?<from.xml.to.filter.dbunit.anchor>`

|

----

*******************
parameter.named.sql
*******************

**Category-name** |_|: :guilabel:`parameter.named.sql`

.. admonition:: What |_|?

    :guilabel:`parameter.named.sql` is a resource type that represents a map of parameters for parameterized sql queries using named parameters (see :guilabel:`query.sql`).

|

----

*********************
parameter.indexed.sql
*********************

**Category-name** |_|: :guilabel:`parameter.indexed.sql`

.. admonition:: What |_|?

   :guilabel:`parameter.indexed.sql` is a resource type that represents a list of parameters for parameterized sql queries using positional parameters (see :guilabel:`query.sql`).

|

----

*********
query.sql
*********

**Category-name** |_|: :guilabel:`query.sql`

.. admonition:: What |_|?

    :guilabel:`query.sql` is a resource type that represents a query written in SQL. It can be parameterized either using named parameters or positional (indexed) parameters.

* **Named parameters** |_|: Usually a named parameter appears in a sql query as a column ':' followed by its name. For instance |_|:

.. code-block:: sql

    SELECT * FROM MY_TABLE WHERE id = :value;
    with value: the name of the parameter

* **Indexed parameters** |_|: Usually indexed parameters appear in a sql query as a question mark '?'. For instance |_|:

.. code-block:: sql

    SELECT * FROM MY_TABLE WHERE id = ?;
    with '?': the indexed parameter


Since the parameters are identified by their position relative to each others, the order in which they are passed in does actually matter (they are matched by their position).

|

----

**********
result.sql
**********

**Category-name** |_|: :guilabel:`result.sql`

.. admonition:: What |_|?

    :guilabel:`result.sql` is a resource type that represents the result of a sql query (or :guilabel:`query.sql` in **TF**).

|

----

**********
script.sql
**********

**Category-name** |_|: :guilabel:`script.sql`

.. admonition:: What |_|?

    :guilabel:`script.sql` is a resource type that represents a script written in SQL. They aren't meant to read data, rather to perform massive operations in bulk like insertion or manipulation of the structure of the database.

|

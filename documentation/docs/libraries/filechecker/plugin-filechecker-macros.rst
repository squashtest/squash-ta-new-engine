..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###########################
Filechecker Plugin - Macros
###########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

.. _libraries.plugin.filechecker.macros.assert:

*************************************************************************************************
# LOAD_FFF {fixed_field_file_path} USING {fixed_field_file_descriptor_path} AS {fixed_field_file}
*************************************************************************************************

.. admonition:: What |_|?

    This macro will load the fixed field file descriptor and the fixed field file. It will then verify that the fixed field file is valid by using the fixed field file descriptor. Finally it will check that the fixed field file has the expected autonumbers. The macro will also convert the fixed field file into a resource of type fixed.fileld.file.

**Underlying instructions** |_|:

.. code-block:: html

        LOAD {fixed_field_file_descriptor_path} AS __temp{%%rand1}.file
        CONVERT __temp{%%rand1}.file TO fixed.field.file.descriptor(descriptor) AS __temp_{%%rand2}.fff.descriptor

        LOAD {fixed_field_file_path} AS __temp{%%rand3}.file
        CONVERT __temp{%%rand3}.file TO fixed.field.file(structured) USING __temp_{%%rand2}.fff.descriptor AS {fixed_field_file}

        ASSERT {fixed_field_file} IS valid
        ASSERT {fixed_field_file} HAS expected.autonumbers

**> Input** |_|:

        * ``{fixed_field_file_path}`` |_|: The path to the fixed field file (relative to the root of the repository).
        * ``{fixed_field_file_descriptor_path}`` |_|: The path to the fixed field file descriptor (relative to the root of the repository).

**> Output** |_|:

        * ``{fixed_field_file}`` |_|: The name (in the context) of the fixed field file.

**Example** |_|:

    .. list-table::

        * - :macros:`# LOAD_FFF repo/fff/data.txt USING repo/descriptor/my_descriptor.xml AS fixed_field_file.fff`

    **Fixed Field File to process** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-fff-txt.png

    **Descriptor xml file to process** (partial) |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-descriptor.png

    **The project's doctree showing the resources directory (containing the fff and descriptor xml file) to process** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-project-load.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-macro-load.png

    **Result output on success** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-load-success.png

    To get a clearer view of the functionnality offered by the LOAD_FFF macro we can create a deliberate error in the fixed field file.

    We add an **empty line** between the end of the first **leaf record** and the start of the second |_|: 

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-load-fff-error.png
    
    **Result output on failure** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-load-fail.png


|

--------------------

****************************************************************
# ASSERT_FFF {fff} HAS EXPECTED CONTENT USING {fff_queries_path}
****************************************************************

.. admonition:: What |_|?

    This macro allows to valid the content of a fixed field file using a fixed field file query file.

**Underlying instructions** |_|:

.. code-block:: html

        LOAD {fff_queries_path} AS __temp{%%rand1}.file
        CONVERT __temp{%%rand1}.file TO fixed.field.file.queries(query) AS __temp_{%%rand2}.fff.queries
        ASSERT {fff} HAS expected.content USING __temp_{%%rand2}.fff.queries

**> Input** |_|:

        * ``{fff}`` |_|: The path to the fixed field file (relative to the root of the repository).
        * ``{fff_queries_path}`` |_|: The path to the query file (relative to the root of the repository).

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT_FFF repo/fff/data.txt HAS EXPECTED CONTENT USING repo/queries/my_queries.xml`

    This example is based on the previous one. For more details, please check :ref:`here <libraries.plugin.filechecker.macros.assert>`.

    **Queries file to process** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-queries.png

    **The project's doctree showing the resources directory (containing the fff, descriptor xml file and now queries file) to process** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-project-assert.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-macro-assert.png

    **Result output on success** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-load-success.png

    To get a clearer view of the functionnality offered by the ASSERT_FFF macro we can create a deliberate error in the queries file |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-assert-queries-error.png
    
    **Result output on failure** |_|:

    .. figure:: ../../_static/plugin-filechecker/doc-filchecker-assert-fail.png

|

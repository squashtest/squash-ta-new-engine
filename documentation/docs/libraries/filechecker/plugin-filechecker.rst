..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##################
Filechecker Plugin
##################


.. toctree::
    :titlesonly:
    :maxdepth: 2

    Resources <plugin-filechecker-resources.rst>
    Macros <plugin-filechecker-macros.rst>
    Filechecker specifications for the Fixed Files <plugin-filechecker-specifications.rst>
    Advanced Users <plugin-filechecker-advanced-users.rst>

| 

This plugin provides all the elements needed to use the Filechecker tool (only for fixed field file currently) in SKF.
Fixed Field Files are files whose position and length of different fields are known.

To use Filechecker in your Squash-TF project, you should provide the following ressources |_|: 

    * The file to process and its descriptor file.
    * A query file if you want to do content test on the file to process.
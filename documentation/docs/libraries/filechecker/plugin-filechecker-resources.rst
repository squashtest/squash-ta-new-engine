..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##############################
Filechecker Plugin - Resources
##############################

.. contents:: Contents |_|:
   :local:

|

****************
fixed.field.file 
****************

**Category-name** |_|: :guilabel:`fixed.field.file`

.. admonition:: What |_|?

    :guilabel:`fixed.field.file` is a resource type that represents a fixed field file (aka FFF).

| 

--------------------

***************************
fixed.field.file.descriptor 
***************************

**Category-name** |_|: :guilabel:`fixed.field.file.descriptor`

.. admonition:: What |_|?

    :guilabel:`fixed.field.file.descriptor` is a resource type that represents the descriptor of fixed field file. This descriptor defines the structure of the fixed field file.

| 

--------------------

************************
fixed.field.file.queries
************************

**Category-name** |_|: :guilabel:`fixed.field.file.queries`

.. admonition:: What |_|?

    :guilabel:`fixed.field.file.queries` is a resource type that represents a file which contains a list of queries. Each query is an assertion against a fixed field file.



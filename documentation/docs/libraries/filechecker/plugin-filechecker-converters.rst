..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############################
Filechecker Plugin - Converters
###############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

****************************************
From file To fixed.field.file.descriptor 
****************************************

**Category-name** |_|: :guilabel:`descriptor`

.. admonition:: What |_|?

    This :guilabel:`descriptor` converter will convert a ``file`` type resource to a ``fixed.field.file.descriptor`` type resource. This converter verifies that the resource is a well formed descriptor (structure + content).

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`fixed.field.file.descriptor` (:converter-name:`descriptor`) :keywords:`AS` :resources:`{converted<Res:fixed.field.file.descriptor>}`

**> Input** |_|:

    * ``{resourceToConvert<Res:file>}`` |_|: The name of the resource to convert (``file`` type resource). This ressource should reference a fixed field file descriptor.

**> Output** |_|:

    * ``{converted<Res:fixed.field.file.descriptor>}`` |_|: The name of the converted resource (``fixed.field.file.descriptor`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/fixed_field_file_descriptor.txt :keywords:`AS` :resources:`fff_descriptor.file`

        :keywords:`CONVERT` :resources:`fff_descriptor.file` :keywords:`TO` :resource-type:`fixed.field.file.descriptor` (:converter-name:`descriptor`) :keywords:`AS` :resources:`fff_descriptor.fffd`

| 

--------------------

*****************************
From file to fixed.field.file 
*****************************

**Category-name** |_|: :guilabel:`structured`

.. admonition:: What |_|?

    This :guilabel:`structured` converter will convert a ``file`` type resource to a ``fixed.field.file`` type resource by using fixed.field.file.descriptor.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{fffToConvert<Res:file>}` :keywords:`TO` :resource-type:`fixed.field.file` (:converter-name:`structured`) :keywords:`USING` :resources:`{fffDescriptor<Res:fixed.field.file.descriptor>}` :keywords:`AS` :resources:`{converted<Res:fixed.field.file>}`

**> Input** |_|:

    * ``{fffToConvert<Res:file>}`` |_|: The name (in the context) of the resource to convert (``file`` type resource). This ressource should reference a fixed field file (e.g. by using a LOAD instruction on the fixed field file path).
    * ``{fffDescriptor<Res:fixed.field.file.descriptor>}`` |_|: The name (in the context) of the descriptor resource (``fixed.field.file.descriptor`` type resource).

**> Output** |_|:

    * ``{converted<Res:fixed.field.file>}`` |_|: The name of the converted resource (``fixed.field.file`` type reqsource). 

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD path/to/fixed_field_file_descriptor.txt :keywords:`AS` :resources:`fff_descriptor.file`

        :keywords:`CONVERT` :resources:`fff_descriptor.file` :keywords:`TO` :resource-type:`fixed.field.file.descriptor` (:converter-name:`descriptor`) :keywords:`AS` :resources:`fff_descriptor.fffd`

        :keywords:`LOAD` path/to/fixed_field_file.txt :keywords:`AS` :resources:`fixed_field_file.file`

        :keywords:`CONVERT` :resources:`fixed_field_file.file` :keywords:`TO` :resource-type:`fixed.field.file` (:converter-name:`structured`) :keywords:`USING` :resources:`fff_descriptor.fffd` :keywords:`AS` :resources:`fixed_field_file.fff`

| 

--------------------

*************************************
From file to fixed.field.file.queries
*************************************

**Category-name** |_|: :guilabel:`query`

.. admonition:: What |_|?

    This :guilabel:`query` converter will convert a ``file`` type resource to a ``fixed.field.file.queries`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{queriesToConvert<Res:file>}` :keywords:`TO` :resource-type:`fixed.field.file.queries` (:converter-name:`query`) :keywords:`AS` :resources:`{converted<Res:fixed.field.file.queries>}`

**> Input** |_|:

    * ``{{queriesToConvert<Res:file>}`` |_|: The name (in the context) of the resource to convert (``file`` type resource). This ressource should reference a fixed field file queries (e.g. by using a LOAD instruction on the fixed field file queries path). 

**> Output** |_|:

    * ``{converted<Res:fixed.field.file.queries>}`` |_|: The name of the converted resource (``fixed.field.file.queries`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/fixed_field_file_queries.txt :keywords:`AS` :resources:`fff_queries.file`

        :keywords:`CONVERT` :resources:`fff_queries.file` :keywords:`TO` :resource-type:`fixed.field.file.queries` (:converter-name:`query`) :keywords:`AS` :resources:`fff_query.fffq`

|

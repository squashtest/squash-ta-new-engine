..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#############################################################
Filechecker Plugin - Specifications for the Fixed Field Files
#############################################################

.. contents:: Contents |_|:
   :local:

|

***************
Functionalities
***************

The Filechecker plugin allows to |_|:

    * Read a FFF (Fixed Field File) in the two kinds of format |_|: binary or text.
    * Identify leafrecords / composite records of a FFF.
    * Validate fields syntax and validate the structure of a FFF.
    * Verify the value of the fields.

To accomplish the first 3 points, an XML file named 'FFF descriptor' is needed. In addition, an Xpath queries file is used to verify the value of the fields.

|

*************************
Terminology about the FFF
*************************

Leafrecords
###########

A FFF file is built of leafrecords and each leafrecord is built of fields. For instance |_|:

.. figure:: ../../_static/plugin-filechecker/leafrecord-example1.jpg

    This file is built of 2 leafrecord and each leafrecord constituted of 3 fields.

Textual FFF (a FFF of type text) have one record per line (records are separated with a word wrap character) so the records access is sequential. Within a record, the position and the number of characters for each field is known. In our example, the civility field is built of 3 characters whereas the name and the first name are built of 10 characters.

**Remarks** |_|:

    * It's the same for a binary file. All record have the same bytes number which allows distinguishing them from each other. And inside a record, the position and the number of bytes for each field is known.

    * In a FFF file, if all records are built of the same type (as our example, we speak about mono-recording file). If the file is built of several kinds of records, we speak about multi-recording file and each record has one or more identifier fields. For instance |_|:

      .. figure:: ../../_static/plugin-filechecker/leafrecord-example2.jpg
    
        In this example of a multi-recording file, the field '00' allows identifying a physical person and the field '01' allows identifying a moral person.

    * If a file is built of only leafrecords we speak about 'Flat File'.

Composite records
#################

When a suite of leafrecords form an unity, we speak about composite records. For instance |_|:

.. image:: ../../_static/plugin-filechecker/composite-records.jpg
      :alt: Composite records. 
      :align: center

In this example, each composite record is built of 3 leafrecords |_|:

    * The leafrecord '00' for the civility.
    * The leafrecord '01' for the adress.
    * The leafrecord '02' for the phone number.

Among the leafrecords composing a composite record, we can distinguish 3 kinds of leafrecords |_|:

    * The opening record which indicates the first leafrecord of a composite record.
    * The closing record which indicates the last leafrecord of a composite record. It allows to detect the end of a composite record but they're not mandatory (in the previous example, there is no closing records).
    * Others leafrecords are named children leafrecords.

**Remark** |_|: Generally the suite of leafrecords of a composite record are subject to management rules. For instance, a person must have a civil status AND a phone number.

|

**************
FFF Descriptor
**************

Structure of the FFF descriptor
###############################

A FFF descriptor is a XML file which has the following structure |_|:

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <root>
        <sequences>…</sequences>
        <records>
            </leaves>…</leaves>
            <composites>…</composites>
        </records>
    </root>

The <ROOT> tag is the root element of the FFF descriptor. It allows to |_|:

    * State the schema to use to validate the FFF descriptor.
    * Describe the general characteristics of the FFF to verify.

It contains 2 tags |_|:

    * The <sequences> tag (Optional) |_|: It contains sequences definition used for the auto-incremental fields.
    * The <records> tag |_|: It contains the records description of the file and is built with |_|:

        * A <leaves> tag which contains n <leafRecord> tags (they describe the leafrecords type of the FFF to verify).
        * A <composites> tag which contains n <compositeRecord> tags (they describe the composite records type of the FFF to verify).

<root> element
**************

The <root> tag must have the following attibutes |_|:

.. code-block:: html

    <fff:root name="LISTING" binary="true" encoding="Cp1047" bytesPerLine="62"
    xmlns = "http://www.squashtest.org/SquashTA/FFFDescriptorSchema"

    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation=
    "http://www.squashtest.org/SquashTA/FFFDescriptorSchema http://www.squashtest.org/docta/schemas/FFFDescriptorSchema_1.1.xsd">

We're going to explain the different attributes of the <root> tag |_|: 

.. code-block:: html

    xmlns= "http://www.squashtest.org/SquashTA/FFFDescriptorSchema"

The 'xmlns' attribute allows to declare the URL of the dafault namespace. It means that the XML elements used in the FFF descriptor must have been defined in this namespace.

.. code-block:: html

    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

This namespace has several attributes which allows to declare the schema to use to validate the file.

.. code-block:: html

    xsi:schemaLocation=
    "http://www.squashtest.org/SquashTA/FFFDescriptorSchema
    http://www.squashtest.org/docta/schemas/FFFDescriptorSchema_1.1.xsd"

The attribute 'schema location' of the namespace "**http://www.w3.org/2001/XMLSchema-instance**" allows to declare the XSD schema to use for the validation and to associate it with the previous namespace.

The others attributes of the <root> tag are used to describe the general characteristics of the FFF to verify |_|:

    * The ``'name'`` attribute |_|: It indicates the name of the file to use.
    * The ``'binary'`` attribute |_|: A boolean specifying if the FFF is a binary or not.
    * The ``'encoding'`` attribute |_|: It allows to specify the encoding of the file. Using names to design the encoding are those of the java class 'java.nio.charset.Charset'. For a binary file, this attribute is mandatory whereas for a text file it's optional because if not specified it's the encoding of the Java Virtual Machine who's used.
    * The ``'bytesPerLine'`` attribute |_|: It allows for a binary file to specify the amount of bytes per record.

<sequences> and <sequence> elements
***********************************

The <sequences> tag contains a list of <sequence> tags. Sequences are counters. They are used to incremente fields of 'autonumber' type.

.. code-block:: html
    
    <sequences>
        <sequence id="No" start="1" increment="1" />
    <sequences>

**With** |_|:

    * ``id`` |_|: The attribute identifying the sequence.
    * ``start`` |_|: The number from which the sequence begin.
    * ``increment`` |_|: The incrementation step of the sequence.

LeafRecord
**********

Each <leafRecord> tag decribes a leafRecord and inside each <leafRecord> tag we have  <fields> and <field> tags which describe the fields of each leafrecord.

**For instance** |_|:

.. image:: ../../_static/plugin-filechecker/leafrecord-xml.jpg
      :alt: Composite records. 
      :align: center

**<leafRecord> elements** |_|:

    * A ``'name'`` attribute |_|: It designs the record name of the leaf.
    * a ``<label>`` tag (Optional) |_|: It contains the wording of the leafRecord.
    * a ``<fields>`` tag which contains n <field> tags (One for each field of the leafRecord).

**<field> elements** |_|:

    * a ``<label>`` tag (Optional) |_|: It contains the wording of the field.
    * a ``<description>`` tag (Optional) |_|: It contains a description of the field.
    * a ``'type'`` attribute |_|: It indicates the type of the field.
    * a ``'start'`` attribute |_|: It describes the position of the first character / byte inside the record (It begins at 1).
    * a ``'length'`` attribute which describes the characters / bytes number of the field.
    * Depending of the field type, other attributes are available (See the fields types description section).

Composite Record
****************

The <compositeRecord> tag contains 3 tags |_|:

    * The ``<openingRecord>`` tag which defines the opening leafRecord. The text of this tag must be linked to the value of the attribute name of the ``<leafRecord>`` tag corresponding.
    * The ``<closingRecord>`` tag which defines the closing leafRecord. The text of this tag must be linked to the value of the attribute name of the ``<leafRecord>`` tag corresponding.
    * The ``<children>`` tag which contains the list of the children records.

**Example 1**\ |_|\ **: Composite record with closingRecord**

.. code-block:: none

    <?xml version="1.0" encoding="UTF-8"?>
    <root…>
      <records>
        <leaves>
        …
          <leafRecord name="leafRecord00">
          …
          </leafRecord>
          <leafRecord name="leafRecord99">
          …
          </leafRecord>
        …
        </leaves>
        <composites>    
          <compositeRecord name="personne">
            <label>Coordonnées d’une personne</label>
            <openingRecord>leafRecord00</openingRecord>
            <closingRecord> leafRecord99</closingRecord>
            <children>
               …
            </children>
          </compositeRecord>
        </composites>
        …

**Example 2**\ |_|\ **: Composite Record without closingRecord**

.. code-block:: none

    <?xml version="1.0" encoding="UTF-8"?>
    <root…>
      <records>
        <leaves>
          <leafRecord name="leafRecord00">
          …
          </leafRecord>
          …
        </leaves>
        <composites>     
          <compositeRecord name="personne">
            <label>Coordonnées d’une personne</label>
            <openingRecord>leafRecord00</openingRecord>
            <children>
               …
            </children>
          </compositeRecord>
        </composites>

Succession of the composite Record's children
*********************************************

The succession of the composite Record's children is defined with the help of a pattern built by combining 'and', 'or' and 'repeat'  clauses.

'AND' Clause
============

The 'and' clause is used to indicate that a A-type leafRecord **AND** a B-type leafRecord (**AND** a C-type leafRecord...) must be present. The number of records included in an 'and' clause must be higher or equal to 2.

**Example:** |_|:

.. code-block:: none

    …
    <compositeRecord name="personne">
     <label>Détail d’une personne<label>
     <openingRecord>leafRecord00<openingRecord>
     <closingRecord>leafRecord 99<closingRecord>
       <children>
         <and>
           <record>leafRecord01<record>
           <record>leafRecord02<record>
           <record>leafRecord03<record>
         <and>
       <children>
    <compositeRecord>
    …

'or' Clause
===========

The 'or' clause is used to indicate that a A-type leafRecord **OR** a B-type leafRecord (**OR** a C-type leafRecord...) must be present. The number of records included in an 'or' clause must be higher or equal to 2.

**Example** |_|:

.. code-block:: html

    …
    <compositeRecord name="personne">
     <label>Détail d’une personne<label>
     <openingRecord>leafRecord00<openingRecord>
     <closingRecord>leafRecord 99<closingRecord>
       <children>
         <or>
           <record>leafRecord01<record>
           <record>leafRecord02<record>
           <record>leafRecord03<record>
         <or>
       <children>
    <compositeRecord>
    …

'repeat' Clause
===============

The 'repeat' clause is used to indicate that leafRecord must be present a number of times, between a minimal and a maximal defined value (min>=0, min<max<unbounded).

**Example** |_|:

.. code-block:: html

    …
    <compositeRecord name="personne">
        <label>Détail d’une personne</label>
        <openingRecord>leafRecord00</openingRecord>
        <closingRecord>leafRecord99</closingRecord>
        <children>
            <repeat min="1" max="unbounded">
                <record>leafRecord01</record>
            </repeat>
        </children>
    </compositeRecord>
    …

Combination of the 'and', 'or' and 'repeat' clauses
===================================================

The 'and', 'or and 'repeat' clauses can be recursively combined.

**Example** |_|:

.. code-block:: html

    …
    <compositeRecord name="evenement">
        <label>Evenement</label>
        <openingRecord>100</openingRecord>
        <children>
            <and>
                <record>101</record>
                <record>102</record>
                <repeat min="1" max="8">
                    <and>
                        <or>
                            <record>103A</record>
                            <record>103D</record>
                            <record>103S</record>
                        </or>
                        <record>105</record>
                    </and>
                </repeat>
            </and>
        </children>
    </compositeRecord>
    …

Validation of the FFF descriptor
################################

A FFF descriptor loaded by the FileChecker must be validated. Its structure is validated by the declared XSD schema and other additionnal validations used to ensure the functionnal coherence of the file.

|

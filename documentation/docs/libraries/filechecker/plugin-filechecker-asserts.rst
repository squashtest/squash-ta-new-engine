..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

############################
Filechecker Plugin - Asserts
############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

*****************************
'file' does 'contain' {regex} 
*****************************

.. admonition:: What |_|?

    This assertion searches a pattern inside a ``file`` resource. If the pattern is not found, the assertion fails.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{resource<file>}` :keywords:`DOES` contain :keywords:`THE` $(<pattern>)

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see this page this page.

**> Input** |_|:

    * ``{resource<Res:file>}`` |_|: The name (in the context) of the file resource (``file`` type resource).
    * ``{<pattern>}`` |_|: The regular expression searched in the file.

**Note** |_|:  If you want to check for special characters used in the regular expression formalism, you will have to escape them with a backslash (" \\ ").

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` filechecker/FFF_txt_test.txt :keywords:`AS` :resources:`fixed_field_file.file`

        :keywords:`ASSERT` :resources:`fixed_field_file.file` :keywords:`DOES` :converter-name:`contain` :keywords:`THE` $(Hello)

| 

--------------------

*********************************
'file' does 'not.contain' {regex} 
*********************************

.. admonition:: What |_|?

    This assertion verifies that a pattern is not present inside a ``file`` resource. If the pattern is found, the assertion fails.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{resource<Res:file>}` :keywords:`DOES` :converter-name:`not.contain` :keywords:`THE` $(<pattern>)

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{resource<Res:file>}`` |_|: The name (in the context) of the file resource (``file`` type resource).
    * ``{<pattern>}`` |_|: The regular expression searched in the file.

**Note** |_|:  If you want to check for special characters used in the regular expression formalism, you will have to escape them with a backslash (" \\ ").

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` filechecker/FFF_txt_test.txt :keywords:`AS` :resources:`fixed_field_file.file`

        :keywords:`ASSERT` :resources:`fixed_field_file.file` :keywords:`DOES` :converter-name:`not.contain` :keywords:`THE` $(Hello)

| 

--------------------

*****************************
'fixed.field.file' is 'valid'
*****************************

.. admonition:: What |_|?

    This assertion verifies that a ``fixed.field.file`` resource is valid (structure and syntax).

.. list-table::

    * - :keywords:`ASSERT` :resources:`{fffResource<Res:fixed.field.file>}` :keywords:`IS` :converter-name:`valid`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{fffResource<Res:fixed.field.file>}`` |_|: The name (in the context) of the fixed field file resource to validate (``fixed.field.file`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` filechecker/descriptor_txt_test.xml :keywords:`AS` :resources:`fixed_field_file_descriptor.file`

        :keywords:`CONVERT` :resources:`fixed_field_file_descriptor.file` :keywords:`TO` :resource-type:`fixed.field.file.descriptor` (:converter-name:`descriptor`) :keywords:`AS` :resources:`fixed_field_file_descriptor.descriptor`

        :keywords:`LOAD` filechecker/FFF_txt_test.txt :keywords:`AS` :resources:`fixed_field_file.file`

        :keywords:`CONVERT` :resources:`fixed_field_file.file` :keywords:`TO` :resource-type:`fixed.field.file` (:converter-name:`structured`) :keywords:`USING` :resources:`fixed_field_file_descriptor.descriptor` :keywords:`AS` :resources:`fixed_field_file.fff`

        :keywords:`ASSERT` :resources:`fixed_field_file.fff` :keywords:`IS` :converter-name:`valid`

| 

--------------------

*********************************************
'fixed.field.file' has 'expected.autonumbers'
*********************************************

.. admonition:: What |_|?

    This assertion verifies that a ``fixed.field.file`` resource has the expected auto numbers.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{fffResource<Res:fixed.field.file>}` :keywords:`HAS` :converter-name:`expected.autonumbers`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{fffResource<Res:fixed.field.file>}`` |_|: The name of the fixed field file resource to verify (``fixed.field.file`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` filechecker/descriptor_txt_test.xml :keywords:`AS` :resources:`fixed_field_file_descriptor.file`
    
        :keywords:`CONVERT` :resources:`fixed_field_file_descriptor.file` :keywords:`TO` :resource-type:`fixed.field.file.descriptor` (:converter-name:`descriptor`) :keywords:`AS` :resources:`fixed_field_file_descriptor.descriptor`

        :keywords:`LOAD` filechecker/FFF_txt_test.txt :keywords:`AS` fixed_field_file.file

        :keywords:`CONVERT` :resources:`fixed_field_file.file` :keywords:`TO` :resource-type:`fixed.field.file` (:converter-name:`structured`) :keywords:`USING` :resources:`fixed_field_file_descriptor.descriptor` :keywords:`AS` :resources:`fixed_field_file.fff`

        :keywords:`ASSERT` :resources:`fixed_field_file.fff` :keywords:`HAS` :converter-name:`expected.autonumbers`

| 

--------------------

*****************************************
'fixed.field.file' has 'expected.content'
*****************************************

.. admonition:: What |_|?

    This assertion verifies a ``fixed.field.file`` resource has the expected content. The expected content is defined in the fixed field file queries resource provided in the :keywords:`USING` clause.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{fffResource<Res:fixed.field.file>}` :keywords:`HAS` :converter-name:`expected.content` :keywords:`USING` :resources:`{fffQueries<Res:fixed.field.file.queries>}`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{fffResource<Res:fixed.field.file>}`` |_|: The name (in the context) of the fixed field file resource to verify (``fixed.field.file`` type resource).
    * ``{fffQueries<Res:fixed.field.file.queries>}`` |_|: The name (in the context) of the fixed field file queries which contains the expected contents (``fixed.field.file.queries`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` filechecker/descriptor_txt_test.xml :keywords:`AS` :resources:`fixed_field_file_descriptor.file`

        :keywords:`CONVERT` :resources:`fixed_field_file_descriptor.file` :keywords:`TO` :resource-type:`fixed.field.file.descriptor` (:converter-name:`descriptor`) :keywords:`AS` :resources:`fixed_field_file_descriptor.descriptor`

        :keywords:`LOAD` filechecker/FFF_txt_test.txt :keywords:`AS` :resources:`fixed_field_file.file`

        :keywords:`CONVERT` :resources:`fixed_field_file.file` :keywords:`TO` :resource-type:`fixed.field.file` (:converter-name:`structured`) :keywords:`USING` :resources:`fixed_field_file_descriptor.descriptor` :keywords:`AS` :resources:`fixed_field_file.fff`

        :keywords:`LOAD` filechecker/FFF_queries_txt_test.xml :keywords:`AS` :resources:`fixed_field_file_queries.file`

        :keywords:`CONVERT` :resources:`fixed_field_file_queries.file` :keywords:`TO` :resource-type:`fixed.field.file.queries` (:converter-name:`query`) :keywords:`AS` :resources:`fixed_field_file_queries.query`

        :keywords:`ASSERT` :resources:`fixed_field_file.fff` :keywords:`HAS` :converter-name:`expected.content` :keywords:`USING` :resources:`fixed_field_file_queries.query`

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################
SAHI Plugin - Asserts
#####################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

**************************
'result.sahi' is 'success' 
**************************

.. admonition:: What |_|?

    This assertion verifies if a sahi execution succeed. If the assertion is verified the test continues else the test failed.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{sahiResult<Res:result.sahi>}` :keywords:`IS` :converter-name:`success`

        :keywords:`VERIFY` :resources:`{sahiResult<Res:result.sahi>}` :keywords:`IS` :converter-name:`success`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{sahiResult<Res:result.sahi>}`` |_|: The name of the resource which contains the result of a sahi execution command (``result.sahi`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` sahi-scripts/test.sah :keywords:`AS` :resources:`sahi-script.file`

        :keywords:`CONVERT` :resources:`sahi-script.file` :keywords:`TO` :resource-type:`script.sahi` (:converter-name:`script`) :keywords:`AS` :resources:`test.script`

        :keywords:`LOAD` configuration/sahi-conf.properties :keywords:`AS` :resources:`conf`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`test.script` :keywords:`ON` :resources:`Connexion-gui` :keywords:`USING` :resources:`conf` :keywords:`AS` :resources:`result`

        :keywords:`ASSERT` :resources:`result` :keywords:`IS` :converter-name:`success`

|

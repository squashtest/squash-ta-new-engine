..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################
SAHI Plugin - Command
#####################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

*********************************
'execute' 'script.sahi' on 'http'
*********************************

.. admonition:: What |_|?

    This command will execute a sahi script againt an http SUT using a configuration file. 

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{sahiScript<Res:script.sahi>}` :keywords:`ON` :resources:`{server<Tar:http>}` :keywords:`USING` :resources:`{sahiConf<Res:file>}`, [:resources:`{mainPath<Res:file>}`] :keywords:`AS` :resources:`{sahiResult<Res:result.sahi>}`

**> Input** |_|:

    * ``{sahiScript<Res:script.sahi>}`` |_|: The name (in the context) which references either a sahi script or a bundle containing a test or a test suite to execute (``script.sahi`` type resource)
    * ``{sahiConf<Res:file>}`` |_|: The name of the sahi configuration file (``file`` type resource). The instruction supports to receive directly a ``file`` type resource instead of a converted resource in 'properties'. It's mandatory and can be defined via an inline instruction. The referenced file contains a list of key |_| / |_| value separated with the character '=' and one property per line. Possible keys are |_|:

        * browserType (mandatory) |_|: name of the browser. It should reference the "name" of a browserType define in the file browser_types.xml of the sahi proxy. You can found this file in SAHI_PROXY_HOME/userdata/config (The proxy should have been launched at least one time in order to the file exist). It's also possible to retrieve the content of the file through a web browser by using the url |_|: http://SAHI_HOST:SAHI_PORT/_s_/dyn/ConfigureUI_readFile?fileName=config/browser_types.xml
        * sahi.proxy.host (optional) |_|: Name of the machine where is the sahi proxy. Default value is localhost.
        * sahi.proxy.port (optional) |_|: Port used by the sahi proxy. Default value is 9999.
        * sahi.thread.nb (optional) |_|: Number of browser instance to launch in parallel. Default value is 1.
        * report.format (optional) |_|: Report type. Default value is html. The other possible value is junit.
        * timeout (since **version 1.7.0** - optional) |_|: The time, in milliseconds, |squashTF| should wait before giving up the execution. Default value is set to 60 seconds (was 30s before **version 1.7.2**).

    * ``{mainPath<Res:file>}`` (optional) |_|: This ``file`` type resource is necessary to the instruction when the ``script.sahi`` type resource is a bundle. It can also be defined via an inline instruction. It contains only one key |_| / |_| value separated with the character ':' and with 'mainpath' as the key. It corresponds to the path, relatively to the bundle root to the sahi file defining the sahi test suite.
    * ``{server<Tar:http>}`` |_|: The name in (the context) of the target corresponding to the SUT (``http`` type target).

**> Output** |_|:

    * ``{sahiResult<Res:result.sahi>}`` |_|: The name of the resource which contains the result of the sahi command execution (``sahi.result`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` sahi-scripts/test.sah :keywords:`AS` :resources:`sahi-script.file`

        :keywords:`CONVERT` :resources:`sahi-script.file` :keywords:`TO` :resource-type:`script.sahi` (:converter-name:`script`) :keywords:`AS` :resources:`test.script`

        :keywords:`LOAD` configuration/sahi-conf.properties :keywords:`AS` :resources:`conf`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`test.script` :keywords:`ON` :resources:`Connexion-gui` :keywords:`USING` :resources:`conf` :keywords:`AS` :resources:`result`

**Remark** |_|: In the case where the ``script.sahi`` type resource is a bundle, the instruction need the configuration key 'mainpath'. It can be obtained via the :keywords:`USING` clause or via a ``script.sahi`` type resource. This configuration key is optional in both instructions but must be defined in one of them. If its defined in both, so the value indicated in the command instruction prime.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
SAHI Plugin - Converters
########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

************************
From file to script.sahi
************************

**Category-name** |_|: :guilabel:`script`

.. admonition:: What |_|?

    This :guilabel:`script` converter will convert a ``file`` type resource to a ``script.sahi`` type resource.


**Two cases** |_|:

Standalone Sahi script 
######################

In the case of a standalone script, it doesn't depend on other files (e.g. other sahi script to be included or files to be downloaded). The initial resource references the sahi script. You could use the syntax below |_|:

.. list-table::

    * - :keywords:`CONVERT` :resources:`{scriptToConvert<Res:file>}` :keywords:`TO` :resource-type:`script.sahi` (:converter-name:`script`) :keywords:`AS` :resources:`{converted<Res:script.sahi>}`

**> Input** |_|:

    * ``{scriptToConvert<Res:file>}`` |_|: The name of the resource to convert (``file`` type resource). This ressource references a sahi script (e.g. by using a :keywords:`LOAD` instruction on the sahi script path). 

**> Output** |_|:

    * ``{converted<Res:scipt.sahi>}`` |_|: The name of the converted resource (``script.sahi`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/sahi_script.sah :keywords:`AS` :resources:`sahi_script.file`

        :keywords:`CONVERT` :resources:`sahi_script.file` :keywords:`TO` :resource-type:`script.sahi` (:converter-name:`script`) :keywords:`AS` :resources:`sahi_script.sahi`

| 

--------------------

Sahi script with dependencies 
#############################

In the case of a script which depends on other file, you have to use the syntax below. The initial resource references a directory. This directory should contains in its tree the whole resources needed by the main sahi script. You also have to define where is the main sahi script to execute (mainScriptPath of the :keywords:`USING` clause). This path should be RELATIVE to the directory referenced by the initial resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{rootDirectory<Res:file>}` :keywords:`TO` script.sahi (:converter-name:`script`) :keywords:`AS` :resources:`{converted<Res:script.sahi>}` :keywords:`USING` :resources:`{mainScriptPath<Res:file>}`

**> Input** |_|:

    * ``{rootDirectory<Res:file>}`` |_|: Name of the resource which references the root directory. This root directory should contain the whole files needed to execute the sahi script.
    * ``{mainScriptPath<Res:file>}`` |_|: Name of the configuration resource. The content of the file should be |_|: 'mainpath:relativePathToMainSahiScript' (Note |_|: you could use an inline definition). This path to main sahi script should be relative to the directory given as rootDirectory. 

**> Output** |_|:

    * ``{converted<Res:scipt.sahi>}`` |_|: The name of the converted resource (``script.sahi`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/rootDirectory :keywords:`AS` :resources:`root_directory.file`

        :keywords:`CONVERT` :resources:`root_directory.file` :keywords:`TO` :resource-type:`script.sahi` (:converter-name:`script`) :keywords:`AS` :resources:`sahi_script.sahi` :keywords:`USING` $(mainpath:relative/path/to/main/sahi_script.sah) 

|

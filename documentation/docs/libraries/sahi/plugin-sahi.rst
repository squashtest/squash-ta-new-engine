..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###########
SAHI Plugin
###########

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Resources <plugin-sahi-resources.rst>
    Macros <plugin-sahi-macros.rst>
    Advanced users <plugin-sahi-advanced-users.rst>

|

The Sahi plugin is part of the base package shipped with SKF. It is automatically installed if you choose the default project configuration for your test project. However, as it is packaged as a seperate plugin, you can exclude it from the test project (and avoid downloading and installing its dependencies).

This plugin provides all the elements needed to execute a sahi script in SKF.


**Overview** |_|:

To use sahi script in your |squashTF| project, you have to |_|: 

    * Create the sahi script and integrate it (and all its dependencies) in the resources directory of your |squashTF| project. If you have a sahi script with dependencies you will have to create a file to define the path to your script. More details below.
    * Put a *sahi_config.properties* file in this resources directory (the name of this file has no importance). This file should contain the definition of the browser you want to use to execute the script.

    .. admonition:: Example of sahi_config.properties file content |_|:

        // For firefox your file should contain |_|:

        browserType=firefox

        // For Internet explorer your file should contain |_|:

        browserType=ie
    
    * Define the http Target which represents your SUT.

|

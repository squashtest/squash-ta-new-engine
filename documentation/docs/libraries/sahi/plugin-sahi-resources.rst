..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#######################
SAHI Plugin - Resources
#######################

.. contents:: Contents |_|:
   :local:

|

***********
script.sahi 
***********

**Category-name** |_|: :guilabel:`script.sahi`

.. admonition:: What |_|?

    :guilabel:`script.sahi` is a resource type that represents a script for Sahi robot which is designed to test Web GUI. This resource can be used for standalone sahi scripts but also for sahi scripts with dependencies (e.g. on other sahi script or file to download).

| 

--------------------

***********
result.sahi 
***********

**Category-name** |_|: :guilabel:`result.sahi`

.. admonition:: What |_|?

     :guilabel:`result.sahi` is a resource type that represents the status of a Sahi script. Sahi scripts usually embed their own assertions and their results are fed back to SKF using this resource. It allows further processing once the script is over while holding the results in memory.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

####################
SAHI Plugin - Macros
####################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

************************************************
# EXECUTE_SAHI {script} ON {server} USING {conf}
************************************************

.. admonition:: What |_|?

    This macro will load a sahi script and a sahi configuration file. Then it will execute the script against the server using the configuration. Finally it will test if the result of the operation is a success. Note that the given sahi script must be standalone |_|: it must not depend on any other files, e.g. script includes or files to be uploaded. When this is the case, you must use the following shortcut instead.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {script} AS __temp{%%rand1}.file
    CONVERT __temp{%%rand1}.file TO file(param.relativedate) AS __temp{%%rand2}.file
    CONVERT __temp{%%rand2}.file TO script.sahi(script) AS __temp{%%rand3}.sahi

    LOAD {conf} AS __temp{%%rand4}.file

    EXECUTE execute WITH __temp{%%rand3}.sahi ON {server} USING __temp{%%rand4}.file AS __temp{%%rand5}.result
    ASSERT __temp{%%rand5}.result IS success

**> Input** |_|:

        * ``{script}`` |_|: The path to the sahi script to execute relative to the root of the repository (standalone script).
        * ``{server}`` |_|: The name (in the context) of the target which corresponding to the SUT (``http`` type target).
        * ``{conf}`` |_|: The path to the sahi configuration file relative to the root of the repository.

**Example** |_|:

.. list-table::

    * - :macros:`#EXECUTE_SAHI gui-test/sahi/main/test1.sah ON SUT_website USING gui-test/sahi/conf/basic-conf.properties`

| 

--------------------

*****************************************************************************************
# EXECUTE_SAHI_BUNDLE {bundlepath} WITH MAIN SCRIPT {scriptpath} ON {server} USING {conf}
*****************************************************************************************

.. admonition:: What |_|?

    This macro executes a sahi script using a sahi configuration file and against a server, just like above. The difference is the script is now allowed to have dependencies (dependencies should be provided).

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {bundlepath} AS __temp_{%%r1}.file
    CONVERT __temp_{%%r1}.file TO file(param.relativedate) AS __temp_{%%r2}.file
    CONVERT __temp_{%%r2}.file TO script.sahi AS __temp_{%%r3}.bundle USING $(mainpath : {scriptpath})

    LOAD {conf} AS __temp_{%%r4}.file
    CONVERT __temp_{%%r4}.file to properties(structured) AS __temp_{%%r5}.properties

    EXECUTE execute WITH __temp_{%%r3}.bundle ON {server} USING __temp_{%%r5}.properties AS __temp_{%%r6}.result
    ASSERT __temp_{%%r6}.result IS success

**> Input** |_|:

        * ``{bundlepath}`` |_|: The path to the root directory of the bundle relative to the root of the repository (see bundle resource in commons component reference documentation). This root directory should contain in the script and all its dependencies.
        * ``{scriptpath}`` |_|: The path to the main script in the bundle (RELATIVE to the bundle path).
        * ``{server}`` |_|: The name (in the context) of the target corresponding to the SUT (``http`` type target).
        * ``{conf}`` |_|: The path to the sahi configuration file relative to the root of the repository. 

**Example** |_|:

.. list-table::

    * - :macros:`#EXECUTE_SAHI_BUNDLE gui-test/sahi WITH MAIN SCRIPT main/test1.sah ON SUT_website USING gui-test/sahi/conf/basic-conf.properties`

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

########################
Selenium Plugin - Macros
########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

****************************************************************
# EXECUTE_SELENIUM2 {bundlePath} WITH MAIN CLASS {mainClassName}
****************************************************************

.. admonition:: What |_|?

    This macro will compile the selenium 2 test suite contained in the specified bundle and execute the specified test suite (main class) from the bundle.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {bundlePath} AS __temp_{%%rand1}.file
    CONVERT __temp_{%%rand1}.file TO script.java(compile) AS __temp_{%%rand2}.compiled
    CONVERT __temp_{%%rand2}.compiled TO script.java.selenium2(script) USING $({mainClassName}) AS __temp_{%%rand3}.selenium
    EXECUTE execute WITH __temp_{%%rand3}.selenium AS __temp_{%%rand4}.result
    ASSERT __temp_{%%rand4}.result IS success

**> Input** |_|:

        * ``{bundlePath}`` |_|: The path to the selenium bundle to execute relative to the resources repository. You have to point to the folder containing the java folder.
        * ``{mainClassName}`` |_|: The qualified name of the main class.

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE_SELENIUM2 path/to/selenium2 WITH MAIN CLASS com.example.selenium.TestSuite`



    **Selenium bundle location** |_|:

    .. figure:: ../../_static/plugin-selenium/execute-selenium2-macro-selenium-bundle-location.png

    **SKF script** |_|:

    .. figure:: ../../_static/plugin-selenium/execute-selenium2-macro.png

    **Main class** |_|:

    .. code-block:: java

        package seleniumExample;

        import org.junit.After;
        import org.junit.Before;
        import org.junit.Test;
        import org.junit.Assert;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.chrome.ChromeDriver;
        import org.openqa.selenium.firefox.FirefoxDriver;
        import org.openqa.selenium.support.PageFactory;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;

        import java.util.concurrent.TimeUnit;

        public class SeleniumExample {

            WebDriver driver;
            WebDriverWait wait;

            @Before
            public void setUp(){
                try{
                    driver = new FirefoxDriver();
                    driver.get("https://www.bbc.com/news");

                }
                catch(Exception e){
                    System.err.println(e.getMessage());
                }
                driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
                wait = new WebDriverWait(driver,30);
            }

            @Test
            public void randomTest() throws InterruptedException {
                driver.findElement(By.xpath("//input[@id='orb-search-q']")).sendKeys("selenium");
                        String value = driver.findElement(By.xpath("//button[@id='orb-search-button']")).getText();
                Assert.assertTrue("The search button doesn't exist", value.equals("Search the BBC"));
            }

            @After
            public void tearDown(){
                //driver.quit();
            }
        }
    
|

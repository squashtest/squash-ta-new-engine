..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _selenium.converters.anchor:

############################
Selenium Plugin - Converters
############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

.. _selenium.from.script.java.to.script.java.selenium2.anchor:

*****************************************
From script.java to script.java.selenium2
*****************************************

**Category-name** |_|: :guilabel:`script`

.. admonition:: What |_|?

    This :guilabel:`script` converter create a selenium test resource (``script.java.selenium2`` type resource) which can be uses by the :ref:`'execute' 'script.java.selenium2'<selenium.commands.execute.script.java.selenium2.anchor>` command from a ``script.java`` resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:script.java>}` :keywords:`TO` :resource-type:`script.java.selenium2` (:converter-name:`script`) :keywords:`USING` :resources:`{conf<Res:file>}` :keywords:`AS` :resources:`{converted<Res:script.java.selenium2>}`

**> Input** |_|:

    * ``{resourceToConvert<Res:script.java>}`` |_|: The name (in the context) of the resource which references a java bundle which contains the source code, the compiled code and the resources of a selenium2 test suite.
    * ``{conf<Res:file>}`` (optional) |_|: The name of the resource which references a configuration file which can contain only one key |_|:

        * mainClass |_|: The qualified name of the main java class. The configuration must be supplied as a text file with one line containing the qualified name. If you give more, the last read line becomes the main class name. This parameter may be used if you have only one selenium test suite. On the other hand, if your selenium test bundle contains several test suite sharing helper code resources and dependencies, you may omit the main class name and rather give that parameter in the configuration of your various execute commands. It can be defined via an inline instruction.
        

**> Output** |_|:

    * ``{converted<Res:script.java.selenium2>}`` |_|: The name of the converted resource (``script.java.selenium2`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium :keywords:`AS` :resources:`seleniumTestSource`
    
        :keywords:`CONVERT` :resources:`seleniumTestSource` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`seleniumTestCompiled`
    
        :keywords:`CONVERT` :resources:`seleniumTestCompiled` :keywords:`TO` :resource-type:`script.java.selenium2` (:converter-name:`script`) :keywords:`USING` $(org.squashtest.Selenium2JUnit4WithResourceDependency) :keywords:`AS` :resources:`seleniumTest`

|

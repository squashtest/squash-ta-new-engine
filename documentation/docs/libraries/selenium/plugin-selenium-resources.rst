..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###########################
Selenium Plugin - Resources
###########################

.. contents:: Contents |_|:
   :local:

|

*********************
script.java.selenium2   
*********************

**Category-name** |_|: :guilabel:`script.java.selenium2`

.. admonition:: What |_|?

    :guilabel:`script.java.selenium2` is a resource type that is a Selenium 2 test written in the java language. This is basically a bundle containing the test code compiled from its sources and the associated resources. It may be used by :ref:`'execute' 'script.java.selenium2'<selenium.commands.execute.script.java.selenium2.anchor>` command.

**Here are the supported parameters** |_|:

    * mainpath |_|: as in the bundle resource, it defines the path to the main file from the base of the bundle (the base directory of your selenium tests). For more details, see the :ref:`Converters section<selenium.converters.anchor>`.

| 

--------------------

.. _selenium.resource.result.selenium.anchor:

***************
result.selenium   
***************

**Category-name** |_|: :guilabel:`result.selenium`

.. admonition:: What |_|?

    :guilabel:`result.selenium` is a resource type that holds the result of a Selenium test execution. It defines a Selenium execution status (success |_| / |_| failure), and in case of failure an attached failure report (an HTML file that follows the surefire format). This resource may be used by a specialized success assertion. For more details, see the :ref:`Assert section<selenium.assert.anchor>`.

|

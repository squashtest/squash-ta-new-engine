..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _selenium.assert.anchor:

########################
Selenium Plugin - Assert
########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

******************************
'result.selenium' is 'success' 
******************************

.. admonition:: What |_|?

    This assertion checks that the selenium test suite execution was successful. If not, it gives the list of failed selenium tests in the failure message, and attaches the execution report in the surefire format produced by the selenium test suite execution as failure context resource.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{seleniumResult<Res:result.selenium>}` :keywords:`IS` :converter-name:`success`

        :keywords:`VERIFY` :resources:`{seleniumResult<Res:result.selenium>}` :keywords:`IS` :converter-name:`success`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{seleniumResult<Res:result.selenium>}`` |_|: The name of the resource (In the context) which contains the result of a selenium execution command (``result.selenium`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium :keywords:`AS` seleniumTestSource

        :keywords:`CONVERT` :resources:`seleniumTestSource` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`seleniumTestCompiled`
        
        :keywords:`CONVERT` :resources:`seleniumTestCompiled` :keywords:`TO` :resource-type:`script.java.selenium2` (:converter-name:`script`) :keywords:`USING` $(org.squashtest.Selenium2JUnit3WithMvnDependency) :keywords:`AS` :resources:`seleniumTest`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`seleniumTest` :keywords:`AS` :resources:`seleniumResult`
    
        :keywords:`ASSERT` :resources:`seleniumResult` :keywords:`IS` :converter-name:`success`

|

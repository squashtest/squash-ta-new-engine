..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##########################
Selenium Plugin - Commands
##########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

.. _selenium.commands.execute.script.java.selenium2.anchor:

*********************************
'execute' 'script.java.selenium2'
*********************************

.. admonition:: What |_|?

    This command executes selenium 2 tests referenced as script.java.selenium2 resources.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{selenium2Tests<Res:script.java.selenium2>}` :keywords:`USING` :resources:`{conf<Res:file>}` :keywords:`AS` :resources:`{result<Res:result.selenium>}`

**> Input** |_|:

    * ``{selenium2Tests<Res:script.java.selenium2>}`` |_|: The name (in the context) of the resource which references the java bundle containing the tests to execute (``script.java.selenium2`` type resource).
    * ``{conf<Res:file>}`` (optional) |_|: The name of the resource which references a configuration file which can contain only one key |_|: 
        
        * mainclass |_|: The qualified name of the main java class. The configuration must be supplied as a text file with one line containing the qualified name. If you give more, the last read line becomes the main class name. It can be defined via an inline instruction. The format is <key>:<value>.

**Note** |_|: The command needs this key. The command can retrieve it via the :keywords:`USING` clause of the instruction or via the ``script.java.selenium2`` type resource (See the :ref:`From script.java to script.java.selenium2<selenium.from.script.java.to.script.java.selenium2.anchor>` converter).

This configuration key is optional in each instruction (command and conversion) but it must be defined in at least one instruction. If the key is defined in the convert instruction and in the command instruction, the key in the command instruction prime.

**> Output** |_|:

    * ``{result<Res:result.selenium>}`` |_|: The result of the test execution (``result.selenium`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium :keywords:`AS` :resources:`seleniumTestSource`

        :keywords:`CONVERT` :resources:`seleniumTestSource` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`seleniumTestCompiled`

        :keywords:`CONVERT` :resources:`seleniumTestCompiled` :keywords:`TO` :resource-type:`script.java.selenium2` (:converter-name:`script`) :keywords:`USING` $(org.squashtest.Selenium2JUnit4) :keywords:`AS` :resources:`seleniumTest`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`seleniumTest` :keywords:`AS` :resources:`seleniumResult`

|

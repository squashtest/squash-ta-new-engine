..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############
Selenium Plugin
###############

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Introduction <plugin-selenium-introduction.rst>
    Resources <plugin-selenium-resources.rst>
    Macros <plugin-selenium-macros.rst>
    Advanced Users <plugin-selenium-advanced-users.rst>

The Selenium plugin is part of the base package shipped with SKF. It is automatically installed if you choose the default project configuration for your test project. However, as it is packaged as a separate plugin, you can exclude it from the test project (and avoid downloading and installing its dependencies).

This plugin provides all the elements needed to execute selenium 2 and 3 tests in SKF.

If you need to execute selenium 1 tests, please check the `Selenium Plugin Legacy <../selenium-legacy/plugin-selenium-legacy.html>`_.

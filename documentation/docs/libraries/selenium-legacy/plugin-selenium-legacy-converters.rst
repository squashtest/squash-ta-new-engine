..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _selenium.legacy.converters.anchor:

###################################
Selenium Plugin Legacy - Converters
###################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

****************
From file |_|...
****************

|

... |_| to configuration.selenium
#################################

**Category-name** |_|: :guilabel:`configuration.selenium`

.. admonition:: What |_|?

    This converter creates a ``configuration.selenium`` resource which can be used to launch a Selenium Server for selenium RC tests.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`configuration.selenium` (:converter-name:`configuration.selenium`) :keywords:`AS` :resources:`{converted<Res:configuration.selenium>}`

**> Input** |_|:

    * ``{resourceToConvert<Res:file>}`` |_|: The name (in the context) of the resource which references a configuration file for a Selenium sever ('.properties').

**> Output** |_|:

    * ``{converted<Res:configuration.selenium>}`` |_|: The name of the converted resource (``configuration.selenium`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium/selenium-conf.properties :keywords:`AS` :resources:`selenium-conf.file`

        :keywords:`CONVERT` :resources:`selenium-conf.file` :keywords:`TO` :resource-type:`configuration.selenium` :keywords:`AS` :resources:`selenium-conf`

|

----------

.. _selenium.legacy.converters.from.file.to.script.html.anchor:

... |_| to script.html.selenium 
###############################

**Category-name** |_|: :guilabel:`script`

.. admonition:: What |_|?

    This :guilabel:`script` converter will convert a ``file`` type resource to a ``script.html.selenium`` type resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`script.html.selenium` (:converter-name:`script`) :keywords:`USING` :resources:`{conf<Res:file>}` :keywords:`AS` :resources:`{converted<Res:script.html.selenium>}`

**> Input** |_|:

    * ``{resourceToConvert<Res:file>}`` |_|: The name (in the context) of the resource which references the root directory containing the sources needed for a selenium test suite at the format "selenese".
    * ``{conf<Res:file>}`` |_|: The name of the resource which references a configuration file which can contain two keys |_|:

        * mainpath |_|: The path (relative to the root directory of the bundle) to the html file containing the test suite.
        * browser |_|: The browser to use (to retrieve the list of possible values see |_|: http://stackoverflow.com/questions/1317055/how-to-run-google-chrome-with-selenium-rc)

    It can be defined via an inline instruction.

**> Output** |_|:

    * ``{converted<Res:script.html.selenium>}`` |_|: The name of the converted resource (``script.html.selenium`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` path/to/rootDirectory :keywords:`AS` :resources:`directory`

        :keywords:`CONVERT` :resources:`directory` :keywords:`TO` :resource-type:`script.html.selenium` (:converter-name:`script`) :keywords:`USING` (mainpath:relative/path/to/suite.html) :keywords:`AS` :resources:`selenese.bundle`


|

--------------------

.. _selenium.legacy.converters.from.script.java.to.script.java.selenium1.anchor:

*****************************************
From script.java to script.java.selenium1
*****************************************

**Category-name** |_|: :guilabel:`script`

.. admonition:: What?

    This :guilabel:`script` converter create a selenium test resource (``script.java.selenium1`` type resource) which can be used by the :ref:`'execute' 'script.java.selenium1'<selenium.legacy.commands.execute.script.java.selenium1.anchor>` command from a ``script.java`` resource.

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res:script.java>}` :keywords:`TO` :resource-type:`script.java.selenium1`(:converter-name:`script`) :keywords:`USING` :resources:`{conf<Res:file>}` :keywords:`AS` :resources:`{converted<Res:script.java.selenium1>}`

**> Input** |_|:

    * ``{resourceToConvert<Res:script.java>}`` |_|: The name (in the context) of the resource which references a java bundle which contains the source code, the compiled code and the resources of a selenium1 test suite.
    * ``{conf<Res:file>}`` (optional) |_|: The name of the resource which references a configuration file which can contain only one key |_|:

        * mainClass |_|: The qualified name of the main java class. The configuration must be supplied as a text file with one line containing the qualified name. If you give more, the last read line becomes the main class name. This parameter may be used if you have only one selenium test suite. On the other hand, if your selenium test bundle contains several test suite sharing helper code resources and dependencies, you may omit the main class name and rather give that parameter in the configuration of your various execute commands. It can be defined via an inline instruction.
        
**> Output** |_|:

    * ``{converted<Res:script.java.selenium1>}`` |_|: The name of the converted resource (``script.java.selenium1`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` :resources:`selenium` :keywords:`AS` :resources:`seleniumTestSource`
        
        :keywords:`CONVERT` :resources:`seleniumTestSource` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`seleniumTestCompiled`

        :keywords:`CONVERT` :resources:`seleniumTestCompiled` :keywords:`TO` :resource-type:`script.java.selenium1` (:converter-name:`script`) :keywords:`USING` $(org.squashtest.Selenium1JUnit3) :keywords:`AS` :resources:`seleniumTest`

|

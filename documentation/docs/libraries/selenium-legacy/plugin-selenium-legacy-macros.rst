..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############################
Selenium Plugin Legacy - Macros
###############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

****************************************************************
# EXECUTE_SELENIUM1 {bundlePath} WITH MAIN CLASS {mainClassName}
****************************************************************

.. admonition:: What |_|?

    This macro will compile the selenium 1 test suite contained in the specified bundle and execute the specified test suite (main class) from the bundle

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {bundlePath} AS __temp_{%%rand1}.file
    CONVERT __temp_{%%rand1}.file TO script.java(compile) AS __temp_{%%rand2}.compiled
    CONVERT __temp_{%%rand2}.compiled TO script.java.selenium1(script) USING $({mainClassName}) AS __temp_{%%rand3}.selenium
    EXECUTE execute WITH __temp_{%%rand3}.selenium AS __temp_{%%rand4}.result
    ASSERT __temp_{%%rand4}.result IS success

**> Input** |_|:

        * ``{bundlePath}`` |_|: The path to the selenium1 bundle to execute relative to the root of the repository.
        * ``{mainClassName}`` |_|: The qualified name of the main class.

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE_SELENIUM1 path/to/selenium1 WITH MAIN CLASS com.example.selenium.TestSuite`

|

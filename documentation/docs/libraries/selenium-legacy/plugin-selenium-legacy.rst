..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################
Selenium Plugin Legacy
######################

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Introduction <plugin-selenium-legacy-introduction.rst>
    Resources <plugin-selenium-legacy-resources.rst>
    Macros <plugin-selenium-legacy-macros.rst>
    Advanced Users <plugin-selenium-legacy-advanced-users.rst>

|

The main Selenium Plugin handles Selenium 2 and 3 but not Selenium 1 anymore. If you need the integration of Selenium 1 tests into SKF, you can use 
Selenium Plugin Legacy which is an add-on of the main Selenium Plugin allowing the integration of thoses tests.

This plugin excludes the Selenium 3 dependency from the main Selenium Plugin and uses a Selenium 2 dependency which handles Selenium 1 and 2.

This plugin provides all the elements needed to execute selenium 1 tests in SKF.

.. caution::

 If you use the legacy plugin, you won't be able to execute Selenium 1 and Selenium 3 in the same project.

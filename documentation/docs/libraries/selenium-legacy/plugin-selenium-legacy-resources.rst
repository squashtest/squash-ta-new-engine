..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##################################
Selenium Plugin Legacy - Resources
##################################

.. contents:: Contents |_|:
   :local:

|

**********************
configuration.selenium 
**********************

**Category-name** |_|: :guilabel:`configuration.selenium`

.. admonition:: What |_|?

    :guilabel:`configuration.selenium` is the configuration for the Selenium Server used by 'launch' 'configuration.selenium' command.

**Here are the most usefull parameters** |_|:

    * squashtest.ta.selenium (mandatory) |_|: The value doesn't matter.
    * squashtest.ta.selenium.port |_|: if you don't use the default 4444 port.
    * squashtest.ta.selenium.probe.interval |_|: how many milliseconds between two queries to check if the Selenium Server is online.
    * squashtest.ta.selenium.probe.attempts |_|: how many check queries before deciding that the server startup failed and putting the launch command in error.

| 

--------------------

********************
script.html.selenium  
********************

**Category-name** |_|: :guilabel:`script.html.selenium`

.. admonition:: What |_|?

    :guilabel:`script.html.selenium` is a resource type that is a Selenium 1 test "selenese". This is basically a bundle.

| 

--------------------

*********************
script.java.selenium1   
*********************

**Category-name** |_|: :guilabel:`script.java.selenium1`

.. admonition:: What |_|?

     :guilabel:`script.java.selenium1` is a resource type that is a Selenium 1 test written in the java language. This is basically a bundle containing the test code compiled from its sources and the associated resources. It may be used by :ref:`'execute' 'script.java.selenium1'<selenium.legacy.commands.execute.script.java.selenium1.anchor>` command.

**Here are the supported parameters** |_|:

    * mainpath |_|: as in the bundle resource, it defines the path to the main file from the base of the bundle (the base directory of your selenium tests). For more details, see the :ref:`Converters section<selenium.legacy.converters.anchor>`.

| 

--------------------

***************
result.selenium   
***************

This resource is the same than the one used in the main Selenium Plugin. For more informations, please read the following :ref:`page <selenium.resource.result.selenium.anchor>`.

|

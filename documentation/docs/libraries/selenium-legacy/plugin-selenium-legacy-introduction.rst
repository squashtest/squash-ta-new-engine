..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################################
Selenium Plugin Legacy - Introduction
#####################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

********
Overview
********

The Selenium Plugin Legacy for SKF currently allows the integration of the following types of java Selenium Tests |_|:

    * Selenium 1

        * jUnit 3-based tests
        * jUnit 4-based tests

Selenium Tests must be included in the test project as java sources, and their non-selenium dependencies (if any) packaged as maven2 dependencies and declared in the *squash-ta-mavin-plugin* dependencies. The source code and resources are compiled each time before the test is executed. For more details, see the :ref:`Converters section<selenium.legacy.converters.anchor>`.

For Selenium 1 tests, you may at your convenience use the included selenium server management system (see :ref:`Managing your selenium server in ecosystem environment scripts<selenium.legacy.managing.selenium.server.anchor>` section), or use a selenium server already available in your testing environment.

If you are looking for information on how to import your existing selenium tests, see see the :ref:`importing legacy tests section<selenium.legacy.importing.legacy.selenium.tests.anchor>`.

**NB** |_|: a running installation of the browser used by your test is of course required on the machine your selenium RC server runs on.

| 

--------------------

**************************************
How to use the Selenium Legacy Plugin
**************************************

In order to be able to use the Selenium Legacy Plugin, you will need to add the following dependency inside your project pom.xml |_|:

.. code-block:: html

    ...
    <plugin>
        <groupId>org.squashtest.ta</groupId>
        <artifactId>squash-ta-maven-plugin</artifactId>
        <version>${project.version}</version>

        <dependencies>
            ...
            <dependency> 
                <groupId>org.squashtest.ta.plugin</groupId>
                <artifactId>squash-ta-plugin-selenium-one-legacy</artifactId>
                <version>${project.version}</version>
            </dependency>
            ...
        </dependencies>
        ...
    </plugin>
    ...

| 

--------------------

.. _selenium.legacy.managing.selenium.server.anchor:

**************************************************************
Managing your selenium server in ecosystem environment scripts
**************************************************************

To launch and kill your selenium server as part of the ecosystem environment management, proceed as follows |_|:

    * In your ecosystem setup.ta script, insert the following code |_|:

    .. list-table::

        * - :keywords:`LOAD` seleniumConf.properties :keywords:`AS` :resources:`seleniumConf.file`
            :keywords:`CONVERT` :resources:`seleniumConf.file` :keywords:`TO` :resource-type:`configuration.selenium` :keywords:`AS` :resources:`seleniumConf`
            :keywords:`EXECUTE` :converter-name:`launch` WITH :resources:`seleniumConf` :keywords:`AS` :resources:`seleniumServer`

    * In your ecosystem teardown.ta script, insert the following code |_|:

    .. list-table::

        * - :keywords:`EXECUTE` :converter-name:`cleanup` :keywords:`WITH` :resources:`seleniumServer` AS :resources:`ignoredResource`

**NB** |_|: Remember, resources defined in the ecosystem setup and teardown scripts can only be seen in these scripts, not in test scripts. However, a resource defined in the ecosystem setup script will be seen in the corresponding ecosystem teardown script, as the 'seleniumServer' resource above.

    * Create the 'seleniumConf.properties' file in your test resources. The minimal content is as follows |_|:

       .. list-table::

            * - squashtest.ta.selenium=yes

**NB** |_|: Any value is accepted as long as this key exists. See the ``configuration.selenium`` resource type documentation for useful parameters.

| 

--------------------

***********************************************************
Organizing selenium-related files in your Squash-TF project
***********************************************************

'File to script.java.selenium1' converter works from a ``file`` resource that is in fact a bundle (a group of files). This group of files may contain java sources and various resources. All sources and resources used by the test must be included in the file bundle.

This means that they must be all grouped in a directory that will be loaded as a ``file`` resource, and then converted to a ``script.java.selenium1`` resource.

As in usual java code and resources, the directory structure defines packages in which the classes and resources are located. As in the maven convention, source files will be searched for in the '<base>/java' directory, and any directory under '<base>/java' will define a package level. Resources will be searched for in the same way under '<base>/resources'.

Regarding binary dependencies (as opposed to helper source code, which must be included in the selenium test resource directory), they must be provided as maven jars included in the plugin dependencies like so |_|:

**Extract from test project pom.xml file**

.. code-block:: html

    ...
    <plugin>
        <groupId>org.squashtest.ta</groupId>
        <artifactId>squash-ta-maven-plugin</artifactId>
        <version>1.1.0</version>

        <dependencies>
        ...
        <!-- example of a Selenium test dependency -->
            <dependency>
                <groupId>net.sourceforge.javacsv</groupId>
                <artifactId>javacsv</artifactId>
                <version>2.0</version>
            </dependency>
        ...
        </dependencies>
        ...
    </plugin>
    ...

| 

--------------------

.. _selenium.legacy.importing.legacy.selenium.tests.anchor:

*******************************
Importing legacy Selenium tests
*******************************

To import your Selenium tests into your |squashTF| test projects, just copy your test source tree (with code AND resources) under a single subdirectory in the 'squashTA/resources' directory. For example, if your test source code and resources where under 'src/test/java' and 'src/test/resources' respectively, you just have to copy the java and test directories in the 'squashTA/resources/seleniumTests' subdirectory |_|:

.. image:: ../../_static/plugin-selenium/importing-legacy-selenium-tests.jpg
      :alt: Importing legacy Selenium tests. 
      :align: center

Now, you just have to load 'seleniumTests' as a resource to use it in your test.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################################
Selenium Plugin Legacy - Commands
#################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

********************************
'execute' 'script.html.selenium'
********************************

.. admonition:: What |_|?

    This command executes HTML format Selenium Suites ("selenese" format).

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{seleneseTest<Res:script.html.selenium>}` :keywords:`ON` :resources:`{webSUT<Tar:http>}` :keywords:`USING` :resources:`{conf<Res:file>}` :keywords:`AS` :resources:`{result<Res:result.selenium>}`

**> Input** |_|:

    * ``{seleneseTest<Res:script.html.selenium>}`` |_|: The name (in the context) of the resource which references the selenium test to execute (``script.html.selenium`` type resource).
    * ``{webSUT<Tar:http>}`` |_|: The name in (the context) of the target corresponding to the SUT (``http`` type target).
    * ``{conf<Res:file>}`` (optional) |_|: The name of the resource which references a configuration file which can contain two keys separated with comma |_|: 
        
        * mainpath |_|: The path (relative to the root directory of the bundle) to the html file containing the test suite.
        * browser |_|: The browser to use (http://stackoverflow.com/questions/1317055/how-to-run-google-chrome-with-selenium-rc).

    It can be define via an inline instruction. The format is <key>:<value>. 

**Note** |_|: The command needs the two keys (mainpath and browser). The command can retrieve them via the USING clause of the instruction or via ``script.html.selenium`` type resource (see the :ref:`From file to script.html.selenium<selenium.legacy.converters.from.file.to.script.html.anchor>` converter).

Those two configuration keys are optional in each instruction (command and conversion) but each one must be defined in at least one instruction. If a key is defined in the convert instruction and in the command instruction, the key in the command instruction predominates.

**> Output** |_|:

    * ``{result<Res:result.selenium>}`` |_|: The result of the test execution (``result.selenium`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`seleneseTest` :keywords:`ON` :resources:`TargetWeb` :keywords:`USING` :resources:`conf-file` :keywords:`AS` :resources:`result`

| 

--------------------

.. _selenium.legacy.commands.execute.script.java.selenium1.anchor:

*********************************
'execute' 'script.java.selenium1' 
*********************************

.. admonition:: What |_|?

    This command executes selenium 1 tests referenced as script.java.selenium1 resources.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`{selenium1Tests<Res:script.java.selenium1>}` :keywords:`USING` :resources:`{conf<Res:file>}` :keywords:`AS` :resources:`{result<Res:result.selenium>}`

**> Input** |_|:

    * ``{selenium1Tests<Res:script.java.selenium1>}`` |_|: The name (in the context) of the resource which references the java bundle containing the tests to execute (``script.java.selenium1`` type resource).
    * ``{conf<Res:file>}`` (optional) |_|: The name of the resource which references a configuration file which can contain only one key |_|: 
        
        * mainclass |_|: The qualified name of the main java class. The configuration must be supplied as a text file with one line containing the qualified name. If you give more, the last read line becomes the main class name. It can be defined via an inline instruction. The format is <key>:<value>.

**Note** |_|: The command needs this key. The command can retrieve it via the :keywords:`USING` clause of the instruction or via ``script.java.selenium1`` type resource (See the :ref:`From script.java to script.java.selenium1<selenium.legacy.converters.from.script.java.to.script.java.selenium1.anchor>` converter). This configuration key is optional in each instruction (command and conversion) but it must be defined in at least one instruction. If the key is defined in the convert instruction and in the command instruction, the key in the command instruction prime.

**> Output** |_|:

    * ``{result<Res:result.selenium>}`` |_|: The result of the test execution.(``result.selenium`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium :keywords:`AS` :resources:`seleniumTestSource`

        :keywords:`CONVERT` :resources:`seleniumTestSource` :keywords:`TO` :resource-type:`script.java` (:converter-name:`compile`) :keywords:`AS` :resources:`seleniumTestCompiled`
    
        :keywords:`CONVERT` :resources:`seleniumTestCompiled` :keywords:`TO` :resource-type:`script.java.selenium1` (:converter-name:`script`) :keywords:`USING` $(org.squashtest.Selenium1JUnit4) :keywords:`AS` :resources:`seleniumTest`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`seleniumTest` :keywords:`AS` :resources:`seleniumResult`

| 

--------------------

*********************************
'launch' 'configuration.selenium' 
*********************************

.. admonition:: What |_|?

    To execute a selenium1 test at java format a selenium server can be started. This command launches a Selenium Server instance (formerly SeleniumRC server) following the configuration described by a ``configuration.selenium`` resource. This command produces a ``process`` type resource which can be used with the cleanup command to kill it.

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`launch` :keywords:`WITH` :resources:`{seleniumServerConfig<Res:configuration.selenium>}` :keywords:`AS` :resources:`{seleniumServerProcess<Res:process>}`

**> Input** |_|:

    * ``{seleniumServerConfig<Res:configuration.selenium>}`` |_|: The name (in the context) of the resource which references a configuration file permitting to start a Selenium server (``configuration.selenium`` type resource).

**> Output** |_|:

    * ``{seleniumServerProcess<Res:process>}`` |_|: The name (in the context) of the resource which references a processus linked to the Selenium Server (``process`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` selenium/selenium-rc-conf.properties :keywords:`AS` :resources:`selenium-rc-conf.file`

        :keywords:`CONVERT` :resources:`selenium-rc-conf.file` :keywords:`TO` :resource-type:`configuration.selenium` :keywords:`AS` :resources:`selenium-rc-conf`

        :keywords:`EXECUTE` :converter-name:`launch` :keywords:`WITH` :resources:`selenium-rc-conf` :keywords:`AS` :resources:`seleniumServer`

|

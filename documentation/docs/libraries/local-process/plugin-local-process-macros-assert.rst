..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

####################################
Local process Plugin - Assert Macros
####################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

******************************************************
# ASSERT {result} IS FAILURE WITH EXIT CODE {exitCode}
******************************************************

.. admonition:: What |_|?

    This macro will verify that the result of a failed execution command contains the expected exit code.

**Underlying instruction** |_|:

.. code-block:: html

    ASSERT {result} IS failure WITH $({exitCode})

**> Input** |_|:

        * ``{result}`` |_|: The resource file that contains the result of a shell execution command (``result.shell`` type resource).
        * ``{exitCode}`` |_|: The expected return code of the command execution.

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT result IS failure WITH exit code 2`

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-1-script.png

    **Console output** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-1-stacktrace.png

| 

--------------------

*****************************************
# ASSERT {result} STDOUT CONTAINS {regex}
*****************************************

.. admonition:: What |_|?

    This macro will verify that the standard outflow resulting of a shell command execution contains a specific character string.

**Underlying instruction** |_|:

.. code-block:: html

    ASSERT {result} DOES contain WITH $({regex}) USING $(out)

**> Input** |_|:

        * ``{result}`` |_|: The resource file that contains the result of a shell execution command (``result.shell`` type resource).
        * ``{regex}`` |_|: The searched character string.

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT result STDOUT CONTAINS hello world`

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-2-script.png

| 

--------------------

*************************************************
# ASSERT {result} STDOUT DOES NOT CONTAIN {regex}
*************************************************

.. admonition:: What |_|?

    This macro will verify that the standard outflow resulting of a shell command execution does not contain a specific character string.

**Underlying instruction** |_|:

.. code-block:: html

    ASSERT {result} DOES not.contain WITH $({regex}) USING $(out)

**> Input** |_|:

        * ``{result}`` |_|: The resource file that contains the result of a shell execution command (``result.shell`` type resource).
        * ``{regex}`` |_|: The searched character string.

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT result STDOUT DOES NOT CONTAIN hello world`

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-3-script.png

    **Console output** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-3-stacktrace.png

| 

--------------------

*****************************************
# ASSERT {result} STDERR CONTAINS {regex}
*****************************************

.. admonition:: What |_|?

    This macro will verify that the error outflow resulting of a shell command execution contains a specific character string.

**Underlying instruction** |_|:

.. code-block:: html

    ASSERT {result} DOES contain WITH $({regex}) USING $(err)

**> Input** |_|:

        * ``{result}`` |_|: The resource file that contains the result of a shell execution command (``result.shell`` type resource).
        * ``{regex}`` |_|: The searched character string.

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT result STDERR CONTAINS Fichier introuvable`

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-4-script.png

| 

--------------------

*************************************************
# ASSERT {result} STDERR DOES NOT CONTAIN {regex}
*************************************************

.. admonition:: What |_|?

    This macro will verify that the error outflow resulting of a shell command execution does not contain a specific character string.

**Underlying instruction** |_|:

.. code-block:: html

    ASSERT {result} DOES not.contain WITH $({regex}) USING $(err)

**> Input** |_|:

        * ``{result}`` |_|: The resource file that contains the result of a shell execution command (``result.shell`` type resource).
        * ``{regex}`` |_|: The searched character string.

**Example** |_|:

    .. list-table::

        * - :macros:`# ASSERT result STDERR DOES NOT CONTAIN Fichier introuvable`

    **SKF script** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-5-script.png

    **Console output** |_|:

    .. container:: image-container

        .. image:: ../../_static/plugin-local-process/assert/local-process-macros-assert-5-stacktrace.png

|

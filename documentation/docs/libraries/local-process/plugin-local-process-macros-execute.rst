..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#####################################
Local process Plugin - Execute Macros
#####################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

**************************************************
# EXECUTE $({command_content}) LOCALLY AS {result}
**************************************************

.. admonition:: What |_|?

    This macro will execute an inline command on the local system and check if the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    DEFINE $({command_content}) AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell AS __commandLine{%%rand2}
    EXECUTE local WITH __commandLine{%%rand2} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: The shell command to execute, preceded by a call to the shell ("cmd.exe |_| /C" for Windows, "/bin/sh |_| -c" for Linux).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE $(cmd.exe /C echo "hello world") LOCALLY AS result`

| 

--------------------

**********************************************************************
# EXECUTE $({command_content}) LOCALLY AS {result} WITHIN {timeout} ms
**********************************************************************

.. admonition:: What |_|?

    This macro will execute an inline command on the local system, within a timeframe, and check if the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    DEFINE $({command_content}) AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell AS __commandLine{%%rand2} USING $(timeout:{timeout})
    EXECUTE local WITH __commandLine{%%rand2} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: The shell command to execute, preceded by a call to the shell ("cmd.exe |_| /C" for Windows, "/bin/sh |_| -c" for Linux).
        * ``{timeout}`` |_|: Maximal time authorized for the command execution (in milliseconds).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :macros:`# EXECUTE $(cmd.exe /C echo "hello world") LOCALLY AS result WITHIN 15000 ms`

| 

--------------------

******************************************************
# EXECUTE SCRIPT {command_content} LOCALLY AS {result}
******************************************************

.. admonition:: What |_|?

    This macro will execute a script on the local system and check if the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {command_content} AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell AS __commandLine{%%rand2}
    EXECUTE local WITH __commandLine{%%rand2} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: The script file containing the shell commands to execute, preceded by a call to the shell ("cmd.exe |_| /C" for Windows, "/bin/sh |_| -c" for Linux).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE SCRIPT command.bat LOCALLY AS result`

    **File to process (Windows)** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-command.png

    **The folder containing the resources to process** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-tree.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-script.png

| 

--------------------

************************************************************************************
# EXECUTE SCRIPT {command_content} LOCALLY AS {result} WITHIN {timeout_in_seconds} s
************************************************************************************

.. admonition:: What |_|?

    This macro will execute a script on the local system, within a timeframe, and check if the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {command_content} AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell USING $(timeout:{timeout_in_seconds}000) AS __commandLine{%%rand2}
    EXECUTE local WITH __commandLine{%%rand2} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: The script file containing the shell commands to execute, preceded by a call to the shell ("cmd.exe |_| /C" for Windows, "/bin/sh |_| -c" for Linux).
        * ``{timeout_in_seconds}`` |_|: Maximal time authorized for the command execution (in seconds).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE SCRIPT command.bat LOCALLY AS result WITHIN 5 s`

    **File to process (Windows)** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-command.png

    **The folder containing the resources to process** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-tree.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-4-script.png

| 

--------------------

*********************************************************************************
# EXECUTE SCRIPT {command_content} LOCALLY AS {result} WITH STREAMLENGTH {length}
*********************************************************************************

.. admonition:: What |_|?

    This macro will execute a script on the local system, with the length of the stream specified, and check if the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {command_content} AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell USING $(streamlength:{length}) AS __commandLine{%%rand2}
    EXECUTE local WITH __commandLine{%%rand2}  AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: The script file containing the shell commands to execute, preceded by a call to the shell ("cmd.exe |_| /C" for Windows, "/bin/sh |_| -c" for Linux).
        * ``{length}`` |_|: An integer that represents stream length (number of characters). Specifying “full” allows to have the entire stream.

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE SCRIPT command.bat LOCALLY AS result WITH STREAMLENGTH 200`

    **File to process (Windows), with an error** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-5-command.png

    **The folder containing the resources to process** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-tree.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-5-script.png

    .. Warning::
        Streamlength (with a value of *n*) will shorten both the **STDOUT** and the **STDERR**, keeping only the *n* last characters, possibly rendering the debug harder |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-5-error.png

| 

--------------------

***************************************************************************************************************
# EXECUTE SCRIPT {command_content} LOCALLY AS {result} WITH STREAMLENGTH {length} WITHIN {timeout_in_seconds} s
***************************************************************************************************************

.. admonition:: What |_|?

    This macro will execute a script on the local system, with the length of the stream specified and within a timeframe, and check if the result is a success.

**Underlying instructions** |_|:

.. code-block:: html

    LOAD {command_content} AS __command{%%rand1}
    CONVERT __command{%%rand1} TO query.shell USING $(timeout:{timeout_in_seconds}000, streamlength:{length}) AS __commandLine{%%rand2}
    EXECUTE local WITH __commandLine{%%rand2} AS {result}
    ASSERT {result} IS success

**> Input** |_|:

        * ``{command_content}`` |_|: The script file containing the shell commands to execute, preceded by a call to the shell ("cmd.exe |_| /C" for Windows, "/bin/sh |_| -c" for Linux).
        * ``{length}`` |_|: An integer that represents stream length (number of characters). Specifying “full” allows to have the entire stream.
        * ``{timeout_in_seconds}`` |_|: Maximal time authorized for the command execution (in seconds).

**> Output** |_|:

        * ``{result}`` |_|: The name of the resource which references the result of the command (``result.shell`` type resource).

**Example** |_|:

    .. list-table::

        * - :macros:`# EXECUTE SCRIPT command.bat LOCALLY AS result WITH STREAMLENGTH 200 WITHIN 5 s`

    **File to process (Windows), with an error** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-5-command.png

    **The folder containing the resources to process** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-3-tree.png

    **SKF script** |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-6-script.png

    .. Warning::
        Streamlength (with a value of *n*) will shorten both the **STDOUT** and the **STDERR**, keeping only the *n* last characters, possibly rendering the debug harder |_|:

        .. container:: image-container

            .. image:: ../../_static/plugin-local-process/execute/local-process-macros-execute-5-error.png

|

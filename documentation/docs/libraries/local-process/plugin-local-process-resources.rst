..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################################
Local process Plugin - Resources
################################

.. contents:: Contents:
   :local:

|

.. _local.process.resources.process.anchor:

*******
process
*******

**Category-name** |_|: :guilabel:`process`

.. admonition:: What |_|?

    The :guilabel:`process` resource category encapsulates a process handle to allow operations on processes. Currently only one command operates on this kind of resource: the cleanup (process) command that is designed to kill the process (this is mainly associated with environment management).

| 

--------------------

.. _local.process.resources.query.shell.process.anchor:

***********
query.shell
***********

**Category-name** |_|: :guilabel:`query.shell`

.. admonition:: What |_|?

    :guilabel:`query.shell` represents a resource which contains one or several shell command lines.

**Attributes** |_|: The command line supports an optional timeout attribute that specifies how long to wait before timing out during its execution. The time is measured in milliseconds (range |_|: strictly positive up to 2^31-1 (around 2 billions)).

| 

--------------------

.. _local.process.resources.result.shell.process.anchor:

************
result.shell
************

**Category-name** |_|: :guilabel:`result.shell`

.. admonition:: What |_|?

     :guilabel:`result.shell` is a resource type that represents the result of a shell command execution (exit code, output streams...).

|

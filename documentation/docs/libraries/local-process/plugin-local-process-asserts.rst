..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##############################
Local process Plugin - Asserts
##############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

.. _result.shell.is.success.anchor:

***************************
'result.shell' is 'success' 
***************************

.. admonition:: What |_|?

    Asserts that the result of the command execution is success.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{result<Res:result.shell>}` :keywords:`IS` :converter-name:`success`

        :keywords:`VERIFY` :resources:`{result<Res:result.shell>}` :keywords:`IS` :converter-name:`success`

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{result<Res:result.shell>}`` |_|: The name of the resource which references a resource that contains the result of a shell execution command (``result.shell`` type resource).

**Example** |_|:

.. list-table::

    * - :keywords:`LOAD` shell/shell_command.txt :keywords:`AS` :resources:`command.file`

        :keywords:`CONVERT` :resources:`command.file` :keywords:`TO` query.shell :keywords:`USING` $(timeout:15000) :keywords:`AS` :resources:`commandLine`

        :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`commandLine` :keywords:`ON` :resources:`ssh_server` :keywords:`AS` :resources:`result`

        :keywords:`ASSERT` :resources:`result` :keywords:`IS` :converter-name:`success`

| 

--------------------

.. _result.shell.is.failure.anchor:

*******************************************************
'result.shell' is 'failure' with {expected return code} 
*******************************************************

.. admonition:: What |_|?

    Asserts that the return code of a command execution who failed is the expected code.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{result<Res:result.shell>}` :keywords:`IS` :converter-name:`failure` :keywords:`WITH` $(<expectedCode>)

        :keywords:`VERIFY` :resources:`{result<Res:result.shell>}` :keywords:`IS` :converter-name:`failure` :keywords:`WITH` $(<expectedCode>)

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{result<Res:result.shell>}`` |_|: The name of the resource which references a resource that contains the result of a shell execution command (``result.shell`` type resource).
    * ``<expectedCode>`` |_|: The expected return code of the command execution.

**Example** |_|:

.. list-table::

    * - :keywords:`ASSERT` :resources:`result` :keywords:`IS` :converter-name:`failure` :keywords:`WITH` $(1)

| 

--------------------

.. _result.shell.does.contain.anchor:

*************************************
'result.shell' does 'contain' {regex}
*************************************

.. admonition:: What |_|?

    Asserts that a stream (standard exit stream or error stream) resulting of an execution command contains a character string.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{result<Res:result.shell>}` :keywords:`DOES` :converter-name:`contain` :keywords:`WITH` $(<searchPattern>) :keywords:`USING` $(<streamType>)

        :keywords:`VERIFY` :resources:`{result<Res:result.shell>}` :keywords:`DOES` :converter-name:`contain` :keywords:`WITH` $(<searchPattern>) :keywords:`USING` $(<streamType>)

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{result<Res:result.shell>}`` |_|: The resource that contains the result of an execution command (``result.shell`` type resource).
    * ``<searchPattern>`` |_|: The regular expression searched in the stream.
    * ``<streamType>`` |_|: The kind of stream in which we are searching the character string. 2 values are possible |_|:

        * **out** |_|: To search inside a standard output stream.
        * **err** |_|: To search inside the error stream.

**Note** |_|:  If you want to check for special characters used in the regular expression formalism, you will have to escape them with a backslash (" \\ ").

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`commandLine` :keywords:`ON` :resources:`ssh-server` :keywords:`AS` :resources:`result`

        :keywords:`ASSERT` :resources:`result` :keywords:`DOES` :converter-name:`contain` :keywords:`WITH` $(hello world) :keywords:`USING` $(err)

| 

--------------------

.. _result.shell.does.not.contain.anchor:

*****************************************
'result.shell' does 'not.contain' {regex}
*****************************************

.. admonition:: What |_|?

    Asserts that a stream (standard exit stream or error stream) resulting of an execution command does not contain a specific character string.

.. list-table::

    * - :keywords:`ASSERT` :resources:`{result<Res:result.shell>}` :keywords:`DOES` :converter-name:`not.contain` :keywords:`WITH` $(<searchPattern>) :keywords:`USING` $(<streamType>)

        :keywords:`VERIFY` :resources:`{result<Res:result.shell>}` :keywords:`DOES` :converter-name:`not.contain` :keywords:`WITH` $(<searchPattern>) :keywords:`USING` $(<streamType>)

**Note** |_|: For differences between :keywords:`ASSERT` and :keywords:`VERIFY` assertion mode see :ref:`this page<assertion.instruction.anchor>`.

**> Input** |_|:

    * ``{result<Res:result.shell>}`` |_|: The resource that contains the result of an execution command (``result.shell`` type resource).
    * ``<searchPattern>`` |_|: The regular expression searched in the stream.
    * ``<streamType>`` |_|: The kind of stream in which we are searching the character string. 2 values are possible |_|:

        * **out** |_|: To search inside a standard output stream.
        * **err** |_|: To search inside the error stream.

**Note** |_|:  If you want to check for special characters used in the regular expression formalism, you will have to escape them with a backslash (" \\ ").

**Example** |_|:

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`commandLine` :keywords:`ON` :resources:`ssh-server` AS :resources:`result`

        :keywords:`ASSERT` :resources:`result` :keywords:`DOES` :converter-name:`not.contain` :keywords:`WITH` $(hello world) :keywords:`USING` $(err)

|

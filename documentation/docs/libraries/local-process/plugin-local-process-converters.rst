..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################################
Local process Plugin - Converters
#################################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

|

.. _from.file.to.query.shell.anchor:

************************
From file to query.shell 
************************

**Category-name** |_|: :guilabel:`query`

.. admonition:: What |_|?

    This :guilabel:`query` converter will convert a ``file`` type resource to a ``query.shell`` type resource.

+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|:keywords:`CONVERT` :resources:`{resourceToConvert<Res:file>}` :keywords:`TO` :resource-type:`query.shell` (:converter-name:`query`) :keywords:`AS` :resources:`{converted<Res:query.shell>}` [ :keywords:`USING` :resources:`{config<Res:file>}` ]|
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

**> Input** |_|:

    * ``{resourceToConvert<Res:file>}`` |_|: The name of the file which includes one or several shell command lines. Commands can be on one line separated by the character ';' or on several lines (in this case the ';' is optional) and comments beginning with '#' are authorized.
    * ``{config<Res:file> (Optional)}`` |_|: The name of the resource which references a configuration file which contains only one key |_|/ |_| value |_|: 

            * 'timeout |_|: An integer that represents time in milliseconds. It's the time to wait before the command execution times out. It can also be defined via an inline instruction |_|: $(timeout |_|: |_|...).
            * 'streamlength |_|: An integer that represents the stream length (number of characters). An option "full" allows to have the entire stream. It can also be defined via an inline instruction |_|: $(streamlength |_|: |_|...). Streamlength property is available since **1.8.0** version.

**> Output** |_|:

    * ``{converted<Res:query.shell>}`` |_|: The name of the converted resource (``query.shell`` type resource).


**Example** |_|:

+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|:keywords:`LOAD` shell/shell_command_03.txt :keywords:`AS` :resources:`command.file`                                                                                                                                                               |
|                                                                                                                                                                                                                                                   |
|:keywords:`CONVERT` :resources:`command.file` :keywords:`TO` :resource-type:`query.shell` :keywords:`USING` $(timeout:15000, streamlength:600) :keywords:`AS` :resources:`commandLine`                                                             |
|                                                                                                                                                                                                                                                   |
|:keywords:`LOAD` shell/shell_command_03.txt :keywords:`AS` :resources:`command.file`                                                                                                                                                               |
|                                                                                                                                                                                                                                                   |
|:keywords:`CONVERT` :resources:`command.file` :keywords:`TO` :resource-type:`query.shell` :keywords:`USING` $(streamlength:full) :keywords:`AS` :resources:`commandLine`                                                                           |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

|

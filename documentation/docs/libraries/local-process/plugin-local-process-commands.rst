..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###############################
Local process Plugin - Commands
###############################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. contents:: Contents |_|:
   :local:

|

.. _local.process.commands.cleanup.anchor:

*******
cleanup
*******

.. admonition:: What |_|?

    It allows killing a processus, mainly for ecosystem environment management.


+------------------------------------------------------------------------------------------------------------+
|:keywords:`EXECUTE` :converter-name:`cleanup` :keywords:`WITH` :resources:`<Res:process>` :keywords:`AS` $()|
+------------------------------------------------------------------------------------------------------------+


**> Input** |_|:

    * ``<Res:process>`` |_|: The name of the resource which references the processus to kill (``process`` type resource).

| 

--------------------

*********************
'local' 'query.shell'
*********************

.. admonition:: What |_|?

    Command to execute a command line on the local system.

+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|:keywords:`EXECUTE` :converter-name:`local` :keywords:`WITH` :resources:`{query<Res:query.shell>}` :keywords:`USING` :resources:`$(timeout:<n>,streamlength:<n'>)` :keywords:`AS` :resources:`{result<Res:Result.shell>}` |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

**> Input** |_|:

    * ``{query<Res:query.shell>}`` |_|: The name of the resource referencing a file which includes one (and one only) command line (``query.shell`` type resource).
    * <n> |_|: An integer that represents time in milliseconds. It is the time the command execution will wait before crashing.
    * <n'> |_|: An integer that represents the stream length (number of characters). An option "full" allows to have the entire stream. It can be define via an inline instruction |_|: $(streamlength |_|: |_|...). Streamlength property is available since **1.8.0** version.

**Note 1** |_|: If the timeout property is not defined here, we use the timeout property of ``query.shell`` resource (set to 5s by default).

**Note 2** |_|: Be careful |_|: Local process is not equivalent to a console, it only executes programs. So if you want to use it as a console, you should specify which shell you want to use in your command line. Most of the time |_|:

    * For windows, start your command line with |_|: "cmd.exe |_| /C" (first line).
    * For linux, start your command with |_|: /bin/sh |_| -c (according to the distribution this may be useless).

**Note 3** |_|: As local process use the underlying OS, the TA scripts which use it are platform dependent.

**Note 4** |_|: if the streamlength property is not defined here, we use the streamlength property by default (set to 300 characters).

**> Output** |_|:

    * ``{result<Res:result.shell>}`` |_|: The name of the resource which contains the shell command result (``result.shell`` type resource).

**Example (Linux)** |_|:

.. list-table::

    * - :keywords:`DEFINE` $(echo hello world) :keywords:`AS` :resources:`command.file`

        :keywords:`CONVERT` :resources:`command.file` :keywords:`TO` :resource-type:`query.shell` :keywords:`USING` $(timeout:15000, streamlength:600) :keywords:`AS` :resources:`commandLine`

        :keywords:`EXECUTE` :converter-name:`local` :keywords:`WITH` :resources:`commandLine` :keywords:`AS` :resources:`result`
        
        :keywords:`ASSERT` :resources:`result` :keywords:`DOES` :converter-name:`contain` :keywords:`WITH` $(hello world) :keywords:`USING` $(out)


**Note** |_|: To execute several command lines, you will need to execute a batch. You must then either give the absolute path of your batch or its relative path from your project's root in the :keywords:`DEFINE` instruction.

**Example (Windows)** |_|:

.. list-table::

    * - :keywords:`LOAD` command.bat :keywords:`AS` :resources:`command.file`
        
        :keywords:`CONVERT` :resources:`command.file` :keywords:`TO` :resource-type:`query.shell` :keywords:`AS` :resources:`commandLine`

        :keywords:`EXECUTE` :converter-name:`local` :keywords:`WITH` :resources:`commandLine` :keywords:`USING` $(timeout:15000, streamlength:full) :keywords:`AS` :resources:`result`

        :keywords:`ASSERT` :resources:`result` :keywords:`DOES` :converter-name:`contain` :keywords:`WITH` $(hello world) :keywords:`USING` $(out)
        
        :keywords:`ASSERT` :resources:`result` :keywords:`DOES` :converter-name:`contain` :keywords:`WITH` $(nice day) :keywords:`USING` $(out)

**command.bat** |_|:

.. code-block:: html

    cmd.exe /C
    echo hello world
    echo have a nice day

|

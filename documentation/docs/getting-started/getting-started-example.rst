..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _getting.started.example.anchor:

########################
An example to begin with
########################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros

.. role:: red
.. role:: green

.. raw:: html

    <style> .red {color:red} </style>
    <style> .green {color:green} </style>

.. contents:: Contents |_|:
    :local:

|

In this example, we will show you a simple SKF script that uses macros to do tests against an H2 database in embedded mode.

|

----------------------
            
****************
Create a project
****************

First of all you need to open your favorite IDE and create a new maven project with squash-ta-archetype.

If you don’t know how to generate a maven archetype, you can follow our :ref:`guide <getting.started.create.new.project.anchor>`.

Delete all the samples in the folders of the generated project. Just keep the structure |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/clean-skf-project.png
    :align: center

|

----------------------

******************
Configure database
******************

For the database, you need to add the following dependency to your **POM** file |_| :

    .. code-block:: xml

        <dependencies>
             <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>1.3.176</version>
            </dependency>
        </dependencies>

Your **POM** file should look like this |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/pom-file.png
    :align: center

In the targets folder (be careful about the name, targets |_| != |_| target), you need to create a **.properties** file.
      
The **.properties** file should have the following properties |_|:

        * **#!db** |_|: The shebang to indicate that this file contains informations about a database.
        * **squashtest.ta.database.driver** |_|: The driver to be used.
        * **squashtest.ta.database.url** |_|: The path to the database.
        * **squashtest.ta.database.username** (optional, not used in our example) |_|: The username to use to connect to the database.
        * **squashtest.ta.database.password** (optional, not used in our example) |_|: The password to use to connect to the database.

In our example, it will be as follow |_|::

    #!db
    squashtest.ta.database.driver=org.h2.Driver
    squashtest.ta.database.url=jdbc:h2:file:target/database/myH2


**.properties file to connect to a database** |_| **:**

.. figure:: ../_static/getting-started/example-to-begin-with/properties-file.png
    :align: center

|

----------------------

*****************
Create SKF script
*****************

In **tests** folder create a **.ta** file.

In this file, write down |_| :

    .. code-block:: xml

        SETUP :


        TEST :


        TEARDOWN :


.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-new.png
    :align: center

We will use those 3 phases in our example.

|

----------------------

***************************************************
Execute an SQL script to create a table in database
***************************************************

First of all, during the **SETUP phase**, we want te create a new table in our H2 database.

To do so, we need to create a **.sql** script file in **resources** folder. It’s good practice to create different subfolders for each type of resources (sql, selenium, soapui, etc).

**Here is the script** |_| **:**

    .. code-block:: sql

        DROP TABLE IF EXISTS PETS_STORE;

        CREATE TABLE PETS_STORE (
                ID INT NOT NULL,
                ANIMAL VARCHAR(45) NULL,
                COLOR VARCHAR(45) NULL,
                QUANTITY INT NULL,
                PRIMARY KEY (id)
        );

.. figure:: ../_static/getting-started/example-to-begin-with/sql-script.png
    :align: center

In the SKF script, add the following macro to your **SETUP phase** |_| :

.. list-table::

    * - :macros:`# EXECUTE_SQL_SCRIPT {file} ON {database} AS {result}`

``{file}`` |_|: The SQL script, we have just created. Give the path of the file in the **resources** folder.

``{database}`` |_|: The database we want to operate the script on. Give the name of the .properties file you have created in the **targets** folder (without the .properties extension).

``{result}`` |_|: A free identifier for the result. As the ‘execute’ command with an sql script return an empty resource, this result resource will also be empty.

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-sql-script-macro.png
    :align: center

|

----------------------

*************************************************
Populate the database table with a DbUnit dataset
*************************************************

The populate the table, we will use a DbUnit dataset.

Create an **.xml** file in resources folder. You should also create a **dbunit** subfolder.

In this file, write down the following |_| :

    .. code-block:: xml

        <?xml version="1.0" encoding="UTF-8"?>

        <dataset>
            <PETS_STORE ID="1" ANIMAL="cat" COLOR="black" QUANTITY="4"/>
            <PETS_STORE ID="2" ANIMAL="cat" COLOR="white" QUANTITY="2"/>
            <PETS_STORE ID="3" ANIMAL="cat" COLOR="grey" QUANTITY="5"/>
            <PETS_STORE ID="4" ANIMAL="cat" COLOR="red hair" QUANTITY="2"/>    
            <PETS_STORE ID="5" ANIMAL="cat" COLOR="invisible" QUANTITY="0"/>
        </dataset>

.. figure:: ../_static/getting-started/example-to-begin-with/dataset.png
    :align: center

In the SKF script, add the following macro to your **SETUP phase** |_| :

.. list-table::

   * - :macros:`# INSERT_DBUNIT {dataset} INTO {database}`

``{dataset}`` |_|: The **.xml**, we have just created. Give the path of the file in the **resources** folder.

``{database}`` |_|: The database we want to operate the script on. Give the name of the .properties file you have created in the **targets** folder (without the .properties extension).

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-insert-dbunit.png
    :align: center

|

----------------------

****************************************************************
Test that our table contains expected data with a DbUnit dataset
****************************************************************

First we will do an incorrect dataset so that the assertion executed by the script fails.

Create a new **.xml** file in the **resources/dbunit** folder.

Write down the following dataset |_| :

    .. code-block:: xml

        <?xml version="1.0" encoding="UTF-8"?>

        <dataset>
            <PETS_STORE ID="1" ANIMAL="cat" COLOR="black" QUANTITY="4"/>
            <PETS_STORE ID="2" ANIMAL="cat" COLOR="green" QUANTITY="2"/>
        </dataset>

.. figure:: ../_static/getting-started/example-to-begin-with/partial-dataset-ko.png
    :align: center

In the SKF script, add the following macro to your **TEST phase** |_| :

.. list-table::

    * - :macros:`# ASSERT_DBUNIT TARGET {database} CONTAINS {dataset}`

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-assert-contains-macro-ko.png
    :align: center

Now we are going to execute th script. Use the following maven command to build your project |_| :

.. list-table::

    * - mvn squash-ta:run

.. figure:: ../_static/getting-started/example-to-begin-with/build-failure-assert-contains.png
    :align: center

After the execution, an HTML report is generated. It can give further details about the reason of the failure.

You can access this report in **target/squashTF/html-reports** folder |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/html-report-location.png
    :align: center

Open this report with the web browser of your choice |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/html-report-build-failure-assert-contains.png
    :align: center
    :target: ../_static/getting-started/example-to-begin-with/html-report-build-failure-assert-contains.png


You can the diffrences between the dataset and the database by opening **EXECUTION_REPORT-diff** in the attachments |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/html-report-build-failure-assert-contains-attachment-file.png
    :align: center

Now we are going to create a new **.xml** file with a correct dataset |_| :

    .. code-block:: xml

        <?xml version="1.0" encoding="UTF-8"?>

        <dataset>
            <PETS_STORE ID="1" ANIMAL="cat" COLOR="black" QUANTITY="4"/>
            <PETS_STORE ID="2" ANIMAL="cat" COLOR="white" QUANTITY="2"/>
        </dataset>

.. figure:: ../_static/getting-started/example-to-begin-with/partial-dataset.png
    :align: center

Don't forget to change the dataset used in the SKF script |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-assert-contains-macro.png
    :align: center

If you execute the script again, you should have a build :green:`SUCCESS`.

|

----------------------

**************************************************
Test that our table contains all the expected data
**************************************************

As in the previous example, we will start with an incorrect dataset.

Create a new **.xml** file in the **resources/dbunit** folder and write down the following dataset |_| :

    .. code-block:: xml

        <?xml version="1.0" encoding="UTF-8"?>

        <dataset>
            <PETS_STORE ID="1" ANIMAL="cat" COLOR="black" QUANTITY="4"/>
            <PETS_STORE ID="2" ANIMAL="cat" COLOR="white" QUANTITY="2"/>
            <PETS_STORE ID="3" ANIMAL="cat" COLOR="grey" QUANTITY="5"/>
            <PETS_STORE ID="4" ANIMAL="cat" COLOR="red hair" QUANTITY="2"/>    
        </dataset>

The invisible cat is missing.

.. figure:: ../_static/getting-started/example-to-begin-with/dataset-ko.png
    :align: center

In the SKF script, add the following macro to your **TEST phase** |_| :

.. list-table::

    * - :macros:`# ASSERT_DBUNIT TARGET {database} EQUALS {dataset}`

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-assert-equals-macro-ko.png
    :align: center

Execute the script. You should have a build failure with the following error |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/build-failure-assert-equals.png
    :align: center

You can open the HTML report to have more details  |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/html-report-build-failure-assert-equals.png
    :align: center
    :target: ../_static/getting-started/example-to-begin-with/html-report-build-failure-assert-equals.

In SKF script, change the dataset in the last macro and use the first one we created to populate the table |_| :

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-assert-equals-macro.png
    :align: center

If you execute the script again, you should have a build :green:`SUCCESS`.

|

----------------------

******************
Clean the database
******************

The last thing we want to do is to clean the database after the execution of the test.

In SKF script, add the following macro in **TEARDOWN phase** |_| :

.. list-table::

    * - :macros:`# DELETE_DBUNIT {dataset} FROM {database}`

.. figure:: ../_static/getting-started/example-to-begin-with/skf-script-teardown.png
    :align: center

|

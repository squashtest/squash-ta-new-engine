..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#############
Writing tests
#############

A script is at the basis of an SKF automation test. In this section, we're going to see the different elements of an SKF Script and how it is built.

.. toctree::
  :titlesonly:
  :maxdepth: 2

  Tests script sections <writing-tests-phases.rst>
  Resource Components <writing-tests-resource-components.rst>
  Macros <writing-tests-macros.rst>
  Ecosystem <writing-tests-ecosystem.rst>
  Advanced users <writing-tests-advanced-users.rst>

|

An SKF script is a file containing an amount of instructions, resources components, engine components and shortcuts (macros) that will be interpreted by the engine of SKF to execute automation tests.

All those elements form the specific language of SKF to describe automation tests. 

It allows to address an heterogeneous panel of tests with a common formalism.

First, we will explain the different :ref:`phases <writing.test.phases.anchor>` of an SKF script and see what are the :ref:`resource components <writing.test.resources.components.anchor>`.

We will also see how :ref:`macros <writing.test.macros.anchor>` work.

-----------------------------

In an :ref:`Advanced Users <writing.tests.advanced.users.anchor>` section, we will explain the :ref:`instructions <writing.test.instructions.anchor>` and :ref:`engine components <writing.test.engine.components.anchor>` which are behind the macros.

With those knowledge, you can write your own custom macros if you wish.

-----------------------------

If you want to see how macros are used in an SKF script, you can check this :ref:`example <getting.started.example.anchor>`.

To see how instructions and engine components are used, please check this other :ref:`example of SKF script <writing.test.example.anchor>`.

|

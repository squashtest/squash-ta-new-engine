..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _writing.test.resources.components.anchor:

###################
Resource Components
###################

.. contents:: Contents |_|:
    :local:


|

*******************************
Resource, repository and target
*******************************

Instructions and shortcuts are using resources and targets (Targets to test). Resources are included in repositories (librairies of resources).
There are 3 kinds of resource components within SKF |_|:

Resource
********

An SKF resource is a data. More precisely, it is a container which includes certain content (the data) and a category which qualifies the type of data.

It may come from various origins |_|: your own test data, results from the SUT (System Under Test) or derived from another resource. 

SKF has defined one category of resource |_|: the :guilabel:`file` resource. This is the basic category of resource in SKF and is nothing more than a handle on a file.

This type of resource is very common in SKF scripts since any resource loaded from a repository (see below) will first be assumed to be a :guilabel:`file` resource before it can be converted to a more appropriate type (examples of resource categories: :guilabel:`file`, :guilabel:`xml`, :guilabel:`script.sql`...).

When a resource is loaded, it's created in a specific context and is available only for this context (See below :ref:`Test context / Ecosystem context <writing.test.test.ecosystem.context.anchor>`).

Repository
**********

A repository represents a physical resources location where you will be able to access your data and bring them in the Test context as :guilabel:`file` resources.

Repositories are read-only by design |_|: their content cannot be altered by a script execution.

It's defined by |_|:

    * A ``.properties`` file. The name of the file will be the identifier of the resource repository in the test context.
    * Only one resource repository must be defined per ``.properties`` file.
    * Repository categories |_|: :guilabel:`URL`, :guilabel:`FTP`, :guilabel:`classpath`.

Here are some examples we can find in the 'repository' directory of an automation project |_|:

**Example** |_| **1** |_| **: ftp_example.properties**

.. code-block:: none
    
    squashtest.ta.ftp.host=192.168.2.24
    squashtest.ta.ftp.username=squash
    squashtest.ta.ftp.password=squash
    squashtest.ta.ftp.system=windows
    squashtest.ta.ftp.port=21

**Example** |_| **2** |_| **: url_example.properties**

.. code-block:: none

    squashtest.ta.url.base=http://repo.squashtest.org/maven2/releases/eclipse/jdtcore/

Target
******

Targets represent the SUT (System Under Test). They may stand for a http, a ftp server, a SSH server or a database. They may be in read-write mode. It's defined by |_|:

    * A ``.properties`` file. The name of the file will be the identifier of the target in the test context.
    * Only one target must be defined per file.
    * Target categories |_|: :guilabel:`database`, :guilabel:`http`, :guilabel:`FTP`, :guilabel:`SSH`.

Here are some examples we can find in the target directory of an automation project |_|:

**Example** |_| **1** |_| **: 'yahoo.properties'**

.. code-block:: none
    
    #!http
    squashtest.ta.http.endpoint.url=http://www.yahoo.fr

**Example** |_| **2** |_| **: 'webcalendar_DB.properties'**
   
.. code-block:: none

    #!db
    squashtest.ta.database.driver=com.mysql.jdbc.Driver
    squashtest.ta.database.url=jdbc:mysql://localhost:3306/webcalendar-db
    squashtest.ta.database.username=webcalendar
    squashtest.ta.database.password=squash

|

----

*****************************************************
Foreword about repositories and Targets configuration
*****************************************************

Every repository and target are configured using a ``.properties`` file dropped in the 'repositories' / 'targets' directory of your test project.

Each file will create one instance of the corresponding repository / target.

The name under which they are published in the Test context is the name of their configuration file minus the extension (i.e., if the configuration file is ``myrepo.properties`` then the name in the Test context will be 'myrepo').

Every repository and target in the plugins shipped by default in SKF supports overrides using system properties.

Combined with the configuration file itself, a repository can read its configuration from three levels |_|:

    * 'Prefixed' system properties.
    * The 'Normal' configuration file itself.
    * 'Default' system properties. 

Those level are hierarchical |_|: 'Prefixed' > 'Normal' > 'Default'. The higher level at which a property was found defines the definitive value of that property.

For example, consider a property: 'squashtest.ta.my.property' defined in a file named 'myrepo.properties'. The following ways to set that property are all valid |_|: 

.. list-table::
    :header-rows: 1

    * - level
      - property name
      - declaration location
    * - 'Prefixed'
      - **myrepo.**/squashtest.ta.my.property
      - system properties
    * - 'Normal'
      - squashtest.ta.my.property
      - configuration file
    * - 'Default'
      - squashtest.ta.my.property
      - system properties

As you can see the 'Prefixed' level reads a meta property which simply results from appending the property to the name of the Repository or Target (not the full name of the file, ``myrepo.properties``).

|

----

.. _writing.test.test.ecosystem.context.anchor:

***************************
Test and Ecosystem contexts
***************************

An ecosystem is a succession of test cases which have in common a **SETUP phase** and a **TEARDOWN phase**.

From this, we can figure out two distinctives execution context |_|: 

    * **Test context** 

    * **Ecosystem context**.

The **test context** contains the resources for the duration of one single test. Resources can be retrieved from the test context using its name.

The **ecosystem context** contains the resources for both ecosystem's **SETUP** AND **TEARDOWN** phases.

In other words, resources created in the ecosystem setup are also available in the ecosystem teardown whereas those created during a test case are only available for this test case.

.. figure:: ../_static/writing-tests-ecosystem/ecosystem.jpg

For more information, please read the following :ref:`page <writing.test.ecosystem.anchor>`.

|

----

**********************************
Identifiers (names in the context)
********************************** 

Almost anything in the test context has an identifier. To be valid, an identifier (for a Resource, a Target or an Engine Component) can be composed of |_|: 

    * Any lowercase or uppercase a-z letters.
    * Digits.
    * Dashes '-', underscores '_', dots '.' and slashes '/'.

**Examples of valid identifiers** |_|: my_resource05      Another.Test.File        testfolder/My_File.txt

**Example of invalid identifier** |_|: <-no space allowed $ï¿½ï¿½}{ï¿½# etc <-uncommon characters

You should avoid using any file or folder which does not comply with those rules.

When it comes to Resources, a good practice is to mention in the identifiers what kind of resources they actually are.

For instance, when loading a file, a good name would be ``my_resource.file``. This is not mandatory but it really helps.

In the rest of this document we will commonly use 'name' as a synonymous for 'identifier'.

|

----

****************************
Reserved names / identifiers
****************************

There are two reserved names / identifiers in SKF (for :ref:`context parameters <execution.reporting.context.parameters.anchor>`) |_|:

    * context_script_params
    * context_global_params

Note that the framework doesn't prevent you to define your own SKF resource with those context names. If you ever do it, your context parameters will be overwritten (and a warn is logged).

|

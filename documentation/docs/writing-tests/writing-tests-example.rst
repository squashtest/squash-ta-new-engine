..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _writing.test.example.anchor:

#####################
Example of SKF Script
#####################

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros
.. role:: phases

.. role:: green

.. raw:: html

    <style> .green {color:green} </style>

.. contents:: Contents |_|:
    :local:

|

In this example, we are going to use an SKF script which contains standard instructions and macros. 

A script is a plain text file (with **.ta** extension) containing lists of instructions. All you have to do is to drop it in the 'tests' directory (or anywhere in its sub hierarchy) of your :ref:`project <getting.started.create.new.project.anchor>`.

**Here is the script** |_|:

.. list-table::

        * -     // Step 1 

                :phases:`SETUP :`

                // Step 2

                :keywords:`LOAD` queries/sql/select.sql :keywords:`AS` :resources:`select.file`

                :keywords:`CONVERT` :resources:`select.file` :keywords:`TO` :resource-type:`query.sql` :keywords:`AS` :resources:`query`

                // Step 3 

                :macros:`# LOAD dbunit/resultsets/mytable_simple_select.xml TO XML DATASET expected.dataset`

                // Step 4 

                :phases:`TEST :`

                // Step 5

                :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`query` :keywords:`ON` :resources:`my_database` :keywords:`AS` :resources:`raw_result`

                // Step 6 

                :keywords:`CONVERT` :resources:`raw_result` :keywords:`TO` :resource-type:`dataset.dbunit` :keywords:`AS` :resources:`actual.dataset`

                // Step 7

                :keywords:`ASSERT` :resources:`actual.dataset` :keywords:`IS` :converter-name:`equal` :keywords:`WITH` :resources:`expected.dataset`

*********************************
What does this SKF script do |_|?
*********************************

This script executes an SQL query and compares the result to the content of a DbUnit dataset.

We can notice several elements |_|:

    * Some comments (lines beginning with **'//'**)
    * Phases (:phases:`SETUP` |_| :phases:`:` and :phases:`TEST` |_| :phases:`:`)
    * Instructions (multi-colored lines)
    * Macros (brown lines).

Now we will break down and analyze that script, identify its components and see how they work together.

**SETUP phase declaration** |_|:

.. list-table::

        * - // Step 1

            :phases:`SETUP` |_| :phases:`:`

The :phases:`SETUP` phase groups instructions that will prepare the test. Note that the instructions in that phase could also be set in the main :phases:`TEST` phase.

Differences lie in the handling of test failures |_|: when an instruction fails during the :phases:`SETUP` phase, it means that the script itself is wrong or that the resource is not avalaible. On the other hand, a failure happening during the :phases:`TEST` phase means that the System Under Test (SUT) has a problem.

To sum up, the :phases:`SETUP` phase sets the test prerequisites.

|

------------------------

**Loading an SQL query** |_|:

.. list-table::

        * - // Step 2 

            :keywords:`LOAD` queries/sql/select.sql :keywords:`AS` :resources:`select.file`

            :keywords:`CONVERT` :resources:`select.file` :keywords:`TO` :resource-type:`query.sql` :keywords:`AS` :resources:`query`


This pair of instructions will load a file and declare that it contains an SQL query (in that order). 

|

------------------------

**Loading a DbUnit dataset** |_|: 

.. list-table::

        * - // Step 3

            :macros:`# LOAD dbunit/resultsets/mytable_simple_select.xml TO XML DATASET expected.dataset`


The standard way to load a DbUnit dataset requires the same steps than above |_|: load the file and convert it to make it a DbUnit dataset.

However, instead of explicitly writing the corresponding instructions, SKF proposes a shortcut (macro) to achieve that goal.

The line in the script is not syntax colored because it is a macro and not a standard instruction.

|

------------------------

**TEST phase declaration** |_|: 

.. list-table::

        * - // Step 4

            :phases:`TEST` |_| :phases:`:`



The :phases:`TEST` phase groups the main test instructions. The following instructions will interact with the SUT (System Under Test) and the resources created during the :phases:`SETUP` phase remain available.

If an instruction fails, the test will end and the status displayed in the test result will be set according to the nature of the error.

|

------------------------

**Execution of the query** |_|:

.. list-table::

        * - // Step 5

            :keywords:`EXECUTE` :converter-name:`execute` :keywords:`WITH` :resources:`query` :keywords:`ON` :resources:`my_database` :keywords:`AS` :resources:`raw_result`


We use the query created during :phases:`SETUP` and execute it against the database. The name of the database here is **my_database**.

The resulting data are stored in the context under the name supplied at the end of the instruction (**raw_result**).

|


------------------------

**Data transformation** |_|:

.. list-table::

        * - // Step 6

            :keywords:`CONVERT` :resources:`raw_result` :keywords:`TO` :resource-type:`dataset.dbunit` :keywords:`AS` :resources:`actual.dataset`


In step 3 we prepared the (expected) data, formatted as a DbUnit dataset. If we are to compare the actual data with the expected data, we must first convert the actual data to a suitable type. In this case it must be formatted as a DbUnit dataset.

|

------------------------

**Perform the comparison** |_|:

.. list-table::

        * - // Step 7

            :keywords:`ASSERT` :resources:`actual.dataset` :keywords:`IS` :converter-name:`equal` :keywords:`WITH` :resources:`expected.dataset`


Now we're all set to proceed and test the data against each other. The status of the test will depend on the status of that comparison.

If the comparison fails, the test will be flagged as failed, while if the comparison is a success the script continues to the next instruction.

Here there are no further instruction and the test will terminate with :green:`SUCCESS` status. 

|

----

******************
Some more insights
******************

So now you should have a glimpse of what an SKF script is made of.

There is a :phases:`TEARDOWN` phase too which is optional (just like :phases:`SETUP` phase) but it was not used in this example.

When looking closer at an instruction (e.g. step 5), we can distinguish two kinds of elements |_|: the tokens (red words) and the variable elements (black, blue, pink and yellow words). 

The tokens never change whereas identifiers are variable and refer to elements available in the script, including |_|: 

    * A file or other physical resource (e.g. step |_| 2 |_|: queries/sql/select.sql).
    * The assertion type |_|: a command or a converter (e.g. step |_| 5 |_|: EXECUTE :converter-name:`execute` WITH query ON my_database AS raw_result).
    * The resource type (e.g. step |_| 6 |_|: CONVERT raw_result TO :resource-type:`dataset.dbunit`).
    * The name of a target, a repository, an already loaded resource or a resource to be created (e.g. step |_| 5 |_|: EXECUTE execute WITH :resources:`query` ON :resources:`my_database` AS :resources:`raw_result`).

Basically the tokens tell the engine to execute or activate some components and make them interact with each others.

|

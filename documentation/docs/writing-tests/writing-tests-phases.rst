..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _writing.test.phases.anchor:

.. raw:: html

    <style> .blue {color:blue} </style>

########
Sections
########

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros
.. role:: phases

.. contents:: Contents |_|:
    :local:

|

****************
Test case script
****************

As we have seen in our example, a typical test case script contains 4 sections, including 1 Metadata section and 3 execution phases |_|:

.. list-table::
        :header-rows: 1

        * - Label
          - Occurrence
        * - METADATA |_|:
          - optional
        * - SETUP |_|:
          - optional
        * - TEST |_|:
          - exactly one
        * - TEARDOWN |_|:
          - optional

Note that the space-columns |_|' |_|:' are part of the label. A script must contain exactly one **'TEST** |_| **:'** phase and may contain up to one **'METADATA** |_| **:'** section, and up to one **'SETUP**\ |_|\ **:'** and/or **'TEARDOWN** |_| **:'** phase. The phases may be declared in any order, but the **'METADATA** |_| **:'** section must come first. When a label is declared, no other element is allowed on the same line.

A section begins with a label and ends when another label begins or when the end of file is reached. Instructions must be placed within the boundaries of a section, and any instructions out of a section (mainly when placed before the first label) will cause an error and stop the processing.

.. list-table:: Correct test script structure

    * - :phases:`METADATA :`

        //a comment

        key1 : value1

        key2 : value2
    * - :phases:`SETUP :`

        //another comment

        an instruction

        another instruction
    * - :phases:`TEST :`

        some instructions

        //another comment

        lots of instructions...
    * - :phases:`TEARDOWN :`

        more instructions

        //other comment

        and a last instruction

.. list-table:: Minimal test script structure

    * - :phases:`TEST :`

        An instruction

        Another instruction

        Some more instructions...

|

----

.. _metadata-section:

****************
Metadata section
****************

Declaration
===========
This section can be declared by writing **"METADATA** |_| **:"** in a typical Squash Test script, not in an Ecosystem one.

In fact, a typical Test script may or may not contain a Metadata section. However, this section, if any, must be **unique**.
The Metadata section must also be placed **before** any execution phases (SETUP, TEST or TEARDOWN).

Content
=======
A Metadata section can contain only empty lines, comment lines and, of course :guilabel:`metadata lines`.
While an empty line must contain nothing or only spaces/tabulations and a comment line must start with the symbol '//', a metadata line is always 1 of these 3 types :

    * **key**
    * **key** |_| **:** |_| **value**
    *         **:** |_| **value** (allowed only after a metadata line of second type)

The symbol ':' is the *separator*, used between a metadata key and its value.

.. note::
            Spaces/tabulations between the separator and its key or/and value are not mandatory but **strongly advised**.

Metadata key
------------
A metadata key can contain only **alphanumeric characters, dashes, underscores and dots**, and stops at the first space/tabulation found or at the end of the line.
Moreover, no inline space/tabulation is allowed before a metadata key.

Metadata key is **case insensitive** and must be **unique** in a Test file.

Metadata value
--------------
A metadata value is always placed after a separator ":".
It can contain **alphanumeric characters, dashes, underscores, dots and slashes**, and stops at the first space/tabulation found or at the end of the line.

Metadata value is **case sensitive** and must not be empty (i.e. there must be at least a letter/character after the separator ":").

A metadata value must be assigned with a metadata key. If a key has more than one values, the first value will be written with its key as: "**key** |_| **:** |_| **value**".
Other values will be declared of type |_|: ":guilabel:`[space/tabulation]` |_| **:** |_| **value**".

.. important::
            Between "**key** |_| **:** |_| **value**" line and its following ": |_| **value**" lines, comment lines are allowed, but NOT empty lines.


Example
=======

.. container:: image-container

    .. image:: ../_static/writing-tests-phases/metadata.png
        :alt: Squash Test Metadata section

|

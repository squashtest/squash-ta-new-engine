..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _writing.test.instructions.anchor:

############
Instructions
############

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros
.. role:: phases
.. role:: comments

.. contents:: Contents |_|:
    :local:

|

The valid instruction set is defined as follow |_|: 

.. list-table::

    * - Blank lines (no instruction)

        :comments:`Comments : starting by a double slash '//' (no instruction)`
        
        :keywords:`DEFINE` $(raw data) :keywords:`AS` :resources:`{nameInTheContext<Res:File>}`

        :keywords:`LOAD` {path_To_Resource} [ :keywords:`FROM` :resources:`{resourceLibrary<Rep>}`] [ :keywords:`AS` :resources:`{nameInTheContext<Res:File>}` ] 

        :keywords:`CONVERT` :resources:`{resourceToConvert<Res>}` :keywords:`TO` :resource-type:`{<Cat:Res>}`  ( :converter-name:`{<Conv>}` ) [ :keywords:`USING` :resources:`{config<Res>}` ] :keywords:`AS` :resources:`{convertedResource<Res>}`

        :keywords:`EXECUTE` :converter-name:`{<Cmd>}` :keywords:`WITH` :resources:`{<Res>}` :keywords:`ON` :resources:`{<Tar>}` [ :keywords:`USING` :resources:`{config<Res>}` ] :keywords:`AS` :resources:`{result<Res>}`

        :keywords:`ASSERT` :resources:`{resourceToTest<Res>}` ( :keywords:`IS` | :keywords:`HAS` | :keywords:`DOES` ) :converter-name:`{<Asr>}` [ ( :keywords:`WITH` | :keywords:`THAN` | :keywords:`THE` ) :resources:`{expectedResult<Res>}` ] [ :keywords:`USING` :resources:`{config<Res>}` ]

        :keywords:`VERIFY` :resources:`{resourceToTest<Res>}` (:keywords:`IS` | :keywords:`HAS` | :keywords:`DOES` ) :converter-name:`{<Asr>}` [ ( :keywords:`WITH` | :keywords:`THAN` | :keywords:`THE` ) :resources:`{expectedResult<Res>}` ] [ :keywords:`USING` :resources:`{config<Res>}` ]

.. Note:: 

    The VERIFY instruction is available since |squashTA| |_| **1.6.0**. It's a new type of assertion instruction.

*****************
Syntax convention
*****************

    * :keywords:`Red words` |_|: They represent the language tokens. They are in uppercase and they never change.

    * Black words |_|: They represent a physical resource.

    * :resources:`Blue words` |_|: Identifiers which point to a resource component. They have the following structure |_|: {name<Type:Category_name>} or {name<Type>} or {<Type>} with |_|:

        * **name** |_|: A name which corresponds to the element that should be pointed by the identifier.

        * **Type** |_|: The component type of the element pointed by the identifier |_|: Res for resources, Tar for targets, Repo for repositories.

        * **Category_Name** |_|: The category of the component which wraps the pointed element.

    * :converter-name:`Pink words` |_|: Identifiers which reference an engine component |_|: :converter-name:`{<Cmd>}` for commands, :converter-name:`{<Asr>}` for assertions and :converter-name:`{<Conv>}` for converters.

    * :resource-type:`Yellow word` |_|: The category of the expected resource after a conversion.

    * [ ] |_|: Element inside this square brackets can be omitted in some cases.

.. note::

    For convenience, **name** is often use instead of **identifier** in the documentation.

One instruction per line and one line per instruction. In other words, the end of line means that the instruction ends here and will be parsed as is. The language tokens are case-insensitive and accept inline resource definitions (just like in a :keywords:`DEFINE` instruction, see below). On the other hand the identifier we discussed above are case-sensitive (i.e. you should respect lowercase and uppercase letters).

An instruction can be divided into clauses. Some are mandatory while others are optional. A clause can be recognized by its language token (uppercased words) and an identifier that immediately follows it.

For each instruction the most obvious mandatory clause is the first one that states which instruction you are referring to. This first clause is also named head clause.

The optional clauses are stated here between enclosing brackets '[]'.

.. caution:: Those brackets aren't part of the language and just serve the purpose of delimiting those optional clauses.

Except for the head clause which determines the kind of instruction, the order of other clauses is not fixed.

Also note that the DSL does not support nested instructions.

|

----

********
Comments
********

TA Scripts can contain comments. They start with a '//'. To write a multiline comment, start each line of the comment with the '//'. It's not allowed to write a comment on the same line that an instruction.

**Example of a not allowed comment** |_|:

.. list-table::

    * - :keywords:`LOAD` example.txt :keywords:`AS` :resources:`example.file` :comments:`//This comment is not allowed`

|

----

***********************************************
DEFINE instruction / Inlined instruction $(...)
***********************************************

.. list-table::

    * - :keywords:`DEFINE` $(raw data) :keywords:`AS` :resources:`{nameInTheContext<Res:File>}`

**> Input** |_|:

    * raw data |_|: A string (If there is more than one line, each line must be separate with '\\n')

**> Output** |_|:

    * :resources:`{nameInTheContext<Res:File>}` |_|: The identifier of the resource created in the test context.

The :keywords:`DEFINE` instruction is rarely used but may come handy. Basically it let you define any text content directly within the script, and binds it to a name.

This content will be stored in the Test context as a :guilabel:`file` resource, under the name supplied in :keywords:`AS` clause.

This resource will be available throughout the whole test but won't exist anymore when another test begins.

**Example** |_| **1** |_| **: Simple** :keywords:`DEFINE` **resource**

.. list-table::

    * - :keywords:`DEFINE` $(select * from MY_TABLE) :keywords:`AS` :resources:`query.file`

**Example** |_| **2** |_| **: Structured** :keywords:`DEFINE` **resource**

.. list-table::

    * - :keywords:`DEFINE` $(some letters, a tabulation \t and \n the rest after a linefeed.) :keywords:`AS` :resources:`structured-text.file`

A more common use for resource definition is to simply inline them within the instruction that will use it.

**Example** |_| **: Resource inlined in a** :keywords:`CONVERT` **instruction**

.. list-table::

    * - :keywords:`CONVERT` $(select * from MY_TABLE) :keywords:`TO` :resource-type:`query.sql` :keywords:`AS` :resources:`my_query.query.sql`

The advantage of explicitly using :keywords:`DEFINE` is to bind the newly created :guilabel:`file` resource to a name, thus allowing you to refer to it again later in the script. If you won't need to reuse that resource, an inlined definition is fine.

Inlined resources are notably useful when passing configuration to Engine Components. Engine Components sometimes need a few text to be configured properly, which can be inlined instead of explicitly creating a file for it.

|

----

****************
LOAD instruction
****************

.. list-table::

    * - :keywords:`LOAD` {path_To_Resource} [:keywords:`FROM` :resources:`{resourceRepository<Rep>}`] [:keywords:`AS` :resources:`{nameInTheContext<Res:File>}`]

**> Input** |_|:

    * {path_To_Resource} |_|: The path to the resource to load

    * :resources:`{resourceRepository<Rep>}` |_|: The name of the resource repository in which is located the resource to load.

**> Output** |_|:

    * :resources:`{nameInTheContext<Res:File>}` |_|: The name of the resource created in the test context.

The :keywords:`LOAD` instruction will search for a resource in all of the existing repositories. When it is finally found it will be brought to the test context as a :guilabel:`file` resource. If no AS clause is supplied, the name of this :guilabel:`file` resource will be the name under which it was searched for (including folder hierarchy if it was hidden in a deep file tree).

The path of the resource doesn't need to be a full URL, as that kind of details will be handled by the repositories. In case of a repository looking for the file system it generally have a base directory, you can then omit the full path and only supply a path relative to the base directory.

Also note that the directory separator is a slash '/' regardless of the underlying operating system. More precisely, no backslashes '\\' needed under Windows. Backslashes aren't a valid character for an identifier and will be rejected anyway.

If by chance two or more repositories could answer the query (i.e. if a given file name exists in two file systems, each of them being addressed by a distinct repository), the :guilabel:`file` resource returned depends on which of them replied first. Consider it as random, and if problems happen you could be interested in the :keywords:`FROM` clause (see below).

If the loading fails because the resource was not found, the test will end with a status depending on the phase it was executed in.

The :keywords:`FROM` clause is optional. If specified, instead of searching every repository for the resource it will search only the one you specified. It may speed up file retrieval if some of repositories are very slow or busy.

The :keywords:`AS` clause is optional. If specified, instead of binding the new :guilabel:`file` resource to the name used in the first clause, the engine will bind it to this alias instead.

**Example** |_| **1** |_| **: Simple file loading**

.. list-table::

    * - :keywords:`LOAD` :resources:`data-folder/myfile` :comments:`// that's it, the` :guilabel:`file` :comments:`resource will be accessible under the name 'data-folder/myfile'` 

**Example** |_| **2** |_| **: Load with alias**

.. list-table::

    * - :keywords:`LOAD` long/path/to/the/resource :keywords:`AS` :resources:`my_resource.file` 

**Example** |_| **3** |_| **: Load from a specific repository**

.. list-table::

    * - :keywords:`LOAD` myfile :keywords:`FROM` :resources:`my.repository` 

|

----

*******************
CONVERT instruction
******************* 

.. list-table::

    * - :keywords:`CONVERT` :resources:`{resourceToConvert<Res>}` :keywords:`TO` :resource-type:`{<Cat:Res>}` ( :converter-name:`<Conv>` ) [ :keywords:`USING` :resources:`{config<Res>}` ] :keywords:`AS` :resources:`{convertedResource<Res>}`

**> Input** |_|:

    * :resources:`{resourceToConvert<Res>}` |_|: The name of the resource to convert
    * :resource-type:`{<Cat:Res>}` |_|: The category of the resource expected after the conversion.
    * :converter-name:`<Conv>` |_|: The category of the converter used for the conversion.
    * :resources:`{config<Res>}` |_|: The name of the complementary resource needed for the conversion.

**> Output** |_|:

    * :resources:`{convertedResource<Res>}` |_|: The name of the converted resource.

The :keywords:`CONVERT` instruction will take an input resource and produce a new resource, that will then be available under the name mentioned in the :keywords:`AS` clause. The resource must exist in the Test context beforehand (for instance as resulting from a :keywords:`LOAD` instruction).

Remember that no Engine Component will ever modify the input resource, and it will still be available as it was after the conversion is over.

Depending on the invoked converter, a :keywords:`CONVERT` instruction will perform at least one of the two operations |_|: 

    * Produce a resource with the same data than the input resource but wrapped in a different category.

    * Produce a resource with new data based on the input resource but the category stays the same.

Some converters do even both. In any case you should refer to the documentation of this converter.

The :keywords:`TO` clause is mandatory, as it is where you specify the category of the output (which may be the same than the category of the input resource).

However in some cases, it may happen that two or more converters, accepting the same input and output categories, exist together in the engine, thus leading to an error.

In such cases one should deambiguate the situation by specifying which specific converter you need.

This is the only case where you need to expand the full signature of that converter. You can specify that converter by immediately appending its name to the output category, surrounded by parenthesis **( )**.

.. warning :: 

    Even in the cases where you don't need to specify the converter name, we highly advise you to do it.

    Indeed this could prevent you from encountering problems if a new converter with the same input and output is created (making mandatory to specify the converter category).

The optional :keywords:`USING` clause lets you specify an arbitrary number or resources that will be treated as configuration for this operation. The category of resources, or which informations they should convey depends on the converter being used. Having a look at the documentation of that converter is certainly useful.

**Example** |_| **1** |_| **: Simple** :keywords:`CONVERT` **from file to CSV**

.. list-table::

    * - :keywords:`CONVERT` :resources:`mydata.file` :keywords:`TO` :resource-type:`csv` :keywords:`AS` :resources:`mydata.csv` 

**Example** |_| **2** |_| **:** :keywords:`CONVERT` **with configuration**

.. list-table::

    * - :keywords:`CONVERT` :resources:`my_result.resultset` :keywords:`TO` :resource-type:`dataset.dbunit` :keywords:`USING` $(tablename : MY_TABLE) :keywords:`AS` :resources:`mydata.csv`

**Example** |_| **3** |_| **:** :keywords:`CONVERT` **with an inline definition**

.. list-table::

    * - :keywords:`CONVERT` $(select * from MY_TABLE) :keywords:`TO` :resource-type:`query.sql` (:converter-name:`query`) :keywords:`AS` :resources:`my_query.query.sql`

|

---- 

*******************
EXECUTE instruction 
*******************

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`{<Cmd>}` :keywords:`WITH` :resources:`{<Res>}`` :keywords:`ON` :resources:`{<Tar>}` [ :keywords:`USING` :resources:`{config<Res>}` ] :keywords:`AS` :resources:`{result<Res>}`

**> Input** |_|:

    * :converter-name:`{<Cmd>}` |_|: The command to execute.
    * :resources:`{<Res>}` |_|: The name of the resource to use with the command.
    * :resources:`{<Tar>}` |_|: The name of the target.
    * :resources:`{config<Res>}` |_|: The name of the complementary resource needed to use with the command.

**> Output** |_|:

    * :resources:`{convertedResource<Res>}` |_|: The name of the resource generated by the command.

The :keywords:`EXECUTE` instruction will perform an operation involving a resource (:keywords:`WITH` clause), on a given target (:keywords:`ON` clause). The result of this operation, if any, will be returned as a resource published in the Test context under the name supplied in the :keywords:`AS` clause.

If the operation returns some results, the actual type of the resulting resource depends on the command being executed, so you should refer to the documentation of that command to know how to handle it in the rest of the test.

The optional :keywords:`USING` clause lets you specify an arbitrary number of resources that will be treated as configuration for this operation. The category of resources, or which informations they should convey depends on the command being used.

You MUST provide an input resource, a target and an alias for the result, even if the command does not actually use all of theses features.

**Example** |_| **1** |_| **: Command using a dummy identifier for the result name (because that command doesn't return any)**

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`put` :keywords:`WITH` :resources:`my_file.file` :keywords:`ON` :resources:`my_ftp` :keywords:`AS` :resources:`no_result_anyway` 

**Example** |_| **2** |_| **: Command with configuration**

.. list-table::

    * - :keywords:`EXECUTE` :converter-name:`get` :keywords:`WITH` $() :keywords:`ON` :resources:`my_ftp` :keywords:`USING` $(remotepath : data/the-file.txt, filetype : ascii) :keywords:`AS` :resources:`my_new_file.file`

Note that in the last example we used a dummy inlined resource $(), since in that case the **get** command doesn't use any input resource.

|

----

.. _assertion.instruction.anchor:

****************************************
Assertion instructions (ASSERT / VERIFY)
****************************************

.. list-table::

    * - :keywords:`ASSERT` :resources:`{resourceToTest<Res>}` ( :keywords:`IS` | :keywords:`HAS` | :keywords:`DOES` ) :converter-name:`{<Asr>}` [ ( :keywords:`WITH` | :keywords:`THAN` | :keywords:`THE` ) :resources:`{expectedResult<Res>}` ] [ :keywords:`USING` :resources:`{config<Res>}` ]

        :keywords:`VERIFY` :resources:`{resourceToTest<Res>}` ( :keywords:`IS` | :keywords:`HAS` | :keywords:`DOES` ) :converter-name:`{<Asr>}` [ ( :keywords:`WITH` | :keywords:`THAN` | :keywords:`THE` ) :resources:`{expectedResult<Res>}` ] [ :keywords:`USING` :resources:`{config<Res>}` ]

**> Input** |_|:

    * :resources:`{resourceToTest<Res>}` |_|: The name of the resource to validate.
    * :converter-name:`{<Asr>}` |_|: The kind of assertion to use.
    * :resources:`{expectedResult<Res>}` |_|: The name of the reference resource.
    * :resources:`{config<Res>}` |_|: The name of the complementary resource needed for the assertion.

The assertion instructions will perform a test on the supplied resource, optionally compared to another resource.

If the assertion is successful, the test will continue.

If the assertion failed or finished in error |_|:

    * In :keywords:`ASSERT` mode, the execution of the current test phase is stopped. The teardown test phase is then executed (if it was not already in this teardown test phase).

    * In :keywords:`VERIFY` mode, the next instructions is executed.

The test final status will always be the most severe status of its instructions.

For details on the execution workflow and test status please see :ref:`this page <execution.reporting.squash.tf.workflow.anchor>`.

The :keywords:`VERIFY` assertion mode is available since |squashTA| |_| **1.6.0**. Before only the :keywords:`ASSERT` mode was available.

Note that, unlike other instructions, an assertion can have multiple choices.

The first multi-token clause is the one identifying the assertion (:converter-name:`{<Asr>}`, in the syntax above).

The second one is identifying the secondary resource (:resources:`{expectedResult<Res>}`, in the syntax above).

In either case you only need to pick one, and it makes sense to pick the one that fits the most to the grammar of the instruction (see examples below).

The optional (:keywords:`WITH` | :keywords:`THAN` | :keywords:`THE`) clause specifies another resource. In that case, the primary resource will be compared to the secondary resource.

If that clause is used, then we talk of a 'binary assertion', and the primary resource usually represents the actual result from the SUT while the secondary result represents the expected result.

If no (:keywords:`WITH` | :keywords:`THAN` | :keywords:`THE`) clause is used, the resource and the assertion are assumed self-sufficient to perform the check. We then talk of a 'unary assertion'.


The optional :keywords:`USING` clause let you specify an arbitrary number or resources that will be treated as configuration for this operation. The category of resources, or which information they should convey depends on the assertion being used, so having a look at the documentation of that assertion is certainly useful.

**Example** |_| **1** |_| **: Simple unary assertion**

.. list-table::

    * - :keywords:`ASSERT` :resources:`my_result.result.sahi` :keywords:`IS` :converter-name:`success`

**Example** |_| **2** |_| **: Simple binary assertion (awkward)**

.. list-table::

    * - :keywords:`ASSERT` :resources:`actual_result.dataset.dbunit` :keywords:`IS` :converter-name:`contain` :keywords:`WITH` :resources:`expected_result.dataset.dbunit`

In this example, the sentence is grammatically wrong but it will work as expected. You might prefer the following syntax |_|: 

**Example** |_| **3** |_| **: Simple binary assertion (better)**

.. list-table::

    * - :keywords:`ASSERT` :resources:`actual_result.dataset.dbunit` :keywords:`DOES` :converter-name:`contain` :keywords:`THE` :resources:`expected_result.dataset.dbunit`

This version does exactly the same thing but is better.

|

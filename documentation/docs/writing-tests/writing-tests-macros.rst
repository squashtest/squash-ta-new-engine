..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _writing.test.macros.anchor:

######
Macros
######

.. role:: keywords
.. role:: resources
.. role:: resource-type
.. role:: converter-name
.. role:: macros
.. role:: phases
.. role:: comments

.. contents:: Contents |_|:
    :local:

|

The basic instructions set covers your need for writing working scripts, but they might be quite verbose. If you find yourself writing the same group of instructions again and again, you will probably find a use for shortcuts (or macros).

A certain number of shortcuts are defined natively in the SKF modules.

|

***********************************
What does a shortcut look like |_|?
***********************************

A shortcut is simply a sequence of instructions put in a separate file, that defines a hook of your choice that will be replaced by a set of instructions defined below. The syntax is the following |_|:

.. list-table::

    * - :macros:`# <macro expression>`

        =>

        <instruction 1>

        <instruction 2>

        …

        <instruction n>

The part above the **=>** separator is the **hook** and the part below is the **expansion**.

Spacing characters don't matter |_|: you may put any spaces or tabulations between every word, either in the hook or in the expansion, or before and after the separator.

**Please respect the following rules** |_|:

* The hook must |_|:

    * Hold in one single line.
    * Be the first line of the shortcut file.
    * Have a space between **#** and **<macro** |_| **expression>**. This is mandatory.

* After the **hook**, the next line must immediately have the separator **=>**.

* The **expansion** must comply to the rules of the basic instruction set (one instruction per line, etc...).

|

----

*************************
How do I use a macro |_|?
*************************

The file you just created must land in the ``shortcuts`` directory (or its subdirectories) of your test project, and the filename must end with a '.macro' extension.

You can now write regular tests using your shortcut just like any other basic instruction. You don't have to respect your hooks to the letter |_|: lowercase and uppercase characters are matched equally, and you may put any extra spaces you like. When your script is executed, any hook encountered will be replaced by the corresponding expansion.

**Example** |_| **: load 'myfile.xml' and convert it to type 'dataset.dbunit' under the name 'mydataset.dataset'**

.. list-table::

    * - :macros:`# load my favourite dataset`

        => 

        :comments:`//remember that, although the original extension of`

        :comments:`//the file is .xml, as a resource in the Test context,`

        :comments:`//its initial type is 'file', not 'xml'` 

        :keywords:`LOAD` myfile.xml :keywords:`AS` :resources:`myfile.file`

        :keywords:`CONVERT` :resources:`myfile.file` :keywords:`TO` :resource-type:`xml` :keywords:`AS` :resources:`myfile.intermediate.xml`

        :keywords:`CONVERT` :resources:`myfile.intermediate.xml` :keywords:`TO` :resource-type:`dataset.dbunit` :keywords:`AS` :resources:`mydataset.dataset`

**Macro usage** |_|:

.. list-table::

    * - :macros:`# load my favourite dataset`

|

----

**********************
Variabilized shortcuts
**********************

You can define variables in the hook and use them in the expansion. When a variable is defined in the hook it won't be literally matched, the hook will match solely on the basis of the other tokens. The variables defined in the hook may then be used in the expansion. Variables are declared within curly braces '{}', and are used as follow |_|:

**Example** |_| **: Definition of the macro**

.. list-table::

    * - :macros:`# LOAD {file_name} TO XML DATASET {converted_name}`

        => 

        :keywords:`LOAD` {file_name} :keywords:`AS` :resources:`data.file`

        :keywords:`CONVERT` :resources:`data.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`data.file`

        :keywords:`CONVERT` :resources:`data.xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`{converted_name}`

**Macro usage** |_|:

.. list-table::

    * - :macros:`# LOAD foo.xml TO XML DATASET foo.dataset`

**Corresponding instructions** |_|:

.. list-table::

    * - :keywords:`LOAD` foo.file :keywords:`AS` :resources:`data.file`

        :keywords:`CONVERT` :resources:`data.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`data.file`

        :keywords:`CONVERT` :resources:`data.xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`foo.dataset`

At some point you will probably have to create temporary variables, and thus have to worry about possible conflicting Resource identifiers. 

Thankfully there is a mechanism of randomization that helps to tackle the problem, using a special expression in the extension that will generate a random number between -32768 and 32767.

It looks like this |_|: 

.. list-table::

    * - **{%%whatever}**, where **whatever** is a string of your choice.

When the expression **{%%whatever}** is used in a resource name inside a macro, it's replaced by a string dynamically generated.

If an identical expression **{%%whatever}** is used several times inside a macro, it's replaced each time with the same value.

If two different expressions **{%%whatever}** are used inside a macro (for example **%%data1** and **%%data2**), they're replaced by two different values.

When a script is processed and matches the hook, the variables will be remembered and replaced at their corresponding place in the expression, and placeholders will be filled as well.

Let's rewrite the previous example |_|: 

**Example** |_| **: The same shortcut than above, with variables**

.. list-table::

    * - :macros:`# LOAD {file_name} TO XML DATASET {converted_name}`

        => 

        :keywords:`LOAD` {file_name} :keywords:`AS` :resources:`__{%%data1}.file`

        :keywords:`CONVERT` :resources:`__{%%data1}.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`result{%%data2}.xml`

        :keywords:`CONVERT` :resources:`result{%%data2}.xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` {converted_name}

**Macro usage** |_|:

.. list-table::

    * - :macros:`# LOAD foo.xml TO XML DATASET foo.dataset`

**Corresponding instructions** |_|:

.. list-table::

    * - :keywords:`LOAD` foo.file :keywords:`AS` :resources:`__354.file`

        :keywords:`CONVERT` :resources:`__354.file` :keywords:`TO` :resource-type:`xml` (:converter-name:`structured`) :keywords:`AS` :resources:`result6345.xml`

        :keywords:`CONVERT` :resources:`result6345.xml` :keywords:`TO` :resource-type:`dataset.dbunit` (:converter-name:`dataset`) :keywords:`AS` :resources:`foo.dataset`

|

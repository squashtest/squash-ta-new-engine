..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _writing.test.ecosystem.anchor:

#########
Ecosystem
#########

.. contents:: Contents |_|:
    :local:

|

************
Introduction
************

Let's introduce the ecosystem notion in SKF. An ecosystem is a suite of test cases which have in common a setup phase and a teardown phase.

It allows to prepare the environment for a specific bunch of test cases and doing so for as many ecosystems as you need.

Each ``tests`` directory of an SKF project and its subdirectories correspond to an ecosystem as soon as they contain test cases.

The name of the ecosystem (Such as it will appear in the execution reports) is determined by the name of the directory which contains it.

The ecosystem directory contains |_|:

        * **A setup.ta file** (optional) |_|: To prepare the environement for the bunch of test cases included in the ecosystem.
        * **From 1 to N test files (<test_name>.ta)** |_|: Each file corresponding to one test case.
        * **A teardown.ta file** (optional) |_|: To clean-up the environement after the execution of all test cases included in the ecosystem.

An ecosystem execution takes place in 3 ordered steps (Independant of the order in the directory) |_|:

    1. A setup phase where the SKF script **setup.ta** is executed (if present). This phase is executed only once.
    2. A test cases execution phase during which each test case is executed one after the other.
    3. A teardown phase where the SKF script **teardown.ta** is executed (if present). This phase is executed only once.

Here is an example with 5 different ecosystems in a |squashTF| automated project |_|:

.. figure:: ../_static/writing-tests-ecosystem/squash-ta-ecosystem.jpg

|

----

********
Workflow
********

First there is an initialization of contents of the following directories |_|: targets, repositories and shortcuts. At this step the different elements of those directories are verified. If everything is ok these elements are available for the full duration of the execution, therefore for all different ecosystems to execute.

Afterwards comes the execution of the different ecosystems. Each of them with its own :ref:`ecosystem context <writing.test.test.ecosystem.context.anchor>` and for each test case its own :ref:`test context <writing.test.test.ecosystem.context.anchor>`.

Whatever status an ecosystem has after its execution, the next ecosystem of the test suite is launched.

Once all ecosystems have been executed a cleaning-up step occurs, and finally the publication of the execution report available in the 'target' directory.

Here is a schema of the SKF workflow |_|:

.. figure:: ../_static/writing-tests-ecosystem/workflow-squashtf.jpg

|

----

***********************************
Ecosystems Setup & Teardown scripts
***********************************

As said before, an ecosystem is a suite of test cases which have in common a setup phase and a teardown phase. An ecosystem may - |_| but is not required to |_| - define up to one setup script and one teardown script. They obey to the same rules than regular test scripts, except two points |_|:

    * They don't care about phases |_|: A setup or teardown script only contains instructions, no phase should be defined.

    * Regular resource names have a special context |_|: the ecosystem context, which is common to BOTH setup and teardown ecosystem script and which is INDEPENDANT (in term of resources) of the test cases included in the ecosystem.

Note that unlike other tests (that may be named freely), setup and teardown scripts MUST be named respectively **'setup.ta'** and **'teardown.ta'**.

**Example** |_| **: valid setup/teardown script**

.. code-block:: none

    * - //that's right, no phase needs to be defined
        //instructions are directly written as follow :

        instruction 
        instruction
        instruction

An example of project containing ecosystems setup and teardown scripts |_|:

.. figure:: ../_static/writing-tests-ecosystem/project-ecosystems-teardown.jpg

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _writing.test.engine.components.anchor:

#################
Engine Components
#################

.. contents:: Contents |_|:
    :local:

|

Scripts are written using a DSL (Domain Specific Language) that provides very basic ways to invoke parts of the SKF engine. Those parts are known as the Engine Components and are divided in three categories |_|: Converters, Commands and Assertions.

|

**********
Converters
**********

They will help you to modify your resources. Usually a converter will handle one task between the following |_|:

    * To modify the content (aka data) of a resource. 

        * **For example** |_|: To modify the content of a text file.

    * To modify the wrapper (aka type) of a resource. 

        * **For example** |_|: To convert a text file to a SQL batch.

In either case the resulting resource will be a new resource. Both the previous and the new version exist and are available in the Test Context once the operation is done.

|

----

********
Commands
********

A command will perform an operation against a Target, usually using a resource. For instance, execute an SQL query against a database, send a file to a FTP etc.

If the operation carries some results they will be brought back to the Test context as resources.

|

----

**********
Assertions
**********

An assertion is a check ran on resource(s). Sometimes the tested resources carry all the information you need to test. In this case we speak of unary assertion.

On the other hand, when you need to compare one resource to another, we speak of binary assertion.

A successful assertion (i.e. the tested resource(s) match(es) the expected criteria) will let the script continue. A failed assertion will stop the script and report the error.

|

----

**************************************
Common behavior of an Engine Component
**************************************

As resources are immutable, an engine component will never modify a resource. If any new data/result is produced, it will be brought in as a new resource.

An engine component is identified by its name AND by the type of resources and/or targets it manipulates.

For example, let's consider a command named **"put"** that uses SQL batches against databases, and a second command also named **"put"** that uploads files to an FTP.

At runtime when the engine reads an instruction, depending on the name of command, resource and target, it will decide which operation will be ran.

An engine component may take additional resources that will tweak its behavior. Those (mostly optional) resources are called configuration (or parameters).

**Example** |_|:

Let’s imagine you want to upload multiple files to an FTP (e.g. using the command **"put"** above) using the defaults settings associated to the FTP Target. For one of those files, you need to override the settings. 

In this case the **"put"** command allows you to mention another resource carrying those overridden values, and will use them for this specific case only.

|

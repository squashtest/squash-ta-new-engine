// call the library : https://bitbucket.org/nx/std-builds-library
@Library('std-builds-library') _

pipeline {
    agent {
        kubernetes {
            label 'maven-builder-jdk8'
            defaultContainer 'maven'
        }
    }

    environment {
        JAVA_TOOL_OPTIONS = "-Duser.timezone=Europe/Paris -Duser.language=FR -Duser.country=FR -Duser.variant=FR"
        HOME = "${WORKSPACE}"
    }
    parameters{
        choice(
                name : 'build_type',
                description : 'Choose between regular or release build',
                choices: ["regular", "release"]
        )

        string(
                name: "release_version",
                description: "the release version (if release)",
                defaultValue : ""
        )

        string(
                name: "next_version",
                description: "the next development version (if release)",
                defaultValue : ""
        )
    }    

    stages {
        stage('install dependencies') {
            steps {
                //installing sahi outside of the project folder
               dir('/home/jenkins/agent/workspace/'){
                    echo 'installation des packets'
                    sh 'apt-get install wget iputils-ping -y'
                    
                    echo 'preparation installation sahi'
                    sh '''
                    wget http://sahipro.com/static/builds/pro/install_sahi_pro_v600_20141015.jar
                    cat > silent_install.xml << EOF 
                    <AutomatedInstallation langpack="eng">
                    <com.izforge.izpack.panels.HelloPanel/>
                    <com.izforge.izpack.panels.HTMLInfoPanel/>
                    <com.izforge.izpack.panels.LicencePanel/>
                    <com.izforge.izpack.panels.TargetPanel>
                        <installpath>sahi_pro</installpath>
                    </com.izforge.izpack.panels.TargetPanel>
                    <com.izforge.izpack.panels.PacksPanel>
                        <pack name="Sahi Core" index="0" selected="true"/>
                        <pack name="Tools" index="1" selected="true"/>
                        <pack name="Upgrade" index="2" selected="true"/>
                        <pack name="Sample Java Project" index="3" selected="true"/>
                        <pack name="Ruby" index="4" selected="true"/>
                        <pack name="Editor support files" index="5" selected="true"/>
                    </com.izforge.izpack.panels.PacksPanel>
                    <com.izforge.izpack.panels.InstallPanel/>
                    <com.izforge.izpack.panels.ShortcutPanel/>
                    <com.izforge.izpack.panels.ProcessPanel/>
                    <com.izforge.izpack.panels.FinishPanel/>
                </AutomatedInstallation>
                EOF
                '''
                echo 'installation sahi'
                sh '''
                java -jar install_sahi_pro_v600_20141015.jar silent_install.xml
                cd sahi_pro/userdata/bin/
                ./start_sahi.sh >> ../logs/console.log 2>&1 &
                echo $! > "${WORKSPACE}/sahi.pid"
                
                '''   
               }
                
            }
        }        
        stage('Build') {
            when{
                expression { params.build_type == 'regular' }
            }            
            steps {

                sh "mvn -B -ntp -Pci org.jacoco:jacoco-maven-plugin:prepare-agent deploy"
                publishHTML target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'test/squash-ta-self-contained-integration-tests/functionalKOtests/target/squashTF/html-reports/',
                    reportFiles: 'squash-tf-report.html',
                    reportName: 'Self-contained-Integration-Tests-functionalKOtests-Report'
                ]   
                publishHTML target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'test/squash-ta-self-contained-integration-tests/target/squashTF/html-reports/',
                    reportFiles: 'squash-tf-report.html',
                    reportName: 'Self-contained-Integration-Tests-Report'
                ]                     

            }
        }
        stage('QA'){
                when{
                    expression { params.build_type == 'regular' }
                }            
            steps {
                withSonarQubeEnv('SonarQube'){
                    sh 'mvn -Dsonar.scm.disabled=true sonar:sonar'
                }
                timeout(time: 10, unit: 'MINUTES'){
                    waitForQualityGate abortPipeline: true
                }
            }
            post {
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
            }             
        }

        stage('release'){
            when{
                expression { params.build_type == 'release' }
            }
            steps {
                container('maven') {
                    script {

                        def valid = release.isConfValid()
                        if (!valid) {
                            error('Cannot release : either the release version or next dev version is wrong')
                        }
                        //option -DuseReleaseProfile=false is used to help to release:perform not to upload sources twice during execution of maven-deploy-plugin
                        //actuellement is deprecated. The release profile will be removed from future versions of the super POM
                        echo "releasing : "
                            sh """mvn -B -ntp release:clean release:prepare release:perform -Darguments=" -Dmaven.javadoc.skip=true" \
                                -DuseReleaseProfile=false \
                                -Dmaven.source.skip \
                                -Dresume=false \
                                -DreleaseVersion=${params.release_version} \
                                -DdevelopmentVersion=${params.next_version}  \
                                -Pci,public"""
                        echo "release complete !"

                    }
                }
            }
            post{
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
                success {
                    script {
                        release.addReleaseInfo(this)
                        release.push()
                    }
                }
            }
        }
    }
}
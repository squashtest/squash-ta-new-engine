/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.squashtest.ta.backbone.engine.AssertionFindSettings;
import org.squashtest.ta.backbone.engine.impl.AssertionFindSettingsImpl;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.CannotApplyAssertionException;
import org.squashtest.ta.backbone.exception.IncompatibleNaturesException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.test.DefaultResourceMetadata;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata.ResourceRole;

public class DefaultBinaryAssertionRunner extends
		AbstractDefaultInstructionRunner {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBinaryAssertionRunner.class); 
	
	private BinaryAssertionInstruction instruction;
	
	private static final String MISSING_ACTUAL_RESULT_NAME_ERROR = "the name of the actual result is missing";
	private static final String MISSING_EXPECTED_RESULT_NAME_ERROR = "the name of the expected result is missing";
	private static final String MISSING_ASSERTION_NAME_ERROR = "the identifier of the assertion is missing";
	private static final String GENERIC_ERROR_MESSAGE = "Cannot apply assertion : ";
	
	public DefaultBinaryAssertionRunner(BinaryAssertionInstruction instruction) {
		this.instruction=instruction;
	}

	@Override
	protected void checkSettings() {
		if (instruction.getActualResultName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_ACTUAL_RESULT_NAME_ERROR);
		}
		
		if (instruction.getExpectedResultName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_EXPECTED_RESULT_NAME_ERROR);
		}
		
		if (instruction.getAssertionCategory()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_ASSERTION_NAME_ERROR);
		}
	}

	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}

	@Override
	protected void doRun() {
	
		ResourceWrapper actualResult = getActualResult();
		ResourceWrapper expectedResult = getExpectedResult();
		
		BinaryAssertionHandler assertion = setupAssertion(actualResult, expectedResult);
		try{
			//apply the assertion
			assertion.test();
		}catch(BinaryAssertionFailedException afe){
			if(afe.getActual()==null || afe.getExpected()==null){
				BinaryAssertionFailedException completeReport=new BinaryAssertionFailedException(afe.getMessage(), expectedResult.unwrap().copy(), actualResult.unwrap().copy(), afe.getFailureContext());
				completeReport.initCause(afe);//so sad Mr Checkstyle shuns initCause()... 
				throw completeReport;//NOSONAR
			}else{
				throw afe;
			}
		}
				
	}
	
	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		ResourceWrapper actualResult = testRunner.getResourceFromCache(instruction.getActualResultName());
		if (actualResult != null) {
			ResourceAndContext actualRac = new ResourceAndContext();
			actualRac.setResource( actualResult.unwrap().copy());

			DefaultResourceMetadata actualMetadata = new DefaultResourceMetadata(
					actualResult);
			actualMetadata.setRole(ResourceRole.ACTUAL_RESULT);
			actualRac.setMetadata (actualMetadata);
			executionDetails.addResourceAndContext(actualRac);
		}
		
		ResourceWrapper expectedResult = testRunner
				.getResourceFromCache(instruction.getExpectedResultName());
		if (expectedResult != null) {
			ResourceAndContext expectedRac = new ResourceAndContext();
			expectedRac.setResource (expectedResult.unwrap().copy());

			DefaultResourceMetadata expectedMetadata = new DefaultResourceMetadata(
					expectedResult);
			expectedMetadata.setRole(ResourceRole.EXPECTED_RESULT);
			expectedRac.setMetadata(expectedMetadata);

			executionDetails.addResourceAndContext(expectedRac);
		}
	}
	
	/* *********** private stuffs *************** */
	
	private ResourceWrapper getActualResult(){
		return fetchResourceOrFail(instruction.getActualResultName());
	}
	
	private ResourceWrapper getExpectedResult(){
		return fetchResourceOrFail(instruction.getExpectedResultName());
	}
	
	private AssertionFindSettings buildSettings(ResourceWrapper actualResource, ResourceWrapper expectedResource){
		
		AssertionFindSettings settings = new AssertionFindSettingsImpl();
		settings.setActualResultNatureName(actualResource.getNature().getName());
		settings.setExpectedResultNatureName(expectedResource.getNature().getName());
		settings.setAssertionCategory(instruction.getAssertionCategory());

		return settings;
	}

	//if an assertion is not found because the settings contains unknown natures or identifier,
	//if doesn't mean the operation fail. The diagnostic will be run later.
	private BinaryAssertionHandler safeFindAssertion(AssertionFindSettings settings){
            BinaryAssertionHandler binaryAssertionHandler = null;
            Collection<BinaryAssertionHandler> found = testRunner.getContextManager().getBinaryAssertion(settings);
            if (found.size()==1){
                    binaryAssertionHandler = found.iterator().next();
            }
            return binaryAssertionHandler;
	}
	
	//will attemps to get the assertion. In case it's impossible because of incompativle natures, if the test runner is set to autoconvert
	//it will retry with optimistic conversion.
	private BinaryAssertionHandler setupAssertion(ResourceWrapper actualResource, ResourceWrapper expectedResource){
            try{
                AssertionFindSettings settings = buildSettings(actualResource, expectedResource);
                BinaryAssertionHandler assertion;

                assertion = safeFindAssertion(settings);

                if (assertion!=null){
                        assertion.setActualResult(actualResource);
                        assertion.setExpectedResult(expectedResource);
                        Collection<ResourceWrapper> conf = fetchConfiguration(instruction.getAssertionConfiguration());
                        assertion.addConfiguration(conf);
                        return assertion;
                }else{
                        throw diagnoseError(actualResource, expectedResource, instruction.getAssertionCategory());
                }
            }catch(ResourceNotFoundException ex){
                BrokenTestException error=diagnoseError(actualResource, expectedResource, MISSING_ASSERTION_NAME_ERROR);
                error.initCause(ex);
                throw error;
            }
	}
	
	//basically, the question is : was absolutely no assertion found, or was the user assertion definition ambiguous ? 
	private BrokenTestException diagnoseError(ResourceWrapper actualResult, ResourceWrapper expectedResult, String assertionName){
		
		AssertionFindSettings settings = buildSettings(actualResult, expectedResult);
		settings.setActualResultNatureName(null);
		settings.setExpectedResultNatureName(null);
		Collection<BinaryAssertionHandler> assertions = null;
		
		assertions = testRunner.getContextManager().getBinaryAssertion(settings);
		
		if (!assertions.isEmpty()){
			return buildIncompatibleNatureException(actualResult, expectedResult, assertionName);
		}
		else{
			return buildCannotApplyAssertionException(assertionName);
		}
	}

	
	private BrokenTestException buildIncompatibleNatureException(ResourceWrapper actual, ResourceWrapper expected, String assertionName){
		ResourceName actualName = actual.getName();
		ResourceName expectedName = expected.getName();
		String actNatureName = actual.getNature().getName();
		String expNatureName = expected.getNature().getName();
		
		FormattingTuple tuple = MessageFormatter.arrayFormat("Cannot apply binary assertion : assertion '{}' found but cannot handle resource '{}' and " +
				"resource '{}' together. This is probably due to incompatible natures : '{}' is of nature '{}' and '{}' is of nature '{}', and no assertion in the context is able " +
				"to handle those natures and all conversion attempts failed. If needed, consider explicit conversion and deactivate" +
				" auto conversion.", new Object[]{assertionName, actualName, expectedName, actualName, actNatureName, expectedName, expNatureName});
		
		
		LOGGER.error(tuple.getMessage());
		
		return new IncompatibleNaturesException(tuple.getMessage());		
		
	}
	
	
	private BrokenTestException buildCannotApplyAssertionException(String assertionName){
		CannotApplyAssertionException ex = new CannotApplyAssertionException("Cannot apply assertion : assertion'"+assertionName+"'(binary) not found.");
		LOGGER.error(ex.getMessage());
		return ex;		
	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		return GENERIC_ERROR_MESSAGE;
	}

}

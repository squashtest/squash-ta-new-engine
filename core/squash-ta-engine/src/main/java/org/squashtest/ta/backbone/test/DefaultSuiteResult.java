/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.ResultPart;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;

/**
 * Default implementation for the {@link SuiteResult} interface. 
 * @author bsiri
 **/
public class DefaultSuiteResult implements SuiteResult {

	/** The suite name */
	private String suiteName;
	
	/** The groupId of the project which contains the test suite */
	private String projectGroupId;
	
	/** The artifactId of the project which contains the test suite */
	private String projectArtifactId;
	
	/** The version of the project which contains the test suite */
	private String projectVersion;
	
	/** The list of ecosystem result. One for each ecosystem of the test suite run */
	private List<EcosystemResult> ecosystemResults = new ArrayList<>();
	
	/** The list of target initialisation result. One for each target we try to initialise */
	private Set<TargetInitialisationResult> targetInitialisationResult = new HashSet<>();

	
	/**
	 * Constructor with mandatory testSuiteName initialization.
	 * 
	 * @param testSuiteName
	 */
	public DefaultSuiteResult(String testSuiteName) {
		if (testSuiteName == null) {
			throw new IllegalArgumentException("null suite name forbidden");
		}
		suiteName = testSuiteName;
	}

	/**
	 * Add a {@link EcosystemResult} to this {@link DefaultSuiteResult}.
	 * 
	 * @param result
	 *            the {@link EcosystemResult} to add.
	 */
	public void addTestEcosystemResult(EcosystemResult result) {
		ecosystemResults.add(result);
	}

	@Override
	public String getName() {
		return suiteName;
	}

	@Override
	public Date startTime() {
		Date minDate = new Date();
		for (EcosystemResult result : ecosystemResults) {
			if (minDate.compareTo(result.startTime()) > 0) {
				minDate = result.startTime();
			}
		}
		return minDate;
	}

	@Override
	public Date endTime() {
		Date maxDate = new Date();
		for (EcosystemResult result : ecosystemResults) {
			if (maxDate.compareTo(result.endTime()) < 0) {
				maxDate = result.endTime();
			}
		}
		return maxDate;
	}

	@Override
	public int getTotalTests() {
		int i=0;
		for(EcosystemResult r:ecosystemResults){
			i+=r.getTotalTests();
		}
		return i;
	}

	@Override
	public int getTotalSuccess() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalSuccess();
		}
		return total;
	}

	/**
	 * @deprecated since 1.6.0
	 */
	@Override
	@Deprecated
	public int getTotalWarning() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalWarning();
		}
		return total;
	}

	@Override
	public int getTotalFailures() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalFailures();
		}
		return total;
	}

	@Override
	public int getTotalErrors() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalErrors();
		}
		return total;
	}

	@Override
	public int getTotalNotRun() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalNotRun();
		}
		return total;
	}
	
	@Override
	public int getTotalNotFound() {
		int total=0;
		for(EcosystemResult result:ecosystemResults){
			total+=result.getTotalNotFound();
		}
		return total;
	}

	public GeneralStatus getStatus() {
		GeneralStatus status = GeneralStatus.SUCCESS;
		for (EcosystemResult result : ecosystemResults) {
			status = status.mostSevereStatus(result.getStatus());
		}
		return status;
	}

	@Override
	public void cleanUp() {
		for (ResultPart part : ecosystemResults){
			part.cleanUp();
		}
	}

	@Override
	public List<? extends EcosystemResult> getSubpartResults() {
		return ecosystemResults;
	}

	@Override
	public int getTotalPassed() {
		int totalPassed=0;
		for(EcosystemResult result:ecosystemResults){
			totalPassed+=result.getTotalPassed();
		}
		return totalPassed;
	}

	@Override
	public int getTotalNotPassed() {
		int totalNotPassed=0;
		for(EcosystemResult result:ecosystemResults){
			totalNotPassed+=result.getTotalNotPassed();
		}
		return totalNotPassed;
	}
	
	@Override
	public String getProjectGroupId() {
		return projectGroupId;
	}

	@Override
	public String getProjectArtifactId() {
		return projectArtifactId;
	}

	@Override
	public String getProjectVersion() {
		return projectVersion;
	}
	
	/**
	 * This method set the data of the project which contains the test suite
	 * 
	 * @param groupId groupId of the project which contains the test suite
	 * @param artifactId artifactId of the project which contains the test suite
	 * @param version version of the project which contains the test suite
	 */
	public void setProjectDescription(String groupId, String artifactId, String version) {
		projectGroupId = groupId;
		projectArtifactId = artifactId;
		projectVersion = version;
	}

	@Override
	public List<TargetInitialisationResult> getTargetInitialisationResults() {
		return targetInitialisationResultInOrder();
	}

	public void setTargetInitialisationResults(Set<TargetInitialisationResult> list) {
		this.targetInitialisationResult = list;
	}
	
	private List<TargetInitialisationResult> targetInitialisationResultInOrder() {
		List<TargetInitialisationResult> alphabeticalList = new LinkedList<>(targetInitialisationResult);
		Collections.sort(alphabeticalList, new Comparator<TargetInitialisationResult>() {

			@Override
			public int compare(TargetInitialisationResult o1, TargetInitialisationResult o2) {				
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		return alphabeticalList;
	}

}

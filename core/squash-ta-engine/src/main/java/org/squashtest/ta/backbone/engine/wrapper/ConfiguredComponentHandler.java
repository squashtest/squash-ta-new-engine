/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import java.util.ArrayList;
import java.util.Collection;

import org.squashtest.ta.framework.components.Resource;

public abstract class ConfiguredComponentHandler {

	/**
	 * Method to send the unwrapped configuration into the wrapped component.
	 * @param unwrappedConfig the configuration resources.
	 */
	protected abstract void addConfigurationToWrapped(Collection<Resource<?>> unwrappedConfig);
	
	/**
	 * Method to unwrap a configuration for the handled component.
	 * @param config the configuration.
	 */
	public void addConfiguration(Collection<ResourceWrapper> config) {
		Collection<Resource<?>> unwrappedConfig=new ArrayList<Resource<?>>();
		for(ResourceWrapper confElementWrapper:config){
			unwrappedConfig.add(confElementWrapper.unwrap());
		}
		addConfigurationToWrapped(unwrappedConfig);
	}

}
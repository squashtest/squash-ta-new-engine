/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Properties;

import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;

/**
 * Context parameters represents parameters associate to a test (script params) and parameters associate to a test suite
 * (global parameters). Those test and test suite parameters are provided through the test suite definition. Those
 * parameters are injected in the runner resource repository using TA resource name reserved for them.<br>
 * According to the {@link EcosysPhase} phase, how and what inject into the test resource repository change. Each
 * implementation of this interface will define the correct behavior.
 * 
 * @author bfranchet
 * 
 */
public interface ContextParameter {

	/** Reserved TA resource name for parameters associated to the test provided by the user */
	String RESERVED_SCRIPT_RESOURCE_NAME = "CONTEXT_SCRIPT_PARAMS";

	/** Reserved TA resource name for parameters associated to the test suite provided by the user */
	String RESERVED_GLOBAL_RESOUCE_NAME = "CONTEXT_GLOBAL_PARAMS";

	/**
	 * Real TA resource name used as identifier in the resource repository for the resource containing the parameters
	 * associated to the test.
	 */
	String SCRIPT_PARAMS_RESOURCE_NAME = "@" + RESERVED_SCRIPT_RESOURCE_NAME;

	/**
	 * Real TA resource name used as identifier in the resource repository for the resource containing the parameters
	 * associated to the test suite.
	 */
	String GLOBAL_PARAMS_RESOURCE_NAME = "@" + RESERVED_GLOBAL_RESOUCE_NAME;

	/**
	 * Methods to inject into the test resource repository, and so make available, the TA resources for test and test
	 * suite parameters:
	 * 
	 * <ul>
	 * <li>Test parameters will be registered as {@link ContextParameter#SCRIPT_PARAMS_RESOURCE_NAME} into the resource
	 * repository of the runner</li>
	 * <li>Test suite parameters will be registered as {@link ContextParameter#GLOBAL_PARAMS_RESOURCE_NAME} into the
	 * resource repository of the runner</li>
	 * </ul>
	 * 
	 * @param runner
	 *            The current test runner. It contains the resource repository and the parameters associated to the test
	 *            suite
	 * @param scriptParams
	 *            Parameters associated to the test
	 */
	void addContextParameterToResources(InternalTestRunner runner, Properties scriptParams);

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.squashtest.ta.framework.test.result.ExecutionDetails;

/**
 * An instruction runner i, local to the {@link org.squashtest.ta.backbone.engine.TestRunner} 
 * and will handle a single instruction.
 * 
 * @author bsiri
 * 
 */
public interface InstructionRunner {

	/**
	 * Set the test runner
	 * 
	 * @param runner
	 *            The test runner
	 */
	void setTestRunner(InternalTestRunner runner);

	/**
	 * Execute the instruction and return an instruction execution report
	 * 
	 * @return an instruction execution report
	 */
	ExecutionDetails run();

	/**
	 * Return instruction execution report filled with the instruction information and a NOT_RUN status
	 * 
	 * @return an instruction execution report
	 */
	ExecutionDetails getInstructionsDetails();

}

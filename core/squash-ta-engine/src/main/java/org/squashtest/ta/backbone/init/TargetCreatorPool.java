/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.init;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.components.TargetCreator;

/**
 * This class regroups all instances of TargetCreator it finds. When the
 * frameworks initializes, it will first look for ResourceRepositories in the
 * main configuration and try to create them. The success of this operation rely
 * on the RepositoryCreator instances that the RepositoryCreatorPool holds, and
 * will query each of them for each properties files it will be given.
 * 
 * @author bsiri
 *
 */
@Component
public class TargetCreatorPool {

	private static final Logger LOGGER = LoggerFactory.getLogger(TargetCreatorPool.class);

	@Autowired(required = false)
	private List<TargetCreator<?>> creators = new LinkedList<TargetCreator<?>>();

	@Inject
	private EngineComponentDefinitionManager definitionManager;

	public void addRepositoryCreator(TargetCreator<?> creator) {
		creators.add(creator);
	}

	public TargetWrapper wrapTarget(Target target) {

		Nature nature = definitionManager.fetchOrAddUniqueType(target.getClass());
		return new TargetWrapper("default", nature, target);

	}

	/**
	 * Determine if a target must be created. It must be created if and only if
	 * it is present in the launched tests
	 * 
	 * @param manager
	 *            The ContextManager containing the launched tests data
	 * @param definition
	 *            The URL of the potential target file we must test
	 * @return true if the definition name is present in the manager, false
	 *         otherwise
	 */
	public boolean mustBeCreated(ContextManager manager, URL definition) {
		return manager.getTargetsNames().contains(getShortName(definition.getPath()));
	}

	/**
	 * @param propertiesFile
	 * @return if one of the RepositoryCreators could handle the file, null if
	 *         none could.
	 */
	public TargetWrapper createTarget(URL propertiesFile) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Trying to instanciate target " + propertiesFile.toExternalForm());
		}
		for (TargetCreator<?> creator : creators) {
			LOGGER.debug("Trying with " + creator.getClass().getName());
			if (creator.canInstantiate(propertiesFile)) {
				try {
					Target target = creator.createTarget(propertiesFile);
					TargetWrapper wrapper = wrapTarget(target);
					String shortName = getShortName(propertiesFile.getPath());
					wrapper.setName(shortName);
					LOGGER.debug("Success!");
					return wrapper;
				} catch (Exception e) {
					if (LOGGER.isWarnEnabled()) {
						LOGGER.warn("TargetCreator " + creator.getClass().getName()
								+ " tried but failed to instanciate target "
								+ (propertiesFile == null ? "null" : propertiesFile.toExternalForm()), e);
					}
				}
			} else {
				LOGGER.debug(creator.getClass().getName() + " does not support this target");
			}
		}
		return null;
	}

	private String getShortName(String filePath) {
		String name = FilenameUtils.getName(filePath);
		int suffixIndex = name.lastIndexOf(".properties");
		if (suffixIndex == -1) {
			return name;
		} else {
			return name.substring(0, suffixIndex);
		}
	}
}

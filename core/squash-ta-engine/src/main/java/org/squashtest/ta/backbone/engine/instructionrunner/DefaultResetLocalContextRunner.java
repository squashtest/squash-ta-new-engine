/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import org.squashtest.ta.backbone.engine.impl.DefaultLocalContextImpl;
import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;

/**
 * Runner to execute the {@link ResetLocalContextInstruction} instruction.
 * @author edegenetais
 *
 */
public class DefaultResetLocalContextRunner
        extends AbstractDefaultInstructionRunner {
    /** data of the instruction to run */
	private ResetLocalContextInstruction instruction;
	
	/**
	 * Constructor with full initialization.
	 * @param instruction the instruction to run.
	 */
	public DefaultResetLocalContextRunner(ResetLocalContextInstruction instruction) {
		this.instruction=instruction;
	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("No use for a generic instruction error.");
	}

	@Override
	protected void checkSettings() {
		//no check there: too simple to fail...
	}

	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}

	@Override
	protected void doRun() {
		LocalContext localContext=testRunner.getLocalContext();
		if(localContext!=null){
			localContext.cleanUp();
		}
		testRunner.setLocalContext(new DefaultLocalContextImpl(instruction.getContextIdentifier()));
	}
	
	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		//NOOP: no input to add here!
	}

}

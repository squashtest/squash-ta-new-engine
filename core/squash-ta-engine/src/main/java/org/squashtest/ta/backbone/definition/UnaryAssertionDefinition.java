/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.definition;

import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.framework.components.UnaryAssertion;

public class UnaryAssertionDefinition extends EngineComponentDefinition<UnaryAssertion<?>> {
	
	public static final Nature UNARY_ASSERTION_NATURE = new Nature("(unary assertion)");
	
	public UnaryAssertionDefinition(Nature first, Nature category, Class<UnaryAssertion<?>> clazz){
		super(first, UNARY_ASSERTION_NATURE, category, clazz);
	}
}

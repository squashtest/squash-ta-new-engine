/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.ResourceGenerator;


/**
 * A ResourceWrapper wraps an instance of Resource in its context metadata. These metadata helps the engine to manage its components and resources,
 * and makes them easily accessible from the outer context (namely the {@link org.squashtest.ta.framework.test.instructions.TestInstruction}s that call theses items).
 * 
 * @author bsiri
 *
 */
public class ResourceWrapper {

	private Nature nature;
	
	private ResourceName name;
	
	private Resource<?> wrappedResource;
	
	private ResourceGenerator generator;
	
	
	public ResourceWrapper(){
		super();
	}
	
	public ResourceWrapper(Nature nature, ResourceName name, Resource<?> resource, ResourceGenerator generator){
		super();
		this.nature=nature;
		this.name=name;
		this.wrappedResource=resource;
		this.generator=generator;
	}
	
	public void setNature(Nature nature){
		this.nature=nature;
	}
	
	public Nature getNature(){
		return nature;
	}
	
	public void setName(ResourceName name){
		this.name=name;
	}
	
	public ResourceName getName(){
		return name;
	}
	
	public void wrap(Resource<?> resource){
		this.wrappedResource = resource;
	}
	
	public Resource<?> unwrap(){
		return wrappedResource;
	}
	
	public void setGenerator(ResourceGenerator generator){
		this.generator = generator;
	}
	
	public ResourceGenerator getGenerator(){
		return generator;
	}
	
	public ResourceWrapper copy(){
		Resource<?> copy = wrappedResource.copy();
		return new ResourceWrapper(nature, name, copy, generator);
	}
	
	/**
	 * @see Resource#cleanUp()
	 */
	public void cleanUp(){
		wrappedResource.cleanUp();
	}
	
}

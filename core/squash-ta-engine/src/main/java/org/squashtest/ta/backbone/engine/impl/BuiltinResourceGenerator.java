/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceProvider;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.ResourceGenerator;

/**
 *
 * @author edegenetais
 */
class BuiltinResourceGenerator implements ResourceGenerator,ResourceProvider {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(BuiltinResourceGenerator.class);
    private static final HashSet<String> RESERVED_NAMES = new HashSet<String>(Arrays.asList(new String[]{"context_global_params","context_script_params",VoidResource.VOID_BUILTIN_NAME.getName()}));

    private final Map<String,Resource<?>> registry;
    protected Nature repositoryCategory;
    protected String name;
    
    public BuiltinResourceGenerator(Map<String,Resource<?>> exploitationBuiltins, Properties globalParams) {
        Set<String> conflictingNames=findConflictingNames(exploitationBuiltins);
        if(conflictingNames.isEmpty()){
            LoggerFactory.getLogger(BuiltinResourceGenerator.class).debug("Registering the following exploitation-defined builtin resources : {}",exploitationBuiltins);
            registry=new HashMap<>(exploitationBuiltins.size()+1,1.0f);
            
        }else{
            throw new EngineInitException("Failed to register exploitation layer builtins due to conflicts with the following engine-defined builtins: "+conflictingNames);
        }
        registry.putAll(exploitationBuiltins);
        registry.put(VoidResource.VOID_BUILTIN_NAME.getName(), new VoidResource());
        registry.put("context_global_params",new PropertiesResource(globalParams));
    }

    @Override
    public ResourceWrapper findResources(ResourceName resourceName) {
         ResourceWrapper res;
        if(ResourceName.Scope.SCOPE_BUILTIN.equals(resourceName.getScope()) && registry.containsKey(resourceName.getName())){
            Resource<?> resource=registry.get(resourceName.getName());
            res=new ResourceWrapper(new Nature(extractResourceNatureString(resource)), resourceName, resource, this);
        }else if(!ResourceName.Scope.SCOPE_BUILTIN.equals(resourceName.getScope())){
            LOGGER.warn("I only manage builtin scope, so resource {} is unknown to me!",name);
            res=null;
        }else{
            LOGGER.error("Unknown resource with name {} was requested from builtin context.",resourceName);
            res=null;
        }
        return res;
    }

    @Override
    public void cleanup() {
        for(Resource<?> res:registry.values()){
            res.cleanUp();
        }
    }

    @Override
    public void init() {
        //noop - nothing to do so far
    }

    @Override
    public void reset() {
        //noop - nothing to do so far
    }

    @Override
    public Nature getRepositoryCategory() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Set<String> findConflictingNames(Map<String, Resource<?>> exploitationBuiltins) {
        Set<String> conflictingNames=new HashSet<>(exploitationBuiltins.keySet());
        conflictingNames.retainAll(RESERVED_NAMES);
        return conflictingNames;
    }

    @Override
    public ResourceGenerator toGenerator() {
        return this;
    }

    
    
    @Override
    public String getGeneratorType() {
        return getName();
    }

    @Override
    public Properties getConfiguration() {
        return new Properties();
    }
    
    private String extractResourceNatureString(Resource<?> resource){
        final Class<? extends Resource> resourceClass = resource.getClass();
        final TAResource annotation = resourceClass.getAnnotation(TAResource.class);
        final String resourceNatureString;
        if(annotation==null){
            ResourceType deprecatedAnnotation=resourceClass.getAnnotation(ResourceType.class);
            if(deprecatedAnnotation==null){
                throw new EngineInitException("Resource class "+resourceClass.getName()+" is not properly annotated with @TAResource, as consequence it is not valid.");
            }else{
                resourceNatureString = deprecatedAnnotation.value();
                LOGGER.warn("Resource class {} uses the deprecated @ResourceType annotation. Please use @TAResource instead.",resourceClass);
            }
        }else{
            resourceNatureString = annotation.value();
        }
        return resourceNatureString;
    }

    public String getName() {
        return name;
    }
}

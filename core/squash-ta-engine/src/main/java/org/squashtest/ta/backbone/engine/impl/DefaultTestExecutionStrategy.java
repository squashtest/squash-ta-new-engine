/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.squashtest.ta.backbone.engine.InstructionFlowStrategy;
import org.squashtest.ta.backbone.exception.UnexpectedStatusException;
import org.squashtest.ta.framework.test.result.GeneralStatus;

/**
 * This is the default strategy used for the test phase execution.
 * 
 * @author edegenetais
 * 
 */
public class DefaultTestExecutionStrategy implements InstructionFlowStrategy {

	@Override
	public boolean continueExecution(GeneralStatus instructionStatus, boolean continueOnFailOrError) {
		// If we enter in this method, that means the instruction has been executed. So its status can't be NOT_RUN
		if (instructionStatus.isNotRun()) {
			throw new UnexpectedStatusException(instructionStatus);
		}
		return instructionStatus.isPassed() || continueOnFailOrError;
	}

	@Override
	public GeneralStatus aggregate(GeneralStatus aggregateStatus, GeneralStatus instructionStatus) {
		// If we enter in this method, that means the test is currently running. So its status can't be NOT_RUN
		if (aggregateStatus.isNotRun()) {
			throw new UnexpectedStatusException(aggregateStatus);
		}
		GeneralStatus newStatus = aggregateStatus;
		if (!instructionStatus.isNotRun()) {
			newStatus = aggregateStatus.mostSevereStatus(instructionStatus);
		}
		return newStatus;
	}

	@Override
	public boolean beginGroupExecution(GeneralStatus aggregateStatus, boolean hasOnlyVerifyFailOrError) {
		// If we enter in this method, that means the test is currently running. So its status can't be NOT_RUN
		if (aggregateStatus.isNotRun()) {
			throw new UnexpectedStatusException(aggregateStatus);
		}
		return aggregateStatus.isPassed() || hasOnlyVerifyFailOrError;
	}

}

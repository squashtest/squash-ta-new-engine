/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;

public class DefaultEcosystemResult implements EcosystemResult {
	
	/** setup process result, in case you need to drill down. */
	private TestResult setupResult;
	
	/** Individual test results, in case you need to drill down. */
	private List<TestResult> testResults=new LinkedList<>();
	
	/** tearDown process result, in case you need to drill down. */
	private TestResult tearDownResult;
	
	/** global status for the ecosystem. */
	private GeneralStatus status;
	
	/** End time of ecosystem execution. */
	private Date endTime;

	/** Start time of ecosystem execution. */
	private Date startTime;
	
	/** Name of the ecosystem. */
	private String name;

	/* **********************
	 * Framework API methods.
	 */
	@Override
	public List<? extends TestResult> getSubpartResults() {
		return testResults;
	}

	@Override
	public int getTotalTests() {
		return testResults.size();
	}

	@Override
	public int getTotalErrors() {
		return statusCount(GeneralStatus.ERROR);
	}

	@Override
	public int getTotalFailures() {
		return statusCount(GeneralStatus.FAIL);
	}

	@Override
	public int getTotalNotRun() {
		return statusCount(GeneralStatus.NOT_RUN);
	}
	
	@Override
	public int getTotalNotFound() {
		return statusCount(GeneralStatus.NOT_FOUND);
	}

	/**
	 * @deprecated since 1.6.0
	 */
	@Override
	@Deprecated
	public int getTotalWarning() {
		return statusCount(GeneralStatus.WARNING);
	}
	
	@Override
	public int getTotalSuccess() {
		return statusCount(GeneralStatus.SUCCESS);
	}
	
	@Override
	public int getTotalPassed() {
		int totalSuccess=0;
		for(TestResult tr:testResults){
			if(tr.getStatus().isPassed()){
				totalSuccess++;
			}
		}
		return totalSuccess;
	}

	@Override
	public int getTotalNotPassed() {
		int totalSuccess=0;
		for(TestResult tr:testResults){
			if(!tr.getStatus().isPassed()){
				totalSuccess++;
			}
		}
		return totalSuccess;
	}
	
	@Override
	public TestResult getSetupResult() {
		return setupResult;
	}

	@Override
	public TestResult getTearDownResult() {
		return tearDownResult;
	}

	@Override
	public GeneralStatus getStatus() {
		return status;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Date startTime() {
		return startTime;
	}

	@Override
	public Date endTime() {
		return endTime;
	}

	@Override
	public void cleanUp() {
		for(TestResult tr:testResults){
			tr.cleanUp();
		}
		if (setupResult!=null){
			setupResult.cleanUp();
		}
		if (tearDownResult!=null){
			tearDownResult.cleanUp();
		}
	}
	
	/**
	 * Register the setup execution report.
     * @param result test result
	 */
	public void setSetupResult(TestResult result){
		setupResult=result;
	}
	
	/**
	 * Register the tearDown execution report.
     * @param result test result
	 */
	public void setTearDownResult(TestResult result){
		tearDownResult=result;
	}
	
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addTestResult(TestResult tr){
		testResults.add(tr);
	}

	public void setStatus(GeneralStatus newStatus){
		this.status=newStatus;
	}
	/* ************************************
	 * End of Engine implementation methods
	 */

	/**
	 * General status count method.
	 * @param targetStatus value of the status to extract.
	 * @return
	 */
	private int statusCount(GeneralStatus targetStatus) {
		int totalTargetStatusCount=0;
		for(TestResult tr:testResults){
			if(targetStatus==tr.getStatus()){
				totalTargetStatusCount++;
			}
		}
		return totalTargetStatusCount;
	}
	
}

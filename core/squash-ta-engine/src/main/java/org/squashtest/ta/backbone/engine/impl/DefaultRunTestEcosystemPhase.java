/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Collections;
import java.util.Map;

import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EcosystemPhase;
import org.squashtest.ta.backbone.engine.event.ContextualStatusUpdateEvent;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;
import org.squashtest.ta.framework.test.instructions.ResourceName;

public class DefaultRunTestEcosystemPhase extends AbstractEcosystemContextSource implements EcosystemPhase{
	
	public DefaultRunTestEcosystemPhase(String ecosystemName) {
		super(ecosystemName);
	}
	
	private final class RunTestContextManager extends AbstractContextManagerDecorator {
		
		public RunTestContextManager(ContextManager manager) {
			super(manager);
		}

		
                @Override
		public Map<ResourceName, ResourceWrapper> getEcosystemResources() {
			//during run test phase, ecosystem resources are unmodifiable.
			return Collections.unmodifiableMap(super.getEcosystemResources());
		}

		@Override
		public void postEvent(ContextualStatusUpdateEvent<?> event) {
			event.addContext(DefaultRunTestEcosystemPhase.this);
			super.postEvent(event);
		}
		
	}
	
	@Override
	public ContextManager getContextManagerReference(ContextManager manager) {
		return new RunTestContextManager(manager);
	}

	@Override
	public EcosysPhase getEcosysPhase() {
		return EcosysPhase.RUN; 
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine;

import java.util.Collection;

import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;

public interface AssertionManager {
	

	/**
	 * Will add a definition for a BinaryAssertion. 
         * Will refuse to perform this operation if a factory having the same actual result nature, 
         * expected result nature and identifier is already present.
	 * 
	 * @param definition
	 */
	void addAssertionDefinition(BinaryAssertionDefinition definition);
	
	void removeAssertionDefinition(BinaryAssertionDefinition definition);
	
	void removeAssertionDefinition(Nature actualNature, Nature expectedNature, Nature category);
	
	/**
	 * Same for unary assertion.
	 * 
	 */
	void addAssertionDefinition(UnaryAssertionDefinition definition);
	
	void removeAssertionDefinition(UnaryAssertionDefinition definition);
	
	void removeAssertionDefinition(Nature actualNature, Nature category);	
	
	
	/**
	 * <p>
	 * 	Depending on the given parameter, will return a list of new BINARY Assertion instances (with empty configuration).
	 * 	Each parameter accepts null, but the more the caller provides the more precise will be the result. 
	 *  Theoretically if the user supplies all the arguments the returned list shall be of size 0 of 1. 
	 * </p>
	 * 
	 * 
	 * @param actualNature
	 * @param expectedNature
	 * @param category
	 * @return a Collection of Assertions fitting the description (never null).
	 */
	Collection<BinaryAssertionHandler> getAllBinaryAssertions(Nature actualNature, Nature expectedNature, Nature category);
	
	Collection<BinaryAssertionHandler> getAllBinaryAssertionsByName(String actualNatureName, String expectedNatureName, String category);

	/**
	 * <p>
	 * Depending on the given parameter, will return a list of new UNARY Assertion instances 
         * (with empty configuration).
	 * Each parameter accepts null, but the more the caller provides the more precise will be the result. 
	 * Theoretically if the user supplies all the arguments the returned list shall be of size 0 of 1. 
	 * </p>
	 * 
	 * 
	 * @param actualNature
	 * @param category
	 * @return a Collection of Assertions fitting the description (never null).
	 */
	Collection<UnaryAssertionHandler> getAllUnaryAssertions(Nature actualNature, Nature category);
	
	Collection<UnaryAssertionHandler> getAllUnaryAssertionsByName(String actualNatureName, String categoryName);

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import java.util.Collection;

import org.squashtest.ta.backbone.engine.ResourceConversionSettings;
import org.squashtest.ta.backbone.engine.impl.ResourceConversionSettingsImpl;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.AmbiguousConversionException;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.test.DefaultResourceMetadata;
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata.ResourceRole;

class DefaultConvertResourceRunner extends AbstractDefaultInstructionRunner {
	
	private static final String MISSING_NAME_ERROR = "the resource name is missing";
	private static final String MISSING_ALIAS_ERROR = "is must be given an alias";
	private static final String MISSING_CONVERSION_NATURE_ERROR = "result type isn't specified";
	private static final String GENERIC_ERROR_MESSAGE = "Cannot convert resource : ";
	
	private ConvertResourceInstruction instruction;
	
	DefaultConvertResourceRunner(ConvertResourceInstruction instruction){
		this.instruction=instruction;
	}

	protected void doRun() {
		
		ResourceWrapper converted = convertResource();		
		
		//give it an alias
		
		converted.setName(instruction.getResultName());
		
		//store it in the local cache
		testRunner.addResourceToCache(converted);
		
	}

	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		ResourceWrapper source=testRunner.getResourceFromCache(instruction.getResourceName());
		if (source != null) {
			DefaultResourceMetadata sourceMetadata = new DefaultResourceMetadata(
					source);
			sourceMetadata.setRole(ResourceRole.INPUT);
			ResourceAndContext sourceRac = new ResourceAndContext();
			sourceRac.setResource(source.unwrap().copy());
			sourceRac.setMetadata(sourceMetadata);
			executionDetails.addResourceAndContext(sourceRac);
		}
	}
	
	private ResourceConversionSettings buildSettings(ResourceWrapper res){
		ResourceConversionSettingsImpl settings = new ResourceConversionSettingsImpl();
		settings.setOriginalNature(res.getNature().getName());
		settings.setDesiredNature(instruction.getResultType());
		settings.setConverterCategory(instruction.getConverterCategory());
		return settings;
	}
	
	
	private ResourceWrapper convertResource(){
		
		ResourceName name = instruction.getResourceName();
		ResourceWrapper resource = fetchResourceOrFail(name);
		
		ResourceConverterHandler converter = findConverter(resource);
		return applyConverter(resource, converter);
		
	}
	
	private ResourceConverterHandler findConverter(ResourceWrapper resource){

		ResourceConversionSettings settings = buildSettings(resource);
		
		Collection<ResourceConverterHandler> converters = testRunner.getContextManager().getConverters(settings);
		
		if (converters.isEmpty()){
			throw new ImpossibleConversionException("Impossible to convert resource "+resource.getName()+
					" : unable to convert from nature "+resource.getNature().getName()+" to nature "+
					settings.getDesiredNature() + " with " +  settings.getConverterCategory() + " converter");
		}
		else if (converters.size()>1){
			StringBuilder builder = new StringBuilder();
			
			String message = "Impossible to convert resource "+resource.getName()+" : multiple converters found from nature "+
			 resource.getNature().getName()+" to nature "+settings.getDesiredNature()+" : ";
			
			builder.append(message);
			
			for (ResourceConverterHandler c : converters){
				builder.append(c.getConverterCategory().getName()+" , ");
			}
			throw new AmbiguousConversionException(builder.toString());
		}
		else{
			return converters.iterator().next();			
		}		
	}
	
	private ResourceWrapper applyConverter(ResourceWrapper resource, ResourceConverterHandler converter){
		Collection<ResourceWrapper> conf = fetchConfiguration(instruction.getConfiguration());
		converter.addConfiguration(conf);
		return converter.convert(resource);		
	}

	protected void checkSettings(){
		if (instruction.getResourceName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_NAME_ERROR);
		}
		if (instruction.getResultName()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_ALIAS_ERROR);
		}
		
		if (instruction.getResultType()==null){
			logAndThrow(GENERIC_ERROR_MESSAGE+MISSING_CONVERSION_NATURE_ERROR);
		}
		
	}
	
	protected TestInstruction getInstruction(){
		return instruction;
	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		return GENERIC_ERROR_MESSAGE;
	}

}

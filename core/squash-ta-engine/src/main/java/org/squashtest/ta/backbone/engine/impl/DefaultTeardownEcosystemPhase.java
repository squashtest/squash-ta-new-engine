/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EcosystemPhase;
import org.squashtest.ta.backbone.engine.event.ContextualStatusUpdateEvent;
import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;

/**
 * The execution strategy for the ecosystem teardown phase.
 * @author edegenetais
 *
 */
public class DefaultTeardownEcosystemPhase extends AbstractEcosystemContextSource implements EcosystemPhase{
	/**
	 * @param ecosystemName
	 */
	public DefaultTeardownEcosystemPhase(String ecosystemName) {
		super(ecosystemName);
	}

	private final class ContextManagerDecorator extends
			AbstractContextManagerDecorator {
		private ContextManagerDecorator(ContextManager provider) {
			super(provider);
		}

		@Override
		public void postEvent(ContextualStatusUpdateEvent<?> event) {
			event.addContext(DefaultTeardownEcosystemPhase.this);
			super.postEvent(event);
		}
	}

	@Override
	public ContextManager getContextManagerReference(ContextManager manager) {
		return new ContextManagerDecorator(manager);
	}

	@Override
	public EcosysPhase getEcosysPhase() {
		return EcosysPhase.TEARDOWN;
	}

}

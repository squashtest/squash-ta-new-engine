/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.PhaseResult;

/**
 * {@link PhaseResult} subinterface to give the core writing access the phase result object.
 * 
 * @author bfranchet
 * 
 */
public interface AbstractPhaseResult extends PhaseResult {

	/**
	 * Add a new instruction execution details
	 * 
	 * @param instructionExecution
	 *            New instructionExecution to add
	 * @param continueOnFailOrError 
         *              <code>true</code> if the execution should continue on fail or error
	 */
	public abstract void addInstruction(ExecutionDetails instructionExecution, boolean continueOnFailOrError);

}

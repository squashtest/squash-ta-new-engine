/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine;

import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;

/**
 * This strategy interface represents phases of the ecosystem lifecycle.
 * @author edegenetais
 *
 */
public interface EcosystemPhase {
	/**
	 * This method gives the current {@link EcosystemPhase} an opportunity to decorate the context manager in order to influence its behavior.
	 * @param manager the ecosystem contextManager, transmitted by the higher order runner.
	 * @return
	 */
	ContextManager getContextManagerReference(ContextManager manager);
	
	/**
	 * This method returns one of the possible phase of the ecsosystem define in the enum : {@link EcosysPhase} 
	 * @return Returns the current ecosystem phase
	 */
	EcosysPhase getEcosysPhase();
}

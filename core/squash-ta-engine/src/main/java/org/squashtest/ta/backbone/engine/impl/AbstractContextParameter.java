/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Properties;

import org.squashtest.ta.backbone.engine.wrapper.GenericResourceGenerator;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.result.ResourceGenerator;

/**
 * Abstract implementation of {@link ContextParameter} which define how add as a TA resource parameters associate to a
 * test and test suite:
 * <ul>
 * <li>To add test parameters, implementation class should use {@link AbstractContextParameter#addScriptParams(InternalTestRunner, Properties)} method</li>
 * <li>To add test suite parameters, implementation class should use {@link AbstractContextParameter#addGlobalParams(InternalTestRunner, Scope)} method</li>
 * </ul>
 * 
 * @author bfranchet
 * 
 */
public abstract class AbstractContextParameter implements ContextParameter {

	private Nature nature;
	private ResourceGenerator generator;

	/**
	 * Default constructor
	 */
	public AbstractContextParameter() {
		nature = new Nature("properties");
		generator = new GenericResourceGenerator("internal", "context_parameters", new Properties());
	}

	/**
	 * Method to make available, as TA resource, parameters associated to the test suite.<br>
	 * This method creates a {@link PropertiesResource} containing the test suite parameters given in arguments. This TA
	 * resource is then add to the repository of available TA resource with for resource name
	 * {@link ContextParameter#GLOBAL_PARAMS_RESOURCE_NAME}.<br>
	 * Two resource scope could be use according to the type of tests :
	 * <ul>
	 * <li>For regular test the scope to use is: {@link Scope#SCOPE_TEST}</li>
	 * <li>For ecosystem test the scope to use is: {@link Scope#SCOPE_ECOSYSTEM}</li>
	 * </ul>
	 * 
	 * @param runner
	 *            The current test runner
	 * @param scope
	 *            The resource
	 */
	protected void addGlobalParams(InternalTestRunner runner, Scope scope) {
		ResourceName globalParamsName = new ResourceName(scope, GLOBAL_PARAMS_RESOURCE_NAME);
		Resource<PropertiesResource> globalParamsResource = new PropertiesResource(new Properties(runner
				.getContextManager().getGlobalParams())).copy();
		ResourceWrapper globalParams = new ResourceWrapper(nature, globalParamsName, globalParamsResource, generator);
		runner.addResourceToCache(globalParams);
	}

	/**
	 * Method to make available, as TA resource, parameters associated to the test.<br>
	 * This method creates a {@link PropertiesResource} containing the test parameters. This TA resource is then add to
	 * the repository of available TA resource with for resource name
	 * {@link ContextParameter#SCRIPT_PARAMS_RESOURCE_NAME}.<br>
	 * As only regular test (regular in contrast with ecosystem test) could have parameters, then the resource scope is
	 * always {@link Scope#SCOPE_TEST}.
	 * 
	 * @param runner
	 *            Current test runner
	 * @param scriptParams
	 *            Parameters associated to the test
	 */
	protected void addScriptParams(InternalTestRunner runner, Properties scriptParams) {
		ResourceName scriptParamsName = new ResourceName(SCRIPT_PARAMS_RESOURCE_NAME);
		Resource<PropertiesResource> scriptParamsResource = new PropertiesResource(scriptParams);
		ResourceWrapper scriptParam = new ResourceWrapper(nature, scriptParamsName, scriptParamsResource, generator);
		runner.addResourceToCache(scriptParam);
	}

	/**
	 * Utility method to test whether a resource name match a reserved TA resource name used for test and test suite
	 * parameters. Note that the resource name is case insensitive
	 * 
	 * @param resourceName
	 *            The resource name to test (case insensitive)
	 * 
	 * @return <code>true</code> If the resource name given in argument match a reserved TA resource name.
	 */
	public static boolean isReservedResourceName(String resourceName) {
		String resourceNameToTest = resourceName.toUpperCase();
		return resourceNameToTest.equals(RESERVED_SCRIPT_RESOURCE_NAME)
				|| resourceNameToTest.equals(RESERVED_GLOBAL_RESOUCE_NAME);
	}

	/**
	 * Utility method to retrieve the TA resource, containing the test / test suite parameters, associated to the
	 * resource name given in argument. Note that the resource name is case insensitive<br>
	 * This method throw an {@link UnsupportedOperationException} if the given resource name is not a reserved TA
	 * resource name for test / test suite parameters.<br>
	 * 
	 * @param resourceName
	 *            The resource name (case insensitive)
	 * 
	 * @return The resource found
	 */
	public static ResourceWrapper getContextResourcesFromCache(InternalTestRunner runner, String resourceName,
			Scope scope) {
		String resourceNameToTest = resourceName.toUpperCase();
		String resourceNameToSearch;
		if (resourceNameToTest.equals(RESERVED_SCRIPT_RESOURCE_NAME)) {
			resourceNameToSearch = SCRIPT_PARAMS_RESOURCE_NAME;
		} else if (resourceNameToTest.equals(RESERVED_GLOBAL_RESOUCE_NAME)) {
			resourceNameToSearch = GLOBAL_PARAMS_RESOURCE_NAME;
		} else {
			throw new UnsupportedOperationException("The resource name \"" + resourceName
					+ ";\" doesn't match any context resource name");
		}
		ResourceName contextResourceName = new ResourceName(scope, resourceNameToSearch);
		return runner.getResourceFromCache(contextResourceName);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import java.util.Collection;

import org.squashtest.ta.backbone.tools.ReflectionUtils;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.result.ResourceGenerator;

public class CommandHandler extends ConfiguredComponentHandler{
	
	private static final String TARGET_SETTER_METHOD = "setTarget";
	private static final String RESOURCE_SETTER_METHOD = "setResource";
	
	private ReflectionUtils reflector = new ReflectionUtils();
	
	private Nature resourceNature;
	private Nature targetNature;
	private Nature commandCategory;
	private Command<?,?> wrappedCommand;
	private Nature resultType;
	private Class<?> inputType;
	private Class<?> targetType;
	
	private ResourceGenerator generator;
	
	
	/* ****************** ctors *************************** */
	
	public CommandHandler(){
		super();
	}
	
	public CommandHandler(Nature ressourceNature, Nature targetNature, Nature commandCategory, Nature resultType,
			Command<?,?> wrappedCommand){
		
		super();
		
		this.resourceNature=ressourceNature;
		this.targetNature=targetNature;
		this.commandCategory = commandCategory;
		this.wrappedCommand=wrappedCommand;
		this.resultType=resultType;
		Class<?> commandClass=wrappedCommand.getClass();
		Class<?>[] parameters=reflector.getGenericTypes(commandClass, Command.class);
		this.inputType=parameters[0];
		this.targetType=parameters[1];
	}
	
	/* ****************** metadata ************************ */
	
	public Nature getResourceNature() {
		return resourceNature;
	}
	
	public void setResourceNature(Nature resourceNature) {
		this.resourceNature = resourceNature;
	}
	
	public Nature getTargetNature() {
		return targetNature;
	}
	
	public void setTargetNature(Nature targetNature) {
		this.targetNature = targetNature;
	}
		
	public void wrap(Command<?,?> command){
		this.wrappedCommand = command;
	}

	public Nature getCommandCategory() {
		return commandCategory;
	}

	public void setCommandCategory(Nature commandCategory) {
		this.commandCategory = commandCategory;
	}
	
	
	public Nature getResultType() {
		return resultType;
	}

	public void setResultType(Nature resultType) {
		this.resultType = resultType;
	}


	/* ***************** wrapped methods ******************* */

	public void setTarget(TargetWrapper target){
		Target wTarget = target.unwrap();
		reflector.invoke(wrappedCommand, TARGET_SETTER_METHOD, targetType,wTarget);
		generator=target.toGenerator();
	}
	
	
	public void setResource(ResourceWrapper resourceWrapper){
		Resource<?> resource = resourceWrapper.unwrap();
                
		reflector.invoke(wrappedCommand, RESOURCE_SETTER_METHOD, inputType,resource);
	}
	
	
	public ResourceWrapper apply(){
		Resource<?> res = wrappedCommand.apply();
		if (res!=null){
			ResourceName name = new ResourceName(Scope.SCOPE_TEST,"(command-result)");
			return new ResourceWrapper(resultType, name, res, generator);
		}
		else{
			return null;
		}
	}
	
	
	public void cleanUp(){
		wrappedCommand.cleanUp();
	}

	@Override
	protected void addConfigurationToWrapped(
			Collection<Resource<?>> unwrappedConfig) {
		wrappedCommand.addConfiguration(unwrappedConfig);
	}
	
	
}

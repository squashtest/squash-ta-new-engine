/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata;

public class DefaultExecutionDetails extends AbstractExecutionDetails {

	private Exception caughtException;

	private Collection<ResourceAndContext> resourcesInContext = new ArrayList<>();

	/* *********************************** API part ************************* */

	@Override
	public Exception caughtException() {
		return caughtException;
	}

	@Override
	public Collection<ResourceAndContext> getResourcesAndContext() {
		return resourcesInContext;
	}

	/**
	 * Set the exception {@link Exception}) occurred during the instruction execution.
	 * 
	 * @param ex
	 *            The exception occurred
	 */
	public void setCaughtException(Exception ex) {
		this.caughtException = ex;
	}

	/***
	 * Set the context ({@link ResourceAndContext}) when an exception occurred 
         * during the instruction execution.
	 * 
	 * @param contextResource
	 *            The context to add
	 */
        @Override
	public void addResourceAndContext(ResourceAndContext contextResource) {
		this.resourcesInContext.add(contextResource);
	}

	/***
	 * Set the context ({@link ResourceAndContext}) when an exception occurred 
         * during the instruction execution.
	 * 
	 * @param contextResources
	 *            The context to add
	 */
	public void addAllResourceAndContext(List<ResourceAndContext> contextResources) {
		this.resourcesInContext.addAll(contextResources);
	}

	/**
	 * This method will build a ResourceAndContext object from a ResourceWrapper, a
         * dd it to the collection and return it. 
         * So later on we can modify its role using {@link DefaultResourceMetadata#setRole(org.squashtest.ta.framework.test.result.ResourceMetadata.ResourceRole)}.
	 * 
	 * @param wrapper
         * @return ResourceAndContext
	 */
	public ResourceAndContext addPartialResourceAndContext(ResourceWrapper wrapper) {
		Resource<?> resource = wrapper.unwrap();
		ResourceMetadata meta = new DefaultResourceMetadata(wrapper);
		ResourceAndContext rac = new ResourceAndContext();
		rac.setResource(resource);
		rac.setMetadata(meta);
		resourcesInContext.add(rac);
		return rac;
	}

}

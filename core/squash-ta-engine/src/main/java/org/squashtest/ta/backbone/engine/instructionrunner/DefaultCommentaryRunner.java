/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import org.squashtest.ta.framework.test.instructions.CommentaryInstruction;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;

public class DefaultCommentaryRunner extends AbstractDefaultInstructionRunner {

    /** data of the instruction to run */
	private CommentaryInstruction instruction;

	/**
	 * Constructor with full initialization.
	 * @param instruction the instruction to run.
	 */
	public DefaultCommentaryRunner(CommentaryInstruction instruction) {
		this.instruction=instruction;
	}

	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		// noop
	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		throw new UnsupportedOperationException("No use for a generic instruction error.");
	}

	@Override
	protected void checkSettings() {
		// noop
	}

	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}

	@Override
	protected void doRun() {
		// noop
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.net.URL;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EventManager;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceProvider;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.backbone.init.RepositoryCreatorPool;
import org.squashtest.ta.backbone.init.TargetCreatorPool;
import org.squashtest.ta.core.tools.io.PropertiesLoader;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.facade.TFTestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
/**
 * <p>Since we need fine-tuned configuration that Spring cannot provide we use that class to complete the framework
 * init phase. There is surely a more clever way to achieve that but we'll check that out in later versions.
 * </p>
 * 
 * <p>
 * 	this class should be discarded when it's job is done (ie, remove it from the bean factory and be done with it).
 * </p>
 * 
 * <p>the proper use of the implementation should be :</p>
 * 
 * <ol>
 * 	<li>set the configuration,</li>
 * 	<li>call init</li>
 * </ol>
 * 
 * <p>do it in the main class after Spring fired up, or through an OSGI bundle activator for instance.</p>
 * 
 * <p>
 * the configuration is supplied by a TestWorkspaceBrowser. That object will supply the initializer with the various configuration things it needs.
 * </p>
 * 
 * @author bsiri
 *
 */
@Component
public class ContextInitializer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ContextInitializer.class);
        
        public static final String MDC_ELEMENT_ELEMENT_URL = "elementURL";
        
	private static final PropertiesLoader PROPERTIES_LOADER = new PropertiesLoader();
	
	private static final String DEFAULT_REPOSITORY = "default.repository";
	
	private static final String WARN_UNKNOWN_REPOSITORY
	= "TA context initialization : configuration warning : repository '{}' could not be instanciated";	

	private static final String WARN_UNKNOWN_TARGET
	= "TA context initialization : configuration warning : target '{}' was not recognised";
	
	private static final String WARN_UNKNOWN_DEFAULT_CONFIG
	= "TA context initialization : configuration warning : no default configuration file available.";

	@Inject
	private EngineComponentDefinitionManager definitionsManager;

	@Inject
	private RepositoryCreatorPool repoCreatorPool;
	
	@Inject
	private TargetCreatorPool targetCreatorPool;
	
	@Inject
	private EventManager eventManager;
	
	//not injected yet;//may never be Spring-injected, since it comes from the exploitation layer...
	private TFTestWorkspaceBrowser workspaceBrowser;
	
	public void setTestWorkspaceBrowser(TFTestWorkspaceBrowser browser){
                
		this.workspaceBrowser=browser;
	}
	
	public void addEventListener(StatusUpdateListener listener){
		eventManager.addEventListener(listener);
	}
	
	public boolean removeEventListener(StatusUpdateListener listener){
		return eventManager.removeEventListener(listener);
	}
	
	public ContextManager newContextManager(TestSuite suite){
		
		initGlobalConfiguration();

		ContextManagerImpl manager = new ContextManagerImpl();
		manager.setTargetsNames(suite.getTargetsNames());
		manager.setGlobalParams(suite.getGlobalParams());
		
		initResourcesLoader(manager);					
		initTargets(manager);
		initConverterManager(manager);
		initCommandManager(manager);
		initAssertionManager(manager);
		initEventManager(manager);
                
                ResourceProvider builtinScope=new BuiltinResourceGenerator(workspaceBrowser.getExploitationBuiltinResources(),suite.getGlobalParams());
                manager.setBuiltinScope(builtinScope);
		
		return manager;
	}
	
	private void initResourcesLoader(ContextManagerImpl manager){
		ResourceLoaderImpl loader = new ResourceLoaderImpl();
		
		
		//one should first include the default repository
		RepositoryWrapper defaultRepository = repoCreatorPool.wrapRepository(workspaceBrowser.getDefaultResourceRepository());
		defaultRepository.setName(DEFAULT_REPOSITORY);
		loader.addRepository(defaultRepository);
		
		//now the user-defined repositories
		for (URL definition:workspaceBrowser.getRepositoriesDefinitions()){
                        MDC.put(MDC_ELEMENT_ELEMENT_URL, definition.toExternalForm());
			RepositoryWrapper repo = repoCreatorPool.createRepository(definition);
			if (repo==null){
				LOGGER.warn(WARN_UNKNOWN_REPOSITORY, definition.toExternalForm());
			}else{
				loader.addRepository(repo);
			}
		}	
                MDC.remove(MDC_ELEMENT_ELEMENT_URL);
		
		manager.setResourceLoader(loader);
	}
	
	private void initTargets(ContextManager manager){	
		
		//the void target is part of the default.
		TargetWrapper voidTarget = targetCreatorPool.wrapTarget(new VoidTarget());
		voidTarget.setName(VoidTarget.INSTANCE_NAME);
		manager.registerTarget(voidTarget);
		
		for (URL definition:workspaceBrowser.getTargetsDefinitions()){
                        MDC.put(MDC_ELEMENT_ELEMENT_URL, definition.toExternalForm());
			//we register the targets only if we need them
			if (targetCreatorPool.mustBeCreated(manager, definition)){
				TargetWrapper target= targetCreatorPool.createTarget(definition);
				if (target==null){
					LOGGER.warn(WARN_UNKNOWN_TARGET, definition.toExternalForm());
				}else {
					manager.registerTarget(target);
				}
			}			
		}
                MDC.remove(MDC_ELEMENT_ELEMENT_URL);
	}
	
	private void initGlobalConfiguration(){
		File definition = workspaceBrowser.getDefaultResourceFile();
		if(definition != null){
                    MDC.put(MDC_ELEMENT_ELEMENT_URL, definition.toURI().toString());
			try{
                            Properties defaultProperties = PROPERTIES_LOADER.load(definition);
                            if (defaultProperties != null) {
                                injectConfigurationIntoJVMSystemProperties(defaultProperties);
                            }
			} catch (IOException e){
				LOGGER.warn(WARN_UNKNOWN_DEFAULT_CONFIG,e);
			}
		}
                MDC.remove(MDC_ELEMENT_ELEMENT_URL);
	}

        public void injectConfigurationIntoJVMSystemProperties(Properties defaultProperties) {
            Enumeration<?> propertyNames = defaultProperties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String key = (String) propertyNames.nextElement();
                System.setProperty(key, defaultProperties.getProperty(key));
            }
        }

	private void initConverterManager(ContextManagerImpl manager){
		
		ResourceConverterManagerImpl converterManager = new ResourceConverterManagerImpl();
		List<ConverterDefinition> definitions = definitionsManager.getConverterDefinitions();
		
		ObjectFactory factory = definitionsManager.getFactory();
		converterManager.setObjectFactory(factory);
		
		for (ConverterDefinition definition : definitions){
			converterManager.addConverterDefinition(definition);			
		}		
		
		manager.setResourceConverterManager(converterManager);
	}
	
	private void initCommandManager(ContextManagerImpl manager){
		
		CommandManagerImpl commandManager = new CommandManagerImpl();
		List<CommandDefinition> definitions = definitionsManager.getCommandDefinitions();
		
		ObjectFactory factory = definitionsManager.getFactory();
		commandManager.setObjectFactory(factory);
		
		for (CommandDefinition definition : definitions){
			commandManager.addCommandDefinition(definition);
		}
		
		manager.setCommandManager(commandManager);
		
	}
	
	private void initAssertionManager(ContextManagerImpl manager){
		
		AssertionManagerImpl assertionManager = new AssertionManagerImpl();
		List<BinaryAssertionDefinition> bDefinitions = definitionsManager.getBinaryAssertionDefinitions();
		List<UnaryAssertionDefinition> uDefinitions = definitionsManager.getUnaryAssertionDefinitions();
		
		ObjectFactory factory = definitionsManager.getFactory();
		assertionManager.setObjectFactory(factory);
		
		for (BinaryAssertionDefinition definition : bDefinitions){
			assertionManager.addAssertionDefinition(definition);
		}
		
		for (UnaryAssertionDefinition definition : uDefinitions){
			assertionManager.addAssertionDefinition(definition);
		}
		
		manager.setAssertionManager(assertionManager);
		
	}
	
	private void initEventManager(ContextManagerImpl manager){
		eventManager.start();
		manager.setEventManager(eventManager);
	}
}

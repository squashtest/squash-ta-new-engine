/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.AssertionFindSettings;
import org.squashtest.ta.backbone.engine.AssertionManager;
import org.squashtest.ta.backbone.engine.CommandFindSettings;
import org.squashtest.ta.backbone.engine.CommandManager;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EventManager;
import org.squashtest.ta.backbone.engine.ResourceConversionSettings;
import org.squashtest.ta.backbone.engine.ResourceConverterManager;
import org.squashtest.ta.backbone.engine.ResourceLoader;
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings;
import org.squashtest.ta.backbone.engine.event.ContextualStatusUpdateEvent;
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.engine.wrapper.ResourceProvider;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.backbone.exception.AmbiguousConversionException;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.backbone.tools.ElementUtils;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult.TargetStatus;

public class ContextManagerImpl implements ContextManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContextManager.class);

	private static final ElementUtils ELEMENT_UTILS = new ElementUtils();

	private Map<String, TargetWrapper> targets = new HashMap<>();

	private Properties globalParams = new Properties();

	/* the following managers should be configured then passed to the context manager */

	private ResourceLoader resourceLoader;

	private ResourceConverterManager converterManager;

	private CommandManager commandManager;

	private AssertionManager assertionManager;

	private EventManager eventManager;

	private Map<ResourceName, ResourceWrapper> ecosystemResources = new HashMap<>();

	private Set<String> targetsNames;

	private Set<TargetInitialisationResult> targetInitialisationResults;
        
        private ResourceProvider builtinScope;

	/* ***************** building ******************** */

	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}

	public void setResourceLoader(ResourceLoader loader) {
		this.resourceLoader = loader;
	}

	public ResourceConverterManager getResourceConverterManager() {
		return converterManager;
	}

	public void setResourceConverterManager(ResourceConverterManager manager) {
		this.converterManager = manager;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}

	public void setCommandManager(CommandManager manager) {
		this.commandManager = manager;
	}

	public AssertionManager getAssertionManager() {
		return assertionManager;
	}

	public void setAssertionManager(AssertionManager manager) {
		this.assertionManager = manager;
	}

	public void setEventManager(EventManager manager) {
		eventManager = manager;
	}

        public void setBuiltinScope(ResourceProvider p){
            this.builtinScope=p;
        }
        
	/* ****************************** lifecycle ************************************************ */

	@Override
	public void initAll() {
		LOGGER.debug("Initializing all resources and needed targets.");
		resourceLoader.initAll();
		// this set is decremented to make us sure we are treated ALL the concerned targets
		Set<String> targetsInTest = targetsNames;
		targetInitialisationResults = new HashSet<>();
		Set<String> toRemoveFromContext = new HashSet<>();
		for (TargetWrapper wrapper : targets.values()) {
			String name = wrapper != null ? wrapper.getName() : null;
			if (targetsNames.contains(name)) {
				targetsInTest.remove(name);
				TargetStatus status = initTarget(wrapper);
				if (status.equals(TargetStatus.ERROR)) {
					toRemoveFromContext.add(name);
				}
			}
		}
		// we add the target given in the test but not found or wrongly configured in the workspace
		for (String targetName : targetsInTest) {
			addUnknownTargetResult(targetName);
		}
		// We remove all the not initialized target from the context
		toRemoveFromContext.addAll(targetsInTest);
		for (String targetName : toRemoveFromContext) {
			targets.remove(targetName);
		}
	}

	private TargetStatus initTarget(TargetWrapper wrapper) {
		TargetStatus status = TargetStatus.ERROR;
		try {

			boolean init = wrapper.init();
			status = init ? TargetStatus.OK : TargetStatus.ERROR;
		} catch (Exception e) {
			status = TargetStatus.ERROR;
			String targetName = (wrapper == null ? "null" : wrapper.getName());
			LOGGER.warn("Target {} could not be initialized. Some tests may fail.",targetName, e);
		}
		addKnownTargetResult(wrapper, status);
		// If the target can't be fully initialized properly then we try to cleanup
		if (status.equals(TargetStatus.ERROR)) {
			wrapper.cleanup();
		}
		return status;
	}

	@Override
	public void resetAll() {
		LOGGER.debug("Resetting all resources and targets.");
		resourceLoader.resetAll();
		for (TargetWrapper w : targets.values()) {
			try {
				if (targetsNames.contains(w.getName())) {
					w.reset();
				}
			} catch (Exception e) {
				String targetName = (w == null ? "null" : w.getName());
				LOGGER.warn("Target {} could not be reset. Some tests may fail.",targetName, e);
			}
		}
	}

	@Override
	public void cleanupAll() {
		LOGGER.debug("Cleaning all resources and needed targets.");
		resourceLoader.cleanupAll();
		for (TargetWrapper w : targets.values()) {
			try {
				if (targetsNames.contains(w.getName())) {
					w.cleanup();
				}
			} catch (Exception e) {
				String targetName = (w == null ? "null" : w.getName());
				LOGGER.warn("Target {} could not be cleaned.",targetName, e);
			}
		}
                // cleaning up resources used by converters might be a good idea at that stage
                converterManager.cleanUp();
                // as we are closing everything, let's nudge event processing a bit:
		eventManager.shutdown();
	}

	/* ****************************** resgistrers ********************************************** */

        @Override
	public void registerRepository(RepositoryWrapper repository) {
		resourceLoader.addRepository(repository);
	}

        @Override
	public void registerTarget(TargetWrapper target) {
		ELEMENT_UTILS.checkUniqueness(targets.keySet(), target.getName());
		targets.put(target.getName(), target);
	}

        @Override
	public void registerConverterDefinition(ConverterDefinition definition) {
		this.converterManager.addConverterDefinition(definition);
	}

        @Override
	public void registerCommandDefinition(CommandDefinition definition) {
		this.commandManager.addCommandDefinition(definition);
	}

        @Override
	public void registerAssertionDefinition(BinaryAssertionDefinition definition) {
		this.assertionManager.addAssertionDefinition(definition);
	}

        @Override
	public void registerAssertionDefinition(UnaryAssertionDefinition definition) {
		this.assertionManager.addAssertionDefinition(definition);
	}

	/* ****************************** unregistrers ************************************************ */

        @Override
	public void unregisterRepository(RepositoryWrapper repository) {
		resourceLoader.removeRepository(repository.getName());
	}

        @Override
	public void unregisterRepository(String repositoryName) {
		resourceLoader.removeRepository(repositoryName);
	}

        @Override
	public void unregisterTarget(TargetWrapper target) {
		targets.remove(target.getName());
	}

        @Override
	public void unregisterTarget(String targetName) {
		targets.remove(targetName);
	}

        @Override
	public void unregisterConverterDefinition(ConverterDefinition definition) {
		converterManager.removeConverterDefinition(definition);
	}

        @Override
	public void unregisterConverterDefinition(Nature input, Nature output, Nature category) {
		converterManager.removeConverterDefinition(input, output, category);
	}

        @Override
	public void unregisterCommandDefinition(CommandDefinition definition) {
		commandManager.removeCommandDefinition(definition);
	}

        @Override
	public void unregisterCommandDefinition(Nature resourceNature, Nature targetNature, Nature commandCategory) {
		commandManager.removeCommandDefinition(resourceNature, targetNature, commandCategory);
	}

        @Override
	public void unregisterAssertionDefinition(BinaryAssertionDefinition definition) {
		assertionManager.removeAssertionDefinition(definition);
	}

        @Override
	public void unregisterAssertionDefinition(Nature actualNature, Nature expectedNature, Nature assertionCategory) {
		assertionManager.removeAssertionDefinition(actualNature, expectedNature, assertionCategory);
	}

        @Override
	public void unregisterAssertionDefinition(UnaryAssertionDefinition definition) {
		assertionManager.removeAssertionDefinition(definition);
	}

        @Override
	public void unregisterAssertionDefinition(Nature actualNature, Nature assertionCategory) {
		assertionManager.removeAssertionDefinition(actualNature, assertionCategory);
	}

	/* ******************************** getters ***************************************************** */

        @Override
	public ResourceWrapper getResource(ResourceName resourceName) {
		return simpleLoad(resourceName);
	}

        @Override
	public ResourceWrapper getResource(ResourceName resourceName, String repositoryName) {

		ResourceLoadingSettings settings = new ResourceLoadingSettingsImpl();
		settings.setResourceName(resourceName);
		settings.setRepositoryName(repositoryName);
		return resourceLoader.loadResource(settings);
	}

	/* in this case the resource might be specifically converted into something */
        @Override
	public ResourceWrapper getResource(ResourceLoadingSettings settings) {

		// check the most important parameter : the resource name

		ResourceName name = settings.getResourceName();

		if (name == null) {
			ResourceNotFoundException ex = new ResourceNotFoundException(
					"cannot load resource: name was not specified.");
			LOGGER.error(ex.getMessage());
			throw ex;
		}

		// now load a copy of the data
		return resourceLoader.loadResource(settings);
	}

        @Override
	public TargetWrapper getTarget(String targetName) {
		return targets.get(targetName);
	}

	/* *********************************** conversion ************************************************ */

        @Override
	public Collection<ResourceConverterHandler> getConverters(ResourceConversionSettings settings) {
	
            return converterManager.getAllConvertersByName(
                                                        settings.getOriginalNature(), 
                                                        settings.getDesiredNature(),
                                                        settings.getConverterCategory());
	}

        @Override
	public ResourceWrapper convertResource(ResourceWrapper resource, ResourceConversionSettings settings) {

		String desiredType = settings.getDesiredNature();
		String identifier = settings.getConverterCategory();

		Collection<ResourceConverterHandler> converters;
		ResourceConverterHandler converter = null;

		String inputNature = resource.getNature().getName();
		converters = converterManager.getAllConvertersByName(inputNature, desiredType, identifier);

            switch (converters.size()) {
                case 1:
                    converter = converters.iterator().next();
                    ResourceWrapper result = converter.convert(resource);
                    converter.cleanUp();
                    return result;
                case 0:
                    throw new ImpossibleConversionException("Impossible to convert resource " + resource.getName()
                            + " : no converter found from nature " + resource.getNature().getName() + " to nature "
                            + desiredType);
                default:
                    throw new AmbiguousConversionException(buildAmbiguousConversionMessage(resource, desiredType, converters));
            }
	}

        @Override
	public Collection<CommandHandler> getCommand(CommandFindSettings settings) {

		return commandManager.getAllCommandsByName(
                                                        settings.getResourceNatureName(),
                                                        settings.getTargetNatureName(),
                                                        settings.getCommandIdentifier());

	}

	/* ******************** assertions **************************** */

        @Override
	public Collection<BinaryAssertionHandler> getBinaryAssertion(AssertionFindSettings settings) {

		return assertionManager.getAllBinaryAssertionsByName(
                                                                    settings.getActualResultNatureName(), 
                                                                    settings.getExpectedResultNatureName(),
                                                                    settings.getAssertionCategory());

	}

        @Override
	public Collection<UnaryAssertionHandler> getUnaryAssertion(AssertionFindSettings settings) {

		return assertionManager.getAllUnaryAssertionsByName(
                                                                    settings.getActualResultNatureName(), 
                                                                    settings.getAssertionCategory());
	}

	@Override
	public Map<ResourceName, ResourceWrapper> getEcosystemResources() {
		return ecosystemResources;
	}

	public void setTargetsNames(Set<String> targetsNames) {
		this.targetsNames = targetsNames;
	}

	@Override
	public Set<String> getTargetsNames() {
		return targetsNames;
	}

	@Override
	public Set<TargetInitialisationResult> getTargetInitialisationResults() {
		return targetInitialisationResults;
	}

	/* *************************events************************** */
	@Override
	public void postEvent(ContextualStatusUpdateEvent<?> event) {
		eventManager.postEvent(event);
	}

	/* ********************** non public *********************** */

	private String buildAmbiguousConversionMessage(ResourceWrapper resource, String desiredType,
			Collection<ResourceConverterHandler> converters) {
		StringBuilder message = new StringBuilder("Impossible to convert resource ").append(resource.getName())
				.append(" : multiple converters found from nature ").append(resource.getNature().getName())
				.append(" to nature ").append(desiredType).append(" : ");
		for (ResourceConverterHandler c : converters) {
			message.append(c.getConverterCategory().getName()).append(" , ");
		}
		if (!converters.isEmpty()) {
			message.setLength(message.length() - " , ".length());
		}
		return message.toString();
	}

	private ResourceWrapper simpleLoad(ResourceName name) {
		ResourceLoadingSettings settings = new ResourceLoadingSettingsImpl();
		settings.setResourceName(name);
		return resourceLoader.loadResource(settings);
	}

	private void addKnownTargetResult(TargetWrapper w, TargetStatus status) {
		// we register only "real" targets
		if (!w.getName().equals(VoidTarget.INSTANCE_NAME)) {
			targetInitialisationResults
					.add(new TargetInitialisationResult(w.getName(), w.getNature().getName(), status));
		}
	}

	private void addUnknownTargetResult(String targetName) {
		targetInitialisationResults.add(new TargetInitialisationResult(targetName, "unknown", TargetStatus.NOT_FOUND));
	}

	@Override
	public Properties getGlobalParams() {
		return globalParams;
	}

	/**
	 * Method to set to the context manager the global parameters associated to the test suite
	 * 
	 * @param globalParams
	 *            The global paramters to add
	 */
	public void setGlobalParams(Properties globalParams) {
		this.globalParams = globalParams;
	}

    @Override
    public ResourceProvider getSuiteBuiltinScope() {
        return builtinScope;
    }
}

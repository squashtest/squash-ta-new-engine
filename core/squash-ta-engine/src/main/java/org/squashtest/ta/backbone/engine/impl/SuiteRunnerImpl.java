/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.EcosystemRunner;
import org.squashtest.ta.backbone.engine.SuiteRunner;
import org.squashtest.ta.backbone.engine.SuiteRunnerSettings;
import org.squashtest.ta.backbone.engine.event.TestSuiteCompletionEvent;
import org.squashtest.ta.backbone.engine.event.TestSuiteLaunchEvent;
import org.squashtest.ta.backbone.test.DefaultSuiteResult;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.SuiteResult;

public class SuiteRunnerImpl implements SuiteRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(SuiteRunnerImpl.class);
	private SuiteRunnerSettings settings;
	private TestSuite suite;

        @Override
	public void configure(SuiteRunnerSettings settings) {
		this.settings = settings;
	}

	@Override
	public void setSuite(TestSuite suite) {
		this.suite = suite;
	}

        @Override
	public SuiteResult execute() {

		TempFileUtils.setExecutionName(suite.getName());		
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Begins executing suite " + suite.getName());
		}
		DefaultSuiteResult result = new DefaultSuiteResult(suite.getName());
		result.setProjectDescription(suite.getProjectGroupId(), suite.getProjectArtifactId(), suite.getProjectVersion());
		settings.getContextManager().initAll();
		result.setTargetInitialisationResults(settings.getContextManager().getTargetInitialisationResults());
		settings.getContextManager().postEvent(new TestSuiteLaunchEvent(suite));

		for (Ecosystem ecosystem : suite) {
			EcosystemRunner runner = configureRunner(ecosystem);
			EcosystemResult ecosystemResult = runner.run();
			result.addTestEcosystemResult(ecosystemResult);
		}

		LOGGER.debug("Test suite execution complete.");
		// suite completion event must be published before cleanup, otherwise it will be ignored!
		settings.getContextManager().postEvent(new TestSuiteCompletionEvent(result));
		settings.getContextManager().cleanupAll();
		LOGGER.debug("Context manager cleanup complete.");
		
		return result;
	}

	private EcosystemRunner configureRunner(Ecosystem ecosystem) {
		EcosystemRunnerImpl runner = new EcosystemRunnerImpl();

		runner.setEcosystem(ecosystem);
		runner.setContextManager(settings.getContextManager());

		return runner;
	}

}

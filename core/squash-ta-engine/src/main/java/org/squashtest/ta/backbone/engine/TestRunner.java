/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine;

import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * a TestRunner will run a {@link Test} within the context provided by the {@link ContextManager} it receives.
 * 
 * @author bsiri
 */
public interface TestRunner {
	
	/**
	 * Sets the ContextManager, which holds anything the runner needs to perform its task.
	 * 
	 * @param manager
	 */
	void setContextManager(ContextManager manager);
	

	/**
	 * sets the test to be run.
	 * 
	 * @param test
	 */
	void setTest(Test test);
	
	/**
	 * 
	 * @return the test, once it has been ran.
	 */
	Test getTest();
	
	/**
	 * Will run the test and return it with complementary informations regarding its execution.
	 * You may get the result using {@link #getTest()}.
	 * @return 
	 */
	TestResult runTest();
        
}

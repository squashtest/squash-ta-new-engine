/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.init;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.exception.EngineInitException;

/**
 * This object probes the engine classpath to list all declared packages that
 * define engine components. To declare packages in a given classpath element
 * (typically an engine plugin extension jar), the classpath element must
 * include a manifest named <code>META-INF/squashTA-components.mf</code>.
 * @see ComponentManifestParser (parser documentation).
 * 
 * @author edegenetais
 * 
 */
public class ComponentPackagesEnumerator {
	/** Logger of the class. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ComponentPackagesEnumerator.class);

	/**
	 * Parser as a private field to decouple by allowing separated testing of
	 * the enumerator and parser for unit testing.
	 * 
	 */
	private ComponentManifestParser parser=new ComponentManifestParser();
	
	/**
	 * Loads the squashTA engine component configuration.
	 * 
	 * @return the component configuration. This objects lists all declared plugins with their declared component packages and excluded elements.
	 */
	public ComponentConfiguration getComponentConfiguration() {
		ComponentConfiguration componentConfiguration = new ComponentConfiguration();

		LOGGER.info("Listing sqhashTA engine component packages");
		try {
			ClassLoader cl = getClass().getClassLoader();
			Enumeration<URL> manifestUrlList = cl
					.getResources("META-INF/squashTA-components.mf");

			LOGGER.debug("Manifest URL have been listed, enumerating...");
			while (manifestUrlList.hasMoreElements()) {
				URL manifestURL = manifestUrlList.nextElement();

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("..Listing packages from: " + manifestURL.toExternalForm());
				}

				parser.loadManifest(componentConfiguration, manifestURL);
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Enumeration complete. Found "
						+ componentConfiguration.getBasePackages().size()
						+ " base component packages and "
						+ componentConfiguration.getExcludedElements().size()
						+ " package exclusion specifications.");
			}
		} catch (IOException e) {
			throw new EngineInitException(
					"Failure during component package listing", e);
		}
		return componentConfiguration;
	}
}

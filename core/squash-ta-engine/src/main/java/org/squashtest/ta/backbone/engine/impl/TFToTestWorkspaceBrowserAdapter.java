/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.facade.TFTestWorkspaceBrowser;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;

/**
 * This adapter allows pre-TF runners to run new versions of the Squash TA engine.
 * Interface extensions and the use of this adapter were prefered to java 8 default methods
 * in order to avoid forcing JDK7 to JDK8 transition on users.
 * This two-tiered interface design will remain for API compatibility, but default methods
 * may be used if further framework interface extensions are made in a 100% post-JDK7 world
 * (aka when we've had a decent deprecation time and/or JDK7 will have been EOL long enough).
 * 
 * @author edegenetais
 */
class TFToTestWorkspaceBrowserAdapter implements TFTestWorkspaceBrowser {

    private final TestWorkspaceBrowser resourceBrowser;

    public TFToTestWorkspaceBrowserAdapter(TestWorkspaceBrowser resourceBrowser) {
        this.resourceBrowser = resourceBrowser;
    }

    @Override
    public Map<String, Resource<?>> getExploitationBuiltinResources() {
        return Collections.emptyMap();
    }

    @Override
    public List<URL> getTargetsDefinitions() {
        return resourceBrowser.getTargetsDefinitions();
    }

    @Override
    public List<URL> getRepositoriesDefinitions() {
        return resourceBrowser.getRepositoriesDefinitions();
    }

    @Override
    public ResourceRepository getDefaultResourceRepository() {
        return resourceBrowser.getDefaultResourceRepository();
    }

    @Override
    public File getDefaultResourceFile() {
        return resourceBrowser.getDefaultResourceFile();
    }

}

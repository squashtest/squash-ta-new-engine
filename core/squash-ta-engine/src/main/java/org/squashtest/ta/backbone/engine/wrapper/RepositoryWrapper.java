/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.wrapper;

import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.ResourceGenerator;


public class RepositoryWrapper implements ResourceProvider {

    private ResourceRepository repository;
    protected Nature repositoryCategory;
    protected String name;
    
    
	public RepositoryWrapper(String name, Nature repositoryCategory, ResourceRepository repo){
		this.name=name;
		this.repositoryCategory=repositoryCategory;
		this.repository=repo;
	}    
	
        @Override
	public Nature getRepositoryCategory() {
		return repositoryCategory;
	}
	
	public void setRepositoryCategory(Nature repositoryCategory) {
		this.repositoryCategory = repositoryCategory;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void wrap(ResourceRepository repo){
		this.repository=repo;
	}
	
	public ResourceGenerator toGenerator(){
		return new GenericResourceGenerator(name, repositoryCategory.getName(), repository.getConfiguration());
	}
	
	public ResourceWrapper findResources(ResourceName resourceName){
		FileResource maybeFound = repository.findResources(resourceName.getName());
		if (maybeFound!=null){
			return new ResourceWrapper(Nature.FILE_RESOURCE_NATURE, resourceName, maybeFound, toGenerator());
		}
		else{
			return null;
		}
	}
	
	public void cleanup(){
		repository.cleanup();
	}
	
	public void init(){
		repository.init();
	}
	
	public void reset(){
		repository.reset();
	}

    public String getName() {
        return name;
    }
	
}

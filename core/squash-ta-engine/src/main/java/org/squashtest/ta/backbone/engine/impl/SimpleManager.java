/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.definition.EngineComponentDefinition;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.exception.DuplicateFactoryException;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;
import org.squashtest.ta.backbone.tools.AggregativeChristmasTree;
import org.squashtest.ta.backbone.tools.MultiDimensionMap;


/**
 * 
 * That class puts together common actions among some managers. Basically it stores component definitions
 * in a map against a key made of three elements. See other classes using it (like {@link ResourceConverterManager} to see how it works.   
 *  
 * @author bsiri
 *
 * @param <CLASS> the class of engine component we want to store in our map
 */
class SimpleManager<DEFINITION extends EngineComponentDefinition<?>> {
	

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleManager.class); 
	
	/** Definition of dimension 3 */
	private static final int DIM3=3;
	/** Elements are stored in a 3 dimensional map */
	private MultiDimensionMap<DEFINITION> elements = new AggregativeChristmasTree<DEFINITION>(DIM3);
	

	
	/**
	 * Will refuse to perform this operation if a definition having the same (ID1, ID2, ID3) is already present.
	 * 
	 * @param definition
	 */
	
	void addDefinition(DEFINITION definition){		
		try{
			elements.put(definition, definition.getFirstNature(), definition.getSecondNature(), definition.getCategory());			
		}catch(NotUniqueEntryException exe){
			DuplicateFactoryException ex = buildException(definition);
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn(ex.getMessage());
			}
			ex.initCause(exe);/*(so sad sonar shuns initCause!)*/
			throw ex;//NOSONAR 
		}			
	}
	

	
	void removeDefinition(Nature id1, Nature id2, Nature id3){
		elements.remove(id1, id2, id3);
	}
	
	void removeDefinition(DEFINITION definition){
		elements.remove(definition.getFirstNature(), definition.getSecondNature(), definition.getCategory());
	}
	
	/**
	 * <p>
	 * 	Depending on the given parameter, will return the list matching registered elements.
	 * 	Each parameter accepts null, but the more the caller provides the more precise will be the result. 
	 *  Theoretically if the user supplies all the arguments the returned list shall be of size 0 of 1. 
	 * </p>
	 * 
	 * 
	 * @param resourceNature
	 * @param targetNature
	 * @param name
	 * @return a Collection of commands fitting the description (never null).
	 */
	Collection<DEFINITION> getAllDefinitions(Nature id1, Nature id2, Nature id3){
		Object[] key = new Object[3];
		
		key[0] = convertToAny(id1);
		key[1] = convertToAny(id2);
		key[2] = convertToAny(id3);
		
		return elements.get(key);
	}
	
	
	protected Object convertToAny(Object key){
		if (key==null){
			return MultiDimensionMap.ANY;
		}else{
			return key;
		}
	}

	
	protected DuplicateFactoryException buildException(DEFINITION definition){
		
		Nature n1 = definition.getFirstNature();
		Nature n2 = definition.getSecondNature();
		Nature category = definition.getCategory();
		DEFINITION alreadyMapped = elements.get(n1, n2, category).iterator().next();
		
		return new DuplicateFactoryException("Cannot register '"+definition.getComponentClass().getName()+"' mapped to ['"+
				n1.getName()+"', '"+
				n2.getName()+"', '"+ 
				category.getName()+"' ] : class '"+
				alreadyMapped.getComponentClass().getName()+"' is already bound for those attributes.");		
	}
	
}

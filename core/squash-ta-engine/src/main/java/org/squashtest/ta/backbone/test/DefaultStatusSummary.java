/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.StatusSummary;

/**
 * Summary of the execution status:
 * <ul>
 * <li>did it fail or succeed</li>
 * <li>if it failed, details about the failure</li>
 * </ul>
 * @author edegenetais
 * @deprecated no more used since 1.6.0 
 */
@Deprecated
public class DefaultStatusSummary implements StatusSummary {
	
	private ExecutionDetails failureExecutionDetails;
	private Phase failurePhase;
	private boolean failed;
	private Integer failureContextIdentifier;

	@Override
	public Phase getFailurePhase() {
		return failurePhase;
	}

	@Override
	public ExecutionDetails getFailureDetails() {
		ExecutionDetails failureDetails=null;
		if(failureExecutionDetails==null || failurePhase==null){
			failureDetails=null;
		}else{
			failureDetails=failureExecutionDetails;
		}
		return failureDetails;
	}
	
	/**
	 * Register instruction execution failure. 
	 * @param details the instruction execution details.
	 * @param phase the failure phase
	 */
	public void fail(ExecutionDetails details,Integer localContextId,Phase phase){
		failed=true;
		failureExecutionDetails=details;
		failurePhase=phase;
	}



	@Override
	public boolean isFailed() {
		return failed;
	}



	@Override
	public Integer getFailureContextIdentifier() {
		return failureContextIdentifier;
	}
}

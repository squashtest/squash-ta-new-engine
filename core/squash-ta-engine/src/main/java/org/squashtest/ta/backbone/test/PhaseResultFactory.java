/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.Phase;

/**
 * Factory class for {@link AbstractPhaseResult} objects.
 * 
 * @author bfranchet
 * 
 */
public final class PhaseResultFactory {

	/** Private constructor */
	private PhaseResultFactory() {

	}

	/**
	 * Return the {@link AbstractPhaseResult} correctly initialized for the current ecosystem phase 
         * and test phase.
	 * 
	 * @param ecosystemPhase
	 *            The ecosystem phase
	 * @param phase
	 *            The test phase (make sense only for RUN ecosystem phase)
	 * @return an initialized result phase
	 */
	public static AbstractPhaseResult getPhaseResult(EcosysPhase ecosystemPhase, Phase phase) {
		if (ecosystemPhase.isEcosystemRun()) {
			return new DefaultPhaseResult(phase);
		} else {
			return new DefaultPhaseResult();
		}
	}

	/**
	 * Return a {@link AbstractPhaseResult} for a not running phase correctly initialized a
         * ccording to the {@link Phase} given in argument.
	 * 
	 * @param phase
	 *            The current test phase
	 * @return An result for a not running phase
	 */
	public static AbstractPhaseResult getNotRunPhaseResult(Phase phase) {
		AbstractPhaseResult phaseResult = getPhaseResult(EcosysPhase.RUN, phase);
		phaseResult.setPhaseStatus(GeneralStatus.NOT_RUN);
		return phaseResult;
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.engine.ResourceConverterManager;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.ResourceConverter;

public class ResourceConverterManagerImpl implements ResourceConverterManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceConverterManagerImpl.class);
    
	private ObjectFactory factory;
	
	private Map<String, Nature> natureMap = new HashMap<String, Nature>();
	private Map<String, Nature> categoryMap = new HashMap<String, Nature>();
	
	private ConverterMap definitionManager = new ConverterMap();
        
        private List<ResourceConverter<?,?>> converterInstances=new ArrayList<>();
	
	public void setObjectFactory(ObjectFactory factory){
		this.factory=factory;
	}

	/* ********************** interface implementation ****************** */
	
	public void addConverterDefinition(ConverterDefinition definition){	
		mapIfUnknown(definition.getFirstNature());
		mapIfUnknown(definition.getSecondNature());
		addIfUnknown(definition.getCategory());
		definitionManager.addDefinition(definition);
	}
	
	
	public void removeConverterDefinition(ConverterDefinition definition){
		definitionManager.removeDefinition(definition);
	}

	public void removeConverterDefinition(Nature inputNature, Nature outputNature, Nature category) {
		definitionManager.removeDefinition(inputNature, outputNature, category);
	}


	public Collection<ResourceConverterHandler> getAllConverters(Nature inputNature, Nature outputNature, Nature category) {
		Collection<ResourceConverterHandler> result = new ArrayList<ResourceConverterHandler>();
		Collection<ConverterDefinition> definitions = definitionManager.getAllDefinitions(inputNature, outputNature, category);
		for (ConverterDefinition def : definitions){
			result.add(createHandler(def));
		}		
		return result;
	}


	public Collection<ResourceConverterHandler> getAllConvertersByName(String inputNatureName, String outputNatureName,String categoryName) {
		
		checkIfNatureKnownOrFail(inputNatureName);
		Nature inputNature = natureMap.get(inputNatureName);
		
		checkIfNatureKnownOrFail(outputNatureName);
		Nature outputNature = natureMap.get(outputNatureName);
		
		checkIfCategoryKnownOrFail(categoryName);
		Nature category = categoryMap.get(categoryName);
		
		return getAllConverters(inputNature, outputNature, category);
			
	}
	
	/* ***************************** private code *********************** */
	
	private void mapIfUnknown(Nature nature){
		if (! natureMap.containsKey(nature.getName())){
			natureMap.put(nature.getName(), nature);
		}
	}
	
	private void addIfUnknown(Nature category){
		if (! categoryMap.containsKey(category.getName())){
			categoryMap.put(category.getName(), category);
		}
	}
	
	//null is always valid, as a wildcard
	private void checkIfNatureKnownOrFail(String natureName){ 
		if ((natureName!=null) &&(! natureMap.containsKey(natureName))){
			throw new ImpossibleConversionException("Cannot perform conversion : nature "+natureName+" is unknown");
		}
	}
	
	private void checkIfCategoryKnownOrFail(String categoryName){
		if ((categoryName!=null) && (! categoryMap.containsKey(categoryName))){
			throw new ImpossibleConversionException("Cannot perform conversion : specified converter "+categoryName+" is unknown");
		}
	}
	
	
	private ResourceConverterHandler createHandler(ConverterDefinition def){
		ResourceConverter<?,?> instance = factory.newInstance(def.getComponentClass());
                this.converterInstances.add(instance);
		return new ResourceConverterHandler(def.getFirstNature(), def.getSecondNature(), def.getCategory(), instance);
	}

    @Override
    public void cleanUp() {
        List<ResourceConverter<?,?>> doneList=new ArrayList<ResourceConverter<?,?>>();
        for(ResourceConverter<?,?> converter:converterInstances){
            try{
                converter.cleanUp();
                doneList.add(converter);
            }catch(Exception e){
                LOGGER.warn("Failed to perform cleanup for converter {}, resource leaks may happen!",converter.getClass().getAnnotation(TAResourceConverter.class).value(),e);
            }
        }
        converterInstances.removeAll(doneList);
        LOGGER.debug("ResourceConverterManager cleanup complete.");
    }
	
	/* ************ inner class to mask the generics boiler plate ************** */

	private static class ConverterMap extends SimpleManager<ConverterDefinition>{}
	
	/* **************************** findAnyWay ********************************* */
	
}

	
	


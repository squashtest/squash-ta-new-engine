/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import org.squashtest.ta.backbone.engine.impl.InstructionRunner;
import org.squashtest.ta.backbone.engine.impl.InstructionRunnerFactory;
import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction;
import org.squashtest.ta.framework.test.instructions.CommentaryInstruction;
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.InlineMetaInstruction;
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction;
import org.squashtest.ta.framework.test.instructions.MacroInstruction;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.instructions.TestInstructionVisitor;
import org.squashtest.ta.framework.test.instructions.UnaryAssertionInstruction;

/**
 * This instruction visitor is the factory to provide the test runner with the right instruction runner for any given
 * instruction.
 * 
 * @author edegenetais
 * 
 */
public class DefaultInstructionRunnerFactory implements InstructionRunnerFactory, TestInstructionVisitor {

	private ThreadLocal<InstructionRunner> runnerHolder = new ThreadLocal<>();

        @Override
	public void accept(LoadResourceInstruction instruction) {
		DefaultLoadResourceRunner runner = new DefaultLoadResourceRunner(instruction);
		runnerHolder.set(runner);
	}

        @Override
	public void accept(ConvertResourceInstruction instruction) {
		DefaultConvertResourceRunner runner = new DefaultConvertResourceRunner(instruction);
		runnerHolder.set(runner);
	}

        @Override
	public void accept(ExecuteCommandInstruction instruction) {
		DefaultExecuteCommandRunner runner = new DefaultExecuteCommandRunner(instruction);
		runnerHolder.set(runner);
	}

        @Override
	public void accept(BinaryAssertionInstruction instruction) {
		DefaultBinaryAssertionRunner runner = new DefaultBinaryAssertionRunner(instruction);
		runnerHolder.set(runner);
	}

        @Override
	public void accept(UnaryAssertionInstruction instruction) {
		DefaultUnaryAssertionRunner runner = new DefaultUnaryAssertionRunner(instruction);
		runnerHolder.set(runner);
	}

        @Override
	public void accept(DefineResourceInstruction instruction) {
		DefaultDefineTempResourceRunner runner = new DefaultDefineTempResourceRunner(instruction);
		runnerHolder.set(runner);
	}

        @Override
	public InstructionRunner getRunner(TestInstruction instruction) {
		instruction.visit(this);
		return runnerHolder.get();
	}

	@Override
	public void accept(ResetLocalContextInstruction instruction) {
		DefaultResetLocalContextRunner runner = new DefaultResetLocalContextRunner(instruction);
		runnerHolder.set(runner);
	}

	@Override
	public void accept(InlineMetaInstruction instruction) {
		// No op. A meta instruction is only a wrapper, each instruction of the meta instruction will use its own
		// runner.
	}

	@Override
	public void accept(MacroInstruction instruction) {
		// No op. A meta instruction is only a wrapper, each instruction of the meta instruction will use its own
		// runner.
	}

	@Override
	public void accept(CommentaryInstruction instruction) {
		DefaultCommentaryRunner runner = new DefaultCommentaryRunner(instruction);
		runnerHolder.set(runner);
	}

}

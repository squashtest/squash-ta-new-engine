/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine;

import org.squashtest.ta.framework.test.result.GeneralStatus;

/**
 * An InstructionFlowStrategy defines the rules for the following two decisions after running an execution:
 * <ul>
 * <li>should the instruction group execution go on</li>
 * <li>how should the instruction result {@link GeneralStatus} 
 *          be aggregated to the current instruction group status.</li>
 * </ul>
 * 
 * @author edegenetais
 * 
 */
public interface InstructionFlowStrategy {
	/**
	 * Decides if instruction group execution must begin or not, based on current aggregate status.
	 * 
	 * @param aggregateStatus
	 *            The current aggregate status, if it is defined by a previous instruction group.
	 * @param hasOnlyVerifyFailOrError
	 *            <code>true</code> if fail or error in previous phase(s) 
         *              was (were) thrown only by VERIFY instruction(s) 
	 * @return <code>true</code> if instruction group execution has to begin, <code>false</code> otherwise.
	 */
	boolean beginGroupExecution(GeneralStatus aggregateStatus, boolean hasOnlyVerifyFailOrError);

	/**
	 * Decides if the instruction group execution has to go on or stop based on the last instruction's exit
	 * {@link GeneralStatus}.
	 * 
	 * @param instructionStatus
	 *            the instruction's last general status.
	 * @param continueOnFailOrError
	 *            <code>true</code> if the execution should continue on fail or error
	 * @return <code>true</code> if execution has to go on, <code>false</code> if it has to stop.
	 */
	boolean continueExecution(GeneralStatus instructionStatus, boolean continueOnFailOrError);

	/**
	 * Compute the new aggregate instruction group status after execution of an instruction 
         * based on the instruction's exit {@link GeneralStatus} and the current value of the aggregate status.
	 * 
	 * @param aggregateStatus
	 *            current value of the group status.
	 * @param instructionStatus
	 *            new instruction exit status to aggregate to the group status.
	 * @return
	 */
	GeneralStatus aggregate(GeneralStatus aggregateStatus, GeneralStatus instructionStatus);

}

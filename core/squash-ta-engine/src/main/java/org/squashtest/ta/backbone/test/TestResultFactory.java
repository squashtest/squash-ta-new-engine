/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Date;

import org.squashtest.ta.framework.test.result.GeneralStatus;

public final class TestResultFactory {

	/** Private constructor */
	private TestResultFactory() {

	}

	/**
	 * @return Return default {@link AbstractTestResult}
	 */
	public static AbstractTestResult getTestResult() {
		return new DefaultTestResult();
	}

	/**
	 * Return a {@link AbstractTestResult} initialized with the name given in arguments and with status NOT_RUN for not
	 * run test
	 * 
	 * @param testName
	 *            The test name
         * @param testId
         *            The test ID
	 * @return An initialized test result
	 */
	public static AbstractTestResult getNotRunTestResult(String testName, String testId) {
		DefaultTestResult result = new DefaultTestResult();
		result.setName(testName);
		result.setTestId(testId);
		result.setStatus(GeneralStatus.NOT_RUN);
		Date date = new Date();
		result.setStartTime(date);
		result.setEndTime(date);
		return result;
	}
	
	/**
	 * Return a {@link AbstractTestResult} initialized with the name given in arguments and with status NOT_FOUND for not
	 * found test
	 * 
	 * @param testName
	 *            The test name
         * @param testId
         *            The test ID
	 * @return An initialized test result
	 */
	public static AbstractTestResult getNotFoundTestResult(String testName, String testId) {
		DefaultTestResult result = new DefaultTestResult();
		result.setName(testName);
		result.setTestId(testId);
		result.setStatus(GeneralStatus.NOT_FOUND);
		Date date = new Date();
		result.setStartTime(date);
		result.setEndTime(date);
		return result;
	}

}

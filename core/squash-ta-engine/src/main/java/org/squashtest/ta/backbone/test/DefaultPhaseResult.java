/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 * Default implementation for phase result.
 * 
 * @author bfranchet
 * 
 */
public class DefaultPhaseResult implements AbstractPhaseResult {

	/** List of all instructions of the phase */
	private List<ExecutionDetails> allInstructions = new LinkedList<>();

	/** List of instructions which failed or were in error */
	private List<ExecutionDetails> failedInstructions = new LinkedList<>();

	/** The phase of the test corresponding to this {@link PhaseResult} */
	private Phase phase;

	/** Status of this phase */
	private GeneralStatus phaseStatus;

	/** Phase execution start time */
	private Date startTime;

	/** Phase execution end time */
	private Date endTime;

	/** Name of the phase. In general it's the phase name */
	private String name;

	private int totalCommands = 0;
	private int totalAssertions = 0;
	private int totalInstructions = 0;

	private int totalFailedOrErrorWithContinue = 0;

	/**
	 * Constructor set to the phase attribute the phase value given in argument 
         * and set for name attribute the name of the phase.
	 * 
	 * @param phase
	 *            The phase of the test corresponding to this {@link DefaultPhaseResult}.
	 */
	public DefaultPhaseResult(Phase phase) {
		this.phase = phase;
		this.name = phase.toString();
	}

	/**
	 * Default constructor. 
         * It sets phase attribute to TEST and name attribute to "". 
         * Useful for ecosystem setup and teardown results.
	 * 
	 */
	public DefaultPhaseResult() {
		this.phase = Phase.TEST;
		this.name = "";
	}

	@Override
	public List<ExecutionDetails> getAllInstructions() {
		return allInstructions;
	}

	@Override
	public List<ExecutionDetails> getFailedInstructions() {
		return failedInstructions;
	}

	@Override
	public void addInstruction(ExecutionDetails instructionExecution, boolean continueOnFailOrError) {
		allInstructions.add(instructionExecution);
		if (instructionExecution.getStatus().isFailOrError()) {
			failedInstructions.add(instructionExecution);
			if (continueOnFailOrError) {
				totalFailedOrErrorWithContinue++;
			}
		}
		updateCounter(instructionExecution);
	}

	@Override
	public Phase getPhase() {
		return phase;
	}

	@Override
	public GeneralStatus getStatus() {
		return phaseStatus;
	}

	@Override
	public void setPhaseStatus(GeneralStatus phaseStatus) {
		this.phaseStatus = phaseStatus;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Date startTime() {
		return new Date(startTime.getTime());
	}

	@Override
	public Date endTime() {
		return new Date(endTime.getTime());
	}

	@Override
	public void cleanUp() {
		cleanUpFailed(failedInstructions);
	}

	private void cleanUpFailed(List<ExecutionDetails> failedInstructions) {
		for (ExecutionDetails executionDetail : failedInstructions) {
			if (executionDetail != null) {
				if (!executionDetail.getChildrens().isEmpty()) {
					cleanUpFailed(executionDetail.getErrorOrFailedChildrens());
				} else {
					cleanDetails(executionDetail);
				}
			}
		}
	}

	private void cleanDetails(ExecutionDetails executionDetail) {
		if (executionDetail.getResourcesAndContext() != null) {
			cleanupContext(executionDetail.getResourcesAndContext());
		}
		if (executionDetail.caughtException() instanceof AssertionFailedException) {
			AssertionFailedException assertionFailedException = (AssertionFailedException) executionDetail
					.caughtException();
                        if (assertionFailedException.getActual() != null){
			assertionFailedException.getActual().cleanUp();
                        }
			cleanupContext(assertionFailedException.getFailureContext());
		}
		for (ExecutionDetails childDetails : executionDetail.getErrorOrFailedChildrens()) {
			if (childDetails != null) {
				cleanDetails(childDetails);
			}
		}
	}

	private void cleanupContext(Collection<ResourceAndContext> failureContext) {
		for (ResourceAndContext context : failureContext) {
			Resource<?> resource = context.getResource();
			if (resource != null) {
				resource.cleanUp();
			}
		}
	}

	private void updateCounter(ExecutionDetails instructionExecution) {
		InstructionType type = instructionExecution.instructionType();
		if (!type.isInternalInstruction() && !instructionExecution.getStatus().isNotRun()) {
			totalInstructions++;
			if (type.isAssertion()) {
				totalAssertions++;
			} else if (type.isCommand()) {
				totalCommands++;
			}
		}
	}

	@Override
	public int getTotalCommands() {
		return totalCommands;
	}

	@Override
	public int getTotalAssertions() {
		return totalAssertions;
	}

	@Override
	public int getTotalInstructions() {
		return totalInstructions;
	}

	@Override
	public int getFailedOrErrorWithContinue() {
		return totalFailedOrErrorWithContinue;
	}

	/**
	 * Set the phase execution start time
	 * 
	 * @param startTime
	 *            The start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Set the phase execution end time
	 * 
	 * @param endTime
	 *            The end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;

/**
 * Factory class to return the right ContextParameter implementation according to the ecosystem phase
 * 
 * @author bfranchet
 * 
 */
public final class ContextParameterFactory {

	private ContextParameterFactory() {
	}

	/**
	 * Method to return the right {@link ContextParameter} implementation according to the ecosystem phase given in
	 * argument
	 * 
	 * @param phase
	 *            The ecosystem phase
	 * @return The {@link ContextParameter} implementation
	 */
	public static ContextParameter getInstance(EcosysPhase phase) {
		ContextParameter instance;
		switch (phase) {
		case SETUP:
			instance = new SetupEcosysContextParameterImpl();
			break;
		case RUN:
			instance = new RunEcosysContextParameterImpl();
			break;
		case TEARDOWN:
			instance = new TeardownEcosysContextParameterImpl();
			break;
		default:
			// If a new value is added to the enum and not already take into account here.
			throw new UnsupportedOperationException("The " + phase + " ecosystem phase is not supported");
		}
		return instance;
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.init;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.facade.Engine;

/**
 * This objects enumerates all engine components packages and loads the engine
 * spring context according to the configuration data extracted from the
 * detected manifests and an optional external exclude file (the latter to solve
 * conflicts between component definitions, if any).
 * 
 * @author edegenetais
 * 
 */
public class EngineLoader extends ScannerComponentLoader {
	private static final String COMPUTED_CONFIGURATION_RESOURCE_NAME = "Computed squashTA engine configuration";
	private static final String TEMPLATE_CLOSE_ERROR_MSG = "Non blocking error during engine configuration template loading.";
	private static final String SQUASH_COMPONENT_EXCLUDEFILE = "squash.component.excludefile";
	private static final String TEMPLATE_RESOURCE_NAME = "engineConfigurationTemplate.xml";
	/** Special logger for configuration debugging. */
	private static final Logger CONF_DEBUG_LOGGER = LoggerFactory.getLogger("org.squashTA.configuration");
	/**
	 * Enumerator. This is put as a private instance member to make the
	 * component more unit testable.
	 */
	private ComponentPackagesEnumerator enumerator = new ComponentPackagesEnumerator();

	public Engine load() {
		//First step: loading plugin configuration.
		ComponentConfiguration pluginConfiguration = enumerator
				.getComponentConfiguration();
		
		//Second step: loading the optional component exclude file if it is defined
		loadExcludeFile(pluginConfiguration);
		
		//Third step: loading the configuration template.
		String configurationTemplate= loadConfigurationTemplate();
		
		//Fourth step: inserting the plugin configuration into the configuration template.
		String configurationString = insertPluginsIntoConfiguration(
				pluginConfiguration, configurationTemplate);
		
		//Last, but not least: load the resulting spring context and return the resulting engine instance.
		try {
			ApplicationContext engineContext = loadGeneratedConfiguration(
					configurationString, COMPUTED_CONFIGURATION_RESOURCE_NAME);
			return engineContext.getBean(Engine.class);
		} catch (UnsupportedEncodingException e) {
			throw new EngineInitException("Encoding error during engine configuration.", e);
		}
	}

	/**
	 * Load the optional exclude file if it is specified.
	 * @param pluginConfiguration the configuration to update with the exclude set.
	 */
	private void loadExcludeFile(ComponentConfiguration pluginConfiguration) {
		String globalExcludeElementList=System.getProperty(SQUASH_COMPONENT_EXCLUDEFILE);
		if(globalExcludeElementList!=null){
			try {
				SimpleLinesData excludeFile=new SimpleLinesData(globalExcludeElementList);
				pluginConfiguration.addGlobalExcludedElements(excludeFile.getLines());
			} catch (IOException e) {
				throw new EngineInitException("Exclude file "+globalExcludeElementList+" could not be loaded.", e);
			}
		}
	}

	private String insertPluginsIntoConfiguration(
			ComponentConfiguration pluginConfiguration,
			String configurationTemplate) {
		Set<String> basePackages = pluginConfiguration.getBasePackages();
		Set<String> excludedElements = pluginConfiguration.getExcludedElements();
		
		String configurationString = generateConfigurationFromTemplate(
				configurationTemplate, basePackages, excludedElements);
		
		if(CONF_DEBUG_LOGGER.isDebugEnabled()){
			CONF_DEBUG_LOGGER.debug("Computed engine configuration:\n"+configurationString);
		}
		return configurationString;
	}

	/**
	 * Load the template from resources.
	 * @return the configuration template as a String.
	 */
	private String loadConfigurationTemplate() {
		return loadConfigurationTemplate(TEMPLATE_RESOURCE_NAME);
	}

	@Override
	protected String getTemplateCloseErrorMsg() {
		return TEMPLATE_CLOSE_ERROR_MSG;
	}
}

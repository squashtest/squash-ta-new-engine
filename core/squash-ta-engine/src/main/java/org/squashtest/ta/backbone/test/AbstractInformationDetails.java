/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import org.squashtest.ta.framework.test.result.InformationDetails;
import org.squashtest.ta.framework.test.result.MetadataType;

/**
 * Abstract class for common behavior of InformationDetails components (leaf and composite) 
 * 
 * @author qtran
 */
public abstract class AbstractInformationDetails implements InformationDetails{
    /** Type of metadata*/
    private MetadataType type;
    
    /** Initial metadata text */
    private String metadataAsText;
    
    /** metadata position in the current section*/
    private int sectionPosition = 0;
        
    /** Position of the metadata in the test script*/
    private int absolutePosition = 0;
    
    @Override
    public MetadataType getMetadataType() {
        return type;
    }

    @Override
    public int getMetadataNumberInSection() {
        return sectionPosition;
    }

    @Override
    public int getMetadataNumberInFile() {
        return absolutePosition;
    }

    @Override
    public String getMetadataAsText() {
        return metadataAsText;
    }
    
    @Override
    public Exception caughtException() {
            throw new UnsupportedOperationException();
    }
    
    /* ***************************** Setters part ****************************** */

    public void setMetadataType(MetadataType type) {
        this.type = type;
    }

    public void setMetadataAsText(String metadataAsText) {
        this.metadataAsText = metadataAsText;
    }

    public void setSectionPosition(int number) {
        this.sectionPosition = number;
    }

    public void setAbsolutePosition(int number) {
        this.absolutePosition = number;
    }
    
    
}

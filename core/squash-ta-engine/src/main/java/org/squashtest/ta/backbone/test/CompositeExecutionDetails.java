/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.framework.test.result.ExecutionDetails;

/**
 * Composite part of the Executiondetails
 * 
 * @author bfranchet
 *
 */
public class CompositeExecutionDetails extends AbstractExecutionDetails {

	/** {@link ExecutionDetails} component of this composite*/
	private List<ExecutionDetails> childrens;
	
	/** {@link ExecutionDetails} for instructions which has failed */
	private List<ExecutionDetails> failedChildrens;
	
	/** Number of instructions which failed or finish in error and has for behavior to continue when an error / a failure occurs*/
	private int failedOrErrorWithContinue;
	
	/**
	 * Constructor
	 */
	public CompositeExecutionDetails() {
		childrens = new ArrayList<ExecutionDetails>();
		failedChildrens =  new ArrayList<ExecutionDetails>();
		failedOrErrorWithContinue = 0;
	}

	@Override
	public List<ExecutionDetails> getErrorOrFailedChildrens(){
		return failedChildrens;
	}
		
	@Override
	public int getErrorOrFailedWithContinue(){
		return failedOrErrorWithContinue;
	}
	
	/* *********************************** API part ************************* */
	
	@Override
	public List<ExecutionDetails> getChildrens() {
		return childrens;
	}
	
	/* ********* Setter parts ********* */
	
	/**
	 * Add a new child to the composite
	 * 
	 * @param child The new child
	 */
	public void addChild(ExecutionDetails child, boolean continueOnFailOrError) {
		childrens.add(child);
		if(child.getStatus().isFailOrError()){
			failedChildrens.add(child);
			if(continueOnFailOrError){
				failedOrErrorWithContinue++;
			}
		}
	}
	
}

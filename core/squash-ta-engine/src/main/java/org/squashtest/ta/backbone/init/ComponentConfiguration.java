/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.init;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This objects represents the squash TA component package configuration.
 * 
 * @author edegenetais
 * 
 */
public class ComponentConfiguration {
	/**
	 * Mapping of plugin names to their included component base packages.
	 */
	private Map<String, Set<String>> basePackages = new HashMap<String, Set<String>>();

	/**
	 * Mapping of plugin names to their excluded packages.
	 */
	private Map<String, Set<String>> pluginExcludedElements = new HashMap<String, Set<String>>();
	
	/**
	 * General exclude list to solve conflicts.
	 */
	private Set<String> globalExcludeSet=new HashSet<String>();

	/**
	 * @return The set of base packages (<i>� la</i> spring component-scan) to
	 *         search for engine components. <strong>Modifications on this
	 *         {@link Set} DO NOT CHANGE component configuration data.</strong>
	 */
	public Set<String> getBasePackages() {
		return collectAllValues(basePackages);
	}

	/**
	 * @return The list of elements to exclude from component search (<i>�
	 *         la</i> spring component-scan regex exclude filter).
	 *         <strong>Modifications on this {@link Set} DO NOT CHANGE component
	 *         configuration data.</strong>
	 */
	public Set<String> getExcludedElements() {
		Set<String> excludeSet=collectAllValues(pluginExcludedElements);
		excludeSet.addAll(globalExcludeSet);
		return excludeSet;
	}

	/**
	 * @return The list of squashTA plugin-names. <strong>Modifications on this
	 *         {@link Set} DO NOT CHANGE component configuration data.</strong>
	 */
	public Set<String> getPluginNames() {
		HashSet<String> hashSet = new HashSet<String>();
		hashSet.addAll(basePackages.keySet());
		hashSet.addAll(basePackages.keySet());
		return hashSet;
	}

	/**
	 * Looks for the base package list of a given plugin.
	 * 
	 * @param name
	 *            the name of the desired plugin.
	 * @return the base package set, or <code>null</code> if no such plugin is
	 *         registered.
	 */
	public Set<String> getPluginBasePackages(String name) {
		return nullSafeExtractValueSet(name, basePackages);
	}

	/**
	 * Looks for the base package list of a given plugin.
	 * 
	 * @param name
	 *            the name of the desired plugin.
	 * @return the base package set, or <code>null</code> if no such plugin is
	 *         registered.
	 */
	public Set<String> getPluginExcludedPackages(String name) {
		return nullSafeExtractValueSet(name, pluginExcludedElements);
	}

	/**
	 * Add a base package to the set of a plugin.
	 * 
	 * @param pluginName
	 *            name of the target plugin.
	 * @param basePackage
	 *            name of the base package to add.
	 */
	public void addBasePackageToPlugin(String pluginName, String basePackage) {
		createPluginSetIfNeedBe(pluginName, basePackages);
		basePackages.get(pluginName).add(basePackage);
	}

	/**
	 * Add an excluded element specification to the configuration of a plugin.
	 * 
	 * @param pluginName
	 *            name of the target plugin.
	 * @param excludedElement
	 *            regex to specify the excluded element. See
	 *            <code>&lt;scan-package&gt;</code> element exclude filter
	 *            documentation for more information on exclude regex.
	 */
	public void addExcludedElementToPlugin(String pluginName,
			String excludedElement) {
		createPluginSetIfNeedBe(pluginName, pluginExcludedElements);
		pluginExcludedElements.get(pluginName).add(excludedElement);
	}

	/**
	 * Add a single excluded element.
	 * @param excludedElementDefinition a regex (<i>�
	 *         la</i> spring component-scan regex exclude filter) to define the excluded element(s)
	 */
	public void addGlobalExcludedElement(String excludedElementDefinition){
		globalExcludeSet.add(excludedElementDefinition);
	}
	
	/**
	 * Add a collection of excluded elements.
	 * @param excludedElementDefinitionSet a regex (<i>à
	 *         la</i> spring component-scan regex exclude filter) to define the excluded element(s)
	 */
	public void addGlobalExcludedElements(Collection<String> excludedElementDefinitionSet){
		globalExcludeSet.addAll(excludedElementDefinitionSet);
	}
	
	private void createPluginSetIfNeedBe(String pluginName,
			Map<String, Set<String>> targetBinder) {
		synchronized (targetBinder) {
			if (!targetBinder.containsKey(pluginName)) {
				targetBinder.put(pluginName, new HashSet<String>());
			}
		}
	}

	private Set<String> collectAllValues(Map<String, Set<String>> targetBinder) {
		synchronized (targetBinder) {
			Set<String> basePackageSet = new HashSet<String>();
			for (Set<String> pluginPackageSet : targetBinder.values()) {
				basePackageSet.addAll(pluginPackageSet);
			}
			return basePackageSet;
		}
	}

	private Set<String> nullSafeExtractValueSet(String name,
			Map<String, Set<String>> targetBinder) {
		synchronized (targetBinder) {
			if (targetBinder.containsKey(name)) {
				return new HashSet<String>(basePackages.get(name));
			} else {
				return null;
			}
		}
	}
}

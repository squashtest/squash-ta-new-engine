/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EcosystemPhase;
import org.squashtest.ta.backbone.engine.InstructionFlowStrategy;
import org.squashtest.ta.backbone.engine.TestRunner;
import org.squashtest.ta.backbone.engine.event.TestCompletionEvent;
import org.squashtest.ta.backbone.engine.event.TestStatusUpdateBase;
import org.squashtest.ta.backbone.engine.wrapper.GenericResourceGenerator;
import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceProvider;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.test.AbstractPhaseResult;
import org.squashtest.ta.backbone.test.AbstractTestResult;
import org.squashtest.ta.backbone.test.CompositeExecutionDetails;
import org.squashtest.ta.backbone.test.DefaultInformationDetails;
import org.squashtest.ta.backbone.test.DefaultInformativeSectionResult;
import org.squashtest.ta.backbone.test.PhaseResultFactory;
import org.squashtest.ta.backbone.test.TestResultFactory;
import org.squashtest.ta.backbone.tools.ElementUtils;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.instructions.MetaInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.metadata.TestMetadata;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InformationDetails;
import org.squashtest.ta.framework.test.result.InformativeSection;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.framework.test.result.InformativeSectionResult;
import org.squashtest.ta.backbone.test.AbstractInformativeSectionResult;

public class TestRunnerImpl implements TestRunner, InternalTestRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestRunnerImpl.class);

    private static final ElementUtils ELEMENT_UTILS = new ElementUtils();

    private ContextManager manager;
    private Test test;

    private InstructionRunnerFactory instructionRunnerFactory;

    private EcosystemPhase ecosystemPhase;

    private ResourceProvider builtinContext;

    private LocalContext localContext;

    private Map<ResourceName, ResourceWrapper> ecosystemResources;
    private Map<ResourceName, ResourceWrapper> testResources = new HashMap<>();

    private AbstractTestResult result;

    public TestRunnerImpl(ResourceProvider builtinScopeGenerator) {
        this.builtinContext = builtinScopeGenerator;
    }

    public TestRunnerImpl() {
    }

    @Override
    public void setContextManager(ContextManager manager) {
        this.manager = manager;
    }

    @Override
    public void setTest(Test test) {
        this.test = test;
    }

    @Override
    public Test getTest() {
        return test;
    }

    public TestResult listTest(boolean disableMetadata) {
        Date startTime = new Date();
        result = TestResultFactory.getTestResult();
        result.setName(test.getName());
        result.setTestId(test.getTestId());
        if (!disableMetadata) {
            runSection(test.getMetadata(), InformativeSection.METADATA);
        }

        result.setStartTime(startTime);
        result.setEndTime(new Date());
        LOGGER.debug("Test listing complete.");
        cleanUp();
        LOGGER.debug("Test context cleanup complete.");
        return result;
    }

    @Override
    public TestResult runTest() {
        try {
            TempFileUtils.setTestName(test.getName());
            ecosystemResources = manager.getEcosystemResources();
            LOGGER.debug("Ecosystem resources engaged.");
            initContextParameters();
            Date startTime = new Date();
            result = TestResultFactory.getTestResult();
            result.setName(test.getName());
            result.setTestId(test.getTestId());
            LOGGER.info("Beginning execution of test " + test.getName());
            manager.postEvent(new TestLaunchEvent(test.getName(), test.getTestId()));
            manager.postEvent(new TestInitSynchronousEvent(test.getName(), test.getTestId()));
            
            runSection(test.getMetadata(), InformativeSection.METADATA);

            // Run instructions of the setup phase of the test
            runPhase(test.getSetup(), Phase.SETUP, new DefaultSetupExecutionStrategy());
            // Run instructions of the test phase of the test
            runPhase(test.getTests(), Phase.TEST, new DefaultTestExecutionStrategy());
            // Run instructions of the teardown phase of the test
            runPhase(test.getTeardown(), Phase.TEARDOWN, new DefaultTeardownExecutionStrategy());
            result.setStartTime(startTime);
            result.setEndTime(new Date());
            LOGGER.debug("Test execution complete.");
            cleanUp();
            LOGGER.debug("Test context cleanup complete.");
            manager.postEvent(new TestCompletionEvent(result));
            return result;
        } finally {
            ecosystemResources = null;
            LOGGER.debug("Ecosystem resources released.");
        }
    }

    private void initContextParameters() {
        /*
		 * This method inject in the resource ( according the current scope) the test suite global parameters and the
		 * script parameters in order they become available resources for the instructions.
         */
        ContextParameter contextParameters = ContextParameterFactory.getInstance(ecosystemPhase.getEcosysPhase());
        contextParameters.addContextParameterToResources(this, test.getScriptParams());
    }

    private void runSection(Iterator<TestMetadata> iterator, InformativeSection informativeSection) {
        InformativeSectionResult informativeSectionResult = runMetadataList(informativeSection, iterator);
        result.setInformativeSectionResult(informativeSection, informativeSectionResult);
    }

    private InformativeSectionResult runMetadataList(InformativeSection informativeSection, Iterator<TestMetadata> iterator) {
        InformationSectionMetadata infoSectionMetadata = new InformationSectionMetadata(informativeSection);
        while (iterator.hasNext()) {
            TestMetadata currentMetadata = iterator.next();
            InformationDetails infoDetail = readInformation(currentMetadata);
            infoSectionMetadata.addMetadataToSectionResult(infoDetail);
        }
        return infoSectionMetadata.getSectionResult();
    }

    private InformationDetails readInformation(TestMetadata currentMetadata) {
        DefaultInformationDetails informationDetails = new DefaultInformationDetails();
        informationDetails.setMetadataType(currentMetadata.getType());
        informationDetails.setMetadataAsText(currentMetadata.toString());
        String pos = "";
        String lineNumberInStr = currentMetadata.getLine();
        if (lineNumberInStr.contains(".")) {
            String[] splitStr = lineNumberInStr.split("\\.");
            pos = splitStr[splitStr.length - 1];
            informationDetails.setAbsolutePosition(Integer.parseInt(pos));
        } else {
            informationDetails.setAbsolutePosition(Integer.parseInt(lineNumberInStr));
        }
        return informationDetails;
    }

    private void runPhase(Iterator<TestInstruction> iterator, Phase phase,
            InstructionFlowStrategy instructionFlowStrategy) {
        PhaseExecutionData phaseExecData = new PhaseExecutionData(phase, instructionFlowStrategy, ecosystemPhase,
                result);
        PhaseResult phaseResult = runInstructionList(phaseExecData, iterator, null);
        result.setPhaseResult(phase, phaseResult);
        result.setStatus(result.getStatus().mostSevereStatus(phaseResult.getStatus()));
    }

    private PhaseResult runInstructionList(PhaseExecutionData execData, Iterator<TestInstruction> iterator,
            CompositeExecutionDetails parent) {
        while (iterator.hasNext()) {
            TestInstruction currentInstruction = iterator.next();
            if (currentInstruction.getType().isMeta()) {
                runMetaInstruction(execData, currentInstruction, parent);
            } else {
                ExecutionDetails execDetail = runInstruction(execData, currentInstruction);
                if (parent != null && execDetail != null) {
                    parent.addChild(execDetail, currentInstruction.continueOnFailOrError());
                } else if (execDetail != null) {
                    execData.addInstructionToPhaseResult(execDetail, currentInstruction.continueOnFailOrError());
                }
            }
        }
        return execData.getPhaseResult();
    }

    private void runMetaInstruction(PhaseExecutionData execData, TestInstruction currentInstruction,
            CompositeExecutionDetails parent) {
        MetaInstruction meta = (MetaInstruction) currentInstruction;
        CompositeExecutionDetails composite = new CompositeExecutionDetails();
        composite.setInstructionAsText(meta.toText());
        String[] split = meta.getLine().split("\\.");
        composite.setInstructionNumber(Integer.parseInt(split[split.length - 1]));
        // Initial status
        GeneralStatus compoStatus = GeneralStatus.SUCCESS;
        if (!execData.executeNextInstruction) {
            compoStatus = GeneralStatus.NOT_RUN;
        }
        // Run instructions of the meta instruction
        runInstructionList(execData, meta.getInstructions().iterator(), composite);
        // Set instruction type for the meta ExecutionDetails => Macro for a macro, last child instruction type for an
        // inline meta
        if (currentInstruction.getType().equals(InstructionType.INLINE_META)) {
            ExecutionDetails lastDetail = composite.getChildrens().get(composite.getChildrens().size() - 1);
            composite.setInstructionType(lastDetail.instructionType());
        } else {
            composite.setInstructionType(currentInstruction.getType());
        }
        // Compute meta instruction status
        for (ExecutionDetails compoElement : composite.getChildrens()) {
            if (!compoStatus.isNotRun()) {
                compoStatus = execData.instructionFlowStrategy.aggregate(compoStatus, compoElement.getStatus());
            }
        }
        composite.setStatus(compoStatus);
        if (parent != null) {
            parent.addChild(composite,
                    composite.getErrorOrFailedChildrens().size() == composite.getErrorOrFailedWithContinue());
        } else {
            execData.addInstructionToPhaseResult(composite,
                    composite.getErrorOrFailedChildrens().size() == composite.getErrorOrFailedWithContinue());
        }
    }

    private ExecutionDetails runInstruction(PhaseExecutionData execData, TestInstruction instruction) {
        ExecutionDetails executionDetails;
        InstructionRunner runner = instructionRunnerFactory.getRunner(instruction);
        runner.setTestRunner(this);
        boolean add = !instruction.getType().equals(InstructionType.RESET_LOCAL_CONTEXT);
        if (execData.executeNextInstruction()) {
            executionDetails = runner.run();
            GeneralStatus instructionStatus = executionDetails.getStatus();
            execData.updatePhaseStatus(instructionStatus);
            execData.updateExecuteNextInstruction(instructionStatus, instruction);
            if (instructionStatus.isFailOrError()) {
                add = true;
                logError(execData.getPhase(), executionDetails);
            }
        } else {
            executionDetails = runner.getInstructionsDetails();
        }
        if (!add) {
            executionDetails = null;
        }
        return executionDetails;
    }

    private void logError(Phase phase, ExecutionDetails executionDetails) {
        StringBuilder builder = new StringBuilder("The execution ");
        if (executionDetails.getStatus().equals(GeneralStatus.FAIL)) {
            builder.append("failed");
        } else {
            builder.append("raised an error");
        }
        builder.append(" in the ");

        EcosysPhase ecoPhase = ecosystemPhase.getEcosysPhase();
        switch (ecoPhase) {
            case SETUP:
                builder.append("ECOSYSYEM SETUP phase ");
                break;
            case TEARDOWN:
                builder.append("ECOSYSTEM TEARDOWN phase ");
                break;
            default:
                builder.append(phase);
                builder.append(" phase of the TA script '");
                builder.append(test.getName());
                builder.append("' ");
                break;
        }
        builder.append("with the message: '");
        builder.append(executionDetails.caughtException().getMessage());
        builder.append("'.");
        LOGGER.error(builder.toString());
    }

    public void setInstructionRunnerFactory(InstructionRunnerFactory factory) {
        instructionRunnerFactory = factory;
    }

    public void setEcosystemPhase(EcosystemPhase ecosystemPhase) {
        this.ecosystemPhase = ecosystemPhase;
    }

    /* ************************ InternalTestRunner impl ******************** */
    @Override
    public ResourceWrapper getResourceFromCache(ResourceName resourceName) {
        ResourceWrapper resource;
        switch (resourceName.getScope()) {
            case SCOPE_ECOSYSTEM:
                resource = ecosystemResources.get(resourceName);
                break;
            case SCOPE_TEST:
                resource = testResources.get(resourceName);
                break;
            case SCOPE_TEMPORARY:
                resource = localContext.getResource(resourceName);
                break;
            case SCOPE_BUILTIN:
                resource = fetchBuiltinResource(resourceName);
                break;
            default:
                // just for an easier diagnosis in case we forget something in the
                // future...
                throw new UnsupportedOperationException("The " + resourceName.getScope() + " scope is not supported");
        }
        if (AbstractContextParameter.isReservedResourceName(resourceName.getName())) {
            if (resource != null) {
                LOGGER.warn("You use "
                        + resourceName.getName()
                        + " as resource name in tour TA script, which could hide a TA context parameters resources. You should use another resource name.");
            }
            resource = AbstractContextParameter.getContextResourcesFromCache(this, resourceName.getName(),
                    resourceName.getScope());
        }
        return resource;
    }

    private ResourceWrapper fetchBuiltinResource(ResourceName resourceName) {
        ResourceWrapper resource;
        resource = builtinContext.findResources(resourceName);
        if (resourceName.equals(new ResourceName(ResourceName.Scope.SCOPE_BUILTIN, "context_script_params"))) {
            PropertiesResource scriptParams = new PropertiesResource(test.getScriptParams());
            resource = new ResourceWrapper(new Nature(PropertiesResource.class.getAnnotation(TAResource.class).value()), resourceName, scriptParams, new GenericResourceGenerator("test.runner", getClass().getName(), new Properties()));
        }
        return resource;
    }

    @Override
    public void addResourceToCache(ResourceWrapper resource) {
        switch (resource.getName().getScope()) {
            case SCOPE_ECOSYSTEM:
                ELEMENT_UTILS.checkUniqueness(ecosystemResources.keySet(), resource.getName());
                addEcosystemResources(resource);
                break;
            case SCOPE_TEST:
                ELEMENT_UTILS.checkUniqueness(testResources.keySet(), resource.getName());
                testResources.put(resource.getName(), resource);
                break;
            case SCOPE_TEMPORARY:
                if (localContext == null) {
                    throw new BrokenTestException("Using a temporary storage before the first context initialisation.");
                }
                localContext.addResource(resource);
                break;
            default:
                // just for an easier diagnosis in case we forget something in the
                // future...
                throw new UnsupportedOperationException("Adding resources to the " + resource.getName().getScope() + " scope is not supported");
        }
    }

    private void addEcosystemResources(ResourceWrapper resource) {
        try {
            ecosystemResources.put(resource.getName(), resource);
        } catch (UnsupportedOperationException e) {
            throw new BrokenTestException("Detected attempt to write to the ecosystem scope during ecosystem Run Tests Phase.", e);
        }
    }

    @Override
    public ContextManager getContextManager() {
        return manager;
    }

    @Override
    public LocalContext getLocalContext() {
        return localContext;
    }

    @Override
    public void setLocalContext(LocalContext context) {
        localContext = context;
    }

    /* ************************* private stuffs **************************** */
    private void cleanUp() {
        for (ResourceWrapper resource : testResources.values()) {
            resource.cleanUp();
        }
        testResources.clear();
        if (localContext != null) {
            localContext.cleanUp();
        }
    }

    private static class InformationSectionMetadata {

        private InformativeSection section;
        private AbstractInformativeSectionResult sectionResult;

        private InformationSectionMetadata(InformativeSection informativeSection) {
            this.section = informativeSection;
            init();
        }

        private void init() {
            sectionResult = new DefaultInformativeSectionResult();
        }

        public InformativeSection getSection() {
            return section;
        }

        public AbstractInformativeSectionResult getSectionResult() {
            return sectionResult;
        }

        public void setSectionResult(AbstractInformativeSectionResult sectionResult) {
            this.sectionResult = sectionResult;
        }

        public void addMetadataToSectionResult(InformationDetails executionDetails) {
            sectionResult.addMetadata(executionDetails);
        }
    }

    private static class PhaseExecutionData {

        private boolean executeNextInstruction;
        private Phase phase;
        private InstructionFlowStrategy instructionFlowStrategy;
        private AbstractPhaseResult phaseResult;

        PhaseExecutionData(Phase phase, InstructionFlowStrategy strategy, EcosystemPhase ecosysPhase,
                AbstractTestResult testResult) {
            this.phase = phase;
            instructionFlowStrategy = strategy;
            init(ecosysPhase, testResult);
        }

        private void init(EcosystemPhase ecosysPhase, AbstractTestResult testResult) {
            phaseResult = PhaseResultFactory.getPhaseResult(ecosysPhase.getEcosysPhase(), phase);
            executeNextInstruction = instructionFlowStrategy.beginGroupExecution(testResult.getStatus(),
                    testResult.hasOnlyFailOrErrorWithContinue());
            if (!executeNextInstruction) {
                phaseResult.setPhaseStatus(GeneralStatus.NOT_RUN);
            } else {
                phaseResult.setPhaseStatus(GeneralStatus.SUCCESS);
            }
        }

        boolean executeNextInstruction() {
            return executeNextInstruction;
        }

        void updateExecuteNextInstruction(GeneralStatus instructionStatus, TestInstruction currentInstruction) {
            executeNextInstruction = instructionFlowStrategy.continueExecution(instructionStatus,
                    currentInstruction.continueOnFailOrError());
        }

        public Phase getPhase() {
            return phase;
        }

        public void addInstructionToPhaseResult(ExecutionDetails executionDetails, boolean continueOnFailOrError) {
            phaseResult.addInstruction(executionDetails, continueOnFailOrError);
        }

        public void updatePhaseStatus(GeneralStatus instructionStatus) {
            phaseResult.setPhaseStatus(instructionFlowStrategy.aggregate(phaseResult.getStatus(), instructionStatus));

        }

        public AbstractPhaseResult getPhaseResult() {
            return phaseResult;
        }

    }
    
    private static class TestInitSynchronousEvent extends TestStatusUpdateBase {

        private String ecosystemName;
        private String testName;
        private String testId;

        public TestInitSynchronousEvent(String testName, String testId) {
            this.testName = testName;
            this.testId = testId;
        }

        @Override
        public TestResult getPayload() {
            return new RunningTestResult(testId, testName);
        }

        @Override
        public String getEcosystemName() {
            return ecosystemName;
        }

        @Override
        public void setEcosystemName(String ecosystemName) {
            this.ecosystemName = ecosystemName;
        }

        public String getTestName() {
            return testName;
        }

        public String getTestId() {
            return testId;
        }

        @Override
        public boolean isSynchronous() {
            return true;
        }
    }

    private static class TestLaunchEvent extends TestStatusUpdateBase {

        private String ecosystemName;
        private String testName;
        private String testId;

        public TestLaunchEvent(String testName, String testId) {
            this.testName = testName;
            this.testId = testId;
        }

        @Override
        public TestResult getPayload() {
            return new RunningTestResult(testId, testName);
        }

        @Override
        public String getEcosystemName() {
            return ecosystemName;
        }

        @Override
        public void setEcosystemName(String ecosystemName) {
            this.ecosystemName = ecosystemName;
        }

        @Override
        public boolean isSynchronous() {
            return false;
        }

    }

    protected static final class RunningTestResult implements TestResult {

        private Date startTime = new Date();
        private String testId;
        private String testName;

        public RunningTestResult(String testId, String testName) {
            this.testId = testId;
            this.testName = testName;
        }

        @Override
        public Date startTime() {
            return startTime;
        }

        @Override
        public GeneralStatus getStatus() {
            return GeneralStatus.RUNNING;
        }

        @Override
        public String getName() {
            return testName;
        }

        @Override
        public Date endTime() {
            return null;
        }

        @Override
        public void cleanUp() {/* clean what? */
        }

        @Override
        public int getTotalInstructions() {
            return 0;
        }

        @Override
        public int getTotalCommands() {
            return 0;
        }

        @Override
        public int getTotalAssertions() {
            return 0;
        }

        @Override
        public ExecutionDetails getFirstFailure() {
            return null;
        }

        @Override
        public PhaseResult getSetupPhaseResult() {
            return null;
        }

        @Override
        public PhaseResult getTestPhaseResult() {
            return null;
        }

        @Override
        public PhaseResult getTeardownPhaseResult() {
            return null;
        }

        @Override
        public boolean hasOnlyFailOrErrorWithContinue() {
            return false;
        }

        @Override
        public String getTestId() {
            return testId;
        }

    }

}

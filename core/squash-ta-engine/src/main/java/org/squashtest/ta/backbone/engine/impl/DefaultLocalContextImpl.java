/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.HashMap;
import java.util.Map;

import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;
import org.squashtest.ta.framework.test.instructions.ResourceName;

/**
 * Default engine implementation of the {@link LocalContext} resource store.
 * @author edegenetais
 *
 */
public class DefaultLocalContextImpl implements LocalContext{
	/** Map for the resource store. */
	private Map<ResourceName, ResourceWrapper> resourceStore=new HashMap<ResourceName, ResourceWrapper>();
	
	/** identifier */
	private int identifier;
	
	public DefaultLocalContextImpl(int id) {
		this.identifier=id;
	}
	
	@Override
	public int getContextIdentifier() {
		return identifier;
	}

	@Override
	public void addResource(ResourceWrapper wrapper) throws NotUniqueEntryException{
		ResourceName name = wrapper.getName();
		if(resourceStore.containsKey(name)){
			throw new NotUniqueEntryException("Duplicate resource name: "+name);
		}else{
			resourceStore.put(name, wrapper);
		}
	}

	@Override
	public ResourceWrapper getResource(ResourceName name) {
		return resourceStore.get(name);
	}

	@Override
	public void cleanUp() {
		for(ResourceWrapper rsc:resourceStore.values()){
			rsc.cleanUp();
		}
		resourceStore.clear();
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings;
import org.squashtest.ta.backbone.engine.impl.ResourceLoadingSettingsImpl;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.TestInstruction;
import org.squashtest.ta.framework.test.result.ExecutionDetails;

class DefaultLoadResourceRunner extends AbstractDefaultInstructionRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLoadResourceRunner.class);
	private static final String MISSING_NAME_ERROR = "the resource name is missing";
	private static final String GENERIC_ERROR_MESSAGE = "Cannot convert resource : ";

	private LoadResourceInstruction instruction;

	DefaultLoadResourceRunner(LoadResourceInstruction ins) {
		instruction = ins;
	}

	/**
	 * will first attempt to load from cache then fallback to the manager,
	 * unless a specific repository was specified. Will persist the resource in
	 * the local context only once all operations were performed.
	 * 
	 * will not autoconvert the resource unless explicitly specified : the
	 * conversion will occur when required (commands, assertions)
	 */
	@Override
	public void doRun() {

		// step 1 : fetch the resource
		ResourceWrapper resource = getResource();

		// step 2 : alias it
		ResourceName alias = instruction.getNewName();
		if (alias != null) {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Aliasing as "+alias);
			}
			resource.setName(alias);
		}

		// step 3 : done
		testRunner.addResourceToCache(resource);
	}

	@Override
	protected void addInputToFailureReport(ExecutionDetails executionDetails) {
		//NOOP: no input resources here!
	}
	
	private ResourceWrapper getResource() {

		boolean forcedRepository = (instruction.getRepositoryName() != null);
		ResourceName resourceName = instruction.getResourceName();
		ResourceWrapper resource = null;

		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Loading "+resourceName);
		}
		
		if (!forcedRepository) {
			ResourceWrapper temp = testRunner
					.getResourceFromCache(resourceName);
			if (temp != null) {
				resource = temp.copy();
			}
		}

		if (resource == null) {
			LOGGER.debug("Not in cache: try to load it.");
			ContextManager manager = testRunner.getContextManager();
			ResourceLoadingSettings loadSettings = buildLoadingSettings();
			resource = manager.getResource(loadSettings);

			// remember : no need to copy the resource returned from the manager
			// :
			// it's a copy already
			
		}

		return resource;
	}

	/* ******************* /conversion stuff************************ */

	@Override
	protected void checkSettings() {
		if (instruction.getResourceName() == null) {
			logAndThrow(GENERIC_ERROR_MESSAGE + MISSING_NAME_ERROR);
		}
	}

	@Override
	protected TestInstruction getInstruction() {
		return instruction;
	}

	private ResourceLoadingSettings buildLoadingSettings() {
		ResourceLoadingSettings settings = new ResourceLoadingSettingsImpl();
		settings.setResourceName(instruction.getResourceName());
		settings.setRepositoryName(instruction.getRepositoryName());
		return settings;
	}

	@Override
	protected String getInstructionGenericFailureMessage() {
		return GENERIC_ERROR_MESSAGE;
	}

}

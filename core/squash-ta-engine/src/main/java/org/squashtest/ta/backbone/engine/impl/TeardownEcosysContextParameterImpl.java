/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Properties;

import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

public class TeardownEcosysContextParameterImpl extends AbstractContextParameter {

	public TeardownEcosysContextParameterImpl() {
		super();
	}

	@Override
	public void addContextParameterToResources(InternalTestRunner runner, Properties scriptParams) {
		// For the teardown ecosystem phase we add global parameters to the test context only if they haven't been add
		// during the setup ecosystem phase.
		ResourceName globalParamsName = new ResourceName(Scope.SCOPE_ECOSYSTEM, GLOBAL_PARAMS_RESOURCE_NAME);
		if (runner.getResourceFromCache(globalParamsName) == null) {
			addGlobalParams(runner, Scope.SCOPE_ECOSYSTEM);
		}
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.engine.CommandManager;
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidResource;


public class CommandManagerImpl implements CommandManager {
	
	private ObjectFactory factory;
	private CommandMap commandMap = new CommandMap();
	
	//allows faster lookup
	private Map<String, Nature> natureMap = new HashMap<String, Nature>();
        private Map<String, Nature> targetMap = new HashMap<String, Nature>();
	private Map<String, Nature> categoryMap = new HashMap<String, Nature>();
	
	
	public void setObjectFactory(ObjectFactory factory){
		this.factory=factory;
	}
	
	/* ********************************* interface part **************************** */

	public void addCommandDefinition(CommandDefinition definition) {
		
		mapResourceNatureIfUnknown(definition.getFirstNature());
		mapTargetNatureIfUnknown(definition.getSecondNature());
		addIfUnknown(definition.getCategory());
		commandMap.addDefinition(definition);	
	}

	
	public void removeCommandDefinition(CommandDefinition definition) {
		commandMap.removeDefinition(definition);
	}

	public void removeCommandDefinition(Nature resourceNature,
			Nature targetNature, Nature category) {
		commandMap.removeDefinition(resourceNature, targetNature, category);
	}

	public Collection<CommandHandler> getAllCommands(Nature resourceNature,
			Nature targetNature, Nature category) {
		Collection<CommandHandler> result = new ArrayList<CommandHandler>();
		Collection<CommandDefinition> defs = commandMap.getAllDefinitions(resourceNature, targetNature, category);
		for (CommandDefinition def : defs){
			result.add(createHandler(def));
		}
		return result;
	}

	/**
	 * if null is passed as argument for resourceNatureName and/or targetNatureName,
	 * will perform a search with wildcards
	 * 
	 * if they are specified but not found, returns empty list.
	 * 
	 */
	public Collection<CommandHandler> getAllCommandsByName(String resourceNatureName,
			String targetNatureName, String categoryName) {
	
		
		checkIfNatureKnownOrFail(resourceNatureName);
		Nature resNature = natureMap.get(resourceNatureName);


		checkIfTargetNatureKnownOrFail(targetNatureName);
		Nature tarNature = targetMap.get(targetNatureName);
		
		checkIfCategoryKnownOrFail(categoryName);
		Nature category = categoryMap.get(categoryName);

		// if we found nothing and the resource is a FileResource, we try with a VoidResource
        // DON'T do this you fool ! Next step you'lle try to feed a commend expecting a VoidResource
        // with a FileResource. This triggers nasty non human readable reflection errors and at the 
        // end kill penguins. Just man up and live with the fact that you did not find anything.
		return getAllCommands(resNature, tarNature, category);		
	}


	/* *************************** private code. Keep out ! *************** */
	
	
	private void mapResourceNatureIfUnknown(Nature nature){
		if (! natureMap.containsKey(nature.getName())){
			natureMap.put(nature.getName(), nature);
		}
	}
        
        private void mapTargetNatureIfUnknown(Nature nature){
		if (! targetMap.containsKey(nature.getName())){
			targetMap.put(nature.getName(), nature);
		}
	}
	
	private void addIfUnknown(Nature category){
		if (! categoryMap.containsKey(category.getName())){
			categoryMap.put(category.getName(), category);
		}
	}
	
	//null is always valid, as a wildcard
	private void checkIfNatureKnownOrFail(String natureName){ 
		if ((natureName!=null) &&(! natureMap.containsKey(natureName))){
			throw new ResourceNotFoundException("No known command operates on resource type "+natureName+".");
		}
	}
        
        //null is always valid, as a wildcard
	private void checkIfTargetNatureKnownOrFail(String natureName){ 
		if ((natureName!=null) &&(! targetMap.containsKey(natureName))){
			throw new ResourceNotFoundException("target type "+natureName+" is unknown");
		}
	}
	
	private void checkIfCategoryKnownOrFail(String categoryName){
		if ((categoryName!=null) && (! categoryMap.containsKey(categoryName))){
			throw new ResourceNotFoundException("category "+categoryName+" is unknown");
		}
	}
	
	
	private CommandHandler createHandler(CommandDefinition def){
		Command<?,?> instance = factory.newInstance(def.getComponentClass());
		return new CommandHandler(def.getFirstNature(), def.getSecondNature(), def.getCategory(), def.getResultNature(),instance);
	}
	
	// Catch the value of a resource class @TAResource annotation
	//(allow us to be able to change them in the future without breaking everything)
	@SuppressWarnings("unchecked")
	private String getAnnotationValue (Class<? extends Resource<?>> resourceClass){
		Annotation annotation = resourceClass.getAnnotations()[0];
		Class<TAResource> taResourceClass = (Class<TAResource>) annotation.annotationType();
		return taResourceClass.cast(annotation).value();
	}
		

	/* ******************* used to get rid of generics ******************** */
	
	private static class CommandMap extends SimpleManager<CommandDefinition>{}
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InformationDetails;
import org.squashtest.ta.framework.test.result.InformativeSection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation for informative section result
 * 
 * @author qtran
 */
public class DefaultInformativeSectionResult implements AbstractInformativeSectionResult {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultInformativeSectionResult.class);
    
    /** List of all instructions of the phase */
    private List<InformationDetails> allMetadata = new LinkedList<>();
    
    /** The section of the test corresponding to this {@link InformativeSectionResult} */
    private InformativeSection section;
    
    /** section execution start time */
    private Date startTime;

    /** section execution end time */
    private Date endTime;

    /** Name of the section. In general it's the section name */
    private String name;

    private int totalMetadata = 0;
        
    /**
    * Constructor set to the section attribute the section value given in argument 
    * and set for name attribute the name of the section.
    * 
    * @param section
    *            The section of the test corresponding to this {@link DefaultInformativeSectionResult}.
    */
    public DefaultInformativeSectionResult(InformativeSection section) {
            this.section = section;
            this.name = section.toString();
    }
    
    /**
    * Default constructor. 
    * It sets section attribute to METADATA and name attribute to "". 
    * 
    */
    public DefaultInformativeSectionResult() {
            this.section = InformativeSection.METADATA;
            this.name = "";
    }

    @Override
    public void addMetadata(InformationDetails metadataInformation) {
        allMetadata.add(metadataInformation);
        ++totalMetadata;
    }

    @Override
    public List<InformationDetails> getAllMetadata() {
        return allMetadata;
    }

    @Override
    public InformativeSection getInformativeSection() {
        return section;
    }

    @Override
    public int getTotalMetadata() {
        return totalMetadata;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Date startTime() {
        return new Date(startTime.getTime());
    }

    @Override
    public Date endTime() {
        return new Date(endTime.getTime());
    }

    @Override
    public GeneralStatus getStatus() {
        return GeneralStatus.SUCCESS;
    }

    @Override
    public void cleanUp() {
        LOGGER.debug(getClass().getCanonicalName()+" : nothing to be cleaned in METADATA section");
    }
    
    /**
    * Set the section execution start time
    * 
    * @param startTime
    *            The start time
    */
    public void setStartTime(Date startTime) {
            this.startTime = startTime;
    }

    /**
     * Set the section execution end time
     * 
     * @param endTime
     *            The end time
     */
    public void setEndTime(Date endTime) {
            this.endTime = endTime;
    }
}

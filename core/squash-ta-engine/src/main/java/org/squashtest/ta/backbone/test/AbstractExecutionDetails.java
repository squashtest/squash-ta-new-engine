/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 * Abstract class for common behavior of ExecutionDetails components (leaf and composite) 
 * 
 * @author bfranchet
 * 
 */
public abstract class AbstractExecutionDetails implements ExecutionDetails {

	/** Instruction execution status */
	private GeneralStatus status = GeneralStatus.NOT_RUN;
	
	/** Type of instruction*/
	private InstructionType type;
	
	/** Initial instruction text */
	private String instructionAsText;

	/** Instruction position in the current phase*/
	private int phasePosition = 0;
	
	/** Position of the instruction in the test script*/
	private int absolutePosition = 0;

	@Override
	public GeneralStatus getStatus() {
		return status;
	}

	@Override
	public InstructionType instructionType() {
		return type;
	}

	@Override
	public int instructionNumberInPhase() {
		return phasePosition;
	}

	@Override
	public int instructionNumber() {
		return absolutePosition;
	}

	@Override
	public String instructionAsText() {
		return instructionAsText;
	}

	@Override
	public Exception caughtException() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<ResourceAndContext> getResourcesAndContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addResourceAndContext(ResourceAndContext contextResource) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<ExecutionDetails> getChildrens() {
		return Collections.<ExecutionDetails> emptyList();
	}

	@Override
	public List<ExecutionDetails> getErrorOrFailedChildrens() {
		return Collections.<ExecutionDetails> emptyList();
	}

	@Override
	public int getErrorOrFailedWithContinue() {
		return 0;
	}

	/* ***************************** Setters part ****************************** */

	/**
	 * Set the status of the executed instruction
	 * 
	 * @param status
	 *            The new status
	 */
	public void setStatus(GeneralStatus status) {
		this.status = status;
	}

	/**
	 * Set the {@link InstructionType}
	 * 
	 * @param type
	 *            The instruction type
	 */
	public void setInstructionType(InstructionType type) {
		this.type = type;
	}

	/**
	 * Set the instruction as text
	 * 
	 * @param text
	 *            The text corresponding to the instruction
	 */
	public void setInstructionAsText(String text) {
		this.instructionAsText = text;
	}

	/**
	 * Set the number of the instruction in the related phase
	 * 
	 * @param number
	 *            The instruction number
	 */
	public void setInstructionNumberInPhase(int number) {
		this.phasePosition = number;
	}

	/**
	 * Set the absolute instruction number
	 * 
	 * @param number
	 *            The instruction number
	 */
	public void setInstructionNumber(int number) {
		this.absolutePosition = number;
	}

}

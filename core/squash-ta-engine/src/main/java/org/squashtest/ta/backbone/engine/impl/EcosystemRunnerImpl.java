/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.Date;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.EcosystemPhase;
import org.squashtest.ta.backbone.engine.EcosystemRunner;
import org.squashtest.ta.backbone.engine.TestRunner;
import org.squashtest.ta.backbone.engine.event.EcosystemLaunchEvent;
import org.squashtest.ta.backbone.engine.event.TestCompletionEvent;
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultInstructionRunnerFactory;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.test.DefaultEcosystemResult;
import org.squashtest.ta.backbone.test.TestResultFactory;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Environment;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.backbone.engine.wrapper.ResourceProvider;

public class EcosystemRunnerImpl implements EcosystemRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(EcosystemRunnerImpl.class);
	private Ecosystem ecosystem;
	private ContextManager contextManager;
	private DefaultInstructionRunnerFactory factory = new DefaultInstructionRunnerFactory();

	/** This is here to isolate the {@link EcosystemRunnerImpl} from the {@link TestRunnerImpl} for testing purpose. */
	private TestRunnerFactory runnerFactory = new TestRunnerFactory();

	/** This is here to isolate the {@link EcosystemRunnerImpl} from the {@link TestRunnerImpl} for testing purpose. */
	class TestRunnerFactory {
		/**
		 * {@link TestRunner} instantiation code separated for testing purpose.
		 * 
		 * @param process
		 *            the test to run.
		 * @param currentStrategy
		 *            current instruction flow strategy.
		 * @return a {@link TestRunner} configured to run the test.
		 */
		public TestRunner configureTestRunner(ResourceProvider builtinRepository,Test process, EcosystemPhase phase) {
			TestRunnerImpl setupRunner = new TestRunnerImpl(builtinRepository);
			setupRunner.setTest(process);
			setupRunner.setEcosystemPhase(phase);
			ContextManager cm = phase.getContextManagerReference(contextManager);
			setupRunner.setContextManager(cm);
			setupRunner.setInstructionRunnerFactory(factory);
			return setupRunner;
		}
	}

	public void setEcosystem(Ecosystem target) {
		this.ecosystem = target;
	}

	public void setContextManager(ContextManager manager) {
		contextManager = manager;
	}

	@Override
	public EcosystemResult run() {

		TempFileUtils.setEcosystemName(ecosystem.getName());

		DefaultEcosystemResult result = new DefaultEcosystemResult();
		result.setName(ecosystem.getName());

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Beginning execution of ecosystem " + ecosystem.getName());
		}
		contextManager.postEvent(new EcosystemLaunchEvent(ecosystem));

		// should start with a clean ecosystem scope...
		contextManager.getEcosystemResources().clear();
		result.setStartTime(getCurrentTime());
		TestResult setupResult = executeSetup(result);
		GeneralStatus ecosystemStatus = runAllTests(result, setupResult);
		runTearDown(result, ecosystemStatus);
		result.setEndTime(getCurrentTime());

		// should free and clean scope for who comes next...
		cleanUp();
		LOGGER.debug("Ecosystem execution complete.");
		contextManager.postEvent(new EcosystemCompletionEvent(result));
		return result;
	}

	private void cleanUp() {
		for (Entry<ResourceName, ResourceWrapper> environmentEntry : contextManager.getEcosystemResources().entrySet()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Clearing ecosystem resource " + environmentEntry.getKey().toString());
			}
			environmentEntry.getValue().cleanUp();
		}
		contextManager.getEcosystemResources().clear();
	}

	private Date getCurrentTime() {
		return new Date();
	}

	/**
	 * Run the setup process.
	 * 
	 * @param result
	 *            ecosystem execution result object, to update it.
	 * @return the result of the setup process, to control the next execution phases.
	 */
	private TestResult executeSetup(DefaultEcosystemResult result) {
		Environment environment = ecosystem.getEnvironment();
		if (environment.getSetUp() != null) {
			TestRunner setupRunner = runnerFactory.configureTestRunner(contextManager.getSuiteBuiltinScope(),environment.getSetUp(),
					new DefaultSetupEcosystemPhase(ecosystem.getName()));
			TestResult setupResult = setupRunner.runTest();
			result.setSetupResult(setupResult);
			return setupResult;
		} else {
			// By default the factory generate a test result with a status set to SUCCESS
			return TestResultFactory.getTestResult();
		}

	}

	/**
	 * Run the tearDown process.
	 * 
	 * @param result
	 *            ecosystem execution result object, to update it.
	 * @param ecosystemStatus
	 *            ecosystem status from previous execution phases.
	 */
	private void runTearDown(DefaultEcosystemResult result, GeneralStatus ecosystemStatus) {
		Environment environment = ecosystem.getEnvironment();
		if (environment.getTearDown() != null) {
			TestRunner tearDownRunner = runnerFactory.configureTestRunner(contextManager.getSuiteBuiltinScope(),environment.getTearDown(),
					new DefaultTeardownEcosystemPhase(ecosystem.getName()));
			TestResult tearDownResult = tearDownRunner.runTest();
			result.setTearDownResult(tearDownResult);
			result.setStatus(ecosystemStatus.mostSevereStatus(tearDownResult.getStatus()));
		} else {
			result.setStatus(ecosystemStatus);
		}

	}

	/**
	 * Run all tests in the ecosystem population.
	 * 
	 * @param result
	 *            the ecosystem result object, to update it.
	 * @param setupResult
	 *            result of the setup process, to control the test phase execution process.
	 * @return the status resulting from the test execution process for aggregation with next phases.
	 */
	private GeneralStatus runAllTests(DefaultEcosystemResult result, TestResult setupResult) {

		GeneralStatus ecosystemStatus;
		GeneralStatus setupStatus = setupResult.getStatus();
		ecosystemStatus = setupStatus;
		DefaultRunTestEcosystemPhase runTestPhase = new DefaultRunTestEcosystemPhase(ecosystem.getName());
		if (setupStatus.isPassed() || setupResult.hasOnlyFailOrErrorWithContinue()) {
			for (Test test : ecosystem.getTestPopulation()) {
				if(test.isVirtual()){
					TestResult notFoundTestResult = TestResultFactory.getNotFoundTestResult(test.getName(), test.getTestId());
					runTestPhase.getContextManagerReference(contextManager).postEvent(
							new TestCompletionEvent(notFoundTestResult));
					result.addTestResult(notFoundTestResult);
					StringBuilder msg = new StringBuilder("Test \"");
					msg.append(test.getName());
					msg.append("\" was not found");
					LOGGER.error(msg.toString());
					ecosystemStatus = ecosystemStatus.mostSevereStatus(GeneralStatus.ERROR);
				}else{
					TestRunner testRunner = runnerFactory.configureTestRunner(contextManager.getSuiteBuiltinScope(), test, runTestPhase);
					TestResult testResult = testRunner.runTest();
					contextManager.resetAll();
					result.addTestResult(testResult);
					GeneralStatus testStatus = testResult.getStatus();
					ecosystemStatus = ecosystemStatus.mostSevereStatus(testStatus);
				}
			}
		} else {
			for (Test test : ecosystem.getTestPopulation()) {
				TestResult notRunTestResult = TestResultFactory.getNotRunTestResult(test.getName(), test.getTestId());
				runTestPhase.getContextManagerReference(contextManager).postEvent(
						new TestCompletionEvent(notRunTestResult));
				result.addTestResult(notRunTestResult);
			}
		}
		return ecosystemStatus;
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.engine.EventManager;
import org.squashtest.ta.framework.annotations.SynchronousEventHandling;
import org.squashtest.ta.framework.test.event.StatusUpdateEvent;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;

/**
 * Concrete implementation for Squash TA test execution.
 * @author edegenetais
 *
 */
public class EventManagerImpl implements EventManager,Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(EventManagerImpl.class);

        private static final int MAX_SYNCHRONOUS_DURATION = 100;
        
	private List<StatusUpdateListener> listeners=new ArrayList<StatusUpdateListener>();
        private Map<StatusUpdateListener,Boolean> listenerSynchronicity=new WeakHashMap<>();
        
	private BlockingQueue<StatusUpdateEvent<?>> eventQueue=new LinkedBlockingQueue<StatusUpdateEvent<?>>();
	
	private Thread eventPump;
	
	private volatile boolean running=true;
	
	@Override
	public void postEvent(StatusUpdateEvent<?> event) {
		boolean pending = true;
                boolean isSynchronous = event.isSynchronous();
		if(!running){
			LOGGER.warn("Event "
					+ event.toString()
					+ " has been posted after event manager shutdown and will be ignored!");
		}
                
                if(isSynchronous==true){
                    
                    long startTime = System.currentTimeMillis();
                    dispatchEvent(event);
                    long endTime = System.currentTimeMillis();
                    
                    if (endTime-startTime > MAX_SYNCHRONOUS_DURATION) {
                        LOGGER.warn("The event : {} has lasted monre than {} milliseconds. Please make sure, it needs to be synchronous.", event.toString(), MAX_SYNCHRONOUS_DURATION);
                    }
                    
                } else {
                    while(pending && running){
                        pending=!eventQueue.offer(event);
                    }
                }
	}

	@Override
	public void addEventListener(StatusUpdateListener listener) {
            synchronized(listener){
                final boolean isAsynchronousAnnotated = listener.getClass().getAnnotation(SynchronousEventHandling.class) != null;
                
                listenerSynchronicity.put(listener, isAsynchronousAnnotated);
                
		listeners.add(listener);
            }
	}

	@Override
	public boolean removeEventListener(StatusUpdateListener listener) {
            synchronized(listener){
                listenerSynchronicity.remove(listener);
		return listeners.remove(listener);
            }
	}
	
	@Override
	public void run() {
		LOGGER.debug("StatusEvent loop started.");
		while(running){
			try{
				StatusUpdateEvent<?> event=eventQueue.take();
				dispatchEvent(event);
			}catch(InterruptedException ie){
				LOGGER.debug("Interrupted while waiting on event queue",ie);
			}
		}
		LOGGER.debug("StatusEvent loop enters shutdown mode - all pending events will be treated as soon as possible.");
		StatusUpdateEvent<?> nextPendingEvent=eventQueue.poll();
		while(nextPendingEvent!=null){
			dispatchEvent(nextPendingEvent);
			nextPendingEvent=eventQueue.poll();
		}
		LOGGER.debug("StatusEvent loop terminates.");
	}
        
	protected void dispatchEvent(StatusUpdateEvent<?> event) {
		LOGGER.debug("Unstacked event: "+event.toString());
                
                synchronized(listeners){
		
                    for(StatusUpdateListener listener:listeners){
			try{
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Firing event on listener "+listener.toString());
				}
                                if(event.isSynchronous()==listenerSynchronicity.get(listener)){
                                    event.fire(listener);
                                }
			}catch(Exception e){
				LOGGER.error("Event misfired on listener "+listener,e);
			}
		
                    }
                }
	}

	@Override
	public synchronized void start() {
		if(eventPump==null){
			eventPump=new Thread(this);
			eventPump.setName("StatusUpdateEvent loop "+hashCode());
			eventPump.start();
		}
		running=true;
	}

	@Override
	public synchronized void shutdown() {
		this.running=false;
		//force wake the event pump thread to keep if from waiting forever for an event that's never to come...
		if(eventPump!=null){
			eventPump.interrupt();
			//this thread is terminating, let's unregister it to enable event manager reuse if needed (others will have to wait, though
			try {
				eventPump.join();
			} catch (InterruptedException e) {
				LOGGER.debug("Interrupted while waiting for eventPump to terminate");
			}
		}
		eventPump=null;		
		LOGGER.debug("Event manager shutdown requested.");
	}

	@Override
	public synchronized boolean active() {
		return running;
	}
}

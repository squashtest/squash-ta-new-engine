/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Date;

import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InformativeSection;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.framework.test.result.InformativeSectionResult;

/**
 * Abstract interface for the setter element.
 * 
 * @author bfranchet
 * 
 */
public abstract class AbstractTestResult implements TestResult {

	/**
	 * Set the start time of the test
	 * 
	 * @param startTime
	 *            The start time
	 */
	public abstract void setStartTime(Date startTime);

	/**
	 * Set the end time of the test
	 * 
	 * @param endTime
	 *            The end time of the test
	 */
	public abstract void setEndTime(Date endTime);

	/**
	 * Set the name of the test
	 * 
	 * @param testName
	 *            The name of the test
	 */
	public abstract void setName(String testName);

	/**
	 * Set the external test Id
	 * 
	 * @param testId
	 *            The external Id
	 */
	public abstract void setTestId(String testId);

	/**
	 * Set the phase result {@link PhaseResult} to the corresponding {@link Phase}
	 * 
	 * @param phase
	 *            The phase
	 * @param result
	 *            The phase result
	 */
	public abstract void setPhaseResult(Phase phase, PhaseResult result);
        
        /**
	 * Set the phase result {@link InformativeSectionResult} to the corresponding {@link InformativeSection}
	 * 
	 * @param informativeSection
	 *            The informative section (METADATA)
	 * @param result
	 *            The informative section result
	 */
	public abstract void setInformativeSectionResult(InformativeSection informativeSection, InformativeSectionResult result);

	/**
	 * Set the test status
	 * 
	 * @param status
	 *            The test status
	 */
	public abstract void setStatus(GeneralStatus status);
}

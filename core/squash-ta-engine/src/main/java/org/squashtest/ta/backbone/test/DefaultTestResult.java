/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test;

import java.util.Date;

import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InformativeSection;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.InformativeSectionResult;

/**
 * Default implementation of the TestResult interface. Represent the result of a TA test case execution.
 */
public class DefaultTestResult extends AbstractTestResult {
	
	/** Name of the test */
	private String testName;
	
	/** Test external Id */
	private String testId;

	/** Test starting time */
	private Date startTime;

	/** Test ending time */
	private Date endTime;

	/** Status of the test case */
	private GeneralStatus testStatus = GeneralStatus.SUCCESS;
        
        /** Result of the metadata section */
	private InformativeSectionResult metadataSection = new DefaultInformativeSectionResult();  

	/** Result of the setup phase */
	private PhaseResult setupPhase = PhaseResultFactory.getNotRunPhaseResult(Phase.SETUP);

	/** Result of the teardown phase */
	private PhaseResult teardownPhase = PhaseResultFactory.getNotRunPhaseResult(Phase.TEARDOWN);

	/** Result of the test phase */
	private PhaseResult testPhase = PhaseResultFactory.getNotRunPhaseResult(Phase.TEST);
	
	/* ************** API part **************** */
	@Override
	public String getName() {
		return testName;
	}
	
	@Override
	public String getTestId() {
		return testId;
	}

	@Override
	public Date startTime() {
		return startTime;
	}

	@Override
	public Date endTime() {
		return endTime;
	}
          
	@Override
	public int getTotalCommands() {
		return setupPhase.getTotalCommands() + testPhase.getTotalCommands() + teardownPhase.getTotalCommands();
	}

	@Override
	public int getTotalAssertions() {
		return setupPhase.getTotalAssertions() + testPhase.getTotalAssertions() + teardownPhase.getTotalAssertions();
	}

	@Override
	public int getTotalInstructions() {
		return setupPhase.getTotalInstructions() + testPhase.getTotalInstructions()
				+ teardownPhase.getTotalInstructions();
	}

	@Override
	public GeneralStatus getStatus() {
		return testStatus;
	}

	@Override
	public ExecutionDetails getFirstFailure() {
		ExecutionDetails exec = null;
		if (!setupPhase.getFailedInstructions().isEmpty()) {
			exec = setupPhase.getFailedInstructions().get(0);
		} else if (!testPhase.getFailedInstructions().isEmpty()) {
			exec = testPhase.getFailedInstructions().get(0);
		} else if (!teardownPhase.getFailedInstructions().isEmpty()) {
			exec = teardownPhase.getFailedInstructions().get(0);
		}
		return exec;
	}

	@Override
	public boolean hasOnlyFailOrErrorWithContinue() {
		int totalFailedOrErrorAssertions = setupPhase.getFailedInstructions().size()+testPhase.getFailedInstructions().size()+teardownPhase.getFailedInstructions().size();
		int totalFailedOrErrorVerifyAssertions = setupPhase.getFailedOrErrorWithContinue()+testPhase.getFailedOrErrorWithContinue()+teardownPhase.getFailedOrErrorWithContinue();
		return totalFailedOrErrorAssertions == totalFailedOrErrorVerifyAssertions;
	}

	@Override
	public void cleanUp() {
                metadataSection.cleanUp();
		setupPhase.cleanUp();
		testPhase.cleanUp();
		teardownPhase.cleanUp();
	}

	/* ****************** setters and utility *************** */
	@Override
	public void setName(String name) {
		this.testName = name;
	}
	
	@Override
	public void setTestId(String testId) {
		this.testId = testId;
	}

	@Override
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Override
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

        @Override
        public void setInformativeSectionResult(InformativeSection informativeSection, InformativeSectionResult sectionResult) {
            //for the moment we only have 1 informative section that is METADATA section
            if (informativeSection.equals(InformativeSection.METADATA)){
                metadataSection = sectionResult;
            } else {
                throw new LinkageError("Unsupported test section: " + informativeSection.toString());
            }
        }
    
	@Override
	public void setPhaseResult(Phase phase, PhaseResult phaseResult) {

		switch (phase) {
		case SETUP:
			setupPhase = phaseResult;
			break;

		case TEST:
			testPhase = phaseResult;
			break;

		case TEARDOWN:
			teardownPhase = phaseResult;
			break;

		default:
			throw new LinkageError("Unsupported test Phase: " + phase.toString());

		}
	}

	@Override
	public void setStatus(GeneralStatus status) {
		testStatus = status;
	}
            
	@Override
	public PhaseResult getSetupPhaseResult() {
		return setupPhase;
	}

	@Override
	public PhaseResult getTestPhaseResult() {
		return testPhase;
	}

	@Override
	public PhaseResult getTeardownPhaseResult() {
		return teardownPhase;
	}

}

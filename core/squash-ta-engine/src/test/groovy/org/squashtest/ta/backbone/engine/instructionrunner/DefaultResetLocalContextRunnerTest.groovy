/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.TestRunner;
import org.squashtest.ta.backbone.engine.impl.InstructionRunner;
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner;
import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.backbone.exception.MalformedInstructionException;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction;
import org.squashtest.ta.framework.test.result.GeneralStatus;

import spock.lang.Specification;

/**
 * Test for the {@link DefaultResetLocalContextRunner} instruction runner.
 * @author edegenetais
 *
 */
class DefaultResetLocalContextRunnerTest extends Specification {
	def testee
	def testRunner
	def instruction
	
	def setup(){
		instruction = new ResetLocalContextInstruction()
		testee=new DefaultResetLocalContextRunner(instruction)
		testRunner = Mock(InternalTestRunner)
		testee.setTestRunner(testRunner)
	}

	def "should push a different local context"(){
		given:
			def initialContext = Mock(LocalContext)
			initialContext.getContextIdentifier() >> 1 
			testRunner.getLocalContext() >> initialContext
		and:			
			instruction.setContextIdentifier(2)
		when:
			testee.run()
		then:
			1 * testRunner.setLocalContext({it.getContextIdentifier() != 1})
			1 * initialContext.cleanUp()
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.pseudoimplem;

import java.util.Properties;

import org.squashtest.ta.framework.annotations.ResourceType;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;

@ResourceType("pseudoResourceRepository")
public class PseudoResourceRepository implements ResourceRepository {

	@Override
	public Properties getConfiguration() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("TODO");
	}

	@Override
	public FileResource findResources(String name) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("TODO");
	}
	
	@Override
	public void cleanup(){
		//nothing to do
	}
	
	@Override
	public void init(){
		//nothing to do
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.ResourceLoader
import org.squashtest.ta.backbone.engine.wrapper.Nature
import org.squashtest.ta.backbone.exception.AmbiguousConversionException;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException
import org.squashtest.ta.backbone.engine.ResourceConverterManager
import org.squashtest.ta.backbone.engine.CommandManager
import org.squashtest.ta.backbone.engine.AssertionManager
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper
import org.squashtest.ta.backbone.definition.CommandDefinition
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.engine.ResourceConversionSettings
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler
import org.squashtest.ta.backbone.engine.wrapper.BinaryAssertionHandler
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import spock.lang.Specification

class ContextManagerImplTest extends Specification {

	ContextManager manager = new ContextManagerImpl();
	
	def resourceLoader = Mock(ResourceLoader)
	
	def converterManager = Mock(ResourceConverterManager)
	
	def commandManager = Mock(CommandManager)
	
	def assertionManager = Mock(AssertionManager)
	
	def nat1 = new Nature("nat1")
	def nat2 = new Nature("nat2")
	def nat3 = new Nature("nat3")
	
	def setup(){
		
		manager.setResourceConverterManager converterManager;
		manager.setResourceLoader resourceLoader;
		manager.setCommandManager commandManager;
		manager.setAssertionManager assertionManager;
	}
	
	
	def "should register a repository"(){
		given :
			def newRepo = Mock(RepositoryWrapper)
		when :
			manager.registerRepository newRepo 
		then :
			1 * resourceLoader.addRepository(newRepo)
	}
	
	def "should register a target"(){
		
		given:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)	
		and :
			def newTarget = Mock(TargetWrapper)
			newTarget.getName() >> "target"
		when :
			manager.registerTarget newTarget
		then :
			manager.targets.get("target") == newTarget		
	}
	
	def "should init and register a target that is needed"(){
		
		given :
			def newTarget = Mock(TargetWrapper)
			newTarget.getName() >> "target"
			newTarget.getNature() >> Mock(Nature)
			newTarget.getNature().getName() >> "nature"
		and:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)
			manager.registerTarget newTarget
		when :
			manager.initAll()
		then :
			1 * newTarget.init()
			manager.targetInitialisationResults.size() == 1
	}
	
	def "should not init but register an unknown target"(){
		
		given :
			def newTarget = Mock(TargetWrapper)
			newTarget.getName() >> "target"
			newTarget.getNature() >> Mock(Nature)
			newTarget.getNature().getName() >> "nature"
		and:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)
		when :
			manager.initAll()
		then :
			0 * newTarget.init()
			manager.targetInitialisationResults.size() == 1
	}
	
	def "should init but not register a void target that is needed"(){
		
		given :
			def newTarget = Mock(TargetWrapper)
			newTarget.getName() >> VoidTarget.INSTANCE_NAME
			newTarget.getNature() >> Mock(Nature)
			newTarget.getNature().getName() >> "nature"
		and:
			def set = new HashSet<String>()
			set.add(VoidTarget.INSTANCE_NAME)
			manager.setTargetsNames(set)
			manager.registerTarget newTarget
		when :
			manager.initAll()
		then :
			1 * newTarget.init()
			manager.targetInitialisationResults.size() == 0
	}
	
	def "should not init and not register a target that is not needed"(){
		
		given:
			manager.setTargetsNames(new HashSet<String>())	
		and :
			def newTarget = Mock(TargetWrapper)
			newTarget.getName() >> "target"
			manager.registerTarget newTarget
		when :
			manager.initAll()	
		then :
			0 * newTarget.init()	
			manager.targetInitialisationResults.size() == 0
	}
	
	def "should rant because the said target already exists"(){
			
		given:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)	
		and :
			def newTarget = Mock(TargetWrapper)
			newTarget.getName() >> "target"
		when :
			manager.registerTarget newTarget
			manager.registerTarget newTarget
		
		then :
			thrown NotUniqueEntryException
		
	}
	
	def "should register a command definition"(){
		
		given :
			def newDefinition = Mock(CommandDefinition)
		when :
			manager.registerCommandDefinition(newDefinition)
			
		then :
			1 * commandManager.addCommandDefinition(newDefinition)
		
	}

	def "should register a binary assertion definition"(){
		
		given :
			def newDefinition = Mock(BinaryAssertionDefinition)
		when :
			manager.registerAssertionDefinition(newDefinition)
			
		then :
			1 * assertionManager.addAssertionDefinition(newDefinition)
		
	}
	
	def "should register an unary assertion definition"(){
		
		given :
			def newDefinition = Mock(UnaryAssertionDefinition)
		when :
			manager.registerAssertionDefinition(newDefinition)
			
		then :
			1 * assertionManager.addAssertionDefinition(newDefinition)
		
	}
		
	def "should unregister a repository"(){
		given :
			def repo = Mock(RepositoryWrapper)
			repo.getName() >> "repo"
		when :
			manager.unregisterRepository repo
			
		then :
			1 * resourceLoader.removeRepository("repo")
	}
	
	
	def "should unregister a repository by name"(){
		given:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)	
		and :
			def repo = Mock(RepositoryWrapper)
		when :
			manager.unregisterRepository "test"
		then :
			1 * resourceLoader.removeRepository("test")
	}
		
	def "should unregister a target"(){
		given:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)	
		and :
			def target = Mock(TargetWrapper)
			target.getName() >> "target"		
		when :
			manager.registerTarget target
			def before = (manager.targets.get("target") == target)
			
			manager.unregisterTarget target
			def after = (manager.targets.get("target") == null)
		
		then :
			before == true
			after == true		
	}
	
	def "should unregister a target by name"(){
		given:
			def set = new HashSet<String>()
			set.add("target")
			manager.setTargetsNames(set)	
		and :
			def target = Mock(TargetWrapper)
			target.getName() >> "target"
			
		when :
			manager.registerTarget target
			def before = (manager.targets.get("target") == target)
			
			manager.unregisterTarget target
			def after = (manager.targets.get("target") == null)
		
		then :
			before == true
			after == true
			
	}
	
	def "should unregister a command definition"(){
		given :
			def commandDefinition = Mock(CommandDefinition)
			
		when :
			manager.unregisterCommandDefinition(commandDefinition)
			
		then :
			1 * commandManager.removeCommandDefinition(commandDefinition)
		
	}

	def "should unregister a command definition by content"(){
			
		when :
			manager.unregisterCommandDefinition(nat1,
											 nat2,
											 nat3)
			
		then :
			1 * commandManager.removeCommandDefinition( nat1,
														nat2, 
														nat3)
	}
	
	def "should unregister a binary assertion definition"(){
		
		given :
			def assertionDefinition = Mock(BinaryAssertionDefinition)
			
		when :
			manager.unregisterAssertionDefinition(assertionDefinition)
			
		then :
			1 * assertionManager.removeAssertionDefinition(assertionDefinition)
		
	}

	def "should unregister a binary assertion definition by content"(){
			
		when :
			manager.unregisterAssertionDefinition(nat1,
											 	nat2,
											 	nat3)
			
		then :
			1 * assertionManager.removeAssertionDefinition(nat1,
													nat2,
													nat3)
	}
	
	
	
	def "should unregister an unary assertion definition"(){
		
		given :
			def assertionDefinition = Mock(UnaryAssertionDefinition)
			
		when :
			manager.unregisterAssertionDefinition(assertionDefinition)
			
		then :
			1 * assertionManager.removeAssertionDefinition(assertionDefinition)
		
	}

	def "should unregister an unary assertion definition by content"(){
			
		when :
			manager.unregisterAssertionDefinition(nat1,
												nat3)
			
		then :
			1 * assertionManager.removeAssertionDefinition(nat1,
													nat3)
	}
	
	def "should get a resource by name from the loader"(){
		
		given :
			def resource = Mock(ResourceWrapper)
			resource.getName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
		
		and :
			def name = new ResourceName (Scope.SCOPE_TEST,"resource")
		
		when :
			def res = manager.getResource(name)
		
		then :
			res == resource
			1 * resourceLoader.loadResource({it.resourceName==new ResourceName (Scope.SCOPE_TEST,"resource")}) >> resource
		
	}
	
	def "should get a resource by name and repository from loader"(){
		
		given :
			def resource = Mock(ResourceWrapper)
			resource.getName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
		
		and :
			def name = new ResourceName (Scope.SCOPE_TEST,"resource")
		
		when :
			def res = manager.getResource(name, "repo")
		
		then :
			res == resource
			1 * resourceLoader.loadResource(
				{it.resourceName==new ResourceName (Scope.SCOPE_TEST,"resource") && it.repositoryName=="repo"}
			) >> resource

	}
	
	def "should get a Resource by settings from loader"(){
		given :
			def resource = Mock(ResourceWrapper)
			resource.getName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
			
		and :
			def settings = Mock(ResourceLoadingSettings)
			settings.getResourceName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
			
		when :
			def res = manager.getResource(settings)
			
		then :	
			res == resource
			1 * resourceLoader.loadResource(settings) >> resource			
	}
	
	def "should rant because no name was supplied in the settings"(){
		
		given :
			def settings = Mock(ResourceLoadingSettings)
		
		when :
			manager.getResource(settings)
			
		then :
			thrown ResourceNotFoundException
	}
	
	def "should convert resource to the specified type"(){
		given :
			def resource = Mock(ResourceWrapper)
			def resNature = Mock(Nature)
		
			def output = Mock(ResourceWrapper)
			
		and :
			resource.getNature() >> resNature
			resNature.getName() >> "mock"
			
		
		and :
			def settings = Mock(ResourceConversionSettings)
			settings.getDesiredNature() >> "remock"
			def conf = [] as List
			settings.getConfiguration() >> conf 
			
		and : 
			def converter = Mock(ResourceConverterHandler)
			converter.convert(resource) >> output
			
		and :
			converterManager.getAllConvertersByName("mock", "remock", null) >> [converter]
		
		when :
			def res = manager.convertResource(resource, settings)
		
		then :
			res == output
	}
	
	
	def "should rant because no converter was found"(){
		
		given :
			def settings = Mock(ResourceConversionSettings)
			def conf = [] as List
			settings.getConfiguration() >> conf
			
		and :
			def resource = Mock(ResourceWrapper)
			def inNature = Mock(Nature)
			resource.getNature() >> inNature
			inNature.getName() >> null
			converterManager.getAllConvertersByName(null, null, null)>> []
		
		when :
			manager.convertResource(resource, settings)
		
		then :
			thrown ImpossibleConversionException
	}
	
	def "should rant because multiple converters were found"(){
		given :
			def settings = Mock(ResourceConversionSettings)
			def conf = [] as List
			settings.getConfiguration() >> conf
			
		and :
			def resource = Mock(ResourceWrapper)
			def inNature = Mock(Nature)
			resource.getNature() >> inNature
			inNature.getName() >> null
			
		and :
			def con1 = Mock(ResourceConverterHandler)
			con1.getConverterCategory() >> nat3
			
			def con2 = Mock(ResourceConverterHandler)
			con2.getConverterCategory() >> nat2
			
			converterManager.getAllConvertersByName(null, null, null) >> [con1, con2]
			
		when :
			manager.convertResource(resource, settings)
			
		then :
			thrown AmbiguousConversionException	
	}
	
	def "should load a bunch of commands"(){
		
		given :
			def settings = new CommandFindSettingsImpl();
			settings.setResourceNatureName "resource.nature"
			settings.setTargetNatureName "target.nature"
			settings.setCommandIdentifier "command"
			
		and :
			def command = Mock(CommandHandler)
		and :
			commandManager.getAllCommandsByName("resource.nature", "target.nature",
				"command") >> [command]
		
		when :
			def res = manager.getCommand(settings)
		
		then :
			res == [command]
			command.addConfiguration([])
		
	}
	
	
	def "should load a bunch of binary assertions"(){
		
		given :
			def settings = new AssertionFindSettingsImpl();
			settings.setActualResultNatureName "actual.nature"
			settings.setExpectedResultNatureName "expected.nature"
			settings.setAssertionCategory "assert"
			
		and :
			def assertion = Mock(BinaryAssertionHandler)
		and :
			assertionManager.getAllBinaryAssertionsByName("actual.nature", "expected.nature",
				"assert") >> [assertion]
		
		when :
			def res = manager.getBinaryAssertion(settings)
		
		then :
			res == [assertion]
			assertion.addConfiguration([])
		
	}
	
	def "should load a bunch of unary assertions"(){
		
		given :
			def settings = new AssertionFindSettingsImpl();
			settings.setActualResultNatureName "actual.nature"
			settings.setAssertionCategory "assert"
			
		and :
			def assertion = Mock(UnaryAssertionHandler)
		and :
			assertionManager.getAllUnaryAssertionsByName("actual.nature", "assert") >> [assertion]
		
		when :
			def res = manager.getUnaryAssertion(settings)
		
		then :
			res == [assertion]
			assertion.addConfiguration([])
	}
	
}

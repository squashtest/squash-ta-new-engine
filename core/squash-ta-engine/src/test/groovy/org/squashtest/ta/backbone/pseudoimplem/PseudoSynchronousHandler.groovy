/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.pseudoimplem

import org.squashtest.ta.framework.annotations.SynchronousEventHandling
import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate
import org.squashtest.ta.framework.test.event.StatusUpdateListener
import org.squashtest.ta.framework.test.event.TestStatusUpdate
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate

/**
 * Created to test that synchronous events are properly dispatched to synchronous handlers.
 * @author ericdegenetais
 */
@SynchronousEventHandling
class PseudoSynchronousHandler implements StatusUpdateListener {
	private int nbTestEvents;
        
        public void handle(TestStatusUpdate event){
            nbTestEvents++;
        }
        
        public void handle(TestSuiteStatusUpdate event){}
        public void handle(EcosystemStatusUpdate event){}
        
        public int nbTestEvents(){
            return nbTestEvents;
        }
}


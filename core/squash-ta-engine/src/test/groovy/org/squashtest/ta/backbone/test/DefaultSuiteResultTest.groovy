/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.test

import java.util.Collection

import org.squashtest.ta.backbone.tools.AggregativeChristmasTree;
import org.squashtest.ta.backbone.tools.MultiDimensionMap;
import org.squashtest.ta.framework.components.ResourceRepository
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult.TargetStatus;
import org.squashtest.ta.backbone.engine.wrapper.Nature
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;

import spock.lang.Specification

class DefaultSuiteResultTest extends Specification {

	DefaultSuiteResult testee = new DefaultSuiteResult("testSuiteName")
	
	def "should return target results alphabetically ordered"(){
		given :
			TargetInitialisationResult t1 = new TargetInitialisationResult("ccc", "mock", TargetStatus.OK)
			TargetInitialisationResult t2 = new TargetInitialisationResult("aaa", "mock", TargetStatus.OK)
			TargetInitialisationResult t3 = new TargetInitialisationResult("ddd", "mock", TargetStatus.OK)
			TargetInitialisationResult t4 = new TargetInitialisationResult("bbb", "mock", TargetStatus.OK)
		and :
			def list = new HashSet<TargetInitialisationResult>()
			list.add(t1)
			list.add(t2)
			list.add(t3)
			list.add(t4)
			testee.setTargetInitialisationResults(list)
		when :
			List<TargetInitialisationResult> result = testee.getTargetInitialisationResults()	
		then :
			result.get(0) == t2
			result.get(1) == t4
			result.get(2) == t1
			result.get(3) == t3
	}
	
}


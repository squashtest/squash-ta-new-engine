/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
// *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2014 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.ta.backbone.init

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.squashtest.ta.backbone.pseudoimplem.PseudoBinaryAssertion;
import org.squashtest.ta.backbone.pseudoimplem.PseudoBinaryAssertionBis;
import org.squashtest.ta.backbone.pseudoimplem.PseudoCommand;
import org.squashtest.ta.backbone.pseudoimplem.PseudoConverter;
import org.squashtest.ta.backbone.pseudoimplem.PseudoResource;
import org.squashtest.ta.backbone.pseudoimplem.PseudoTarget;
import org.squashtest.ta.backbone.pseudoimplem.PseudoUnaryAssertion;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.SpringObjectFactory;
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.backbone.exception.ResourceTypeConflictException;
import org.squashtest.ta.backbone.tools.ReflectionUtils;

import spock.lang.Specification;

class EngineComponentDefinitionManagerTest extends Specification {
	
	EngineComponentDefinitionManager definitionManager
	ReflectionUtils reflector
	BeanFactory springFactory;
	
	def nat1 = new Nature("nat1")
	def nat2 = new Nature("nat2")
	def nat3 = new Nature("nat3")
	
	def setup(){
		
		definitionManager= new EngineComponentDefinitionManager()
		reflector = Mock(ReflectionUtils)
		springFactory = Mock(BeanFactory)
		
		definitionManager.reflector = reflector
		definitionManager.springFactory = springFactory
		
				
	}
	
	
	def "should create a category if not exist then return it"(){
		
		given :
			reflector.getEngineComponentValue(_) >> "test"
		
		when :
			def result = definitionManager.fetchOrAddCategory(String.class)
		
		
		then :		
			result.name == "test"
			definitionManager.categoryMap.containsKey("test")
			definitionManager.categoryMap.containsValue(result)
		
	}
	
	def "should simply return the category if already exists"(){
		given :
			reflector.getEngineComponentValue(_) >> "nat1"
			definitionManager.categoryMap.put("nat1", nat1)
		when :
			def result = definitionManager.fetchOrAddCategory(String.class)
			
		then :
			result == nat1
	}
	
	
	def "should return the type if exist"(){
		given :
			def clazz = PseudoResource.class
			definitionManager.naturesMap.put(clazz, nat1)
		
		when :
			def result = definitionManager.fetchOrAddUniqueType(clazz)
			
		then :
			result == nat1
			
	}
	
	def "should create the type if not exist then return it"(){
		
		given :
			def clazz = PseudoTarget.class
			reflector.getResourceTypeValue(clazz) >> "target"
			
		when :
			def result = definitionManager.fetchOrAddUniqueType(clazz)
		
		then :
			result.name == "target"
			definitionManager.naturesMap.containsKey(clazz)
			definitionManager.naturesMap[clazz].name=="target"
		
	}
	
	def "should rant because the supplied argument is of invalid class"(){
		
		when :
			definitionManager.fetchOrAddUniqueType(String.class)
		
		then :
			thrown EngineInitException
		
	}
	
	def "should rant because a resource type of the argument is already mapped to another class"(){
		
		given :
			definitionManager.naturesMap.put(String.class, nat1)
			def clazz = PseudoResource.class
			reflector.getResourceTypeValue(clazz) >> "nat1"
		when :
			definitionManager.fetchOrAddUniqueType(clazz)
		
		then :
			thrown ResourceTypeConflictException
			
	}
	
	def "should register a new converter definition"(){
		
		given :
			definitionManager.reflector = new ReflectionUtils();
					
		when :
			definitionManager.processAsResourceConverter(PseudoConverter.class)
			
		then :
			definitionManager.converterDefinitions[0]
			.collect{ return [it.firstNature.name, it.secondNature.name, it.category.name, it.componentClass]}	== [
					[ "file", "pseudoresource", "pseudo", PseudoConverter.class] ]

	}
	
	def "should register a new command definition"(){
		
		given :
			definitionManager.reflector = new ReflectionUtils();
			
		when :
			definitionManager.processAsCommand(PseudoCommand.class)
			
		then :
			definitionManager.commandDefinitions[0]
			.collect{ return [it.firstNature.name, it.secondNature.name, it.category.name, it.componentClass]} == [
					["pseudoresource", "pseudotarget", "pseudo", PseudoCommand.class]
				]
	}
	
	def "should register a new binary assertion definition"(){
		
		given :
			definitionManager.reflector = new ReflectionUtils();
			
		when :
			definitionManager.processAsBinaryAssertion(PseudoBinaryAssertion.class)	
		
		then : 
		
			definitionManager.binaryAssertionDefinitions[0]
			.collect{ return [it.firstNature.name, it.secondNature.name, it.category.name, it.componentClass]} == [
					["pseudoresource", "pseudoresource", "binaryassertion", PseudoBinaryAssertion.class]
				]
	}
	
	def "[new annotations] should register a new binary assertion definition"(){
		
		given :
			definitionManager.reflector = new ReflectionUtils();
			
		when :
			definitionManager.processAsBinaryAssertion(PseudoBinaryAssertionBis.class)
		
		then :
		
			definitionManager.binaryAssertionDefinitions[0]
			.collect{ return [it.firstNature.name, it.secondNature.name, it.category.name, it.componentClass]} == [
					["pseudoresource.bis", "pseudoresource", "binaryassertion.bis", PseudoBinaryAssertionBis.class]
				]
	}
	
	
	def "should register a new unary assertion definition"(){
		
		given :
			definitionManager.reflector = new ReflectionUtils();
			
		when :
			definitionManager.processAsUnaryAssertion(PseudoUnaryAssertion.class)
		
		then :
		
			definitionManager.unaryAssertionDefinitions[0]
			.collect{ return [it.firstNature.name, it.secondNature, it.category.name, it.componentClass]} == [
					["pseudoresource", UnaryAssertionDefinition.UNARY_ASSERTION_NATURE, "unaryassertion", PseudoUnaryAssertion.class]
				]
	}
	
	//almost all the class is being tested here
	def "[test A] should process a beanfactory, set the scope and sort the classes"(){
		
		given :
			def setup = configureTest_A()
			def factory = setup[0]
			def defs = setup[1..4]
			definitionManager.reflector = new ReflectionUtils();
			
		when :
			definitionManager.postProcessBeanFactory(factory)
			
			def objFactory = definitionManager.getFactory()
			def convDefinitions = definitionManager.getConverterDefinitions()
			def commDefinitions = definitionManager.getCommandDefinitions()
			def unarDefinitions = definitionManager.getUnaryAssertionDefinitions()
			def binaDefinitions = definitionManager.getBinaryAssertionDefinitions()
			
		then :
			
			1 * defs[0].setScope(BeanDefinition.SCOPE_PROTOTYPE)
			1 * defs[1].setScope(BeanDefinition.SCOPE_PROTOTYPE)
			1 * defs[2].setScope(BeanDefinition.SCOPE_PROTOTYPE)
			1 * defs[3].setScope(BeanDefinition.SCOPE_PROTOTYPE)
		
			objFactory instanceof SpringObjectFactory
			objFactory.beanFactory == factory
			
			convDefinitions.size() == 1
			commDefinitions.size() == 1
			unarDefinitions.size() == 1
			binaDefinitions.size() == 1
		
	}
	
	
	def "[test B] should rant because a bean definition references an unknown class"(){
		
		given :
			def factory = configureTest_B()
			definitionManager.reflector = new ReflectionUtils();
		when :
			definitionManager.postProcessBeanFactory(factory)
			
		then :
			def ex = thrown(EngineInitException)
			ex.getCause() instanceof ClassNotFoundException
	}
	
	
	private configureTest_A = {
		
		def factory = Mock(ConfigurableListableBeanFactory)
		def beanDef1 = Mock(BeanDefinition)
		def beanDef2 = Mock(BeanDefinition)
		def beanDef3 = Mock(BeanDefinition)
		def beanDef4 = Mock(BeanDefinition)
		
		factory.getBeanDefinitionNames() >> [ "def1", "def2", "def3", "def4" ]
		
		beanDef1.getBeanClassName() >> PseudoConverter.class.getName();
		beanDef2.getBeanClassName() >> PseudoCommand.class.getName();
		beanDef3.getBeanClassName() >> PseudoBinaryAssertion.class.getName();
		beanDef4.getBeanClassName() >> PseudoUnaryAssertion.class.getName();
		
		factory.getBeanDefinition(_) >>> [beanDef1, beanDef2, beanDef3, beanDef4]
		
		return [factory, beanDef1, beanDef2, beanDef3, beanDef4]
	}
	
	private configureTest_B = {
		
		def factory = Mock(ConfigurableListableBeanFactory)
		def beanDef1 = Mock(BeanDefinition)
		
		factory.getBeanDefinitionNames() >> [ "def1"]
		beanDef1.getBeanClassName() >> "ab.cd.ef.UnknownClass"
		factory.getBeanDefinition(_) >> beanDef1
		
		return factory
	
	}
	
}

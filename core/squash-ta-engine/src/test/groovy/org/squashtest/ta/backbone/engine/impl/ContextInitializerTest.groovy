/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl;



import java.util.Properties;

import org.squashtest.ta.backbone.pseudoimplem.PseudoConverter;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.engine.CommandManager;
import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.ResourceConverterManager;
import org.squashtest.ta.backbone.engine.ResourceLoader;
import org.squashtest.ta.backbone.engine.impl.ContextInitializer;
import org.squashtest.ta.backbone.engine.impl.ContextManagerImpl;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.backbone.init.RepositoryCreatorPool;
import org.squashtest.ta.backbone.init.TargetCreatorPool;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.facade.TFTestWorkspaceBrowser;

import spock.lang.Specification;

public class ContextInitializerTest extends Specification {

	ContextInitializer initializer 
	
	RepositoryCreatorPool repoCrePool
	
	TargetCreatorPool targCrePool
	
	ContextManagerImpl manager;

	EngineComponentDefinitionManager definitions
	
	TFTestWorkspaceBrowser browser
	
	def nat1 = new Nature("nat1")
	def nat2 = new Nature("nat2")
	def nat3 = new Nature("nat3")
	
	
	def setup(){
		
		initializer=new ContextInitializer()
		
		repoCrePool = Mock()
		targCrePool = Mock()
		definitions = Mock()
		browser 	= Mock()
		manager		= Mock()

		initializer.repoCreatorPool=repoCrePool
		initializer.targetCreatorPool = targCrePool
		initializer.workspaceBrowser = browser
		initializer.definitionsManager = definitions
	}
	

	
	def "should populate the loader with repositories"(){
			
		given :			
		
			browser.getRepositoriesDefinitions () >> initList();
			
		and :
			def rep1 = Mock(RepositoryWrapper)
			def rep2 = Mock(RepositoryWrapper)
			
			rep1.getName() >> "rep1"
			rep2.getName() >> "rep2"
			
		and :
			repoCrePool.createRepository(_) >>> [rep1, rep2]
			
		
		and :
			def defRep = Mock(ResourceRepository)	
			def defWrap = Mock(RepositoryWrapper)		
			defWrap.getName() >> "default.repository"
			browser.getDefaultResourceRepository() >> defRep
			repoCrePool.wrapRepository(defRep) >> defWrap
			
		when :
			initializer.initResourcesLoader(manager)

		then :
			1 * manager.setResourceLoader( { 
				it.repositories.keySet() == [ "default.repository", "rep1", "rep2"] as Set
			} )		
			
	}
	
	def "should populate the manager with targets"(){
		
		given :
			browser.getTargetsDefinitions() >> initList()
			
		and :
			def tar1 = Mock(TargetWrapper)
			def tar2 = Mock(TargetWrapper)
			def tar3 = Mock(TargetWrapper)
			
			tar1.getName() >> "tar1"
			tar2.getName() >> "tar2"
			tar3.getName() >> "tar3"
		and:
			targCrePool.mustBeCreated(_, _) >>> [true, true, true]
			targCrePool.createTarget(_) >>> [tar1, tar2, tar3]
			targCrePool.wrapTarget(_)>>Mock(TargetWrapper)
			targCrePool.createLocalTarget()>>Mock(TargetWrapper)			
		when :
			initializer.initTargets(manager)
			
		then :
			1 * manager.registerTarget(tar1)
			1 * manager.registerTarget(tar2)
			//tar3 is in the workspace but not in the tests: it must not be instanciated
			0 * manager.registerTarget(tar3)
			
	}

	
	def "should raise the code coverage by testing dumb initConverterManager() "(){
		
		given :
			def def1 = new ConverterDefinition(nat1, nat2, nat3, PseudoConverter.class)
			def def2 = new ConverterDefinition(nat2, nat3, nat1, PseudoConverter.class)
			def def3 = new ConverterDefinition(nat3, nat1, nat2, PseudoConverter.class)
			
					
		and :
			definitions.getConverterDefinitions() >> [def1, def2, def3]
			
		when :
			initializer.initConverterManager(manager)
		
		then :
			1 * manager.setResourceConverterManager({ 
				it instanceof ResourceConverterManager
			})
		
	}
	
	
	private initList = {
		
		def file1 = new URL("file:/dev/null/tar1")
		def file2 = new URL("file:/dev/null/tar2")
				
		def list = [ file1, file2]
	
		targCrePool.getShortName(file1.path) >> "tar1"
		targCrePool.getShortName(file2.path) >> "tar2"
		
		return list
	
	}
	
}

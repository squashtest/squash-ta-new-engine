/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultExecuteCommandRunner
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.UnaryAssertionHandler;
import org.squashtest.ta.backbone.exception.CannotApplyAssertionException
import org.squashtest.ta.backbone.exception.setup.IncompatibleNaturesException
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.exception.TargetNotFoundException
import org.squashtest.ta.backbone.exception.MalformedInstructionException
import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.instructions.UnaryAssertionInstruction
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction
import org.squashtest.ta.framework.components.Command
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.components.Target
import org.squashtest.ta.framework.exception.CannotApplyCommandException;
import org.squashtest.ta.framework.exception.BrokenTestException
import org.squashtest.ta.backbone.exception.IncompatibleNaturesException

import spock.lang.Specification

class DefaultUnaryAssertionRunnerTest extends Specification {
	
	DefaultUnaryAssertionRunner runner
	InternalTestRunner testRunner

	UnaryAssertionInstruction instruction
	ContextManager manager
	
	
	def setup(){
		instruction = new UnaryAssertionInstruction()
		runner = new DefaultUnaryAssertionRunner(instruction)
		
		testRunner = Mock(InternalTestRunner)
		
		runner.setTestRunner testRunner
		
		manager = Mock(ContextManager)
		
	}
	
	
	def "should rant because the actual result name is missing"(){
		when :
			runner.checkSettings()
		
		then :
			thrown MalformedInstructionException
	}
	
	
	def "should rant because the assertion name is missing"(){
		given :
			instruction.getActualResultName() >> "actual"
		
		when :
			runner.checkSettings()
		then :
			thrown MalformedInstructionException;
	}
	
	

	def "should be clean"(){
		given :
			mockUnaryAssertionInstruction()
			
		when :
			runner.checkSettings()
			
		then :
			true
	}
	

	
	def "should return the instruction"(){
		expect :
			runner.getInstruction() == instruction
		
	}
	
	def "should rant because the actual result is not found"(){
		given :
			mockUnaryAssertionInstruction()
			
		when :
			runner.getActualResult()
			
		then :
			thrown ResourceNotFoundException
		
	}

	
	def "should work normally for unary assertions"(){
		
		given :
			mockUnaryAssertionInstruction()
			def actual = mockActualResult()
			
		and :
			def assertion = Mock(UnaryAssertionHandler)
			
		and :
			testRunner.getContextManager() >> manager
			manager.getUnaryAssertion(_) >> [assertion]
			
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"actual")) >> actual
						
		when :
			runner.run()
			
		then :
			1 * assertion.setActualResult(actual)
	}
	

	
	def "should diagnose error and return incompatible nature exception"(){
		
		given :
			mockUnaryAssertionInstruction()
			def actual = mockActualResult()
		
		and :
			testRunner.getContextManager() >> manager
			manager.getUnaryAssertion(_) >> [Mock(UnaryAssertionHandler)]
			
		when :
			def res = runner.diagnoseError(actual,  "test")
			
		then :
			res instanceof IncompatibleNaturesException
		
	}
	

	def "should diagnose error and return a CannotApplyAssertionException"(){
		
		given :
			mockUnaryAssertionInstruction()
			def actual = mockActualResult()
			
		and :
			testRunner.getContextManager() >> manager
			manager.getUnaryAssertion(_) >> []
			
		when :
			def res = runner.diagnoseError(actual, "test")
			
		then :
			res instanceof CannotApplyAssertionException
		
		
	}
		

	def "should fail because autoconvert is set to false (unary)"(){
		
		given :
			mockUnaryAssertionInstruction()
			def actual = mockActualResult()
			
		and :
			
			testRunner.getContextManager() >> manager
			testRunner.getResourceFromCache(_) >> actual
		and :
			testRunner.isAutoConvert() >> false
			//first try it returns no result, second time it returns something
			//since constraints are relaxed
			manager.getUnaryAssertion(_) >>> [ [], [1] ]
			
		when :
			runner.setupAssertion(actual)
			
		then :
			thrown IncompatibleNaturesException
	}
	
	/* *************************** ************************** */

	def mockUnaryAssertionInstruction = {
		instruction.setActualResultName(new ResourceName(Scope.SCOPE_TEST,"actual"))
		instruction.setAssertionCategory("assertion")
		//no need to setup an empty configuration: the default is empty		
	}
	
	def mockActualResult = {
		def actual = Mock(ResourceWrapper)
		def actNature = Mock(Nature)
		actNature.getName() >> "nature13"
		actual.getNature() >> actNature
		actual.getName() >> new ResourceName(Scope.SCOPE_TEST,"actual")
		return actual		
	}
	

	
}

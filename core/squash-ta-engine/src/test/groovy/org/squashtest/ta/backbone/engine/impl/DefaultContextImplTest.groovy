/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.backbone.engine.wrapper.LocalContext;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;

import spock.lang.Specification;

/**
 * Test for the default {@link LocalContext} implementation.
 * @author edegenetais
 *
 */
class DefaultContextImplTest extends Specification{
	
	def testee
	def wantedResource
	def origName
	
	def setup(){
		testee=new DefaultLocalContextImpl(10)
		origName=new ResourceName(Scope.SCOPE_TEST, "wanted!");
		wantedResource=Mock(ResourceWrapper)
		wantedResource.getName() >> origName
	}
	
	def "should not find non existent resource"(){
		given:
			def name=new ResourceName(Scope.SCOPE_TEST,"wanted!")
		when:
			def resultat=testee.getResource(name)
		then:
			resultat == null
	}
	
	def "should find resource after adding it"(){
		given:
			testee.addResource(wantedResource)
		and:
			def queryName=new ResourceName(Scope.SCOPE_TEST,"wanted!")
		when:
			def resultat=testee.getResource(queryName)
		then:
			resultat == wantedResource
	}
	
	def "should give the right resource"(){
		given:
			testee.addResource(wantedResource)
		and:
			def dummyName=new ResourceName(Scope.SCOPE_TEST, "dummy!");
			def dummyResource=Mock(ResourceWrapper)
			dummyResource.getName() >> dummyName
			testee.addResource(dummyResource)
		and:
			def queryName=new ResourceName(Scope.SCOPE_TEST,"wanted!")
		when:
			def resultat=testee.getResource(queryName)
		then:
			resultat==wantedResource
			resultat!=dummyResource
			resultat!=null
	}
	
	def "should give no resource if the name is not known"(){
		given:
			testee.addResource(wantedResource)
		and:
			def unknownName=new ResourceName(Scope.SCOPE_TEST,"unknownName")
		when:
			def resultat=testee.getResource(unknownName)
		then:
			resultat != wantedResource //we didn't ask for that one...
			resultat == null
	}
	
	def "should swear if you add a resource with an already existing name"(){
		given:
			testee.addResource(wantedResource)
		and:
			def twinName=new ResourceName(Scope.SCOPE_TEST,"wanted!")
			def twinResource=Mock(ResourceWrapper)
			twinResource.getName() >> twinName
		when:
			testee.addResource(twinResource)
		then:
			thrown NotUniqueEntryException		
	}
	
	def "should cleanup all resources and unregister'em"(){
		given:
			testee.addResource(wantedResource)
		and:
			def otherName=new ResourceName(Scope.SCOPE_TEST, "otherResource")
			def otherResource=Mock(ResourceWrapper)
			otherResource.getName() >> otherName
			testee.addResource(otherResource)
		when:
			testee.cleanUp()
		then:
			testee.getResource(origName) == null
			testee.getResource(otherName)  == null
			1 * wantedResource.cleanUp()
			1 * otherResource.cleanUp()
	}
}

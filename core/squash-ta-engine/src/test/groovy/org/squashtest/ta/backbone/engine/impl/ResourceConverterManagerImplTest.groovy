/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import java.util.Collection;

import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.engine.ResourceConverterManager;
import org.squashtest.ta.backbone.engine.impl.ResourceConverterManagerImpl;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.engine.wrapper.ResourceConverterHandler;
import org.squashtest.ta.backbone.exception.DuplicateFactoryException
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.pseudoimplem.PseudoConverter;
import org.squashtest.ta.backbone.pseudoimplem.PseudoResource;
import org.squashtest.ta.backbone.tools.AggregativeChristmasTree;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;

import spock.lang.Specification;

class ResourceConverterManagerImplTest extends Specification {
	
	ResourceConverterManagerImpl manager 
	ObjectFactory factory 
	
	Nature nat1 = new Nature("nat1")
	Nature nat2 = new Nature("nat2")
	Nature nat3 = new Nature("nat3")
	Nature nat4 = new Nature("nat4")
	Nature nat5 = new Nature("nat5")
	Nature nat6 = new Nature("nat6")
	

	
	def setup(){
		manager = new ResourceConverterManagerImpl();
		factory = Mock(ObjectFactory)
		manager.factory=factory;
	}
	
	def "should add a converter definition"(){
		
		given :
			def converterClass = PseudoConverter.class
			def definition = new ConverterDefinition(nat1, nat3, nat4, converterClass)
		and :
			def cmd = new PseudoConverter()
			factory.newInstance(converterClass) >> cmd
		when :
			manager.addConverterDefinition(definition)
			
			def res = manager.getAllConverters(nat1, nat3, nat4)
			
		then :
			res.size() == 1
			def handler = res.get(0)
			handler.inputNature == nat1
			handler.outputNature == nat3
			handler.converterCategory == nat4

		
	}
	
	
	def "should rant when trying to add twice the same definition"(){
		given :
			def definition = new ConverterDefinition(nat1, nat3, nat4, PseudoConverter.class)

		when :
			manager.addConverterDefinition(definition)
			manager.addConverterDefinition(definition)

		then :
			thrown DuplicateFactoryException
	
	}
	
	
	
	def "should remove a converter factory (1)"(){
		given :
			def definition = new ConverterDefinition(nat1, nat3, nat4, String.class)
			
		when :
			manager.addConverterDefinition(definition)
			manager.removeConverterDefinition(definition)
			def res = manager.getAllConverters(nat1, nat3, nat4)

		then :
			res.size()==0
	}
	
	
	def "should remove a converter factory (2)"(){
		given :
			def definition =  new ConverterDefinition(nat1, nat3, nat4, String.class)

		when :
			manager.addConverterDefinition(definition)
			manager.removeConverterDefinition(nat1, nat3, nat4)
			def res = manager.getAllConverters(nat1, nat3, nat4)
	
		then :
			res.size()==0
		
	}
	
	
	def "should get a subset of converters (1)"(){
		
		given :
			def definitions = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(definitions, converters)
			
		and :
			definitions.each{manager.addConverterDefinition(it)}
			
		when :
			def handlers = manager.getAllConverters(null, nat3, null)
			
		then :
			handlers.size() == 4
			handlers.collect{it.outputNature} == [nat3, nat3, nat3, nat3]
			def wrappeds = handlers.collect{it.wrappedConverter};
			wrappeds.containsAll([
				converters[0],
				converters[1],
				converters[4],
				converters[5]
			])
	}
	
	def "should get a subset of converters (2)"(){
		
		given :
			def factories = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(factories, converters)
			
		and :
			factories.each{manager.addConverterDefinition(it)}
			
		when :
			def res = manager.getAllConverters(null, nat4, nat5)
			
		then :
			res.size()==2
			res.collect{it.wrappedConverter}.containsAll([
				converters[2],
				converters[6]
			])
		
	}
	
	def "should get one converter"(){
		given :
			def factories = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(factories, converters)
			
		and :
			factories.each{manager.addConverterDefinition(it)}
			
		when :
			def res = manager.getAllConverters(nat1, nat4, nat6)
			
		then :
			res.size()==1
			res.collect{it.wrappedConverter}.containsAll([
				converters[3]
			])
	
	}

	def "should get all converters"(){
		given :
			def factories = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(factories, converters)
			
		and :
			factories.each{manager.addConverterDefinition(it)}
			
		when :
			def res = manager.getAllConverters(null, null, null)
			
		then :
			res.size()==8
			res.collect{it.wrappedConverter}.containsAll(converters)
	}
		
	def "should not find a converter"(){
		given :
			def factories = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(factories, converters)
			
		and :
			def manager = new ResourceConverterManagerImpl()
			factories.each{manager.addConverterDefinition(it)}
			
		when :
			def res = manager.getAllConverters(nat3, nat3, nat5)
			
		then :
			res.size()==0
		
	}
	
	def "should get all converters by name arguments"(){
		
		given :
			def factories = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(factories, converters)
			
		and :
			factories.each{manager.addConverterDefinition(it)}
		
		when :
			def res = manager.getAllConvertersByName(null, null, null)
		
		then :
			res.size()==8
			res.collect{it.wrappedConverter}.containsAll(converters)
	}
	
	
	def "should rant because the resource nature wasn't found by name"(){
		given :
			def manager = new ResourceConverterManagerImpl()
		
		when :
			manager.getAllConvertersByName("test", null, null)
		then :
			thrown ImpossibleConversionException
		
	}
		
	def "should return void because the target nature wasn't found by name"(){
		given :
			def factories = buildDefinitionSet()
			def converters = buildResourceConverterSet()
			associate(factories, converters)
			
		and :
			def manager = new ResourceConverterManagerImpl()
			factories.each{manager.addConverterDefinition(it)}
			
		when :
			manager.getAllConvertersByName("nat1", "test", null)
			
		then :
			thrown ImpossibleConversionException
		
	}
	
	

	
	
	/* ***************** utils **************** */
	

	
	def buildDefinition = { a, b, c, t->
		return new ConverterDefinition(a,b,c,t)
	}
	
	
	def buildResourceConverter = { a, b , c ->
		def converter = new c1()	
		return converter
	}
	
	def buildDefinitionSet = {
		def definitions = []
		
		def counter=0
		
		def testClasses = [ c1.class, c2.class, c3.class, c4.class, c5.class, c6.class, c7.class, c8.class]
		
		[ nat1, nat2].each{i ->
				[nat3, nat4].each{o ->
					[nat5, nat6].each{ c -> definitions << buildDefinition(i, o, c, testClasses[counter++]) }
			}
		}
		return definitions
	}
	
	@TAResourceConverter("1")
	class c1 implements ResourceConverter<FileResource, PseudoResource>{

		@Override
		public float rateRelevance(FileResource input) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void addConfiguration(Collection<Resource<?>> configuration) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public PseudoResource convert(FileResource resource) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void cleanUp() {
			throw new UnsupportedOperationException("TODO");
		}
	}
	
	@TAResourceConverter("2")
	abstract class c2 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	@TAResourceConverter("3")
	abstract class c3 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	@TAResourceConverter("4")
	abstract class c4 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	@TAResourceConverter("5")
	abstract class c5 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	@TAResourceConverter("6")
	abstract class c6 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	@TAResourceConverter("7")
	abstract class c7 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	@TAResourceConverter("8")
	abstract class c8 implements ResourceConverter<FileResource, PseudoResource>{
	}
	
	def buildResourceConverterSet = {
		def converters = []
		
		[ nat1, nat2].each{r ->
			[nat3, nat4].each{t ->
					[nat5, nat6].each{ n -> 
						converters << buildResourceConverter(r, t, n) }
			}
		}
		return converters
	}
	
	
	def associate = { defs, convs ->
		8.times { factory.newInstance(defs[it].componentClass) >> convs[it] }	
	}
	
	
}

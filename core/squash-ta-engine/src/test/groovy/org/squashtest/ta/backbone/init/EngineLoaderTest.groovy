/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.init

import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource
import org.squashtest.ta.backbone.init.ScannerComponentLoader.ContextFactory
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.tools.TempDir;

import spock.lang.Specification

class EngineLoaderTest extends Specification {
	def testee
	def mockContextFactory
	def mockConf
	def conf
	
	def setup(){
		testee=new EngineLoader()
		mockContextFactory=Mock(ContextFactory)
		testee.factory=mockContextFactory
		
		def mockEnumerator=Mock(ComponentPackagesEnumerator)
		mockConf = Mock(ComponentConfiguration)
		mockEnumerator.getComponentConfiguration() >> mockConf
		testee.enumerator=mockEnumerator
		
	}
	
	def "should insert base packages"(){
		given:
			mockConf.getBasePackages() >> ["org.squashtest.ta.backbone.pseudoimplem"]
			mockConf.getExcludedElements() >> []
		and:
			def referenceConf=loadConfBytes("basePackageOnlyConf.xml")
		when:
			testee.load()
		then:
			1 * mockContextFactory.createContext({conf=loadConfBytes(it)}) >> Mock(ApplicationContext)
			referenceConf.equals(conf)
	}
	
	def "should insert excluded elements"(){
		given:
			mockConf.getBasePackages() >> []
			mockConf.getExcludedElements() >> ["org.squashtest.ta.backbone.pseudoimplem.*"]
		and:
			def referenceConf=loadConfBytes("excludedElementsOnlyConf.xml")
		when:
			testee.load()
		then:
			1 * mockContextFactory.createContext({conf=loadConfBytes(it)}) >> Mock(ApplicationContext)
			referenceConf.equals(conf)
	}
	
	def "should insert base package + excluded elements"(){
		given:
			mockConf.getBasePackages() >> ["org.squashtest.ta.backbone.pseudoimplem"]
			mockConf.getExcludedElements() >> ["org.*.Dummy"]
		and:
			def referenceConf=loadConfBytes("completelySubstituedConf.xml")
		when:
			testee.load()
		then:
			1 * mockContextFactory.createContext({conf=loadConfBytes(it)}) >> Mock(ApplicationContext)
			referenceConf.equals(conf)
	}
	
	def "should include the exclude file in the configuration if specified"(){
		given:
			mockConf.getBasePackages() >> ["org.squashtest.ta.backbone.pseudoimplem"]
			mockConf.getExcludedElements() >> ["org.*.Dummy"]
		and:
			File tempFile=File.createTempFile("enginetest", "exclude", TempDir.getExecutionTempDir());
			BinaryData excludeData=new BinaryData(getClass().getResource("testExcludeFile.txt"))
			excludeData.write(tempFile);
			
			System.setProperty(EngineLoader.SQUASH_COMPONENT_EXCLUDEFILE, tempFile.getAbsolutePath())
			def excluded = null
		and:
			mockContextFactory.createContext(_) >> Mock(ApplicationContext)
		when:
			testee.load()
		then:
			1 * mockConf.addGlobalExcludedElements({excluded=it})
			excluded.containsAll(["excluded1","excluded2"])
			["excluded1","excluded2"].containsAll(excluded)
		cleanup:
			System.clearProperty(EngineLoader.SQUASH_COMPONENT_EXCLUDEFILE)
	}
	
	def "should not include the exclude file in the configuration if ommitted"(){
		given:
			mockConf.getBasePackages() >> ["org.squashtest.ta.backbone.pseudoimplem"]
			mockConf.getExcludedElements() >> ["org.*.Dummy"]
		and:
			File tempFile=File.createTempFile("enginetest", "exclude", TempDir.getExecutionTempDir());
			BinaryData excludeData=new BinaryData(getClass().getResource("testExcludeFile.txt"))
			excludeData.write(tempFile);
			//we want the file to exist, but the property NOT to exist
			System.clearProperty(EngineLoader.SQUASH_COMPONENT_EXCLUDEFILE)
			def excluded = null
		and:
			mockContextFactory.createContext(_) >> Mock(ApplicationContext)
		when:
			testee.load()
		then:
			0 * mockConf.addGlobalExcludedElements(_)
	}
	
	def loadConfBytes(org.springframework.core.io.Resource r){
		return loadConfBytes(r.getInputStream());
	}
	
	def loadConfBytes(String confName){
		def confURL=getClass().getResource(confName)
		def inStream=confURL.openStream();
		return loadConfBytes(inStream);
	}
	
	def loadConfBytes(InputStream inStream){
		byte[] buffer=new byte[4096];
		ByteArrayOutputStream contentBuffer=new ByteArrayOutputStream();
		int nb=inStream.read(buffer);
		while(nb>=0){
			contentBuffer.write(buffer, 0, nb);
			nb=inStream.read(buffer);
		}
		inStream.close()
		return contentBuffer.toString("UTF-8");
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import java.util.Collection;

import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.engine.CommandManager
import org.squashtest.ta.backbone.engine.impl.CommandManagerImpl
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.exception.DuplicateFactoryException
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.pseudoimplem.PseudoCommand;
import org.squashtest.ta.backbone.pseudoimplem.PseudoTarget;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.facade.Engine;

import spock.lang.Specification

class CommandManagerImplTest extends Specification {

	Nature nat1=new Nature("nat1")
	Nature nat2=new Nature("nat2")
	Nature nat3=new Nature("nat3")
	Nature nat4=new Nature("nat4")
	Nature nat5=new Nature("nat5")
	Nature nat6=new Nature("nat6")
	Nature nat7=new Nature("nat7")
	
	CommandManager manager
	ObjectFactory factory
	
	def setup(){
		manager = new CommandManagerImpl();
		factory = Mock(ObjectFactory)
		manager.factory=factory
	}
	
	def "should add a command definition"(){
		
		given :
			def commandClass = PseudoCommand.class
		and:
			def definition = new CommandDefinition(nat1, nat3, nat5, nat7, commandClass)
		and:
			def ass = commandClass.newInstance()
			factory.newInstance(commandClass) >> ass
			
		when :
			manager.addCommandDefinition(definition)
			
			def res = manager.getAllCommands(nat1, nat3, nat5)
			
		then :
			res.size() == 1
			def handler = res.get(0)
			handler.resourceNature==nat1
			handler.targetNature==nat3
			handler.commandCategory==nat5
		
	}
	
	def "should rant when trying to add twice the same definition"(){
		given :
			def definition = new CommandDefinition(nat1, nat2, nat3, nat7, PseudoCommand.class)

		when :
			manager.addCommandDefinition(definition)
			manager.addCommandDefinition(definition)

		then :
			thrown DuplicateFactoryException
	
	}
	
	def "should remove a command definition (1)"(){
		given :
			def definition= new CommandDefinition(nat1, nat2, nat3, nat7, PseudoCommand.class)
			
		when :
			manager.addCommandDefinition(definition)
			manager.removeCommandDefinition(definition)
			def res = manager.getAllCommands(nat1, nat2, nat3)

		then :
			res.size()==0		
	}
	
	def "should remove a command factory (2)"(){
		given :
			def definition = new CommandDefinition(nat1, nat2, nat3, nat7, PseudoCommand.class)
	
		when :
			manager.addCommandDefinition(definition)
			manager.removeCommandDefinition(nat1,nat2,nat3)
			def res = manager.getAllCommands(nat1, nat2, nat3)
		then :
			res.size()==0
		
	}
	
	
	def "should get a subset of commands (1)"(){
		
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and : 
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(null, nat3, null)
			
		then :
			res.size() == 4
			res.collect{it.wrappedCommand}.containsAll([
				commands[0],
				commands[1],
				commands[4],
				commands[5]	
			])
	}
	
	def "should get a subset of commands (2)"(){
		
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(null, nat4, nat5)
			
		then :
			res.size()==2
			res.collect{it.wrappedCommand}.containsAll([
				commands[2],
				commands[6]	
			])
		
	}
	
	def "should get one command"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(nat1, nat4, nat6)
			
		then :
			res.size()==1
			res.collect{it.wrappedCommand}.containsAll([
				commands[3]
			])
	
	}

	def "should get all commands"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(null, null, null)
			
		then :
			res.size()==8
			res.collect{it.wrappedCommand}.containsAll(commands)
	}
		
	def "should not find a command"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			def res = manager.getAllCommands(nat1, nat1, nat3)
			
		then :
			res.size()==0
		
	}
	
	def "should get all commands by name arguments"(){
		
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
		
		when :
			def res = manager.getAllCommandsByName(null, null, null)
		
		then :
			res.size()==8
			res.collect{it.wrappedCommand}.containsAll(commands)		
	}
	
	
	def "should rant because the resource nature is unknown"(){
		when :
			manager.getAllCommandsByName("test", null, null)
		then :
			thrown ResourceNotFoundException
		
	}
		
	def "should rant because the target nature is unknown"(){
		given :
			def factories = buildDefinitionSet()
			def commands = buildCommandSet()
			associate(factories, commands)
			
		and :
			factories.each{manager.addCommandDefinition(it)}
			
		when :
			manager.getAllCommandsByName("nat1", "test", null)
			
		then :
			thrown ResourceNotFoundException
		
	}
	
	
	/* ************************ scaffolding code ********************* */
	
	
	def buildDefinition = { a, b, c, t ->
		return new CommandDefinition(a,b,c, nat7, t)
	}
	
	def buildCommand = { a, b , c ->
		def command = new c1()
		return command
	}
	
	def buildDefinitionSet = {
		def definitions = [] 	
		
		
		def counter=0		
		//the following items just need to be different types of Class.
		def testClasses = [ c1.class, c2.class, c3.class, c4.class, c5.class, c6.class, c7.class, c8.class]
		
		[ nat1, nat2].each{r ->
				[nat3, nat4].each{t ->
					[nat5, nat6].each{ n -> definitions << buildDefinition(r, t, n, testClasses[counter++]) }
			}
		}
		return definitions
	}
	
	@TACommand("1")
	class c1 implements Command<FileResource, PseudoTarget>{

		@Override
		public void addConfiguration(Collection<Resource<?>> configuration) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void setTarget(PseudoTarget target) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void setResource(FileResource resource) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public Resource<VoidResource> apply() {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void cleanUp() {
			throw new UnsupportedOperationException("TODO");
		}
		
	}
	
	@TACommand("2")
	abstract class c2 implements Command<FileResource, PseudoTarget>{
		
	}
	
	@TACommand("3")
	abstract class c3 implements Command<FileResource, PseudoTarget>{
		
	}
	
	@TACommand("4")
	abstract class c4 implements Command<FileResource, PseudoTarget>{
		
	}
	
	@TACommand("5")
	abstract class c5 implements Command<FileResource, PseudoTarget>{
		
	}
	
	@TACommand("6")
	abstract class c6 implements Command<FileResource, PseudoTarget>{
		
	}
	
	@TACommand("7")
	abstract class c7 implements Command<FileResource, PseudoTarget>{
		
	}
	
	@TACommand("8")
	abstract class c8 implements Command<FileResource, PseudoTarget>{
		
	}
	
	def buildCommandSet = {
		def commands = []	
	
		[ nat1, nat2].each{r ->
				[nat3, nat4].each{t ->
					[nat5, nat6].each{ n -> commands << buildCommand(r, t, n) }
			}
		}
		return commands
	}
	
	def associate = { defs, convs -> 
		defs.size().times {factory.newInstance(defs[it].componentClass) >> convs[it] }	
	}
	
}

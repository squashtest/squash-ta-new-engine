/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.api.test
/** TODO: test on the SuiteResult/TestResult collaboration.
import spock.lang.Specification;

class TestResultBuilderImpl extends Specification {

	def "should create a TestResult with generic infos"(){
		
		given :
			def builder = new TestResultBuilder();
			builder.statistics = createBasicStatistics();
					
		when :
			def result = builder.createWithGeneralInformations()
		
		then :
			result.testName == "test"
			result.assertionCounter == 3
			result.commandCounter == 1
			result.usedResources*.name == ["res"]

	}
	
	def "should add failure details"(){
		given :
			def builder = new TestResultBuilder();
			def ex = createException();
			builder.statistics = createBasicStatistics();
			
		and :
			TestResultImpl result = new TestResultImpl(); 
			
		when :
			builder.addFailureDetails(result, ex);
			
		then :
			result.topException == ex
			result.failingInstructionText ==  "insert some data into some db"
			result.exceptionMessages == [ "operation failed" , "because of that" ]
			
		
	}
	
	
	def "should create a Success test result"(){
		
		given : 
			def builder = new TestResultBuilder();
			builder.statistics = createBasicStatistics();
			
		when :
			def result = builder.generateSuccessResult()
			
		then :
			result.testStatus == TestStatus.SUCCESS;
		
	}
	
	def "should create a SetupError test result"(){
		given :
			def builder = new TestResultBuilder();
			def ex = createSetupException();
			builder.statistics = createBasicStatistics();
		
		when :
			def result = builder.generateSetupErrorResult(ex)
			
		then :
			result.testStatus == TestStatus.SETUP_ERROR;
	}
	
	def "should create a AssertionError test result"(){
		given :
			def builder = new TestResultBuilder();
			def ex = createAssertionException();
			builder.statistics = createBasicStatistics();
		
		when :
			def result = builder.generateAssertionErrorResult(ex)
			
		then :
			result.testStatus == TestStatus.FAILED;
	}
	
	def "should create a UnexpectedError test result"(){
		given :
			def builder = new TestResultBuilder();
			def ex = createException();
			builder.statistics = createBasicStatistics();
		
		when :
			def result = builder.generateUnexpectedErrorResult(ex)
			
		then :
			result.testStatus == TestStatus.UNEXPECTED_ERROR;
	}
	
	
	def createBasicStatistics = {
		
		def statistics = Mock(TestStatisticsProvider)
		
		statistics.getTestName() >> "test"
		statistics.getCommandCounter() >> 1
		statistics.getAssertionCounter() >> 3
		
		def inst = Mock(TestInstruction)
		inst.toText() >> "insert some data into some db"
		
		statistics.getFailingInstruction() >> inst	
		
		def res1 = Mock(Resource)
		def resC1 = Mock(Resource)
		res1.copy() >> resC1
		resC1.getName() >> "res"
		
		statistics.getUsedResources() >> [res1]
		
		return statistics
		
	}
	
	def createException = {
		def ex = Mock(Exception)
		ex.getMessage() >> "operation failed"
		
		def sub = Mock(Exception)
		sub.getMessage() >> "because of that"
		
		ex.getCause() >> sub
		
		return ex;
	}
	
	def createSetupException = {
		def ex = Mock(TestSetupException)
		ex.getMessage() >> "operation failed"
		
		def sub = Mock(Exception)
		sub.getMessage() >> "because of that"
		
		ex.getCause() >> sub
		
		return ex;
	}
	
	def createAssertionException = {
		def ex = Mock(TestAssertionError)
		ex.getMessage() >> "operation failed"
		
		def sub = Mock(Exception)
		sub.getMessage() >> "because of that"
		
		ex.getCause() >> sub
		
		return ex;
	}
	
}
*/
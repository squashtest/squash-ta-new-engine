/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl
 
import org.squashtest.ta.backbone.definition.ConverterDefinition
import org.squashtest.ta.backbone.engine.impl.ResourceConverterManagerImpl.ConverterMap
import org.squashtest.ta.backbone.engine.wrapper.Nature
import org.squashtest.ta.backbone.exception.DuplicateFactoryException;
import org.squashtest.ta.backbone.exception.NotUniqueEntryException;
import org.squashtest.ta.backbone.tools.AggregativeChristmasTree
import org.squashtest.ta.backbone.tools.MultiDimensionMap
import spock.lang.Specification


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2011 Squashtest TA, Squashtest.org
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */


class SimpleManagerTest extends Specification {

	def nat1
	def nat2
	def nat3
	
	def manager
	
	def setup(){
		
		nat1 = new Nature("nat1")
		nat2 = new Nature("nat2")
		nat3 = new Nature("nat3")
		manager= new SimpleManager<ConverterDefinition>()
		
	}
	
	def "should add a definition"(){
		
		given :
			def definition = new ConverterDefinition(nat1, nat2, nat3, String.class)
		
		and :
			def elements = Mock(AggregativeChristmasTree)		
			manager.elements= elements
		when :
			manager.addDefinition(definition)
			
		then :
			1 * elements.put(definition, nat1, nat2, nat3)
		
	}
	
	def "should rant because  something is already there"(){
		
		given :
			def definition = new ConverterDefinition(nat1, nat2, nat3, String.class)
		
		and :
			def elements = Mock(AggregativeChristmasTree)		
			manager.elements= elements
			
		and : 
			elements.put(_,_,_,_) >> { throw new NotUniqueEntryException() }
			/* for consistencie's sake, if we claim there already is a mapping, let's return one if queried! */
			elements.get(_,_,_) >> {[definition]}
		
		when :
			manager.addDefinition(definition)
			
		then :
			thrown DuplicateFactoryException		
	}
	
	def "should remove a definition"(){
		
		
		given :
			def definition = new ConverterDefinition(nat1, nat2, nat3, String.class)
		
		and :
			def elements = Mock(AggregativeChristmasTree)		
			manager.elements= elements
			
		when :
			manager.removeDefinition(definition)
		
		then :
			1 * elements.remove(nat1, nat2, nat3)
		
	}
	
	def "should remove a definition (2)"(){
		given :
			def elements = Mock(AggregativeChristmasTree)		
			manager.elements= elements
		
		when :
			manager.removeDefinition(nat1, nat2, nat3)
		
		then :
			1 * elements.remove(nat1, nat2, nat3)		
	}
	
	def "should convert a 'null' arguments to 'Any'"(){
		given :
			def elements = Mock(AggregativeChristmasTree)
			manager.elements = elements;
		when :
			manager.getAllDefinitions(null, nat2, null)
		then :
			1 * elements.get(MultiDimensionMap.ANY, nat2, MultiDimensionMap.ANY)
	}
	
	
}

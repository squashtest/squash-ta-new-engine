/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings;
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultLoadResourceRunner
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.MalformedInstructionException
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.components.ResourceConverter

import spock.lang.Specification

class DefaultLoadResourceRunnerTest extends Specification {

	
	DefaultLoadResourceRunner runner
	InternalTestRunner testRunner
	LoadResourceInstruction instruction
	ContextManager manager
	
	def setup(){
		
		instruction = Mock(LoadResourceInstruction)
		runner = new DefaultLoadResourceRunner(instruction);
		
		testRunner = Mock(InternalTestRunner)
		
		runner.setTestRunner testRunner
		
		manager = Mock(ContextManager)
		
	}
	
	def "should rant because the resource name is not given"(){
		when :
			runner.checkSettings();
			
		then :
			thrown MalformedInstructionException
		
	}
	
	def "should pass the check"(){
		
		given :
			instruction.getResourceName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
			
		when :
			runner.checkSettings();
			
		then :
			true
		
	}
	
	def "should return the instruction"(){
		
		expect :
			runner.getInstruction() == instruction;
		
	}
	
	def "should load a resource"(){
		given :
			def res = Mock(ResourceWrapper)
			def name = new ResourceName (Scope.SCOPE_TEST,"resource")
			instruction.getResourceName() >> name
			instruction.toText() >> "Mock load test instruction"
			instruction.getType() >> InstructionType.LOAD
			instruction.getLine() >> "0"
		
		and :
			testRunner.getContextManager() >> manager
			manager.getResource(_) >> res
		when :
			runner.run()	
		
		then :
			1 * testRunner.addResourceToCache(res)
			
	}
	
	def "should load a resource from test runner cache"(){
		given :
			def res = Mock(ResourceWrapper)
			instruction.getResourceName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
			
			def copy = Mock(ResourceWrapper)
		
		and :
			testRunner.getResourceFromCache(new ResourceName (Scope.SCOPE_TEST,"resource")) >> res
			
		when :
			def result = runner.getResource();
			
		then :
			1 * res.copy() >> copy
			result == copy
			
	}
	
	def "should load a resource from context manager because not found in test context"(){
		
		given :
			
			def res = Mock(ResourceWrapper)			
			instruction.getResourceName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
			testRunner.getResourceFromCache(new ResourceName (Scope.SCOPE_TEST,"resource")) >> null
			manager.getResource(_) >> res
			testRunner.getContextManager() >> manager
	
		when :
			def result = runner.getResource();
			
		then :
			result == res
		
	}
	
	def "should load a resource from the context manager because the repository name was specified"(){
		
		given :
		
			def fromRunner = Mock(ResourceWrapper)
			def fromManager = Mock(ResourceWrapper)
			
			def copyFromRunner = Mock(ResourceWrapper)
			fromRunner.copy() >> copyFromRunner
		
		and :
			instruction.getResourceName() >> new ResourceName (Scope.SCOPE_TEST,"resource")
			instruction.getRepositoryName() >> "repo"
		
		and :
			testRunner.getResourceFromCache(new ResourceName (Scope.SCOPE_TEST,"resource")) >> fromRunner 
			manager.getResource( _) >> fromManager
			testRunner.getContextManager() >> manager

			
		when :
			def result = runner.getResource();
			
		then :
			result == fromManager
			result != copyFromRunner
			result != fromRunner
	}

	
}

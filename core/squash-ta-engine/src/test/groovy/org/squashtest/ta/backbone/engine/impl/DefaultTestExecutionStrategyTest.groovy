/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.backbone.exception.UnexpectedStatusException;
import org.squashtest.ta.framework.test.result.GeneralStatus

import spock.lang.Specification
import spock.lang.Unroll

class DefaultTestExecutionStrategyTest extends Specification{
	def testee
	def setup(){
		testee=new DefaultTestExecutionStrategy()
	}
	
	
	@Unroll("When instrcution execution status is #instructionStatus and isVerifyInstruction is #isVerifyInstruction the continueExecution is #continueExecution")
	def "Should continue execution"(){
	
		expect :
			testee.continueExecution(instructionStatus, isVerifyInstruction) == continueExecution
		
		where :
			instructionStatus		|	isVerifyInstruction			| 	continueExecution
		
			GeneralStatus.SUCCESS	|	true						|	true
			GeneralStatus.SUCCESS	|	false						|	true
			GeneralStatus.FAIL		|	true						|	true
			GeneralStatus.FAIL		|	false						|	false
			GeneralStatus.ERROR		|	true						|	true
			GeneralStatus.ERROR		|	false						|	false
		
	}
	
	@Unroll ("Should thrown an error when continueExecution receive a NOT_RUN instruction status and isVerifyInstruction is #isVerifyInstruction")
	def "Should thrown an error when continueExecution receive a NOT_RUN instruction status"(){
		when:
			testee.continueExecution(GeneralStatus.NOT_RUN, isVerifyInstruction)
		then:
			thrown(UnexpectedStatusException)
		where:
			isVerifyInstruction << [true,false]
	}
	
	@Unroll("When the previous test status is #testStatus and instruction execution status is #instructionStatus the new test status should be #newtestStatus")
	def "Aggregation result"(){
	
		expect :
			testee.aggregate(testStatus,instructionStatus) == newTestStatus
		
		where :
			testStatus				|	instructionStatus			| 	newTestStatus
		
			GeneralStatus.SUCCESS	|	GeneralStatus.SUCCESS		|	GeneralStatus.SUCCESS
			GeneralStatus.SUCCESS	|	GeneralStatus.FAIL			|	GeneralStatus.FAIL
			GeneralStatus.SUCCESS	|	GeneralStatus.ERROR			|	GeneralStatus.ERROR
			GeneralStatus.SUCCESS	|	GeneralStatus.NOT_RUN		|	GeneralStatus.SUCCESS
			
			GeneralStatus.FAIL		|	GeneralStatus.SUCCESS		|	GeneralStatus.FAIL
			GeneralStatus.FAIL		|	GeneralStatus.FAIL			|	GeneralStatus.FAIL
			GeneralStatus.FAIL		|	GeneralStatus.ERROR			|	GeneralStatus.ERROR
			GeneralStatus.FAIL		|	GeneralStatus.NOT_RUN		|	GeneralStatus.FAIL
			
			GeneralStatus.ERROR		|	GeneralStatus.SUCCESS		|	GeneralStatus.ERROR
			GeneralStatus.ERROR		|	GeneralStatus.FAIL			|	GeneralStatus.ERROR
			GeneralStatus.ERROR		|	GeneralStatus.ERROR			|	GeneralStatus.ERROR
			GeneralStatus.ERROR		|	GeneralStatus.NOT_RUN		|	GeneralStatus.ERROR
		
	}
	
	@Unroll ("Should thrown an error when aggregate method receive a NOT_RUN testStatus and isVerifyInstruction is #isVerifyInstruction")
	def "Should thrown an error when aggregate method receive a NOT_RUN testStatus"(){
		when:
			testee.aggregate(GeneralStatus.NOT_RUN, instructionStatus)
		then:
			thrown(UnexpectedStatusException)
		where:
			instructionStatus << [GeneralStatus.SUCCESS,GeneralStatus.FAIL,GeneralStatus.ERROR]
	}
	
	@Unroll("When status is #status and hasOnlyVerifyFailOrError is #hasOnlyVerifyFailOrError then begin should be #begin")
	def "Should begin test phase"(){
		expect:
			testee.beginGroupExecution(status, hasOnlyVerifyFailOrError) == begin
		
		where:
			status					|	hasOnlyVerifyFailOrError	|	begin
			
			GeneralStatus.SUCCESS	|	true						|	true
			GeneralStatus.SUCCESS	|	false						|	true
			GeneralStatus.FAIL		|	true						|	true
			GeneralStatus.FAIL		|	false						|	false
			GeneralStatus.ERROR		|	true						|	true
			GeneralStatus.ERROR		|	false						|	false

	}
	
	@Unroll ("Should thrown an error when beginGroupExecution method receive a NOT_RUN testStatus and hasOnlyVerifyFailOrError is #hasOnlyVerifyFailOrError")
	def "Should thrown an error when beginGroupExecution method receive a NOT_RUN testStatus"(){
		when:
			testee.beginGroupExecution(GeneralStatus.NOT_RUN, hasOnlyVerifyFailOrError)
		then:
			thrown(UnexpectedStatusException)
		where:
			hasOnlyVerifyFailOrError << [true,false]
	}
	
}

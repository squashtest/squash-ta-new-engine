/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import net.sf.cglib.reflect.FastClassEmitter.GetIndexCallback;

import org.codehaus.groovy.classgen.ReturnAdder;
import org.squashtest.ta.backbone.engine.InstructionFlowStrategy;
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner
import org.squashtest.ta.backbone.engine.instructionrunner.AbstractDefaultInstructionRunner
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.exception.MalformedInstructionException
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.instructions.TestInstruction
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.exception.BrokenTestException

import spock.lang.Specification

class AbstractDefaultInstructionRunnerTest extends Specification {

	AbstractDefaultInstructionRunner runner
	InternalTestRunner testRunner
	TestInstruction instruction 
	
	class ADIRImpl extends AbstractDefaultInstructionRunner{
		
		TestInstruction instruction;
		Throwable expectedThrowable;
		
		public ADIRImpl(TestInstruction ins){
			this.instruction = ins;
		}
		
		public ADIRImpl(TestInstruction ins, Throwable expected){
			this.instruction = ins;
			this.expectedThrowable = expected;
		}
		
		@Override
		protected void checkSettings() {
			
		}
		
		@Override
		protected void doRun() {
			if(expectedThrowable!=null){
				throw expectedThrowable;
			}else{
				throw new RuntimeException()
			}			
		}
		
		@Override
		protected TestInstruction getInstruction() {
			return instruction;
		}
		
		@Override
		protected String getInstructionGenericFailureMessage() {
			return "AbstractDefaultInstructionRunnerTestMock failed: ";
		}

		@Override
		protected void addInputToFailureReport(ExecutionDetails executionDetails) {
			//NOOP ... evrything fake, why bother with input report?
		}

//		@Override
//		public InstructionType getInstructionType() {
//			return InstructionType.LOAD;
//		}
	}
	
	
	def setup(){
		instruction = Mock(TestInstruction)
		instruction.toText() >> "Mock instruction"
		instruction.getType() >> InstructionType.LOAD
		instruction.getLine() >> "0"
		runner = new ADIRImpl(instruction)
		
		testRunner = Mock(InternalTestRunner)

		runner.setTestRunner testRunner
		
	}
	
	def "no exception should be thrown from instruction execution"(){
		when:
			def res=runner.run()
		then:
			true
	}
	
	def "java.lang.Errors & co should pass"(){
		given:
			def error=new OutOfMemoryError("Just for the test!")
			def errorRunner=new ADIRImpl(instruction,error)
		when:
			errorRunner.run()
		then:
			Error actualError=thrown()
			actualError == error
	}
	
	def "should throw a MalformedInstructionException"(){
		when :
			runner.logAndThrow("test")
			
		then :
			MalformedInstructionException e = thrown()
			e.message == "test"
		
	}
	
	
	
	def "should rant because a resource wasn't loaded"(){
		

		when :
			runner.fetchResourceOrFail(new ResourceName(Scope.SCOPE_TEST,"test"))
		
		then :	
			thrown ResourceNotFoundException
	}
	
	def "should fetch a resource from runner cache"(){
		
		given :
			def res = Mock(ResourceWrapper)
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"test")) >> res
			
		when :
			def result = runner.fetchResourceOrFail(new ResourceName(Scope.SCOPE_TEST,"test"))
			
		then :
			result == res
	
	}
	
	def "should yield OK when no error!"(){
		given:
			
			def yesRunner=new AbstractDefaultInstructionRunner(){
				TestInstruction instruct;
				@Override
				protected void checkSettings() {}
				@Override
				protected void doRun() {}
				@Override
				protected TestInstruction getInstruction( ) { return instruct; }
				@Override
				protected String getInstructionGenericFailureMessage() {}
				@Override
				protected void addInputToFailureReport(ExecutionDetails arg0) {}
				@Override 
				public InstructionType getInstructionType(){ return InstructionType.LOAD; }
				// Added in this dummy implementation in order to add the instruction. 
				// We can't return directly the instruction Mock because in this case the test throw an error 
				public setInstruction(TestInstruction inst){
					instruct = inst;
				} 
			};
			yesRunner.setInstruction(instruction)
		when:
			def report=yesRunner.run()
		then:
			report.getStatus()==GeneralStatus.SUCCESS
	}
	
	def "Should return an ExecutionDetail with status NOT_RUN when call getExecutionDetails"(){
		when:
			ExecutionDetails detail = runner.getInstructionsDetails()
		then:	
			detail.getStatus() == GeneralStatus.NOT_RUN
	}
}

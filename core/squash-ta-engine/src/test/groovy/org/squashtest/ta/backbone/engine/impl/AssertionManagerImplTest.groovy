/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import java.util.Collection;

import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.AssertionManager
import org.squashtest.ta.backbone.engine.impl.AssertionManagerImpl
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ObjectFactory;
import org.squashtest.ta.backbone.exception.DuplicateFactoryException
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.pseudoimplem.PseudoBinaryAssertion;
import org.squashtest.ta.backbone.pseudoimplem.PseudoResource;
import org.squashtest.ta.backbone.pseudoimplem.PseudoUnaryAssertion;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.facade.Engine;

import spock.lang.Specification

class AssertionManagerImplTest extends Specification {


	Nature nat1=new Nature("nat1")
	Nature nat2=new Nature("nat2")
	Nature nat3=new Nature("nat3")
	Nature nat4=new Nature("nat4")
	Nature nat5=new Nature("nat5")
	Nature nat6=new Nature("nat6")

	AssertionManager manager
	ObjectFactory factory

	def setup(){
		manager = new AssertionManagerImpl()
		factory = Mock(ObjectFactory)
		manager.factory=factory;
	}


	/* ************************* Binary assertion API ****************************** */


	def "should add an unary assertion definition"(){

		given :
		def assertionClass = PseudoUnaryAssertion.class
		def definition = new UnaryAssertionDefinition(nat1, nat3, assertionClass)

		def ass = assertionClass.newInstance()
		factory.newInstance(assertionClass) >> ass

		when :
		manager.addAssertionDefinition(definition)

		def res = manager.getAllUnaryAssertions(nat1, nat3)

		then :
		res.size() == 1
		def handler = res.get(0)
		handler.actualNature == nat1
		handler.assertionCategory == nat3
	}

	def "should rant when trying to add twice the same unary definition"(){
		given :
		def definition = new UnaryAssertionDefinition(nat1, nat3, PseudoUnaryAssertion.class)
		when :
		manager.addAssertionDefinition(definition)
		manager.addAssertionDefinition(definition)

		then :
		thrown DuplicateFactoryException
	}

	def "should remove an unary Assertion definition (1)"(){
		given :
		def definition = new UnaryAssertionDefinition(nat1,nat3, PseudoUnaryAssertion.class)

		when :
		manager.addAssertionDefinition(definition)
		manager.removeAssertionDefinition(definition)
		def res = manager.getAllUnaryAssertions(nat1, nat3)

		then :
		res.size()==0
	}

	def "should remove an unary Assertion definition (2)"(){
		given :
		def definition = new UnaryAssertionDefinition(nat1,  nat3, PseudoUnaryAssertion.class)

		when :
		manager.addAssertionDefinition(definition)
		manager.removeAssertionDefinition(nat1,nat3)
		def res = manager.getAllUnaryAssertions(nat1, nat3)

		then :
		res.size()==0
	}


	def "should get a subset of unary Assertions (1)"(){

		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllUnaryAssertions(null, nat3)

		then :
		res.size() == 2
		res.collect{it.wrappedAssertion}.containsAll([
			assertions[0],
			assertions[2]
		])
	}

	def "should get a subset of unary Assertions (2)"(){

		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllUnaryAssertions(nat2,null )

		then :
		res.size()==2
		res.collect{it.wrappedAssertion}.containsAll([
			assertions[2],
			assertions[3]
		])
	}


	def "should get one unary Assertion"(){
		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllUnaryAssertions(nat2, nat4)

		then :
		res.size()==1
		res.collect{it.wrappedAssertion}.containsAll([assertions[3]])
	}

	def "should get all unary Assertions"(){
		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllUnaryAssertions(null, null)

		then :
		res.size()==4
		res.collect{it.wrappedAssertion}.containsAll(assertions)
	}

	def "should not find an unary Assertion"(){
		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllUnaryAssertions(nat3, nat2)

		then :
		res.size()==0
	}

	def "should get all unary Assertions by name arguments"(){

		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllUnaryAssertionsByName(null, null)

		then :
		res.size()==4
		res.collect{it.wrappedAssertion}.containsAll(assertions)
	}


	def "should rant because the resource type wasn't found by name"(){

		when :
		manager.getAllUnaryAssertionsByName("test", null)
		then :
		thrown ResourceNotFoundException
	}

	def "should rant because the target type wasn't found by name"(){

		given :
		def factories = buildUnaryDefinitionSet()
		def assertions = buildUnaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		manager.getAllUnaryAssertionsByName("nat1", "test")

		then :
		thrown ResourceNotFoundException
	}


	/* ************************* Binary assertion API ****************************** */


	def "should add a binary assertion definition"(){

		given :
		def assertionClass = PseudoBinaryAssertion.class
		and:
		def definition = new BinaryAssertionDefinition(nat1, nat2, nat3, assertionClass)
		and:
		def ass = assertionClass.newInstance()
		factory.newInstance(assertionClass) >> ass

		when :
		manager.addAssertionDefinition(definition)

		def res = manager.getAllBinaryAssertions(nat1, nat2, nat3)

		then :
		res.size() == 1
		def handler = res.get(0)
		def cmd = handler.wrappedAssertion
		handler.actualNature == nat1
		handler.expectedNature == nat2
		handler.assertionCategory == nat3
	}

	def "should rant when trying to add twice the same definition"(){
		given :
		def definition = new BinaryAssertionDefinition(nat1, nat2, nat3, BinaryAssertion.class)
		when :
		manager.addAssertionDefinition(definition)
		manager.addAssertionDefinition(definition)

		then :
		thrown DuplicateFactoryException
	}

	def "should remove a Assertion factory (1)"(){
		given :
		def definition = new BinaryAssertionDefinition(nat1, nat2, nat3, BinaryAssertion.class)

		when :
		manager.addAssertionDefinition(definition)
		manager.removeAssertionDefinition(definition)
		def res = manager.getAllBinaryAssertions(nat1, nat2, nat3)

		then :
		res.size()==0
	}

	def "should remove a Assertion factory (2)"(){
		given :
		def definition = new BinaryAssertionDefinition(nat1, nat2, nat3, BinaryAssertion.class)

		when :
		manager.addAssertionDefinition(definition)
		manager.removeAssertionDefinition(nat1, nat2, nat3)
		def res = manager.getAllBinaryAssertions(nat1, nat2, nat3)

		then :
		res.size()==0
	}


	def "should get a subset of Assertions (1)"(){

		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllBinaryAssertions(null, nat3, null)

		then :
		res.size() == 4
		res.collect{it.wrappedAssertion}.containsAll([
			assertions[0],
			assertions[1],
			assertions[4],
			assertions[5]
		])
	}

	def "should get a subset of Assertions (2)"(){

		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllBinaryAssertions(null, nat3, nat5)

		then :
		res.size()==2
		res.collect{it.wrappedAssertion}.containsAll([
			assertions[0],
			assertions[4]
		])
	}




	def "should get one binary Assertion"(){
		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllBinaryAssertions(nat1, nat4, nat6)

		then :
		res.size()==1
		res.collect{it.wrappedAssertion}.containsAll([assertions[3]])
	}

	def "should get all binary Assertions"(){
		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllBinaryAssertions(null, null, null)

		then :
		res.size()==8
		res.collect{it.wrappedAssertion}.containsAll(assertions)
	}

	def "should not find a binary Assertion"(){
		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllBinaryAssertions(nat3, nat2, nat5)

		then :
		res.size()==0
	}

	def "should get all Assertions by name arguments"(){

		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		def res = manager.getAllBinaryAssertionsByName(null, null, null)

		then :
		res.size()==8
		res.collect{it.wrappedAssertion}.containsAll(assertions)
	}


	def "should rant because the resource nature wasn't found by name"(){

		when :
		manager.getAllBinaryAssertionsByName("test", null, null)
		then :
		thrown ResourceNotFoundException
	}

	def "should rant because the target nature wasn't found by name"(){

		given :
		def factories = buildBinaryDefinitionSet()
		def assertions = buildBinaryAssertionSet()
		associate(factories, assertions)

		and :
		factories.each{manager.addAssertionDefinition(it)}

		when :
		manager.getAllBinaryAssertionsByName("nat1", "test", null)

		then :
		thrown ResourceNotFoundException
	}

	/* *************************** scaffolding ****************************** */



	def buildBinaryDefinition = { a, b, c, t->
		return new BinaryAssertionDefinition(a,b,c,t)
	}

	def buildUnaryDefinition = { a,  c, t->
		return new UnaryAssertionDefinition(a,c,t)
	}

	def buildBinaryAssertion = { a, b , c ->
		def assertion = new b1()
		return assertion
	}

	def buildUnaryAssertion = { a, b  ->
		def assertion = new u1()
		return assertion
	}

	def buildBinaryDefinitionSet = {
		def definitions = []

		def counter=0

		//the following items just need to be different types of Class.
		def testClasses = [
			b1.class,
			b2.class,
			b3.class,
			b4.class,
			b5.class,
			b6.class,
			b7.class,
			b8.class
		]

		[nat1, nat2].each{a ->
			[nat3, nat4].each{e ->
				[nat5, nat6].each{ c -> definitions << buildBinaryDefinition(a, e, c, testClasses[counter++]) }
			}
		}
		return definitions
	}

	@TABinaryAssertion("1")
	class b1 implements BinaryAssertion<FileResource, PseudoResource>{

		@Override
		public void setActualResult(FileResource actual) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void setExpectedResult(PseudoResource expected) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void addConfiguration(Collection<Resource<?>> configuration) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void test() throws BinaryAssertionFailedException {
			throw new UnsupportedOperationException("TODO");
		}
		
	}
	
	@TABinaryAssertion("2")
	abstract class b2 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	@TABinaryAssertion("3")
	abstract class b3 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	@TABinaryAssertion("4")
	abstract class b4 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	@TABinaryAssertion("5")
	abstract class b5 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	@TABinaryAssertion("6")
	abstract class b6 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	@TABinaryAssertion("7")
	abstract class b7 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	@TABinaryAssertion("8")
	abstract class b8 implements BinaryAssertion<FileResource, PseudoResource>{
		
	}
	
	def buildUnaryDefinitionSet = {
		def definitions = []

		def counter=0

		def testClasses = [
			u1.class,
			u2.class,
			u3.class,
			u4.class
		]

		[nat1, nat2].each{a ->
			[nat3, nat4].each{ c ->
				definitions << buildUnaryDefinition(a, c, testClasses[counter++])
			}
		}
		return definitions
	}
	@TAUnaryAssertion("1")
	 class u1 implements UnaryAssertion<FileResource>{

		@Override
		public void setActualResult(FileResource actual) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void addConfiguration(Collection<Resource<?>> configuration) {
			throw new UnsupportedOperationException("TODO");
		}

		@Override
		public void test() throws AssertionFailedException {
			throw new UnsupportedOperationException("TODO");
		}

	}
	@TAUnaryAssertion("2")
	abstract class u2 implements UnaryAssertion<FileResource>{

	}
	@TAUnaryAssertion("3")
	abstract class u3 implements UnaryAssertion<FileResource>{

	}
	@TAUnaryAssertion("4")
	abstract class u4 implements UnaryAssertion<FileResource>{

	}
	def buildBinaryAssertionSet = {
		def assertions = []

		[nat1, nat2].each{r ->
			[nat3, nat4].each{t ->
				[nat5, nat6].each{ c ->
					assertions << buildBinaryAssertion(r, t, c) }
			}
		}
		return assertions
	}

	def buildUnaryAssertionSet = {
		def assertions = []

		[nat1, nat2].each{r ->
			[nat3, nat4].each{c ->
				assertions << buildUnaryAssertion(r,c)
			}
		}
		return assertions
	}


	def associate = { defs, convs ->
		defs.size().times { factory.newInstance(defs[it].componentClass) >> convs[it] }
	}


}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl



import org.squashtest.ta.backbone.engine.ResourceLoader
import org.squashtest.ta.backbone.engine.ResourceLoadingSettings
import org.squashtest.ta.backbone.engine.impl.ResourceLoaderImpl
import org.squashtest.ta.backbone.engine.wrapper.RepositoryWrapper
import org.squashtest.ta.backbone.exception.ElementNotFoundException
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper

import spock.lang.Specification


class ResourceLoaderImplTest extends Specification {

	ResourceLoader loader;
	
	RepositoryWrapper repo1;
	RepositoryWrapper repo2;
	RepositoryWrapper repo3;
	
	
	def setup(){		
		loader = new ResourceLoaderImpl();
		
		repo1 = Mock(RepositoryWrapper)
		repo2 = Mock(RepositoryWrapper)
		repo3 = Mock(RepositoryWrapper)
		
		repo1.getName() >> "repo1"
		repo2.getName() >> "repo2"
		repo3.getName() >> "repo3"
		
		loader.addRepository repo1
		loader.addRepository repo2
		loader.addRepository repo3
				
		
	}
	
	def specialSetup(){
		loader.repositories = new LinkedHashMap()
		loader.addRepository repo1
		loader.addRepository repo2
		loader.addRepository repo3
	}
	

	def "should fail because no name was given"(){
		
		given :
			def settings = Mock(ResourceLoadingSettings);
			settings.getResourceName() >> null	
			
		when :
			loader.loadResource settings
		
		then :
			thrown ElementNotFoundException		
	}
	
	def "should search through all repositories"(){
		
		given :
		//special setup : we need to enforce the 
		// order in which repositories are iterated on
		specialSetup()
		
		and :
			def settings = Mock(ResourceLoadingSettings)
			def name = new ResourceName (Scope.SCOPE_TEST,"test")
			settings.getResourceName() >> name
		
		and : 
			def file = Mock(File)
			file.exists() >> true
			file.isDirectory() >> false

		and :
			def resource = Mock(ResourceWrapper)
			def copyResource = Mock(ResourceWrapper)
			resource.copy() >> copyResource
			
		when :
			def res = loader.loadResource(settings)
		
		then :
			res == copyResource
			
			1 * repo1.findResources(name) >> null
			1 * repo2.findResources(name) >> resource
			0 * repo3.findResources(name) >> null
	}
	
	def "should search the specified repository"(){
		
		given :
			def settings = Mock(ResourceLoadingSettings)
			def name = new ResourceName (Scope.SCOPE_TEST,"test")
			def repoName = "repo1"
			settings.getResourceName() >> name
			settings.getRepositoryName() >> repoName
			
		and :
			def file = Mock(File)
			file.exists() >> true
			file.isDirectory () >> false
			
		and :
			def resource = Mock(ResourceWrapper)
			def copyResource = Mock(ResourceWrapper)
			resource.copy() >> copyResource
		
		when :
			def res = loader.loadResource(settings)
		
		then :
			res == copyResource
			
			1 * repo1.findResources(name) >> resource
			0 * repo2.findResources(name) >> null
			0 * repo3.findResources(name) >> null

	}
	
	def "should rant when no file was found"(){
		
		given :
			def settings = Mock(ResourceLoadingSettings)
			def name = new ResourceName (Scope.SCOPE_TEST,"test")
			settings.getResourceName() >> name
			
		when :
			loader.loadResource settings
			
		then :
			thrown ResourceNotFoundException
	}
	
	def "should not go haywire if a repository thrown an exception when performing a broadcast query"(){
		
		given :
			//special setup : we need to enforce the
			// order in which repositories are iterated on
			specialSetup()
		
		and :
			def settings = Mock(ResourceLoadingSettings)
			def name = new ResourceName (Scope.SCOPE_TEST,"test")
			settings.getResourceName() >> name
		
			
		and :
			def resource = Mock(ResourceWrapper)
			def copyResource = Mock(ResourceWrapper)
			resource.copy() >> copyResource
			
		when :
			def res = loader.loadResource(settings)
			
		then :
			
			1 * repo1.findResources(name) >> { throw new IOException()}
			1 * repo2.findResources(name) >> { throw new ResourceNotFoundException() }
			1 * repo3.findResources(name) >> resource
			
			res == copyResource
	}
	
	def "should build a nice message if no resource was found yet errors were thrown"(){
		given :
			def errors = [ "repo1" : new IOException(),
							"repo2" : new ResourceNotFoundException()
						]
			def resName = new ResourceName (Scope.SCOPE_TEST,"test")
			
		when :
			def res = loader.buildErrorMessage(errors, resName)
			
		then :
			res.contains("IOException")
			res.contains("ResourceNotFoundException")
			
	}


	
	
}

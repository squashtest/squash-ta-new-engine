/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.instructionrunner.DefaultConvertResourceRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultExecuteCommandRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultInstructionRunnerFactory
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultLoadResourceRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultResetLocalContextRunner
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction
import org.squashtest.ta.framework.test.instructions.LoadResourceInstruction
import org.squashtest.ta.framework.test.instructions.ResetLocalContextInstruction

import spock.lang.Specification

class DefaultInstructionRunnerFactoryTest extends Specification {

	DefaultInstructionRunnerFactory factory = new DefaultInstructionRunnerFactory();
	
	//TODO: weak coverage rate!
	def "should return a DefaultLoadResourceRunner"(){
		
		given :
			def instruction = Mock(LoadResourceInstruction)
			instruction.visit(_) >> {
				def factory = it[0] 
				factory.accept(instruction)
			}
			
		when :
			def res = factory.getRunner(instruction)
			
		then :
			res instanceof DefaultLoadResourceRunner
	}
	
	
	
	def "should return a DefaultConvertResourceRunner"(){
		
		given :
			def instruction = new ConvertResourceInstruction()
			instruction.visit(_) >> {
				def factory = it[0]
				factory.accept(instruction)
			}
			
		when :
			def res = factory.getRunner(instruction)
			
		then :
			res instanceof DefaultConvertResourceRunner
	}
	
	def "should return a DefaultExecuteCommandRunner"(){
		
		given :
			def instruction = Mock(ExecuteCommandInstruction)
			instruction.visit(_) >> {
				def factory = it[0]
				factory.accept(instruction)
			}
			
		when :
			def res = factory.getRunner(instruction)
			
		then :
			res instanceof DefaultExecuteCommandRunner
	}
	
	def "should return a DefaultResetLocalContextRunner"(){
		
		given:
			def instruction = Mock(ResetLocalContextInstruction)
			instruction.visit(_) >> {
				def factory = it[0]
				factory.accept(instruction)
			}
			
		when:
			def res = factory.getRunner(instruction)
			
		then:
			res instanceof DefaultResetLocalContextRunner
	}		
}

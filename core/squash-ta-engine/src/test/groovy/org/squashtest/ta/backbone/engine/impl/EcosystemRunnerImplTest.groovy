/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import org.squashtest.ta.backbone.engine.ContextManager;
import org.squashtest.ta.backbone.engine.TestRunner;
import org.squashtest.ta.backbone.engine.impl.EcosystemRunnerImpl.TestRunnerFactory;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Environment;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.TestResult;

import spock.lang.Specification
import spock.lang.Unroll;

class EcosystemRunnerImplTest extends Specification {
	def testee
	
	def testRunnerFactory
	
	def setupRunner
	def setupResult
	ContextManager contextManager
	
	def tearDownRunner
	def tearDownResult
	
	def testRunner1
	def testRunner2
	def testRunner3
	def result1
	def result2
	def result3
	
	def setup(){
		testee=new EcosystemRunnerImpl();
		def ecosystem = Mock(Ecosystem)
		testee.setEcosystem(ecosystem)
		
		contextManager=Mock()
		testee.contextManager = contextManager
		
		def environment = Mock(Environment)
		def setup=Mock(Test)
		def tearDown=Mock(Test)
		environment.getSetUp() >> setup
		environment.getTearDown() >> tearDown
		ecosystem.getEnvironment() >> environment
		
		testRunnerFactory=Mock(TestRunnerFactory)
		testee.runnerFactory=testRunnerFactory
		
		setupRunner = Mock(TestRunner)
		testRunnerFactory.configureTestRunner(_,setup,_) >> setupRunner
		setupResult = Mock(TestResult)
		
		tearDownRunner = Mock(TestRunner)
		testRunnerFactory.configureTestRunner(_,tearDown,_) >> tearDownRunner
		tearDownResult = Mock(TestResult)
		
		def test1=Mock(Test)
		def test2=Mock(Test)
		def test3=Mock(Test)
		
		def tests=[test1,test2,test3]
		ecosystem.getTestPopulation() >> tests
		
		testRunner1 = Mock(TestRunner)
		result1 = Mock(TestResult)
		testRunnerFactory.configureTestRunner(_,test1,_) >> testRunner1
		
		testRunner2 = Mock(TestRunner)
		result2 = Mock(TestResult)
		testRunnerFactory.configureTestRunner(_,test2,_) >> testRunner2
		
		testRunner3 = Mock(TestRunner)
		result3 = Mock(TestResult)
		testRunnerFactory.configureTestRunner(_,test3,_) >> testRunner3
		
		contextManager.getEcosystemResources() >> new HashMap()
                
                //FIXME @brokenDesign : cette classe porte un beoin caché d'initialisation statique
                TempFileUtils.init(System.getProperty("java.io.tmpdir")+"/tst","true")
	}
	
	def "Should execute setup, tests and teardown and end with SUCCESS status"(){
		given:
			setupResult.getStatus() >> setupStatus
		and:
			result1.getStatus()>>testStatus
			result2.getStatus()>>testStatus
			result3.getStatus()>>testStatus
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.SUCCESS	]
			testStatus 		<<	[GeneralStatus.SUCCESS	]
			teardownStatus	<<	[GeneralStatus.SUCCESS	]
			finalStatus     <<	[GeneralStatus.SUCCESS	]
			setupRunExec	<<	[1						]
			testRun1Exec	<<	[1						]
			testRun2Exec	<<	[1						]
			testRun3Exec	<<	[1						]
			teardownRunExec	<<	[1						]
	}
	
	@Unroll("When setup has problem and setupHasOnly is false then final status should be #finalStatus, tests are NOT_RUN and teardown is SUCCESS")
	def "Setup has problem, setupHasOnly is false, teardown is SUCCESS then verify tests are NOT_RUN and finalStatus"(){
		given:
			setupResult.getStatus() >> setupStatus
			setupResult.hasOnlyFailOrErrorWithContinue() >> setupHasOnly
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			for (TestResult testResult : result.getSubpartResults()) {
				testResult.getStatus()==testStatus
			}
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			setupHasOnly	<<	[false					,false					]
			// In this case whole tests should not be run => There result status should be NOT_RUN
			testStatus		<<	[GeneralStatus.NOT_RUN	,GeneralStatus.NOT_RUN	]
			teardownStatus	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			finalStatus     <<	[GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			setupRunExec	<<	[1						,1						]
			testRun1Exec	<<	[0						,0						]
			testRun2Exec	<<	[0						,0						]
			testRun3Exec	<<	[0						,0						]
			teardownRunExec	<<	[1						,1						]
	}

	@Unroll("When setup has problem and setupHasOnly is true then verify final status is #finalStatus, tests are SUCCESS and teardown is SUCCESS")
	def "When setup has problem and setupHasOnly is true then verify tests and teardown are SUCCESS"(){
		given:
			setupResult.getStatus() >> setupStatus
			setupResult.hasOnlyFailOrErrorWithContinue() >> setupHasOnly
		and:
			result1.getStatus()>>testStatus
			result2.getStatus()>>testStatus
			result3.getStatus()>>testStatus
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			setupHasOnly	<<	[true					,true					]
			testStatus		<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			teardownStatus	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			finalStatus     <<	[GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			setupRunExec	<<	[1						,1						]
			testRun1Exec	<<	[1						,1						]
			testRun2Exec	<<	[1						,1						]
			testRun3Exec	<<	[1						,1						]
			teardownRunExec	<<	[1						,1						]
	}
	
	
	@Unroll("When setup and teardown is SUCCESS and test1 is #result1Status, test2 is #result2Status, test3 is #result3Status then final status should be #finalStatus")
	def "When setup and teardon success, mix test1, test2 and test3 status then verify final status"(){
		given:
			setupResult.getStatus() >> setupStatus
		and:
			result1.getStatus()>>result1Status
			result2.getStatus()>>result2Status
			result3.getStatus()>>result3Status
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			result1Status 	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.FAIL		,GeneralStatus.SUCCESS	]
			result2Status	<<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.SUCCESS	,GeneralStatus.ERROR	]
			result3Status	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			teardownStatus	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			finalStatus     <<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.ERROR	]
			setupRunExec	<<	[1						,1						,1						,1						]
			testRun1Exec	<<	[1						,1						,1						,1						]
			testRun2Exec	<<	[1						,1						,1						,1						]
			testRun3Exec	<<	[1						,1						,1						,1						]
			teardownRunExec	<<	[1						,1						,1						,1						]
		}

	
	@Unroll("When setup is #setupStatus, setupHasOnly is true and teardown is SUCCESS and test1 is #result1Status, test2 is #result2Status, test3 is #result3Status then final status should be #finalStatus")
	def "When setup has problem and setupHasOnly is true, teardon success, mix test1, test2 and test3 status then verify final status"(){
		given:
			setupResult.getStatus() >> setupStatus
			setupResult.hasOnlyFailOrErrorWithContinue() >> setupHasOnly
		and:
			result1.getStatus()>>result1Status
			result2.getStatus()>>result2Status
			result3.getStatus()>>result3Status
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.FAIL		,GeneralStatus.FAIL		,GeneralStatus.ERROR	]
			setupHasOnly	<<	[true					,true					,true					]
			result1Status 	<<	[GeneralStatus.SUCCESS	,GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			result2Status	<<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.SUCCESS	]
			result3Status	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.ERROR	]
			teardownStatus	<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			finalStatus     <<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.ERROR	]
			setupRunExec	<<	[1						,1						,1						]
			testRun1Exec	<<	[1						,1						,1						]
			testRun2Exec	<<	[1						,1						,1						]
			testRun3Exec	<<	[1						,1						,1						]
			teardownRunExec	<<	[1						,1						,1						]
		}
	
	@Unroll("When setup and test are SUCCESS and teardown is #teardownStatus then final status should be #finalStatus")
	def "Teardown problem when setup and tests are SUCCESS"(){
		given:
			setupResult.getStatus() >> setupStatus
		and:
			result1.getStatus()>>testStatus
			result2.getStatus()>>testStatus
			result3.getStatus()>>testStatus
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			testStatus		<<	[GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			teardownStatus	<<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	]
			finalStatus     <<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	]
			setupRunExec	<<	[1						,1						]
			testRun1Exec	<<	[1						,1						]
			testRun2Exec	<<	[1						,1						]
			testRun3Exec	<<	[1						,1						]
			teardownRunExec	<<	[1						,1						]
	}
	
	@Unroll("When setup status is #setupStatus and setupHasOnly is false, teardown status is #teardownStatus then final status should be #finalStatus and tests are NOT_RUN")
	def "Setup has problem and setupHasOnly is false, teardown has problem, mix test1, test2 and test3 status then verify final status"(){
		given:
			setupResult.getStatus() >> setupStatus
			setupResult.hasOnlyFailOrErrorWithContinue() >> setupHasOnly
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			for (TestResult testResult : result.getSubpartResults()) {
				testResult.getStatus()==testStatus
			}
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.FAIL		,GeneralStatus.FAIL		]
			setupHasOnly	<<	[false					,false					,false					,false					]
			// In this case whole tests should not be run => There result status should be NOT_RUN
			testStatus		<<	[GeneralStatus.NOT_RUN	,GeneralStatus.NOT_RUN	,GeneralStatus.NOT_RUN	,GeneralStatus.NOT_RUN	]
			teardownStatus	<<	[GeneralStatus.ERROR	,GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			finalStatus     <<	[GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			setupRunExec	<<	[1						,1						,1						,1						]
			testRun1Exec	<<	[0						,0						,0						,0						]
			testRun2Exec	<<	[0						,0						,0						,0						]
			testRun3Exec	<<	[0						,0						,0						,0						]
			teardownRunExec	<<	[1						,1						,1						,1						]
	}
	
	
	@Unroll("When setup is #setupStatus, setupHasOnly is true and teardown is #teardownStatus and test1 is #result1Status, test2 is #result2Status, test3 is #result3Status then final status should be #finalStatus")
	def "When setup has problem and setupHasOnly is true, teardon has problem, mix test1, test2 and test3 status then verify final status"(){
		given:
			setupResult.getStatus() >> setupStatus
			setupResult.hasOnlyFailOrErrorWithContinue() >> setupHasOnly
		and:
			result1.getStatus()>>result1Status
			result2.getStatus()>>result2Status
			result3.getStatus()>>result3Status
		and:
			tearDownResult.getStatus()>>teardownStatus
		when:
			def result = testee.run()
		then:
			result.getStatus()==finalStatus
			setupRunExec * setupRunner.runTest() >> setupResult
			
			testRun1Exec * testRunner1.runTest() >> result1
			testRun2Exec * testRunner2.runTest() >> result2
			testRun3Exec * testRunner3.runTest() >> result3
			
			teardownRunExec * tearDownRunner.runTest() >> tearDownResult
			
		where:
			setupStatus		<<	[GeneralStatus.FAIL		,GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.FAIL		,GeneralStatus.FAIL		,GeneralStatus.ERROR	]
			setupHasOnly	<<	[true					,true					,true					,true					,true					,true					]
			result1Status 	<<	[GeneralStatus.SUCCESS	,GeneralStatus.ERROR	,GeneralStatus.FAIL		,GeneralStatus.SUCCESS	,GeneralStatus.ERROR	,GeneralStatus.FAIL		]
			result2Status	<<	[GeneralStatus.FAIL		,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	,GeneralStatus.FAIL		,GeneralStatus.SUCCESS	,GeneralStatus.SUCCESS	]
			result3Status	<<	[GeneralStatus.SUCCESS	,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.SUCCESS	,GeneralStatus.ERROR	,GeneralStatus.ERROR	]
			teardownStatus	<<	[GeneralStatus.FAIL		,GeneralStatus.FAIL		,GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.ERROR	]
			finalStatus     <<	[GeneralStatus.FAIL		,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.ERROR	,GeneralStatus.ERROR	]
			setupRunExec	<<	[1						,1						,1						,1						,1						,1						]
			testRun1Exec	<<	[1						,1						,1						,1						,1						,1						]
			testRun2Exec	<<	[1						,1						,1						,1						,1						,1						]
			testRun3Exec	<<	[1						,1						,1						,1						,1						,1						]
			teardownRunExec	<<	[1						,1						,1						,1						,1						,1						]
		}
	
	def "should have setup, test and teardown status after execution"(){
		given:
			setupResult.getStatus() >> GeneralStatus.SUCCESS
		and:
			result1.getStatus()>>GeneralStatus.SUCCESS
			result2.getStatus()>>GeneralStatus.SUCCESS
			result3.getStatus()>>GeneralStatus.SUCCESS
		and:
			tearDownResult.getStatus()>>GeneralStatus.SUCCESS
			setupRunner.runTest() >> setupResult
			testRunner1.runTest() >> result1
			testRunner2.runTest() >> result2
			testRunner3.runTest() >> result3
			tearDownRunner.runTest() >> tearDownResult
		when:
			def result = testee.run()
		then:
			result.getSubpartResults()!=null
			result.getSubpartResults().size()==3
			
			result.getSetupResult()==setupResult
			result.getTearDownResult()==tearDownResult
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.impl

import static org.squashtest.ta.framework.test.result.GeneralStatus.*

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.EcosystemPhase
import org.squashtest.ta.backbone.engine.wrapper.LocalContext
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper
import org.squashtest.ta.backbone.exception.NotUniqueEntryException
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.exception.BrokenTestException
import org.squashtest.ta.framework.test.definition.Test
import org.squashtest.ta.framework.test.definition.Ecosystem.EcosysPhase
import org.squashtest.ta.framework.test.instructions.AbstractTestInstruction
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction
import org.squashtest.ta.framework.test.instructions.InlineMetaInstruction
import org.squashtest.ta.framework.test.instructions.MacroInstruction
import org.squashtest.ta.framework.test.instructions.ResourceName
import org.squashtest.ta.framework.test.instructions.TestInstruction
import org.squashtest.ta.framework.test.instructions.TestInstructionVisitor
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope
import org.squashtest.ta.framework.test.result.ExecutionDetails
import org.squashtest.ta.framework.test.result.GeneralStatus
import org.squashtest.ta.framework.test.result.InstructionType
import org.squashtest.ta.framework.test.result.PhaseResult
import org.squashtest.ta.framework.test.result.TestResult

import spock.lang.Specification
import spock.lang.Unroll

class TestRunnerImplTest extends Specification {

	TestRunnerImpl runner;
	ContextManager manager;
	Test test;
	InstructionRunnerFactory runnerFactory;
	
        Iterator metadataIter;
	Iterator setupIter;
	Iterator testIter;
	Iterator teardownIter;
	
	TestInstruction instLoad1
	DummyAssertionInstruction instAssert1
	DummyAssertionInstruction instAssert2
	DummyAssertionInstruction instAssert3
	DummyAssertionInstruction instAssert4
	TestInstruction instCmd1
	
	InstructionRunner runLoad1
	InstructionRunner runAssert1
	InstructionRunner runAssert2
	InstructionRunner runAssert3
	InstructionRunner runAssert4
	InstructionRunner runCmd1
	
	ExecutionDetails exeLoad1
	ExecutionDetails exeAssert1
	ExecutionDetails exeAssert2
	ExecutionDetails exeAssert3
	ExecutionDetails exeAssert4
	ExecutionDetails exeCmd1
	
	TestResult result
	
	Exception caughtException
	
	EcosystemPhase ecosystemPhase
	
	def setup(){
		/* runner setup */
		runner = new TestRunnerImpl();
		manager = Mock(ContextManager);
		runnerFactory = Mock(InstructionRunnerFactory)
		test = Mock(Test)
		ecosystemPhase = Mock(EcosystemPhase)
		
		runner.setContextManager(manager)
		runner.setInstructionRunnerFactory(runnerFactory)
		runner.setTest(test)
		runner.setEcosystemPhase(ecosystemPhase)
		
		caughtException = Mock(Exception)
		caughtException.getMessage() >> "Error message"
		
		/* other mock setups */
                metadataIter = Mock(Iterator)
		setupIter = Mock(Iterator)
		testIter = Mock(Iterator)
		teardownIter = Mock(Iterator)	
		
		
		instLoad1 = Mock(TestInstruction)
		runLoad1 = Mock(InstructionRunner)
		exeLoad1 = Mock(ExecutionDetails)
		initLoad(instLoad1, exeLoad1)
		
		instAssert1 = Mock(DummyAssertionInstruction)
		runAssert1 = Mock(InstructionRunner)
		exeAssert1 = Mock(ExecutionDetails)
		initAssert(instAssert1, exeAssert1)
		
		instAssert2 = Mock(DummyAssertionInstruction)
		runAssert2 = Mock(InstructionRunner)
		exeAssert2 = Mock(ExecutionDetails)
		initAssert(instAssert2, exeAssert2)
			
		instAssert3 = Mock(DummyAssertionInstruction)
		runAssert3 = Mock(InstructionRunner)
		exeAssert3 = Mock(ExecutionDetails)
		initAssert(instAssert3, exeAssert3)
		
		instAssert4 = Mock(DummyAssertionInstruction)
		runAssert4 = Mock(InstructionRunner)
		exeAssert4 = Mock(ExecutionDetails)
		initAssert(instAssert4, exeAssert4)
		
		instCmd1 = Mock(TestInstruction)
		runCmd1 = Mock(InstructionRunner)
		exeCmd1 = Mock(ExecutionDetails)
		initCmd(instCmd1, exeCmd1)
		
		
        test.getMetadata() >> metadataIter
		test.getSetup() >> setupIter
		test.getTests() >> testIter
		test.getTeardown() >> teardownIter
		
                //FIXME @brokenDesign : cette classe porte un beoin caché d'initialisation statique
                TempFileUtils.init(System.getProperty("java.io.tmpdir")+"/tst","true")
	}
	
	def initAssert(TestInstruction instruction, ExecutionDetails detail){
		instruction.getType() >> InstructionType.ASSERTION
		detail.instructionType() >> InstructionType.ASSERTION
		// Useful when the instruction execution not succeed
		detail.caughtException() >> caughtException
	}
	
	def initLoad(TestInstruction instruction, ExecutionDetails detail){
		instruction.getType() >> InstructionType.LOAD
		detail.instructionType() >> InstructionType.LOAD
		// Useful when the instruction execution not succeed
		detail.caughtException() >> caughtException
	}
	
	def initCmd(TestInstruction instruction, ExecutionDetails detail){
		instruction.getType() >> InstructionType.COMMAND
		detail.instructionType() >> InstructionType.COMMAND
		// Useful when the instruction execution not succeed
		detail.caughtException() >> caughtException
	}
	
	
	def init(){
		
		ecosystemPhase.getEcosysPhase() >> EcosysPhase.RUN
		
		runnerFactory.getRunner(instLoad1) >> runLoad1

		runnerFactory.getRunner(instAssert1) >> runAssert1
		runnerFactory.getRunner(instAssert2) >> runAssert2
		runnerFactory.getRunner(instAssert3) >> runAssert3
		runnerFactory.getRunner(instAssert4) >> runAssert4
		
		runnerFactory.getRunner(instCmd1) >> runCmd1
		
	}

		
	def "Should run some instructions and expect normal result (no setup and teardown phase)"(){
		given :	
                metadataIter.hasNext() >>> [false]
			setupIter.hasNext() >>> [false]
		
			testIter.hasNext() >>> [true, true, true, false]
			testIter.next() >>> [instLoad1, instAssert1, instAssert2]
			
			teardownIter.hasNext() >>> [false]
			
		and:
			ecosystemPhase.getEcosysPhase() >> EcosysPhase.RUN
		
		and:	
			exeLoad1.getStatus() >> SUCCESS
			exeAssert1.getStatus() >> SUCCESS
			exeAssert2.getStatus() >> SUCCESS
			
		and:
			runnerFactory.getRunner(instLoad1) >> runLoad1
			runnerFactory.getRunner(instAssert1) >> runAssert1
			runnerFactory.getRunner(instAssert2) >> runAssert2
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == SUCCESS
			
			1 * runLoad1.run() >> exeLoad1
			1 * runAssert1.run() >> exeAssert1
			1 * runAssert2.run() >> exeAssert2
			
			0 * runLoad1.getInstructionsDetails() >> exeLoad1
			0 * runAssert1.getInstructionsDetails() >> exeAssert1
			0 * runAssert2.getInstructionsDetails() >> exeAssert2
			
	}
	
	
	def "Should run some instructions and expect normal result with setup and teardown phase"(){
		given :
			setupIter.hasNext() >>> [true, true, false]
			setupIter.next() >>> [instLoad1, instAssert1]
		
			testIter.hasNext() >>> [true, true, false]
			testIter.next() >>> [instAssert2, instAssert3]
			
			teardownIter.hasNext() >>> [true, true, false]
			teardownIter.next() >>> [instCmd1, instAssert4]
			
		and:
			ecosystemPhase.getEcosysPhase() >> EcosysPhase.RUN
			
		and:
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeAssert2.getStatus() >> exeAssert2Status
			exeAssert3.getStatus() >> exeAssert3Status
			
			exeCmd1.getStatus() >> exeCmd1Status
			exeAssert4.getStatus() >> exeAssert4Status
			
		and:
			runnerFactory.getRunner(instLoad1) >> runLoad1
			runnerFactory.getRunner(instAssert1) >> runAssert1
			
			runnerFactory.getRunner(instAssert2) >> runAssert2
			runnerFactory.getRunner(instAssert3) >> runAssert3
			
			runnerFactory.getRunner(instCmd1) >> runCmd1
			runnerFactory.getRunner(instAssert4) >> runAssert4
			
		when :
			def result=runner.runTest()
		
		then :
			result.getStatus() == finalStatus
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			
			callRunAssert2 * runAssert2.run() >> exeAssert2			
			callRunAssert3 * runAssert3.run() >> exeAssert3
			
			callRunCmd1 * runCmd1.run() >> exeCmd1
			callRunAssert4 * runAssert4.run() >> exeAssert4
			

			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2			
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			
		where :
			
			exeLoad1Status 		<<	[ SUCCESS 	]
			exeAssert1Status 	<<	[ SUCCESS	]
			
			exeAssert2Status	<<	[ SUCCESS 	]
			exeAssert3Status	<<	[ SUCCESS 	]
			
			exeCmd1Status 		<<	[ SUCCESS	]
			exeAssert4Status	<<	[ SUCCESS 	]
			
			finalStatus 		<<	[ SUCCESS 	]
			
			callRunLoad1		<<	[ 1			]
			callRunAssert1		<<	[ 1			]
			
			callRunAssert2		<<	[ 1			]
			callRunAssert3		<<	[ 1			]
			
			callRunCmd1			<<	[ 1			]		
			callRunAssert4		<<	[ 1			]
			
			callDetailsLoad1	<<	[ 0			]
			callDetailsAssert1	<<	[ 0			]
			
			callDetailsAssert2	<<	[ 0			]
			callDetailsAssert3	<<	[ 0			]
			
			callDetailsCmd1		<<	[ 0			]	
			callDetailsAssert4	<<	[ 0			]
			
	}
	
	@Unroll("Setup problem and no verify assertion. load1 #exeLoad1Status, assert1 #exeAssert1Status, assert2 #exeAssert1Status et cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup problem and no verify assertion."(){
		given :
			setupIter.hasNext() >>> [true, true, true, true, false]
			setupIter.next() >>> [instLoad1, instAssert1, instAssert2, instCmd1]
		
			testIter.hasNext() >>> [ true, false]
			testIter.next() >>> [instAssert3]
			
			teardownIter.hasNext() >>> [true, false]
			teardownIter.next() >>> [instAssert4]
		
		and:
			init()
			
		and:
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert1.getStatus() >> exeAssert1Status
			exeAssert2.getStatus() >> exeAssert2Status
			exeCmd1.getStatus() >> exeCmd1Status
			
			exeAssert3.getStatus() >> exeAssert3Status

			exeAssert4.getStatus() >> exeAssert4Status
			
		when :
			def result=runner.runTest()
		
		then :
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			setupPhase.getFailedInstructions().size() == setupFailNb
			setupPhase.getFailedOrErrorWithContinue() == setupVerifyPbNb
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert1 * runAssert1.run() >> exeAssert1
			callRunAssert2 * runAssert2.run() >> exeAssert2
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			callRunAssert3 * runAssert3.run() >> exeAssert3

			callRunAssert4 * runAssert4.run() >> exeAssert4
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3

			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4

			
		where :
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, ERROR		]
			exeAssert1Status 	<<	[ SUCCESS	, ERROR		, NOT_RUN	]
			exeAssert2Status	<<	[ FAIL		, NOT_RUN	, NOT_RUN	]
			exeCmd1Status		<<	[ NOT_RUN	, NOT_RUN	, NOT_RUN	]
			
			exeAssert3Status	<<	[ NOT_RUN	, NOT_RUN	, NOT_RUN	]

			exeAssert4Status	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	]
			
			setupPhaseStatus	<<	[ FAIL		, ERROR		, ERROR		]
			setupVerifyPbNb		<<	[ 0			, 0			, 0			]
			setupFailNb			<<	[ 1			, 1			, 1			]
			testPhaseStatus		<<	[ NOT_RUN	, NOT_RUN	, NOT_RUN	]
			teardownPhaseStatus	<<	[ SUCCESS	, SUCCESS	, SUCCESS	]
			finalStatus 		<<	[ FAIL		, ERROR		, ERROR		]
			resultHasOnly		<<	[ false		, false		, false		]
			
			callRunLoad1		<<	[ 1			, 1			, 1			]
			callRunAssert1		<<	[ 1			, 1			, 0			]
			callRunAssert2		<<	[ 1			, 0			, 0			]
			callRunCmd1			<<	[ 0			, 0			, 0			]
			
			callRunAssert3		<<	[ 0			, 0			, 0			]

			callRunAssert4		<<	[ 1			, 1			, 1			]

			
			callDetailsLoad1	<<	[ 0			, 0			, 0			]
			callDetailsAssert1	<<	[ 0			, 0			, 1			]
			callDetailsAssert2	<<	[ 0			, 1			, 1			]
			callDetailsCmd1		<<	[ 1			, 1			, 1			]
			
			callDetailsAssert3	<<	[ 1			, 1			, 1			]

			callDetailsAssert4	<<	[ 0			, 0			, 0			]
			
	}
		
	@Unroll("Setup problem with verify assertion, test and teardown ok. load1 #exeLoad1Status, assert1 #exeAssert1Status continue = #continue1, assert2 #exeAssert2Status continue = #continue2 and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup problem with verify assertion, test and teardown ok"(){
		given :
			setupIter.hasNext() >>> [true, true, true, true, false]
			setupIter.next() >>> [instLoad1, instAssert1, instAssert2, instCmd1]
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instAssert3]
			
			teardownIter.hasNext() >>> [true, false]
			teardownIter.next() >>> [instAssert4]
		
		and:
			init()
			
		and:
			instAssert1.continueOnFailOrError() >> continue1
			instAssert2.continueOnFailOrError() >> continue2
		
		and:
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert1.getStatus() >> exeAssert1Status
			exeAssert2.getStatus() >> exeAssert2Status
			exeCmd1.getStatus() >> exeCmd1Status
			
			exeAssert3.getStatus() >> exeAssert3Status

			exeAssert4.getStatus() >> exeAssert4Status
			
		when :
			def result=runner.runTest()
		
		then :
			
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			setupPhase.getFailedInstructions().size() == setupFailNb
			setupPhase.getFailedOrErrorWithContinue() == setupVerifyPbNb
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert1 * runAssert1.run() >> exeAssert1
			callRunAssert2 * runAssert2.run() >> exeAssert2
			callRunCmd1 * runCmd1.run() >> exeCmd1
						
			callRunAssert3 * runAssert3.run() >> exeAssert3

			callRunAssert4 * runAssert4.run() >> exeAssert4
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
						
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3

			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			
		where :
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			exeAssert1Status 	<<	[ FAIL		, ERROR		, FAIL		, FAIL		, ERROR		]
			continue1			<<	[ true		, true	, true	, true	, true	]
			exeAssert2Status	<<	[ SUCCESS	, FAIL		, ERROR		, ERROR		, FAIL		]
			continue2			<<	[ false	, true	, true	, false	, false	]
			exeCmd1Status		<<	[ SUCCESS	, SUCCESS	, SUCCESS	, NOT_RUN	, NOT_RUN	]
			
			exeAssert3Status	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, NOT_RUN	, NOT_RUN	]

			exeAssert4Status	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			setupPhaseStatus	<<	[ FAIL		, ERROR		, ERROR		, ERROR		, ERROR		]
			setupVerifyPbNb		<<	[ 1			, 2			, 2			, 1			, 1		    ]
			setupFailNb			<<	[ 1			, 2			, 2			, 2			, 2		    ]
			testPhaseStatus		<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, NOT_RUN	, NOT_RUN	]
			teardownPhaseStatus	<<	[ SUCCESS	, SUCCESS	, SUCCESS 	, SUCCESS	, SUCCESS	]
			finalStatus 		<<	[ FAIL		, ERROR		, ERROR		, ERROR		, ERROR		]
			resultHasOnly		<<	[ true		, true		, true		, false		, false	    ]
			
			callRunLoad1		<<	[1			,1			,1			,1			,1			]
			callRunAssert1		<<	[1			,1			,1			,1			,1			]
			callRunAssert2		<<	[1			,1			,1			,1			,1			]
			callRunCmd1			<<	[1			,1			,1			,0			,0			]
			
			callRunAssert3		<<	[1			,1			,1			,0			,0			]

			callRunAssert4		<<	[1			,1			,1			,1			,1			]
			
			callDetailsLoad1	<<	[0			,0			,0			,0			,0			]
			callDetailsAssert1	<<	[0			,0			,0			,0			,0			]
			callDetailsAssert2	<<	[0			,0			,0			,0			,0			]
			callDetailsCmd1		<<	[0			,0			,0			,1			,1			]
			
			callDetailsAssert3	<<	[0			,0			,0			,1			,1			]

			callDetailsAssert4	<<	[0			,0			,0			,0			,0			]
			
	}

	@Unroll("Setup and teardown ok, test problems with no verify assertion. load1 #exeLoad1Status, assert2 #exeAssert2Status, assert3 #exeAssert3Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup ok, test problems with no verify assertion and teardown ok"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, true, true, true, false]
			testIter.next() >>> [instLoad1, instAssert2, instAssert3, instCmd1]
			
			teardownIter.hasNext() >>> [true, false]
			teardownIter.next() >>> [instAssert4]
		
		and:
			init()

		and:
			
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert2.getStatus() >> exeAssert2Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeCmd1.getStatus() >> exeCmd1Status
			
			exeAssert4.getStatus() >> exeAssert4Status
			
			
		when :
			def result=runner.runTest()
		
		then :
			
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			testPhase.getFailedInstructions().size() == setupFailNb
			testPhase.getFailedOrErrorWithContinue() == setupVerifyPbNb
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert2 * runAssert2.run() >> exeAssert2
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			callRunAssert4 * runAssert4.run() >> exeAssert4
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
				
		where :
			
			exeAssert1Status 	<<	[ SUCCESS	, SUCCESS	]
			
			exeLoad1Status 		<<	[ ERROR		, SUCCESS	]
			exeAssert2Status	<<	[ NOT_RUN	, FAIL		]
			exeAssert3Status	<<	[ NOT_RUN 	, NOT_RUN	]
			exeCmd1Status		<<	[ NOT_RUN	, NOT_RUN	]
			
			exeAssert4Status	<<	[ SUCCESS 	, SUCCESS	]
			
			setupPhaseStatus	<<	[ SUCCESS	, SUCCESS	]
			testPhaseStatus		<<	[ ERROR		, FAIL		]
			setupVerifyPbNb		<<	[ 0			, 0			]
			setupFailNb			<<	[ 1			, 1			]
			teardownPhaseStatus	<<	[ SUCCESS	, SUCCESS	]
			finalStatus 		<<	[ ERROR		, FAIL		]
			resultHasOnly		<<	[ false		, false		]
			
			
			callRunAssert1		<<	[ 1			, 1			]
			
			callRunLoad1		<<	[ 1			, 1			]
			callRunAssert2		<<	[ 0			, 1			]
			callRunAssert3		<<	[ 0			, 0			]
			callRunCmd1			<<	[ 0			, 0			]
			
			callRunAssert4		<<	[ 1			, 1			]
			
			
			callDetailsAssert1	<<	[ 0 		, 0			]
			
						
			callDetailsLoad1	<<	[ 0			, 0			]
			callDetailsAssert2	<<	[ 1			, 0			]
			callDetailsAssert3	<<	[ 1			, 1			]
			callDetailsCmd1		<<	[ 1			, 1			]
			
			callDetailsAssert4	<<	[ 0			, 0			]
			
	}
	
	@Unroll("Setup and teardown ok, test problems with verify assertion. load1 #exeLoad1Status, assert2 #exeAssert2Status, assert3 #exeAssert3Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup ok, test problems with verify assertion and teardown ok"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, true, true, true, false]
			testIter.next() >>> [instLoad1, instAssert2, instAssert3, instCmd1]
			
			teardownIter.hasNext() >>> [true, false]
			teardownIter.next() >>> [instAssert4]
		
		and:
			init()
			
		and:
			instAssert2.continueOnFailOrError() >> continue2
			instAssert3.continueOnFailOrError() >> continue3
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert2.getStatus() >> exeAssert2Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeCmd1.getStatus() >> exeCmd1Status
			
			exeAssert4.getStatus() >> exeAssert4Status
			
		when :
			def result=runner.runTest()
		
		then :
			
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			testPhase.getFailedInstructions().size() == testFailNb
			testPhase.getFailedOrErrorWithContinue() == testVerifyPbNb
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert2 * runAssert2.run() >> exeAssert2
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			callRunAssert4 * runAssert4.run() >> exeAssert4
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			
		where :
			exeAssert1Status 	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			exeAssert2Status	<<	[ FAIL 		, FAIL		, ERROR		, FAIL		, ERROR		]
			continue2			<<	[ true	, true	, true	, true	, true	]
			exeAssert3Status	<<	[ SUCCESS 	, ERROR		, FAIL		, ERROR		, SUCCESS	]
			continue3			<<	[ false	, false	, true	, true	, false	]
			exeCmd1Status		<<	[ SUCCESS	, NOT_RUN	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeAssert4Status	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			setupPhaseStatus	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			testPhaseStatus		<<	[ FAIL 		, ERROR		, ERROR		, ERROR		, ERROR		]
			testVerifyPbNb		<<	[ 1			, 1			, 2			, 2			, 1			]
			testFailNb			<<	[ 1			, 2			, 2			, 2			, 1			]
			teardownPhaseStatus	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			finalStatus 		<<	[ FAIL 		, ERROR		, ERROR		, ERROR		, ERROR		]
			resultHasOnly		<<	[ true		, false		, true		, true		, true		]
			
			
			callRunAssert1		<<	[ 1			, 1			, 1			, 1			, 1			]
			
			callRunLoad1		<<	[ 1			, 1			, 1			, 1			, 1			]
			callRunAssert2		<<	[ 1			, 1			, 1			, 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			, 1			, 1			, 1			]
			callRunCmd1			<<	[ 1			, 0			, 1			, 1			, 1			]
			
			callRunAssert4		<<	[ 1			, 1			, 1			, 1			, 1			]	
			
			
			callDetailsAssert1	<<	[ 0			, 0			, 0			, 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			, 0			, 0			, 0			]
			callDetailsAssert2	<<	[ 0			, 0			, 0			, 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			, 0			, 0			, 0			]
			callDetailsCmd1		<<	[ 0			, 1			, 0			, 0			, 0			]
			
			callDetailsAssert4	<<	[ 0			, 0			, 0			, 0			, 0			]
					
	}
	
	
	@Unroll("Setup status #setupPhaseStatus and only Verify instructions, teardown ok, test problems. load1 #exeLoad1Status, assert2 #exeAssert2Status, assert3 #exeAssert3Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup ko, test problems with verify assertion and teardown ok"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, true, true, true, false]
			testIter.next() >>> [instLoad1, instAssert2, instAssert3, instCmd1]
			
			teardownIter.hasNext() >>> [true, false]
			teardownIter.next() >>> [instAssert4]
		
		and:
			init()
			
		and:
			instAssert1.continueOnFailOrError() >> continue1
			instAssert2.continueOnFailOrError() >> continue2
			instAssert3.continueOnFailOrError() >> continue3
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert2.getStatus() >> exeAssert2Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeCmd1.getStatus() >> exeCmd1Status
			
			exeAssert4.getStatus() >> exeAssert4Status
			
		when :
			def result=runner.runTest()
		
		then :
			
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			testPhase.getFailedInstructions().size() == testFailNb
			testPhase.getFailedOrErrorWithContinue() == testVerifyPbNb
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert2 * runAssert2.run() >> exeAssert2
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			callRunAssert4 * runAssert4.run() >> exeAssert4
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			
		where :
			exeAssert1Status 	<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			continue1			<<	[ true	, true	, true	, true	]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			exeAssert2Status	<<	[ FAIL 		, SUCCESS	, ERROR		, FAIL		]
			continue2			<<	[ true	, false	, true	, true	]
			exeAssert3Status	<<	[ SUCCESS 	, ERROR		, FAIL		, FAIL		]
			continue3			<<	[ false	, false	, true	, false	]
			exeCmd1Status		<<	[ SUCCESS	, NOT_RUN	, SUCCESS	, NOT_RUN	]
			
			exeAssert4Status	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			setupPhaseStatus	<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			testPhaseStatus		<<	[ FAIL 		, ERROR		, ERROR		, FAIL		]
			testVerifyPbNb		<<	[ 1			, 0			, 2			, 1			]
			testFailNb			<<	[ 1			, 1			, 2			, 2			]
			teardownPhaseStatus	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			finalStatus 		<<	[ FAIL 		, ERROR		, ERROR		, ERROR		]
			resultHasOnly		<<	[ true		, false		, true		, false		]
			
			
			callRunAssert1		<<	[ 1			, 1			, 1			, 1			]
			
			callRunLoad1		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert2		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			, 1			, 1			]
			callRunCmd1			<<	[ 1			, 0			, 1			, 0			]
			
			callRunAssert4		<<	[ 1			, 1			, 1			, 1			]
			
			
			callDetailsAssert1	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert2	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			, 0			, 0			]
			callDetailsCmd1		<<	[ 0			, 1			, 0			, 1			]
			
			callDetailsAssert4	<<	[ 0			, 0			, 0			, 0			]
					
	}
	
	
	@Unroll("Setup and test ok, teardown problems with no verify assertion. load1 #exeLoad1Status, assert3 #exeAssert3Status, assert4 #exeAssert4Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup and test ok, teardown problems with no verify assertion"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instAssert2]
			
			teardownIter.hasNext() >>> [true, true, true, true, false]
			teardownIter.next() >>> [instLoad1, instAssert3, instAssert4, instCmd1]
		
		and:
			init()
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeAssert2.getStatus() >> exeAssert2Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeAssert4.getStatus() >> exeAssert4Status
			exeCmd1.getStatus() >> exeCmd1Status
			
		when:
			def result=runner.runTest()
		
		then :
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			teardownPhase.getFailedInstructions().size() == teardownFailNb
			teardownPhase.getFailedOrErrorWithContinue() == teardownVerifyPbNb
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunAssert2 * runAssert2.run() >> exeAssert2
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunAssert4 * runAssert4.run() >> exeAssert4
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			
		where :
			exeAssert1Status 	<<	[ SUCCESS	, SUCCESS	]
			
			exeAssert2Status	<<	[ SUCCESS	, SUCCESS	]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	]
			exeAssert3Status	<<	[ FAIL	 	, SUCCESS	]
			exeAssert4Status	<<	[ NOT_RUN 	, ERROR		]
			exeCmd1Status		<<	[ NOT_RUN	, NOT_RUN	]
			
			
			
			setupPhaseStatus	<<	[ SUCCESS	, SUCCESS	]
			testPhaseStatus		<<	[ SUCCESS 	, SUCCESS	]
			teardownPhaseStatus	<<	[ FAIL		, ERROR		]
			teardownVerifyPbNb	<<	[ 0			, 0			]
			teardownFailNb		<<	[ 1			, 1			]
			finalStatus 		<<	[ FAIL 		, ERROR		]
			resultHasOnly		<<	[ false		, false		]
			
			
			callRunAssert1		<<	[ 1			, 1			]
			
			callRunAssert2		<<	[ 1			, 1			]
				
			callRunLoad1		<<	[ 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			]
			callRunAssert4		<<	[ 0			, 1			]
			callRunCmd1			<<	[ 0			, 0			]
			
			
			callDetailsAssert1	<<	[ 0			, 0			]
			
			callDetailsAssert2	<<	[ 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			]
			callDetailsAssert4	<<	[ 1			, 0			]
			callDetailsCmd1		<<	[ 1			, 1			]
			
			
					
	}
	
	
	@Unroll("Setup and test ok, teardown problems with verify assertion. load1 #exeLoad1Status, assert3 #exeAssert3Status, assert4 #exeAssert4Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup and test ok, teardown problems with verify assertion"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instAssert2]
			
			teardownIter.hasNext() >>> [true, true, true, true, false]
			teardownIter.next() >>> [instLoad1, instAssert3, instAssert4, instCmd1]
		
		and:
			init()
			
		and:
			instAssert3.continueOnFailOrError() >> continue3
			instAssert4.continueOnFailOrError() >> continue4
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeAssert2.getStatus() >> exeAssert2Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeAssert4.getStatus() >> exeAssert4Status
			exeCmd1.getStatus() >> exeCmd1Status
			
		when:
			def result=runner.runTest()
		
		then :
			PhaseResult setupPhase = result.getSetupPhaseResult()
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult()
			testPhase.getStatus() == testPhaseStatus
			
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			teardownPhase.getFailedInstructions().size() == teardownFailNb
			teardownPhase.getFailedOrErrorWithContinue() == teardownVerifyPbNb
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunAssert2 * runAssert2.run() >> exeAssert2
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunAssert4 * runAssert4.run() >> exeAssert4
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			
		where :
			exeAssert1Status 	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeAssert2Status	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			exeAssert3Status	<<	[ FAIL	 	, SUCCESS	, FAIL		, ERROR		]
			continue3			<<	[ true	, true	, true	, true	]
			exeAssert4Status	<<	[ SUCCESS 	, ERROR		, ERROR		, FAIL		]
			continue4			<<	[ false	, true	, true	, false	]
			exeCmd1Status		<<	[ SUCCESS	, SUCCESS	, SUCCESS 	, NOT_RUN	]
			
			
			
			setupPhaseStatus	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			testPhaseStatus		<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	]
			teardownPhaseStatus	<<	[ FAIL		, ERROR		, ERROR		, ERROR		]
			teardownVerifyPbNb	<<	[ 1			, 1			, 2			, 1			]
			teardownFailNb		<<	[ 1			, 1			, 2			, 2			]
			finalStatus 		<<	[ FAIL 		, ERROR		, ERROR		, ERROR		]
			resultHasOnly		<<	[ true		, true		, true		, false		]
			
			
			callRunAssert1		<<	[ 1			, 1			, 1			, 1			]
			
			callRunAssert2		<<	[ 1			, 1			, 1			, 1			]
				
			callRunLoad1		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert4		<<	[ 1			, 1			, 1			, 1			]
			callRunCmd1			<<	[ 1			, 1			, 1			, 0			]
			
			
			callDetailsAssert1	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsAssert2	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert4	<<	[ 0			, 0			, 0			, 0			]
			callDetailsCmd1		<<	[ 0			, 0			, 0			, 1			]	
					
	}
	
	
	@Unroll("Setup #setupPhaseStatus with only verify instructions, test ok, teardown problems with verify assertion. load1 #exeLoad1Status, assert3 #exeAssert3Status, assert4 #exeAssert4Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup ko with only verify instructions and test ok, teardown problems with verify assertion"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instAssert2]
			
			teardownIter.hasNext() >>> [true, true, true, true, false]
			teardownIter.next() >>> [instLoad1, instAssert3, instAssert4, instCmd1]
		
		and:
			init()
			
		and:
			instAssert1.continueOnFailOrError() >> true
			instAssert3.continueOnFailOrError() >> continue3
			instAssert4.continueOnFailOrError() >> continue4
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeAssert2.getStatus() >> exeAssert2Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeAssert4.getStatus() >> exeAssert4Status
			exeCmd1.getStatus() >> exeCmd1Status
			
		when:
			def result=runner.runTest()
		
		then :
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			teardownPhase.getFailedInstructions().size() == teardownFailNb
			teardownPhase.getFailedOrErrorWithContinue() == teardownVerifyPbNb
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunAssert2 * runAssert2.run() >> exeAssert2
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunAssert4 * runAssert4.run() >> exeAssert4
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			
		where :
			exeAssert1Status 	<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			
			exeAssert2Status	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, ERROR		, SUCCESS	]
			
			exeAssert3Status	<<	[ FAIL	 	, SUCCESS	, NOT_RUN	, FAIL		]
			continue3			<<	[ true	, true	, true	, true	]
			
			exeAssert4Status	<<	[ SUCCESS 	, ERROR		, NOT_RUN	, SUCCESS	]
			continue4			<<	[ false	, false	, true	, false	]
			
			exeCmd1Status		<<	[ SUCCESS	, NOT_RUN	, NOT_RUN 	, SUCCESS	]
			
			setupPhaseStatus	<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			testPhaseStatus		<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	]
			teardownPhaseStatus	<<	[ FAIL		, ERROR		, ERROR		, FAIL		]
			teardownVerifyPbNb	<<	[ 1			, 0			, 0			, 1			]
			teardownFailNb		<<	[ 1			, 1			, 1			, 1			]
			finalStatus 		<<	[ FAIL 		, ERROR		, ERROR		, ERROR		]
			resultHasOnly		<<	[ true		, false		, false		, true		]
			
			
			callRunAssert1		<<	[ 1			, 1			, 1			, 1			]
			
			callRunAssert2		<<	[ 1			, 1			, 1			, 1			]
				
			callRunLoad1		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			, 0			, 1			]
			callRunAssert4		<<	[ 1			, 1			, 0			, 1			]
			callRunCmd1			<<	[ 1			, 0			, 0			, 1			]
			
			
			callDetailsAssert1	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsAssert2	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			, 1			, 0			]
			callDetailsAssert4	<<	[ 0			, 0			, 1			, 0			]
			callDetailsCmd1		<<	[ 0			, 1			, 1			, 0			]
					
	}
	
	
	@Unroll("Setup ok, test #testPhaseStatus with only verify instructions, teardown problems with verify assertion. load1 #exeLoad1Status, assert3 #exeAssert3Status, assert4 #exeAssert4Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup ok, test ko with only verify instructions, teardown problems with verify assertion"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instAssert2]
			
			teardownIter.hasNext() >>> [true, true, true, true, false]
			teardownIter.next() >>> [instLoad1, instAssert3, instAssert4, instCmd1]
		
		and:
			init()
			
		and:
			instAssert2.continueOnFailOrError() >> true
			instAssert3.continueOnFailOrError() >> continue3
			instAssert4.continueOnFailOrError() >> continue4
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeAssert2.getStatus() >> exeAssert2Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeAssert4.getStatus() >> exeAssert4Status
			exeCmd1.getStatus() >> exeCmd1Status
			
		when:
			def result=runner.runTest()
		
		then :
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			teardownPhase.getFailedInstructions().size() == teardownFailNb
			teardownPhase.getFailedOrErrorWithContinue() == teardownVerifyPbNb
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunAssert2 * runAssert2.run() >> exeAssert2
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunAssert4 * runAssert4.run() >> exeAssert4
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			
		where :
			exeAssert1Status 	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeAssert2Status	<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, ERROR		, SUCCESS	]
			
			exeAssert3Status	<<	[ ERROR	 	, SUCCESS	, NOT_RUN	, SUCCESS	]
			continue3			<<	[ true	, true	, true	, false	]
			
			exeAssert4Status	<<	[ SUCCESS 	, FAIL		, NOT_RUN	, FAIL		]
			continue4			<<	[ false	, false	, true	, true	]
			
			exeCmd1Status		<<	[ SUCCESS	, NOT_RUN	, NOT_RUN 	, SUCCESS	]
			
			
			
			setupPhaseStatus	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	]
			testPhaseStatus		<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			teardownPhaseStatus	<<	[ ERROR		, FAIL		, ERROR		, FAIL		]
			teardownVerifyPbNb	<<	[ 1			, 0			, 0			, 1			]
			teardownFailNb		<<	[ 1			, 1			, 1			, 1			]
			finalStatus 		<<	[ ERROR 	, FAIL		, ERROR		, ERROR		]
			resultHasOnly		<<	[ true		, false		, false		, true		]
			
			
			callRunAssert1		<<	[ 1			, 1			, 1			, 1			]
			
			callRunAssert2		<<	[ 1			, 1			, 1			, 1			]
				
			callRunLoad1		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			, 0			, 1			]
			callRunAssert4		<<	[ 1			, 1			, 0			, 1			]
			callRunCmd1			<<	[ 1			, 0			, 0			, 1			]
			
			
			callDetailsAssert1	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsAssert2	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			, 1			, 0			]
			callDetailsAssert4	<<	[ 0			, 0			, 1			, 0			]
			callDetailsCmd1		<<	[ 0			, 1			, 1			, 0			]
					
	}
	
	@Unroll("Setup ok, test #testPhaseStatus with no verify instructions, teardown problems with verify assertion. load1 #exeLoad1Status, assert3 #exeAssert3Status, assert4 #exeAssert4Status and cmd1 #exeCmd1Status. Final status should be #finalStatus")
	def "Setup ok, test ko with no verify instructions, teardown problems with verify assertion"(){
		given :
			setupIter.hasNext() >>> [true, false]
			setupIter.next() >>> [instAssert1]
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instAssert2]
			
			teardownIter.hasNext() >>> [true, true, true, true, false]
			teardownIter.next() >>> [instLoad1, instAssert3, instAssert4, instCmd1]
		
		and:
			init()
			
		and:
			instAssert3.continueOnFailOrError() >> continue3
			instAssert4.continueOnFailOrError() >> continue4
		
		and:
			exeAssert1.getStatus() >> exeAssert1Status
			
			exeAssert2.getStatus() >> exeAssert2Status
			
			exeLoad1.getStatus() >> exeLoad1Status
			exeAssert3.getStatus() >> exeAssert3Status
			exeAssert4.getStatus() >> exeAssert4Status
			exeCmd1.getStatus() >> exeCmd1Status
			
		when:
			def result=runner.runTest()
		
		then :
			PhaseResult setupPhase = result.getSetupPhaseResult();
			setupPhase.getStatus() == setupPhaseStatus
			
			PhaseResult testPhase = result.getTestPhaseResult();
			testPhase.getStatus() == testPhaseStatus
			
			
			PhaseResult teardownPhase = result.getTeardownPhaseResult();
			teardownPhase.getStatus() == teardownPhaseStatus
			teardownPhase.getFailedInstructions().size() == teardownFailNb
			teardownPhase.getFailedOrErrorWithContinue() == teardownVerifyPbNb
			
			result.getStatus() == finalStatus
			result.hasOnlyFailOrErrorWithContinue() == resultHasOnly
			
			callRunAssert1 * runAssert1.run() >> exeAssert1
			
			callRunAssert2 * runAssert2.run() >> exeAssert2
			
			callRunLoad1 * runLoad1.run() >> exeLoad1
			callRunAssert3 * runAssert3.run() >> exeAssert3
			callRunAssert4 * runAssert4.run() >> exeAssert4
			callRunCmd1 * runCmd1.run() >> exeCmd1
			
			
			callDetailsAssert1 * runAssert1.getInstructionsDetails() >> exeAssert1
			
			callDetailsAssert2 * runAssert2.getInstructionsDetails() >> exeAssert2
			
			callDetailsLoad1 * runLoad1.getInstructionsDetails() >> exeLoad1
			callDetailsAssert3 * runAssert3.getInstructionsDetails() >> exeAssert3
			callDetailsAssert4 * runAssert4.getInstructionsDetails() >> exeAssert4
			callDetailsCmd1 * runCmd1.getInstructionsDetails() >> exeCmd1
			
			
		where :
			exeAssert1Status 	<<	[ SUCCESS	, SUCCESS	, SUCCESS	, SUCCESS	]
			
			exeAssert2Status	<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			
			exeLoad1Status 		<<	[ SUCCESS	, SUCCESS	, ERROR		, SUCCESS	]
			
			exeAssert3Status	<<	[ ERROR	 	, SUCCESS	, NOT_RUN	, SUCCESS	]
			continue3			<<	[ true	, true	, true	, false	]
			
			exeAssert4Status	<<	[ SUCCESS 	, FAIL		, NOT_RUN	, FAIL		]
			continue4			<<	[ false	, false	, true	, true	]
			
			exeCmd1Status		<<	[ SUCCESS	, NOT_RUN	, NOT_RUN 	, SUCCESS	]
			
			
			
			setupPhaseStatus	<<	[ SUCCESS 	, SUCCESS	, SUCCESS	, SUCCESS	]
			testPhaseStatus		<<	[ FAIL		, FAIL		, ERROR		, ERROR		]
			teardownPhaseStatus	<<	[ ERROR		, FAIL		, ERROR		, FAIL		]
			teardownVerifyPbNb	<<	[ 1			, 0			, 0			, 1			]
			teardownFailNb		<<	[ 1			, 1			, 1			, 1			]
			finalStatus 		<<	[ ERROR 	, FAIL		, ERROR		, ERROR		]
			resultHasOnly		<<	[ false		, false		, false		, false		]
			
			
			callRunAssert1		<<	[ 1			, 1			, 1			, 1			]
			
			callRunAssert2		<<	[ 1			, 1			, 1			, 1			]
				
			callRunLoad1		<<	[ 1			, 1			, 1			, 1			]
			callRunAssert3		<<	[ 1			, 1			, 0			, 1			]
			callRunAssert4		<<	[ 1			, 1			, 0			, 1			]
			callRunCmd1			<<	[ 1			, 0			, 0			, 1			]
			
			
			callDetailsAssert1	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsAssert2	<<	[ 0			, 0			, 0			, 0			]
			
			callDetailsLoad1	<<	[ 0			, 0			, 0			, 0			]
			callDetailsAssert3	<<	[ 0			, 0			, 1			, 0			]
			callDetailsAssert4	<<	[ 0			, 0			, 1			, 0			]
			callDetailsCmd1		<<	[ 0			, 1			, 1			, 0			]
					
	}
	
	def "Run inlined instruction"(){
		given :
			def instDefine = Mock(DefineResourceInstruction)
			def runDefine = Mock(InstructionRunner)
			def exeDefine = Mock(ExecutionDetails)
			instDefine.getType() >> InstructionType.DEFINE
			exeDefine.instructionType() >> InstructionType.ASSERTION
			runnerFactory.getRunner(instDefine) >> runDefine
		and :
			def inlinedInstr = Mock(InlineMetaInstruction)
			inlinedInstr.getInstructions() >> [instDefine,instLoad1]
			inlinedInstr.getType() >> InstructionType.INLINE_META
			inlinedInstr.getLine() >> "1"
		and :
			setupIter.hasNext() >>> [false]
			setupIter.next() >>> []
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [inlinedInstr]
			
			teardownIter.hasNext() >>> [false]
			teardownIter.next() >>> []
		
		and :
			init()
			
		and:
			exeLoad1.getStatus() >> GeneralStatus.SUCCESS
			exeDefine.getStatus() >> GeneralStatus.SUCCESS
		
		when :
			def result=runner.runTest()
			
		then :
			result.getTestPhaseResult().getAllInstructions().size() == 1
			result.getTestPhaseResult().getAllInstructions()[0].getChildrens().size() == 2
			1 * runDefine.run() >> exeDefine
			1 * runLoad1.run() >> exeLoad1
			result.getTestPhaseResult().getAllInstructions()[0].getStatus() == GeneralStatus.SUCCESS
			result.getTestPhaseResult().getAllInstructions()[0].instructionType() == InstructionType.LOAD
	}
	
	def "Run macro instruction"(){
		given :
			def instMacro = Mock(MacroInstruction)
			instMacro.getInstructions() >> [instLoad1,instCmd1,instAssert1]
			instMacro.getType() >> InstructionType.MACRO
			instMacro.getLine() >> "1"
		
		and :
			setupIter.hasNext() >>> [ false]
			setupIter.next() >>> []
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instMacro]
			
			teardownIter.hasNext() >>> [false]
			teardownIter.next() >>> []
		
		and :
			init()
			
		and:
			exeAssert1.getStatus() >> GeneralStatus.SUCCESS			
			exeLoad1.getStatus() >> GeneralStatus.SUCCESS
			exeCmd1.getStatus() >> GeneralStatus.SUCCESS
		
		when :
			def result=runner.runTest()
			
		then :
			result.getTestPhaseResult().getAllInstructions().size() == 1
			result.getTestPhaseResult().getAllInstructions()[0].getChildrens().size() == 3
			1 * runAssert1.run() >> exeAssert1
			1 * runLoad1.run() >> exeLoad1
			1 * runCmd1.run() >> exeCmd1
			result.getTestPhaseResult().getAllInstructions()[0].getStatus() == GeneralStatus.SUCCESS
	}
	
	
	def "Run macro instruction with failed children"(){
		given :
			def instMacro = Mock(MacroInstruction)
			instMacro.getInstructions() >> [instLoad1,instCmd1,instAssert1]
			instMacro.getType() >> InstructionType.MACRO
			instMacro.getLine() >> "1"
			
		and:
			setupIter.hasNext() >>> [ false]
			setupIter.next() >>> []
		
			testIter.hasNext() >>> [true, false]
			testIter.next() >>> [instMacro]
			
			teardownIter.hasNext() >>> [false]
			teardownIter.next() >>> []
		
		and :
			init()
			
		and:
			exeAssert1.getStatus() >> GeneralStatus.NOT_RUN
			exeLoad1.getStatus() >> GeneralStatus.SUCCESS
			exeCmd1.getStatus() >> GeneralStatus.ERROR
		
		when :
			def result=runner.runTest()
			
		then :
			result.getTestPhaseResult().getAllInstructions().size() == 1
			result.getTestPhaseResult().getAllInstructions()[0].getChildrens().size() == 3
			1 * runAssert1.getInstructionsDetails() >> exeAssert1
			1 * runLoad1.run() >> exeLoad1
			1 * runCmd1.run() >> exeCmd1
			result.getTestPhaseResult().getAllInstructions()[0].getStatus() == GeneralStatus.ERROR
	}
	
	
	def "should add then get a resource to/from the cache"(){
		
		given :
			def res = Mock(ResourceWrapper)
			res.getName() >> new ResourceName("resource")
			
		when :
			runner.addResourceToCache(res)
			def result = runner.getResourceFromCache(new ResourceName("resource"))
			
		then :
			result == res		
		
	}
	
	def "should rant because another resource with that name already exists"(){
		
		given :
			def res = Mock(ResourceWrapper)
			res.getName() >> new ResourceName("resource")
			
		and :
			def res2 = Mock(ResourceWrapper)
			res2.getName() >> new ResourceName("resource")
		when :
			runner.addResourceToCache(res)
			runner.addResourceToCache(res2)
		
		then :
			thrown NotUniqueEntryException
		
	}
	
	def "should not return a resource that is not cached"(){
		
		expect :
			runner.getResourceFromCache(new ResourceName("test")) == null
		
	}
	
	def "should clean the place up once the test is over"(){
		
		given :
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			def res3 = Mock(ResourceWrapper)
		
			res1.getName() >> new ResourceName("resource1")
			res2.getName() >> new ResourceName("resource2")
			res3.getName() >> new ResourceName("resource3")
			
		and :
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
			runner.addResourceToCache(res3)
		and :
			runner.setLocalContext(Mock(LocalContext))
		when :
			runner.cleanUp()
			
		then :
			runner.testResources.isEmpty()==true
			1 * res1.cleanUp()
			1 * res2.cleanUp()
			1 * res3.cleanUp()
			1 * runner.localContext.cleanUp()
	}
	
	def "should sucessfully clean up without the optional local context"(){
		
		given :
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			def res3 = Mock(ResourceWrapper)
		
			res1.getName() >> new ResourceName("resource1")
			res2.getName() >> new ResourceName("resource2")
			res3.getName() >> new ResourceName("resource3")
			
		and :
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
			runner.addResourceToCache(res3)
		when :
			runner.cleanUp()
		then :
			runner.testResources.isEmpty()==true
			1 * res1.cleanUp()
			1 * res2.cleanUp()
			1 * res3.cleanUp()
	}
	
	def "should not conflict resources with same short name in two namespaces"(){
		given:
			runner.setLocalContext(new DefaultLocalContextImpl(0))
		and:
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			
			res1.getName() >> new ResourceName(Scope.SCOPE_TEST,"samename")
			res2.getName() >> new ResourceName(Scope.SCOPE_TEMPORARY,"samename")
			
		and:
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
		when:
			def found1=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEMPORARY,"samename"))
			def found2=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"samename"))
		then:
			found1 == res2
			found2 == res1
	}
	
	def "should lose temporary but not test scope resources when reset local context"(){
		given:
			runner.setLocalContext(new DefaultLocalContextImpl(0))
		and:
			def res1 = Mock(ResourceWrapper)
			def res2 = Mock(ResourceWrapper)
			
			res1.getName() >> new ResourceName(Scope.SCOPE_TEST,"samename")
			res2.getName() >> new ResourceName(Scope.SCOPE_TEMPORARY,"samename")
		and:
			runner.addResourceToCache(res1)
			runner.addResourceToCache(res2)
		when:
			runner.setLocalContext(Mock(LocalContext))
			def found1=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEMPORARY,"samename"))
			def found2=runner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"samename"))
		then:
			found1==null
			found2==res1
	}
	
	def "should throw cleanly when using a local context before initialisation"(){
		given:
			def res = Mock(ResourceWrapper)
		and:
			res.getName() >> new ResourceName(Scope.SCOPE_TEMPORARY,"name")
		when:
			runner.addResourceToCache(res)
		then:
			thrown BrokenTestException
	}
	
	class DummyAssertionInstruction extends AbstractTestInstruction{
	
		@Override
		public String toText() {
			return "Dummy Assertion Instruction for unit test purpose";
		}
	
		@Override
		public void visit(TestInstructionVisitor visitor) {
			// nothing to do here 
	
		} 
		
	}
	
}

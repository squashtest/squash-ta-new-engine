/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.tools;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

import org.squashtest.ta.backbone.pseudoimplem.PseudoCommand
import org.squashtest.ta.backbone.pseudoimplem.PseudoConverter
import org.squashtest.ta.backbone.pseudoimplem.PseudoResource
import org.squashtest.ta.backbone.pseudoimplem.PseudoTarget
import org.squashtest.ta.backbone.exception.EngineInitException;
import org.squashtest.ta.backbone.exception.EngineRuntimeException;
import org.squashtest.ta.backbone.exception.MissingAnnotationException;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.CannotApplyCommandException;

import spock.lang.Specification;

public class ReflectionUtilsTest extends Specification {

	ReflectionUtils utils = new ReflectionUtils(); 
	
	def "should get a class from type"(){
		given :
			Type[] types = PseudoConverter.class.getGenericInterfaces(); 
		
		when :
			def result = utils.getClassFromType(types[0])
		
		then :
			result == ResourceConverter.class
		
	}
	
	def "should get classes from generic types"(){
		given :
			def types = PseudoConverter.class.getGenericInterfaces()[0].getActualTypeArguments();
		
		when :
			def result = []
			result << utils.getClassFromType(types[0])
			result << utils.getClassFromType(types[1])
			
		then :
			result == [FileResource.class, PseudoResource.class]
	}
	
	def "among many types, should find which ones corresponds to the class we specified"(){
		given :
			def types = PseudoCommand.class.getGenericInterfaces();
		
		when :
			def result = utils.findTypeMatchingClass(types, Command.class)
		
		then :
			result == types[2]
	}
	
	def "should get the generic parameters of an engine component (1)"(){
		given :
			def componentClass = PseudoConverter.class
			def componentInterface = ResourceConverter.class
		
		when :
			def result = utils.getGenericTypes(componentClass, componentInterface)
			
		then :
			result == [FileResource.class, PseudoResource.class]
	}
	
	def "should get the generic parameters of an engine component (2)"(){
		given :
			def componentClass = PseudoCommand.class
			def componentInterface = Command.class
			
		when :
			def result = utils.getGenericTypes(componentClass, componentInterface)
			
		then :
			result == [PseudoResource.class, PseudoTarget.class]
	}
	
	def "should rant because we inspect a class that don't have the right interface"(){
		given :
			def componentClass = PseudoConverter.class
			def componentInterface = BinaryAssertion.class
			
		when :
			utils.getGenericTypes(componentClass, componentInterface)
			
		then :
			thrown(EngineInitException)
		
	}
	
	def "should get the value of ResourceType annotation"(){
		given :
			def inst = new PseudoResource();	
		
		when :
			def result = utils.getResourceTypeValue(inst);
					
		then :
			result == "pseudoresource"
	}
	
	def "should rant because the ResourceType annotation was not found"(){
		given :
			def inst = new PseudoConverter();	
		
		when :
			utils.getResourceTypeValue(inst);
		
		then :
			thrown MissingAnnotationException
	}
	
	def "should get the value of EngineComponent annotation"(){
		given :
			def inst = new PseudoConverter()	
		
		when :
			def result = utils.getEngineComponentValue(inst)
		
		then :
			result == "pseudo"
	}	
	
	def "should rant because the EngineComponent annotation was not found"(){
		given :
			def inst = new PseudoResource()
			
		when :
			utils.getEngineComponentValue(inst);
			
		then :
			thrown MissingAnnotationException
	}
	
	def "should return the return type of a <? extends Command> apply"(){
		
		given :
			def componentClass = PseudoCommand.class
			
		when :
			def clazz = utils.getReturnType(componentClass, "apply")
			
		then :
			clazz == PseudoResource.class
		
	}
	
	
	def "should invoke a method on an object with no argument"(){
		given :
			def command = new PseudoCommand()
		
		when :
			def result = utils.invoke(command, "apply", null,null)
		
		then :
			result instanceof PseudoResource								
		
	}
	
	def "should invoke a method on an object with one argument"(){
		given :
			def command = new PseudoCommand()
			def target = new PseudoTarget()
		
		when :
			utils.invoke(command, "setTarget", PseudoTarget.class,target)
			def res = command.getTarget()
		
		then :
			res == target	
		
	}
	
	def "should wrap no such method exceptions in an engine runtime exception"(){
		given :
			def command = new PseudoCommand();
		when :
			utils.invoke(command, "test", null,null)
		then :
			def ex = thrown(EngineRuntimeException)
			ex.getCause() instanceof NoSuchMethodException
	}
	
	def "should unwrap invokation target exception and rewrap them in engine runtime exception"(){
		given :
			def command = new PseudoCommand();
		when :
			utils.invoke(command, "throwInvocationTargetException",null, null)
		then :
			def ex = thrown(EngineRuntimeException)
			ex.getCause() instanceof CannotApplyCommandException
	}
	
	
	//other exceptions are boring to test and unlikely to happen
	
	
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine.instructionrunner

import org.squashtest.ta.backbone.engine.ContextManager
import org.squashtest.ta.backbone.engine.impl.InternalTestRunner
import org.squashtest.ta.backbone.engine.instructionrunner.DefaultExecuteCommandRunner
import org.squashtest.ta.backbone.engine.wrapper.CommandHandler;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.ResourceWrapper;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.exception.ImpossibleConversionException;
import org.squashtest.ta.backbone.exception.IncompatibleNaturesException
import org.squashtest.ta.backbone.exception.MalformedInstructionException;
import org.squashtest.ta.backbone.exception.ResourceNotFoundException
import org.squashtest.ta.backbone.exception.TargetNotFoundException
import org.squashtest.ta.backbone.test.DefaultExecutionDetails
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.components.Command
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.components.Target
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.exception.CannotApplyCommandException;
import org.squashtest.ta.framework.exception.BrokenTestException

import spock.lang.Specification

class DefaultExecuteCommandRunnerTest extends Specification {
	
	DefaultExecuteCommandRunner runner
	InternalTestRunner testRunner
	ExecuteCommandInstruction instruction
	ContextManager manager
	
	
	def setup(){
		instruction = Mock(ExecuteCommandInstruction)
		runner = new DefaultExecuteCommandRunner(instruction)
		
		testRunner = Mock(InternalTestRunner)
		
		runner.setTestRunner testRunner
		
		manager = Mock(ContextManager)
		
	}
	
	
	def "should rant because resource name is missing"(){
		when :
			runner.checkSettings()
		
		then :
			thrown MalformedInstructionException
	}
	
	
	def "should rant because result name is missing"(){
		given :
			instruction.getResourceName() >> new ResourceName(Scope.SCOPE_TEST,"test")
		
		when :
			runner.checkSettings()
		then :
			thrown BrokenTestException;
	}
	
	
	def "should rant because command name is missing"(){
		given :
			instruction.getResourceName() >> new ResourceName(Scope.SCOPE_TEST,"test")
			instruction.getResultName() >> new ResourceName(Scope.SCOPE_TEST,"result")
			
		when :
			runner.checkSettings()
		then :
			thrown BrokenTestException;
	}
	
	def "should rant because target name is missing"(){
		given :
			instruction.getResourceName() >> new ResourceName(Scope.SCOPE_TEST,"test")
			instruction.getResultName() >> new ResourceName(Scope.SCOPE_TEST,"result")
			instruction.getCommandCategory() >> "command"
		
		when :
			runner.checkSettings()
		then :
			thrown BrokenTestException;
	}
	
	def "should be clean"(){
		given :
			mockInstruction()
			
		when :
			runner.checkSettings()
			
		then :
			true
	}
	

	
	def "should return the instruction"(){
		expect :
			runner.getInstruction() == instruction
		
	}
	
	def "should rant because the resource is not found"(){
		given :
			mockInstruction()
			
		when :
			def resultat=runner.run()
			
		then :
			resultat.getStatus() == GeneralStatus.ERROR
			resultat.caughtException() instanceof ResourceNotFoundException
	}
	
	def "should rant because the target is not found"(){
		
		given :
			mockInstruction()
		
		and :
			def resource = Mock(ResourceWrapper)
			resource.getNature()>>new Nature("mock")
			resource.unwrap()>> Mock(Resource)
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"resource")) >> resource
			
		and :
			testRunner.getContextManager() >> manager
		
		when :
			def resultat=runner.run()
		then :
			resultat.getStatus() == GeneralStatus.ERROR
			resultat.caughtException() instanceof TargetNotFoundException
	}
	
	
	def "should work normally"(){
		
		given :
			mockInstruction()
			def resource = mockResource()
			def target = mockTarget()
			
		and :
		
			def command = Mock(CommandHandler)
			def resultResource = Mock(ResourceWrapper)
			
		and :
			testRunner.getContextManager() >> manager
			manager.getCommand(_) >> [command]
			
		and :
			testRunner.getResourceFromCache(new ResourceName(Scope.SCOPE_TEST,"resource")) >> resource
			manager.getTarget("target") >> target
			
		when :
			runner.run()
			
		then :
			1 * command.apply() >> resultResource
			1 * testRunner.addResourceToCache(resultResource)
	}
	
	def "should diagnose error and return incompatible nature exception"(){
		
		given :
			mockInstruction()
			def resource = mockResource()
			def target = mockTarget()
		
		and :
			testRunner.getContextManager() >> manager
			manager.getCommand(_) >> [Mock(Command)]
			
		when :
			def res = runner.diagnoseError(resource, target, "test")
			
		then :
			res instanceof IncompatibleNaturesException
		
	}
	
	def "should diagnose error and return cannot apply command exception"(){
		
		given :
			mockInstruction()
			def resource = mockResource()
			def target = mockTarget()
			
		and :
			testRunner.getContextManager() >> manager
			manager.getCommand(_) >> []
			
		when :
			def res = runner.diagnoseError(resource, target, "test")
			
		then :
			res instanceof CannotApplyCommandException
	
	}
	
	def "stock non void result"(){
		given:
			FileResource fileR = new FileResource(null)
			ResourceWrapper wrap = new ResourceWrapper(null, null, fileR, null)
			
		when:
			runner.stockResult(wrap)
		then:
		1 * runner.testRunner.addResourceToCache(wrap)
	}
	
	def "doesn't stock VoidResult"(){
		given:
			VoidResource voidR = new VoidResource()
			ResourceWrapper wrap = new ResourceWrapper(null, null, voidR, null)			
		when:
			runner.stockResult(wrap)
		then:
		0 * runner.testRunner.addResourceToCache(wrap)
	}
	
	/* *************************** ************************** */
	
	def mockInstruction = {
		instruction.getResourceName() >> new ResourceName(Scope.SCOPE_TEST,"resource")
		instruction.getResultName() >> new ResourceName(Scope.SCOPE_TEST,"result")
		instruction.getCommandCategory() >> "command"
		instruction.getTargetName() >> "target"
		instruction.getCommandConfiguration() >> []
		instruction.getType() >> InstructionType.COMMAND
		instruction.getLine() >> "0"
		instruction.toText() >> "Mock command test instruction"
		
	}
	
	def mockResource(){
		def resource = Mock(ResourceWrapper)
		def resNature = Mock(Nature)
		resNature.getName() >> "resource.nature"
		resource.getNature() >> resNature
		resource.getName() >> new ResourceName(Scope.SCOPE_TEST,"resource")
		return resource		
	}
	
	def mockTarget(){
		def target = Mock(TargetWrapper)
		def tarNature = Mock(Nature)
		tarNature.getName() >> "target.nature"
		target.getNature() >> tarNature
		return target
		
	}
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.backbone.engine

import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.engine.wrapper.TargetWrapper;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.backbone.pseudoimplem.PseudoBinaryAssertion;
import org.squashtest.ta.backbone.pseudoimplem.PseudoCommand;
import org.squashtest.ta.backbone.pseudoimplem.PseudoConverter;
import org.squashtest.ta.backbone.pseudoimplem.PseudoResourceRepository;
import org.squashtest.ta.backbone.pseudoimplem.PseudoSynchronousHandler
import org.squashtest.ta.backbone.pseudoimplem.PseudoUnaryAssertion;
import org.squashtest.ta.core.tools.io.TempFileUtils
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.Ecosystem;
import org.squashtest.ta.framework.test.definition.Environment;
import org.squashtest.ta.framework.test.definition.Test;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.ta.framework.test.instructions.BinaryAssertionInstruction;
import org.squashtest.ta.framework.test.instructions.ConvertResourceInstruction;
import org.squashtest.ta.framework.test.instructions.DefineResourceInstruction;
import org.squashtest.ta.framework.test.instructions.ExecuteCommandInstruction;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.instructions.ResourceName.Scope;
import org.squashtest.ta.framework.test.instructions.UnaryAssertionInstruction;

import spock.lang.Specification

/**
 * This test class features a number of engine integration tests mainly aimed at testing the engine component assembly mechanism. 
 * @author edegenetais
 *
 */
class EngineIntegrationTest extends Specification {
	def workspaceBrowser
	Engine testEngine
	def pseudof1
	Test test
	def TestSuite suite
	
	def setup(){
            //FIXME: only add init() methods to static classes if you are ready to face torture! In modern multithreaded systems static classes are meant to be stateless
            //if it has state, it's an instance, and the state is orderly setup in a CONSTRUCTOR!!!
            //for the time being, let's work around bad code...
            org.squashtest.ta.core.tools.io.TempFileUtils.init(java.lang.System.getProperty("java.io.tmpdir"),"true");
		workspaceBrowser=Mock(TestWorkspaceBrowser)
		workspaceBrowser.getDefaultResourceRepository() >> new PseudoResourceRepository()
		workspaceBrowser.getRepositoriesDefinitions() >> []
		workspaceBrowser.getTargetsDefinitions() >> []
		workspaceBrowser.getDefaultResourceFile() >> null
		
		def loader=new EngineLoader()
		testEngine=loader.load()
		
		pseudof1=new DefineResourceInstruction()
		pseudof1.setResourceName(new ResourceName("pseudof1"))
		
		test=Mock(Test)
                test.getMetadata() >> [].iterator()
		test.getSetup() >> [].iterator()
		test.getTeardown() >> [].iterator()
		
		def ecoSetup=Mock(Test)
                ecoSetup.getMetadata() >> [].iterator()
		ecoSetup.getSetup() >> [].iterator()
		ecoSetup.getTests() >> [].iterator()
		ecoSetup.getTeardown() >> [].iterator()
		ecoSetup.getTargetsNames() >> new HashSet<String>()
		
		def ecoTeardown=Mock(Test)
                ecoTeardown.getMetadata() >> [].iterator()
		ecoTeardown.getSetup() >> [].iterator()
		ecoTeardown.getTests() >> [].iterator()
		ecoTeardown.getTeardown() >> [].iterator()
		ecoTeardown.getTargetsNames() >> new HashSet<String>()
		
		def environment=Mock(Environment)
		environment.getSetUp() >> ecoSetup
		environment.getTearDown() >> ecoTeardown
		
		def ecosystem=new Ecosystem();
		ecosystem.setName("Engine integration test ecosystem")
		ecosystem.setEnvironment(environment)
		ecosystem.getTestPopulation().add(test)
		
		suite=new TestSuite()
		suite.setName("Engine integration test suite")
		suite.addEcosystem(ecosystem)
		
		
	}
	
	def "should have file to pseudo converter loaded"(){
		given:
			def pseudo1=new ConvertResourceInstruction()
			pseudo1.setConverterCategory("pseudo")
			pseudo1.setResourceName(new ResourceName("pseudof1"))
			pseudo1.setResultName(new ResourceName("pseudo1"))
			pseudo1.setResultType("pseudoresource")
		and:
			test.getTests() >> [pseudof1,pseudo1].iterator()
		and:
			suite.getTargetsNames() >> new HashSet<String>()
			test.getTargetsNames() >> new HashSet<String>()
		when:
			def resultat=testEngine.execute(suite,workspaceBrowser)
		then:
			resultat.getStatus().isPassed()
			PseudoConverter.tested==true;
	}

	def "should have binary pseudo assertion loaded"(){
		given:
			def binary=new BinaryAssertionInstruction()
			binary.setAssertionCategory("binaryassertion")
			binary.setActualResultName(new ResourceName("pseudo1"))
			binary.setExpectedResultName(new ResourceName("pseudo2"))
		and:
			def pseudof2=new DefineResourceInstruction()
			pseudof2.setResourceName(new ResourceName("pseudof2"))
		and:
			def pseudo1=new ConvertResourceInstruction()
			pseudo1.setConverterCategory("pseudo")
			pseudo1.setResourceName(new ResourceName("pseudof1"))
			pseudo1.setResultName(new ResourceName("pseudo1"))
			pseudo1.setResultType("pseudoresource")
		and:
			def pseudo2=new ConvertResourceInstruction()
			pseudo2.setConverterCategory("pseudo")
			pseudo2.setResourceName(new ResourceName("pseudof2"))
			pseudo2.setResultName(new ResourceName("pseudo2"))
			pseudo2.setResultType("pseudoresource")
		and:
			test.getTests() >> [pseudof1,pseudof2,pseudo1,pseudo2,binary].iterator()
		and:
			suite.getTargetsNames() >> new HashSet<String>()
			test.getTargetsNames() >> new HashSet<String>()
			when:
			def resultat=testEngine.execute(suite,workspaceBrowser)
		then:
			resultat.getStatus().isPassed()
			PseudoBinaryAssertion.tested==true;
	}
	
	def "should have unary pseudo assertion loaded"(){
		given:
			def unary=new UnaryAssertionInstruction()
			unary.setAssertionCategory("unaryassertion")
			unary.setActualResultName(new ResourceName("pseudo1"))
		and:
			def pseudo1=new ConvertResourceInstruction()
			pseudo1.setConverterCategory("pseudo")
			pseudo1.setResourceName(new ResourceName("pseudof1"))
			pseudo1.setResultName(new ResourceName("pseudo1"))
			pseudo1.setResultType("pseudoresource")
		and:
			test.getTests() >> [pseudof1,pseudo1,unary].iterator()
		and:
			suite.getTargetsNames() >> new HashSet<String>()
			test.getTargetsNames() >> new HashSet<String>()
		when:
			def resultat=testEngine.execute(suite,workspaceBrowser)
		then:
			resultat.getStatus().isPassed()
			PseudoUnaryAssertion.tested==true;
	}
	
	def "should have pseudo command loaded"(){
		given:
			def command=new ExecuteCommandInstruction();
			command.setCommandCategory("pseudo")
			command.setResourceName(new ResourceName("pseudo1"))
			command.setResultName(new ResourceName("mathom"))
			command.setTargetName("dummy")
		and:
			workspaceBrowser.getTargetsDefinitions().add(new File("dummy").toURI().toURL())
		and:
			def pseudo1=new ConvertResourceInstruction()
			pseudo1.setConverterCategory("pseudo")
			pseudo1.setResourceName(new ResourceName("pseudof1"))
			pseudo1.setResultName(new ResourceName("pseudo1"))
			pseudo1.setResultType("pseudoresource")
		and:
			test.getTests() >> [pseudof1,pseudo1,command].iterator()
		and:
			def targetsNames = new HashSet<String>()
			targetsNames.add("dummy")
			suite.getTargetsNames() >> targetsNames
			test.getTargetsNames() >> targetsNames
		when:
			def resultat=testEngine.execute(suite,workspaceBrowser)
		then:
			resultat.getStatus().isPassed()
			PseudoCommand.tested==true;
	}
	
	def "should emit events to listeners"(){
		given:
			StatusUpdateListener listener=Mock()
                        PseudoSynchronousHandler synchroListener=new PseudoSynchronousHandler()
                        
			testEngine.addEventListener(listener)
                        testEngine.addEventListener(synchroListener)
		and:
			test.getTests() >> [].iterator()
		and:
			suite.getTargetsNames() >> new HashSet<String>()
			test.getTargetsNames() >> new HashSet<String>()
		when:
			testEngine.execute(suite, workspaceBrowser)
			java.lang.Thread.sleep(3000)
		then:
			2 * listener.handle({it instanceof TestSuiteStatusUpdate}) 
			
			2 * listener.handle({it instanceof EcosystemStatusUpdate})
                        			
			6 * listener.handle({it instanceof TestStatusUpdate}) // 2 asynchronous events/test, 3 'tests': ecosystem setup, test, ecosystem teardown

                        synchroListener.nbTestEvents()==3 // 1 asynchronous events/test, 3 'tests': ecosystem setup, test, ecosystem teardown
	}
}

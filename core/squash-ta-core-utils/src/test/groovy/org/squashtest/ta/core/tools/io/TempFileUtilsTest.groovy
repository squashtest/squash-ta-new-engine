/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io

import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.core.tools.io.Unzipper;
import org.squashtest.ta.framework.tools.TempDir;
import org.apache.commons.io.FileUtils;



import spock.lang.Specification;

class TempFileUtilsTest extends Specification{
	
	def "init properly"(){
		when:
			TempFileUtils.init("target/tempDirectory", "false")
		then:
			TempFileUtils.timestamp != null
			TempFileUtils.debugMode == false
			TempDir.getMacroTempDir().getPath().replace("\\", "/").contains("target/tempDirectory/Squash_TA/"+TempFileUtils.timestamp+"/macros")
		cleanup:
			new File("target/tempDirectory").deleteDir()		
	}
	
	def "construct full directory"(){
		given:
			TempFileUtils.executionName = "execution"
			TempFileUtils.ecosystemName = "ecosystem"
			TempFileUtils.testName = "test"
			TempFileUtils.timestamp = 321
			TempDir.setMainTempDir("target/mainPath")
		when:
			def result=TempFileUtils.createDirectory()
			def path = result.getAbsolutePath().replace("\\", "/")
		then:
			path.contains("target/mainPath/Squash_TA/321/execution/ecosystem/test")
		cleanup:
			new File("target/mainPath").deleteDir()
	}
	
	def "if an argument is empty, construct default directory (1)"(){
		given:
			TempFileUtils.executionName = "execution"
			TempFileUtils.ecosystemName = "ecosystem"
			TempFileUtils.testName = null
			TempFileUtils.timestamp = 321
			TempDir.setMainTempDir("target/mainPath")
		when:
			def result=TempFileUtils.createDirectory()
		then:
			result.getAbsolutePath() == TempDir.getDefaultTempDir().getAbsolutePath()
		cleanup:
			new File("target/mainPath").deleteDir()
	}
	
	def "if an argument is empty, construct default directory (2)"(){
		given:
			TempFileUtils.executionName = null
			TempFileUtils.ecosystemName = "ecosystem"
			TempFileUtils.testName = "test"
			TempFileUtils.timestamp = 321
			TempDir.setMainTempDir("target/mainPath")
		when:
			def result=TempFileUtils.createDirectory()
		then:
			result.getAbsolutePath() == TempDir.getDefaultTempDir().getAbsolutePath()
		cleanup:
			new File("target/mainPath").deleteDir()
	}
	
	def "if an argument is empty, construct default directory (3)"(){
		given:
			TempFileUtils.executionName = null
			TempFileUtils.ecosystemName = null
			TempFileUtils.testName = null
			TempFileUtils.timestamp = 321
			TempDir.setMainTempDir("target/mainPath")
		when:
			def result=TempFileUtils.createDirectory()
		then:
			result.getAbsolutePath() == TempDir.getDefaultTempDir().getAbsolutePath()
		cleanup:
			new File("target/mainPath").deleteDir()
	}
	
	def "delete after execution if debugMode = false"(){
		given:
                        TempFileUtils.init("mock", "false")

		when:
			
                        TempFileUtils.cleanUpTempDirectory()
		then:
                        !TempDir.getTestMainTempDir().exists()   
                        
		cleanup:
			new File("target/mock").deleteDir()
        
		}
		
	
	def "do not delete after execution if debugMode = true"(){
		given:
                        TempFileUtils.init("mock", "true")

		when:
                        
		      TempFileUtils.cleanUpTempDirectory()
		then:

                      TempDir.getTestMainTempDir().exists()
		cleanup:
			new File("target/mock").deleteDir()
        
		}				
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io

import spock.lang.Specification;
import spock.lang.Unroll;

import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;

/**
 * Test stub for the zipper.
 * @author edegenetais
 */
class ZipperTest extends Specification{
	def baseDir
	
	def rootTest
	
	def setup(){
		baseDir=new Unzipper().unzipInTemp(getClass().getResourceAsStream("/zippertest.zip"))
		rootTest= new File(baseDir, "zippertest")
	}
	def "should be able to zip files without error"(){
            when:
                File zip=Zipper.ZIPPER.compress(rootTest);
            then:
                zip.exists()
        }

        def "should have the same list of files"(){
            given:
                Set<String> expectedFileNames=['target1.properties','target2.properties','targetGroup/target1.properties','targetGroup/target4.properties']
            when:
                File zip=Zipper.ZIPPER.compress(rootTest);
                Set<String> actualFileNames=new HashSet();
                ZipInputStream zis=new ZipInputStream(new FileInputStream(zip))
                ZipEntry z=zis.getNextEntry()
                while(z!=null){
                    actualFileNames.add(z.getName())
                    z=zis.getNextEntry()
                }
            then:
                expectedFileNames.equals(actualFileNames);
            cleanup:
                zis.close()
        }

        @Unroll
        def "#name contents in zip should match original file"(){
            given:
                File zip=Zipper.ZIPPER.compress(rootTest);
                ZipInputStream zis=new ZipInputStream(new FileInputStream(zip))
            and:
                ZipEntry z=zis.getNextEntry()
                while(z!=null && z.getName()!=name){
                    z=zis.getNextEntry()
                }
                int actualData=zis.read();
            and:
                File source=new File(rootTest,name)
                FileInputStream fis=new FileInputStream(source)
                int expectedData=fis.read()
            expect:
                actualData==expectedData
                while(actualData>=0 && expectedData>=0){
                    assert actualData==expectedData
                    actualData=zis.read()
                    expectedData=fis.read()
                }
            where:
                name << ['target1.properties','target2.properties','targetGroup/target1.properties','targetGroup/target4.properties']
        }
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io

import spock.lang.Specification;
import spock.lang.Unroll;
import org.squashtest.ta.core.tools.io.StorageSizeUnit.StorageSizeFormatException;

/**
 *
 * @author edegenetais
 */
class StorageSizeUnitTest extends Specification {
    
	def "Should reject empty string"(){
            when:
                StorageSizeUnit.getSizeUnit("")
            then:
                thrown StorageSizeFormatException
        }
        
        def "Should reject string without unit"(){
            when:
                StorageSizeUnit.getSizeUnit("256")
            then:
                thrown StorageSizeFormatException
        }
        
        @Unroll("When size string is #sizeString then size in bytes should be #expected")
        def "should compute correct byte size"(){
            when:
                def result=StorageSizeUnit.getSizeInBytes(sizeString)
            then:
                result==expected
            where:
                sizeString              |expected
                "23b"                   |23L
                "12Kb"                  |12288L
                "12Mb"                  |12582912L
                "12Gb"                  |12884901888L
                "12.5Gb"                |13421772800L
        }
}


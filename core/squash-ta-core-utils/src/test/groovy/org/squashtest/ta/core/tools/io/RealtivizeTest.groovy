/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io

import spock.lang.Specification
import spock.lang.Unroll

class RealtivizeTest extends Specification {

	def Relativize testee
	
	def setup(){
		testee = new Relativize()
	}
	
	@Unroll("When rootPath is \"#rootPath\" and path to transform as relatif is \"#pathToTransform\" then return path should be \"#result\"")
	def "Should return good relative path"(){ 
		when:
			def res = testee.relativize(rootPath, pathToTransform)
		then:
			res == result
		where:
			rootPath			|	pathToTransform				|	result					
			"c:\\root"			|	"c:\\root\\todo\\..\\done"	|	"todo/../done"
			"root"				|	"root/todo/done"			|	"todo/done"
			"/"					|	"/root/todo/./done"			|	"root/todo/./done"
			 
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.util.Arrays;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * This class encapsulates the various strategies that may be used to inject classpath element.
 * @author edegenetais
 */
public class ClasspathInjector {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ClasspathInjector.class);

    public boolean classPathInjector(final Class<?> aClass, final String domain, String... jarPathTable) throws BrokenTestException {
        boolean classPathEntriesInjectedOrDetected = false;
        ClassLoader cl = aClass.getClassLoader();
        final ClasspathInjectionStrategy[] strategyFamily = new ClasspathInjectionStrategy[]{new URLClassloaderClasspathInjectionStrategy()};
        final Iterator<ClasspathInjectionStrategy> strategyIterator = Arrays.asList(strategyFamily).iterator();
        while (strategyIterator.hasNext() && !classPathEntriesInjectedOrDetected) {
            classPathEntriesInjectedOrDetected = strategyIterator.next().tryInjectionStrategy(cl, domain, jarPathTable);
        }
        if (!classPathEntriesInjectedOrDetected) {
            LOGGER.warn("No successful classpath injection strategy, and the {} jars where not detected in the classpath. {} tests may fail with a NoCLassDefFoundError", domain, domain);
        }
        return classPathEntriesInjectedOrDetected;
    }
    
}

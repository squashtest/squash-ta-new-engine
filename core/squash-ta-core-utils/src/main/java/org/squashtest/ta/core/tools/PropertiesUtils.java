/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.library.properties.PropertiesSource;

/**
 * Component for properties set aggregation, with a priority system for conflicting definitions.
 * @author bsiri
 */
public final class PropertiesUtils {

	private static final PropertiesUtils INSTANCE = new PropertiesUtils();

	public PropertiesUtils() {
            //empty constructor
	}

	/**
	 * Will return a Properties based on a hierarchical search in the given
	 * {@link PropertiesSource} array, and for the keys listed in the given
	 * PropertiesKeySet. The PropertieSources must be ordered by decreasing
	 * priority, i.e. if a property exists in the first and third PropertySource
	 * the returned value is read from the first one.
	 * 
	 * @param keys
	 *            the set of keys one want to look for in the PropertySources
	 * @param propertiesSources
	 *            the PropertiesSources
	 * @return a Properties aggregated just like explained above.
	 */
	public static Properties staticGetAggregatedProperties(
			PropertiesKeySet keys, PropertiesSource... propertiesSources) {
		return INSTANCE.getAggregatedProperties(keys, propertiesSources);
	}

	public Properties getAggregatedProperties(PropertiesKeySet keys,
			PropertiesSource... propertiesSources) {
		// now let's fill the result
		Properties resultProperties = new Properties();

		// now we loop over the keys, searching each sources. Since the sources
		// are sorted by increasing priority we skip the remaining sources
		// as soon as a property was found.
		for (String key : keys) {
			for (PropertiesSource source : propertiesSources) {
				String found = source.getValue(key);
				if (found != null) {
					resultProperties.setProperty(key, found);
					break;
				}
			}
		}

		// job done
		return resultProperties;

	}

	/**
	 * Alias for
	 * {@link #getAggregatedProperties(PropertiesKeySet, PropertiesSource...)}.
	 * Effective properties just sound as good.
	 * 
	 * @param keys
	 * @param propertiesSources
	 * @return
	 */
	public static Properties staticGetEffectiveProperties(
			PropertiesKeySet keys, PropertiesSource... propertiesSources) {
		return INSTANCE.getEffectiveProperties(keys, propertiesSources);
	}

	/**
	 * Alias for
	 * {@link #getAggregatedProperties(PropertiesKeySet, PropertiesSource...)}.
	 * Effective properties just sound as good.
	 * 
	 * @param keys
	 * @param propertiesSources
	 * @return
	 */
	public Properties getEffectiveProperties(PropertiesKeySet keys,
			PropertiesSource... propertiesSources) {
		return getAggregatedProperties(keys, propertiesSources);
	}

	/**
	 * Given a regular expression, will look for all keys matching it in the
	 * various {@link PropertiesSource}s object supplied as argument. The
	 * resulting key set will be returned as a PropertiesKeySet.
	 * 
	 * @param regexp
	 * @param propertiesSources
	 * @return
	 */
	public static PropertiesKeySet staticFindKeysByPattern(String regexp,
			PropertiesSource... propertiesSources) {
		return INSTANCE.findKeysByPattern(regexp, propertiesSources);
	}

	/**
	 * Given a regular expression, will look for all keys matching it in the
	 * various {@link PropertiesSource}s object supplied as argument. 
         * The resulting key set will be returned as a PropertiesKeySet.
	 * 
	 * @param regexp
	 * @param propertiesSources
	 * @return
	 */
	public PropertiesKeySet findKeysByPattern(String regexp,
			PropertiesSource... propertiesSources) {

		// now we loop over the sources and add the results
		PropertiesKeySet result = new PropertiesKeySet();

		for (PropertiesSource source : propertiesSources) {
			List<String> found = source.findKeysMatchingPattern(regexp);
			result.addUnique(found);
			continue;
		}

		return result;
	}

	/**
	 * <p>
	 * That method returns a 'stripped' Properties object. 
         * For each key matching the prefix, 
         * the key-value pair will be included in the result file with the
	 * key being stripped of the prefix.
	 * </p>
	 * <p>
	 * Example :</p>
	 * <ul>
	 * <li>before :{org.mystuff.key1 = a, org.mystuff.key2 = b,
	 * org.otherstuff.key = c}</li>
	 * <li>after application with prefix org.mystuff. : {key1 = a, key2 = b}
	 * <li>after application with prefix org.otherstuff : {.key = c}</li>
	 * </ul>
         * <p>
	 * Note that the final '.' in prefix are relevant and decide the output
	 * keys.
	 * </p>
	 */
	public static Properties staticGetStrippedProperties(Properties properties,
			String prefix) {
		return INSTANCE.getStrippedProperties(properties, prefix);
	}

	/**
	 * <p>
	 * that method returns a Properties object. For each key matching the
	 * prefix, the key-value pair will be included in the result file with the
	 * key being stripped of the prefix.
	 * </p>
	 * <p>
	 * Example :</p>
	 * <ul>
	 * <li>before :{org.mystuff.key1 = a, org.mystuff.key2 = b,
	 * org.otherstuff.key = c}</li>
	 * <li>after application with prefix org.mystuff. : {key1 = a, key2 = b}
	 * <li>after application with prefix org.otherstuff : {.key = c}</li>
	 * </ul>
	 * <p>
         * Note that the final '.' in prefix are relevant and decide the output
	 * keys.
	 * </p>
	 */
	public Properties getStrippedProperties(Properties properties, String prefix) {

		Pattern pattern = Pattern
				.compile("^" + Pattern.quote(prefix) + "(.+)$");
		Properties newProperties = new Properties();

		@SuppressWarnings("unchecked")//property keys are ALWAYS strings, see Properties.setProperty(String,String)...
		Enumeration<String> keySet = (Enumeration<String>) properties
				.propertyNames();

		while (keySet.hasMoreElements()) {
			String key = keySet.nextElement();

			Matcher matcher = pattern.matcher(key);

			if (matcher.matches()) {
				String strippedKey = matcher.group(1);
				newProperties.setProperty(strippedKey, properties.getProperty(key));
			}

		}

		return newProperties;
	}

	/**
	 * Gets a key's associated value as an {@link Integer}.
	 * 
	 * @param properties
	 *            the source properties object.
	 * @param key
	 *            the key
	 * @return <ul>
	 *         <li>an Integer if the value is a valid {@link Integer} String
	 *         representation.</li>
	 *         <li><code>null</code> if the property is not defined or empty</li>
	 *         </ul>
	 * @throws IllegalArgumentException if the value exists and is not parseable as an Integer.
	 */
	public Integer getIntegerValue(Properties properties, String key) {
		Integer value = null;
		String valueString = properties.getProperty(key);
		if (valueString != null && valueString.length() > 0) {
			try {
				value = Integer.valueOf(valueString);
			} catch (NumberFormatException nfe) {
				throw new IllegalArgumentException(key
						+ " is not a valid integer properties.", nfe);
			}
		}
		return value;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.library.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;


/**
 * 
 * A PropertiesKeySet, in its most simple expression, is a bunch of strings that represent keys in properties files. It 
 * supplies a few methods meant for checking which keys are available and which are not.
 * 
 * 
 * @author bsiri
 *
 */


public class PropertiesKeySet implements Iterable<String>{
	
	private List<String> keys = new ArrayList<String>();
	
	
	/* **************************** ctors  *************************************** */
	
	public PropertiesKeySet(){
		super();
	}
		
	
	public PropertiesKeySet(String[] keys){
		this.keys.addAll(Arrays.asList(keys));
	}
	
	
	
	public PropertiesKeySet(Collection<String> keys){
		this.keys.addAll(keys);
	}

	
	public PropertiesKeySet(PropertiesKeySet set){
		for (String k : set ){
			this.keys.add(k);
		}
	}
	
	
	public PropertiesKeySet(Properties ppts){
		for (Object k : ppts.keySet()){
			this.keys.add((String)k);
		}
	}

	
	/* ***************************** unary arguments ******************************** */
	
	public void addUnique(String key){
		if (! keys.contains(key)){
			keys.add(key);
		}
	}
	
	
	public void remove(String key){
		keys.remove(key);
	}
	
	
	public boolean contains(String key){
		return (keys.contains(key));
	}
	
	
	/* ************************* from String[] ************************* */

	
	public void addUnique(String[] keys){
		for (String k : keys){
			addUnique(k);
		}
	}	
	
	public void removeAll(String[] keys){
		this.keys.removeAll(Arrays.asList(keys));
	}
	
	public boolean containsAll(String... keys){
		for (String k : keys){
			if (! this.keys.contains(k)) {
                            return false;
                        }
		}
		return true;
	}
	
	public boolean containsOne(String[] keys){
		for (String k : keys){
			if (this.keys.contains(k)) {
                            return true;
                        }
		}
		return false;
	}
	
	public boolean containsExactly(String[] keys){
		List<String> lKeys = Arrays.asList(keys);
		
		for (String k : this.keys){
			if (! lKeys.contains(k)) {
                            return false;
                        }
		}
		
		return containsAll(keys);
	}
	
	public boolean containsNone(String[] keys){
		for (String k : keys){
			if (this.keys.contains(k)) {
                            return false;
                        }
		}
		return true;
	}
	
	
	/* ************************* from Collection ************************ */
	
	
	public void addUnique(Collection<String> keys){
		for (String k : keys){
			addUnique(k);
		}
	}
		
	public void removeAll(Collection<String> keys){
		this.keys.removeAll(keys);
	}
	
	public boolean containsAll(Collection<String> keys){
		for (String k : keys){
			if (! this.keys.contains(k)) {
                            return false;
                        }
		}
		return true;
	}
	
	public boolean containsOne(Collection<String> keys){
		for (String k : keys){
			if (this.keys.contains(k)) {
                            return true;
                        }
		}
		return false;
	}
	
	public boolean containsExactly(Collection<String> keys){
		
		for (String k : this.keys){
			if (! keys.contains(k)) {
                            return false;
                        }
		}
		
		return containsAll(keys);
	}
	
	public boolean containsNone(Collection<String> keys){
		for (String k : keys){
			if (this.keys.contains(k)) {
                            return false;
                        }
		}
		return true;
	}
	
	
	/* ************************* from other PropertiesKeySet ************* */
	
	public void addUnique(PropertiesKeySet set){
		for (String k : set){
			addUnique(k);
		}
	}
		
	public void removeAll(PropertiesKeySet set){
		for (String k : set){
			this.keys.remove(k);
		}
	}

	public boolean containsAll(PropertiesKeySet set){
		for (String k : set){
			if (! keys.contains(k)) {
                            return false;
                        }
		}
		return true;
	}
	
	public boolean containsOne(PropertiesKeySet set){
		for (String k : keys){
			if (this.keys.contains(k)) {
                            return true;
                        }
		}
		return false;
	}
	
	public boolean containsExactly(PropertiesKeySet set){
		
		for (String k : this.keys){
			if (! set.contains(k)) {
                            return false;
                        }
		}
		
		return containsAll(set);
	}
	
	public boolean containsNone(PropertiesKeySet set){
		for (String k : keys){
			if (this.keys.contains(k)) {
                            return false;
                        }
		}
		return true;
	}
	
	
	/* ************************ from Properties *************************** */
	

	public void addUnique(Properties ppts){
		for (Object k : ppts.keySet()){
			addUnique((String)k);
		}
	}
		
	public void removeAll(Properties ppts){
		for (Object k : ppts.keySet()){
			keys.remove((String)k);
		}
	}

	public boolean containsAll(Properties ppts){
		for (Object k : ppts.keySet()){
			if (! keys.contains((String)k)) {
                            return false;
                        }
		}
		return true;
	}
	
	public boolean containsOne(Properties ppts){
		for (Object k : ppts.keySet()){
			if (this.keys.contains((String)k)) {
                            return true;
                        }
		}
		return false;
	}
	
	public boolean containsExactly(Properties ppts){
		
		for (String k : this.keys){
			if (ppts.getProperty(k)==null) {
                            return false;
                        }
		}
		
		return containsAll(ppts);
	}
	
	public boolean containsNone(Properties ppts){
		for (Object k : ppts.keySet()){
			if (this.keys.contains((String)k)) {
                            return false;
                        }
		}
		return true;
	}
	
	/**
	 * Get the set of common keys between this and a properties object.  
	 * @param ppts the properties to search against.
	 * @return the set of common keys. An empty set if there are none.
	 */
	@SuppressWarnings("unchecked")//here we known what the keys are (as they are comming from properties) but we want to use hashtable interface for performance
	public Set<String> getCommonKeys(Properties ppts){
		@SuppressWarnings("rawtypes")//see above.
		Set commonKeys=new HashSet(keys);
		commonKeys.retainAll(ppts.keySet());
		return (Set<String>)commonKeys;
	}

	/* ********************************* utils ******************************* */
	
	@Override
	public Iterator<String> iterator() {
		return keys.iterator();
	}
	
	
	public PropertiesKeySet copy(){
		return new PropertiesKeySet(keys);
	}
	
}

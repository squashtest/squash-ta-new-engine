/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.framework.tools.TempDir;

/**
 * Test helper class to unzip a file tree in a temporary location in order to
 * test classes designed to operate explicitely on the filesystem.
 * 
 * @author edegenetais
 * 
 */
public class Unzipper {
	public static final Unzipper UNZIPPER=new Unzipper();
	/**
	 * Method to unzip a file
	 * 
	 * @param sourceStream The stream to unzip
	 * @param targetDirectory Directory into which the file should be unzip
	 * @throws IOException Exception occur during the unzip
	 */
	public void unzip(InputStream sourceStream, File targetDirectory) throws IOException {
		ZipInputStream zis = new ZipInputStream(sourceStream);
		byte[] buffer = new byte[4096];
		ZipEntry entry = zis.getNextEntry();
		while (zis.available() == 1) {
			File entryFile = new File(targetDirectory, entry.getName());
			if (entry.isDirectory()) {
				boolean created=entryFile.mkdirs();
				if(!created){
					throw new IOException("Failed to create unzipped directory "+entryFile);
				}
				entry = zis.getNextEntry();
			} else {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				int len = zis.read(buffer, 0, buffer.length);
				while (len >= 0) {
					baos.write(buffer, 0, len);
					len = zis.read(buffer, 0, buffer.length);
				}
				if (!entryFile.getParentFile().exists()) {
					cleanCreatePathDirs(entryFile);
				}
				flushToFile(baos, entryFile);
				zis.closeEntry();
				entry = zis.getNextEntry();
			}
		}
	}
	
	/**
	 * Unzips the target in a temporary file.
	 * 
	 * @return
	 * @throws IOException
	 */
	public File unzipInTemp(InputStream sourceStream) throws IOException {
		File tempFile = cleanCreateTempDestDirectory();
		unzip(sourceStream, tempFile);
		return tempFile;
	}

	protected File cleanCreateTempDestDirectory() throws IOException {
                LoggerFactory.getLogger(Unzipper.class).debug("Trying to create temporary storage in "+TempDir.getExecutionTempDir());
		File tempFile = File.createTempFile("unzipTemp", "rootDir", TempDir.getExecutionTempDir());
		boolean deleted=tempFile.delete();
		if(!deleted){
			throw new IOException("Could not delete temp file? WTF?");
		}
		boolean dirCreated=tempFile.mkdir();
		if(!dirCreated){
			throw new IOException("Could not create temp directory? WTF?");
		}
		return tempFile;
	}

	protected void cleanCreatePathDirs(File entryFile) throws IOException {
		boolean treeCreated=entryFile.getParentFile().mkdirs();
		if(!treeCreated && !entryFile.getParentFile().exists()){
			throw new IOException("Failed to create destination path when writing "+entryFile.getAbsolutePath());
		}
	}

	private void flushToFile(ByteArrayOutputStream baos, File entryFile)
			throws FileNotFoundException, IOException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(entryFile);
			fos.write(baos.toByteArray());
		} finally {
			if (fos != null){
				fos.close();
			}
		}
	}

	public void clean(File directory) throws IOException {
		new FileTree().clean(directory);
	}
}

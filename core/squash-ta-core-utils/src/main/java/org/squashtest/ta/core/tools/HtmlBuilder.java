/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.Unzipper;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * HTML builder tool based on the freemarker html templating tool.
 * 
 * @author bfranchet
 * 
 */
public class HtmlBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(HtmlBuilder.class);
        
        private static final String UTF8_ENCODING = "UTF-8";

	/** freemarker configuration */
	private Template freemarkerTemplate;

	/**
	 * Constructor which initialize the HTML builder. A template with the templateName given in argument will be
	 * searched in the classpath. The class and path prefix are used to locate the template in the classpath.
	 * 
	 * @param templateLoadingclazz
	 *            The Class from which the template will be search in the classpath
	 * @param templateLoadingPathPrefix
	 *            The path to the template file in the classpath relative to the Class position.
	 * @param templateName
	 *            The name of the template
	 * @throws IOException
	 *             Exception occurs while retrieving the template in the classpath
	 */
	public HtmlBuilder(Class templateLoadingclazz, String templateLoadingPathPrefix, String templateName)
			throws IOException {
		Configuration conf = new Configuration();
		conf.setClassForTemplateLoading(templateLoadingclazz, templateLoadingPathPrefix);
		conf.setObjectWrapper(new DefaultObjectWrapper());
		// reading
		conf.setDefaultEncoding(UTF8_ENCODING);
		// writing
		conf.setOutputEncoding(UTF8_ENCODING);
		freemarkerTemplate = conf.getTemplate(templateName);
	}

	/**
	 * <p>This method create the html file in the target directory given in argument. 
         * If you give the name of a zip resource file (for stylesheets, css, images, ...),
         * it will be unziped and put in the target directory. 
         * If you have no zip, set the zipResouceName argument to <code>null</code>. </p>
         * <p>
	 * Concerning the zip resource, a shared resource module has been created (squash-ta-shared-resources).
         * This maven module is supposed to contain all the resources for all generated html files. 
         * A specific zip is created for each feature using the HtmlBuilder by using maven assembly. 
         * (You can have a look to the html-report or the diff-report code as use case example).
	 * </p>
	 * @param map
	 *            The data to use to populate the template
	 * @param targetDirectory
	 *            The directory in which the html file will be generated
	 * @param htmlFileName
	 *            The name of the generated html file
	 * @param zipResourceName
	 *            The name of zip which contains additional resources (javascript, css, images, ...)
	 * @throws IOException
	 *             Exception occurs during the creation of the html file
	 */
	public void process(Map<String, Object> map, File targetDirectory, String htmlFileName, String zipResourceName)
			throws IOException {
		if (targetDirectory.isDirectory()) {
			Writer file = null;
			try {
				if (zipResourceName != null) {
					unzipResource(zipResourceName, targetDirectory);
				}
				file = new OutputStreamWriter(new FileOutputStream(new File(targetDirectory, htmlFileName)),UTF8_ENCODING);
				freemarkerTemplate.process(map, file);
			} catch (TemplateException e) {
				LOGGER.error("A problem occurs during the HTML report generation", e);
			} finally {
				if (file != null) {
					file.flush();
					file.close();
				}
			}
		} else {
			throw new IllegalArgumentException(targetDirectory.getAbsolutePath() + " is not a directory.");
		}
	}

	/**
	 * This method unzips the resources (images, stylesheet or javascript) needed by the generated html code.
	 * 
	 * @param zipResourceName
	 *            The zip file name to unzip
	 * @throws IOException
	 *             Exception occur during the unzip
	 */
	private void unzipResource(String zipResourceName, File targetDirectory) throws IOException {
		InputStream iStream = this.getClass().getClassLoader().getResourceAsStream(zipResourceName);
		if (iStream != null) {
			Unzipper unzipTool = new Unzipper();
			unzipTool.clean(targetDirectory);
			unzipTool.unzip(iStream, targetDirectory);
		}
	}

}

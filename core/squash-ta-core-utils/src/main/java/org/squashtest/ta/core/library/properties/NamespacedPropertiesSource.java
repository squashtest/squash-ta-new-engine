/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.library.properties;

import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class NamespacedPropertiesSource implements PropertiesSource{
	
	private final Properties ppts;
	private final String namespace;
	
	public NamespacedPropertiesSource(Properties properties, String namespace){
		this.ppts=properties;
		this.namespace=namespace;
	}
	
	@Override
	public String getValue(String key) {
		String finalKey = namespace + '.' + key;
		return ppts.getProperty(finalKey);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findKeysMatchingPattern(String regexp){
		
		String finalPattern = makeNamespacedPattern(regexp);
		List<String> resultKeys = new LinkedList<String>();
		
		Enumeration<String> keys = (Enumeration<String>)ppts.propertyNames();
		
		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			if (key.matches(finalPattern)){
				resultKeys.add(stripNamespace(key));
			}
		}
		
		return resultKeys;
	}
	
	private String makeNamespacedPattern(String regexp){
		
		if (regexp.charAt(0)=='^'){
			String res = regexp.substring(1);
			return "^"+namespace+"\\."+res;
		}else{
			return namespace+"\\."+regexp;
		}
	}
	
	private String stripNamespace(String key){
		return key.substring(namespace.length()+1);//+1 for the dot '.'
	}
	
	
}
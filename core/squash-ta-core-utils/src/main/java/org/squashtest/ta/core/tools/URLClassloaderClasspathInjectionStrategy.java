/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * This {@link ClasspathInjectionStrategy} implementation targets URLClassLoader.
 * @author edegenetais
 */
class URLClassloaderClasspathInjectionStrategy implements ClasspathInjectionStrategy {
    private static final Logger LOGGER=LoggerFactory.getLogger(URLClassloaderClasspathInjectionStrategy.class);
    @Override
    public boolean tryInjectionStrategy(ClassLoader cl, String domainName, String... jarPathTable) throws BrokenTestException {
        boolean classPathInjected = false;
        if (cl instanceof URLClassLoader) {
            try {
                URL[] urls = ((URLClassLoader) cl).getURLs();
                final List<URL> classpathEntries = Arrays.asList(urls);
                final List<URL> newEntries = new ArrayList<>(jarPathTable.length);
                for (String path : jarPathTable) {
                    newEntries.add(new File(path).toURI().toURL());
                }
                if (classpathEntries.containsAll(newEntries)) {
                    classPathInjected = true;
                    LOGGER.debug("Component classloader already contains the {} classpath entries.", domainName);
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Trying to add classpath entries [{}] to the component classloader.", Arrays.toString(jarPathTable));
                    }
                    Method addUrlMeth = cl.getClass().getMethod("addURL", URL.class);
                    for (URL entry : newEntries) {
                        addUrlMeth.invoke(cl, entry);
                    }
                    classPathInjected = true;
                }
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Unable to add {} jar entries to the classloader used by the engine for components. You may want to add these files to your pom project dependencies : {}", domainName, Arrays.toString(jarPathTable), ex);
                }
            } catch (MalformedURLException ex) {
                LOGGER.error("URL bizarre", ex);
            }
        } else {
            LOGGER.debug(getClass().getSimpleName() + " cannot ensure that your {} jars are in the classpath because the {} classloader is not a {}.", domainName, URLClassLoader.class.getName());
        }
        return classPathInjected;
    }
    
}

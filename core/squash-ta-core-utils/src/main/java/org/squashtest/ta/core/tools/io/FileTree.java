/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.tools.TempDir;

/**
 * Enumerator for file trees.
 * @author edegenetais
 *
 */
public class FileTree {
      
	private static final Logger LOGGER = LoggerFactory.getLogger(FileTree.class);
	/**
	 * Just to save garbage collection & array constructor time...
	 */
	private static final File[] EMPTY_FILELIST = new File[]{};
	/**
	 * FileTree toolkit instance (will allow smooth derivation should the need arise).
	 */
	private static final FileTree INSTANCE=new FileTree();
	
	private static final String DEFAULT_PREFIX="_ta_";
    
        /** Public instance to avoid instanciation */
        public static final FileTree FILE_TREE = new FileTree();
        
        protected FileTree(){}
	
	
	/**
	 * Describes the various enumeration modes, controlling the elements included in the resulting tree content set.
	 * @author edegenetais
	 *
	 */
	public enum EnumerationMode{
		/** Only non-directory files are enumerated */
		FILES_ONLY,
		/** All elements are enumerated */
		FILES_AND_DIRECTORIES,
		/** Only directories are enumerated */
		DIRECTORIES_ONLY
	}
	
	/**
	 * Enumerate a file tree.
	 * @param root root directory of the file tree to enumerate.
	 * @param withDirs if <code>true</code>, directories are included as entries in the content list, otherwise only non-directories are.
	 * @return the list of all nodes of the file tree. The root is never included.
	 */
	public List<File> enumerate(File root, boolean withDirs){
		return enumerate(root, withDirs?EnumerationMode.FILES_AND_DIRECTORIES:EnumerationMode.FILES_ONLY);
	}
	
	/**
	 * Enumerate a file tree.
	 * @param root root directory of the file tree to enumerate.
	 * @param mode enumeration mode (which elements do we add to the result, see {@link EnumerationMode} for more details).
	 * @return the list of all nodes of the file tree. The root is never included.
	 */
	public List<File> enumerate(File root, EnumerationMode mode){
		int index=0;
		List<File>directoryQueue=new ArrayList<>();
		Set<File> enumeratedFiles=new HashSet<>();//index to speed up contains calls...
		List<File> content=new LinkedList<>();
		directoryQueue.add(root);
		while(index<directoryQueue.size()){
			File currentFile=directoryQueue.get(index);
			index++;
			if(currentFile.isDirectory()){
				/* If the current file is a directory, we enqueue its contents for further treatment. */
				File[] listFiles = currentFile.listFiles();
				if(listFiles==null){//could happen if read access denied...security exception not thrown, but in this cas we get a shitty null return!
					listFiles=EMPTY_FILELIST;
				}
				for(File currFile:listFiles){
					if(!enumeratedFiles.contains(currFile)){//guard against cycles induced by symlinks or junctions
						directoryQueue.add(currFile);
					}
				}
				if(mode!=EnumerationMode.FILES_ONLY){
					addToContent(root, currentFile, content);//added separately from index to keep order...
					addToContent(root, currentFile, enumeratedFiles);
				}
			}else if (mode!=EnumerationMode.DIRECTORIES_ONLY){
				addToContent(root, currentFile, content);//added separately from index to keep order...
				addToContent(root, currentFile, enumeratedFiles);
			}
		}
		return content;
	}
	
	/**
	 * Enumerate the content of a directory without analyzing subdirectories
	 * 
	 * @param directory The directory to explore
	 * @param mode The enumeration mode for the exploration see {@link EnumerationMode}
	 * @return The the list of {@link File} contains in the directory
	 */
	public List<File> enumerateCurrentLevel(File directory, EnumerationMode mode) {
		List<File> content=new LinkedList<>();
		File[] listFiles = directory.listFiles();
		if(listFiles==null){//could happen if read access denied...security exception not thrown, but in this cas we get a shitty null return!
			listFiles=EMPTY_FILELIST;
		}
		for (File file : listFiles) {
			if(file.isDirectory()){
				if ( mode!=EnumerationMode.FILES_ONLY){
					content.add(file);
				}
			} else if(mode!=EnumerationMode.DIRECTORIES_ONLY){
				content.add(file);
			}
		}
		
		return content;
	}
	
        public File copyToTemp(File original) throws IOException{
            if(!original.exists()){
                throw new IllegalArgumentException(original.getAbsolutePath()+" does not exist, cannot be copied!");
            }
            File destination;
            if(original.isFile()){
                destination=createTempCopyDestination(original);
                fileCopy(original, destination);
            }else if(original.isDirectory()){
                destination=createTempCopyDestination(original);
                toTempDirectory(destination);
                List<File> content=enumerate(original, EnumerationMode.FILES_AND_DIRECTORIES);
                for(File f:content){
                    File copyFile=new File(destination,getRelativePath(original, f));
                        if(f.isDirectory() && !copyFile.exists()){
                            copyFile.mkdirs();    
                        }else if(f.isFile()){
                            copyFile.getParentFile().mkdirs();
                            fileCopy(f, copyFile);
                        }
                    }
            }else{
                throw new IllegalArgumentException("Unsupported file type: "+original.getAbsoluteFile());
            }
            return destination;
        }

    private void fileCopy(File original, File destination) throws IOException {
        try(
                FileInputStream input=new FileInputStream(original);
                FileOutputStream output=new FileOutputStream(destination);
                ){
            IOUtils.copy(input, output);
        }
    }
        
	/**
	 * Wipes out a file subtree. To be used with care! This is normally guarded
	 * against following links to delete outside of the intended subtree (and
	 * may fail if the subtree contains links), but be sure you actually want to
	 * delete the directory and all its content and sub-directories contents.
	 * 
	 * @param targetDirectory
	 *            the root of the subtree to remove.
	 * @throws IOException
	 *             happens if something goes amiss with the suppression.
	 */
	public void clean(File targetDirectory) throws IOException{
		//first, enumerate the subtree
		List<File> subtreeContent=enumerate(targetDirectory, true);
		//second, delete, but forbid ourself to delete outside the root (to avoid the explosive effects of symlinks!)
		int index=subtreeContent.size()-1;
		Map<File,Boolean> fileStatusCache=new HashMap<>();
		while(index>=0){
			File deleteCandidate=subtreeContent.get(index);
			Boolean inside = checkIfInsideBaseDir(targetDirectory,
					fileStatusCache, deleteCandidate);
			if(inside){
				boolean deleted=deleteCandidate.delete();
				if(!deleted && deleteCandidate.exists()){
					
					throw new IOException("Failed to delete file "+deleteCandidate.getAbsolutePath());
				}
			}
			index--;
		}
		targetDirectory.delete();
	}

	/**
	 * This method checks if a given file is inside a given base directory. If
	 * the transmitted path traverses a link, the location taken into account is
	 * the physical, not soft, location of the file.
	 * 
	 * @param baseDirectory
	 *            the base directory to check.
	 * @param fileStatusCache
	 *            optional {@link Map} with pre-computed status for some
	 *            reference directories (if you don't need this, see {@link #checkInsideBaseDir(File, File)}).
	 * @param toCheck
	 *            the file to check.
	 * @return <code>true</code> if the file to check is physically inside the
	 *         base directory. <code>false</code> if not (for example if it is
	 *         seen as inside through a soft link).
	 * @throws IOException
	 *             if the checks trigger an I/O error.
	 */
	public boolean checkIfInsideBaseDir(File baseDirectory,
			Map<File, Boolean> fileStatusCache, File toCheck)
			throws IOException {
		baseDirectory=baseDirectory.getCanonicalFile();
		Set<File> parentSet=new HashSet<>();
		File canonicalFile = toCheck.getCanonicalFile();
		File parent=canonicalFile.getParentFile();
		Boolean inside=null;
		while(inside==null){
			parentSet.add(parent);
			if(Boolean.TRUE.equals(fileStatusCache.get(parent)) || parent.equals(baseDirectory)){
				inside=Boolean.TRUE;
			}else{
				//otherwise we climb up a level.
				parent=parent.getParentFile();
				if(parent==null || Boolean.FALSE.equals(fileStatusCache.get(parent))){
					/*
					 * We climbed up to a known outsider, or
					 * we climbed up to the root without finding a known
					 * [sub]directory of our tree --> we are outside
					 */
					inside=Boolean.FALSE;
				}
			}
		}
		for(File pathElement:parentSet){
			fileStatusCache.put(pathElement, inside);
		}
		return inside;
	}
	
	/**
	 * This method checks if a given file is inside a given base directory. If
	 * the transmitted path traverses a link, the location taken into account is
	 * the physical, not soft, location of the file.
	 * 
	 * @param baseDirectory
	 *            the base directory to check.
	 * @param toCheck
	 *            the file to check.
	 * @return <code>true</code> if the file to check is physically inside the
	 *         base directory. <code>false</code> if not (for example if it is
	 *         seen as inside through a soft link).
	 * @throws IOException
	 *             if the checks trigger an I/O error.
	 */
	public boolean checkInsideBaseDir(File baseDirectory, File toCheck) throws IOException{
		return checkIfInsideBaseDir(baseDirectory, new HashMap<File, Boolean>(), toCheck);
	}
	
	private void addToContent(File root, File file, Collection<File> content){
		if(!file.equals(root)){
			content.add(file);
		}
	}
	
	
	
	/**
	 * Returns true if the directory 'directory' contains the file ' file', false if not.
	 * However the test is pathname-based, which make it less reliable than the other
	 * methods.
	 * 
	 * @param directory
	 * @param  file
	 * @return
     * @throws java.io.IOException
	 */
	public boolean cheapCheckIfInsideBaseDir(File directory, File  file) throws IOException{
		String normDirPath = FilenameUtils.normalize(directory.getCanonicalPath());
		String normFilePath = FilenameUtils.normalize(file.getCanonicalPath());
		return Pattern.matches("^"+Pattern.quote(normDirPath)+".*", normFilePath);
	}
	
	/**
	 * static version of {@link #cheapCheckIfInsideBaseDir(File, File)} for tired people.
	 * @param directory
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static  boolean staticCheapCheckIfInsideBaseDir(File directory, File  file) throws IOException{
		return INSTANCE.cheapCheckIfInsideBaseDir(directory, file);
	}
	
	/**
	 * Returns the path of the File 'file' relative to the directory 'directory'. 
	 * If the file is not included in 'directory', then null will be returned instead.
	 * 
	 * String pathname based : the resolution may have flaws. It does not support links. But for simple needs it'll do fine.
	 * 
	 * @param directory
	 * @param file
	 * @return
     * @throws java.io.IOException
	 */
	public String getRelativePath(File directory, File file) throws IOException{
		String path=null;
		if (cheapCheckIfInsideBaseDir(directory, file)){//if we're not inside, don't even try to compute relative path...
			
			//we're inside, let's see where...
			String normDirPath = FilenameUtils.normalize(directory.getCanonicalPath());
			String normFilePath = FilenameUtils.normalize(file.getCanonicalPath());
					
			Matcher matcher = Pattern.compile("^"+Pattern.quote(normDirPath)+"(?:\\/|\\\\)?(.*)$").matcher(normFilePath);

			if (matcher.find()){
				path=matcher.group(1);
			}
		}
		return path;
	}
	
	/**
	 * static version of {@link #getRelativePath(File, File)} for tired people.
	 * @param directory
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String staticGetRelativePath(File directory, File file) throws IOException{
		return INSTANCE.getRelativePath(directory, file);
	}
	
	/**
	 * Create a temp directory with basic failure/success checks inside! 
	 * XXX Directory is registered to delete on exit, but emptying it is your responsibility.
	 * @return the directory's file object.
	 * @throws IOException
	 */
	public File createTempDirectory() throws IOException{
		String prefix = "random";
		String suffix = "random";
		
		return createTempDirectory(prefix, suffix);
	}

	/**
	 * Create a temp directory with basic failure/success checks inside! 
	 * XXX Directory is registered to delete on exit, but emptying it is your responsibility.
	 * @param prefix requested beginning of the generated name.
	 * @param suffix requested ending of the generated name.
	 * @return the directory's file object.
	 * @throws IOException
	 */
	public File createTempDirectory(String prefix, String suffix)
			throws IOException {
                LOGGER.debug("Trying to create temporary storage in "+TempDir.getExecutionTempDir());
		File dir = File.createTempFile(prefix, suffix, TempDir.getExecutionTempDir());		
		toTempDirectory(dir);
		return dir;
	}
	
	/**
	 * Converts a temp file to directory with error checking.
	 * @param tempDir
	 * @throws IOException 
	 */
	public void toTempDirectory(File tempDir) throws IOException{
		boolean couldDelete = tempDir.delete();
		boolean couldCreate = tempDir.mkdir();
		
		if (! (couldDelete && couldCreate)){
			throw new IOException("failed to create temporary directory");
		}
	}
	
	/**
	 * Static version of {@link #createTempDirectory()} for tired people...
	 * @return
	 * @throws IOException
	 */
	public static File staticCreateTempDirectory() throws IOException{
		return INSTANCE.createTempDirectory();
	}
	
	public File createTempCopyDestination(File file) throws IOException {
		String fileName=file.getAbsoluteFile().getName();
		return createTempCopyDestination(fileName);
	}

	public File createTempCopyDestination(String fileName) throws IOException {
		File copyFile;
		int extensionIndex=fileName.lastIndexOf('.');
		String prefix;
		String suffix;
		if(extensionIndex>=0){
			prefix=fileName.substring(0,Math.min(20, extensionIndex));
			suffix=fileName.substring(extensionIndex,fileName.length());
		}else{
			prefix=fileName.substring(0,Math.min(20, fileName.length()));
			suffix="temp";
		}
		
		if(prefix.length()<3){
			prefix+=DEFAULT_PREFIX;
		}
		LOGGER.debug("Trying to create temporary storage in "+TempDir.getExecutionTempDir());
		copyFile = File.createTempFile(prefix, suffix, TempDir.getExecutionTempDir());
		return copyFile;
	}
}

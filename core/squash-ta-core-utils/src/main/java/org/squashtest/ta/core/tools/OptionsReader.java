/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Reads options given from a file. 
 * Here an option is a key:value pair. 
 * It's a kind of a Properties file except that all the options are comma-separated and written on the same line, 
 * instead of one per line.</p>
 * 
 *  <p>The options are separated using commas ',' 
 * and within an option the key and value are separated by a column ':'.
 * Trailing spaces will be stripped.</p>
 *  
 *  <p>{@literal Example : " name : bob , buddy : mike , hobby : barfly   " -> { "name=bob"; "buddy=mike"; "hobby=barfly"}}</p>
 *  
 * 
 * 
 * @author bsiri
 *
 */
public abstract class OptionsReader extends org.squashtest.ta.framework.tools.OptionsReader{

	/**
	 * A basic option reader instance with no key filtering if you don't want to define it.
	 */
	public static final OptionsReader BASIC_READER=new OptionsReader(){
		private final Logger logger=LoggerFactory.getLogger(OptionsReader.class.getName()+".BASIC_READER");
		@Override
		protected Set<String> supportedKeys(Map<String, String> source) {return source.keySet();}
		@Override
		protected Logger getLogger() {
			return logger;
		}
	};

        /**
	 * Default option reader (uses commas to separate options)
	 */
	protected OptionsReader(){
            super();
        }
        
        /**
	 * Cusotimzed option reader (parametrize options separator)
	 */
	protected OptionsReader(char optionsSeparator){
            super(optionsSeparator);
        }
        
	/**
	 * Function to filter options, retaining only supported options and warning about unsupported ones.
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public Map<String,String>getFilteredOptions(File file) throws IOException{
		Map<String,String>options=getOptions(file);
		Set<String> unsupported=unSupportedKeys(options);
		options.keySet().removeAll(unsupported);
		Logger logger = getLogger();
		if(!unsupported.isEmpty() && logger.isWarnEnabled()){
			StringBuilder msg=new StringBuilder("Ignoring unsupported options ");
			new ReportBuilderUtils().appendCollectionContentString(msg, unsupported);
			msg.append(" from ").append(file==null?"{null}":file.getAbsolutePath());
			logger.warn(msg.toString());
		}
		return options;
	}
	
}

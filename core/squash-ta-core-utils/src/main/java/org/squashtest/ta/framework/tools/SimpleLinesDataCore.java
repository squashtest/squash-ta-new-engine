/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.framework.tools;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * This class to make {@link SimpleLinesDataImpl} visible to the core tools,
 * while it is a package visible class only (non-API) class in the framework jar.
 * @author edegenetais
 */
public class SimpleLinesDataCore extends SimpleLinesDataImpl{

    public SimpleLinesDataCore(byte[] data) {
        super(data);
    }

    public SimpleLinesDataCore(byte[] data, Charset charset) {
        super(data, charset);
    }

    public SimpleLinesDataCore(URL url) throws IOException {
        super(url);
    }

    public SimpleLinesDataCore(URL url, String encoding) throws IOException {
        super(url, encoding);
    }

    public SimpleLinesDataCore(String path) throws IOException {
        super(path);
    }

    public SimpleLinesDataCore(String path, String encoding) throws IOException {
        super(path, encoding);
    }

    public SimpleLinesDataCore(Iterable<String> data) {
        super(data);
    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import static org.squashtest.ta.core.tools.io.Relativize.RELATIVIZE;

/**
 * Utility class to creaate a zip archive from a file tree. This was created to attach
 * DirectoryResources as result resources without clogging the report attachment list,
 * and make the download easy.
 * @author edegenetais
 */
public class Zipper {
    
    public static final Zipper ZIPPER=new Zipper();
    
    
    public File compress(File root) throws IOException{
        File file=File.createTempFile(root.getName(), ".zip");
        file.deleteOnExit();
        try(ZipOutputStream os=new ZipOutputStream(new FileOutputStream(file));){
            List<File> entries=FILE_TREE.enumerate(root, false);
            for(File entry:entries){
                addZipEntry(entry, os, root);
            }
        }
        return file;
    }
    
    private void addZipEntry(File entry, final ZipOutputStream os, File root) throws IOException {
        final String absolutePath = entry.getAbsolutePath();
        os.putNextEntry(new ZipEntry(RELATIVIZE.relativize(root.getAbsolutePath(), absolutePath)));
        try(FileInputStream fis=new FileInputStream(entry)){
            byte[] buffer=new byte[4096];
            int nb=fis.read(buffer);
            while(nb>=0){
                os.write(buffer, 0, nb);
                nb=fis.read(buffer);
            }
        }
    }
}

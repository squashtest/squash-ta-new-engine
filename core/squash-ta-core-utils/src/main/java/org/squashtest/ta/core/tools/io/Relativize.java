/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.io.IOException;

/**
 * Utility class to extract relative paths.
 * 
 * @author bfranchet
 * 
 */
public class Relativize {
    public static final Relativize RELATIVIZE = new Relativize();

    private Relativize(){}
	/**
	 * This method transform a path into a path relative to a root path. If the paths have nothing in common, then it
	 * return null. If the path are the same, then it returns an empty string.
	 * <ul>
	 * <li>For rootPath = "c:\\root" and pathToTranform = "c:\\root\\todo\\..\\done", returned path is : "todo/../done"</li>
	 * <li>For rootPath = "root" and pathToTranform = "root/todo/done", returned path is : "todo/done"</li>
	 * <li>For rootPath = "/" and pathToTranform = "/root/todo/./done", returned path is : "root/todo/./done"</li>
	 * <li>For rootPath = "/" and pathToTranform = "root/todo/./done", returned path is : null</li>
	 * </ul>
	 * 
	 * @param rootPath
	 *            The root path.
	 * @param pathToTransform
	 *            The path to transform
	 * @return The relative path
	 * @throws IOException
	 *             I/O exception occurs during the processus
	 */
	public String relativize(String rootPath, String pathToTransform) throws IOException {
		StringBuilder relativePath = null;
		// Based on:
		// http://mrpmorris.blogspot.com/2007/05/convert-absolute-path-to-relative-path.html
		String preparedRootPath = rootPath.replaceAll("\\\\", "/");
		String preparedPathToTransform = pathToTransform.replaceAll("\\\\", "/");
		if (!preparedRootPath.equals(preparedPathToTransform)) {
			String[] absoluteDirectories = preparedRootPath.split("/");
			String[] relativeDirectories = preparedPathToTransform.split("/");

			int lastCommonRoot = retrieveLastCommoneRoot(absoluteDirectories, relativeDirectories);

			if (lastCommonRoot != -1) {
				// Build up the relative path
				relativePath = buildPath(lastCommonRoot, absoluteDirectories, relativeDirectories);
			} else if ("/".equals(rootPath) && pathToTransform.startsWith("/")) {
				relativePath = new StringBuilder(pathToTransform.substring(1));
			}
		} else {
			relativePath = new StringBuilder();
		}
		return relativePath == null ? null : relativePath.toString();

	}

	private int retrieveLastCommoneRoot(String[] absoluteDirectories, String[] relativeDirectories) {
		// Get the shortest of the two paths
		int length = absoluteDirectories.length < relativeDirectories.length ? absoluteDirectories.length
				: relativeDirectories.length;

		// Use to determine where in the loop we exited
		int lastCommonRoot = -1;
		int index;

		// Find common root
		for (index = 0; index < length; index++) {
			if (absoluteDirectories[index].equals(relativeDirectories[index])) {
				lastCommonRoot = index;
			} else {
				break;
				// If we didn't find a common prefix then throw
			}
		}
		return lastCommonRoot;
	}

	private StringBuilder buildPath(int lastCommonRoot, String[] absoluteDirectories, String[] relativeDirectories) {
		StringBuilder relativePath = new StringBuilder();
		int index;
		for (index = lastCommonRoot + 1; index < absoluteDirectories.length; index++) {
			if (absoluteDirectories[index].length() > 0) {
				relativePath.append("../");
			}
		}
		for (index = lastCommonRoot + 1; index < relativeDirectories.length - 1; index++) {
			relativePath.append(relativeDirectories[index] + "/");
		}
		relativePath.append(relativeDirectories[relativeDirectories.length - 1]);
		return relativePath;
	}

}

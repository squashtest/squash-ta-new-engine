/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tool to implement storage size formulae.
 * @author edegeenetais
 */
public enum StorageSizeUnit {
    Gb(1024*1024*1024),
    Mb(1024*1024),
    Kb(1024),
    b(1);
    private static final Pattern STORAGE_SIZE_FORMAT = Pattern.compile("^([0-9]+(?:\\.[0-9]*)?)([a-zA-Z]+)$");
    private final long coefficent;
    
    private StorageSizeUnit(long coefficient){
        this.coefficent=coefficient;
    }
    
    public static StorageSizeUnit getSizeUnit(String sizeString){
        final Matcher matcher = STORAGE_SIZE_FORMAT.matcher(sizeString);
        if(matcher.matches()){
            String unitString=matcher.group(2);
            return valueOf(unitString);
        }else{
            throw new StorageSizeFormatException("'"+sizeString+"' does not match the storage size format "+STORAGE_SIZE_FORMAT.pattern());
        }
    }
    
    protected static Number getSizeInUnits(String sizeString){
        final Matcher matcher = STORAGE_SIZE_FORMAT.matcher(sizeString);
        if(matcher.matches()){
            String moduleString=matcher.group(1);
            try{
                return Integer.parseInt(moduleString);
            }catch(NumberFormatException e){
                try{
                    return Double.parseDouble(moduleString);
                }catch(NumberFormatException e1){
                    final StorageSizeFormatException numberFormatException = new StorageSizeFormatException(sizeString+" is not a valid number.");
                    numberFormatException.addSuppressed(e1);
                    numberFormatException.addSuppressed(e);
                    throw numberFormatException;
                }
            }
        }else{
            throw new StorageSizeFormatException("'"+sizeString+"' does not match the storage size format "+STORAGE_SIZE_FORMAT.pattern());
        }
    }
    
    public static long getLongUnitNumber(String sizeString){
        return getSizeInUnits(sizeString).longValue();
    }
    
    public static double getDoubleUnitNumber(String sizeString){
        return getSizeInUnits(sizeString).doubleValue();
    }
    
    public static long getSizeInBytes(String sizeString){
        StorageSizeUnit unit=getSizeUnit(sizeString);
        Number module=getSizeInUnits(sizeString);
        if(module instanceof Double){
            return unit.getSizeInBytes(module.doubleValue());
        }else{
            return unit.getSizeInBytes(module.longValue());
        }
    }
    
    public long getSizeInBytes(double sizeInUnits){
        if(sizeInUnits>Long.MAX_VALUE/coefficent){
            throw new IllegalArgumentException("Overflow: "+sizeInUnits+name()+" exceeds Long.MAXVALUE bytes");
        }
        return (long)(sizeInUnits*coefficent);
    }
    
    public long getSizeInBytes(long sizeInUnits){
        if(sizeInUnits>Long.MAX_VALUE/coefficent){
            throw new IllegalArgumentException("Overflow: "+sizeInUnits+name()+" exceeds Long.MAXVALUE bytes");
        }
        return sizeInUnits*coefficent;
    }
    
    /**
     * Exception thrown when parsing a string that is not a legal storage size representation.
     */
    public static class StorageSizeFormatException extends NumberFormatException{
        public StorageSizeFormatException(String string) {
            super(string);
        }
    }
}

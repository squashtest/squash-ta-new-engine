/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper to the load properties operation.
 * @author edegenetais
 *
 */
public class PropertiesLoader {
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);
	
	/**
	 * Load a properties file.
	 * @param file the source file.
	 * @return a properties object.
	 * @throws IOException in case the file reading goes amiss.
	 */
	public Properties load(File file) throws IOException{
		Properties prop=new Properties();
		load(file, prop);
		return prop;
	}

	/**
	 * Load a properties file.
         * Use this if the properties file encoding is different from the default.
	 * @param file the source file.
         * @param encoding properties file encoding.
	 * @return a properties object.
	 * @throws IOException in case the file reading goes amiss.
	 */
	public Properties load(File file, String encoding) throws IOException{
		Properties prop=new Properties();
		load(file, prop, encoding);
		return prop;
	}

	/**
	 * Update an existing properties file with data from a file.
	 * @param file the source file.
	 * @param prop an existing properties object to update.
	 * @throws IOException in case the file reading goes amiss.
	 */
	public void load(File file, Properties prop) throws IOException {
		BinaryData data=new BinaryData(file);
		ByteArrayInputStream bais=new ByteArrayInputStream(data.toByteArray());
		prop.load(bais);
		bais.close();
	}
	/**
	 * Update an existing properties file with data from a file.
	 * @param file the source file.
	 * @param prop an existing properties object to update.
	 * @param encoding an encoding to enforce
	 * @throws IOException in case the file reading goes amiss.
	 */
	public void load(File file, Properties prop, String encoding) throws IOException {
		BinaryData data=new BinaryData(file);
		ByteArrayInputStream bais=new ByteArrayInputStream(data.toByteArray());
		InputStreamReader r=new InputStreamReader(bais, encoding);
		prop.load(r);
		r.close();
	}
	
	/**
	 * Update a properties object by stripping a suffix from all keys in a given property file and setting the corresponding properties.
	 * @param file file to load
	 * @param prop properties object to update
	 * @param prefix prefix to look for and strip from keys
	 * @throws IOException if file loading fails.
	 */
	public void loadAndStrip(File file, Properties prop, String prefix) throws IOException{
		Properties fileContent=load(file);
		Set<String>longKeySet=fileContent.stringPropertyNames();
		String prefixAndDot = prefix+".";
		for(String longKey:longKeySet){
			if(longKey.startsWith(prefixAndDot)){
				prop.setProperty(longKey.substring(prefix.length()+1), fileContent.getProperty(longKey));
			}else{
				LOGGER.warn("Ignoring key " + longKey + ": missing prefix " + prefixAndDot);
			}
		}
	}
	
	/**
	 * Creating a properties object by stripping a suffix from all keys in a given property file and setting the corresponding properties.
	 * @param file file to load
	 * @param prefix prefix to look for and strip from keys
	 * @throws IOException if file loading fails
	 * @return a properties object where all properties from the file are set, but with stripped keys.
	 */
	public Properties loadAndStrip(File file, String prefix) throws IOException{
		Properties prop=new Properties();
		loadAndStrip(file, prop, prefix);
		return prop;
	}
}

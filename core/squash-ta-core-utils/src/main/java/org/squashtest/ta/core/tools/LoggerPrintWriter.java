/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.tools.TempDir;

/**
 * {@link PrintStream} subclass to direct messages from legacy logging unaware libraries
 * into loggers.
 * 
 * @author ericdegenetais
 */
public class LoggerPrintWriter extends PrintStream {
	private final Logger logger;
        private final Level level;
        /**
         * This is the logging level used by a given {@link LoggerPrintWriter} instance
         * to log message in its destination logger.
         */
        public static enum Level{
            DEBUG{
                @Override
                public void log(Logger target, String message) {
                    target.debug(message);
                }
            }
            ,
            INFO{
                @Override
                public void log(Logger target, String message) {
                    target.info(message);
                }
            },
            WARN{
                @Override
                public void log(Logger target, String message) {
                    target.warn(message);
                }
            },
            ERROR{
                @Override
                public void log(Logger target, String message) {
                    target.error(message);
                }
            }
            ;
            public abstract void log(Logger target,String message);
        }

        /**
         * Creates a {@link PrintStream} that directs its input into a logger.
         * This constructor allows to build a handle with a chosen logging level.
         * @param fileName logger name (also the (dummy) filename transmitted to the PrintWriter.)
         * @param level the logging level is selected according to this parameter.
         * @throws FileNotFoundException should not happen, 
         * as this exception comes from the PrintWriter layer 
         * but we pre-create a file for this.
         */
        public LoggerPrintWriter(String fileName, Level level) throws FileNotFoundException {
            super(filename(fileName));//completely dummy as all output is to be redirected to the logger.
            this.logger = LoggerFactory.getLogger(fileName);
            this.level = level;
        }
        
	/**
	 * Creates a {@link PrintStream} that directs its input into a logger.
         * This default level constructor logs through {@link Logger#debug(java.lang.String) }.
	 * @param fileName logger name (also the (dummy) filename transmitted to the PrintWriter.)
	 * @throws FileNotFoundException should not happen, 
         * as this exception comes from the PrintWriter layer 
         * but we pre-create a file for this.
	 */
	public LoggerPrintWriter(String fileName) throws FileNotFoundException {
		this(fileName,Level.DEBUG);
	}

	/**
	 * Try to create a temporary filename in case some outputs leaks to the PrintStream layer.
	 * @param fileName the requested filename.
	 * @return a temporary file build like this: "filename"+generated name part+".sysout"
	 */
	protected static String filename(String fileName) {
		String effectiveName;
		try {
			File tempFilename=createTempFile(fileName, ".sysout");
			effectiveName=tempFilename.getAbsolutePath();
		} catch (IOException e) {
                        LoggerFactory.getLogger(LoggerPrintWriter.class).debug("Failed to create printwriter file. If somathing leaks, errors may ensue",e);
			effectiveName=fileName;
		}
		return effectiveName;
	}

	private static File createTempFile(String prefix, String suffix) throws IOException {
                LoggerFactory.getLogger(LoggerPrintWriter.class).debug("Trying to create temporary storage in "+TempDir.getExecutionTempDir());
		return File.createTempFile(prefix, suffix, TempDir.getExecutionTempDir());
	}

	@Override
	public void println(String s) {
		level.log(logger, s);
	}
}
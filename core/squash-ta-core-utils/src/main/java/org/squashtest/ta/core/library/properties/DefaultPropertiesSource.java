/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.library.properties;

import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class DefaultPropertiesSource implements PropertiesSource{
	
	private final Properties ppts;
	
	public DefaultPropertiesSource(Properties properties){
		this.ppts=properties;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findKeysMatchingPattern(String regexp) {
		List<String> resultKeys = new LinkedList<String>();

		Enumeration<String> keys = (Enumeration<String>)ppts.propertyNames();
		
		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			if (key.matches(regexp)){
				resultKeys.add(key);
			}
		}
		
		return resultKeys;
	}
	
	@Override
	public String getValue(String key) {
		return ppts.getProperty(key);
	}
}
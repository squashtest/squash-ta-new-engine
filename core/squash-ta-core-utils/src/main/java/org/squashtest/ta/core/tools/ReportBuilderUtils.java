/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class implements various data formatting functionalitites to display data
 * in XML or HTML reports.
 * @author edegenetais
 */
public class ReportBuilderUtils {
	private static final String VALUE_SEPARATOR = ",";
	private static Map<String, String> XML_ESCAPING=new HashMap<String, String>();
        
	static{
		XML_ESCAPING.put("<", "&lt;");
		XML_ESCAPING.put(">", "&gt;");
		XML_ESCAPING.put("\"", "&quot;");
		XML_ESCAPING.put("'", "&apos;");
	}
	/**
	 * Append a collection to a string as the list of string representations (@see {@link Object#toString()}) for its values.
	 * @param builder the target buffer.
	 * @param collection the collection.
	 */
	public void appendCollectionContentString(StringBuilder builder, Collection<?> collection){
		if(!collection.isEmpty()){
			for(Object o:collection){
				builder.append(o.toString()).append(VALUE_SEPARATOR);
			}
			builder.setLength(builder.length()-VALUE_SEPARATOR.length());
		}
	}
	
	/**
	 * Add a trace to contents, escaping some sequence as entities to use in HTML/XML.
	 * @param failure the source of the stacktrace.
	 * @return the stack trace as content string.
	 */
	public String printTraceToContent(Throwable failure) {
		return printTraceToContentInternal(failure).toString();
	}

	/**
	 * Add a trace to contents, escaping some sequence as entities to use in HTML/XML.
	 * @param failure the source of the stacktrace.
	 * @param target target buffer to put the trace content into.
	 * @throws IOException if the Appendable is based on a stream and an I/O error occurs on that receiving stream.
	 */
	public void printTraceToContent(Throwable failure, Appendable target) throws IOException {
		target.append(printTraceToContentInternal(failure));
	}
	
	/**
	 * Add a trace to contents, escaping some sequence as entities to use in HTML/XML.
	 * @param failure the source of the stacktrace.
	 * @param target target buffer to put the trace content into.
	 */
	public void printTraceToContent(Throwable failure, StringBuffer target){
		target.append(printTraceToContentInternal(failure));
	}
	
	/**
	 * Add a trace to contents, escaping some sequence as entities to use in HTML/XML.
	 * @param failure the source of the stacktrace.
	 * @param target target buffer to put the trace content into.
	 */
	public void printTraceToContent(Throwable failure, StringBuilder target){
		target.append(printTraceToContentInternal(failure));
	}
		
	public StringBuilder printTraceToContentInternal(Throwable failure) {
		StringWriter stackTrace=new StringWriter();
		failure.printStackTrace(new PrintWriter(stackTrace));
		
		StringBuilder buffer=new StringBuilder(stackTrace.getBuffer());
		
		escapeForXml(buffer);
		return buffer;
	}
	
	public void escapeForXml(StringBuilder target){
		replaceAll(target, "&","&amp;");//must be down before injecting entities!
		replaceAll(target, XML_ESCAPING);
	}
	
	public String escapeForXml(String targetContent){
		StringBuilder buffer=new StringBuilder(targetContent);
		escapeForXml(buffer);
		return buffer.toString();
	}
	
	public String replaceAll(String targetString,Map<String,String>mappings){
		StringBuilder buffer=new StringBuilder(targetString);
		replaceAll(buffer,mappings);
		return buffer.toString();
	}

	public void replaceAll(StringBuilder targetContent,Map<String, String> mappings) {
		for(Entry<String, String> mapping:mappings.entrySet()){
			replaceAll(targetContent, mapping.getKey(), mapping.getValue());
		}
	}
	
	public void replaceAll(StringBuilder buffer, String target,
			String replacement) {
		int currentIndex=buffer.indexOf(target);
		while(currentIndex>=0){
			buffer.replace(currentIndex, currentIndex+target.length(), replacement);
			currentIndex=buffer.indexOf(target,currentIndex+replacement.length());
		}
	}
}

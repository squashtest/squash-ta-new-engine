/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.tools.TempDir;

/**
 * This class deals with the creation (and deletion) of all the temporary files (cf Mantis 2080) This is a low-level
 * class, which mainly builds the directory and gives them to framework TempDir class.
 * 
 * It contains only static methods.
 * 
 * @author cruhlmann
 */
public final class TempFileUtils {
    
        private static final String INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE = "The init method SHOULD have been called before using this!";
        
        //FIXME this is a temporary FIX pending re-design of this steaming stack of indetermination and bug-mongering ** urgh !
        private static boolean waiting4init=true;
        //FIXME static variables trigger extremely nasty bugs and should be avoided at all cost!
	private static String timestamp;
        //FIXME static variables trigger extremely nasty bugs and should be avoided at all cost!
	private static String executionName;
        //FIXME static variables trigger extremely nasty bugs and should be avoided at all cost!
	private static String ecosystemName;
        //FIXME static variables trigger extremely nasty bugs and should be avoided at all cost!
	private static String testName;
        //FIXME static variables trigger extremely nasty bugs and should be avoided at all cost!
	private static boolean debugMode;
        //FIXME static variables trigger extremely nasty bugs and should be avoided at all cost!
	private static Logger logger;

	/**
	 * private constructor
	 */
	private TempFileUtils() {
	}

	/**
	 * Initialize the main temp directory and its parameters
	 * 
	 * @param tempDirectory
	 *            The directory where the temp file should be created (default value: default system temp directory)
	 * @param debug
	 *            - false if the file should be deleted after the execution, true otherwise (default value: false)
	 */
	public static void init(String tempDirectory, String debug) throws IOException {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                waiting4init=false;
            
		// we need this instantiation to be made AFTER the log4j init in AbstractSquashTAMojo
                //FIXME should be related to an known anomaly, be cause mere use of a logger dos NOT override log4j (re)initialization, so AFAIK we could remove this
		TempFileUtils.logger = LoggerFactory.getLogger(TempFileUtils.class);

		/* we fix the date once and for all for the timestamp
                       ^
                       |
                      sic (sick ?)      I dunno if 'we' 'fix' the date, but this DOES BREAK tempfile logic : 
                   YES SEVERAL BUILDS MAY HAPPEN TO START IN THE SAME MILLISECOND, DAMMIT !!!!!!!!
                */
		String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS").format(new Date());

		// we determine if we are in debug mode
		TempFileUtils.debugMode = false;
		if (debug != null) {
			if ("true".equals(debug.toLowerCase().trim())) {
				TempFileUtils.debugMode = true;
			} else if (!"false".equals(debug.toLowerCase().trim())) {
				TempFileUtils.logger.error(debug
						+ " is not a correct value for ta.debug.mode property. Correct values are 'true' and 'false'.");
			}
		}

		// we create the main directory
		if (tempDirectory != null) {
                        TempFileUtils.logger.debug("Setting main temporary storage area to "+tempDirectory);
			TempDir.setMainTempDir(tempDirectory);
		}
                File mainTempDir=TempDir.getMainTempDir();
                Path mainTempPath=Paths.get(mainTempDir.toURI());
                Path instanceTempDir=Files.createTempDirectory(mainTempPath, timestamp);
                
                //NOW, we have a usable tempdir name, because the OS vouches for its uniqueness
		TempFileUtils.timestamp = instanceTempDir.getFileName().toString();
                File mainTestDir = new File (TempDir.getMainTempDir(), TempFileUtils.timestamp);
                TempDir.setTestMainTempDir(mainTestDir);
                
		// ... and then the macro directory
                File dir=createDirectory(TempDir.MACRO_DIR_NAME);
                TempFileUtils.logger.debug("Trying to create temporary storage in "+dir.getPath());
		TempDir.setMacroTempDir(dir);
	}

	/**
	 * This method stores the actual execution name, in order to use it after in creating directories
	 * 
	 * @param executionName
	 *            The name of the actual execution name
	 */
	public static void setExecutionName(String executionName) {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                if(waiting4init){
                    throw new IllegalStateException(INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE);
                }
		TempFileUtils.executionName = executionName;
	}

	/**
	 * This method stores the actual ecosystem name, in order to use it after in creating directories
	 * 
	 * @param ecosystemName
	 *            The name of the actual ecosystem name
	 */
	public static void setEcosystemName(String ecosystemName) {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                if(waiting4init){
                    throw new IllegalStateException(INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE);
                }
		TempFileUtils.ecosystemName = ecosystemName;
	}

	/**
	 * This method stores the actual test name, create a directory in the form
	 * timestamp/{executionName}/{ecosystemName}/{testName} and gives it to the framework TempDir class
	 * 
	 * @param testName
	 *            The name of the actual test name
	 */
	public static void setTestName(String testName) {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                if(waiting4init){
                    throw new IllegalStateException(INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE);
                }
		TempFileUtils.testName = testName;
                final File dir = createDirectory();
                TempFileUtils.logger.debug("Trying to create execution temporary storage in "+dir.getPath());
		TempDir.setExecutionTempDir(dir);
	}

	/**
	 * Static method used to clean the temp directory after the test suite execution
	 */
	public static void cleanUpTempDirectory() {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                if(waiting4init){
                    throw new IllegalStateException(INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE);
                }
		File directory = TempDir.getTestMainTempDir();
           
		if (TempFileUtils.debugMode) {
			TempFileUtils.logger
					.info("You can access to the temporary files created in " + directory.getAbsolutePath());
		} else {
			boolean deleted = FileUtils.deleteQuietly(directory);
			if (deleted) {
				TempFileUtils.logger.info("All the files from " + directory.getAbsolutePath()
						+ " were properly deleted.");
			} else {
				TempFileUtils.logger.warn("All the files from " + directory.getAbsolutePath()
						+ " could not be deleted.");
			}
		}
	}

	/****************** PRIVATE PART **************************/

	private static File createDirectory() {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                if(waiting4init){
                    throw new IllegalStateException(INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE);
                }
                
		if (TempFileUtils.executionName == null || TempFileUtils.ecosystemName == null
				|| TempFileUtils.testName == null) {
			// if the three mandatory parts of the file tree are not complete, we build a default directory
			return TempDir.getDefaultTempDir();
		}
		// else, we build a directory on the form executionName_timestamp/ecosystemName/testName
		StringBuilder path = new StringBuilder(TempDir.getMainTempDir().getAbsolutePath());
		path.append(File.separatorChar);
		path.append(TempFileUtils.timestamp);
		path.append(File.separatorChar);
		path.append(TempFileUtils.executionName);
		path.append(File.separatorChar);
		path.append(TempFileUtils.ecosystemName);
		path.append(File.separatorChar);
		path.append(TempFileUtils.testName);
		return new File(path.toString());
	}

	private static File createDirectory(String directoryName) {
                // until we redesign this whole ah ... thingy, let's try to at least secure it a bit !
                if(waiting4init){
                    throw new IllegalStateException(INIT_METHOD_SHOULD_HAVE_BEEN_CALLED_BEFORE_MESSAGE);
                }
                
		StringBuilder path = new StringBuilder(TempDir.getMainTempDir().getAbsolutePath());
		path.append(File.separatorChar);
		path.append(TempFileUtils.timestamp);
		path.append(File.separatorChar);
		path.append(directoryName);
		return new File(path.toString());
	}
}

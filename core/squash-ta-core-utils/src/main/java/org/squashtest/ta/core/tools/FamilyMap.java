/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Map from name to value lists. 
 * This is NOT thread safe, please synchronize on it if you want to share it between threads. 
 * @author edegenetais
 * @param <Key> type of the keys.
 * @param <Value> type of the value list elements.
 */
public class FamilyMap<Key,Value> {
	private Map<Key,List<Value>> delegate=new HashMap<Key, List<Value>>();
	
	/**
	 * Add a mapping to the families.
	 * @param key the key to map the value to.
	 * @param value the value to map
	 */
	public void put(Key key, Value value){
		List<Value> valueList = getOrCreateValueList(key);
		
		valueList.add(value);
	}

	private List<Value> getOrCreateValueList(Key key) {
		List<Value> valueList=delegate.get(key);
		if(valueList==null){
			valueList=new ArrayList<Value>();
			delegate.put(key, valueList);
		}
		return valueList;
	}
	
	public void putFamily(Key key, Collection<Value> values){
		List<Value> valueList=getOrCreateValueList(key);
		valueList.addAll(values);
	}
	
	/**
	 * Adds all the {@literal key-->value} mappings to the {@link FamilyMap}.
	 * @param mappings the mappings to add.
	 */
	public void putAll(Map<Key, Value> mappings){
		for(Entry<Key, Value> entry:mappings.entrySet()){
			put(entry.getKey(),entry.getValue());
		}
	}
	
        /**
         * Adds all the {@literal key-->value family} mappings to the {@link FamilyMap}.
         * @param mappings 
         */
	public void putAll(FamilyMap<Key, Value>mappings){
		for(Entry<Key, List<Value>> entry:mappings.delegate.entrySet()){
			putFamily(entry.getKey(),entry.getValue());
		}
	}
	
	/**
	 * Returns the {@link Set} of all defined keys as in {@link Map#keySet()}.
	 * 
	 * @return the {@link Set} of keys. As it is backed by the data collection,
	 *         removing a key from this set unmaps the corresponding value
	 *         family.
	 */
	public Set<Key> keySet(){
		return delegate.keySet();
	}
	
	/**
	 * Gets a set containing once each value mapped to at least one key.
	 * This set is independant from the backing data, and its changes are NOT mirrored by the original {@link FamilyMap}.
	 * @return the {@link Set}
	 */
	public Set<Value> getValueSet(){
		Set<Value> valueSet=new HashSet<Value>();
		for(List<Value> family:delegate.values()){
			valueSet.addAll(family);
		}
		return valueSet;
	}
	
	/**
	 * Retrieves the value family mapped to the given key.
	 * 
	 * @param key
	 *            the desired key.
	 * @return the {@link List} containing the values of the family. Please note
	 *         that it is backed by the data, so that modifications in this list
	 *         are reflected in the original data (mappings and unmappings).
	 */
	public List<Value> getFamily(Key key){
		return delegate.get(key);
	}
}

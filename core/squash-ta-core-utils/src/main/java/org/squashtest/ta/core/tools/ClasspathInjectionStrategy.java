/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.core.tools;

import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * Interface of the strategy family used to inject jars into the classpath at runtime.
 * @author edegenetais
 */
public interface ClasspathInjectionStrategy {
    /**
     * This methods attempts to detect the requested entries in the classloader's classpath,
     * and if they are not detected, tries to inject it.
     * Strategy implementations should, as far as possible,  strive to check wether their method applies,
     * and leave the classloader alone if it doesn't.
     * They are also expected to return <code>true</code> without changing the classpath if the entries are already here.
     * @param cl the target classloader instance.
     * @param domainName dispoay name of the functionalities injected through the new entries.
     * @param jarPathTable all entries to inject, as path strings.
     * @return <code>true</code> if the classpath entries have been successfully detected or injected, <code>false</code> otherwise.
     * @throws BrokenTestException 
     */
    boolean tryInjectionStrategy(ClassLoader cl, String domainName, String... jarPathTable) throws BrokenTestException;
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library

import java.io.InputStream;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorSpockBase;
import org.squashtest.ta.framework.exception.BadDataException;

/**
 * @author ericdegenetais
 */
class OutputAnalyzerTest extends ResourceExtractorSpockBase{
    private OutputAnalyzer testee=new OutputAnalyzer();
    
    def "status should be success if successfull test"(){
        given:
            InputStream successCTIS=openResourceStream("success-output.xml")
        when:
            def status=testee.getTestStatus(successCTIS);
        then:
            status==OutputAnalyzer.Status.PASS
    }
    
    def "status should be fail if failed test"(){
        given:
            InputStream successCTIS=openResourceStream("fail-output.xml")
        when:
            def status=testee.getTestStatus(successCTIS);
        then:
            status==OutputAnalyzer.Status.FAIL
    }
    
    def "should properly extract test failure message"(){
        given:
            InputStream successCTIS=openResourceStream("fail-output.xml")
        when:
            def message=testee.getLastFailureMessage(successCTIS)
        then:
            message=="No ! Dont wanna Go to bed"
    }
    
    def "should throw if several tests in scope"(){
        given:
            InputStream successCTIS=openResourceStream("several-output.xml")
        when:
            def status=testee.getTestStatus(successCTIS);
        then:
            thrown(BadDataException)
    }
    
    def "should list one suite from one-suite dryrun all ok"(){
        given:
            InputStream successCTIS=openResourceStream("onesuite-dr-output.xml")
        when:
            def suiteList=testee.getTestSuites(successCTIS)
        then:
            suiteList.size()==1
            suiteList.get(0).getName()=="Keyword Driven"
    }
    
    def "should list two suites from two-suite dryrun all ok"(){
        given:
            InputStream successCTIS=openResourceStream("twosuite-dr-output.xml")
        when:
            def suiteList=testee.getTestSuites(successCTIS)
        then:
            suiteList.size()==2
            Set<String> suiteNames=new HashSet<>();
            for(RobotFWTestSuite suite:suiteList){
                suiteNames.add(suite.getName())
            }
            suiteNames==new HashSet<>(["Keyword Driven","Other Keyword Driven"])
    }
    
    
    def "should list all expected tests from one suite dryrun"(){
        given:
            InputStream successCTIS=openResourceStream("onesuite-dr-output.xml")
        when:
            def suiteList=testee.getTestSuites(successCTIS)
        then:
            int nbTest=0;
            Set<String> testNames=new HashSet<>();
            for(RobotFWTestSuite suite:suiteList){
                for(RobotFWTest test:suite.getTests()){
                    nbTest++;
                    testNames.add(test.getName())
                }
            }
            nbTest==2
            testNames==new HashSet<>(["That one works","That one fails"]);
    }
    
    def "should list all expected tests from two suite dryrun"(){
        given:
            InputStream successCTIS=openResourceStream("twosuite-dr-output.xml")
        when:
            def suiteList=testee.getTestSuites(successCTIS)
        then:
            int nbTest=0;
            Set<String> testNames=new HashSet<>();
            for(RobotFWTestSuite suite:suiteList){
                for(RobotFWTest test:suite.getTests()){
                    nbTest++;
                    testNames.add(test.getName())
                }
            }
            nbTest==4
            testNames==new HashSet<>(["That one works","That one fails","Wanna test keyword","Another broken feature"]);
    }
    
    def "should list all expected tests from two suite missingKW dryrun"(){
        given:
            InputStream successCTIS=openResourceStream("twosuite-missingkw-dr-output.xml")
        when:
            def suiteList=testee.getTestSuites(successCTIS)
        then:
            int nbTest=0;
            Set<String> testNames=new HashSet<>();
            for(RobotFWTestSuite suite:suiteList){
                for(RobotFWTest test:suite.getTests()){
                    nbTest++;
                    testNames.add(test.getName())
                }
            }
            nbTest==4
            testNames==new HashSet<>(["That one works","That one fails","Wanna test keyword","Another broken feature"]);
    }
    
    def "should list all expected tests from multilevel project dryrun"(){
        given:
            InputStream successCTIS=openResourceStream("multilevel-dr-output.xml")
        when:
            def suiteList=testee.getTestSuites(successCTIS)
        then:
            int nbTest=0;
            Set<String> testNames=new HashSet<>();
            for(RobotFWTestSuite suite:suiteList){
                for(RobotFWTest test:suite.getTests()){
                    nbTest++;
                    testNames.add(test.getName())
                }
            }
            nbTest==4
            testNames==new HashSet<>(["That one works","That one fails","Wanna test keyword","Another broken feature"]);
    }
    
}


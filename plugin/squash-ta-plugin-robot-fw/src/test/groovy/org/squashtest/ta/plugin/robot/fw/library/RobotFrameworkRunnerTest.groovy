/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library

import org.squashtest.ta.api.test.toolkit.ResourceExtractorSpockBase;
import org.squashtest.ta.local.process.library.process.LocalProcessClient
import org.squashtest.ta.local.process.library.shell.ShellResult
import org.squashtest.ta.xml.functions.library.Transform
import java.util.regex.Pattern
import org.slf4j.LoggerFactory

/**
 *
 * @author ericdegenetais
 */
class RobotFrameworkRunnerTest extends ResourceExtractorSpockBase{
    
    File robotWorkDir;
    RobotFrameworkRunner.LocalProcessClientFactory clientFactory;
    LocalProcessClient processClientMock;
    
    def setup(){
        robotWorkDir=createNtrackDir()
        
    }
    
    def setRunnerExpectedInteractions(String commandRegex, int exitCode){
       
        LoggerFactory.getLogger(RobotFrameworkRunnerTest.class).debug("Expected command regex is \n{}\n",commandRegex)
        
        processClientMock=Mock(LocalProcessClient)

        0 * processClientMock.runLocalProcessCommand({!it.matches(commandRegex)},_,_,_) >> new ShellResult(exitCode,"","",commandRegex)
        1 * processClientMock.runLocalProcessCommand({it.matches(commandRegex)},_,_,_) >> new ShellResult(exitCode,"","",commandRegex)
        
        clientFactory=Mock(RobotFrameworkRunner.LocalProcessClientFactory)
        clientFactory.getClient() >> processClientMock
        
        return new RobotFrameworkRunner(clientFactory,robotWorkDir,new Properties(),new Properties())
    }
    
    def "should actually create result directory"(){
        given:
            def testee=setRunnerExpectedInteractions(".*",0)
        when:
            def result=testee.runSingleTest("Known Suite","That one works")
        then:
            result.getResultDir().exists()
    }
    
    def protectCanonicalPathToAvoidPenguinKillingWindozeries(File path){
        return Pattern.quote(path.getCanonicalPath())
    }
    
    def "should launch the right test"(){
        given:
            def testName="That One Works"
            def suiteName="SuiteName"
        and:
            def regex="python -m robot --test \""+testName+"\" --suite \"/.SuiteName\" --name \"/\" --outputdir \".*\" \""+protectCanonicalPathToAvoidPenguinKillingWindozeries(robotWorkDir)+"\""
        and:
            def testee=setRunnerExpectedInteractions(regex,0)
        when:
            def result=testee.runSingleTest(suiteName,testName)
        then:
            true
    }
    
    def "Failed test does not throw"(){
        given:
            def testee=setRunnerExpectedInteractions(".*",1)
        when:
            def result=testee.runSingleTest("SuiteName","That one fails")
        then:
            true
    }
    
    def "If robotFramework reports anomaly different from failed test then throw."(){
        given:
            def testee=setRunnerExpectedInteractions(".*",252)
        when:
            def result=testee.runSingleTest("UnknownSuite","Dunno that one")
        then:
            thrown(RobotFrameworkExecutionException)
    }
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library

import org.squashtest.ta.api.test.toolkit.ResourceExtractorSpockBase;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.SimpleLinesData
import org.squashtest.ta.plugin.commons.library.param.date.DateReplacer

import java.io.File;

/**
 * 
 * @author ericdegenetais
 */
class PythonTFParmsCodecTest extends ResourceExtractorSpockBase{
    
    def "should write empty pair with both sections"(){
        given:
            PythonTFParmsCodec testee=new PythonTFParmsCodec()
        and:
            File destination=createNtrackFile()
        and:
            Properties global=new Properties()
            Properties test=new Properties()
        and:
            File expectedResult=createFile("empty.python.tst")
        when:
            testee.write(global,test,destination)
        then:
            checkActualContentAgainstExpected(destination,expectedResult)
    }
    
    def "should write properly if only test has data"(){
        given:
            PythonTFParmsCodec testee=new PythonTFParmsCodec()
        and:
            File destination=createNtrackFile()
        and:
            Properties global=new Properties()
            Properties test=new Properties()
            test.putAll(["tc1":"val1","TC1":"otherVal1","tc2":"val2"])
        and:
            File expectedResult=createFile("testparms.only.python.tst")
        when:
            testee.write(global,test,destination)
        then:
            SimpleLinesData actualSLD=new SimpleLinesData(destination.toURI().toURL())
            SimpleLinesData expectedSLD=new SimpleLinesData(expectedResult.toURI().toURL())
            //First, test for what's in the global section
            Iterator<String> actualIt=actualSLD.getLines().iterator()
            Iterator<String> expectedIt=expectedSLD.getLines().iterator()
            
            String reportOfGlobalSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,"[test]")
            reportOfGlobalSection=="Missing: []\nUnexpected: []"
            
            String reportOfTestSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,null)
            reportOfTestSection=="Missing: []\nUnexpected: []"
    }
    
    def "should write properly if only global has data"(){
        given:
            PythonTFParmsCodec testee=new PythonTFParmsCodec()
        and:
            File destination=createNtrackFile()
        and:
            Properties global=new Properties()
            Properties test=new Properties()
            global.putAll(["glob1":"val1","TC1":"defaultOfDaDeath","glob2":"val2"])
        and:
            File expectedResult=createFile("globparms.only.python.tst")
        when:
            testee.write(global,test,destination)
        then:
            SimpleLinesData actualSLD=new SimpleLinesData(destination.toURI().toURL())
            SimpleLinesData expectedSLD=new SimpleLinesData(expectedResult.toURI().toURL())
            //First, test for what's in the global section
            Iterator<String> actualIt=actualSLD.getLines().iterator()
            Iterator<String> expectedIt=expectedSLD.getLines().iterator()
            
            String reportOfGlobalSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,"[test]")
            reportOfGlobalSection=="Missing: []\nUnexpected: []"
            
            String reportOfTestSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,null)
            reportOfTestSection=="Missing: []\nUnexpected: []"
    }
    
    def "should write properly if both parameter sets hold data"(){
        given:
            PythonTFParmsCodec testee=new PythonTFParmsCodec()
        and:
            File destination=createNtrackFile()
        and:
            Properties global=new Properties()
            global.putAll(["glob1":"val1","TC1":"defaultOfDaDeath","glob2":"val2"])
            Properties test=new Properties()
            test.putAll(["tc1":"val1","TC1":"otherVal1","tc2":"val2"])
        and:
            File expectedResult=createFile("globalparms.testparms.python.tst")
        when:
            testee.write(global,test,destination)
        then:
            SimpleLinesData actualSLD=new SimpleLinesData(destination.toURI().toURL())
            SimpleLinesData expectedSLD=new SimpleLinesData(expectedResult.toURI().toURL())
            //First, test for what's in the global section
            Iterator<String> actualIt=actualSLD.getLines().iterator()
            Iterator<String> expectedIt=expectedSLD.getLines().iterator()
            
            String reportOfGlobalSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,"[test]")
            reportOfGlobalSection=="Missing: []\nUnexpected: []"
            
            String reportOfTestSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,null)
            reportOfTestSection=="Missing: []\nUnexpected: []"
    }
    
    def "should not escape colons, as they have no special meaning in the target python format"(){
        given:
            PythonTFParmsCodec testee=new PythonTFParmsCodec()
        and:
            File destination=createNtrackFile()
        and:
            Properties global=new Properties()
            global.putAll(["glob1":"val1","TC1":"defaultOfDaDeath!","glob2":"val=2"])
            Properties test=new Properties()
            test.putAll(["tc1":"val1","TC1":"other:Val1","tc2":"val2"])
        and:
            File expectedResult=createFile("escapes.testparms.python.tst")
        when:
            testee.write(global,test,destination)
        then:
            SimpleLinesData actualSLD=new SimpleLinesData(destination.toURI().toURL())
            SimpleLinesData expectedSLD=new SimpleLinesData(expectedResult.toURI().toURL())
            //First, test for what's in the global section
            Iterator<String> actualIt=actualSLD.getLines().iterator()
            Iterator<String> expectedIt=expectedSLD.getLines().iterator()
            
            String reportOfGlobalSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,"[test]")
            reportOfGlobalSection=="Missing: []\nUnexpected: []"
            
            String reportOfTestSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,null)
            reportOfTestSection=="Missing: []\nUnexpected: []"
    }
    
    def "should write proper utf8 rendition of parameters, as seen with french's e with acute accent"(){
        given:
            PythonTFParmsCodec testee=new PythonTFParmsCodec()
        and:
            File destination=createNtrackFile()
        and:
            Properties global=new Properties()
            Properties test=new Properties()
            test.putAll(["tc1":"val1","TC1":"do you speak martien, ohé la soucoupe?","tc2":"val2"])
        and:
            File expectedResult=createFile("charspec.uencode.only.python.tst")
        when:
            testee.write(global,test,destination)
        then:
            SimpleLinesData actualSLD=new SimpleLinesData(destination.toURI().toURL())
            SimpleLinesData expectedSLD=new SimpleLinesData(expectedResult.toURI().toURL())
            //First, test for what's in the global section
            Iterator<String> actualIt=actualSLD.getLines().iterator()
            Iterator<String> expectedIt=expectedSLD.getLines().iterator()
            
            String reportOfGlobalSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,"[test]")
            reportOfGlobalSection=="Missing: []\nUnexpected: []"
            
            String reportOfTestSection=buildMissingAndUnexpectedForSection(actualIt,expectedIt,null)
            reportOfTestSection=="Missing: []\nUnexpected: []"
    }
    
    //we suppose we never get two identical lines
    def buildMissingAndUnexpectedForSection(Iterator<String> actualIt,Iterator<String> expectedIt,String sectionEndLine){
        boolean actualItHasNext=actualIt.hasNext()
        boolean expectedItHasNext=expectedIt.hasNext()
        
        String actualCurrentLine=actualIt.next()
        String expectedCurrentLine=expectedIt.next()
        
        Set<String> expectedData=new HashSet<>()
        while(expectedItHasNext && expectedCurrentLine!=sectionEndLine){
            expectedData.add(expectedCurrentLine)
            if(expectedIt.hasNext()){
                expectedCurrentLine=expectedIt.next()
            }else{
                expectedItHasNext=false
            }
        }
        System.out.println("Expected data : "+expectedData)

        Set<String> actualData=new HashSet<>()
        while(actualItHasNext && actualCurrentLine!=sectionEndLine){
            actualData.add(actualCurrentLine)
            
            if(actualIt.hasNext()){
                actualCurrentLine=actualIt.next()
            }else{
                actualItHasNext=false
            }

        }
        System.out.println("Actual data : "+actualData)
        
        String report="Missing: []\nUnexpected: []"
        if(!actualData.equals(expectedData)){
            System.out.println("Oops : pas pareil !")
            Set<String> missing=new HashSet<>(expectedData)
            missing.removeAll(actualData)
            report="Missing: "+missing+"\n"
            Set<String> unexpected=new HashSet<>(actualData)
            unexpected.removeAll(expectedData)
            report=report+"Unexpected: "+unexpected
            System.out.println("Report : \n"+report)
        }
        return report
    }
}

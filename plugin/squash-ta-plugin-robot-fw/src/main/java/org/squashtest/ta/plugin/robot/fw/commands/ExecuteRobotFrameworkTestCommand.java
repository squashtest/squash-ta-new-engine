/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.commands;

import java.util.Collection;
import java.util.Properties;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWResult;
import org.squashtest.ta.plugin.robot.fw.library.RobotFrameworkRunner;
import org.squashtest.ta.plugin.robot.fw.resources.RobotFWProjectResource;
import org.squashtest.ta.plugin.robot.fw.resources.RobotFWResultResource;

/**
 * Command component to launch a robotframework test.
 * @author edegenetais
 */
@TACommand("execute")
public class ExecuteRobotFrameworkTestCommand implements Command<RobotFWProjectResource, VoidTarget>{
    
    private String testName;
    private String suiteName;
    private RobotFWProjectResource project;
    private Properties testParms;
    
    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        final ConfigurationExtractor configurationExtractor = new ConfigurationExtractor(this);
        ConfigurationExtractor.ExtractionResult<Properties> propertiesExtraction=configurationExtractor.extractOptionalPropertiesResourceFromConfiguration(testParms, configuration);
        testParms=propertiesExtraction.getExtractedData();
        testName=configurationExtractor.extractOptionValue(propertiesExtraction.getRemaningConfiguration(), "testName");
        suiteName=configurationExtractor.extractOptionValue(propertiesExtraction.getRemaningConfiguration(), "suiteName");
        LoggerFactory.getLogger(ExecuteRobotFrameworkTestCommand.class).debug("Target Robotframework test name : {}",testName);
    }

    @Override
    public void setTarget(VoidTarget target) {
        //noop : the void target is a place holder, we have nothing to deal with here.
    }

    @Override
    public void setResource(RobotFWProjectResource resource) {
        this.project=resource;
    }

    @Override
    public RobotFWResultResource apply() {
        Properties projectGlobalParms=project.getGlobalParms();
        RobotFrameworkRunner runner=new RobotFrameworkRunner(project.getProjectBase(),projectGlobalParms,testParms);
        RobotFWResult result=runner.runSingleTest(suiteName,testName);
        return new RobotFWResultResource(result);
    }

    @Override
    public void cleanUp() {
        //noop - yet !
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.BadDataException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author edegenetais
 */
public class OutputAnalyzer {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutputAnalyzer.class);
    private static final String TEST_STATUS_EXTRACTOR_XPATH = "//test/status/@status";
    private static final String LAST_FAILURE_MSG_EXTRACTOR_XPATH = "//msg[@level='FAIL']/text()[last()]";
    
    // We won't share it, because it is not thread-safe
    private final XPath xpath = XPathFactory.newInstance().newXPath();
    private final XPathExpression testStatusExtractor;
    private final XPathExpression lastFailureMessageExtractor;
    private final XPathExpression testSuiteExtractor;
    
    public OutputAnalyzer() throws XPathExpressionException {
        testSuiteExtractor = xpath.compile("/robot/suite/suite");
        this.testStatusExtractor = xpath.compile(TEST_STATUS_EXTRACTOR_XPATH);
        this.lastFailureMessageExtractor = xpath.compile(LAST_FAILURE_MSG_EXTRACTOR_XPATH);
    }
    
    
    public static enum Status{
        PASS,FAIL
    }
    
    public List<RobotFWTestSuite> getTestSuites(final InputStream xmlDescriptorInputStream) throws XPathExpressionException{
        final List<RobotFWTestSuite> suiteList = new LinkedList<>();
        NodeList suites=(NodeList)testSuiteExtractor.evaluate(new InputSource(xmlDescriptorInputStream), XPathConstants.NODESET);
        for(int i=0;i<suites.getLength();i++){
            Node suite=suites.item(i);
            suiteList.addAll(extractSuitesFromTopLevelNode(suite));
        }
        return suiteList;
    }

    private List<RobotFWTestSuite> extractSuitesFromTopLevelNode(Node suiteNode) {
        return extractSuitesFromNode(suiteNode, "");
    }

    private List<RobotFWTestSuite> extractSuitesFromNode(Node suiteNode, String namePrefix) {
        String suiteName=suiteNode.getAttributes().getNamedItem("name").getNodeValue();
        File suiteSource=new File(suiteNode.getAttributes().getNamedItem("source").getNodeValue());
        List<RobotFWTestSuite> suiteList=new ArrayList<>();
        List<RobotFWTest> testList=new LinkedList<>();
        final NodeList suiteChildren = suiteNode.getChildNodes();
        
        for(int t=0;t<suiteChildren.getLength();t++){
            Node child=suiteChildren.item(t);
            final String nodeName = child.getNodeName();
            switch(nodeName){
                case "test":
                    String testName=child.getAttributes().getNamedItem("name").getNodeValue();
                    RobotFWTest test = checkMetadataAndLoadContent(child, testName);
                    testList.add(test);
                    break;
                case "suite":
                    suiteList.addAll(extractSuitesFromNode(child, namePrefix+suiteName+"/"));
                    break;
                default:
                    LOGGER.debug("Skipping {}, we don't need its contents.",nodeName);
            }
        }
        
        if(testList.isEmpty()){
            LOGGER.debug("Skipped test suite without test (probably a file tree level)");
        }else{
            suiteList.add(new RobotFWTestSuite(namePrefix+suiteName, suiteSource,testList));
        }
        return suiteList;
    }
    
    private RobotFWTest checkMetadataAndLoadContent(Node child, String testName) {
        final NodeList testChildren = child.getChildNodes();
        RobotFWTest test = new RobotFWTest(testName);
        
        for (int t = 0; t < testChildren.getLength(); t++) {
            Node testChild = testChildren.item(t);
            final String nodeName = testChild.getNodeName();
  
            if ("tags".equals(nodeName)){                
                    test = extractMetadataFromTags(testChild, testName);
            } else {
                LOGGER.debug("This test has no metadata."); 
            }
        }
        return test;
    }

    private RobotFWTest extractMetadataFromTags(Node testChild, String testName) {
        
        RobotFWTest test;
        final NodeList metadataTags = testChild.getChildNodes();
        
        //We want to retrieve all tag values from output.xml and put them in a list.
        List<String> rawMetadataList = getRawMetadataFromTags(metadataTags);
        
        /*We use the RobotFWMetadataExtractorFromTags to check if the tag is a tf metadata (prefixed with "tf:")
        and then extract key and value(s) for each metadata and return a list of all the metadata. */
        RobotFWMetadataExtractorFromTags extractor = new RobotFWMetadataExtractorFromTags();
        
        Map<String, List<String>> metadataList = extractor.extractMetadataFromTags(rawMetadataList);
        List<RobotFWTestAnomaly> testAnomalies = extractor.getTestAnomalies();
        
        test = new RobotFWTest(testName,metadataList,testAnomalies);
        
        return test;
    }
    
    private List<String> getRawMetadataFromTags(NodeList metadataTags) {
        List<String> rawMetadataList = new ArrayList();

        for (int t = 0; t < metadataTags.getLength(); t++) {
            Node metadataTag = metadataTags.item(t);
            String metadataTagName = metadataTag.getNodeName();
            
            if ("tag".equals(metadataTagName)){
                String metadata = metadataTag.getTextContent();
                rawMetadataList.add(metadata);
            }
        }

        return rawMetadataList;
    }

    public Status getTestStatus(final InputStream resultInputStream) throws XPathExpressionException {
        NodeList testStatusList = (NodeList)testStatusExtractor.evaluate(new InputSource(resultInputStream),XPathConstants.NODESET);
        LOGGER.debug("Found {} test results.",testStatusList.getLength());
        if(testStatusList.getLength()==1){
            final String nodeValue = testStatusList.item(0).getNodeValue();
            LOGGER.debug("Extracted status : {}",nodeValue);
            return Status.valueOf(nodeValue);
        }else{
            throw new BadDataException("We have "+testStatusList.getLength()+" test results, but one and only one was expected.");
        }
    }
    
    public String getLastFailureMessage(final InputStream resultInputStream) throws XPathExpressionException{
        return lastFailureMessageExtractor.evaluate(new InputSource(resultInputStream));
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description of a test in our library model : it has a name.
 * It is part of a {@link RobotFWTestSuite}.
 * 
 * @author edegenetais
 */
public class RobotFWTest {
    
    private String name;
    private Map<String, List<String>> metadataList = new HashMap<>();
    
    private List<RobotFWTestAnomaly> testAnomalies = new ArrayList<>();


    public RobotFWTest(String name) {
        this.name = name;    
    }
    
    public RobotFWTest(String name,  Map<String, List<String>> metadataList, List<RobotFWTestAnomaly> testAnomalies) {
        this.name = name;
        this.metadataList = metadataList;
        this.testAnomalies = testAnomalies;
    }

    public String getName() {
        return name;
    }

    public Map<String, List<String>> getMetadataList() {
        return metadataList;
    }

    public List<RobotFWTestAnomaly> getTestAnomalies() {
        return testAnomalies;
    }
  
}

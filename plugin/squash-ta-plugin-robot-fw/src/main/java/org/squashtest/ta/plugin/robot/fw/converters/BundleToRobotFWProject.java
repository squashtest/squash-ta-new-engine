/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.converters;

import java.io.File;
import java.util.Collection;
import java.util.Properties;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.robot.fw.resources.RobotFWProjectResource;

/**
 *
 * @author edegenetais
 */
@TAResourceConverter("structured")
public class BundleToRobotFWProject implements ResourceConverter<FileResource, RobotFWProjectResource>{
    private Properties projectParms;
    
    @Override
    public float rateRelevance(FileResource input) {
        return 0.5f;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        projectParms=new ConfigurationExtractor(this).extractSingleOptionalPropertiesResourceFromConfiguration(projectParms,configuration);
    }

    @Override
    public RobotFWProjectResource convert(FileResource resource) {
        if(projectParms==null){
            projectParms=new Properties();
        }
        
        boolean hasRobot=false;
        for(File f : FileTree.FILE_TREE.enumerate(resource.getFile(), false)){
            if(f.isFile() && f.getName().endsWith(".robot")){
                hasRobot=true;
                break;
            }
        }
        
        if(hasRobot){
            return new RobotFWProjectResource(resource.getFile(),projectParms);
        }else{
            throw new BadDataException("This is not a robot framework project");
        }
    }

    @Override
    public void cleanUp() {
        //noop
    }

}

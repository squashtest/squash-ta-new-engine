/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Properties;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * This class implements a codec to write (mainly) or read (perhaps one day)
 * global and test parameters in a format 
 * that may easily be read and written in python.
 * 
 * @author edegenetais
 */
public class PythonTFParmsCodec {
    
    private static final String TEST_SECTION_COMMENT = "Here go test (case) parameters";
    private static final String TEST_SECTION_NAME = "test";
    private static final String GLOBAL_SECTION_COMMENT = "Here go global (test suite) parameters";
    private static final String GLOBAL_SECTION_NAME = "global";

    public void write(Properties globalParms,Properties testParms, File destination){
        ByteArrayOutputStream globalData=new ByteArrayOutputStream();
        try {
            writeData(globalData, globalParms, testParms);
        } catch (IOException ex) {
            throw new InstructionRuntimeException("Failed to write parameter data into byte array streams. Somethin's rotten in the realm of Denmark!",ex);
        }
        try {
            new BinaryData(globalData.toByteArray()).write(destination);
        } catch (IOException ex) {
            throw new InstructionRuntimeException("IO failure while trying to write parameter data to the intended destination file ("+destination.getAbsolutePath()+")",ex);
        }
    }

    private void writeData(ByteArrayOutputStream destination, Properties globalParms, Properties testParms) throws IOException {
        ByteArrayOutputStream buffer=new ByteArrayOutputStream();
        try(
                OutputStreamWriter writer=new OutputStreamWriter(buffer,"utf-8");
                ){
            writeSectionContent(writer, GLOBAL_SECTION_NAME, GLOBAL_SECTION_COMMENT, globalParms);
            writeSectionContent(writer, TEST_SECTION_NAME, TEST_SECTION_COMMENT, testParms);
        }
        destination.write(buffer.toByteArray());
    }
    
    private void writeSectionContent(OutputStreamWriter writer, final String sectionName, final String comment, Properties globalParms) throws IOException {
        writer.write("["+sectionName+"]\n");
        writeParmProperties(writer, comment, globalParms);
    }

    private void writeParmProperties(OutputStreamWriter writer, final String comment, Properties globalParms) throws IOException {
        writer.write("# "+comment+"\n");
        for(String key:globalParms.stringPropertyNames()){
            writer.write(key+"="+globalParms.getProperty(key)+"\n");
        }
    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.assertions;

import org.squashtest.ta.plugin.robot.fw.library.OutputAnalyzer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import javax.xml.xpath.XPathExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWResult;
import org.squashtest.ta.plugin.robot.fw.resources.RobotFWResultResource;
import org.squashtest.ta.xml.functions.library.Transform;

/**
 * Assertion class to check if the robot framework result is for a successful test.
 * @author edegenetais
 */
@TABinaryAssertion("success")
public class AssertRobotFWIsSuccess implements UnaryAssertion<RobotFWResultResource>{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AssertRobotFWIsSuccess.class);
    private static final String OUTPUT_XML_REF = "output.xml";
    
    private OutputAnalyzer outputAnalyzer;    
    private RobotFWResultResource actual;

    public AssertRobotFWIsSuccess() throws XPathExpressionException{
        outputAnalyzer=new OutputAnalyzer();
    }
    
    @Override
    public void setActualResult(RobotFWResultResource actual) {
        this.actual=actual;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        new ConfigurationExtractor(this).expectNoConfiguration(configuration);
    }

    @Override
    public void test() {
        final RobotFWResult robotResult = actual.getResult();
            final File resultDir = robotResult.getResultDir();
            final File executionReport = new File(resultDir, OUTPUT_XML_REF);
        try(FileInputStream fileInputStream = new FileInputStream(executionReport)) {
            OutputAnalyzer.Status testStatus=outputAnalyzer.getTestStatus(fileInputStream);
            
            if((OutputAnalyzer.Status.PASS).equals(testStatus)){
                LOGGER.debug("Success detected in RobotFramework result {}",actual.getResult().getResultDir());
            }else{
                buildFailureReportAndThrow(executionReport, resultDir);
            }
            
        } catch (XPathExpressionException ex) {
            throw new InstructionRuntimeException("Failed to compile XPath expression, something's rotten in the realm of Denmark, please file a bug.", ex);
        } catch (FileNotFoundException ex) {
            throw new InstructionRuntimeException("Somethings rotten in the realm of Denmark : the output file SHOULD EXIST",ex);
        } catch (IOException ex) {
            throw new InstructionRuntimeException("Somethings rotten in the realm of Denmark : failed to read result data.",ex);
        }
    }

    private void buildFailureReportAndThrow(final File executionReport, final File resultDir) {
        String failureMessage="<No recoverable failure message>";
        try(FileInputStream outputIS=new FileInputStream(executionReport)){
            failureMessage=outputAnalyzer.getLastFailureMessage(outputIS);
        }catch(IOException e){
            LOGGER.warn("Something's rotten in the realm of Denmark : should be able to open the report file !", e);
        }catch(XPathExpressionException e){
            LOGGER.warn("The execution report apparently fails to fit in the expected structure, no way to get the failure message.", e);
        }
        
        
        final ArrayList<ResourceAndContext> failureContext = new ArrayList<>();
        addFileRAC(executionReport, OUTPUT_XML_REF, failureContext);
        addHtmlReadableRAC(resultDir, failureContext);
        throw new AssertionFailedException("The RobotFramework test failed : "+failureMessage, actual, failureContext);
    }

    private void addHtmlReadableRAC(final File resultDir, final ArrayList<ResourceAndContext> failureContext) {
        try {
            File reportingXSLT=File.createTempFile("reportXSLT", ".xslt",TempDir.getTestMainTempDir());
            File report=File.createTempFile("report", "html", TempDir.getTestMainTempDir());
            URL xsltResourceURL = getClass().getResource("reportXSLT.xslt");
            new BinaryData(xsltResourceURL).write(reportingXSLT);
            Transform reportingTransform=new Transform(reportingXSLT, true);
            reportingTransform.transform(new File(resultDir, OUTPUT_XML_REF), report);
            failureContext.add(new ResourceAndContext(new FileResource(report), new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, "executionReport.html")));
        } catch (
                StackOverflowError | //XSLT does that easily when you hit a corner case...it should not kill the whole test process !
                RuntimeException   | //Whatever exception happens, let's not let it prevent normal failure reporting.
                        IOException ex) {
            LOGGER.warn("Failed to create html report, it will be missing.", ex);
        }
    }
    
    private void addFileRAC(final File executionReport, final String resourceName, final ArrayList<ResourceAndContext> failureContext) {
        try{
            ResourceAndContext outputXmlRAC = createFileRAC(executionReport, resourceName);
            failureContext.add(outputXmlRAC);
        }catch(InstructionRuntimeException e){
            LOGGER.warn("Failed to attach failure context resource "+executionReport.getAbsolutePath()+" as "+resourceName,e);
        }
    }

    private ResourceAndContext createFileRAC(final File file, String resourceName) {
        
        ResourceAndContext generatedRAC = new ResourceAndContext();
        generatedRAC.setMetadata(new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, resourceName));
        generatedRAC.setResource(new FileResource(file).copy());
        return generatedRAC;
        
    }

}

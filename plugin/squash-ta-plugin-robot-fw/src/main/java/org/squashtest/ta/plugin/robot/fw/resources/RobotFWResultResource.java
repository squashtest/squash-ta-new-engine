/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.resources;

import java.io.IOException;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWResult;

/**
 *
 * @author edegenetais
 */
@TAResource("result.robotFW")
public class RobotFWResultResource implements Resource<RobotFWResultResource>{
    
    private RobotFWResult result;

    public RobotFWResultResource() {
        //this default constructor is useless, but Spring needs it during resource type enumeration
    }

    public RobotFWResultResource(RobotFWResult result) {
        this.result = result;
    }

    public RobotFWResult getResult() {
        return result;
    }

    @Override
    public RobotFWResultResource copy() {
        try {
            RobotFWResult newResult=new RobotFWResult(result.getExitValue(), FileTree.FILE_TREE.copyToTemp(result.getResultDir()),result.getStdout(),result.getStderr());
            return new RobotFWResultResource(newResult);
        } catch (IOException ex) {
            throw new InstructionRuntimeException("Failed to copy robotframework result resource.",ex);
        }
    }

    @Override
    public void cleanUp() {
        try {
            FileTree.FILE_TREE.clean(result.getResultDir());
        } catch (IOException ex) {
            LoggerFactory.getLogger(RobotFWProjectResource.class).warn("Failed to clean up robot framework result files.", ex);
        }
    }
}

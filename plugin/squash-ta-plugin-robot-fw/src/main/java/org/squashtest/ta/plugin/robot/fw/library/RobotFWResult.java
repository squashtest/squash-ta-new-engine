/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;

import java.io.File;

/**
 * Execution report from the robotFW runner.
 * @author edegenetais
 */
public class RobotFWResult {
    private final int exitValue;
    private final File resultDir;
    private final String stdout;
    private final String stderr;

    public RobotFWResult(int exitValue, File resultDir, String stdout, String stderr) {
        this.exitValue = exitValue;
        this.resultDir = resultDir;
        this.stdout = stdout;
        this.stderr = stderr;
    }

    

    public int getExitValue() {
        return exitValue;
    }

    public File getResultDir() {
        return resultDir;
    }

    public String getStdout() {
        return stdout;
    }

    public String getStderr() {
        return stderr;
    }
    
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dclaerhout
 */
public class RobotFWMetadataExtractorFromTags {
    
    List<RobotFWTestAnomaly> testAnomalies = new ArrayList<>();
        
    public Map<String, List<String>> extractMetadataFromTags(List<String> metadataRawListFromTags) {

        Map<String, List<String>> metadataList = new HashMap<>();


        for (String rawMetadata : metadataRawListFromTags) {

      
            String metadataKey;
            List<String> metadataValues = new LinkedList<String>();

            //We want to identify if the tag in robot output.xml file is a metadata prefixed with "tf:"
            if (rawMetadata.startsWith("tf:")) {

                //We have verified it was a metadata so we can now remove "tf:" prefix
                rawMetadata = rawMetadata.substring(3);

                //We must now check if the metadata has just one key with no value   
                if (!rawMetadata.contains("=")) {
                    metadataKey = rawMetadata.trim();

                    checkKeyUnicity(metadataList, testAnomalies, metadataKey);
                    metadataList.put(metadataKey, metadataValues);
                    
                } else {
                    //The metadata has one or multiple values. First we extract the key.
                    metadataKey = rawMetadata.substring(0, rawMetadata.indexOf('='));

                    //We do not need anymore the metadata key from the rawMetadata so we just keep the part after the "=" symbol
                    String rawMetadataValues = rawMetadata.substring(rawMetadata.indexOf('=')+1);

                    metadataValues = extractMetadataValues(rawMetadataValues);
                    checkKeyUnicity(metadataList, testAnomalies, metadataKey);
                    metadataList.put(metadataKey, metadataValues);
                    
                }

            }
        }

        return metadataList;
    }

    private void checkKeyUnicity(Map<String, List<String>> metadataList, List<RobotFWTestAnomaly> testAnomalies, String metadataKey) {
        if (!metadataList.isEmpty()){
            for (Map.Entry<String, List<String>> metadata : metadataList.entrySet()){
                String currentKey = metadata.getKey().trim();

                /*duplicate Metadata KEY found. We add it to the list of duplicate keys. If key and values are stricly identical,
                  Robot framework automatically delete one of the two keys and doesn't generate only one tag.*/
                if (currentKey.equalsIgnoreCase(metadataKey.trim())) {
                    testAnomalies.add(new RobotFWTestAnomaly("Duplicate_key", metadataKey));
                }           
            }
        }
    }


    private List<String> extractMetadataValues(String rawMetadataValues) {
         List<String> metadataValues=new LinkedList<String>();
         
            //if there is no "|" symbol to separate the values, so the metadata has only one key and one value
            if (!rawMetadataValues.contains("|")){
                metadataValues.add(rawMetadataValues.trim());           
            } else {
                
                final String[] values=rawMetadataValues.split("\\|");
                
                for (final String value : values) {
                    metadataValues.add(value.trim());
                }
            }
         return metadataValues;
    }

    public List<RobotFWTestAnomaly> getTestAnomalies() {
        return testAnomalies;
    }

    
 
}

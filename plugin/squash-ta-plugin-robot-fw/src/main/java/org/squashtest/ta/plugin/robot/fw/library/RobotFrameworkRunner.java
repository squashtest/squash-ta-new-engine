/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.process.LocalProcessClient;
import org.squashtest.ta.local.process.library.shell.ShellResult;

/**
 * This class is intended to provide low-level functionalities to
 * run tests in RobotFramework.
 * 
 * @author edegenetais
 */
public class RobotFrameworkRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(RobotFrameworkRunner.class);
    
    static interface LocalProcessClientFactory{
        LocalProcessClient getClient();
    }
    
    private LocalProcessClientFactory clientFactory=new LocalProcessClientFactory() {
        @Override
        public LocalProcessClient getClient() {
            return new LocalProcessClient();
        }
    };
    
    private final File projectBase;
    private final Properties globalParms;
    private final Properties testParms;

    /**
     * This constructor was meant for testability.
     * @param clientFactory a factory to privode (most probably mock) {@link LocalProcessClient} instances.
     * @param projectBase the target project.
     */
    RobotFrameworkRunner(LocalProcessClientFactory clientFactory,File projectBase, Properties globamlParms,Properties testParms) {
        this(projectBase,globamlParms,testParms);
        this.clientFactory=clientFactory;
    }

    /**
     * Instanciate the runner for a given project.
     * 
     * @param projectBase the target project.
     * @param globamlParms global parameters mapping
     * @param testParms test parameters mapping
     */
    public RobotFrameworkRunner(File projectBase, Properties globamlParms,Properties testParms) {
        this.projectBase = projectBase;
        this.globalParms=globamlParms;
        this.testParms=testParms;
        
        LOGGER.debug("Runner created for project base {}",projectBase);
    }

    private String[] buildParmTable(final List<String> specificParmList, File resultDir) {
        try {
            final List<String> commandParmList = new ArrayList<>();
            commandParmList.add("-m robot");
            commandParmList.addAll(specificParmList);
            commandParmList.add("--outputdir");
            commandParmList.add("\""+resultDir.getCanonicalPath()+"\"");
            commandParmList.add("\""+projectBase.getCanonicalPath()+"\"");
            
            return commandParmList.toArray(new String[commandParmList.size()]);
        } catch (IOException ex) {
            throw new RobotFrameworkExecutionException("Failed to build robotframework command parameters. Some file reference may be invalid.",ex);
        }
    }
    
    private File createResultDir() throws IOException {
        Path workPath = Paths.get(TempDir.getExecutionTempDir().toURI());
        Path resultPath = Files.createTempDirectory(workPath, "robotFW");
        resultPath.toFile().deleteOnExit();
        return resultPath.toFile();
    }

    protected RobotFWResult executeRobotFWCommand(final List<String> specificParmList) throws IOException {

        File resultDir = createResultDir();

        String[] commandParm = buildParmTable(specificParmList, resultDir);

        LOGGER.debug("Running RobotFramework command {} with result dir {}", commandParm,resultDir);

    try{
        LocalProcessClient client=clientFactory.getClient();
        
        PythonTFParmsCodec parmsCodec=new PythonTFParmsCodec();
        File parmDataFile=File.createTempFile("tmParmsData", ".ini",TempDir.getTestMainTempDir());
        
        LOGGER.trace("Transmitting parameters :\nProject parameters= {} \nTest parameters={}\n",globalParms,testParms);
        
        parmsCodec.write(globalParms, testParms, parmDataFile.getCanonicalFile());
        
        client.setEnvironmentVariable("_SQUASH_TF_TESTCASE_PARAM_FILES", parmDataFile.getCanonicalPath());
        
        StringBuilder commandLineBuilder=new StringBuilder("python");
        for(String parm:commandParm){
            commandLineBuilder.append(" ").append(parm);
        }
        ShellResult robotProcessResult=client.runLocalProcessCommand(commandLineBuilder.toString(),projectBase,Integer.MAX_VALUE, Integer.MAX_VALUE);
        int exitValue = robotProcessResult.getExitValue();

        if (exitValue > 0 && exitValue <= 250) {
            LOGGER.info("RobotFramework reported {} failed critical tests", exitValue);
            LOGGER.debug("Failed execution stdout is:\n{}\n and stderr is:\n{}\n",robotProcessResult.getStdout(),robotProcessResult.getStdout());
        }
        if (exitValue > 250) {
            LOGGER.error("RobotFramework process failed with code {}\n stdout:\n{}\n stderr:\n{}\n", exitValue,robotProcessResult.getStdout(),robotProcessResult.getStderr());
            throw new RobotFrameworkExecutionException("RobotFramework run failed with code " + exitValue);
        } else {
            LOGGER.debug("Executed test with code {}\n stdout:\n{}\nstderr:\n{}\n", exitValue,robotProcessResult.getStdout(),robotProcessResult.getStderr());
        }
        return new RobotFWResult(exitValue, resultDir,robotProcessResult.getStdout(),robotProcessResult.getStderr());
    }catch (InterruptedException ex) {
        throw new RobotFrameworkExecutionException("Something interrupted the wait for robotFW results.", ex);
    }catch (TimeoutException ex) {
        throw new RobotFrameworkExecutionException("The robotFW process timed out!", ex);
    } catch (ParseException ex) {
            throw new RobotFrameworkExecutionException("robotFW execution failed (bad command line).", ex);
    }
        
    }
    
    private String getBaseSuiteUniqueName(){
        return "/";
    }
    
    /**
     * Run the requested test.
     * @param testName the name of the test to run.
     * @return the base directory where reports are located.
     */
    public RobotFWResult runSingleTest(String suiteName,String testName){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("Preparing launch for RobotFramework test {} from project base {}",testName,projectBase==null?"null":projectBase.getAbsolutePath());
        }
        
        final String baseSuiteUniqueName = getBaseSuiteUniqueName();
        
        
        final List<String> specificParmList=new ArrayList<>();
        
        specificParmList.add("--test");
        specificParmList.add("\""+testName+"\"");
        
        specificParmList.add("--suite");
        specificParmList.add("\""+baseSuiteUniqueName+"."+suiteName+"\"");
        
        specificParmList.add("--name");
        specificParmList.add("\""+baseSuiteUniqueName+"\"");
        try {
            return executeRobotFWCommand(specificParmList);
        } catch (IOException ex) {
            throw new RobotFrameworkExecutionException("Failed to open test resources or write results.", ex);
        }
    }

    /**
     * Launches a global dry run to enumerate all available tests and check keyword availability.
     * @return the result of the dry run.
     */
    public RobotFWResult dryRunAll(){
        try {
            return executeRobotFWCommand(Arrays.asList(new String[]{"--dryrun"}));
        } catch (IOException ex) {
            throw new RobotFrameworkExecutionException("Failed to open test resources or write results",ex);
        }
    }
}

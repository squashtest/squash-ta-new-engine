/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.library;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Description of a tests suite in our library model : it is a tests container.
 * @author edegenetais
 */
public class RobotFWTestSuite {
    private final String name;
    private File suiteSource;
    private final List<RobotFWTest> tests;

    public RobotFWTestSuite(String name, File suiteSource, List<RobotFWTest> tests) {
        this.name = name;
        this.suiteSource = suiteSource;
        this.tests = tests;
    }

    /**
     * @return the read-only list of the tests in this tests suite.
     */
    public List<RobotFWTest> getTests() {
        return Collections.unmodifiableList(tests);
    }

    /**
     * @return the name of the tests suite.
     */
    public String getName() {
        return name;
    }

    public File getSuiteSource() {
        return suiteSource;
    }

    
}

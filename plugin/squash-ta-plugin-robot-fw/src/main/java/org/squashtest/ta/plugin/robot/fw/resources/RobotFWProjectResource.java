/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.robot.fw.resources;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * This resource represents a RobotFramework test project.
 * @author edegenetais
 */
@TAResource("bundle.robotFW")
public class RobotFWProjectResource implements Resource<RobotFWProjectResource>{
    
    private File projectBase;
    private Properties globalParms;

    public RobotFWProjectResource() {
        //this default constructor is useless, but Spring needs it during resource type enumeration
    }

    public RobotFWProjectResource(File projectBase,Properties globamlParms) {
        this.projectBase = projectBase;
        Properties parms=new Properties();
        parms.putAll(globamlParms);
        this.globalParms=globamlParms;
    }
    
    @Override
    public RobotFWProjectResource copy() {
        try {
            final File newBase = FileTree.FILE_TREE.copyToTemp(projectBase);
            Properties newGlobalParms;
            if(globalParms==null){
                newGlobalParms=null;
            }else{
                newGlobalParms=new Properties();
                newGlobalParms.putAll(globalParms);
            }
            return new RobotFWProjectResource(newBase,newGlobalParms);
        } catch (IOException ex) {
            throw new InstructionRuntimeException("Failed to copy robot framework project.", ex);
        }
    }

    @Override
    public void cleanUp() {
        try {
            FileTree.FILE_TREE.clean(projectBase);
        } catch (IOException ex) {
            LoggerFactory.getLogger(RobotFWProjectResource.class).warn("Failed to clean up robot framework project copy {}.", projectBase.getAbsolutePath() ,ex);
        }
    }
    
    public File getProjectBase(){
        return projectBase;
    }
    
    /**
     * Read-only access to the project's global parameters.
     * @return an independeant duplicate of the project's global parameters. You may change this, but it won't change the original data.
     */
    public Properties getGlobalParms(){
        Properties p=new Properties();
        if(globalParms!=null){
            p.putAll(globalParms);
        }
        return p;
    }

}

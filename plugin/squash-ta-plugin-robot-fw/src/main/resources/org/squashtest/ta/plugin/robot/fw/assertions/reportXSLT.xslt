<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:call-template name="title">
                        <xsl:with-param name="suite" select="/robot/suite/suite"/>
                    </xsl:call-template>
                </title>
                <link rel="stylesheet">
                    <xsl:attribute name="href"><xsl:call-template name="link2base"/>public/stylesheets/knacss.css</xsl:attribute>
                </link>
                <link rel="stylesheet">
                    <xsl:attribute name="href"><xsl:call-template name="link2base"/>public/stylesheets/tweaks.css</xsl:attribute>
                </link>
            </head>
            <body>
                <table>
                    <caption>Test Case</caption>
                    <tr>
                        <th>Name - id<xsl:value-of select="name()" /><xsl:value-of select="position()" /></th>
                        <th class="w1">Status</th>
                        <th>Message</th>
                    </tr>
                    <tr>
                        <td>
                            <span>
                            <xsl:call-template name="title">
                                <xsl:with-param name="suite" select="/robot/suite/suite"/>
                            </xsl:call-template>
                            </span>
                        </td>
                        <td class="w1">
                            <img>
                                <xsl:attribute name="src">
                                    <xsl:call-template name="ball-src">
                                        <xsl:with-param name="target" select="//test"/>
                                    </xsl:call-template>
                                </xsl:attribute>
                            </img>
                        </td>
                        <td class="optional">
                            <xsl:value-of select="//test/status"/>
                        </td>
                    </tr>
                </table>
                <xsl:apply-templates select="//test" />

                <xsl:if test="count(/robot/errors/msg)>0">
                <table>
                    <tr>
                        <th colspan="2">Toplevel error messages</th>
                    </tr>
                    <xsl:for-each select="/robot/errors/msg">
                        <tr>
                            <td>
                                <xsl:value-of select="position()"/>
                            </td>
                            <td><xsl:value-of select="."/></td>
                        </tr>
                    </xsl:for-each>
                </table>
                </xsl:if>
                <script type="text/javascript">
                    <xsl:attribute name="src"><xsl:call-template name="link2base"/>public/javascripts/dropdown.js</xsl:attribute>
                </script>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="*">
        <xsl:apply-templates select="*"/>
    </xsl:template>
    <xsl:template match="test">
        <table class="w100 striped">
            <caption>Setup Keywords</caption>
            <xsl:call-template name="table-headers" />
            <xsl:apply-templates select="../kw[./@type='setup']"/>
        </table>
        <table class="w100 striped">
            <caption>Test Case Keywords</caption>
            <xsl:call-template name="table-headers" />
            <xsl:apply-templates select="./*"/>
        </table>
        <table class="w100 striped">
            <caption>Teardown Keywords</caption>
            <xsl:call-template name="table-headers" />
            <xsl:apply-templates select="../kw[./@type='teardown']"/>
        </table>
    </xsl:template>
    <xsl:template name="title">
        <xsl:param name="suite"/>
        <xsl:value-of select="$suite/@name"/>
        <xsl:choose>
<xsl:when test="count($suite/suite)=1">.<xsl:call-template name="title"><xsl:with-param name="suite" select="$suite/suite"/></xsl:call-template></xsl:when>
<xsl:when test="count($suite/test)=1">/<xsl:value-of select="$suite/test/@name" /></xsl:when>
        </xsl:choose>
    </xsl:template>
 <xsl:template name="link2base"><xsl:call-template name="linklevel"><xsl:with-param name="level" select="/robot/suite"/></xsl:call-template></xsl:template>
 <xsl:template name="linklevel">
        <xsl:param name="level"/>../<xsl:if test="count($level/suite|$level/test)>0"><xsl:call-template name="linklevel"><xsl:with-param name="level" select="$level/suite|$level/test"/></xsl:call-template></xsl:if>
 </xsl:template>
    <xsl:template name="ball-src">
        <xsl:param name="target" />
<xsl:call-template name="link2base"/>public/images/<xsl:choose><xsl:when test="$target/status/@status='FAIL'">Circle_Yellow.png</xsl:when><xsl:when test="$target/status/@status='PASS'">Circle_Green.png</xsl:when><xsl:otherwise><xsl:message terminate="no">Unknown suite element status<xsl:value-of select="$target/status/@status" /></xsl:message>broken.png</xsl:otherwise></xsl:choose>
    </xsl:template>
    <xsl:template match="kw">
        <tr>
            <td>
                <xsl:call-template name="rowspan-if-args" />
                <span>
                    <xsl:value-of select="@name" />
                </span>
            </td>
            <td class="w1">
                <xsl:call-template name="rowspan-if-args" />
                <img>
                    <xsl:attribute name="src">
                        <xsl:call-template name="ball-src">
                            <xsl:with-param name="target" select="."/>
                        </xsl:call-template>
                    </xsl:attribute>
                </img>
            </td>
            <td class="optional">
                <xsl:call-template name="rowspan-if-args" />
                <xsl:choose>
                    <xsl:when test="status/@status='FAIL'">
                        <xsl:value-of select="msg[@level='FAIL'][last()]" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="msg[last()]" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td class="optional">
                <xsl:value-of select="arguments/arg[1]"/>
            </td>
            <td class="optional master-of-detail">
                <xsl:call-template name="rowspan-if-args" />
                <xsl:if test="count(./kw)>0">
                    <xsl:value-of select="count(./kw)"/> called keyword<xsl:if test="count(./kw)>1">s</xsl:if>
                    <div class="toggle-button">              
                        <xsl:attribute name="id">toggle<xsl:value-of select="generate-id(.)"/>-expand</xsl:attribute>
                        <a class="toggleClass">
                            <xsl:attribute name="name">toggle<xsl:value-of select="generate-id(.)"/></xsl:attribute>
                            <img>
                                <xsl:attribute name="src"><xsl:call-template name="link2base"/>public/images/expand.png</xsl:attribute>
                            </img>
                        </a>
                    </div>
                    <div class="desktop-hidden toggle-button">
                        <xsl:attribute name="id">toggle<xsl:value-of select="generate-id(.)"/>-collapse</xsl:attribute>
                        <a class="toggleClass">
                            <xsl:attribute name="name">toggle<xsl:value-of select="generate-id(.)"/></xsl:attribute>
                            <img>
                                <xsl:attribute name="src"><xsl:call-template name="link2base"/>public/images/collapse.png</xsl:attribute>
                            </img>
                        </a>
                    </div>
                    <table class="desktop-hidden w100 striped">
                        <xsl:attribute name="id">toggle<xsl:value-of select="generate-id(.)"/></xsl:attribute>
                        <xsl:call-template name="table-headers" />
                        <xsl:apply-templates select="./*" />
                    </table>
                </xsl:if>
            </td>
        </tr>
        <xsl:for-each select="arguments/arg[position()>1]">
            <tr>
                <td class="optional"><xsl:value-of select="." /></td>
            </tr>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="table-headers">
        <tr>
            <th>Name</th>
            <th class="w1">Status</th>
            <th>Message</th>
            <th>Parameters</th>
            <th>Details</th>
        </tr>
    </xsl:template>
    <xsl:template name="rowspan-if-args">
        <xsl:if test="count(arguments/arg)>1">
                    <xsl:attribute name="rowspan"><xsl:value-of select="count(arguments/arg)" /></xsl:attribute>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>

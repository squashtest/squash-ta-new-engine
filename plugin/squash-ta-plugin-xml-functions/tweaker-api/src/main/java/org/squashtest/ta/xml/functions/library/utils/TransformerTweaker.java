/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils;

import javax.xml.transform.Transformer;

/**
 * All implementations of transformerTweaker must be very very very specfic. 
 * It is designed to be used to tweak a <strong>specific</strong> {@link Transformer}
 * on a <strong>specific</strong> feature. 
 * 
 * Since Jaxb implementation providers may to bad things to the class loader and registry to be the frist picked
 * we need to avoid as much as we can hard linkage to those. Hence the implemenations of {@link TransformerTweaker} must 
 * also not contain any direct dependency to a specific provider and should tweak their transformer using Reflection or 
 * find anbother clever way to avoid the direct dependency
 * @author fgautier
 *
 */
public interface TransformerTweaker {


	/**
	 * Does the {@link TransformerTweaker} implementation accept the transformer.
	 * @param transformer
	 * @return
	 */
	boolean accept(Transformer transformer);
	
	/**
	 * Tweak a defined Implementation of {@link Transformer} on a specific feature.
	 * 
	 * If for an unknown reason the tweak cannot be performed a rollback should be performed
	 * to ensure that the transformer at the end of the method is exactly the same as the one
	 * used as input.  
	 * 
	 * @param transformer An implementation of a {@link Transformer} about to be tweaked
	 */
	void apply(Transformer transformer);
}

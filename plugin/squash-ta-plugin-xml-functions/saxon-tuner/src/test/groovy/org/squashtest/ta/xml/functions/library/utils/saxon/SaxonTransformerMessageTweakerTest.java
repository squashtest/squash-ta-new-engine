/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils.saxon;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test concerning {@link SaxonTransformerMessageTweaker}.
 * 
 * @author fgautier
 *
 */
public class SaxonTransformerMessageTweakerTest {
	
	/**
	 * The object undertest. 
	 */
	private SaxonTransformerMessageTweaker testee = new SaxonTransformerMessageTweaker();
	
	
	/**
	 * The factory used to pick a transformer. As the only jaxb implemenation provided (in test scope only !)
	 * is saxon 9.4 this {@link TransformerFactory} should be a saxon 9.4 implemetation
	 */
	private TransformerFactory tFactory;
	
	/**
	 * The transfomer we'll want to test tweakin on. As the only jaxb implemenation provided (in test scope only !)
	 * is saxon 9.4 this {@link Transformer} ould be a saxon 9.4 implemetation (i.e. a Controller)
	 */
	private Transformer transformer;
	
	/**
	 * The style sheet used to construct the {@link Transformer}.
	 */
	private static final byte[] identitySheet;
	
	static{
		URL identityUrl=SaxonTransformerMessageTweaker.class.getResource("identity.xslt");
		if(identityUrl==null){
			throw new RuntimeException("Failed to load resource 'identity.xslt' for normalization.");
		}
		ByteArrayOutputStream identityBuffer=new ByteArrayOutputStream();
		byte[] buffer=new byte[4096];
		try(
				InputStream identityLoadStream=identityUrl.openStream();
				){
			int nb=identityLoadStream.read(buffer);
			while(nb>=0){
				identityBuffer.write(buffer, 0, nb);
				nb=identityLoadStream.read(buffer);
			}
		} catch (IOException e) {
			throw new RuntimeException("Failed to load identity stylesheet (for normalization)",e);
		}
		identitySheet=identityBuffer.toByteArray();
	}
	
	/**
	 * Get a saxon 9.4 {@link Transformer} 
	 * @throws TransformerConfigurationException
	 */
	@Before
	public void setup() throws TransformerConfigurationException {
		tFactory = TransformerFactory.newInstance();
		transformer = tFactory.newTransformer(new StreamSource(new ByteArrayInputStream(identitySheet)));
	}
	
	/**
	 * A saxon 9.4 {@link Transformer} implementation should be accepted.
	 */
	@Test
	public void testAcceptSaxonTransformer() {
		Assert.assertTrue(testee.accept(transformer));
	} 
	
	/**
	 * A null transformer should not be accepted. For the sake of simplicity 
	 * we do not test for the moment the non acceptance of another implementation.
	 */
	@Test
	public void testNotAcceptNullTransformer() {
		Assert.assertFalse(testee.accept(null));
	}
	
	/**
	 * A very crude test. Testing only that everything wen't well during the tweaking
	 */
	@Test
	public void testGoesThroughReflectionProcessToChangeEmitter() {
		testee.apply(transformer);
		Assert.assertNotNull(transformer);
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils.saxon;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.xml.transform.Transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.xml.functions.library.utils.TransformerTweaker;


/**
 * A tweaker accepting saxon 9.4 {@link Transformer} and adjusting it's message
 * emitter to log the xsl:message the log "stream". If terminate="yes" is specified
 * then the message is logged in WARN mode. Else it is logged in DEBUG mode. The name of the logger is
 * <strong>squash.ta.xml.xslmessage</strong>
 * 
 * This specific logging behavior is implemented using a {@link LoggerMessageReceiver} which does exactly
 * what its name tells. 
 *   
 * Agani as we wan't to avoid as much as we can a hard link with Saxon, we chose to
 *  to use reflection and the proxy pattern.
 * 
 * To clarify saxon's API, initially a receiver is used to deal with a xml event. 
 * In the case of an xsl:message the specific Receiver dealing with it becomes an 
 * emitter (in saxon's words) since it has to emit somewhere the xsl:message (usually
 * in the stdio). 
 * 
 * @author fgautier
 *
 */
public class SaxonTransformerMessageTweaker implements TransformerTweaker {

	/**
	 * Specific class LOGGER to log class related messages. Do not confuses it with the one used
	 * in {@link LoggerMessageReceiver} which logs the xsl:messages.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SaxonTransformerMessageTweaker.class);
	
	/**
	 * Name of the saxon 9.4 implementation of {@link Transformer}
	 */
	private static final String NET_SF_SAXON_CONTROLLER = "net.sf.saxon.Controller";
	
	/**
	 * Name of the interface an xsl:message emitter should implement in saxon 9.4 API.
	 * This is the interface that {@link LoggerMessageReceiver} is virtually implementing. 
	 */
	private static final String NET_SF_SAXON_EVENT_RECEIVER = "net.sf.saxon.event.Receiver";
	
	/**
	 * Name of the method of the saxon 9.4 Controller used to set his xsl:message
	 * emitter.  
	 */
	private static final String SET_MESSAGE_EMITTER_METHOD = "setMessageEmitter";


	
	@Override
	public boolean accept(Transformer transformer) {
		
		String transformerName = (transformer==null) ? "{null}" : transformer.getClass().getCanonicalName() ;
		
		LOGGER.debug(SaxonTransformerMessageTweaker.class.getSimpleName() + ": Accepting transfomer "
				 		+ transformerName + " ? "
				 		+ (NET_SF_SAXON_CONTROLLER.equals(transformerName) ? "yes" : "no")			 
				 	) ;
		
		return NET_SF_SAXON_CONTROLLER.equals(transformerName);
	}

	
	@Override
	public void apply(Transformer transformer) {

		LOGGER.debug(SaxonTransformerMessageTweaker.class.getSimpleName() + " tweaking " + transformer.getClass().getSimpleName());
		
		try {
			Class<?> receiverInterface = Class.forName(NET_SF_SAXON_EVENT_RECEIVER);
			InvocationHandler handler = new MessageInvocationHandler(new LoggerMessageReceiver());
			
			Object proxyEmitter = Proxy.newProxyInstance(receiverInterface.getClassLoader(), 
															new  Class[]{receiverInterface},
															handler);
			
			Class<?> controllerClass = Class.forName(NET_SF_SAXON_CONTROLLER);
			Method setMessageEmmiter = controllerClass.getMethod(SET_MESSAGE_EMITTER_METHOD, new Class[]{receiverInterface});
			setMessageEmmiter.invoke(transformer, proxyEmitter);
			
		} catch (ClassNotFoundException 
				| NoSuchMethodException 
				| SecurityException 
				| IllegalAccessException 
				| InvocationTargetException
				| IllegalStateException e) {
			/* No real need to rollback here since whenever reflection fails the initial transformer is still untouched,
			   since the Message emitter is set at the end. */
			LOGGER.warn("Tweaking failed. Please ensures that you are tweaking a saxon 9.4 transformer and that saxon 9.4 is provided", e);
		}
		LOGGER.debug("Tweaking succesfull");
		
	}

}

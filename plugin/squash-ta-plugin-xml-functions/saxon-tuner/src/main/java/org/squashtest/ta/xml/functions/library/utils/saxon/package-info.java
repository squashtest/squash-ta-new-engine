/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * This pacakges provides saxon 9.4  related {@link org.squashtest.ta.xml.functions.library.utils.TransformerTweaker}. All the visible twaekers only accepts
 * saxon 9.4 {@link javax.xml.transform.Transformer} and tweak it accordingly. Possible tweakers are
 * 
 * <ul>
 * <li>SaxontTransformerMessageTwaker : This modifies its message emmiter such that xsl:message are logged using sl4j logger in either debug or error</li>
 * </ul>
 * 
 * In this package we choose the Reflection "strategy" in order to avoid direct linkage to saxon 9.4. 
 * @author fgautier
 *
 */
package org.squashtest.ta.xml.functions.library.utils.saxon;
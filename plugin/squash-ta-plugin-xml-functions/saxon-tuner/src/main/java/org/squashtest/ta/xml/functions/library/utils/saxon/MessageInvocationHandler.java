/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils.saxon;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * This {@link InvocationHandler} has to be used to to set via reflection the MessageEmitter emmiting a xsm:message
 * somewhere (which is in fact a Receiver since it receives from saxon API a message and deals with it...)
 * 
 * It shouldnot be used outside of this package hence the protected constructor.
 *   
 * @author fgautier
 *
 */
public class MessageInvocationHandler implements InvocationHandler {
	
	private Class<?> emmiterClass = ProxyMessageReceiverBase.class;
	
	private ProxyMessageReceiverBase delegate;
	
	protected MessageInvocationHandler(ProxyMessageReceiverBase receiver) {
		delegate = receiver;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		
		for (Method refMethod : emmiterClass.getMethods()) {
			if (refMethod.getName().equals(method.getName())) {
				return refMethod.invoke(delegate, args);
			}
		}
		
		throw new UnsupportedOperationException("There is something rotten in the realm of Danemark. Method " 
				+ method.getName()
				+ "is not implemented in proxy Receiver "
				+ delegate.getClass().getName());
	}

}

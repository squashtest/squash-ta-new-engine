/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils.saxon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This {@link ProxyMessageReceiverBase} emmit a received message in the log "stream". A non terminate 
 * message is send to Debug, a terminate one to Error.
 * 
 * This {@link ProxyMessageReceiverBase} is meant only to be used in our tweaked transformer. Hence its 
 * visibility is set to protected to ensure that only an object of this package can instantiate it (preferably the
 * {@link SaxonTransformerMessageTweaker}).
 * @author fgautier
 *
 */
public class LoggerMessageReceiver extends ProxyMessageReceiverBase {

	/**
	 * The logger name in which
	 */
	private static final String LOGGER_NAME = "squash.ta.xml.xslmessage";

	private static final Logger LOGGER = LoggerFactory.getLogger(LOGGER_NAME);
	
	/**
	 * Regulare CTOR. c.f {@link ProxyMessageReceiverBase} contructor : 
	 *
	 * Regular constructor. As this Receiver has to be used is a saxon 9.4 context <strong>only</strong>
	 * its initialization crashes if something bads happens with reflection.
	 * 
	 * Reflection is used to fetch the ReceiverOptions.TERMINATE flag value.
	 *
	 * @throws IllegalStateException if it can't be initiated. i.e. one is not in a saxon 9.4 environnment.
	 */
	public LoggerMessageReceiver() {
		super();
	}
	
	@Override
	protected void treatMessage(String message, boolean isTerminate) {
		if (isTerminate) {
			LOGGER.error(message);
		} else {
			LOGGER.debug(message);
		}
	}

}

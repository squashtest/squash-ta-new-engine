/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library.utils.saxon;

/**
 * Base "implementation" of a basic xls message <strong>saxon 9.4</strong> receiver.
 * 
 * This has to be used <strong>only</strong> as a MessageEmmiter in a Saxon controller (saxon 9.4 transformer implementation).
 * 
 * According to saxon documentation the relevant methods to implements are : 
 * <ul>
 * <li>startDocument : Will be called by each xsl:message instruction. If terminate="yes" is specified in the xsl:message call then the properties arg 
 * of the startDocument will be set to ReceiverOptions.TERMINATE</li>
 * <li>characters : is called to notify part of the message.</li>
 * <li>endDocument: Notifies that the message has been fully received and that it can be processed</li>
 * </ul>
 * 
 * As we still want to avoid as much as we can hard linkage with saxon to avoid (messing with other possible chosen jaxb providers) 
 * we erase from method signatures any possible sign of it and use reflection to get the value of ReceiverOptions.TERMINATE. 
 * 
 * This has to be used in a context where saxon 9.4 is provided otherwise we wan't it to crash badly !
 *  
 * @author fgautier
 *
 */
public abstract class ProxyMessageReceiverBase {

	private int terminateFlag = -1;
		
	private int currentFlag = -1;
	
	private StringBuilder messageBuilder;
	
	
	
	/**
	 * Regular constructor. As this Receiver has to be used is a saxon 9.4 context <strong>only</strong>
	 * its initialization crashes if something bads happens with reflection.
	 * 
	 * Reflection is used to fetch the ReceiverOptions.TERMINATE flag value.
	 */
	protected ProxyMessageReceiverBase() {
		try {
			Class<?> receiverOptions = Class.forName("net.sf.saxon.event.ReceiverOptions");
			terminateFlag = receiverOptions.getDeclaredField("TERMINATE").getInt(null);
		} catch (IllegalArgumentException | IllegalAccessException
				| NoSuchFieldException | SecurityException | ClassNotFoundException e) {
			throw new IllegalStateException("Could not instanciate ProxyMessageReceiverBase. Please use it"
					+ " in a context where saxon 9.4 is provided. Cause", e);
		}
	}

	/**
	 * Will be called by each xsl:message instruction. If terminate="yes" is specified in the xsl:message call then the properties arg 
	 * of the startDocument will be set to ReceiverOptions.TERMINATE
	 * @param flag set by saxon to ReceiverOptions.TERMINATE if terminate="yes" is specified in the xsl:message.
	 */
	public void startDocument(int flag) {
		currentFlag = flag;
		messageBuilder = new StringBuilder();
	} 
	
	/**
	 * Is called to notify part of the message.
	 * @param arg0 the message part
	 * @param arg1 is irrelevant in our case. Check Saxon's documentation for more details.
	 * @param arg2 is irrelevant in our case. Check Saxon's documentation for more details.
	 */
	public void characters(CharSequence arg0, int arg1, int arg2) {
		messageBuilder.append(arg0);
	}

	/**
	 * Notifies that the message has been fully received and that it can be processed.
	 */
	public void endDocument() {
		treatMessage(messageBuilder.toString(), currentFlag==terminateFlag);
	}
	
	/**
	 * Treat a Received xls:message. Should be redefined by a specific implementation of {@link ProxyMessageReceiverBase}.
	 * @param message The retreived xsl:message
	 * @param isTerminate flag set to true if terminate="yes" is mentioned, false otherwise.
	 */
	protected abstract void treatMessage(String message, boolean isTerminate);

	/* In this simple implementation the methods below are not relevant */
	public void startContent() {}
	public String getSystemId() {return null;}
	public void attribute(Object arg0, Object arg1, CharSequence arg2,
			int arg3, int arg4) {}
	public void close() {}
	public void comment(CharSequence arg0, int arg1, int arg2) {}
	public void endElement() {}
	public Object getPipelineConfiguration() {return null;}
	public void namespace(Object arg0, int arg1){}
	public void open() {}
	public void processingInstruction(String arg0, CharSequence arg1, int arg2,
			int arg3) {}
	public void setPipelineConfiguration(Object arg0) {}
	public void setSystemId(String arg0) {}
	public void setUnparsedEntity(String arg0, String arg1, String arg2) {}
	public void startElement(Object arg0, Object arg1, int arg2, int arg3){}
	public boolean usesTypeAnnotations() {return false;}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.converters;

import java.io.File;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.plugin.commons.resources.XMLResource;

/**
 * That class wraps generated XML that HAS to be cleaned up to avoid cluttering disks.
 * @author edegenetais
 *
 */
class GeneratedXmlResource extends XMLResource {
	
	/** Default constructor for Spring enumeration only */
	public GeneratedXmlResource() {
	}
	
	GeneratedXmlResource(File file) {
		super(file);
	}

	//AFAIK, we create that one from scratch, so let's clean after ourselves...	
	@Override
	public void cleanUp() {
		if(!super.getXMLFile().delete()){
			LoggerFactory.getLogger(getClass()).warn("Failed to clean file ", super.getXMLFile().getAbsolutePath());
		}
	}
}
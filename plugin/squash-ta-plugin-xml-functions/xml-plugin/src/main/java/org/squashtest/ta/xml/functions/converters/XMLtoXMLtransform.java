/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.converters;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.resources.XMLResource;

@TAResourceConverter("xslt")
public class XMLtoXMLtransform extends AbstractXSLTTransform<XMLResource> implements ResourceConverter<XMLResource, XMLResource>{

	static final Logger LOGGER=LoggerFactory.getLogger(XMLtoXMLtransform.class);
	
	@Override
	public XMLResource convert(XMLResource input) {
		try {
			File f = applyTransformToFile(input);
			return new GeneratedXmlResource(f);
		} catch (IOException e) {
			throw new InstructionRuntimeException("Applying XSLT failed", e);
		}
	}

	@Override
	protected boolean defaultNormalizeSetting() {
		return true;
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.resources;

import java.io.File;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;

@TAResource("xslt")
public class XSLTResource implements Resource<XSLTResource>{
	
	private File stylesheet;
	
	/** Default constructor for Spring enumeration only. */
	public XSLTResource() {}
	
	public XSLTResource(File stylesheet) {
		this.stylesheet = stylesheet;
	}

	public File getStylesheet() {
		return stylesheet;
	}

	@Override
	public void cleanUp() {
		//noop: we never create anything
	}

	@Override
	public XSLTResource copy() {
			return new XSLTResource(stylesheet);
	}

	@Override
	public String toString() {
		return super.toString()+stylesheet==null?"{identity}":stylesheet.getAbsolutePath();
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.converters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.resources.XMLResource;

import org.squashtest.ta.xml.functions.library.Transform;
import org.squashtest.ta.xml.functions.resources.XSLTResource;

public abstract class AbstractXSLTTransform<OUTPUT extends Resource<OUTPUT>> implements ResourceConverter<XMLResource, OUTPUT >{

	private static final String OUTPUT_NORMALIZE_KEY = "normalize";

	private final Logger LOGGER=LoggerFactory.getLogger(getClass());
	
	private File xslt;
	protected Boolean normalize;

	public AbstractXSLTTransform() {
		super();
	}
        
	protected File getXslt(){
		return xslt;
	}        

	@Override
	public void addConfiguration(Collection<Resource<?>> config) {
		for (Resource<?> configElmt : config) {
			addconfigResource(configElmt);
		}
	}

	private void addconfigResource(Resource<?> configElmt) {
		
		if (configElmt instanceof XSLTResource) {
			if (xslt == null) {
				xslt = ((XSLTResource) configElmt).getStylesheet();
			} else {
				LOGGER.warn("Ignored extra XSLT specification {}.", configElmt);
			}
		} else if (configElmt instanceof FileResource) {
			FileResource cfg = (FileResource) configElmt;
			addConfigFromProperties(configElmt, cfg);
		} else {
			LOGGER.warn("Ignored unrecognized USING resource {}.", configElmt);
		}
	}

	@Override
	public void cleanUp() {
		// noop
	}

	@Override
	public float rateRelevance(XMLResource arg0) {
		return 0.4f;
	}

	protected File applyTransformToFile(XMLResource arg0)
			throws IOException {
				Transform t = new Transform(getXslt(), normalize == null ? defaultNormalizeSetting()
						: normalize);
				File f = File.createTempFile("xslt_prod", ".xml",TempDir.getExecutionTempDir());
				t.transform(arg0.getXMLFile(), f);
				return f;
			}

	protected abstract boolean defaultNormalizeSetting();

	private void addConfigFromProperties(Resource<?> configElmt, FileResource cfg) {
		if (normalize == null) {
			Properties content = new Properties();
			try(FileInputStream cfgContent=new FileInputStream(cfg.getFile())){
				content.load(cfgContent);
			}catch(IOException e){
				throw new IllegalConfigurationException("Failed to load configuration resource", e);
			}
			
			if (content.containsKey(OUTPUT_NORMALIZE_KEY)) {
				normalize = Boolean.parseBoolean(content
						.getProperty(OUTPUT_NORMALIZE_KEY));
			}
		} else {
			LOGGER.warn("Ignored conflicting configuration resource ",
					configElmt);
		}
	}

}
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 	<xsl:template match="/dataset">
 		<dataset>
 		<xsl:for-each select="my_table">
 			<xsl:sort select="@id"/>
 			<xsl:if test="@id=1">
 				<xsl:copy-of select="."></xsl:copy-of>
 			</xsl:if>
 			<xsl:if test="@id=2">
 				<xsl:copy-of select="."></xsl:copy-of>
 			</xsl:if>
 		</xsl:for-each>
 		</dataset>
 	</xsl:template>
 </xsl:stylesheet>
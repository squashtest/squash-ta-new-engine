/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.xml.functions.library;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

/**
 * Unit test for simple App.
 */
public class XMLTransformTest extends ResourceExtractorTestBase{
    
	private static final Logger LOGGER = LoggerFactory.getLogger(XMLTransformTest.class);
	
	static File src;
	static File xslt;

	@BeforeClass
	public static void deployRsc() throws IOException {
                src = createFile4Class("/sample-dataset.xml");
                xslt = createFile4Class("/table1n2sorted.xslt");
	}

	@Rule
	public TestName name = new TestName();

	@Before
	public void giveTestName(){
		LOGGER.info("Cas: "+name.getMethodName());
	}
	
	@Test
	public void xsltResult() throws IOException {
		File actual = createNtrackFile();
		new Transform(xslt, true).transform(src, actual);
		checkActualContentAgainstExpected(actual, "/expectedXsltResult.xml");
	}

	@Test
	public void identityReindentedResult() throws IOException {
		File actual = createNtrackFile();
		new Transform().transform(src, actual);
		checkActualContentAgainstExpected(actual, "/expectedIdentityReindentedResult.xml");
	}
	
	@Test
	public void identityNoindentResult() throws IOException {
		File actual = createNtrackFile();
		new Transform(null,false).transform(src, actual);
                /* 
                This test will break when we update license headers 
                => to FIX it, manually update the expected result file.
                (the plugin cannot manage headers properly in these files)
                */
		checkActualContentAgainstExpected(actual, "/expectedIdentityNoindentResult.xml");
	}

}

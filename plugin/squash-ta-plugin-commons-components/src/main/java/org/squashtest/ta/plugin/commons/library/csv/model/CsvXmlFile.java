/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.model;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Base class of the XML transpose of a CSV FILE. A CSV file is a collection of 
 * lines. If comments are allowed this is stored as a list of fields.
 * 
 * @author fgautier inpired by work done by edegenetais
 * @author edegenetais
 */
@XmlRootElement(name = "file")
public class CsvXmlFile {

    /**
     * The lines the CSV holds.
     */
    @XmlElement(name = "line")
    private List<CsvXmlLine> lines;

    /**
     * Wrapper to a collection holding comments realtive to a column
     */
    @XmlElement
    private CsvXmlComments comments;
    /**
     * Default constructor for Jaxb enumeration.
     */
    public CsvXmlFile() { /* Jaxb */ }

    /**
     * Paramtrized constructor.
     * 
     * @param lines : The lines the CSV holds.
     */
    public CsvXmlFile(List<CsvXmlLine> lines) {
        this.lines = lines;
    }

    /**
     * Fully paramtrized constructor.
     * 
     * @param lines : The lines the CSV holds.
     * @param  comments : The column definition comments.
     */
    public CsvXmlFile(List<CsvXmlLine> lines, List<CsvXmlField> comments) {
        this.lines = lines;
        this.comments = new CsvXmlComments(comments);
    }

    
    /**
     * Simple getter for Jaxb.
     * 
     * @return An unmodifiable collections of lines this CSV holds.
     */
    public List<CsvXmlLine> getLines() {
        return Collections.unmodifiableList(lines);
    }

    /**
     * Simple getter for Jaxb.
     * 
     * @return An unmodifiable collections of fields containing the column definition comments.
     */
    public CsvXmlComments getComments() {
        return comments;
    }
    
    
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.commons.library.csv.CSVEscapeCharacter;
import org.squashtest.ta.plugin.commons.library.csv.CSVNullEntry;
import org.squashtest.ta.plugin.commons.library.csv.CSVQuoteCharacter;
import org.squashtest.ta.plugin.commons.library.csv.CSVSeparator;
import org.squashtest.ta.plugin.commons.library.csv.ConfigEnum;

/**
 * A library containing the separation character of a CSV file
 * The default is "," as in Comma Separated Values
 * @author FOG
 *
 */
@TAResource("csv.conf")
public class CSVConfiguration implements Resource<CSVConfiguration>{
	
	private CSVSeparator separationCharacter = CSVSeparator.COMMA;
	private CSVNullEntry nullEntry = CSVNullEntry.EMPTY;
	private CSVQuoteCharacter quoteCharacter = CSVQuoteCharacter.DOUBLE_QUOTE;
	private CSVEscapeCharacter esacapeCharacter = CSVEscapeCharacter.BACK_SLASH;
	
	/**
	 * Default constructor: creates a default configuration instance.
	 */
	public CSVConfiguration() {}
	
	/**
	 * This constructors takes a varying number of arguments depending what you want to configure at instanciation.
	 * <p> You can configure up to 4 things :<p>
	 * <ul>
	 * 	<li>The separation character</li>
         *      <li>The quote character.</li>
	 * 	<li>The Entry Escape character, in case you want to write an entry containing the separation character</li>
	 *      <li>The Null Entry</li>
	 * </ul>
	 * 
	 * You Can cal these constructors with the different Enum in the order you wish
	 */
	public CSVConfiguration(ConfigEnum... configEnum) {
            for(ConfigEnum element:configEnum){
                setConfig(element);
            }
	}
	
	@Override
	public CSVConfiguration copy() {
		// noop
		return null;
	}

	@Override
	public void cleanUp() {
		//noop	
	}

	/**
	 * Getters of the different configuration parameters
	 * @return
	 */
	public CSVSeparator getSeparationCharacter(){
		return separationCharacter;
	}
	
	public CSVQuoteCharacter getQuoteCharacter(){
		return quoteCharacter;
	}
	
	public CSVNullEntry getNullEntry(){
		return nullEntry;
	}
	
	public CSVEscapeCharacter getEscapeCharacter(){
		return esacapeCharacter;
	}
	
        /**
         * Should be refactored by giving config enum control on what is configured...
         * @param configEnum 
         */
	private void setConfig (ConfigEnum configEnum){
		if (configEnum instanceof CSVQuoteCharacter){
			quoteCharacter = (CSVQuoteCharacter)configEnum;
		} else if (configEnum instanceof CSVNullEntry){
			nullEntry = (CSVNullEntry)configEnum;
		} else if (configEnum instanceof CSVSeparator){
			separationCharacter = (CSVSeparator)configEnum;
		} else if (configEnum instanceof CSVEscapeCharacter){
			esacapeCharacter = (CSVEscapeCharacter)configEnum;
		}
	}
}

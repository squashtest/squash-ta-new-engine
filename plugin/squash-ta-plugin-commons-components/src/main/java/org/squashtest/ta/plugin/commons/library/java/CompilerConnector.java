/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Configures and triggers the compile task.
 *
 * @author edegenetais
 *
 */
public class CompilerConnector extends ProcessConnector {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompilerConnector.class);

    private ToolFactory toolFactory=new ToolFactory();  

/**
 * isStandardPlacement 
 * true: -> for satndard (no flat) files placement in bundle
 * false: default value -> for flat files placement in bundle 
 */
private boolean isStandardPlacement = false;
        
        public CompilerConnector(boolean isStandardPlacement) {
        this.isStandardPlacement = isStandardPlacement;
}        
        public CompilerConnector() {
}       
        
	private class CompilationReportImpl implements CompilationReport {

    private boolean status;
    private Set<String> compiledClassNames;
    private String compilerMessages;

    /**
     * Full initialization constructor.
     *
     * @param status status to report.
     * @param compiledClassNames class names to register.
     * @param compilerMessages compiler messages report to register.
     */
    public CompilationReportImpl(boolean status,
            Set<String> compiledClassNames, String compilerMessages) {
        this.status = status;
        this.compiledClassNames = Collections.unmodifiableSet(compiledClassNames);
        this.compilerMessages = compilerMessages;
    }

    @Override
    public boolean isSuccess() {
        return status;
    }

    @Override
    public Set<String> getCompiledClassNames() {
        return compiledClassNames;
    }

    @Override
    public String getCompilerMessages() {
        return compilerMessages;
    }

}

/**
 * The usual factory to decouple for testing business.
 */
class ToolFactory {

    JavaCompiler getJavaCompiler() {
        return ToolProvider.getSystemJavaCompiler();
    }
}

	
	/**
	 * Compile a set of source files.
	 * @param sourceFiles the source files to compile.
	 * @param options options for the compiler.
	 * @param compiledClassLocation directory where class files will be written.
	 * @return
	 */
	public CompilationReport compile(Set<File> sourceFiles, List<String>options,File compiledClassLocation){
		
		//we will now transmit the context classpath to the compiler to include jars from the mojo classpath
		LOGGER.debug("Base vm classpath: "+System.getProperty("java.class.path"));
		
                if (LOGGER.isDebugEnabled()) {
                for (File file:sourceFiles) {
                LOGGER.debug("sources: " +  file.getName());
                }
                }
                
		JavaCompiler compiler=toolFactory.getJavaCompiler();
                if(compiler==null){
                    throw new CompilerEnvironmentException("No compiler is available. Please check that you are running from a JDK as required.");
                }
		StandardJavaFileManager baseManager=compiler.getStandardFileManager(null, null, null);
		
		setCompilerClasspath(baseManager);
		if(LOGGER.isDebugEnabled()){
			Iterable<? extends File> compilerClasspath=baseManager.getLocation(StandardLocation.CLASS_PATH);
			StringBuilder compilerClassPathBuilder=new StringBuilder("Compiler classpath:\n");
			for(File element:compilerClasspath){
				compilerClassPathBuilder.append(element.getAbsolutePath()).append("\n");
			}
			LOGGER.debug(compilerClassPathBuilder.toString());
		}
		
		
		Iterable<? extends JavaFileObject> compilationUnits=baseManager.getJavaFileObjects(sourceFiles.toArray(new File[sourceFiles.size()]));
		StringWriter errorReportBuffer=new StringWriter();
		List<String> effectiveOptions;
		if(options==null || options.isEmpty()){
			effectiveOptions=null;
		}else{
			effectiveOptions=options;
		}
                
                AbstractPlacementJavaFileManager delegateManager;
                if (isStandardPlacement)
                {
                delegateManager=new StandardPlacementJavaFileManager(baseManager, compiledClassLocation);		
                }
                else 
                {
                    delegateManager = new PlacementJavaFileManager(baseManager, compiledClassLocation);
                }
                CompilationTask compilationTask=compiler.getTask(errorReportBuffer, delegateManager, null, effectiveOptions, null, compilationUnits);
                boolean result=compilationTask.call();
		return new CompilationReportImpl(result, delegateManager.getRegisteredClassNames(), errorReportBuffer.toString());
	}

	/**
	 * So that libraries are found!
	 * @param baseManager the standard file manager returned by the compiler.
	 */
	private void setCompilerClasspath(StandardJavaFileManager baseManager) {
		try {
			List<File> classPath = computeJarClassPath();
			baseManager.setLocation(StandardLocation.CLASS_PATH, classPath);
		} catch (IOException e) {
			throw new JavaClassPathException("Mojo to compiler classpath transmission failed.",e);
		} catch (URISyntaxException e) {
			throw new JavaClassPathException("Mojo to compiler classpath transmission failed.",e);
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.helpers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 *
 * @author cjuillard
 */
public class AttachmentMangementHelper {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentMangementHelper.class);
    
    AttachmentMangementHelper() {};

    public static void injectAttachedFile(File attachment, ArrayList<ResourceAndContext> context) throws IOException {
        ResourceAndContext additionalReportContext = buildAttachementRAC(attachment);
        LOGGER.debug("Adding attachment {} to context as {}", attachment.getAbsolutePath(), additionalReportContext.getMetadata().getName());
        context.add(additionalReportContext);
    }

    public static ResourceAndContext buildAttachementRAC(File additionalReport) throws IOException {
        ResourceAndContext additionalReportContext = new ResourceAndContext();
        if (additionalReport.isFile()) {
            additionalReportContext.setResource(new FileResource(additionalReport).copy());
        } else if (additionalReport.isDirectory()) {
            File reportCopy = FileTree.FILE_TREE.copyToTemp(additionalReport);
            additionalReportContext.setResource(new FileResource(reportCopy));
        }

        final String nomAttachement = additionalReport.getName();
        additionalReportContext.setMetadata(new ExecutionReportResourceMetadata(AttachmentMangementHelper.class, new Properties(), FileResource.class, nomAttachement));
        return additionalReportContext;
    }
}

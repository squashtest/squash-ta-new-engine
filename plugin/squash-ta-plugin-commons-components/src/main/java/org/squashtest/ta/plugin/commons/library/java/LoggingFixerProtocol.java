/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import org.slf4j.LoggerFactory;

/**
 *
 * @author edegenetais
 */
public class LoggingFixerProtocol {
    public static final String SLF4J_BINDER_CLASS_NAME = "org.slf4j.impl.StaticLoggerBinder";
    
    public static final String WARN_LEVEL="warn";
    public static final String ERROR_LEVEL = "error";
    public static final String DEBUG_LEVEL = "debug";
    public static final String INFO_LEVEL = "info";
    public static final String TRACE_LEVEL = "trace";
    
    public boolean accept(String msg) {
        try {
            getClass().getClassLoader().loadClass(SLF4J_BINDER_CLASS_NAME);
            return false;
        } catch (ClassNotFoundException e) {
            LoggerFactory.getLogger(LoggingFixerProtocol.class).debug("Accept turned on because the {} class is not found.", SLF4J_BINDER_CLASS_NAME, e);
            return "STARTLOG".equals(msg);
        }
    }

    public <STATUS> Element<STATUS> getPipeElement(Element<STATUS> endOfLogElement){
        return new LoggingFixerElement<STATUS>(endOfLogElement);
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv;

public enum CSVQuoteCharacter implements ConfigEnum{
	DOUBLE_QUOTE("\""),
	QUOTE("'");

        private static String listOfValidQuotes=null;
        
	private final String entryEscapeCharacter;
    
    private CSVQuoteCharacter(String character) {
        this.entryEscapeCharacter = character;
    }
    
    public String getCharacter(){
    	return entryEscapeCharacter;
    }
    
    public static CSVQuoteCharacter fromString(String s){
        for(CSVQuoteCharacter candidat:values()){
            if(candidat.getCharacter().equals(s)){
                return candidat;
            }
        }
        if(listOfValidQuotes==null){
            StringBuilder builder=new StringBuilder("{");
            for(CSVQuoteCharacter sepDef:values()){
                builder.append(sepDef.entryEscapeCharacter).append(",");
            }
            if(builder.length()>1){
                builder.setLength(builder.length()-1);
            }
            builder.append("}");
            listOfValidQuotes=builder.toString();
        }
        throw new IllegalArgumentException(s+" is no valid CSVQuoteCharacter. Use one of "+listOfValidQuotes);
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date;

import java.util.Calendar;
import org.slf4j.LoggerFactory;

/**
 *
 * @author edegenetais
 */
public class EnsureWorkingDay extends AbstractWorkingDayFunctionBase implements DateFunction{

    @Override
    public Calendar evaluate(Calendar inputDate, String argument) {
        Calendar result=inputDate;
        final String increment;
        switch(argument.toUpperCase()){
            case "AFTER":
                LoggerFactory.getLogger(EnsureWorkingDay.class).debug("Will switch to first working day equals to the date or after it.");
                increment="1";
                break;
            case "BEFORE":
                LoggerFactory.getLogger(EnsureWorkingDay.class).debug("Will switch to last working day equals to the date or before it.");
                increment="-1";
                break;
            default:
                throw new IllegalArgumentException(argument+" is not supported (chose before or after)");
        }
        while(isNonWorkingDay(result)){
            result=delegate.evaluate(result, increment);
        }
        return result;
    }
    
}

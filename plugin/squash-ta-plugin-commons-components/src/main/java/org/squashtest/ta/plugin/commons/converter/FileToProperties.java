/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.core.tools.io.PropertiesLoader;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

/**
 * File To Properties Resource Converts a Properties File entry into a pseudo primary key dbunit filter
 * <p>
 * Every line of the properties file must have the following specifications :
 * <p>
 * <ul>
 * <li>The property name</li>
 * <li>spaces or tabs</li>
 * <li>=</li>
 * <li>spaces or tabs</li>
 * <li>The property value</li>
 * </ul>
 * 
 * @author fgaillard
 * 
 */
@TAResourceConverter("structured")
public class FileToProperties implements ResourceConverter<FileResource, PropertiesResource> {
    
        private static final Logger LOGGER = LoggerFactory.getLogger(FileToProperties.class);
        
	private static final Pattern VALID_START_MULTILINE_PROPERTY = Pattern.compile("^.*\\\\$");
	private static final Pattern VALID_CONTINUE_MULTILINE_PROPERTY = Pattern.compile("^.*$");
	private static final Pattern VALID_NON_EMPTY_PROPERTY_LINE = Pattern.compile("^.*=.*$");
        private static final Pattern VALID_EMPTY_PROPERTY_LINE = Pattern.compile("^.*=$");
	private static final Pattern VALID_COMMENT_LINE = Pattern.compile("^#.*$");
	private static final PropertiesLoader PROPERTIES_LOADER = new PropertiesLoader();

        private static final String PARM_VALUE_CONFLICT_WARNING = "Ignoring extra configuration value {}, {} is already set.";
        private static final String LEGACY_PPK_CHECK_PARM_NAME = "legacyPPkCheck";
        private static final String ENCODING_PARM_NAME = "encoding";
        
	private String encoding;
        private Boolean legacyPPKControl;

	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToProperties() {
            //empty constructor
	}

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		for(Resource<?> res:configuration){
			if(res instanceof FileResource){
                    try {
                        final File configFile = ((FileResource) res).getFile();
                        if (OptionsReader.BASIC_READER.isOptions(configFile)) {
                            parseConversionOptions(configFile);
                        } else {
                            LOGGER.warn("Ignoring unexpected non-option configuration file {}", configFile);
                        }
                    } catch (IOException e) {
                        throw new IllegalConfigurationException("Filed to read file to properties converter configuration", e);
                    }
                }
            }
        }


        protected void parseConversionOptions(final File configFile) throws IllegalArgumentException, IOException {
            final Map<String, String> options = OptionsReader.BASIC_READER.getOptions(configFile);
            for (String parmName : options.keySet()) {
                extractOptionsParmIfExistsAndNew(options, parmName);
            }
        }

        private void extractOptionsParmIfExistsAndNew(final Map<String, String> options, String parmName) {
            if(options.containsKey(parmName)){
                final String parmString = options.get(parmName);
                switch(parmName){
                    case ENCODING_PARM_NAME:
                        if(encoding==null){
                            encoding = parmString;
                        }else{
                            LOGGER.warn(PARM_VALUE_CONFLICT_WARNING, parmString,parmName);
                        }
                        break;
                    case LEGACY_PPK_CHECK_PARM_NAME:
                        if(legacyPPKControl==null){
                            legacyPPKControl=Boolean.parseBoolean(parmString);
                        }else{
                            LOGGER.warn(PARM_VALUE_CONFLICT_WARNING, parmString,parmName);
                        }
                        break;
                    default:
                        LOGGER.warn("Ingoring unsupported parm {} with value {}.",parmName,parmString);
                }
            }
        }

	@Override
	public PropertiesResource convert(FileResource resource) {
		Properties properties;

		try {
			
			SimpleLinesData data;
			if(encoding==null){
				data = new SimpleLinesData(resource.getFile().getPath());
			}else{
				data = new SimpleLinesData(resource.getFile().getPath(),encoding);
			}
			List<String> lines = data.getLines();
			boolean previousLineEndsWithBackSlash = false;
			for (int i=0;i<lines.size();i++) {
				previousLineEndsWithBackSlash = checkLineValidity(previousLineEndsWithBackSlash, lines.get(i),i);
			}
			if(encoding==null){
				properties = PROPERTIES_LOADER.load(resource.getFile());
			}else{
				properties = PROPERTIES_LOADER.load(resource.getFile(),encoding);
			}
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("Properties file not found", fnfe);
		} catch (IOException ioe) {
			throw new BadDataException("Wrongly Configured properties file", ioe);
		}
		return new PropertiesResource(properties);
	}

	/**
	 * Check wether the current line is a valid properties file line, based on its content and wehter we are in a
	 * multiline or not.
	 * 
	 * @param previousLineEndsWithBackSlash
	 *            if this boolean indicator is <code>true</code>, then we are in a multiline property declaration.
	 * @param line
	 *            current line content
         * @param i current line number for error reporting.
	 * @return <code>true</code> if the current line is valid, <code>false</code> other wise.
	 */
	private boolean checkLineValidity(boolean previousLineEndsWithBackSlash,
			String line,int i) {
                if(LOGGER.isDebugEnabled()){
                    if(previousLineEndsWithBackSlash){
                        LOGGER.debug("In multiline: "+line);
                    }else{
                        LOGGER.debug("In monoline : "+line);
                    }
                }
                //Here we warn of empty property definitions, because it may be trouble for configured components
                if(LOGGER.isWarnEnabled() && VALID_EMPTY_PROPERTY_LINE.matcher(line).matches()){
                    LOGGER.warn("Empty property definition: '{}'",line);
                }
		if (!(VALID_COMMENT_LINE.matcher(line).matches() || 
                        VALID_NON_EMPTY_PROPERTY_LINE.matcher(line).matches() || 
                        VALID_EMPTY_PROPERTY_LINE.matcher(line).matches() || 
                        isBlank(line))){
			//We check if previous line ended with a backslash
			if (previousLineEndsWithBackSlash){
				if (!( VALID_CONTINUE_MULTILINE_PROPERTY.matcher(line).matches())){
                                    LOGGER.debug("Line refused (not a valid multiline continuation '{}')",line);
					throw new BadDataException("Invalid properties file content\nline "+i+" '"+line+"' is not a valid multiline property line.");
				}
                                doCheckPPKvalidityCondition(line, i);
			} else {
                                LOGGER.debug("Line refused (not a valid properties file single line.)");
				throw new BadDataException("Invalid properties file content\nline "+i+" '"+line+"' is not a valid property file entry");
			}
		}
		if (VALID_START_MULTILINE_PROPERTY.matcher(line).matches()){
			previousLineEndsWithBackSlash = true;
		} else {
			previousLineEndsWithBackSlash = false;
                        doCheckPPKvalidityCondition(line, i);
		}
		return previousLineEndsWithBackSlash;
	}

        protected void doCheckPPKvalidityCondition(String line, int i) throws BadDataException {
                    // we want something like
                    // MY_TABLE MY_FIRST_PRIMARY_KEY,MY_SECOND_PRIMARY_KEY,....
                    // that does NOT end with a comma.
            if(Boolean.TRUE.equals(legacyPPKControl) && line.endsWith(",")){
                throw new BadDataException("Invalid properties file content\nline "+i+" '"+line+"' is not valid in a ppk spec properties file.");
            }
        }
    
        private boolean isBlank(String line) {
		if ("".equals(line)) {
			return true;
		}
		String[] tokens = line.split("\\s");
		return (tokens.length == 0);
	}
	
	@Override
	public void cleanUp() {
		// noop
	}

}

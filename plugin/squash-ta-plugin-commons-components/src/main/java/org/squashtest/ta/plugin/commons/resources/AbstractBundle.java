/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.File;
import java.io.IOException;

import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

public abstract class AbstractBundle {

	private File mainFile;
	private File baseDirectory;

	// base for the default constructor used for Spring enumeration in concrete
	// subclasses)
	protected AbstractBundle() {
	}

	/**
	 * @param mainFile
	 * @param baseDirectory
	 */
	public AbstractBundle(File baseDirectory,File mainFile) {
		try {
			this.mainFile = mainFile;
			this.baseDirectory = baseDirectory;
			if (!isValidBaseDirectory()) {
				throw new IllegalArgumentException(
						"ResourceBundle : creation of a bundle failed : the base directory cannot be null");
			} else if (!isValidMainFile()) {
				throw new IllegalArgumentException(
						"ResourceBundle : creation of a bundle failed : supplied main File '"
								+ mainFile.getPath()
								+ "' is not contained in directory '"
								+ this.baseDirectory + "'");
			}
		} catch (IOException ex) {
			throw new InstructionRuntimeException(
					"BundleResource : an error occured while checking consistency of file '"
							+ mainFile.getPath() + "' and directory '"
							+ this.baseDirectory + "'", ex);

		}
	}
	public File getBase(){
		return baseDirectory;
	}
	
	
	public File getMain(){
		return mainFile;
	}
	
	/* ***************************************
	 *  check
	 * ***************************************/

	protected boolean isValidBaseDirectory() {
		return ((baseDirectory != null) && (baseDirectory.exists()) && (baseDirectory
				.isDirectory()));
	}

	private boolean isValidMainFile() throws IOException {
		boolean isValidMainFile;
		if (mainFile == null) {
			isValidMainFile = true;
		} else if (!(mainFile.exists() && mainFile.isFile())) {
			isValidMainFile = false;
		} else {
			isValidMainFile = FileTree.staticCheapCheckIfInsideBaseDir(baseDirectory,
					mainFile);
		}
		return isValidMainFile;
	}

	/**
	 * returns the path of the main, relative to the base directory. If the main
	 * is null, returns null.
	 * 
	 * @return
	 */
	public String getMainRelativePath() {
		if (mainFile == null) {
			return null;
		} else {
			try {
				return FileTree.staticGetRelativePath(baseDirectory, mainFile);
			} catch (IOException ex) {
				throw new InstructionRuntimeException(
						"BundleResource : an error occured while resolving '"
								+ mainFile.getPath() + "' and '"
								+ baseDirectory.getPath() + "'", ex);
			}
		}
	}
}
/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.IOException;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.commons.resources.XMLResource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * File To XML Converter
 * Converts a File entry (XML File obviously) into an XMLResource Type 
 * (encapsulated XML File which proves that the XML is "well formed")
 * <p> The file must a correct XML implementation<p>
 * 
 * @author fgaillard
 *
 */
@TAResourceConverter("structured")
public class FileToXML implements ResourceConverter<FileResource, XMLResource> {

	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToXML(){
            //empty constructor
        }
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
            new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public XMLResource convert(FileResource resource) {
		XMLResource resultResource = null;
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
		 
			DefaultHandler handler = new DefaultHandler() {};
			
			saxParser.parse(resource.getFile(), handler);
		} catch (SAXException se) {
			throw new BadDataException("The input file content is not valid XML code: ("+se.getMessage()+")",se);
		} catch (
                        IOException | 
                        ParserConfigurationException e
                        ) {
			throw new BadDataException("Technical failure while trying to convert FileResource to XmlResource",e);
		} 
		resultResource = new XMLResource(resource.getFile());
		return resultResource;
	}

	@Override
	public void cleanUp() {
            //noop
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * Object to enumerate a transmitted source tree. 
 * The supported source code is as follows:
 * <ul>
 * <li>java source files: will be parsed as source code</li>
 * <li>other files: will be copied as is and served as resource files</li>
 * </ul>
 * A major restriction is that class files or jar archives in this tree won't be recognized as dependency
 * implementations during the compiling process.
 * 
 * @see org.squashtest.ta.plugin.commons.converter.FileToJavaCodeBundle converter class 
 * for details about dependency resolution during compilation.
 * 
 * @author edegenetais
 * 
 */
public class SourceEnumerator {
	// Logger of this.class
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceEnumerator.class);	

	private static final String JAVA_FILE_EXTENSION = ".java";
	private static final String DEFAULT_RESOURCE_DIRNAME = "resources";
	private static final String DEFAULT_JAVA_DIRNAME = "java";

        public static final Pattern PUBLIC_CLASS_NAME_EXTRACTOR = Jdk7RegexCompat.INSTANCE.compile("^\\h*public class (?<className>[A-Za-z][A-Za-z0-9]*)(?:\\<[A-Za-z,]+>)?\\s*(?:\\{.*)?$");
        
	private File codeTree;
	private File resourceTree;
	
	// a boolean to check if files are presents in java subdirectory
	private boolean bundleEmpty = false;
	// a boolean to check if seleniums scripts are java files 
	private boolean wrongExtensionNameFile = false;
	// a boolean to check if java directory is present in selenium directory
	private boolean javaIsNotPresent = false;
	

	public SourceEnumerator(File sourceLocation) {
		if (sourceLocation.isDirectory()) {
			codeTree = new File(sourceLocation, DEFAULT_JAVA_DIRNAME);
			resourceTree = new File(sourceLocation, DEFAULT_RESOURCE_DIRNAME);
		} else {// case of an isolated source file
			codeTree = sourceLocation;
			resourceTree = codeTree;
		}
	}

	public Set<File> getJavaSourceFiles() {
		Set<File> sourceFiles = new HashSet<>();
		if (codeTree.isDirectory()) {
			List<File> allCodeFiles = FILE_TREE.enumerate(codeTree, EnumerationMode.FILES_ONLY);

			if (allCodeFiles.isEmpty()) {
				bundleEmpty = true;
			}
                        /*
                        *  FIXME : this wrong extension thingy is broken...only triggering it if no .java file is seen
                        *  makes it a triffle better, but it should be rethough top to bottom...
                        */
                        wrongExtensionNameFile=true;
			for (File candidate : allCodeFiles) {
				if (isJavaSource(candidate)) {
					sourceFiles.add(candidate);					
                                        wrongExtensionNameFile = false;
				} else {
					LOGGER.warn(candidate.getName() + " is not a java file");
				}
			}
		} else {
			javaIsNotPresent = true;
			if (codeTree.isFile() && codeTree.getName().endsWith(JAVA_FILE_EXTENSION)) {
                            sourceFiles.add(copyToPublicClassConsistentFileName());
			}
		}
		return sourceFiles;
	}

    public File copyToPublicClassConsistentFileName() throws InstructionRuntimeException {
        
        try (final BufferedReader fr = new BufferedReader(new FileReader(codeTree))) {
            String publicClassName=null;

            String line=fr.readLine();
            while (line!=null && publicClassName == null) {
                final Matcher classNameMatcher = PUBLIC_CLASS_NAME_EXTRACTOR.matcher(line);
                if (classNameMatcher.matches()) {
                    publicClassName = classNameMatcher.group("className");
                }
                line=fr.readLine();
            }
            int nbPublicClasses=1;
            while (line!=null) {
                final Matcher classNameMatcher1 = PUBLIC_CLASS_NAME_EXTRACTOR.matcher(line);
                if (classNameMatcher1.matches()) {
                    LOGGER.warn("Additional public classname {} found, totalling {} pûblic classes in the file. The compiler will most probably balk at this and fail.", classNameMatcher1.group("className"), nbPublicClasses);
                }
                line=fr.readLine();
            }
            
            if(publicClassName==null){
                LOGGER.warn("Public class name not found. There is a high hazard of compile time or runtime failure. You should consider using the bundle resource configuration (inside a <resourcename>/java/ directory) for this class.");
                return codeTree;
            }else{
                File newSourceDir=FileTree.FILE_TREE.createTempDirectory();
                File newSource1 = new File(newSourceDir, publicClassName + JAVA_FILE_EXTENSION);
                FileUtils.copyFile(codeTree, newSource1);
                return newSource1;
            }
            
        }catch (IOException ex) {
            //this should NEVER happen, probably something's rotten in the realm of Denmark!
            throw new InstructionRuntimeException("Failed to open signle source file ! Something seems rotten in the realm of Denmark, please check that the host operating system is operating properly!", ex);
        }
    }

	public boolean isJavaSource(File candidate) {
		return candidate.getName().endsWith(JAVA_FILE_EXTENSION);
	}

	public Set<File> getResourceFiles() {
		Set<File> resourceFiles = listResourceFromDefaultDirectory();

		/*
		 * strictly speaking, we should always separate resources and code source, but let he that is without sin, cast
		 * the first stone...
		 */
		listResourcesFromExecutableSourceDirectory(resourceFiles);

		return resourceFiles;
	}

	private void listResourcesFromExecutableSourceDirectory(Set<File> resourceFiles) {
		if (codeTree.isDirectory()) {
			List<File> allCodeFiles = FILE_TREE.enumerate(codeTree, EnumerationMode.FILES_ONLY);
			for (File candidate : allCodeFiles) {
				if (isResource(candidate)) {
					resourceFiles.add(candidate);
				}
			}
		} else if (codeTree.isFile() && !codeTree.getName().endsWith(JAVA_FILE_EXTENSION)) {
			resourceFiles.add(codeTree);
		}
	}

	private Set<File> listResourceFromDefaultDirectory() {
		Set<File> resourceFiles;
		if (resourceTree.isDirectory()) {
			resourceFiles = new HashSet<>(FILE_TREE.enumerate(resourceTree, EnumerationMode.FILES_ONLY));
		} else {
			resourceFiles = new HashSet<>(1);
			if (resourceTree.isFile() && isResource(resourceTree)) {
				resourceFiles.add(resourceTree);
			}
		}
		return resourceFiles;
	}

	public String getResourceRelativePath(File resourcePath) throws IOException {
		String path = FILE_TREE.getRelativePath(resourceTree, resourcePath);
		if (path == null) {/*
							 * strictly speaking, we should always separate resources and code source, but let he that
							 * is without sin, cast the first stone...
							 */
			path = FILE_TREE.getRelativePath(codeTree, resourcePath);
		}
		return path;
	}

	public boolean isResource(File candidate) {
		return !isJavaSource(candidate);
	}

	public boolean isBundleEmpty() {
		return bundleEmpty;
	}

	public boolean isWrongExtensionNameFile() {
		return wrongExtensionNameFile;
	}
	
	public boolean isJavaIsNotPresent() {
		return javaIsNotPresent;
	}
}

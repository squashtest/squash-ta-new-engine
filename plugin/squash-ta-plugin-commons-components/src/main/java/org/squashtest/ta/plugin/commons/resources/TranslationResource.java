/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.commons.library.reporting.Translator;

/**
 * This resource is used to configure message translation in components.
 * This may come in handy when the component is generic and will generate generic messages,
 * and context specific translation may enhance report readability 
 * (are your reports for technogeek eyes only, 
 * or may they fall under innocent ... er... tech unaware, business only, eyes?).
 * @author edegenetais
 */
@TAResource("cfg.translation")
public class TranslationResource implements Resource<TranslationResource>{
    
    private Translator translator;

    /**
    * Default constructor for Spring enumeration only.
    */
    public TranslationResource(){
    //default constructor
    }
    
    /**
     * 
     * @param translator 
     */
    public TranslationResource(Translator translator) {
        this.translator=translator;
    }
    
    public Translator getTranslator(){
        return translator;
    }
    
    @Override
    public TranslationResource copy() {
        return new TranslationResource(translator.duplicate());
    }

    @Override
    public void cleanUp() {
        //noop
    }
    
}

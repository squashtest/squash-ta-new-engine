/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.CSVParsingError;
import org.squashtest.ta.plugin.commons.resources.CSVResource;
import org.squashtest.ta.plugin.commons.resources.XMLResource;

/**
 *
 * @author fgautier
 */
@TAResourceConverter("csv.xml")
public class CSVToXMLConverter extends AbstractCsvToXmlConverter<XMLResource> implements ResourceConverter<CSVResource, XMLResource>{

    @Override
    public XMLResource convert(CSVResource input) {
         try {
            File generatedFiled = applyTransformation(input);
            return new XMLResource(generatedFiled).copy();
        } catch (CSVParsingError ex) {
            LoggerFactory.getLogger(CSVToXMLFileConverter.class).warn("Illegal CSVResource {}", input);
            throw new BadDataException("Illegal CSVResource" + input, ex);
        }
    }
    
}

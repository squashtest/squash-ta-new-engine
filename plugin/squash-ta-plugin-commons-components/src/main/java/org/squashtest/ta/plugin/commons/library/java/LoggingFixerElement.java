/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This output stream analysis element defines the part of the IPC protocol used to transmit logging requests 
 * from the slave process.
 * @author edegenetais
 */
class LoggingFixerElement<STATUS> implements Element<STATUS> {
    private static final Logger LOGGER=LoggerFactory.getLogger(LoggingFixerElement.class);
    
    private static final Pattern LOG_LINE = Pattern.compile("^LINE:(.*)$");
    private static final Pattern LOGGER_LINE = Pattern.compile("^LOGGER:(.*)$");
    private static final Pattern LEVEL_LINE = Pattern.compile("LEVEL:(.*)");
    private Logger logger = null;
    private String level = "debug";
    private StringBuilder msg = new StringBuilder();
    private Element<STATUS> endOfLogElement;
    
    public LoggingFixerElement(Element<STATUS> endOfLogElement) {
        this.endOfLogElement=endOfLogElement;
    }

    @Override
    public Element add(String input, Map<String, STATUS> statusMap, Map<String, String> messageMap) {
        final Matcher lineMatcher = LOG_LINE.matcher(input);
        final Matcher loggermatcher = LOGGER_LINE.matcher(input);
        final Matcher levelMatcher = LEVEL_LINE.matcher(input);
        if ("ENDLOG".equals(input)) {
            finalizeAndPerfomLog();
            return endOfLogElement;
        } else if (lineMatcher.matches()) {
            registerNewLogLine(lineMatcher);
            return this;
        } else if (loggermatcher.matches()) {
            logger = LoggerFactory.getLogger(loggermatcher.group(1));
            return this;
        } else if (levelMatcher.matches()) {
            level = levelMatcher.group(1);
            return this;
        } else {
            LOGGER.warn("Strange things happend in log pipe listening elements, got this in the middle of a log:\n{}", input);
            return endOfLogElement;
        }
    }

    private void registerNewLogLine(final Matcher lineMatcher) {
        if (msg.length() > 0) {
            msg.append("\n");
        }
        msg.append(lineMatcher.group(1));
    }

    private void finalizeAndPerfomLog() {
        LOGGER.debug("End of log message");
        if (logger == null) {
            LOGGER.error("Logging pipe protocol error : no logger name");
            logger = LOGGER;
        }
        switch (level) {
            case LoggingFixerProtocol.DEBUG_LEVEL:
                logger.debug(msg.toString());
                break;
            case LoggingFixerProtocol.WARN_LEVEL:
                logger.warn(msg.toString());
                break;
            case LoggingFixerProtocol.ERROR_LEVEL:
                logger.error(msg.toString());
                break;
            case LoggingFixerProtocol.TRACE_LEVEL:
                logger.trace(msg.toString());
                break;
            case LoggingFixerProtocol.INFO_LEVEL:
                logger.info(msg.toString());
                break;
            default:
                logger.warn("String level {} for message '{}' matched no known level.", level, msg.toString());
        }
    }
    
}

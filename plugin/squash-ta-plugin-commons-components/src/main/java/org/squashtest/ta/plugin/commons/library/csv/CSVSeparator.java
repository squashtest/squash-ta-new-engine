/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv;

public enum CSVSeparator implements ConfigEnum{
	COLUMN(":"),
	COMMA(","),
	PIPE("\t|\t"),
	SEMI_COLUMN(";");

	private final String separatingCharacter;
        
        private static String listOfValidSeparators=null;
        
    private CSVSeparator(String character) {
        this.separatingCharacter = character;
    }

	@Override
    public String getCharacter() {
	return separatingCharacter;
    }
    
    public static CSVSeparator fromString(String s){
        for(CSVSeparator candidat:values()){
            if(candidat.getCharacter().equals(s)){
                return candidat;
            }
        }
        if(listOfValidSeparators==null){
            StringBuilder builder=new StringBuilder("{");
            for(CSVSeparator sepDef:values()){
                builder.append(sepDef.separatingCharacter).append(",");
            }
            if(builder.length()>1){
                builder.setLength(builder.length()-1);
            }
            builder.append("}");
            listOfValidSeparators=builder.toString();
        }
        throw new IllegalArgumentException(s+" is no valid CSVSeparator. Use one of "+listOfValidSeparators);
    }
}

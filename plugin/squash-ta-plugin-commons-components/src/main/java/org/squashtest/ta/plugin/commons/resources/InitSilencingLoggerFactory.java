/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Simple Factory creating a Loger Wrapper that dynamically adjust logging level
 * when in "init" phase".
 *
 * The "init" phase is marked by the {@link org.slf4j.MDC} containing the key
 * value pair :
 *
 * <b>tf_junit5_init_phase : on</b>
 *
 * @author fgautier
 */
public class InitSilencingLoggerFactory {

    /**
     * Used for class related messages.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(InitSilencingLoggerFactory.class);

    /**
     * {@link org.slf4j.MDC} key to signal/unsignal the junit init phase.
     */
    private static final String JUNIT_INIT_KEY = "tf_junit5_init_phase";

    /**
     * {@link org.slf4j.MDC} value to signal the junit init phase.
     */
    private static final String JUNIT_INIT_ON_VALUE = "on";

    /**
     * Private ctor. A static factory should not be instanciated.
     */
    private InitSilencingLoggerFactory() {
        /* Static factory should no be instanciated */ }

    /**
     * Factory method.
     *
     * @param clazz : The class that will emit the logs
     *
     * @return the corectly wrapped logger.
     */
    public static Logger getLogger(Class<?> clazz) {
        try {
            Class<?> loggerInterface = Class.forName("org.slf4j.Logger");
            InvocationHandler messageHandler = new SilencingLoggerMesageInvocationHandler(clazz);
            Object newProxyLoggerInstance = Proxy.newProxyInstance(loggerInterface.getClassLoader(),
                    new Class[]{loggerInterface},
                    messageHandler);
            return (Logger) newProxyLoggerInstance;
        } catch (ClassNotFoundException ex) {
            LOGGER.warn("Someting is rotten il the realm of Denmark... Unable to create init silencer logger wrapper. Creating standard one instead", ex);
            return LoggerFactory.getLogger(clazz);
        }
    }

    public static void switchOnInitPhase() {
        MDC.put(JUNIT_INIT_KEY, JUNIT_INIT_ON_VALUE);
    }

    public static void switchOffInitPhase() {
        MDC.remove(JUNIT_INIT_KEY);
    }

    /**
     * This is a delegate wrapper in spirit. Basically it uses a logger delegate
     * to emit the messages. It redirects the warn warn and error flux to debug
     * in the init phase.
     *
     * Relection is used as a commodity just to avoid creating a Empty shell
     * wrapper with "if (isnInitPhase())" written everywhere.
     *
     */
    private static final class SilencingLoggerMesageInvocationHandler implements InvocationHandler {

        /**
         * The delegate Logger.
         */
        private final Logger delegate;

        /**
         * Standard ctor.
         *
         * @param clazz the class that will log.
         */
        private SilencingLoggerMesageInvocationHandler(Class<?> clazz) {
            delegate = LoggerFactory.getLogger(clazz);
        }

        /**
         * Tells wether one is experiencing an init phase or not.
         *
         * @return true if indeed, false otherwise.
         */
        private boolean isInitPhase() {
            return JUNIT_INIT_ON_VALUE.equals(MDC.get(JUNIT_INIT_KEY));
        }

        /**
         * Tells wether a message is emmited in warn or error.
         *
         * @return true if indeed, false otherwise.
         */
        private boolean isWarnOrError(String methodName) {
            return "warn".equals(methodName) || "error".equals(methodName);
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            try {
                Method delegateMethod = findDelegateMethod(method);
                return delegateMethod.invoke(delegate, args);
            } catch (IllegalAccessException
                    | IllegalArgumentException
                    | InvocationTargetException
                    | NoSuchMethodException
                    | SecurityException ex) {
                delegate.debug("Something is rotten in the realm of Denmark. Error while redirecting log flux to delegate logger. Rolling back to orignal method.", ex);
                return invokeRollBack(method, args);
            }
        }

        /*
         * We use reflection to find the correct method to use to redirect the warn and error fluxes into
         * debug when in init phase.
         */
        private Method findDelegateMethod(Method method) throws NoSuchMethodException, SecurityException {
            final Class<?> delegateClazz = delegate.getClass();

            final String methodName = method.getName();
            Class<?>[] methodParametersTypes = method.getParameterTypes();

            Method delegateMethod;

            if (isWarnOrError(methodName) && isInitPhase()) {
                delegateMethod = delegateClazz.getMethod("debug", methodParametersTypes);
            } else {
                delegateMethod = delegateClazz.getMethod(methodName, methodParametersTypes);
            }

            return delegateMethod;
        }

        /**
         * In the case where something wen't wrong with reflection. We try to dodge it and call the original Logger method. 
         * Rest assured only a {@link Logger} method (or Objetc one..) will be called since the only method creating the proxy using this handler
         * cast it using the Logger interface. Hence any implementation should be able to call such method.  
         */
        private Object invokeRollBack(Method method, Object[] args) {
            try {
                return method.invoke(delegate, args);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException oex) {
                delegate.warn("Situation Normal All F..ouled Up detected ! Cannot use original logger method on delegate one. More information available in debug mode", oex);
                delegate.debug("Original call is for method {}, with arguments types {} and values {}", method.getName(), Arrays.toString(method.getParameterTypes()), Arrays.toString(args));
                return lastResortInvocation(method);
            }
        }

        /**
         * This is called as a last resort. This SHOULD NEVER happen. but in a scenario where this happens we try to f... the least up. 
         * We use the wanted return type to perform an educated guess based on the Logger and Objects methods to return something the 
         * closest to being valid.  
         */
        private Object lastResortInvocation(Method method) {
            if (method.getReturnType().equals(Void.TYPE)) {
                delegate.debug("Originally called method return type is void. Returning null from invocation.");
                return null;
            } else if (method.getReturnType().equals(Boolean.TYPE)) {
                delegate.debug("Originally called method return type is boolean. Returning false from invocation.");
                return false;
            } else if (method.getReturnType().equals(String.class)) {
                delegate.debug("Originally called method return type is string. Returning \"\" from invocation.");
                return "";
            } else if (method.getReturnType().equals(Integer.TYPE)) {
                delegate.debug("Orginally called method returning type was int . Only one method for a logger can produce such output, that is the hashCode method inherited form object. Hence returning delegate hashcode");
                return delegate.hashCode();
            } else if (method.getReturnType().equals(Class.class)) {
                delegate.debug("Orginally called method returning type was Class . Only one method for a logger can produce such output, that is the getClass method inherited form object. Hence returning delegate Class");
                return delegate.getClass();
            } else {
                throw new RuntimeException("Well... We did not see that one coming...");
            }
        }
    }
}

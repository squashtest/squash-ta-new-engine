/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

/**
 * Base class of all bundle components.
 * @author edegenetais
 *
 */
public abstract class BundleComponent extends OptionsReader{
	
        private static final Logger LOGGER = LoggerFactory.getLogger(BundleComponent.class);
        
	private static final String MAINPATH_KEY = "mainpath";
	
	protected BundleComponent() {}
	
	protected BundleComponent(char optionsSeparator) {
		super(optionsSeparator);
	}
	
	@Override
	protected Set<String> supportedKeys(Map<String, String> source) {
		HashSet<String>supportedKeys=new HashSet<String>();
		if(source.containsKey(MAINPATH_KEY)){
			supportedKeys.add(MAINPATH_KEY);
		}
		return supportedKeys;
	}
	
	protected Map<String,String> getOptions(Collection<Resource<?>> configuration) throws IOException{
		Map<String,String>result=new HashMap<String, String>();
		Set<String> comparisonSet=new HashSet<String>();
		for(Resource<?> configurationElement:configuration){
			if(isOptions(configurationElement)){
				Map<String,String> options=getOptions(((FileResource)configurationElement).getFile());
				comparisonSet.addAll(result.keySet());
				comparisonSet.retainAll(options.keySet());
				if(comparisonSet.isEmpty()){
					//no conflict -> ok we can add
					result.putAll(options);
				}else{
					reportConfigurationKeyConflict(result, comparisonSet,
							options);
				}
			}
		}
		return result;
	}

	private void reportConfigurationKeyConflict(Map<String, String> result,
			Set<String> comparisonSet, Map<String, String> options) {
		StringBuilder messageBuilder=new StringBuilder("The following elements are configured more than once in the configuration:\n");
		for(String key:comparisonSet){
			messageBuilder.append(key);
			messageBuilder.append("-->'");
			messageBuilder.append(result.get(key));
			messageBuilder.append("' and '");
			messageBuilder.append(options.get(key));
			messageBuilder.append("'");
		}
		throw new IllegalConfigurationException(
				messageBuilder.toString()
				);
	}
	
	protected File extractMainFileFromConfiguration(Collection<Resource<?>> configuration,File bundleBase) throws IOException{
		Map<String,String> optionMap=getOptions(configuration);
		File mainFile=null;
		String mainPath = optionMap.get(MAINPATH_KEY);
		if(mainPath!=null){
			if(bundleBase==null){
				mainFile=new File(mainPath);
			}else{
				mainFile = new File(bundleBase, mainPath);
			}
		}
		return mainFile;
	}
	
	protected boolean isOptions(Resource<?> resource){
		boolean isOptions = false;
		try{
			if (FileResource.class.isAssignableFrom(resource.getClass())){
				isOptions = isOptions(((FileResource)resource).getFile());
			}else{
				isOptions = false;
			}
		}catch(IOException ex){
                        LOGGER.warn("IO Problem while tring to read content of resource {}. Assuming it was not a valid option file.",resource,ex);
			//well it must not have been a valid options file then.
			isOptions = false;
		}
		
		return isOptions;
			
	}

}

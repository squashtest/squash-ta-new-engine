/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.parser.impl;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidFileException;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidLineException;
import org.squashtest.ta.plugin.commons.library.csv.model.CsvXmlField;
import org.squashtest.ta.plugin.commons.library.csv.model.CsvXmlFile;
import org.squashtest.ta.plugin.commons.library.csv.model.CsvXmlLine;
import org.squashtest.ta.plugin.commons.library.csv.parser.CSVParserListener;
import org.squashtest.ta.plugin.commons.library.csv.parser.RawCsvLine;

/**
 * This {@link CSVParserListener} builds incrementally a {@link CsvXmlFile} with 
 * each event thrown at him. 
 * @author fgautier
 */
public class CSVXmlBuilder implements CSVParserListener {

    private Map<String,Integer> namesToIndexMap;
    
    private List<CsvXmlField> comments;
    
    private List<CsvXmlLine> lines;
    
    private CsvXmlFile unmarshalledFile; 
    
    @Override
    public void columnNames(Map<String, Integer> columnNamesToIndexMapping) throws InvalidFileException {
        namesToIndexMap=columnNamesToIndexMapping;
    }

    @Override
    public void commentLine(RawCsvLine parsedLine) throws InvalidFileException {
        List<CsvXmlField> newComments = new ArrayList<>();
        final String[] rawValues = parsedLine.getValues();

        
        for(String column : namesToIndexMap.keySet()) {
            final Integer index = namesToIndexMap.get(column);
            CsvXmlField field = new CsvXmlField(column,rawValues[index]);
            newComments.add(field);
        }
        
        comments=Collections.unmodifiableList(newComments);
    }

    @Override
    public void newLine(RawCsvLine parsedLine) throws InvalidLineException {
        if (lines==null) {
            lines = new ArrayList<>();
        }
        
        final String[] rawValues = parsedLine.getValues();
        
        List<CsvXmlField> lineFields = new ArrayList<>();
        
        for(String column : namesToIndexMap.keySet()) {
            final Integer index = namesToIndexMap.get(column);
            CsvXmlField field = new CsvXmlField(column,rawValues[index]);
            lineFields.add(field);
        }
        
        CsvXmlLine newLine = new CsvXmlLine(parsedLine.getLineNumber(),Collections.unmodifiableList(lineFields));
        
        lines.add(newLine);
    }

    @Override
    public void eof() throws InvalidFileException {
        unmarshalledFile = new CsvXmlFile(lines, comments);
    }
    
    /**
     * Write the content of the build CsvXmlFile to destination. 
     * 
     * @param destination the physical file where the serialization will take place.
     * @param destinationEncoding encoding of the output Xml File
     * 
     * @throws InvalidFileException if the data constructed by the builder is not marshable.
     */
    public void serializeContent(OutputStream destination, String destinationEncoding) throws InvalidFileException {
        if (unmarshalledFile == null) {
            throw new InvalidFileException("Serialization of a null file is impossible. Please call first the Listener API methods for the file to builder to be in a legal state.");
        }

        try {
            new CsvXmlMarshaller(destinationEncoding).marshal(unmarshalledFile, destination);
        } catch (JAXBException e) {
            throw new InvalidFileException("Marshalling of constructed data is impossible.", e);
        }

    }
}

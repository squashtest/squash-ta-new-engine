/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * XML resource implementation.
 * @author fgaillard
 *
 */
@TAResource("xml")
public class XMLResource implements Resource<XMLResource> {
	
	private File xmlFile;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public XMLResource(){}
	
	public XMLResource(File file){
		xmlFile = file;
	}
	
	@Override
	public XMLResource copy() {
		try {
			File newFile = FILE_TREE.createTempCopyDestination(xmlFile);
			FileUtils.copyFile(xmlFile, newFile);
			return new XMLResource(newFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("XML (resource) : could not copy file path '"+xmlFile.getPath()+"', an error occured :", e);
		}
	}

	@Override
	public void cleanUp() {
		//noop
	}

	public File getXMLFile() {
		return xmlFile;
	}
}

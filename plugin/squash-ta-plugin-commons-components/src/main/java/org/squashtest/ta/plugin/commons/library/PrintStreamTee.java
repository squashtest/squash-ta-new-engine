/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Locale;

/**
 *
 * @author edegenetais
 */
public class PrintStreamTee extends PrintStream {
    
    private PrintStream ps;
    private ByteArrayOutputStream buffer;
    /*
    Solution au problème de la poule et de l'oeuf pour à la fois référencer
    le buffer dans le super constructeur et en garder la référence: au sein de
    la même thread il n'y aura jamais rien entre l'appel à teeBuffer qui crée le buffer
    et le référence dans le threadlocal et l'appel direct au thread local dans le constructeur qui récupère la référence.
     */
    private static ThreadLocal<ByteArrayOutputStream> baosFactory = new ThreadLocal<ByteArrayOutputStream>();


    public PrintStreamTee(PrintStream real) {
        super(teeBuffer());
        this.ps = real;
        this.buffer = baosFactory.get();
        /*
        ci-dessous: n'ayant plus besoin de la référence statique/par ThreadLocal
        au buffer, la détruire pour lui permettre d'être normalement
        garbage-collecté en même que l'instance qui l'utilise.
         */
        baosFactory.remove();
    } 
    
    private static ByteArrayOutputStream teeBuffer() {
        baosFactory.set(new ByteArrayOutputStream());
        return baosFactory.get();
    }

    @Override
    public PrintStream append(CharSequence cs) {
        ps.append(cs);
        super.append(cs);
        return this;
    }

    @Override
    public PrintStream append(char c) {
        ps.append(c);
        super.append(c);
        return this;
    }

    @Override
    public PrintStream append(CharSequence cs, int i, int i1) {
        ps.append(cs, i, i1);
        super.append(cs, i, i1);
        return this;
    }

    @Override
    public boolean checkError() {
        return super.checkError() && ps.checkError();
    }

    @Override
    public void close() {
        ps.close();
        super.close();
    }

    @Override
    public void flush() {
        ps.flush();
        super.flush();
    }

    @Override
    public PrintStream format(String string, Object... os) {
        ps.format(string, os);
        super.format(string, os);
        return this;
    }

    @Override
    public PrintStream format(Locale locale, String string, Object... os) {
        ps.format(locale, string, os);
        super.format(locale, string, os);
        return this;
    }

    public String getContent() {
        flush();
        return buffer.toString();
    }
    
}

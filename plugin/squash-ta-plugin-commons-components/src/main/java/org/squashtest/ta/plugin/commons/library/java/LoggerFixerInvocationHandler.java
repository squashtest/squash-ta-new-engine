/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import static org.squashtest.ta.plugin.commons.library.java.LoggingFixerProtocol.*;

/**
 *
 * @author edegenetais
 */
class LoggerFixerInvocationHandler implements InvocationHandler {
    
    private final String loggerName;
    
    private static final String[] LOG_METHOD_NAMES={
        TRACE_LEVEL,
        DEBUG_LEVEL,
        INFO_LEVEL,
        WARN_LEVEL,
        ERROR_LEVEL
    };
    
    private static final String[] IS_ENABLED_METHOD_NAMES={
        "isDebugEnabled",
        "isWarnEnabled",
        "isErrorEnabled",
        "isTraceEnabled",
        "isInfoEnabled"
    };

    public LoggerFixerInvocationHandler(String loggerName) {
        this.loggerName = loggerName;
    }

    private void emittLog(String level, String content) {
        emittLogAsLogger(content, loggerName, level);
    }

    private void emittLogAsLogger(String content, final String targetLogger, String level) {
        String[] lineContents = content.split("\n");
        StringBuilder contentLines = new StringBuilder();
        for (String line : lineContents) {
            contentLines.append("LINE:").append(line).append("\n");
        }
        //NOSONAR : here we are working around the lack of proper logging subsystem, so using sysout as a replacement is precisely the point!
        System.out.println("STARTLOG\nLOGGER:" + targetLogger + "\nLEVEL:" + level + "\n" + contentLines + "ENDLOG");//NOSONAR
    }

    @Override
    public Object invoke(Object o, Method method, Object[] os) throws Throwable {
        final String methodName = method.getName();
        
        if(isLogCall(methodName)){
            treatLogRequest(os, methodName);
            return null;
        }else if(isEnabledCheck(methodName)){
            return true;
        }else{
            logUnhandledmothodCall(method);
            return null;
        }
    }
    
    private void logUnhandledmothodCall(Method method) {
        String content = "LINE:/!\\ " + method.getName() + " called.";
        emittLogAsLogger(WARN_LEVEL, LoggingFixer.class.getName(), content);
    }
    
    private boolean isLogCall(String methodName){
        return isOneOf(methodName, LOG_METHOD_NAMES);
    }

    private boolean isEnabledCheck(String methodName){
        return isOneOf(methodName, IS_ENABLED_METHOD_NAMES);
    }
    
    private boolean isOneOf(String methodName, String[] targetStrings) {
        boolean isLogCall=false;
        for(String levelName:targetStrings){
            if(levelName.equals(methodName)){
                isLogCall=true;
            }
        }
        return isLogCall;
    }
    
    private void treatLogRequest(Object[] os, final String methodName) {
        if (os.length > 1 && os[os.length - 1] instanceof Throwable) {
            Throwable t = (Throwable) os[os.length - 1];
            final ByteArrayOutputStream message = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(message);
            ps.append(os[0].toString()).append("\n");
            t.printStackTrace(ps);
            emittLog(methodName, message.toString());
        } else {
            emittLog(methodName, os[0].toString());
        }
    }
    
}

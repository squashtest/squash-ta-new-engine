/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.StandardJavaFileManager;

/**
 *
 * @author cjuillard
 */
public abstract class AbstractPlacementJavaFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {

    protected File outputLocation;

    protected Set<String> registeredClassNames = new HashSet<String>();

    public AbstractPlacementJavaFileManager(StandardJavaFileManager fileManager, File bundleClassLocation) {
        super(fileManager);
        if (bundleClassLocation.isDirectory()) {
            outputLocation = bundleClassLocation;
        } else {
            throw new IllegalArgumentException(bundleClassLocation.getAbsolutePath() + " isn't a valid directory.");
        }
    }

    /**
     * @return an unmodifiable Set with registered class names.
     */
    public Set<String> getRegisteredClassNames() {
        return Collections.unmodifiableSet(registeredClassNames);
    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.reporting;

/**
 * Interface of components used to translate messages, like report lines.
 * This is intended to integrate reporting help with logics beyond mere key-value translation,
 * making clear that there is no such thing as a limited and predefined key set or output set.
 * @author edegenetais
 */
public interface Translator {
    /**
     * Translate a given message.
     * @param message raw message.
     * @return translation.
     */
    String translate(String message);
    /**
     * Rates translation specificity to allow combining translation contexts.
     * NB: in some implementations, calling this might be as costly as actually translating,
     * therefore doubling the overall cost of translation.
     * @param message raw message to translate.
     * @return a specificity rating from 0 (the translation is only a default fallback) 
     * to 100 (this is a unique and perfect fit for this message).
     */
    int translationSpecificity(String message);
    
    /**
     * In case the translator holds resources that need a bit of tidying.
     */
    void clean();
    /**
     * @return an independant copy of this translator which will still work after this instance was cleaned.
     * @see #clean() 
     */
    Translator duplicate();
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.library.param.Replacer;

public abstract class AbstractPropertiesConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPropertiesConverter.class);

        private static final int REAL_MEGABYTE_NO_SELLER_BULLSHIT = 1024*1024;
        protected long chunkSize= REAL_MEGABYTE_NO_SELLER_BULLSHIT;
        
	protected Properties properties = new Properties();
	
	protected List<File> createdResources = new LinkedList<File>();

	protected static final String EXCLUDE_KEY = "squashtest.ta.param.exclude";

	protected static final String INCLUDE_KEY = "squashtest.ta.param.include";
	
	protected Pattern excludePattern;
	
	protected Pattern includePattern;
	
    protected Charset encoding;
        
	protected void addIncludeExcludeProperties() {
		String exclude = properties.getProperty(EXCLUDE_KEY);	
		String include = properties.getProperty(INCLUDE_KEY);
		
		if (include!= null && exclude != null){
			throw new InstructionRuntimeException("You can't use both 'squashtest.ta.param.include' and 'squashtest.ta.param.exclude' properties!");
		}else{
			if (exclude != null){
				excludePattern = Pattern.compile(exclude);
				properties.remove(EXCLUDE_KEY);
			}
			if (include != null){
				includePattern = Pattern.compile(include);
				properties.remove(INCLUDE_KEY);
			}
		}	
	}
	
        
        
	protected void addFile(File content, File converted, Replacer replacer) throws IOException {
		if (isInclude(content)){
			convertFile(content,converted,replacer);
		}else{
			addFileWithoutModifications(content,converted);
			LOGGER.info("the file "+content.getName()+" was properly excluded.");
		}	
	}
	
	private boolean isInclude(File contentFile) {
		boolean isInclude;
		if (excludePattern != null){
			//there is an exclude pattern: we took only files that don't match the pattern
			Matcher excludeMatcher = excludePattern.matcher(contentFile.getPath());
			isInclude = !excludeMatcher.find();
		}else if (includePattern != null){
			//there is an include pattern: we took only files that does match the pattern
			Matcher includeMatcher = includePattern.matcher(contentFile.getPath());
			isInclude = includeMatcher.find();
		}else{
			//nothing is specified: we consider all files
			isInclude = true;
		}
		return isInclude;
	}
	
	private void convertFile(File initialFile, File convertedFile, Replacer replacer) throws IOException{
            if(initialFile.length()>chunkSize){
                chunkBigFileToApplyConvert(initialFile, convertedFile, replacer);
            }else{
		convertChunk(initialFile, replacer, convertedFile);
            }
	}

        private void chunkBigFileToApplyConvert(File initialFile, File convertedFile, Replacer replacer) throws IOException {
            LOGGER.info("The input file exceeds the {} chunk size. Chunking.",chunkSize);
        try(
                FileInputStream in=new FileInputStream(initialFile);
                InputStreamReader reader=new InputStreamReader(in, getEncoding());
                BufferedReader lineSource=new BufferedReader(reader);
                
                FileOutputStream os=new FileOutputStream(convertedFile);
                ){
            
            StringBuilder chunkBuilder=new StringBuilder();
            String line=lineSource.readLine();
            List<File> translatedChunks=new ArrayList<>();
            long currentChunkSize = 0;
            while(line!=null){
                 currentChunkSize+=line.getBytes("utf-16").length;
                if( currentChunkSize>=chunkSize){
                    LOGGER.debug("Chunk size {} bytes is exceding the {} limit. Chunking",currentChunkSize,chunkSize);
                    writeAndConvertChunk(initialFile, chunkBuilder, replacer, translatedChunks);
                    currentChunkSize=0;
                }
                chunkBuilder.append(line).append("\n");
                line=lineSource.readLine();
            }
            writeAndConvertChunk(initialFile, chunkBuilder, replacer, translatedChunks);
            
            LOGGER.debug("Chunking and chunk translation are complete. Reuniting {} translated chunks.",translatedChunks.size());
            
            for(File chunkFile:translatedChunks){
                FileUtils.copyFile(chunkFile, os);
                chunkFile.delete();
            }
            LOGGER.info("Chunk-wise conversion complete.");
        }
    }

    private void writeAndConvertChunk(File initialFile, StringBuilder chunkBuilder, Replacer replacer, List<File> translatedChunks) throws IOException {
        File chunkFile=File.createTempFile(initialFile.getName(), ".chunk",TempDir.getExecutionTempDir());
        chunkFile.deleteOnExit();
        File translatedChunkFile=File.createTempFile(chunkFile.getName(), ".trad",TempDir.getExecutionTempDir());
        translatedChunkFile.deleteOnExit();
        BinaryData chunkData=new BinaryData(chunkBuilder.toString().getBytes(getEncoding()));
        chunkData.write(chunkFile);
        convertChunk(chunkFile, replacer, translatedChunkFile);
        chunkBuilder.setLength(0);
        translatedChunks.add(translatedChunkFile);
        chunkFile.delete();
    }
    
    public void convertChunk(File initialFile, Replacer replacer, File convertedFile) throws IOException {
        BinaryData inputData = new BinaryData(initialFile);
        BinaryData outputData = replacer.apply(inputData);
        outputData.write(convertedFile);
    }
	
	private void addFileWithoutModifications(File initialFile, File convertedFile) throws IOException{
            FileUtils.copyFile(initialFile, convertedFile);
	}
	
	/**
	 * testing purpose only.
	 */
	protected void setExcludePattern(Pattern excludePattern) {
		this.excludePattern = excludePattern;
	}
	
	/**
	 * testing purpose only.
	 */
	protected void setIncludePattern(Pattern includePattern) {
		this.includePattern = includePattern;
	}
	
	public void cleanUp() {
		for (File file : createdResources) {
			try {
				if (file.isDirectory()) {
					FILE_TREE.clean(file);
				} else {
					// clear demonstration that C-style error handling is SHIT
					boolean deleted = file.delete();
					if (!deleted) {
						LOGGER.warn("Failed to delete created file resource " + file.getAbsolutePath());
					}
				}
			} catch (IOException e) {
				LOGGER.warn("Failed to delete created file resource " + file.getAbsolutePath(), e);
			}
		}
	}

    public Charset getEncoding() {
        if (encoding==null) {
            LOGGER.debug("No encoding has been specified using UTF-8 as default");
            return Charset.forName("UTF-8");
        } else {
            return encoding;

        }
      }

    public void setEncoding(String charsetName) {
        Charset c=Charset.forName(charsetName);
        if(this.encoding==null){
             LOGGER.debug("Setting {} as encoding.", c);
            this.encoding=c;
        }else{
            LOGGER.warn("Ignoring conflicting encoding {}, {} is already set.",charsetName, this.encoding);
        }
    }

    
}

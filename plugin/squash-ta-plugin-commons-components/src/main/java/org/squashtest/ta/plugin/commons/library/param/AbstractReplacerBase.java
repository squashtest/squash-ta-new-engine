/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.squashtest.ta.plugin.commons.library.param;

import java.nio.charset.Charset;
import java.util.List;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

/**
 *
 * @author edegenetais
 */
public abstract class AbstractReplacerBase implements Replacer {
    
    private Charset encoding;
    private ReplacementTokenizer tokenizer;

    public AbstractReplacerBase(Charset encoding, ReplacementTokenizer tokenizer) {
        this.encoding = encoding;
        this.tokenizer = tokenizer;
    }
    
    @Override
    public BinaryData apply(BinaryData originalData) {
        byte[] content = originalData.toByteArray();
        byte[] output = applyInternal(content);
        return new BinaryData(output);
    }

    protected byte[] applyInternal(byte[] content) {
        SimpleLinesData lineData = new SimpleLinesData(content, encoding);
        List<Expression> substituedStructure = tokenizer.tokenize(lineData);
        StringBuilder builder = new StringBuilder();
        for (Expression ex : substituedStructure) {
            ex.evaluate(builder);
        }
        return builder.toString().getBytes(encoding);
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

@TAResource("xsd")
public class XSDResource implements Resource<XSDResource> {

	private File xsdFile;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public XSDResource(){
		
	}
	
	public XSDResource(File xsdFile){
		this.xsdFile = xsdFile;
	}
	
	
	@Override
	public XSDResource copy() {
		try {
			File duplicatedXsdFile = FILE_TREE.createTempCopyDestination(xsdFile);
			FileUtils.copyFile(xsdFile, duplicatedXsdFile);
			return new XSDResource(duplicatedXsdFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("XSD (resource) : could not copy file path '"+xsdFile.getPath()+"', an error occured :", e);
		}
	}

	@Override
	public void cleanUp() {
		// NoOp
	}
	
	public File getXsdFile() {
		return xsdFile;
	}

}

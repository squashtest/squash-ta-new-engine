/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.ComponentRepresentation;

/**
 * This class is an alternative implementation of {@link JavaCodeBundle} for 
 * third-party java code.
 * 
 * @author edegenetais
 */
@TAResource("script.java")
public class ThirdPartyJavaCodeBundle extends JavaCodeBundle implements Resource<JavaCodeBundle>{
    
    private URL[] classpath;
    private ClassLoader classLoader;
    private boolean reload;
    
    public ThirdPartyJavaCodeBundle(){/* Empty default constructor for Spring enumeration */}
    
    protected ThirdPartyJavaCodeBundle(ThirdPartyJavaCodeBundle orig){
        super(orig);
        this.classpath=orig.classpath;
        this.reload=orig.reload;
    }
       
    public ThirdPartyJavaCodeBundle(List<String> classPath,File directoryLocation) {
        this(fromRepresentationList(classPath),directoryLocation);
    }

    public ThirdPartyJavaCodeBundle(URL[] classpath, File directoryLocation) {
        super(directoryLocation, classList(directoryLocation));
        init(classpath, directoryLocation, false);
    }
    
    public ThirdPartyJavaCodeBundle(List<String> classPath,boolean reload,File directoryLocation, Set<String> classNames) {
        this(fromRepresentationList(classPath),reload,directoryLocation,classNames);
    }
    
    public ThirdPartyJavaCodeBundle(URL[] classpath, boolean reload,File directoryLocation, Set<String> classNames) {
        super(directoryLocation, classNames);
        init(classpath, directoryLocation, reload);
    }
    
    private static Set<String> classList(final File bundleBase){
        List<File> files=FileTree.FILE_TREE.enumerate(bundleBase, FileTree.EnumerationMode.FILES_ONLY);
        final HashSet<String> classNames=new HashSet<>(files.size());
        for(File file:files){
                if(file.getName().endsWith(".class")){
                    try {
                        final String relativePath = file.getCanonicalPath().substring(bundleBase.getCanonicalPath().length()+1);
                        final String byteCodeResourceName = relativePath.replace(File.separatorChar, '.');
                        final String className = byteCodeResourceName.substring(0, byteCodeResourceName.length()-".class".length());
                    classNames.add(className);
                } catch (IOException ex) {
                    throw new InstructionRuntimeException("Failed to compute class name of "+file.getAbsolutePath(),ex);
                }
                
            }
        }
        return classNames;
    }
    
    private static URL[] fromRepresentationList(List<String> classpath){
        final int cpSize = classpath.size();
        URL[] bundleCPURLs=new URL[cpSize];
        for(int i=0;i<cpSize;i++){
            final File bundleCPEntry = new File(classpath.get(i));
            try {
                bundleCPURLs[i]=bundleCPEntry.toURI().toURL();
            } catch (MalformedURLException ex) {
                throw new BadDataException("Failed to create classpath entry for "+bundleCPEntry.getName(),ex);
            }
            
        }
        return bundleCPURLs;
    }
    
    private void init(URL[] classpath1, File directoryLocation, boolean reload1) throws InstructionRuntimeException {
        try {
            Set<URL> uniqueCP = new LinkedHashSet<>(Arrays.asList(classpath1));
            uniqueCP.add(directoryLocation.toURI().toURL());
            this.classpath = uniqueCP.toArray(new URL[uniqueCP.size()]);
            this.reload = reload1;
        }catch (MalformedURLException ex) {
            throw new InstructionRuntimeException("Failed to create third party provided "+new ComponentRepresentation(this), ex);
        }
    }

    @Override
    public synchronized ClassLoader getDedicatedClassloader() {
        if(classLoader==null || reload){
            classLoader=new JavaBundleClassLoader(classpath);
        }
        return classLoader;
    }

    @Override
    public JavaCodeBundle copy() {
        return new ThirdPartyJavaCodeBundle(this);
    }

    @Override
    public synchronized void cleanUp() {
        classLoader=null;
    }
 
}

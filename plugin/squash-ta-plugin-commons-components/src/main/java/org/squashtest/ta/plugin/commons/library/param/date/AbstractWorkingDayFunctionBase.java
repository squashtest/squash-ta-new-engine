/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

/**
 * This class is the commons base of all working-day related functor classes.
 * @author edegenetais
 */
public class AbstractWorkingDayFunctionBase {
    private static final Logger LOGGER=LoggerFactory.getLogger(AbstractWorkingDayFunctionBase.class);
    
    protected static String FORMAT_INPUT_DATE = "yyyy-MM-dd";
    public static final String KEY_WORKED_SUNDAY = "org.squashtest.ta.plugin.commons.parms.data.WorkedSunday";
    public static final String KEY_WORKED_SATURDAY = "org.squashtest.ta.plugin.commons.parms.data.WorkedSaturday";
    public static final String KEY_WORKED_MONDAY = "org.squashtest.ta.plugin.commons.parms.data.WorkedMonday";
    //this is kept because it was created in a milestone and used ... yes champagne takes a looooong time to wear in my system, sorry
    public static final String KEY_JOURS_NON_OUVRES = "org.squashtest.ta.plugin.commons.parms.data.joursNonOuvres";
    /*
    * that one was added before official release for API consistency (all english) 
    * while keeping the french thingy as easter egg to avoid breaking tests built from the milestone.
    */
    public static final String KEY_NON_WORKING_DAYS = "org.squashtest.ta.plugin.commons.parms.data.nonWorkingDays";
    protected boolean nonWorkedSaturday = true;
    protected boolean nonWorkedSunday = true;
    protected boolean nonWorkedMonday = false;
    protected Set<String> nonWorkingDays = new HashSet<>();
    protected AddDay delegate = new AddDay();

    public void addDataSource(Properties prop) {
        if (prop.containsKey(KEY_JOURS_NON_OUVRES) || prop.containsKey(KEY_NON_WORKING_DAYS)) {
            final String listString = prop.getProperty(KEY_JOURS_NON_OUVRES,"");
            final String listStringEn = prop.getProperty(KEY_NON_WORKING_DAYS,"");
            if (listString.isEmpty() && listStringEn.isEmpty()) {
                LOGGER.debug("Empty non-working day list, will only consider weekly off days.");
            } else if(!listString.isEmpty()){
                nonWorkingDays.addAll(Arrays.asList(listString.split(",")));
                LOGGER.debug("Loading the following off-day list: {} from alternate key "+KEY_JOURS_NON_OUVRES, listString);
            } else if(!listStringEn.isEmpty()){
                nonWorkingDays.addAll(Arrays.asList(listStringEn.split(",")));
                LOGGER.debug("Loading the following off-day list: {} from main key "+KEY_NON_WORKING_DAYS, listStringEn);
            }
        } else {
            throw new IllegalConfigurationException("Missing key " + KEY_JOURS_NON_OUVRES + " in datasource configuration, which should contain a comma-separated list of non-worked days in the following format : " + FORMAT_INPUT_DATE);
        }
        if (prop.containsKey(KEY_WORKED_SATURDAY)) {
            nonWorkedSaturday = !Boolean.parseBoolean(prop.getProperty(KEY_WORKED_SATURDAY));
            LOGGER.debug("Picking worked saturday configuration from key {}, value={}", KEY_WORKED_SATURDAY, !nonWorkedSaturday);
        } else {
            LOGGER.debug("Using default worked saturday configuration of {}", !nonWorkedSaturday);
        }
        if (prop.containsKey(KEY_WORKED_SUNDAY)) {
            nonWorkedSaturday = !Boolean.parseBoolean(prop.getProperty(KEY_WORKED_SUNDAY));
            LOGGER.debug("Picking worked saturday configuration from key {}, value={}", KEY_WORKED_SUNDAY, !nonWorkedSunday);
        } else {
            LOGGER.debug("Using default worked sunday configuration of {}", !nonWorkedSunday);
        }
        if (prop.containsKey(KEY_WORKED_SUNDAY)) {
            nonWorkedSunday = !Boolean.parseBoolean(prop.getProperty(KEY_WORKED_SUNDAY));
            LOGGER.debug("Picking worked sunday configuration from key {}, value={}", KEY_WORKED_SUNDAY, nonWorkedSunday);
        } else {
            LOGGER.debug("Using default worked monday configuration of {}", !nonWorkedMonday);
        }
        delegate.addDataSource(prop);
    }

    protected boolean isNonWorkingDay(Calendar result) {
        if (nonWorkedSaturday && result.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            return true;
        } else if (nonWorkedSunday && result.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        } else if (nonWorkedMonday && result.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            return true;
        } else {
            return this.nonWorkingDays.contains(new SimpleDateFormat(FORMAT_INPUT_DATE).format(result.getTime()));
        }
    }
    
}

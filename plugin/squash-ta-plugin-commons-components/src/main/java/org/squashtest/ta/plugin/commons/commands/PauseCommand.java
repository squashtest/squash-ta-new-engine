/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.commands;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;

/**
 * 
 * <p><strong>Description:</strong>
 * Makes the test execution sleep for a given time (expressed in milliseconds)</p>
 * <div><strong>DSL example:</strong>
 * <pre>EXECUTE pause WITH $(3000) AS null</pre>
 * </div>
 *  
 * @author cruhlmann
 *
 */
@TACommand("pause")
public class PauseCommand implements Command<FileResource, VoidTarget> {
	
	private FileResource fileResource;
	
	@Override
	public void addConfiguration(final Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public void setTarget(final VoidTarget target) {
		//ignored: the void target holds no information and is here as a place holder in the global engine scheme.		
	}
	
	@Override
	public void cleanUp() {
		// nothing to clean	
	}

	@Override
	public void setResource(final FileResource fileResource) {
		this.fileResource = fileResource;		
	}

	@Override
	public VoidResource apply() {
		try {
			Thread.sleep(getTime());
		} catch (InterruptedException e) {
			throw new InstructionRuntimeException("The execution was interrupted before the end.", e);
		}
		return new VoidResource();
	}
	
	/**
	 * Reads the file to extract the configured time, and convert it in milliseconds
	 * @return (long) time to suspend the execution, expressed in milliseconds
	 */
	protected long getTime() {

		long time = 0;
		try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileResource.getFile()));){
			
			final String data = bufferedReader.readLine();
			if (data != null) {
				try {
					time = Long.parseLong(data);
					if (time < 0){
						throw new BadDataException("The pause time must be a positive number. "+time+" is incorrect.");
					}
				} catch (NumberFormatException nfe) {
                                    throw new BadDataException("The variable "+data+" could not correctly be converted to a number.", nfe);
				}
			}
		} catch (IOException ioe) {
			throw new InstructionRuntimeException("Resource convert I/O error",ioe);
		}
                
		return time;
	}		
}

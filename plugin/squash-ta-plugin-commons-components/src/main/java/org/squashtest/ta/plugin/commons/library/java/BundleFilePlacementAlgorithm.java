/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.File;
import java.net.URI;

import javax.tools.JavaFileObject.Kind;

/**
 * The bundle file placement algorithm for sharing between objects that handle code bundle definitions.
 * @author edegenetais
 *
 */
public class BundleFilePlacementAlgorithm {
	public URI getElementURI(File bundleBaseLocation, String elementName, Kind elementKind){
		return getElementFile(bundleBaseLocation, elementName, elementKind).toURI();
	}
	
	public File getElementFile(File bundleBaseLocation, String elementName, Kind elementKind){
		File file;
		switch(elementKind){
		case CLASS:
			file=new File(bundleBaseLocation,elementName+".class");
			break;
		case HTML:
		case OTHER:
			String flattenedName=elementName.substring(0, 1)+elementName.substring(1).replace('/', '.');
			file=new File(bundleBaseLocation,flattenedName);
			break;
		case SOURCE:
			throw new IllegalArgumentException("Including source in a bundle is not supported - yet.");
		default:
			throw new IllegalArgumentException("Kind: "+elementKind+" is not supported");
		}
		return file;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.library.param.date.DateReplacer;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.core.tools.io.StorageSizeUnit;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

/**
 * Converter to apply date computation formulae. For supported functions, see
 * the org.squashtest.ta.plugin.commons.library.param.date package documentation.
 * 
 * @author edegenetais
 * 
 */
@TAResourceConverter("param.relativedate")
public class RelativeDateConverter extends AbstractPropertiesConverter implements
		ResourceConverter<FileResource, FileResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RelativeDateConverter.class);

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
            try{
		Iterator<Resource<?>> it = configuration.iterator();
		while (it.hasNext()) {
			Resource<?> resource = it.next();
			if (resource instanceof PropertiesResource) {
				Properties prop = ((PropertiesResource) resource).getProperties();
				properties.putAll(prop);
			}else if(resource instanceof FileResource){
                            FileResource cfg=(FileResource)resource;
                            if(OptionsReader.BASIC_READER.isOptions(cfg.getFile())){
                                Map<String,String> options=OptionsReader.BASIC_READER.getOptions(cfg.getFile());
                                for(String option:options.keySet()){
                                    switch(option){
                                        case "encoding":
                                            treatEncodingOption(options, option);
                                            break;
                                        case "chunkSize":
                                            this.chunkSize=StorageSizeUnit.getSizeInBytes(options.get(option));
                                            break;
                                        case "squashtest.ta.param.exclude":
                                            properties.putAll(options);
                                            break;
                                        case "squashtest.ta.param.include":
                                            properties.putAll(options);
                                            break;
                                        default:
                                            LOGGER.warn("Ignoring unsupported params.date configuration option {}",option);
                                            break;
                                    }
                                }
                            }else{
                                LOGGER.warn("Ignoring unexpected non-options file resource {}",((FileResource) resource).getFile());
                            }
                        }else{
                            LOGGER.warn("Ignoring unexpected configuration resource of type {}.",resource.getClass().getAnnotation(TAResource.class).value());
                        }
		}
		addIncludeExcludeProperties();
		if (properties.size() > 0) {
			LOGGER.info(properties.size()
					+ " property values used as date data sources.");
                        if(LOGGER.isDebugEnabled()){
                            for(Object key:properties.keySet()){
                                LOGGER.debug("Key {} has value {}.",key,properties.get(key));
                            }
                        }
		}
            }catch(IOException e){
                throw new IllegalConfigurationException("An error occured while fetching configuration data.",e);
            }
	}

    private void treatEncodingOption(Map<String, String> options, String option) {
        String charsetName = options.get(option);
        if(option != null) {
            setEncoding(charsetName);
        }
    }

	@Override
	public FileResource convert(FileResource resource) {
		try {
			File resourceFile = resource.getFile();
			DateReplacer replacer = new DateReplacer(properties, getEncoding());

			File convertedFile = FILE_TREE.createTempCopyDestination(resourceFile);
			createdResources.add(convertedFile);
			if (resourceFile.isDirectory()) {
				FILE_TREE.toTempDirectory(convertedFile);
				List<File> content = FILE_TREE.enumerate(resourceFile,
						EnumerationMode.FILES_ONLY);
				for (File contentFile : content) {
					File convertedContent = new File(convertedFile,
							FILE_TREE.getRelativePath(resourceFile,
									contentFile));

					convertedContent.getParentFile().mkdirs();
					if (!convertedContent.getParentFile().exists()){
						throw new InstructionRuntimeException("Failed to create directory "+convertedContent.getParent()+" during relative dates resolution.");
					}
					addFile(contentFile, convertedContent, replacer);
				}
			} else {
				addFile(resourceFile, convertedFile, replacer);
			}
			return new FileResource(convertedFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException(
					"RelativeDate computation failed on I/O", e);
		}
	}
}

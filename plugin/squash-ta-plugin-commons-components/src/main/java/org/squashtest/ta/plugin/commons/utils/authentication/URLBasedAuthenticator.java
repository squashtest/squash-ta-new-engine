/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.utils.authentication;

import java.net.PasswordAuthentication;
import java.net.URL;

/**
 * <p>This implementation of {@link EngineAuthenticator} will check the requesting url against a 
 * base url and then decide whether it must answer with its credentials (or not).</p>
 *  
 * @author bsiri
 *
 */
public class URLBasedAuthenticator implements EngineAuthenticator{
	
	private String username;
	private String password="";
	private URL baseURL;
	
	private AuthenticatorChain authChain = AuthenticatorChain.getInstance();
	
	/**
	 * 
	 * @param username the username. May be null : if so this instance will always return false.
	 * @param password the password. May be null : it will then be defaulted to "".
	 * @param baseURL the base url.
	 * @throws IllegalArgumentException if the baseURL parameter is null.
	 */
	public URLBasedAuthenticator(String username, String password, URL baseURL){
		this.username=username;
		this.password = (password!=null) ? password : "";
		if (baseURL==null){
			throw new IllegalArgumentException("URLBasedAuthenticator : baseURL cannot be null.");
		}else{
			this.baseURL=baseURL;
		}
	}

	@Override
	public boolean knowsCredentials() {
		
		if (username==null){
			return false;
		}
		
			
		String requestingHost = authChain.getRequestorHost();
		int requestingPort = authChain.getRequestorPort();
		String requestingProtocol = authChain.getRequestorProtocol();
		
		String baseHost = baseURL.getHost();
		int basePort = ( baseURL.getPort()!=-1) ?  baseURL.getPort() : baseURL.getDefaultPort();
		String baseProtocol = baseURL.getProtocol();
		
		return (requestingHost.equals(baseHost) &&
				requestingPort == basePort &&
				requestingProtocol.equals(baseProtocol)
				);
		
		

	}

	@Override
	public PasswordAuthentication getAuthentication() {
		return new PasswordAuthentication(username, password.toCharArray());
	}
	
}
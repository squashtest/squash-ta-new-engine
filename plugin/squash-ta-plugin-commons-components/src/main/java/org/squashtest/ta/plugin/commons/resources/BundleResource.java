/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.File;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;

/**
 * 
 * a Bundle represents a set of files that must stay coherent to each others, some of them having a specific meaning. This bundle also have properties 
 * regarding its content, such as the "main", pretty much like main classes in manifests for java jars.
 * 
 * @author bsiri
 *
 */

@TAResource("bundle")
public class BundleResource extends AbstractBundle implements Resource<BundleResource> {
	
	public BundleResource(){};
	
	/**
	 * 
	 * Creates a new Bundle, where the first arg represents the base directory of that bundle and the second one 
	 * represent the main file. The main file may be null, but if it's not null it must be contained 
	 * in the baseDirectory.
	 * 
	 * 
	 * @param baseDirectory
	 * @param mainFile
	 */
	public BundleResource(File baseDirectory, File mainFile){
		super(baseDirectory,mainFile);
	}
	
	public BundleResource(BundleResource original){
		super(original.getBase(), original.getMain());
	}
	
	/**
	 * Copy here means 'copy constructor'. The copy itself is shallow.
	 */
	@Override
	public BundleResource copy() {
		return new BundleResource(this);
	}
	
	@Override
	public void cleanUp() {
		//we have only shallow copies - someone else will clean the physical resources.
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.parser;

import java.util.Map;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidFileException;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidLineException;

/**
 * Interface for an object that gets events from the parsing of a CSV File.
 * 
 * @author fgautier inpired by work done by edegenetais
 * @author edegenetais
 */
public interface CSVParserListener {
    
    /**
     * This event gives the {@link CSVParserListener} the mamping between column names and column indexes.
     * 
     * This event is gauranteed to be fired and always comes first.
     * 
     * @param columnNamesToIndexMapping : The map linking column names with column indexes.
     * @throws InvalidFileException if the input is ill defined.
     */
    void columnNames(Map<String,Integer> columnNamesToIndexMapping) throws InvalidFileException;
    
    /**
     * This event gives the {@link CSVParserListener} the labels associated with columns in case there
     * are such labels.
     * 
     * This event is not guaranteed to be fired. If fired it should come right after the {@link #columnNames(java.util.Map)} event.
     * 
     * @param parsedLine  : The map linking column names with column indexes.
     * @throws InvalidFileException if the parsed line is not a valid comment line.
     */
    void commentLine(RawCsvLine parsedLine) throws InvalidFileException;
    
    /**
     * This event gives the {@link CSVParserListener} parsed values for a given line.
     * 
     * This event is not guaranteed to be fired. If so it is guaranteed to be fired after
     * the {@link #columnNames(java.util.Map)} event.
     * @param parsedLine : The raw parsed values for a given line.
     * 
     * @throws InvalidLineException if the input line is ill defined.
     */
    void newLine(RawCsvLine parsedLine) throws InvalidLineException;
    
    /**
     * This events signals the {@link CSVParserListener} that the data flow related to
     * the parsing of the CSV file is finished.
     * 
     * @throws InvalidFileException if the total receieved data flow is ill formed.
     */
    void eof() throws InvalidFileException;
}

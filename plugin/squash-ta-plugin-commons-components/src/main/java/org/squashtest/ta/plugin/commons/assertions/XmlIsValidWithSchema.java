/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.assertions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.custommonkey.xmlunit.Validator;
import org.custommonkey.xmlunit.exceptions.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.commons.resources.XMLResource;
import org.squashtest.ta.plugin.commons.resources.XSDResource;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@TAUnaryAssertion("xsd.valid")
public class XmlIsValidWithSchema implements UnaryAssertion<XMLResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(XmlIsValidWithSchema.class);

	private XMLResource xmlToValidate;

	private File xsdForValidation;

	@Override
	public void setActualResult(XMLResource actual) {
		xmlToValidate = actual;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		Iterator<Resource<?>> it = configuration.iterator();
		while (it.hasNext()) {
			Resource<?> resource = it.next();
			if (resource instanceof XSDResource) {
				if (xsdForValidation == null) {
					XSDResource xsd = (XSDResource) resource;
					xsdForValidation = xsd.getXsdFile();
				} else {
					throw new IllegalConfigurationException("A Xsd resource has been already provided more than once");
				}
			} else {
				LOGGER.warn("Ignoring Resource of type " + resource.getClass().getAnnotation(TAResource.class)
						+ " a 'xsd' TA resource is expected");
			}
		}
		if (xsdForValidation == null) {
			throw new IllegalConfigurationException("A XSD should be provided in the USING close");
		}

	}

	@Override
	public void test() throws AssertionFailedException {
		InputSource is;
		try {
			is = new InputSource(new FileInputStream(xmlToValidate.getXMLFile()));
			Validator v = new Validator(is);
			v.useXMLSchema(true);
			if (xsdForValidation != null) {
				v.setJAXP12SchemaSource(xsdForValidation);
			}
			if (!v.isValid()) {
				List<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
				ResourceAndContext assertionFailureContext = new ResourceAndContext();
				assertionFailureContext.setResource(new FileResource(xmlToValidate.getXMLFile()).copy());
				assertionFailureContext.setMetadata(new ExecutionReportResourceMetadata(getClass(), new Properties(),
						FileResource.class, "xmlFileToValidate.xml"));
				context.add(assertionFailureContext); 
				String message = "The XML file is not valid according to the provided XSD: "+v.toString(); 
				throw new AssertionFailedException(message, xmlToValidate, context);
			}
		} catch (FileNotFoundException e) {
			throw new InstructionRuntimeException(e);
		} catch (ConfigurationException e) {
			throw new InstructionRuntimeException(e);
		} catch (SAXException e) {
			throw new InstructionRuntimeException(e);
		}

	}
}

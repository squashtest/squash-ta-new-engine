/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;


/**
 *
 * @author dclaerhout
 */
public abstract class InfrastructureAwareClassLoader extends URLClassLoader {

    private final ClassLoader frameworkLoader = getClass().getClassLoader();
    public InfrastructureAwareClassLoader(URL[] urls) {
        super(urls);

    }
    
    @Override
    public Class<?> loadClass(String className) throws ClassNotFoundException {
        try {
            return frameworkInfrastructureAwareLoadingStrategy(className);
        } catch (ClassNotFoundException e) {
              loggerLoadClass(className,e);  
            throw e;
        }
    }
    
    protected Class<?> urlClassLoaderLoadClass(String className) throws ClassNotFoundException {
        return super.loadClass(className);
    }
 
    public abstract void loggerLoadClass(String className, ClassNotFoundException e);
    
    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException {
        try {
            loggerFindClass(className);
            return super.findClass(className); //To change body of generated methods, choose Tools | Templates.
        } catch (ClassNotFoundException e) {
            loggerFindClassNotFound(className, e);
            throw e;
        }
    }
 
    public abstract void loggerFindClass(String className);
    
    public abstract void loggerFindClassNotFound(String className, ClassNotFoundException e);     

    
    protected URL urlClassLoaderGetResource(String name){
       return super.getResource(name); 
    }
    
    protected Enumeration<URL> urlClassLoaderFindResourcse(String name) throws IOException{
        return super.findResources(name); 
    }
                  
    public Class<?> frameworkInfrastructureAwareLoadingStrategy(String className) throws ClassNotFoundException {
        loggerFrameworkInfrastructureAwareLoadingStrategyLoadingClass(className);
        Class<?> theClass;

        if (isInfrastructureClass(className)) {
            loggerFrameworkInfrastructureAwareLoadingStrategyIsInfrastructure(className);
            theClass = frameworkLoader.loadClass(className);

            return theClass;
        } else {
            theClass = isNotInfrastructureClassLoading(className);
        }
        
        loggerFrameworkInfrastructureAwareLoadingStrategySuccessfullyLoaded(className);
        return theClass;
    }        
    
    public abstract void loggerFrameworkInfrastructureAwareLoadingStrategyLoadingClass(String className);
    
    public abstract void loggerFrameworkInfrastructureAwareLoadingStrategyIsInfrastructure(String className);
    
    public abstract void loggerFrameworkInfrastructureAwareLoadingStrategySuccessfullyLoaded(String className);
    
    /**
     *
     * Specific looading strategy. If this method returns <code>null</code>, the
     * {@link InfrastructureAwareClassLoader} will fall back to the default {@link URLClassLoader#loadClass(java.lang.String)
     * } method.
     *
     * @return <code>null</code> if you want to load the class with {@link URLClassLoader#loadClass(java.lang.String)
     * } method.
     */
    public abstract Class<?> isNotInfrastructureClassLoading(String className) throws ClassNotFoundException;

     /**
     * To avoid schizoïd snafus, we exclude some 'infrastructure' classes from classloading isolation.
     * 
     * @param className the name of the target class.
     * @return <code>true</code> if it is infrastructure (classes for which we want classloading to remain in order to ensure consistent
     * communication with the outer (engine) world), <code>false</code> for any other class.
     * )
     */
    public abstract boolean isInfrastructureClass(String className);
    
    @Override
    public URL getResource(String name) {
        loggerGetResourceInit(name);
        URL theURL=super.getResource(name);
        
        if(theURL==null){
            theURL=urlResourceIsNullStrategy(name);
        }
        if(theURL!=null){
            loggerGetResourceFound(name);
        }
        return theURL;
    }
    
    public abstract void loggerGetResourceInit(String name);
    
    public abstract void loggerGetResourceFound(String name);    
    
    public abstract URL urlResourceIsNullStrategy(String name);
    
    @Override
    public InputStream getResourceAsStream(String name) {

        loggerGetResourceAsStreamInit(name);
        InputStream stream;
        try {
            final URL resource = getResource(name);
            if (resource == null) {
                stream = null;
            } else {
                stream = resource.openStream();
            }
        } catch (IOException ex) {
            stream = null;
            loggerGetResourceAsStreamFailure(name,ex); 
        }
        return stream;
    }
    
    public abstract void loggerGetResourceAsStreamInit(String name);
    
    public abstract void loggerGetResourceAsStreamFailure(String name, IOException ex);   

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        loggerGetResourcesInit(name);
        List<URL> resourceURLs = addResourceURLs(name);

        loggerGetResourcesFound(resourceURLs);   


        return Collections.enumeration(resourceURLs); //To change body of generated methods, choose Tools | Templates.
    }
    
    public abstract List<URL> addResourceURLs(String name) throws IOException;
    
    public abstract void loggerGetResourcesInit(String name);
    
    public abstract void loggerGetResourcesFound(List<URL> resourceURLs);   

}

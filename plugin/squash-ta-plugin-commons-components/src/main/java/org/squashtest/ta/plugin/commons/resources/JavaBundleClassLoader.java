/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.squashtest.ta.plugin.commons.library.InfrastructureAwareClassLoader;


/**
 *
 * @author dclaerhout
 */
public class JavaBundleClassLoader extends InfrastructureAwareClassLoader{
    private static final Logger LOGGER = InitSilencingLoggerFactory.getLogger(JavaBundleClassLoader.class);
    private static  Set<FrameworkCLInfrastructureAwareTuning> frameworkCLStrategies=new HashSet<>();
    
    
    public JavaBundleClassLoader(URL[] urls) {

        super(computeClasspathURLset(urls));
        frameworkCLStrategies.add(new SKFEngineContextCLInfrastructureAwareTuning());
    }
    
 private static final URL[] computeClasspathURLset(URL[] urls){
    try{
        InitSilencingLoggerFactory.switchOnInitPhase();
            return urls;
    } finally {
        InitSilencingLoggerFactory.switchOffInitPhase();
    }
 }
 
    @Override
    public void loggerLoadClass(String className, ClassNotFoundException e) {
        try {
        InitSilencingLoggerFactory.switchOnInitPhase();
            if (LOGGER.isTraceEnabled()) {
                LOGGER.error("JavaBundleClassLoader: Woops ! Could not find {}", className, e);
            } else {
                LOGGER.error("JavaBundleClassLoader: Woops ! Could not find {}. For full stack trace, please switch to TRACE mode.", className);
            }
        } finally {
            InitSilencingLoggerFactory.switchOffInitPhase();
        }
    }
    
    @Override
    public void loggerFindClass(String className) {
        LOGGER.trace("JavaBundleClassLoader: Looking up {}", className);
    }
    
    @Override
    public void loggerFindClassNotFound(String className, ClassNotFoundException e) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.debug("JavaBundleClassLoader: NoclassDefFoundException while looking for {}", className, e);
        } else {
            LOGGER.debug("JavaBundleClassLoader: NoclassDefFoundException while looking for {}. For full stack trace, please switch to TRACE mode.", className);
        }
    }
            
    @Override
    public void loggerFrameworkInfrastructureAwareLoadingStrategyLoadingClass(String className) {
        LOGGER.debug("JavaBundleClassLoader : Trying to load {}", className);
    }

    @Override
    public void loggerFrameworkInfrastructureAwareLoadingStrategyIsInfrastructure(String className) {
        LOGGER.debug("JavaBundleClassLoader : Loading {} from the framework classloader as it is from the bridge package or infrastructure.", className);
    }
    
    @Override
    public void loggerFrameworkInfrastructureAwareLoadingStrategySuccessfullyLoaded(String className) {
        LOGGER.trace("JavaBundleClassLoader : Successfully loaded {}", className);
    }
    
    @Override
    public void loggerGetResourceInit(String name) {
        LOGGER.debug("JavaBundleClassLoader : Getting resource URL for {}",name);
    }

    @Override
    public void loggerGetResourceFound(String name) {
        LOGGER.trace("JavaBundleClassLoader : Found resource {} at {}",name);
    } 
    
    @Override
    public void loggerGetResourceAsStreamInit(String name) {
        LOGGER.trace("JavaBundleClassLoader : Opening input stream from resource {}", name);
    }

    @Override
    public void loggerGetResourceAsStreamFailure(String name, IOException ex) {
        LOGGER.warn("JavaBundleClassLoader : Failed to open resource Stream for {}", name, ex);
    }

        @Override
    public void loggerGetResourcesInit(String name) {
        LOGGER.debug("JavaBundleClassLoader : Getting all resources matching name {}", name);
    }

    @Override
    public void loggerGetResourcesFound(List<URL> resourceURLs) {
        LOGGER.trace("JavaBundleClassLoader : Found {} matching resources :\n{}", resourceURLs.size(), resourceURLs);
    }
    
     @Override   
     /**
     * To avoid schizoïd snafus, we exclude some 'infrastructure' classes from classloading isolation.
     * 
     * @param className the name of the target class.
     * @return <code>true</code> if it is infrastructure (classes for which we want classloading to remain in order to ensure consistent
     * communication with the outer (engine) world), <code>false</code> for any other class.
     * )
     */
    public boolean isInfrastructureClass(String className) {
        for(FrameworkCLInfrastructureAwareTuning tuner:frameworkCLStrategies){
            if(tuner.isInfrastructure(className)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public Class<?> isNotInfrastructureClassLoading(String className) throws ClassNotFoundException {
    Class<?> theClass;

        if (LOGGER.isTraceEnabled()) {//give a chance to see the stacktrace, but avoid cluttering debug mode logs
            LOGGER.trace("JavaBundleClassLoader: Loading {} with URLClassLoader as it was not a part of the infrastructure", className);
        } else {
            LOGGER.debug("JavaBundleClassLoader: Loading {} with URLClassLoader as it is not a part of the infrastructure", className);
        }
 
            theClass = super.urlClassLoaderLoadClass(className);

        return theClass;
    }
    
    @Override
    public List<URL> addResourceURLs(String name) throws IOException {
      List<URL> resourceURLs = new ArrayList<>();

        resourceURLs.addAll(Collections.list(super.urlClassLoaderFindResourcse(name)));
        
        return resourceURLs;
    }

    @Override
    public URL urlResourceIsNullStrategy(String name) {
       LOGGER.trace("JavaBundleClassLoader: URL resource not found in my base package");
       return null;
    }

}

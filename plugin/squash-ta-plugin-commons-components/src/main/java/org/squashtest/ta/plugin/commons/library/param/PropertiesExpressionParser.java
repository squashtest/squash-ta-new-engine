/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Expression parser implementation for placeholder
 * 
 * @author bfranchet
 */
public class PropertiesExpressionParser implements ExpressionParser {

	/** Pattern for valid key properties */
	private static final Pattern VALIDATION_PATTERN = Pattern.compile("^([a-zA-Z0-9_\\.-]*)$");

	/** The list of properties */
	private Properties properties;

	/**
	 * Constructor which populate the internal property list
	 * 
	 * @param properties
	 */
	public PropertiesExpressionParser(Properties properties) {
		this.properties = properties;
	}

	@Override
	public boolean accept(String expression) {
		Matcher matcher = VALIDATION_PATTERN.matcher(expression);
		return matcher.matches();
	}

	@Override
	public Expression parse(String expression) {
		String value = properties.getProperty(expression);
		/*
		 * If the expression, which here correspond to a property key, is not found in the list of property keys, then
		 * this placeholder is escaped
		 */
		if (value == null) {
			value = "${" + expression + "}";
		}
		return new LitteralExpression(value);
	}

}

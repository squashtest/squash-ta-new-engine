/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.parser.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidFileException;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidLineException;
import org.squashtest.ta.plugin.commons.library.csv.parser.CSVParserListener;
import org.squashtest.ta.plugin.commons.library.csv.parser.RawCsvLine;

/**
 * A simple Decorator used to skip trailing empty lines of a CSV. Those empty lines
 * may be for instance generated using common office tools while saving calc sheets
 * as CSV. 
 * 
 * @author fgautier
 */
public class SkipEmptyLinesParserListener implements CSVParserListener {

    /**
     * A logger used to log class related messages. 
     */
    private  static final Logger LOGGER = LoggerFactory.getLogger(SkipEmptyLinesParserListener.class);
    
    /**
     * The original component of this Decorator Pattern.
     */
    private final CSVParserListener originalInterpreter;
    
    /**
     * Null entry that should be checked for empty lines.
     */
    private final String nullEntry;
    
    /**
     * Stores in receieved order (hence the list that guarantees insertion order when iterated over) the 
     * found empty lines.
     * 
     * Dolores umbridge once said I WILL have ORDER !
     */
    private List<RawCsvLine> emptyLines = new ArrayList<>();
    
    /**
     * Standard constructor. By default the null entry used to search for empty lines
     * is set to the empty string "";
     * 
     * @param originalInterpreter The original listener with it's own logic. This sould 
     * not care about dealing with empty lines. Empty lines are the sole purpose of this
     * decorator
     */
    public SkipEmptyLinesParserListener(CSVParserListener originalInterpreter) {
        this(originalInterpreter,"");
    }
    
    /**
     * Fully paramatrized contructor. 
     * 
     * @param originalInterpreter The original listener with it's own logic. This sould 
     * not care about dealing with empty lines. Empty lines are the sole purpose of this
     * decorator
     * @param nullEntry Null entry that should be checked for empty lines.
     */
    public SkipEmptyLinesParserListener(CSVParserListener originalInterpreter, String nullEntry) {
        this.originalInterpreter=originalInterpreter;
        this.nullEntry=nullEntry;
        if(originalInterpreter == null) {
            throw new IllegalArgumentException("Can't decorate a {null} CSVParserListener !");
        }
        if(nullEntry == null) {
            throw new IllegalArgumentException("Can't use a {null} null entry to check for empty lines !");
        }
    }
    
    @Override
    public void columnNames(Map<String, Integer> columnNamesToIndexMapping) throws InvalidFileException {
        /* Let's feed our orignial listener.*/
        this.originalInterpreter.columnNames(columnNamesToIndexMapping);
    }

    @Override
    public void commentLine(RawCsvLine parsedLine) throws InvalidFileException {
        /* We do not care if the comment line is empty or not we just feed the decorated listener with it. */
        originalInterpreter.commentLine(parsedLine);
    }

    @Override
    public void newLine(RawCsvLine parsedLine) throws InvalidLineException {
        if(isEmpty(parsedLine)) {
            emptyLines.add(parsedLine);
            LOGGER.debug("Processing new empty line. Total number of empty lines encountered so far : {}", emptyLines.size());
        } else {
            LOGGER.debug("Processing new line containing data. Adding {} empty line(s) encountered before it.", emptyLines.size());
            feedListenerWithEncounteredEmptyLines();
            originalInterpreter.newLine(parsedLine);
            // Reset empty lines storage
            emptyLines=new ArrayList<>();
        }
    }

    @Override
    public void eof() throws InvalidFileException {
        /* Let's signal the original listener that the end of file has been reached. */
        originalInterpreter.eof();
    }
    
    
    /**
     * This utility feeds the orginal listener with the encountered (if any) empty lines
     * stored in emptyLines. The feeding process preserves the order of received empty lines. 
     * 
     * @throws InvalidLineException if something goes wrong while feading the original
     * listener
     */
    private void feedListenerWithEncounteredEmptyLines() throws InvalidLineException {
        // As specified in the java specs, The itereators for list is guaranteed to preserve isnertion order.
        for (RawCsvLine emptyLine : emptyLines) {
            originalInterpreter.newLine(emptyLine);
        }
    }
    
    /**
     * This utility method check if a received line is empty.
     * 
     * @param line : The checked line
     * 
     * @throws InvalidLineException if the line or its values are null since those signal
     * ill formed lines.
     */
    private boolean isEmpty(RawCsvLine line) throws InvalidLineException {
        if(line == null ) {
            throw new InvalidLineException("By definition a {null} parsed line is neither empty or not empty it is null.");
        }
        if(line.getValues()==null) {
            throw new InvalidLineException("By definition a parsed line with null values is neither empty or not empty it is just ill formed.");
        }
        
        boolean isEmpty=true;
        
        for (int i=0; i<line.getValues().length && isEmpty; i++) {
            isEmpty = line.getValues()[i].trim().equals(nullEntry);
        }
        
        return isEmpty;
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * 
 * <p><strong>Description:</strong>
 * Makes the test execution sleep for a given time (expressed in seconds)</p>
 * <div><strong>DSL example:</strong>
 * <pre>EXECUTE log WITH $(message à afficher) USING $(logLevel:ERROR) AS whatever</pre>
 * </div>
 *  
 * @author cruhlmann
 *
 */
@TACommand("log")
public class WriteLogCommand implements Command<FileResource, VoidTarget> {
        private static final Logger LOGGER=LoggerFactory.getLogger(WriteLogCommand.class);
    
        private static final String LOG_LEVEL = "logLevel";
	
	private static final String LOG_NAME = "[USER_MESSAGE]";
	
	private static final String ERROR_MESSAGE = "Cannot write an empty message into the logs.";
        private static final String RE_ENABLE_INSANE_LEGACY_CRASH_BEHAVIOR_ON_EMPTY_MESSAGES = "legacyEmptyMessageCrash";
        private static final String EMPTY_MESSAGE_WARNING_MESSAGE = LOG_NAME+" has just received an empty message !\nIf you like sport and want this to crash your test as in the good old time, add the 'legacyEmptyMessageCrash:true' option to the command options in the USING clause";
		
	private enum LoggerMode {DEBUG, INFO, WARN, ERROR}
	
        private File message;

        private Map<String, String> options=Collections.emptyMap();
        
	//return a list of the known logger mode, for error messages.
	private static String listLoggerMode(){
		LoggerMode[] list = LoggerMode.values();
		StringBuffer concat = new StringBuffer("");
		for (int count = 0 ; count < list.length ; count++){
			concat.append(list[count].toString());
			if (count < list.length -1){
				concat.append(", ");
			}
		}
		return concat.toString();
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		try {
			for (Resource<?> resource : configuration){
				if (FileResource.class.isAssignableFrom(resource.getClass())){
					FileResource fileLogger = (FileResource) resource;
					options = getOptions(fileLogger);
				}
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("The USING clause is incorrect : the configuration failed", e);
		} catch (IllegalArgumentException e) {
			throw new InstructionRuntimeException("The USING clause is incorrect : "+e.getMessage(), e);
		}
	}

	@Override
	public void setTarget(VoidTarget target) {
		// noop	
	}

	@Override
	public void setResource(FileResource resource) {
			this.message = resource.getFile();
	}
	
	@Override
	public void cleanUp() {
		// noop
	}

	@Override
	public VoidResource apply() {
                Logger userLogger = LoggerFactory.getLogger(LOG_NAME);
		LoggerMode loggerMode = getLoggerMode();
                
                boolean enabled = checkIfEnabled(loggerMode, userLogger);
                /**
                 * Why waste memory and time by building a message you'll nerver log?
                 */
                if(enabled){
                    doMessageLog(loggerMode, userLogger);
                }
		return new VoidResource();
	}

        protected boolean checkIfEnabled(LoggerMode loggerMode, Logger userLogger) throws InstructionRuntimeException {
            boolean enabled;
            switch(loggerMode){
                case DEBUG:
                    enabled=userLogger.isDebugEnabled();
                    break;
                case INFO:
                    enabled=userLogger.isInfoEnabled();
                    break;
                case WARN:
                    enabled=userLogger.isInfoEnabled();
                    break;
                case ERROR:
                    enabled=userLogger.isErrorEnabled();
                    break;

                default:
                    throw new InstructionRuntimeException("The Logger mode " + loggerMode + " is not correctly implemented. Please contact the incriminated developper.");
            }
            return enabled;
        }

        protected void doMessageLog(LoggerMode loggerMode, Logger userLogger) throws BadDataException, InstructionRuntimeException {
            String messageContent;
            try {
                messageContent = getMessageFromFile(message);
            } catch (IOException e) {
                throw new BadDataException("cannot read message from resource "+message.getAbsolutePath(), e);
            }

            if (messageContent.isEmpty()){
                if(options.containsKey(RE_ENABLE_INSANE_LEGACY_CRASH_BEHAVIOR_ON_EMPTY_MESSAGES) 
                        && Boolean.parseBoolean(options.get(RE_ENABLE_INSANE_LEGACY_CRASH_BEHAVIOR_ON_EMPTY_MESSAGES))){
                    throw new BadDataException(ERROR_MESSAGE);
                }else{
                    /*
                        We log our warning on the same level as the initial message, 
                        so as not to have warnings pop where the initial log message 
                        had not been required in the first place!
                    */
                    logOnRequiredLevel(loggerMode, LOGGER, EMPTY_MESSAGE_WARNING_MESSAGE);
                }
            }

            logOnRequiredLevel(loggerMode, userLogger, messageContent);
    }

    protected void logOnRequiredLevel(LoggerMode loggerMode, Logger userLogger, String messageContent) throws InstructionRuntimeException {
        switch (loggerMode) {
            case DEBUG:
                userLogger.debug(messageContent);
                break;
            case INFO:
                userLogger.info(messageContent);
                break;
            case WARN:
                userLogger.warn(messageContent);
                break;
            case ERROR:
                userLogger.error(messageContent);
                break;
                
            default:
                throw new InstructionRuntimeException("The Logger mode " + loggerMode + " is not correctly implemented. Please contact the incriminated developper.");
        }
    }
	
	
	private Map<String, String> getOptions (FileResource fileResource) throws IOException, IllegalArgumentException{
		return OptionsReader.BASIC_READER.getOptions(fileResource.getFile());
	}

	private String getMessageFromFile(File resource) throws IOException {
		
		String fileMessage = "";		
		BufferedReader br = new BufferedReader(new FileReader(resource));
		StringBuilder msgBuilder=new StringBuilder();
		try{
			boolean goOn=true;
			boolean multiline="yes".equals(options.get("multiline"));
			while (br.ready() && goOn){
				msgBuilder.append(br.readLine().trim()).append("\n");
				goOn=multiline;
			}
			fileMessage=msgBuilder.toString();
		}finally{
			if (br != null){
				br.close();
			}
		}
		return fileMessage;
	}
	
	//return the chosen Logger mode
	private LoggerMode getLoggerMode() {
		
		LoggerMode loggerMode = null;
		//if the mode isn't specified, we choose INFO as default mode
		if (options == null || !options.containsKey(LOG_LEVEL)){
			loggerMode = LoggerMode.INFO;
		}else{
			String logLevel = options.get(LOG_LEVEL);
			
			for (LoggerMode value : LoggerMode.values()){
				if (logLevel.equalsIgnoreCase(value.toString())){
					loggerMode = value;
				}
			}
			if (loggerMode == null){
				//if there is a mode but it is not recognized, it fails.
				throw new BadDataException("The Logger mode "+logLevel+" is unknown. Please note that you must use one of those mode: "+listLoggerMode());
			}
		}
		return loggerMode;
	}
}

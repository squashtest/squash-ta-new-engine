/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class EntityExpressionParser implements ExpressionParser{

	private static final Properties ENTITY_MAPPING=new Properties();
	
	static{
		InputStream resourceAsStream = EntityExpressionParser.class.getResourceAsStream("entityMappings.properties");
		try {
			ENTITY_MAPPING.load(resourceAsStream);
		} catch (IOException e) {
			//should not happen. If it does, we are running a corrupt engine! Seppuku!
			ExceptionInInitializerError error=new ExceptionInInitializerError("entityMappings.properties loading failed");
			error.initCause(e);//too sad Sonar shuns initCause()...<
			throw error;//NOSONAR
		}
	}
	
	@Override
	public boolean accept(String expression) {
		return (ENTITY_MAPPING.getProperty(expression)!=null);
	}

	@Override
	public Expression parse(String expression) {
		String entity = ENTITY_MAPPING.getProperty(expression);
		if(entity==null){
			throw new IllegalExpressionException(expression+" is not a known entity name");
		}
		return new LitteralExpression(entity);
	}

}

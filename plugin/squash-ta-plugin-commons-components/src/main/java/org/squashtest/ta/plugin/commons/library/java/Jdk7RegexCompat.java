/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.slf4j.LoggerFactory;

/**
 * Compatiblity layer between the JDK8 regex we write in the main code
 * and JDK7 runtime.
 * <strong>WARNING</strong> : this is NOT an all-purpose JDK8 to JDK7
 * rewriting facility, it only supports use cases we've met in the Squash TA framework.
 * 
 * @author edegenetais
 */
class Jdk7RegexCompat {

    public static final Jdk7RegexCompat INSTANCE = new Jdk7RegexCompat();

    Pattern compile(String regex) {
        try {
            return Pattern.compile(regex);
        } catch (PatternSyntaxException e) {
            try {
                String correctedRegex = regex.replaceAll("\\Q\\h\\E", "[ \\t\\xA0\\u1680\\u180e\\u2000-\\u200a\\u202f\\u205f\\u3000]");
                return Pattern.compile(correctedRegex);
            } catch (PatternSyntaxException failsIn7Compat) {
                /* 
                 * Support for the \\h escape only came in JDK8, so here we replace it by its explicit charcter class.
                 * We base the regex definition on the escape any way because it is way more readable.
                 */
                LoggerFactory.getLogger(Jdk7RegexCompat.class).error("Tried to edit regex '" + regex + "' for jdk-7 compatibility, but failed", failsIn7Compat);
                failsIn7Compat.addSuppressed(e);
                throw failsIn7Compat;
            }
        }
    }

}

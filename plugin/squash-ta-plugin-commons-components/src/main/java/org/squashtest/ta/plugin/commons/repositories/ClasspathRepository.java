/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.repositories;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TARepository;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;

@TARepository("classpath")
public class ClasspathRepository implements ResourceRepository {

	private static Logger LOGGER = LoggerFactory.getLogger(ClasspathRepository.class);
	
        private Set<File> deployed=new HashSet<File>();
        
	private Properties effectiveConfiguration;
	
	public ClasspathRepository() {}
	
	public ClasspathRepository(Properties properties){
		super();
		effectiveConfiguration = properties;
	}

	@Override
	public Properties getConfiguration() {
		return effectiveConfiguration;
	}

	@Override
	public void init() {
		// nothing to do
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void cleanup() {
            for(File file:deployed){
                boolean deleted=file.delete();
                if(deleted){
                    LOGGER.debug("Cleaned up classpath resource deployed as {}",file.getAbsolutePath());
                }else{
                    LOGGER.warn("Failed to delete classpath resource deployed as {} during cleanup.",file.getAbsolutePath());
                }
            }
	}

	@Override
	public FileResource findResources(String resourcePath) {
		URL url = this.getClass().getClassLoader()
					  .getResource(resourcePath);
		if (url!=null){
			try(InputStream is=url.openStream();
					){
				
				File file = File.createTempFile("classpath_", ".file");
                                deployed.add(file);
                                file.deleteOnExit();
				OutputStream os=new FileOutputStream(file);
				
				IOUtils.copyLarge(is, os);
				
				FileResource resource = new FileResource(file);
				
				LOGGER.info("ClasspathRepository : loaded resource '"+resourcePath+"'.");
				return resource;
				
			}catch(IOException ex){
				LOGGER.warn("ClasspathRepository : resource path '"+resourcePath+"' was found at URL '"+url+"', but an error prevented loading the resource from that URL.{}",ex.getMessage());
				MissingResourceException reported=new MissingResourceException("Resource URL cannot opened","ClasspathRepository:repository",resourcePath);
				reported.initCause(ex);
				throw reported;
			}
		}else{
			return null;	//legal by contract.
		}
	}



}

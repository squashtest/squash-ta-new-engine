/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.File;
import java.io.IOException;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cjuillard
 */
public class StandardPlacementJavaFileManager extends AbstractPlacementJavaFileManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(StandardPlacementJavaFileManager.class);

    private JavaFileManager javaFileManager;

    /**
     * Create a java file manager instance to override fileManager class files
     * placing behavior.
     *
     * @param fileManager the delegate fileManager to use.
     * @param bundleClassLocation the class file location.
     */
    public StandardPlacementJavaFileManager(StandardJavaFileManager fileManager, File bundleClassLocation) {
        super(fileManager, bundleClassLocation);
        javaFileManager = fileManager;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location,
            String className, JavaFileObject.Kind kind, FileObject sibling)
            throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Java file for output: " + location.getName() + " kind: " + kind + " class name: " + className + " sibling: " + sibling.getName());
        }
        registeredClassNames.add(className);
        return javaFileManager.getJavaFileForOutput(location, className, kind, sibling);
    }
}

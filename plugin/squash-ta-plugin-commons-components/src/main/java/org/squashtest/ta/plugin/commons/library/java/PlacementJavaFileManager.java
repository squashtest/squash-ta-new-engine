/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.io.File;
import java.io.IOException;

import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Delegate java file manager to direct output files to our decided storage place and register them on the fly.
 * @author edegenetais
 *
 */
public class PlacementJavaFileManager extends AbstractPlacementJavaFileManager{
	
	private static final Logger LOGGER=LoggerFactory.getLogger(PlacementJavaFileManager.class);
	
	private static final BundleFilePlacementAlgorithm FILE_PLACEMENT_ALGORITHM=new BundleFilePlacementAlgorithm();
	
	/**
	 * Create a java file manager instance to override fileManager class files placing behavior.
	 * @param fileManager the delegate fileManager to use.
	 * @param bundleClassLocation the class file location.
	 */
	public PlacementJavaFileManager(StandardJavaFileManager fileManager, File bundleClassLocation) {
		super(fileManager,bundleClassLocation);
	}

	@Override
	public JavaFileObject getJavaFileForOutput(Location location,
			String className, Kind kind, FileObject sibling)
			throws IOException {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Java file for output: "+location.getName()+" kind: "+kind+" class name: "+className+" sibling: "+sibling.getName());
		}
		
		JavaFileObject objectFile=new PlacementJavaFileObject(FILE_PLACEMENT_ALGORITHM.getElementURI(outputLocation, className, kind), Kind.CLASS);
		registeredClassNames.add(className);
		return objectFile;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Base class of the XML transpose of a CSV Field. A field holds the value of a CSV column for a single 
 * line.
 * 
 * @author fgautier inpired by work done by edegenetais
 * @author edegenetais
 */
public class CsvXmlField {
    
    /**
     * Holds the name of the column this field refers to.
     */
    @XmlAttribute
    private String column;
    /**
     * Holds the value of this field.
     */
    @XmlValue
    private String value;

    /**
     * Default constructor for Jaxb enumeration.
     */
    public CsvXmlField() {/* Jaxb */ }

    
    /**
     * Fully parametrized constuctor. 
     * 
     * @param column : Name of the column the will be Field will refer to.
     * @param value : Value of the Field
     */
    public CsvXmlField(String column, String value) {
        this.column = column;
        this.value = value;
    }

    /**
     * Simple getter for Jaxb.
     * 
     * @return the column the field refers to.
     */
    public String getColumn() {
        return column;
    }
    /**
     * Simple getter for Jaxb.
     * 
     * @return the value of the field.
     */
    public String getValue() {
        return value;
    }
    
    
}

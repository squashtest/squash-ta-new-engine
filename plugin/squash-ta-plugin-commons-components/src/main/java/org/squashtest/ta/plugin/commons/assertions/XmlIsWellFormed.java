/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.commons.library.reporting.IdentityTranslation;
import org.squashtest.ta.plugin.commons.library.reporting.Translator;
import org.squashtest.ta.plugin.commons.resources.TranslationResource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Assertion to check if XML file is valid before converting it.
 * Use if the file is an input file with questionable validity 
 * (ex: file from the SUT, which you want to validate)
 * 
 * @author edegenetais
 */
@TAUnaryAssertion("xml.well.formed")
public class XmlIsWellFormed implements UnaryAssertion<FileResource>{
    
    private static final Logger LOGGER=LoggerFactory.getLogger(XmlIsWellFormed.class);
    
    private static final String NO_VALID_XML = "The resource content is not valid XML";    
    
    private Translator translator=new IdentityTranslation();
    private FileResource actual;
    
    @Override
    public void setActualResult(FileResource actual) {
        this.actual=actual;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        for(Resource<?> res:configuration){
            if(res instanceof TranslationResource){
                LOGGER.debug("Picked up translation resource of type {}",res.getClass().getName());
                translator=((TranslationResource)res).getTranslator();
            }else{
                final String resourceNature = res.getClass().getAnnotation(TAResource.class).value();
                final String assertionNature = getClass().getAnnotation(TAUnaryAssertion.class).value();
                LOGGER.warn("Assertion {}: Ignoring unexpected configuration resources of type {}.", assertionNature, resourceNature);
            }
        }
    }

    @Override
    public void test() throws AssertionFailedException {
        try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
		 
			DefaultHandler handler = new DefaultHandler() {};
			
			saxParser.parse(actual.getFile(), handler);
		}catch(SAXParseException se){
                        LOGGER.warn(NO_VALID_XML,se);
                        List<ResourceAndContext> context = new ArrayList<>();
                        
                        addRAC(actual.getFile(), "xmlInput.xml", context);
                        
                        final String message = "column:"+se.getColumnNumber()+";line:"+se.getLineNumber()+";detail:"+se.toString();
                                                
                        addExceptionMessageRAC(translator.translate(message), context);
                        
			throw new AssertionFailedException(NO_VALID_XML, actual, context);
                }catch (SAXException se) {
                        LOGGER.warn(NO_VALID_XML,se);
                        List<ResourceAndContext> context = new ArrayList<>();
                        
                        addRAC(actual.getFile(), "xmlInput.xml", context);
                                                
                        addExceptionMessageRAC(translator.translate(se.toString()), context);
                        
			throw new AssertionFailedException(NO_VALID_XML, actual, context);
		} catch (
                        IOException | 
                        ParserConfigurationException e
                        ) {
			throw new BadDataException("Technical failure while trying to convert FileResource to XmlResource",e);
		} 
    }


    private void addExceptionMessageRAC(final String exceptionMessage, List<ResourceAndContext> context) {
        try {
            final File xmlAnomalyReport = File.createTempFile("xmlWFCheck", ".xml", TempDir.getExecutionTempDir());
            final String message = exceptionMessage;
            final SimpleLinesData simpleLinesData = new SimpleLinesData(message.getBytes());
            simpleLinesData.write(xmlAnomalyReport, "utf-8");
            addRAC(xmlAnomalyReport, "xmlAnomalyReport.txt", context);
        } catch (IOException ex) {
            LOGGER.warn("Technical failure while writing XML validation report. Report will be missing.",ex);
        }
    }

    private void addRAC(final File file, final String attachmentName, List<ResourceAndContext> context) {
        ResourceAndContext assertionFailureContext = new ResourceAndContext();
        assertionFailureContext.setResource(new FileResource(file).copy());
        assertionFailureContext.setMetadata(new ExecutionReportResourceMetadata(getClass(), new Properties(),
                FileResource.class, attachmentName));
        context.add(assertionFailureContext);
    }
    
}

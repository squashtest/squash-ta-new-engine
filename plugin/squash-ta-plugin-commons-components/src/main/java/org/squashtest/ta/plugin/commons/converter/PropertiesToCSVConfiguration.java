/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.commons.resources.CSVConfiguration;
import org.squashtest.ta.plugin.commons.library.csv.CSVEscapeCharacter;
import org.squashtest.ta.plugin.commons.library.csv.CSVNullEntry;
import org.squashtest.ta.plugin.commons.library.csv.CSVQuoteCharacter;
import org.squashtest.ta.plugin.commons.library.csv.CSVSeparator;
import org.squashtest.ta.plugin.commons.library.csv.ConfigEnum;

/**
 * Converter from file resource to CSV.
 * @author edegenetais
 */
@TAResourceConverter("csv.config")
public class PropertiesToCSVConfiguration implements ResourceConverter<PropertiesResource, CSVConfiguration>{
    
    private static final String SEPARATOR = "separator";
    private static final String QUOTE_CHARACTER = "quote";
    private static final String NULL_ENTRY = "null.entry";
    private static final String ESCAPE_CHARACTER = "escape";
    
    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        new ConfigurationExtractor(this).expectNoConfiguration(configuration);
    }
    
    @Override
    public CSVConfiguration convert(PropertiesResource resource) {
            Properties prop=resource.getProperties();
            List<ConfigEnum> config=new ArrayList<ConfigEnum>();
            if(prop.containsKey(SEPARATOR)){
                String separatorString=prop.getProperty(SEPARATOR);
                config.add(CSVSeparator.fromString(separatorString==null?"null":separatorString));
            }
            if(prop.containsKey(QUOTE_CHARACTER)){
                String separatorString=prop.getProperty(QUOTE_CHARACTER);
                config.add(CSVQuoteCharacter.fromString(separatorString==null?"null":separatorString));
            }
            if(prop.containsKey(NULL_ENTRY)){
                String separatorString=prop.getProperty(NULL_ENTRY);
                config.add(CSVNullEntry.fromString(separatorString==null?"null":separatorString));
            }
            if(prop.containsKey(ESCAPE_CHARACTER)){
                String separatorString=prop.getProperty(ESCAPE_CHARACTER);
                config.add(CSVEscapeCharacter.fromString(separatorString==null?"null":separatorString));
            }
            return new CSVConfiguration(config.toArray(new ConfigEnum[config.size()]));
    }
    
    @Override
    public float rateRelevance(PropertiesResource input) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cleanUp() {
        //noop - no resources to free
    }
}

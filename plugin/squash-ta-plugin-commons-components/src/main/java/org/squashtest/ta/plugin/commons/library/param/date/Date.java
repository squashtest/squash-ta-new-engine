/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.plugin.commons.library.param.IllegalExpressionException;

/**
 *
 * @author edegenetais
 */
public class Date implements DateFunction{
    protected static final Logger LOGGER = LoggerFactory.getLogger(Date.class);
    
    private Properties dataSource;
    
    @Override
    public Calendar evaluate(Calendar inputDate, String argument) {
        try{
            if(LOGGER.isWarnEnabled() && inputDate!=null){
                    LOGGER.warn("Ignoring useless input date {} computed from the beginning of the formula...");
            }
            if(Pattern.compile("^[^,]+,[^,]+$").matcher(argument).matches()){

            }else{
                throw new IllegalExpressionException("Argument '"+argument+"' is not valid it should be of the form : <property key>,<date format>");
            }
            String[] args=argument.split(",");
            String dateProp=args[0];
            String dateVal=dataSource.getProperty(dateProp);
            String dateFormat=args[1];
            LOGGER.debug("Called with arguments {}(=>{}) and {}",inputDate,dateVal,dateFormat);

            if(dateVal==null){
                throw new IllegalExpressionException("Property "+dateProp+" is not known in the date param format.");
            }

            java.util.Date date=new SimpleDateFormat(dateFormat).parse(dateVal);
            final GregorianCalendar result = new GregorianCalendar();
            result.setTime(date);
            return result;
        }catch(ParseException e){
            throw new IllegalExpressionException("Illegal date or date format",e);
        }
    }

    @Override
    public void addDataSource(Properties dataSource) {
        LOGGER.debug("Injecting data source (cfg) object {}",dataSource);
        this.dataSource=(dataSource==null?new Properties():dataSource);
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.parser;

import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.CSVParsingError;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidFileException;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.InvalidLineException;
import org.squashtest.ta.plugin.commons.library.csv.parser.impl.SkipEmptyLinesParserListener;
import org.squashtest.ta.plugin.commons.resources.CSVConfiguration;

/**
 *
 * @author fgautier
 */
public class CSVParser {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CSVParser.class);
    
    private final List<CSVParserListener> interpreters = new ArrayList<>();
    
    private boolean hasCommentLine = false;
    private boolean skipEmptyLines = false;
    
    private final CSVConfiguration csvConfig;

    private CSVParser(CSVConfiguration csvConfig, boolean hasCommentLine, boolean skipEmptyLines) {
        this.csvConfig = csvConfig;
        this.hasCommentLine = hasCommentLine;
        this.skipEmptyLines = skipEmptyLines;
    }
    
    public static CSVParser newInstance(CSVConfiguration csvConfig, boolean hasCommentLine, boolean skipEmptyLines) {
        return new CSVParser(csvConfig, hasCommentLine, skipEmptyLines).registerListener(new LoggerListener());
    }
    
    public CSVParser registerListener(CSVParserListener listener) {
        final CSVParserListener decoratedListener = (skipEmptyLines) ? new SkipEmptyLinesParserListener(listener,csvConfig.getNullEntry().getCharacter()) : listener;
        this.interpreters.add(decoratedListener);
        return this;
    }
    
    public void parse(URL source, String encoding) throws CSVParsingError {
        try (
                InputStream csvFileStream = new FileInputStream(new File(source.toURI()));
                InputStreamReader csvFileReader = new InputStreamReader(csvFileStream,encoding);
                CSVReader csvReader = new CSVReader(csvFileReader,csvConfig.getSeparationCharacter().getCharacter().charAt(0));
                ){
            
           
            String[] header = csvReader.readNext();
            int lineNumber = 1;
            
            signalHeader(header);
            
            String[] values = csvReader.readNext();
            lineNumber++;
            
            if (hasCommentLine) {
                signalCommentLine(lineNumber,values);
                values = csvReader.readNext();
                lineNumber++;
            }
            
            while(values!=null) {
                signalNewLine(lineNumber, values);
                values = csvReader.readNext();
                lineNumber++;
            }
            
            signalEOF();
        } catch (InvalidFileException | InvalidLineException | URISyntaxException | IOException e) {
            throw new CSVParsingError("Parsing of csv resource "+source.toString()+" was terminated.",e);
        }
        
    };

    private void signalHeader(String[] header) throws InvalidFileException {
        if(header==null) {
            throw new InvalidFileException("CSV file is empty (column headers not found), parsing is impossible.");
        }
        
        Map<String,Integer> columnNamesToIndexMapping = new HashMap<>(header.length);
        
        for (int i=0; i<header.length; i++) {
            if(columnNamesToIndexMapping.containsKey(header[i])) {
                final String message = "Column naming conflic for columns "+i +","+columnNamesToIndexMapping.get(header[i])+" : "+header[i];
                throw new InvalidFileException(message);
            }
            columnNamesToIndexMapping.put(header[i], i);
        }
        
        for (CSVParserListener interpreter : interpreters) {
            interpreter.columnNames(columnNamesToIndexMapping);
        }
    }
     
    private void signalCommentLine(int lineNumber, String[] values) throws InvalidFileException {
        if (values == null) {
            throw new InvalidFileException("CSV parser is expecting a comment line but no such line.");
        }
        for (CSVParserListener interpreter : interpreters) {
            interpreter.commentLine(new RawCsvLine(lineNumber, values));
        }
    }
    
    private void signalNewLine(int lineNumber, String[] values) throws InvalidLineException {
        for (CSVParserListener interpreter : interpreters) {
            interpreter.newLine(new RawCsvLine(lineNumber, values));
        }
    }   

    private void signalEOF() throws InvalidFileException {
        for (CSVParserListener interpreter : interpreters) {
            interpreter.eof();
        }
    }   
    
    private static final class LoggerListener implements CSVParserListener {
        
        @Override
        public void columnNames(Map<String, Integer> columnNamesToIndexMapping) throws InvalidFileException {
            // Creates a message looking like Column names to index mapping is :
            // {[name1 : 1],[name3 : 3],[name2 : 2],...]} and loggs it if debug 
            // enabled.
            if (CSVParser.LOGGER.isDebugEnabled()) {
                final Set<String> columnNames = columnNamesToIndexMapping.keySet();
                final StringBuilder messageBuilder = new StringBuilder("Column names to index mapping is : {");
                for (String name : columnNames) {
                    messageBuilder.append("[").append(name).append(" : ")
                            .append(columnNamesToIndexMapping.get(name)).append("]")
                            .append(",");
                }
                if (messageBuilder.charAt(messageBuilder.length() - 1) == ',') {
                    messageBuilder.setLength(messageBuilder.length() - 1);
                }
                messageBuilder.append("}");
                CSVParser.LOGGER.debug(messageBuilder.toString());
            }
        }
        
        @Override
        public void commentLine(RawCsvLine parsedLine) throws InvalidFileException {
            CSVParser.LOGGER.debug("Treating comment line - {} : {}.", parsedLine.getLineNumber(), Arrays.asList(parsedLine.getValues()));
        }

        @Override
        public void newLine(RawCsvLine parsedLine) throws InvalidLineException {
            CSVParser.LOGGER.debug("Treating data line - {} : {}.", parsedLine.getLineNumber(), Arrays.asList(parsedLine.getValues()));
        }

        @Override
        public void eof() throws InvalidFileException {
            CSVParser.LOGGER.debug("Reached the end of physical file");
        }
    }
}

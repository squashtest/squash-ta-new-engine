/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;

/**
 * This class isolates replacement elements from raw data. Sections between arobases <pre>@...@</pre> protected against substitution. 
 * @author edegenetais
 *
 */
public class ReplacementTokenizer {
	private static final LitteralExpression LINE_END_EXPRESSION = new LitteralExpression("\n");
	private static final Pattern LITTERAL_STRINGS=Pattern.compile("@(\\$\\{([^}]+)\\})@");
	private static final Pattern PLACE_HOLDERS=Pattern.compile("\\$\\{([^}]+)\\}");
	private static final Logger LOGGER=LoggerFactory.getLogger(ReplacementTokenizer.class);
	
	private List<ExpressionParser>expressionParsers=new ArrayList<ExpressionParser>();
	
	/**
	 * Convenience version for a single parser.
	 * @param singleParser the parser to use.
	 */
	public ReplacementTokenizer(ExpressionParser singleParser){
		expressionParsers.add(singleParser);
	}
	
	/**
	 * Full initialization constructor.
	 * @param expressionParsers parsers to compute expression values in the expression evaluation process.
	 */
	public ReplacementTokenizer(List<ExpressionParser> expressionParsers) {
		this.expressionParsers.addAll(expressionParsers);
	}
	
	/**
	 * Parse some data.
	 * @param data data to parse.
	 * @return the data as a list of expressions.
	 */
	public List<Expression>tokenize(SimpleLinesData data){
		List<Expression> list=new ArrayList<Expression>();
		LOGGER.debug("Begin parsing");
		for(String line:data.getLines()){
			LOGGER.debug("Parsing line: "+line);
			// A place holder surrounding with @, @${...}@, means to keep the place holder as is. It's a placeholder escapement.
			// So in the final file we will find the place holder unchanged without the @.
			Matcher litteralStringsMatcher=LITTERAL_STRINGS.matcher(line);
			int parseableBegin=0;
			while(litteralStringsMatcher.find()){
				// 
				int parseableEnd=litteralStringsMatcher.start();
				// The escaped placeholder
				String litteralStringDef=litteralStringsMatcher.group(1);
				// The section which still could contains place holder to replace
				String parseable=line.substring(parseableBegin,parseableEnd);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("parseable content: "+parseable+"\nlitteral: "+litteralStringDef);
				}
				// Process the section which could still contains some placeholder to replace 
				parse(list, parseable);
				// Set the new parseable begin position : The end of the escape placeholder  
				parseableBegin=litteralStringsMatcher.end();
				// Add the escape placeholder to the final data
				list.add(new LitteralExpression(litteralStringDef));
			}
			// If there is still element to parse after the analyze for escape place holder then we process it.
			// Else it's the end of the data to process.
			if(parseableBegin<line.length()){
				// We retrieve the last parseable section
				String lastParseable=line.substring(parseableBegin);
				LOGGER.debug("Last parseable part: "+lastParseable);
				// We process the retrievd section
				parse(list,lastParseable+"\n");
			}else{
				list.add(LINE_END_EXPRESSION);
			}
		}
		return list;
	}
	
	private void parse(List<Expression> list, String parseable) {
		Matcher parseableMatcher=PLACE_HOLDERS.matcher(parseable);
		int litteralBegin=0;
		LOGGER.debug("Begin parsing: "+parseable);
		//We search place holder in section given in the method argument.  
		while(parseableMatcher.find()){
			litteralBegin = tokenizeParseableMatch(list, parseable,
					parseableMatcher, litteralBegin);
		}
		if(litteralBegin<parseable.length()){
			String litteralString = parseable.substring(litteralBegin);
			list.add(new LitteralExpression(litteralString));
			LOGGER.debug("Last string litteral in parseable: "+litteralString);
		}
	}

	private int tokenizeParseableMatch(List<Expression> list, String parseable,
			Matcher parseableMatcher, int litteralBegin) {
		int litteralEnd=parseableMatcher.start();
		String expression=parseableMatcher.group(1);
		String fixedPart = parseable.substring(litteralBegin,litteralEnd);
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Found fixed part: "+fixedPart+"\nExpression: "+expression);
		}
		list.add(new LitteralExpression(fixedPart));
		litteralBegin=parseableMatcher.end();
		Iterator<ExpressionParser>parserIterator=expressionParsers.iterator();
		boolean searching=true;
		while(searching && parserIterator.hasNext()){
			ExpressionParser parser=parserIterator.next();
			if(parser.accept(expression)){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Parser "+parser.getClass().getName()+" parses "+expression);
				}
				list.add(parser.parse(expression));
				searching=false;
			}
		}
		if(searching){
			// If we don't found the place holder expression then we keep the expression as is in the final data 
			list.add(new LitteralExpression("${"+expression+"}"));
		}
		return litteralBegin;
	}
}

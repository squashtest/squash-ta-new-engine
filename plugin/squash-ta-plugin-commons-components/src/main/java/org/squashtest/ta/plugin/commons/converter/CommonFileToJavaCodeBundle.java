/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cjuillard
 */
public abstract class CommonFileToJavaCodeBundle {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonFileToJavaCodeBundle.class);
    
    public void addOptionDefinition(String optionDefinition, List<String> options) {
        /*
		 * options here follow the command line convention: a list of space separated elements, with quotes to protect
		 * space in values
         */
        LOGGER.debug("Treating options:\n{}", optionDefinition);
        String[] optionComponents = optionDefinition.split("[\"]");
        for (int i = 0; i < optionComponents.length; i++) {
            if (i % 2 == 0) {// even elements were not protected by double quotes => hack along the spaces
                String[] optionFlags = optionComponents[i].trim().split(" ");
                for (String flag : optionFlags) {
                    LOGGER.trace("Including option flag {}", flag);
                    options.add(flag);
                }
            } else {// odd elements were protected => integrate as is (putting double quotes back)
                LOGGER.trace("Including option flag {}", optionComponents[i]);
                options.add("\"" + optionComponents[i] + "\"");
            }
        }
    }
}

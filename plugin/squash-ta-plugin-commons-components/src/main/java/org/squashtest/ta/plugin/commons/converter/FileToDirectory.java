/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import java.util.Collection;

import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.commons.resources.DirectoryResource;

/**
 * Marks a file resource as a directory, if it is one.
 * @author edegenetais
 *
 */
@TAResourceConverter("filesystem")
public class FileToDirectory implements ResourceConverter<FileResource, DirectoryResource>{

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public DirectoryResource convert(FileResource resource) {
		File fileDescriptor=resource.getFile();
		if(fileDescriptor.isDirectory()){
			return new DirectoryResource(fileDescriptor);
		}else if(fileDescriptor.exists()){
			throw new BadDataException(fileDescriptor.getAbsolutePath()+" is not a directory.");
		}else{
			throw new BadDataException(fileDescriptor.getAbsolutePath()+" does not exist.");
		}
	}

	@Override
	public void cleanUp() {
		//noop: GC should do...
	}
	
}

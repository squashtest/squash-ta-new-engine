/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.utils.authentication;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AuthenticatorChain extends Authenticator {

	private static AuthenticatorChain instance=null;
	private List<EngineAuthenticator> authenticators = new ArrayList<EngineAuthenticator>();
	
	private AuthenticatorChain(){};
	
        
	//don't freak out, this method will be called very scarcely.
	public static synchronized AuthenticatorChain getInstance(){
		if (instance==null){
			instance=new AuthenticatorChain();
			Authenticator.setDefault(instance);
		}
		return instance;
	}
	
	public void clear(){
		authenticators.clear();
	}
	
	public void add(EngineAuthenticator authenticator){
		if (! authenticators.contains(authenticator)){
			authenticators.add(authenticator);
		}
	}
	
	public void remove(EngineAuthenticator authenticator){
		authenticators.remove(authenticator);
	}
	
	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		for (EngineAuthenticator authenticator : authenticators){
			if (authenticator.knowsCredentials()){
				return authenticator.getAuthentication();
			}
		}
		return null;
	}
	
	
	public URL getRequestorURL() {
		return super.getRequestingURL();
	}
	
	@Override
	public RequestorType getRequestorType() {
		return super.getRequestorType();
	}
	

	public String getRequestorHost(){
		return getRequestingHost();
	}
	
	public int getRequestorPort(){
		return getRequestingPort();
	}
	
	public String getRequestorProtocol(){
		return getRequestingProtocol();
	}
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.util.Collection;

import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;

/**
 * That converter simply duplicate the resource. Not the most useful converter I agree, but might be handy sometime.
 * 
 * @author bsiri
 */

@TAResourceConverter("copy")
public class FileToFileConverter implements ResourceConverter<FileResource, FileResource>{
	
	//A copy converter is always relevant, but is likely less relevant than others. So let's pick 0.1.
	public float rateRelevance(FileResource resource) {
		return 0.1f;
	}

	public void addConfiguration(Collection<Resource<?>> configuration) {
            new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	public FileResource convert(FileResource resource) {
		return resource.copy();
	}

	public void cleanUp() {
            //noop
	}


}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.core.tools.io.FileTree.EnumerationMode;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.library.param.PlaceHolderReplacer;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.core.tools.io.StorageSizeUnit;

/**
 * 
 * @author bfranchet
 */
@TAResourceConverter("param")
public class FileToParametrizedFile extends AbstractPropertiesConverter implements ResourceConverter<FileResource, FileResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileToParametrizedFile.class);
	
	private static final String ENCODING_KEY = "squashtest.ta.param.encoding";
        private static final String CHUNK_SIZE_KEY = "squashtest.ta.param.chunkSize";
     
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		Iterator<Resource<?>> it = configuration.iterator();
		while (it.hasNext()) {
			addResource(it.next());
		}
		
		String charset = properties.getProperty(ENCODING_KEY);
		if(charset!=null){
			setEncoding(charset);
			properties.remove(ENCODING_KEY);
		}
		String chunkSizeString = properties.getProperty(CHUNK_SIZE_KEY);
                if(chunkSizeString!=null){
                    this.chunkSize=StorageSizeUnit.getSizeInBytes(chunkSizeString);
                    properties.remove(CHUNK_SIZE_KEY);
                }
                
		checkValidity(configuration);
		
		addIncludeExcludeProperties();
	}

	@Override
	public FileResource convert(FileResource resource) {
		try {
			File resourceFile = resource.getFile();
			PlaceHolderReplacer replacer = new PlaceHolderReplacer(properties,getEncoding());
			File convertedFile = convertTaResource(resourceFile, replacer);
			return new FileResource(convertedFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("RelativeDate computation failed on I/O", e);
		}
	}
	
	private void addResource(Resource<?> resource) {
		if (resource instanceof PropertiesResource) {
			addPropertiesResource(resource);
		} else if (FileResource.class.isAssignableFrom(resource.getClass())) {
			addFileResource(resource);				
		} else {
			LOGGER.warn("There is at least one unrecognized configuration resource in the USING close. Please Check your data.");
		}		
	}

	private void addFileResource(Resource<?> resource) {
		try {
			Map<String, String> options = OptionsReader.BASIC_READER
					.getOptions(((FileResource) resource).getFile());
			properties.putAll(options);
		} catch (IOException e) {
			throw new IllegalConfigurationException("An error occured while reading configuration", e);
		}		
	}

	private void addPropertiesResource(Resource<?> resource) {
		Properties prop = ((PropertiesResource) resource).getProperties();
		properties.putAll(prop);		
	}
	
	private void checkValidity(Collection<Resource<?>> configuration) {
		if (!configuration.isEmpty() && properties.isEmpty()) {
			LOGGER.warn("Properties list is empty whereas some configuration files has been given through the USING close.");
		}
		
		//before adding the include/exclude properties, we check that we have at least one key/value properties
		if (properties.isEmpty()){
			throw new IllegalConfigurationException("This instruction must receive at least one mapping key-value in its configuration to be useful.");
		}	
	}
	
        //since this below is what's unit tested, we'll cross ourselves and try to do some streaming within...*sigh*
	private File convertTaResource(File resourceFile, PlaceHolderReplacer replacer) throws IOException {
		File convertedFile = FILE_TREE.createTempCopyDestination(resourceFile);
		createdResources.add(convertedFile);
		if (resourceFile.isDirectory()) {
			FILE_TREE.toTempDirectory(convertedFile);
			List<File> content = FILE_TREE.enumerate(resourceFile, EnumerationMode.FILES_ONLY);
			for (File contentFile : content) {
				File convertedContent = new File(convertedFile, FILE_TREE.getRelativePath(resourceFile,
						contentFile));
				convertedContent.getParentFile().mkdirs();
				if (!convertedContent.getParentFile().exists()) {
					throw new InstructionRuntimeException("Failed to create directory "
							+ convertedContent.getParent() + " during relative dates resolution.");
				}
				addFile(contentFile, convertedContent, replacer);

			}
		} else {//here is the mother of all evil, volume wise: we'll have to dive into it and chunk the file. Let's pray no single line is bigger than we can chew!
			addFile(resourceFile, convertedFile, replacer);
		}
		return convertedFile;
	}
}

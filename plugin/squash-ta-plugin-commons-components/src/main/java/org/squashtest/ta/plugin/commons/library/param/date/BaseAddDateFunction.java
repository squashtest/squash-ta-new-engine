/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date;

import java.util.Calendar;

import org.squashtest.ta.plugin.commons.library.param.IllegalExpressionException;

abstract class BaseAddDateFunction implements DateFunction {

	public BaseAddDateFunction() {
		super();
	}

	/**
	 * This method allows subclasses to specify the time unit they operate on.
	 * @return a valid field index for the {@link Calendar#add(int, int)} method.
	 */
	public abstract int getTimeUnit();

	@Override
	public Calendar evaluate(Calendar inputDate, String argument) {
		if (argument == null || argument.length() == 0) {
			throw new IllegalExpressionException(
					"Function addDay needs an argument.");
		}
		int nbDays = Integer.parseInt(argument);
		Calendar newDate = Calendar.getInstance();
		newDate.setTime(inputDate.getTime());
		newDate.add(getTimeUnit(), nbDays);
		return newDate;
	}

}
/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.repositories;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.annotations.TARepository;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.plugin.commons.utils.authentication.AuthenticatorChain;
import org.squashtest.ta.plugin.commons.utils.authentication.EngineAuthenticator;

@TARepository("url")
public class URLRepository implements ResourceRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(URLRepository.class);

	public static final String BASE_URL_KEY = "squashtest.ta.url.base";
	public static final String LOGIN_KEY = "squashtest.ta.url.login";
	public static final String PASSWORD_KEY = "squashtest.ta.url.password";
	public static final String USE_CACHE_KEY = "squashtest.ta.url.cache";
	
	private Properties effectiveConfiguration;

	private URL baseURL;
	private EngineAuthenticator authenticator;
	private boolean useCache = false;

	private Map<String, File> cache = new HashMap<String, File>();

	public URLRepository() {
	}

	public URLRepository(Properties properties, URL baseURL, boolean useCache, EngineAuthenticator authenticator) {
		super();
		this.baseURL = baseURL;
		this.useCache = useCache;
		this.authenticator = authenticator;
		this.effectiveConfiguration = properties;
	}

	public URL getBaseURL() {
		return baseURL;
	}

	public boolean isUseCache() {
		return useCache;
	}

	@Override
	public void init() {

		AuthenticatorChain.getInstance().add(authenticator);

	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	@Override
	public void cleanup() {

		AuthenticatorChain.getInstance().remove(authenticator);

		for (File file : cache.values()) {
			boolean deleted = file.delete();
			if (!deleted && file.exists()) {
				LOGGER.warn("Failed to delete file {} during cleanup.",file.getAbsolutePath());
			}
		}
		cache.clear();
	}

	@Override
	public Properties getConfiguration() {
		return effectiveConfiguration;
	}

	@Override
	public FileResource findResources(String name) {
		File file;

		if (useCache && cache.containsKey(name)) {
			file = cache.get(name);
		} else {
			file = download(name);
		}

		return (file != null) ? new FileResource(file) : null; // valid per
																// contract
	}

	private File download(String name) {
		File downloaded = null;

		try {
			URL url = new URL(baseURL.toString() + "/" + name);

			if (!exists(url)) {
				throw new FileNotFoundException("could not load resource '" + name + "' from repository '" + baseURL
						+ "' : no such resource ");
			}

			File local = FILE_TREE.createTempCopyDestination("ta_url_repo_" + name.replace("/", "_"));
			FileUtils.copyURLToFile(url, local);

			if (useCache) {
				cache.put(name, local);
			}

			downloaded = local;

		} catch (MalformedURLException e) {
			LOGGER.info("URLRepository : could not load resource '{}' from repository '{}' : malformed url ",  name, baseURL);
                        LOGGER.debug("Root cause", e);
		} catch (IOException e) {
			LOGGER.info("URLRepository : could not load resource '{}' from repository '{}' : malformed url ",  name, baseURL);
                        LOGGER.debug("Root cause", e);
		}
		return downloaded;
	}

	/**
	 * Check if an url DOES exists
	 * 
	 * @param url
	 *            : the given url
	 * @return true if the url point on something, false otherwise
	 */
	public boolean exists(URL url) {

		final String protocol = url.getProtocol();
		boolean exists;

		if ("http".equals(protocol)) {
			try {
				HttpURLConnection con = (HttpURLConnection) new URL(url.toString()).openConnection();
				exists = (con.getContentLength() >= 0);
			} catch (MalformedURLException e) {
                                LOGGER.debug("URLRepository : could not load resource located at '{}' from HTTP repository '{}' : malformed url ",  url, baseURL,e);
				exists = false;
			} catch (IOException e) {
                                LOGGER.debug("URLRepository : could not load resource located at '{}' from HTTP repository '{}' : malformed url ",  url, baseURL,e);
				exists = false;
			}

		} else if ("file".equals(protocol)) {
			exists = FileUtils.toFile(url).exists();
		} else {
			/*
			 * "ftp" protocol use the method download() from the FTPRepository class. (inexistants files are catch
			 * there)
			 * 
			 * It would maybe be relevant in the future to develop them.
			 */
			LOGGER.info("There is no URL existence control for the protocol \"" + protocol + "\" in Squash TA");
			exists = true;
		}
		return exists;
	}
}

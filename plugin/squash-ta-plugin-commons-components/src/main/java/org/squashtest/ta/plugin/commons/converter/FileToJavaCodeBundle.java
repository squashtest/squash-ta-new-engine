/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.tools.JavaFileObject.Kind;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.library.java.BundleFilePlacementAlgorithm;
import org.squashtest.ta.plugin.commons.library.java.CompilationReport;
import org.squashtest.ta.plugin.commons.library.java.CompilerConnector;
import org.squashtest.ta.plugin.commons.library.java.SourceEnumerator;
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle;

@TAResourceConverter("compile")
public class FileToJavaCodeBundle extends CommonFileToJavaCodeBundle implements ResourceConverter<FileResource, JavaCodeBundle> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileToJavaCodeBundle.class);
	private static final BundleFilePlacementAlgorithm BUNDLE_FILE_PLACEMENT_ALGORITHM = new BundleFilePlacementAlgorithm();
	private static CompilerConnector compilerConnector = new CompilerConnector();
	
	private List<String> options = new ArrayList<String>();

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {

		for (Resource<?> element : configuration) {
			if (element instanceof FileResource) {
				try {
					SimpleLinesData elementContent = new SimpleLinesData(((FileResource) element).getFile()
							.getAbsolutePath());
					for (String optionDefinition : elementContent.getLines()) {
						addOptionDefinition(optionDefinition, options);
					}
				} catch (IOException e) {
					throw new InstructionRuntimeException("Failed to load configuration file", e);
				}
			} else {
				LOGGER.warn("Only resources of type file are supported in configuration. Ignored.");
			}
		}
	}

	@Override
	public JavaCodeBundle convert(FileResource sourceTree) {
		SourceEnumerator enumerator = new SourceEnumerator(sourceTree.getFile());
		try {
			File bundleBase = FILE_TREE.createTempDirectory("bundle", ".basedir");
			CompilationReport compileReport = compilerConnector.compile(enumerator.getJavaSourceFiles(), options,
					bundleBase);
			if (compileReport.isSuccess()) {
				for (File resourceFile : enumerator.getResourceFiles()) {
					String resourceName = enumerator.getResourceRelativePath(resourceFile).replaceAll("\\\\", "/");
					File resourceDestination = BUNDLE_FILE_PLACEMENT_ALGORITHM.getElementFile(bundleBase, resourceName,
							Kind.OTHER);
					File resourceParent = resourceDestination.getParentFile();
					if (resourceParent != null && !resourceParent.exists()) {
						boolean created = resourceParent.mkdirs();
						if (!created) {
							throw new InstructionRuntimeException(
									"Failed to create resource destination in java code bundle: "
											+ resourceParent.getAbsolutePath());
						}
					}
					BinaryData resourceData = new BinaryData(resourceFile);
					resourceData.write(resourceDestination);
				}
				return new JavaCodeBundle(bundleBase, compileReport.getCompiledClassNames());
			} else {
				if (enumerator.isBundleEmpty() || enumerator.isWrongExtensionNameFile() || enumerator.isJavaIsNotPresent()){
					LOGGER.error("It must have java sources in a java subdirectory in resources directory");
					throw new BadDataException("Java code compilation failed:\n" +compileReport.getCompilerMessages());
				}				
				else {
					throw new BadDataException("Java code compilation failed:\n" + compileReport.getCompilerMessages());
				}
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("JavaCodeBundle compile failed during I/O operation.", e);
		}
	}

	@Override
	public void cleanUp() {
		// noop
	}

}

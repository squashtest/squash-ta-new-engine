/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TATarget;
import org.squashtest.ta.framework.components.Target;

/**
 * Checks that a given file wears a given shebang. The shebang is defined as an
 * initial <code>{@literal #!<id>}</code> line, where <code>{@literal <id>}</code> is a free string
 * identifier.
 * 
 * @author edegenetais
 * 
 */
public class ShebangCheck {
	
	private static final String SHEBANG_MARK = "#!";

	private static final Logger LOGGER=LoggerFactory.getLogger(ShebangCheck.class);
		
	/** Content of the target shebang (without the #! prefix). */
	private String shebang;
	
	/**
	 * @param shebang the shebang to check.
	 */
	public ShebangCheck(String shebang){
		if(shebang==null){
			throw new IllegalArgumentException("shebang id cannot be null.");
		}
		this.shebang=shebang;
	}
	
    public ShebangCheck(Class<? extends Target> clazz) {
        if(clazz==null) {
            throw new IllegalArgumentException("shebang id cannot be constructed from a null class.");
        }
        try {
            this.shebang = clazz.getAnnotation(TATarget.class).value();
        } catch (Exception e) {
            throw new IllegalArgumentException("Class "+clazz+" misses annotation "+TATarget.class.getName()+".For compatiblitly with target prior to TA-1.5 please uses the string version of the ShebangChek",e);
        }

    }
	/**
	 * Reads the file to look the shebang up.
	 * @param fileLocation url of the target file.
	 * @return <code>true</code> if the file wears your shebang, <code>false</code> if not.
	 * @throws IOException if the configuration URL cannot be opened.
	 */
	public boolean hasShebang(URL fileLocation) throws IOException{
		return testShebang(fileLocation, true);
	}
	
	/**
	 * {@literal  Reads the file to look if there is a shebang (any of them)
	 * (useful for target.creator.ftp and target.creator.database, as their shebangs are optional, to be retrocompatible with <1.5 versions).}
	 * @param fileLocation url of the target file.
	 * @return <code>true</code> if the file wears a shebang (any shebang, not only the one searched), 
         * <code>false</code> if not.
	 * @throws IOException if the configuration URL cannot be opened.
	 */
	public boolean hasAnyShebang(URL fileLocation, String tagetCreatorName) throws IOException{
		boolean hasShebang = testShebang(fileLocation, false);
		if (!hasShebang){					
			LOGGER.warn( tagetCreatorName+": The target "+fileLocation+" doesn't have any shebang. " +
					"Please add one, otherwise Squash-TA may fail to instantiate this target.");
		}
		return hasShebang;
	}
	
	//method to read shebang
	/**
	 * Check the shebang of a given file
	 * 
	 * @param fileLocation : the file to test
	 * 
	 * @param testForTheGoodShebang : true if we want the shebang to be equals to this shebang, false if any shebang would do
	 * 
	 * @return either true for any shebang or for the good one, false otherwise
	 * 
	 * @throws IOException
	 */
	private boolean testShebang(URL fileLocation, boolean testForTheGoodShebang) throws IOException{
		boolean hasShebang = false;
		BufferedReader br = new BufferedReader(new InputStreamReader(
				fileLocation.openStream()));
		try {
			String line=br.readLine();
			if(line!=null && line.startsWith(SHEBANG_MARK)){
				//if we arrived from the hasAnyShebang() method, any authorised Shebang is correct
				//else, we check if it really is the one demanded
				hasShebang = testForTheGoodShebang ? shebang.equals(line.substring(SHEBANG_MARK.length())) : true;
			}
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				LOGGER.warn(
						"Error while closing reading stream after shebang check",
						e);
			}
		}
		return hasShebang;
	}

    /**
     * Gives the tested Shebang of this instance of ShebangChecker.
     * @return the targeted shebang (wthout the #!).
     */
    public String getShebang() {
        return shebang;
    }
    
    
}

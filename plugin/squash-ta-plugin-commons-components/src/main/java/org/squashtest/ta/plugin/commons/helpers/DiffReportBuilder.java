/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.squashtest.ta.core.tools.ReportBuilderUtils;

/**
 * This object process dbunit diff events to build a diff report.
 * 
 * @author edegenetais
 * 
 */
public class DiffReportBuilder {
	private static final String ROW_ID_KEYNAME = "RowId";

	private static final String PK_TABLE_DIFF_HEADER = "* Differences for table \"#tableName#\":";
	private static final String NO_PK_TABLE_DIFF_HEADER = "* Table \"#tableName#\":";
	
	private static final String TABLE_DIFF_SEPARATOR = "\n\n";

	/**
	 * Warning about diff inaccuracy when no PKs can be used to check line
	 * pairing. To make a long story short: without PKs to check line matching,
	 * the differences that do exist may wreck line pairing, thus adding noise
	 * to the actual difference (a known problem in code comparison algorithms
	 * as well).
	 */
	private static final String NO_PK_DIFF_ACCURACY_WARNING = "Warning: As table \"#tableName#\" has no (pseudo) primary key, the rows from the two datasets might not be matched properly.";	
	private static final String PK_DIFF_ACCURACY_WARNING = "Note : the (pseudo) primary key used for table \"#tableName#\" is :";
	
	private static final String PROP_TABLE_NAME = "#tableName#";
	
	private boolean missingPK;
	
	private Map<String,TableDiffReport>tableReports=new HashMap<String, TableDiffReport>();
	
	/**
	 * Add the diff data for one column in a table row.
	 * @param pkSet mapping {@literal pkName-->pkValue}, if available. 
         * Pass <code>null</code> if PK definitions and/or values are not available.
	 * @param rowId allows difference data mapping if PK data are not available.
	 * @param tableName name of the table in which this difference has been detected.
	 * @param columnName name of the column in which the difference has been detected.
	 * @param expectedValue expected value for the column at the given row.
	 * @param actualValue actual value for the column at the given row.
	 */
	public void addDiffElement(Map<String,String> pkSet, Integer rowId,String tableName, String columnName, Object expectedValue, Object actualValue){
		Map<String,String>effectiveKeys;
		if(pkSet==null || pkSet.isEmpty()){
			missingPK=true;
			effectiveKeys=new HashMap<String, String>(1);
			effectiveKeys.put(ROW_ID_KEYNAME, rowId==null?"":rowId.toString());
		}else{
			effectiveKeys=pkSet;
		}
		TableDiffReport tableReport=tableReports.get(tableName);
		if(tableReport==null){
			tableReport=new TableDiffReport(effectiveKeys.keySet());
			tableReports.put(tableName, tableReport);
		}
		ColumnDiffReport columnReport=new ColumnDiffReport(columnName, expectedValue, actualValue);
		tableReport.addColumnDiffReport(effectiveKeys, columnReport);
	}
	
	/**
	 * String representation of the difference data for visual inspection.
	 */
	public String toString(){
		StringBuilder builder=new StringBuilder();
		for(Entry<String, TableDiffReport> entry:tableReports.entrySet()){
			String tableName=entry.getKey();
			if(missingPK){
				String warning = NO_PK_TABLE_DIFF_HEADER.replace(PROP_TABLE_NAME, tableName);
				builder.append(warning).append("\n");
				warning = NO_PK_DIFF_ACCURACY_WARNING.replace(PROP_TABLE_NAME, tableName);
				builder.append(warning).append("\n");
			}else{
				String warning = PK_TABLE_DIFF_HEADER.replace(PROP_TABLE_NAME, tableName);
				builder.append(warning).append("\n");
				warning = PK_DIFF_ACCURACY_WARNING.replace(PROP_TABLE_NAME, tableName);
				builder.append(warning);
				builder.append(entry.getValue().pkNames);
				builder.append("\n");
			}
				TableDiffReport tableReport=entry.getValue();
				tableReport.toString(builder);
				builder.append(TABLE_DIFF_SEPARATOR);
		}
		return builder.toString();
	}
	
	/**
	 * Class to manage the diff data for a given table.
	 * @author edegenetais
	 *
	 */
	class TableDiffReport{
		private ReportBuilderUtils reportBuilderUtils=new ReportBuilderUtils();
		private SortedSet<String>pkNames;
		/** Diff data, mapped by "primary key" values */
		private Map<PkKey,List<ColumnDiffReport>> tableDiffData=new HashMap<PkKey, List<ColumnDiffReport>>();
		
		/**
		 * Constructor from unordered set of pk names.
		 * @param keyNames
		 */
		public TableDiffReport(Set<String>keyNames){
			this.pkNames=new TreeSet<String>(keyNames);
		}
		
		/**
		 * Constructor from {@link SortedSet} of key names, to keep the
		 * specified order.
		 * 
		 * @param keyNames key names, in desired order.
		 */
		public TableDiffReport(SortedSet<String>keyNames){
			this.pkNames=new TreeSet<String>(keyNames);
		}
		
		/** Immutable key value list for use as map key. */
		class PkKey{
			/** Values of the keys, in order. */
			private List<String> pkValues=new ArrayList<String>();
			
			/**
			 * Create a pkKey value from key map.
			 * @param pkValues pk values.
			 */
			public PkKey(Map<String,String> pkValues) {
				SortedSet<String> keyNames=new TreeSet<String>(pkValues.keySet());
				for(String name:keyNames){
					this.pkValues.add(pkValues.get(name));
				}
			}
			
			/**
			 * Create a pkKey value from key map, imposing key ordering.
			 * @param pkValues pk values.
			 * @param keyOrder sorted set to impose key ordering. 
			 */
			public PkKey(Map<String,String> pkValues, SortedSet<String>keyOrder) {
				SortedSet<String> keyNames=new TreeSet<String>(pkValues.keySet());
				for(String name:keyNames){
					this.pkValues.add(pkValues.get(name));
				}
			}
			
			
			/**
			 * Redefine equals to give the key immutable value semantics for use as a key grouping all known pks in order.
			 */
			public boolean equals(Object o){
				if(o==null){
					return false;
				}
				if(this==o){
					return true;
				}
				if(o instanceof PkKey){
					/*
					 * if the other object is a key too, they are equal if and
					 * only if their value lists are equal.
					 */
					return pkValues.equals(((PkKey)o).pkValues);
				}else{
					return false;
				}
			}

			/**
			 * Redefine hashcode for consistancy with the redefined equals.
			 */
			public int hashCode(){
				int hashCode=0;
				for(String key:pkValues){
					hashCode*=13;
					hashCode+=key.hashCode();
				}
				return hashCode;
			}

			public String toString(){
				StringBuilder representation=new StringBuilder();
				toString(representation);
				return representation.toString();
			}

			/**
			 * Append version to optimize report aggregation.
			 * @param representation
			 */
			public void toString(StringBuilder representation) {
				reportBuilderUtils.appendCollectionContentString(representation, pkValues);
			}
		}
	
		public void addColumnDiffReport(Map<String,String>pkValues,ColumnDiffReport dr){
			PkKey pkKey=new PkKey(pkValues);
			List<ColumnDiffReport>rowReport=tableDiffData.get(pkKey);
			if(rowReport==null){
				rowReport=new ArrayList<DiffReportBuilder.ColumnDiffReport>();
				tableDiffData.put(pkKey, rowReport);
			}
			rowReport.add(dr);
		}
		
		public String toString(){
			StringBuilder report=new StringBuilder();
			toString(report);
			return report.toString();
		}

		/**
		 * Append method to optimize report aggregation.
		 * @param report
		 */
		public void toString(StringBuilder report) {
			Set<Entry<PkKey,List<ColumnDiffReport>>> entrySet=tableDiffData.entrySet();
			if (!missingPK){
				reportBuilderUtils.appendCollectionContentString(report, pkNames);			
				report.append(";differences\n");
			}
			for(Entry<PkKey,List<ColumnDiffReport>> entry:entrySet){
				if (!missingPK){
					PkKey key=entry.getKey();
					key.toString(report);
					report.append(";");
				}
				
				for(ColumnDiffReport dr:entry.getValue()){
					dr.toString(report);
				}
				report.append("\n");
			}
		}
		
	}
	
	/**
	 * Diff report for one row and one column in a given table.
	 * @author edegenetais
	 *
	 */
	class ColumnDiffReport{
		/** Name of the mismatching column */
		private String columnName;
		/** Expected and actual value for the column in the line. */
		private Object expected,actual;
		/**
		 * Complete initialization constructor.
		 * @param columnName
		 * @param expected
		 * @param actual
		 */
		public ColumnDiffReport(String columnName, Object expected,
				Object actual) {
			super();
			this.columnName = columnName;
			this.expected = expected;
			this.actual = actual;
		}
		
		@Override
		public String toString() {
			StringBuilder stringRepBuilder=new StringBuilder();
			toString(stringRepBuilder);
			return stringRepBuilder.toString();
		}

		/**
		 * Append method to optimize report aggregation.
		 * @param stringRepBuilder
		 */
		public void toString(StringBuilder stringRepBuilder) {
			stringRepBuilder.append("{").append(columnName).append(":");
			stringRepBuilder.append("expected='").append(expected).append("'|");
			stringRepBuilder.append("actual='").append(actual).append("'}");
		}
	}
}

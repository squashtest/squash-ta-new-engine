/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.targets;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.TATargetCreator;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.plugin.commons.library.ShebangCheck;

/**
 * Creator for the {@link WebTarget} targets. Here is waht's expected and what
 * is managed (everything else ignored) in a {@link WebTarget} configuration
 * file:
 * <ul>
 * <li>shebang: it must be headed by a <code>#!http</code> comment.</li>
 * <li>squashtest.ta.http.endpoint.url: this property is the mandatory url string defining the
 * end-point</li>
 * <li>squashtest.ta.http.proxy.host : defines a the host (name or IP) of a proxy, if there is one.</li>
 * <li>squashtest.ta.http.proxy.port : defines the port of the proxy.</li>
 * <li>any additional key will be passed on the the target user command, please
 * refer to the using command documentation to know what to put in there.</li>
 * </ul>
 * 
 */
@TATargetCreator("target.creator.http")
public class WebTargetCreator implements TargetCreator<WebTarget> {
	private static final Logger LOGGER=LoggerFactory.getLogger(WebTargetCreator.class);
	/** This object checks that the target definition content is marked as an http (web end-point) target. */
	private ShebangCheck shebangCheck=new ShebangCheck("http");
	/** This object allows overriding the target properties with system properties (@see {@link System#getProperties()}) */
	private PropertiesBasedCreatorHelper propertiesHelper=new PropertiesBasedCreatorHelper();
	
	public WebTargetCreator(){
		propertiesHelper.setKeysRegExp(WebTarget.NAME_SPACE.replaceAll("\\.", "\\.")+"\\..*");
	}
	
	@Override
	public boolean canInstantiate(URL propertiesFile) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Testing eligibility of "+propertiesFile+" as Web configuration URL.");
		}
		boolean valid = false;
		try {
			if ("file".equals(propertiesFile.getProtocol()) && shebangCheck.hasShebang(propertiesFile)) {
				valid = isWebConfigurationType(propertiesFile);
			}
			
		}catch(IllegalArgumentException iae){
			LOGGER.warn("Malformed http target file: "+propertiesFile.toExternalForm(), iae);
		} catch (IOException ioe) {
			throw new BrokenTestException("Cannot access transmitted target definition URL",ioe);
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(propertiesFile+" is "+(valid?" ":"not ")+"eligible.");
		}
		return valid;
	}

	@Override
	public WebTarget createTarget(URL propertiesFile) {
		Properties configuration;
		try {
			File file=new File(propertiesFile.toURI());
			configuration=propertiesHelper.getEffectiveProperties(file);
			return new WebTarget(configuration);
		} catch (URISyntaxException e) {
			throw new BrokenTestException("WebTarget configuration loading failed for "+propertiesFile.toExternalForm(),e);
		} catch (IOException e) {
			throw new BrokenTestException("WebTarget configuration loading failed for "+propertiesFile.toExternalForm(),e);
		}
	}
	
	private boolean isWebConfigurationType(URL propertiesFile) throws IOException{
		boolean valid = false;
		Properties configuration=new Properties();
		configuration.load(propertiesFile.openStream());
		if(configuration.getProperty(WebTarget.ENDPOINT_URL_KEY)!=null){
			valid=true;
		}else{
			LOGGER.error(propertiesFile.toExternalForm()+" has the http shebang, but no "+WebTarget.ENDPOINT_URL_KEY+" property");
		}
		return valid;
	}

}

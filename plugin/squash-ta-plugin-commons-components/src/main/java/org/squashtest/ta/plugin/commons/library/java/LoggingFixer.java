/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java;

import java.lang.reflect.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.squashtest.ta.plugin.commons.library.java.LoggingFixerProtocol.*;

/**
 * This class works around the maven classpath man agement that hides the SLF4J binding.
 * @author edegenetais
 */
public class LoggingFixer {
    
    private Logger LOGGER=getLoggerImpl(getClass().getName());

    public LoggingFixer() {
        this.LOGGER = getLogger(LoggingFixer.class);
    }
    
    public Logger getLogger(Class<?> classObject){
        try {
            getClass().getClassLoader().loadClass(SLF4J_BINDER_CLASS_NAME);
            return LoggerFactory.getLogger(classObject);
        } catch (ClassNotFoundException ex) {//NOSONAR : here we are precisely working around the fact that this class is missing, and log an explicit warning about it. No need to bloat the logs with an exception stack
            LOGGER.warn("No SLF4J binding, falling back to sysout logger for class "+classObject.getName());
            final String loggerName=classObject.getName();
            return getLoggerImpl(loggerName);
        }
    }
    
    public Logger getLogger(String name){
        try {
            getClass().getClassLoader().loadClass(SLF4J_BINDER_CLASS_NAME);
            return LoggerFactory.getLogger(name);
        } catch (ClassNotFoundException ex) {//NOSONAR : here we are precisely working around the fact that this class is missing, and log an explicit warning about it. No need to bloat the logs with an exception stack
            LOGGER.warn("No SLF4J binding, falling back to sysout logger for class "+name);
            return getLoggerImpl(name);
        }
    }
    
    private Logger getLoggerImpl(final String loggerName) {
        return (Logger)Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Logger.class}, new LoggerFixerInvocationHandler(loggerName));
    }
}

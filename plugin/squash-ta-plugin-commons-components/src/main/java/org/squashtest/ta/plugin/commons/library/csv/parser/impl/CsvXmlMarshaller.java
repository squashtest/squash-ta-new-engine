/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.parser.impl;

import java.io.OutputStream;
import java.nio.charset.Charset;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import org.squashtest.ta.plugin.commons.library.csv.model.CsvXmlFile;

/**
 *
 * @author fgautier
 */
public class CsvXmlMarshaller {
    
    private static final String CSV_XML_NAMESPACE = "http://csvxml.ta.squashtest.org";
    
    private final String encoding;

    public CsvXmlMarshaller(String encoding) {
        this.encoding = encoding;
    }

    public CsvXmlMarshaller() {
        this.encoding = Charset.defaultCharset().name();
    }

    
    public void marshal(CsvXmlFile data, OutputStream destination) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(CsvXmlFile.class);
        Marshaller marshallerImpl = context.createMarshaller();
        marshallerImpl.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshallerImpl.setProperty(Marshaller.JAXB_ENCODING, encoding);
        String rootName = CsvXmlFile.class.getAnnotation(XmlRootElement.class).name();
        QName qtag = new QName(CSV_XML_NAMESPACE, rootName);
        JAXBElement<CsvXmlFile> document = new JAXBElement<>(qtag, CsvXmlFile.class, data);
        marshallerImpl.marshal(document, destination);
    }

}

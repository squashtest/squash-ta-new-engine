/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.library.csv.exceptions.CSVParsingError;
import org.squashtest.ta.plugin.commons.library.csv.parser.CSVParser;
import org.squashtest.ta.plugin.commons.library.csv.parser.impl.CSVXmlBuilder;
import org.squashtest.ta.plugin.commons.resources.CSVConfiguration;
import org.squashtest.ta.plugin.commons.resources.CSVResource;
import org.squashtest.ta.plugin.commons.resources.XMLResource;

/**
 * Base implementation of a CSV to XML converter. It provides basic
 * configuration mechanism as well as the base transfomration method.
 *
 * @param <OUTPUT> A CSV to XML converter might either produce a
 * {@link FileResource} or a {@link  XMLResource} or anything else base on a file
 * actually.
 *
 * @author fgautier
 */
public abstract class AbstractCsvToXmlConverter<OUTPUT extends Resource<OUTPUT>> implements ResourceConverter<CSVResource, OUTPUT> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCsvToXmlConverter.class);

    private static final String INPUT_ENCODING_KEY = "squashtest.ta.csvxml.input.encoding";
    private static final String OUTPUT_ENCODING_KEY = "squashtest.ta.csvxml.output.encoding";
    private static final String SKIP_EMPTY_LINES_KEY = "squashtest.ta.csvxml.skip.emptylines";
    private static final String HAS_COMMENT_LINE_KEY = "squashtest.ta.csvxml.has.comment";

    private String inputEncoding;
    private String outputEncoding;
    private Boolean skipEmptyLines;
    private Boolean hasComment;

    private File supportFile;

    /**
     * Standard constructor. Creates a temp support file.
     *
     * @throws InstructionRuntimeException if anything wen't wrong while
     * creating temp support file.
     */
    public AbstractCsvToXmlConverter() {
        try {
            supportFile = File.createTempFile("csv_xml", ".xml", TempDir.getExecutionTempDir());
        } catch (IOException e) {
            throw new InstructionRuntimeException("Unable to create csv xml support file.", e);
        }
    }

    /**
     * Base convert implementation. Converts CSV input as XML and serializes it
     * on a support file.
     *
     * @param input The CSV to be converted.
     * @return the support file containing the serialized CSV XML data. Warning
     * this support file life cycle is tied to the one of the converter. It will
     * be destroyed when cleanUp will be called. Copy it if you wan't it to
     * survive the convert process. Also if you open a stream on it make sure
     * you close it otherwise you'll be causing resource leakage.
     * @throws CSVParsingError
     */
    protected File applyTransformation(CSVResource input) throws CSVParsingError {

        CSVConfiguration configuration = input.getParsingConfiguration();
        setInternalParsingConfiguration();

        CSVXmlBuilder listener = new CSVXmlBuilder();
        CSVParser parser = CSVParser.newInstance(configuration, hasComment, skipEmptyLines).registerListener(listener);

        try (FileOutputStream fos = new FileOutputStream(supportFile)) {
            parser.parse(input.getCSVFile().toURI().toURL(), inputEncoding);
            listener.serializeContent(fos, outputEncoding);
        } catch (MalformedURLException e) {
            throw new InstructionRuntimeException("I/O problem while reading CSV input resoruce " + input + ".", e);
        } catch (IOException e) {
            throw new InstructionRuntimeException("I/O problem while serializing csv xml output support file " + supportFile + ".", e);
        }

        return supportFile;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        for (Resource<?> configurationResource : configuration) {
            if (configurationResource instanceof PropertiesResource) {
                treatPropertiesResource((PropertiesResource) configurationResource);
            }
        }
    }

    @Override
    public void cleanUp() {
        if (supportFile != null) {
            if (supportFile.delete()) {
                LOGGER.debug("CSV XML converter support file {} succesfully deleted.", supportFile);
            } else {
                LOGGER.warn("Deletion of csv xml converter support file {} failed. This may lead to resource leakage.", supportFile);
            }
        } else {
            LOGGER.debug("Nothing to clean.");
        }
    }

    @Override
    public float rateRelevance(CSVResource input) {
        return 0.5f;
        /* Why not ?*/
    }

    private void treatPropertiesResource(PropertiesResource resource) {
        final Properties prop = resource.getProperties();

        final String propInputEncoding = prop.getProperty(INPUT_ENCODING_KEY);
        final String propOutputEncoding = prop.getProperty(OUTPUT_ENCODING_KEY);
        final String propHasComment = prop.getProperty(HAS_COMMENT_LINE_KEY);
        final String propSkipEmptyLines = prop.getProperty(SKIP_EMPTY_LINES_KEY);

        if (propInputEncoding != null) {
            if (inputEncoding == null) {
                LOGGER.debug("Setting input encoding as {}.", propInputEncoding);
                inputEncoding = propInputEncoding;
            } else {
                LOGGER.debug("Input encoding is already set with value {}, discarding encoding {}.", inputEncoding, propInputEncoding);
            }
        } else {
            LOGGER.debug("Configuration properties does not contain input encoding key {}.", INPUT_ENCODING_KEY);
        }

        if (propOutputEncoding != null) {
            if (outputEncoding == null) {
                LOGGER.debug("Setting output encoding as {}.", outputEncoding);
                outputEncoding = propOutputEncoding;
            } else {
                LOGGER.debug("Output encoding is already set with value {}, discarding encoding {}.", outputEncoding, propOutputEncoding);
            }
        } else {
            LOGGER.debug("Configuration properties does not contain outpur encoding key {}.", OUTPUT_ENCODING_KEY);
        }

        if (propHasComment != null) {
            if (hasComment == null) {
                final Boolean parsedPropHasComment = Boolean.parseBoolean(propHasComment);
                LOGGER.debug("Setting output has comment strategy as {}.", parsedPropHasComment);
                hasComment = parsedPropHasComment;
            } else {
                LOGGER.debug("\"Has comment strategy\" is already set {}, discarding strategy {}.", hasComment, propHasComment);
            }
        } else {
            LOGGER.debug("Configuration properties does not contain \"has comment line\" key {}.", HAS_COMMENT_LINE_KEY);
        }

        if (propSkipEmptyLines != null) {
            if (skipEmptyLines == null) {
                final Boolean parsedPropSkipEmptyLines = Boolean.parseBoolean(propSkipEmptyLines);
                LOGGER.debug("Setting output \"skip empty lines\" strategy as {}.", parsedPropSkipEmptyLines);
                skipEmptyLines = parsedPropSkipEmptyLines;
            } else {
                LOGGER.debug("\"Skip empty lines\" is already set {}, discarding strategy {}.", skipEmptyLines, propSkipEmptyLines);
            }
        } else {
            LOGGER.debug("Configuration properties does not contain \"Skip empty lines\" key {}.", SKIP_EMPTY_LINES_KEY);
        }
    }

    private void setInternalParsingConfiguration() {
        if (hasComment == null) {
            LOGGER.debug("\"Has comment strategy\" has not been specified. Assuming that there is no comment line.");
            hasComment = false;
        }
        if (skipEmptyLines == null) {
            LOGGER.debug("\"Skip empty lines strategy\" has not been specified. Assuming that one wants to skip trailing empty lines.");
            skipEmptyLines = true;
        }
        if (inputEncoding == null) {
            inputEncoding = Charset.defaultCharset().displayName();
            LOGGER.debug("Using default plateform encoding {} for input reading.", inputEncoding);
        }
        if (outputEncoding == null) {
            outputEncoding = "UTF-8";
            LOGGER.debug("No output encoding specified. Using UTF-8 as default value.");
        }
    }

}

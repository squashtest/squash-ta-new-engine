/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * CSV resource implementation.
 * @author fgaillard
 *
 */
@TAResource("csv")
public class CSVResource implements Resource<CSVResource> {
        private CSVConfiguration cfg;
	private File csvFile;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public CSVResource(){}
	
        /**
         * Constructor for a CSV with no attached configuration. For compatibility with
         * previous components.
         * @deprecated 
         * 
         * @param file 
         */
        @Deprecated
	public CSVResource(File file){
		csvFile = file;
	}

        /**
         * Full initialization constructor, with added configuration. This is the way to go: users of a CSV resource
         * should known about its configuration in order to use it.
         * @param cfg csv parsing configuration this CSV resource conforms to.
         * @param csvFile CSV data file.
         */
        public CSVResource(File csvFile,CSVConfiguration cfg) {
            this.cfg = cfg;
            this.csvFile = csvFile;
        }
	
        
        
	@Override
	public CSVResource copy() {
		try {
			File newFile = FILE_TREE.createTempCopyDestination(csvFile);
			FileUtils.copyFile(csvFile, newFile);
			return new CSVResource(newFile,cfg);
		} catch (IOException e) {
			throw new InstructionRuntimeException("CSV (resource) : could not copy file path '"+csvFile.getPath()+"', an error occured :", e);
		}
	}

	@Override
	public void cleanUp() {
		//noop
	}

	public File getCSVFile() {
		return csvFile;
	}
        
        public CSVConfiguration getParsingConfiguration(){
            return cfg;
        }
}

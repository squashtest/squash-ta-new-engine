/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.csv.model;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Base class of the XML transpose of a CSV Line. A line refers to a physical source line and is a 
 * container of fields.
 * 
 * @author fgautier inpired by work done by edegenetais
 * @author edegenetais
 */
public class CsvXmlLine {
    
    /**
     * The physical CVS source line number.
     */
    @XmlAttribute
    private Integer sourceLine;
    
    /**
     * The {@link CsvXmlField}s this line contains;
     */
    @XmlElement(name = "field")
    private List<CsvXmlField> fields;

    /**
     * Default constructor for Jaxb enumeration.
     */
    public CsvXmlLine() { /* Jaxb */ }

    /**
     * Fully parametrized constuctor. 
     * 
     * @param sourceLine  : The physical CVS source line number.
     * @param fields  : The {@link CsvXmlField}s this line contains;
     */
    public CsvXmlLine(Integer sourceLine, List<CsvXmlField> fields) {
        this.sourceLine = sourceLine;
        this.fields = fields;
    }

    /**
     * Simple getter for Jaxb.
     * 
     * @return the physical CSV source line number. 
     */
    public Integer getSourceLine() {
        return sourceLine;
    }

    /**
     * Simple getter for Jaxb.
     * 
     * @return an unmodifiable collection of the fields contained by the line. 
     */
    public List<CsvXmlField> getFields() {
        return Collections.unmodifiableList(fields);
    }
    
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter

import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.exception.BadDataException
import org.squashtest.ta.plugin.commons.resources.DirectoryResource
import org.squashtest.ta.framework.tools.TempDir;


import spock.lang.Specification

class FileToDirectoryTest extends Specification {
	def testee
	
	def setup(){
		testee=new FileToDirectory()
	}
	
	def "should convert an existing directory successfully"(){
		given:
			def directoryFileResource=Mock(FileResource)
			directoryFileResource.getFile()>>new File(".")
		when:
			def result=testee.convert(directoryFileResource)
		then:
			result instanceof DirectoryResource
			result.getDirectory().equals(new File("."))
	}
	
	def "should throw when attempting to convert a regular file"(){
		given:
			File existingRegularFile=File.createTempFile("regular", "notDirectory", TempDir.getExecutionTempDir())
			existingRegularFile.delete()
			def directoryFileResource=Mock(FileResource)
			directoryFileResource.getFile()>>existingRegularFile
		when:
			testee.convert(directoryFileResource)
		then:
			thrown BadDataException
	}
	
	def "should throw when attempting to convert a non existing file"(){
		given:
			File IKnowItWontExist=File.createTempFile("regular", "notDirectory", TempDir.getExecutionTempDir())
			IKnowItWontExist.delete()
			def directoryFileResource=Mock(FileResource)
			directoryFileResource.getFile()>>IKnowItWontExist
		when:
			testee.convert(directoryFileResource)
		then:
			thrown BadDataException
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import java.io.File
import java.util.Properties

import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceRepository
import org.squashtest.ta.plugin.commons.resources.CSVResource
import org.squashtest.ta.framework.exception.BadDataException
import org.squashtest.ta.plugin.commons.converter.FileToCSV;
import org.squashtest.ta.plugin.commons.resources.CSVConfiguration;
import org.squashtest.ta.plugin.commons.library.csv.CSVSeparator

import static org.junit.Assert.*

import spock.lang.Specification

class FileToCSVTest extends Specification {

	def "the CSV file must be correctly formated"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/goodCSVFile.csv")
			File file = new File(url.toURI())
			FileResource myResource = new FileResource(file)
			FileToCSV converter = new FileToCSV();
		when :
			CSVResource myCSVResource = converter.convert(myResource)
			
		then :
			myCSVResource != null
			notThrown(BadDataException) 
	}
	
	def "setting the wrong separation character leeds to crash"(){
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/goodCSVFile.csv")
			File file = new File(url.toURI())
			FileResource myResource = new FileResource(file)
			FileToCSV converter = new FileToCSV();
			List<Resource> configurations = new ArrayList<Resource>()
			CSVConfiguration newSeparationCharacter = new CSVConfiguration(CSVSeparator.SEMI_COLUMN)
			configurations.add(newSeparationCharacter)
			converter.addConfiguration(configurations)
		when :
			CSVResource myCSVResource = converter.convert(myResource)
			
		then :
			thrown(BadDataException)
	}
	
	def "a wrongly formated CSV causes crash"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/badCSVFile.csv")
			File file = new File(url.toURI())
			FileResource myResource = new FileResource(file)
			FileToCSV converter = new FileToCSV();
		when :
			CSVResource myCSVResource = converter.convert(myResource)
			
		then :
			thrown(BadDataException)
	}
}

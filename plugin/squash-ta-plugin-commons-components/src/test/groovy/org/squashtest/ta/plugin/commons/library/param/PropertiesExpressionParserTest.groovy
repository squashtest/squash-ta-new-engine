/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param

import spock.lang.Specification
import spock.lang.Unroll;

class PropertiesExpressionParserTest extends Specification {

	PropertiesExpressionParser testee
	String key
	String value
	
	def setup(){
		key="key.name"
		value="I'am the name valueà@@%%%"
		Properties prop = new Properties();
		
		prop.put(key,value);
		testee = new PropertiesExpressionParser(prop);
	}
	
	def "Should accept valid key"(){
		expect:
			testee.accept("Va.lid-Key_9")==true
	}
	
	@Unroll("Should refuse key #expression")
	def "Should refuse invalid key"(){
		expect:
			testee.accept(expression)==false
		where:
			expression << ["aa=aa","aa:aa","aa@aa","aa(aa"]
	}
	
	def"Should return an expression which contain the value corresponding to the searched key"(){
		when:
			def res = testee.parse(key);
		then:
			res.evaluate()==value
	}
	
	def"Should return the place holder when the searched key is not found"(){
		given:
			def searchKey = "not.existing"
			def obtainValue ="\${not.existing}"  
		when:
			def res = testee.parse(searchKey);
		then:
			res.evaluate()==obtainValue
	}
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java

import org.squashtest.ta.core.tools.io.Unzipper;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import java.util.Iterator;

import spock.lang.Specification;

class SourceEnumeratorTest extends Specification {
	SourceEnumerator testee
	def sourceDir
	
	def setup(){
		sourceDir=new Unzipper().unzipInTemp(getClass().getResourceAsStream("srcStandardAvecResources.zip"))
	}
	
	def cleanup(){
		new Unzipper().clean(sourceDir)
	}
	
	def "should find all java files in default codeTree"(){
		given:
			testee=new SourceEnumerator(sourceDir)
		and:
			def source1=new File(sourceDir,"java/com/example/Source1.java")
			def source2=new File(sourceDir,"java/com/example/Source2.java")
			def source3=new File(sourceDir,"java/com/example/subpackage/Source3.java")
			def expected=new HashSet<File>([source1,source2,source3])
		when:
			def result=testee.getJavaSourceFiles()
		then:
			result.size()==3
			expected.equals(result)			
	}
	
	def "should not include non-java file (as defined by extension)"(){
		given:
			testee=new SourceEnumerator(sourceDir)
		when:
			def result=testee.getJavaSourceFiles()
		then:
			!result.contains(new File(sourceDir,"java/com/example/noise.Tofilter"))
	}
	
	def "should not include files listed as resources"(){
		given:
			testee=new SourceEnumerator(sourceDir)
		when:
			def result=testee.getJavaSourceFiles()
		then:
			!result.contains(new File(sourceDir,"resources/com/ValidJavaSourceAsResource.java"))
	}
	
	def "should find all files included as resources"(){
		given:
			testee=new SourceEnumerator(sourceDir)
		and:
			def rsc1=new File(sourceDir,"resources/resource1.txt")
			def rsc2=new File(sourceDir,"resources/resource2.txt")
			def rsc3=new File(sourceDir,"resources/com/ValidJavaSourceAsResource.java")
			def rsc4=new File(sourceDir,"resources/com/example/resource3.txt")
			def expected=new HashSet<File>([rsc1,rsc2,rsc3,rsc4])
		when:
			def result=testee.getResourceFiles()
		then:
			result.containsAll(expected)
	}
	
	def "should find all resource files included as source (errare humanum est...)"(){
		given:
			testee=new SourceEnumerator(sourceDir)
		and:
			def rsc1=new File(sourceDir,"java/com/example/noise.Tofilter")
			def expected=new HashSet<File>([rsc1])
		when:
			def result=testee.getResourceFiles()
		then:
			result.containsAll(expected)
	}
	
	def "should not include source files in resources"(){
		given:
			testee=new SourceEnumerator(sourceDir)
		and:
			def source1=new File(sourceDir,"java/com/example/Source1.java")
			def source2=new File(sourceDir,"java/com/example/Source2.java")
			def source3=new File(sourceDir,"java/com/example/subpackage/Source3.java")
			def notWanted=new HashSet<File>([source1,source2,source3])
		when:
			def result=testee.getResourceFiles()
		then:
			!result.containsAll(notWanted)
	}
	
	def "should not include source in single source bundle resources"(){
		given:
			testee=new SourceEnumerator(new File(sourceDir,"java/com/example/Source1.java"))
		when:
			def result=testee.getResourceFiles()
		then:
			result.isEmpty()
	}
	
	def "should include source in single source bundle sources"(){
		given:
			File source=new File(sourceDir,"java/com/example/Source1.java")
		and:
			testee=new SourceEnumerator(source)
		when:
			Set<File> result=testee.getJavaSourceFiles()
		then:
			result.size()==1
                        File included=result.iterator().next();
			source.getName().equals(included.getName())
                        Iterator<String> sourceData=new SimpleLinesData(source.getAbsolutePath()).getLines().iterator();
                        Iterator<String> resultData=new SimpleLinesData(included.getAbsolutePath()).getLines().iterator();
                        while(sourceData.hasNext()){
                            resultData.hasNext()
                            sourceData.next().equals(resultData.next())
                        }
                        !resultData.hasNext()
	}

	
	def "should not include resource in single resource bundle sources"(){
		given:
			testee=new SourceEnumerator(new File(sourceDir,"java/com/example/noise.Tofilter"))
		when:
			def result=testee.getJavaSourceFiles()
		then:
			result.isEmpty()
	}
	
	def "should include resource in single resource bundle resources"(){
		given:
			File source=new File(sourceDir,"java/com/example/noise.Tofilter")
		and:
			testee=new SourceEnumerator(source)
		when:
			Set<File> result=testee.getResourceFiles()
		then:
			result.size()==1
			source.equals(result.iterator().next())
	}
}

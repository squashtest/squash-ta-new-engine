/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter

import spock.lang.Specification

import java.util.regex.Pattern
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.squashtest.ta.core.tools.io.BinaryData
import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.plugin.commons.library.param.PlaceHolderReplacer;

class FileToParametrizedFileTest extends Specification {

	FileToParametrizedFile testee
	PlaceHolderReplacer replacer
	BinaryData binData
	File file
	
	def setup(){
		testee = new FileToParametrizedFile()
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/param")
		file = new File(url.toURI())
		replacer = Mock(PlaceHolderReplacer)
		binData = Mock(BinaryData)
	}
	
        /*
        *  Yuck! Grrovy ignoring access modifiers and enabling 'untit testing' of PRIVATE mtehods.
        *  Making this test useless as a refactoring NRT harness...*sigh*
        */
	
	def"The 4 file of the bundle should be process"(){
	when :
		testee.convertTaResource(file, replacer)
	then :
		4 * replacer.apply(_) >> binData
	}
	
	def".txt files should be exclude from the bundle"(){
		given :
			Pattern exclude = Pattern.compile(".txt")
			testee.setExcludePattern(exclude)
		when :
			testee.convertTaResource(file, replacer)
		then :
			2 * replacer.apply(_) >> binData
		}
	
	def"Only data.txt file should be treated"(){
		given :
			Pattern include = Pattern.compile("data.txt")
			testee.setIncludePattern(include)
		when :
			testee.convertTaResource(file, replacer)
		then :
			1 * replacer.apply(_) >> binData
		}
	
}

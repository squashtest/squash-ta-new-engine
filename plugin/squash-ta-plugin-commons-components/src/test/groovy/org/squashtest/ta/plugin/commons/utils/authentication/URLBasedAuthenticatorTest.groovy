/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.utils.authentication

import spock.lang.Specification

class URLBasedAuthenticatorTest extends Specification {

	def "constructor : should default the password to empty string when none is provided"(){
		
		when :
			def auth = new URLBasedAuthenticator(null, null, new URL("http://www.locahost.com"))
		
		then :
			auth.password == ""		
	}
	
	def "constructor : should rant if the given base url is null"(){
		
		when :
			new URLBasedAuthenticator(null, null, null)
		then :
			thrown IllegalArgumentException
	}
	
	def "should acknowledge the requesting url"(){
		
		given :
			def auth = new URLBasedAuthenticator("bob", "mike", new URL("http://www.localhost.com"))
			AuthenticatorChain.getInstance().add(auth)
			
			
		when :
			def res =Authenticator.requestPasswordAuthentication("www.localhost.com", null, 80, "http", "please auth:", "(scheme)")
			
		
		then :
			res!=null
		
	}
	
	def "should not acknowledge the requesting url"(){
		
		given :
			def auth = new URLBasedAuthenticator("bob", "mike", new URL("http://www.localhost.com"))
			AuthenticatorChain.getInstance().add(auth)
			
		when :
			def res = Authenticator.requestPasswordAuthentication("ci.squashtest.org", null, 80, "https", "please auth:", "(scheme)")
		
		then :
			res == null
	}
	
}

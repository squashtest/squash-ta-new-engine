/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.commands

import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BadDataException;

import spock.lang.Specification;

class PauseCommandTest extends Specification {

	PauseCommand testee
	FileResource fileResource

	def "pause does execute and compile"(){
		given:
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/resources/pauseCommand/pauseOk")
		File file = new File(url.toURI())
		fileResource = new FileResource(file)

		testee=new PauseCommand()
		testee.setResource(fileResource)
		
		when:
		def t1 = System.currentTimeMillis()
		testee.apply()
		def t2 = System.currentTimeMillis()
		
		then:
		t2 >= t1 + 20
	}
	
	def "pause time could not be a negative number"(){
		given:
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/resources/pauseCommand/pauseKo1")
		File file = new File(url.toURI())
		fileResource = new FileResource(file)

		testee=new PauseCommand()
		testee.setResource(fileResource)
		
		when:
		testee.apply()
		
		then:
		thrown(BadDataException)
	}
	
	def "pause time must be a number"(){
		given:
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/resources/pauseCommand/pauseKo2")
		File file = new File(url.toURI())
		fileResource = new FileResource(file)

		testee=new PauseCommand()
		testee.setResource(fileResource)
		
		when:
		testee.apply()
		
		then:
		thrown(BadDataException)
	}
}

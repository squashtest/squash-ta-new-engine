/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date

import java.text.SimpleDateFormat

import org.squashtest.ta.plugin.commons.library.param.IllegalExpressionException

import spock.lang.Specification

class AddDayTest extends Specification {
	def testee
	def setup(){
		testee=new AddDay();
	}
	
	def "for a random date, forward 2days OK"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/11"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/13"))
		when:
			def actual=testee.evaluate(input,"2")
		then:
			expected.equals(actual)
	}
	
	def "for a random date, back 2days OK"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/11"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/09"))
		when:
			def actual=testee.evaluate(input,"-2")
		then:
			expected.equals(actual)
	}
	
	def "forward 2 days past month end"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/31"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/02/02"))
		when:
			def actual=testee.evaluate(input,"2")
		then:
			expected.equals(actual)
	}
	
	def "back 2 days from month 1st"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/05/01"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/04/29"))
		when:
			def actual=testee.evaluate(input,"-2")
		then:
			expected.equals(actual)
	}
	
	def "forward 2 days past year end"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2011/12/31"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/02"))
		when:
			def actual=testee.evaluate(input,"2")
		then:
			expected.equals(actual)
	}
	
	def "back 2 days from year 1st"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/01"))
		and:
			def expected=Calendar.getInstance()
			expected.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2011/12/30"))
		when:
			def actual=testee.evaluate(input,"-2")
		then:
			expected.equals(actual)
	}
	
	def "null argument gives IllegalArgumentException"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/01"))
		when:
			def actual=testee.evaluate(input,null)
		then:
			thrown(IllegalExpressionException)
	}
	
	def "empty argument gives IllegalArgumentException"(){
		given:
			def input=Calendar.getInstance()
			input.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2012/01/01"))
		when:
			def actual=testee.evaluate(input,"")
		then:
			thrown(IllegalExpressionException)
	}
}

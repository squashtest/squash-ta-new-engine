/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter;

import static org.junit.Assert.*

import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.exception.BadDataException

import spock.lang.Specification

class FileToPropertiesTest extends Specification {

	def "the properties file is correctly configured"(){
		
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/goodPpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
			
		then :
			notThrown BadDataException
	}
	
	def "the properties file with multiline properties is correctly configured"(){
		
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/goodMultiLinePpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
			
		then :
			notThrown BadDataException
	}
	
	def "should accept comments in the file"(){
	given :
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/commentInProperties.properties")
		File file = new File(url.toURI())
		FileResource myFileResource = new FileResource(file)
		FileToProperties myPropertiesConverter = new FileToProperties()
	when :
		PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
		
	then :
		notThrown BadDataException
	}

        def "should accept keys with no value"(){
            given :
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/emptyKeyIsValid.properties")
		File file = new File(url.toURI())
		FileResource myFileResource = new FileResource(file)
		FileToProperties myPropertiesConverter = new FileToProperties()
	when :
		PropertiesResource myProperties = myPropertiesConverter.convert(myFileResource)
		
	then :
		notThrown BadDataException
        }
    
	def "the conversion should crash with stray non-empty non-property line not part of a multiline property"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/badPpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myResource = myPropertiesConverter.convert(myFileResource)
			
		then :
			thrown BadDataException
	}

        def "multiline properties ending with a comma should not crash by default"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/badMultiLinePpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
		when :
			PropertiesResource myResource = myPropertiesConverter.convert(myFileResource)
			
		then :
			notThrown BadDataException
	}
    
	def "multiline properties ending with a comma should crash if ppk controle legacy mode"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/badMultiLinePpkFilter.properties")
			File file = new File(url.toURI())
			FileResource myFileResource = new FileResource(file)
			FileToProperties myPropertiesConverter = new FileToProperties()
                and:
                        URL cfgUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/converter/propertiesPpkLegacyControleActive.cfg")
                        File cfgFile = new File(cfgUrl.toURI());
                        myPropertiesConverter.addConfiguration([new FileResource(cfgFile)])
		when :
			PropertiesResource myResource = myPropertiesConverter.convert(myFileResource)
			
		then :
			thrown BadDataException
	}
}

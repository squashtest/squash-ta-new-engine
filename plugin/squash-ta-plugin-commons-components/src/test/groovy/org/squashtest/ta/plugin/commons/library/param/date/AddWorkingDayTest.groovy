/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date

import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import java.util.Properties;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import spock.lang.Specification

/**
 *
 * @author edegenetais
 */
class AddWorkingDayTest extends Specification{
	def testee
        def dsProps
        def dateFormat
        
        def setup(){
            testee=new AddWorkingDay()
            dsProps=new Properties();
            dateFormat=new SimpleDateFormat("yyyy-MM-dd")
            dsProps.setProperty(AddWorkingDay.KEY_NON_WORKING_DAYS,"")
        }
        
        def addGivenYearlyNonWorking(String dayList){
            dsProps.setProperty(AddWorkingDay.KEY_NON_WORKING_DAYS,dayList)
        }
    
        def "Should throw on missing off day list key"(){
            given:
                dsProps.clear()
            when:
                testee.addDataSource(dsProps);
            then:
                thrown IllegalConfigurationException
        }
        
        def "default friday +1 working day is monday"(){
            given:
                Date friday=dateFormat.parse("2018-01-19")
                Calendar fridayCal=new GregorianCalendar()
                fridayCal.setTime(friday)
            and:
                String expectedDate="2018-01-22"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(fridayCal,"1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "default monday -1 working day is friday"(){
            given:
                Date monday=dateFormat.parse("2018-01-15")
                Calendar mondayCal=new GregorianCalendar()
                mondayCal.setTime(monday)
            and:
                String expectedDate="2018-01-12"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(mondayCal,"-1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "middleWeekDefault wednesday + 1 is thursday"(){
            given:
                Date wednesday=dateFormat.parse("2018-01-17")
                Calendar wednesdayCal=new GregorianCalendar()
                wednesdayCal.setTime(wednesday)
            and:
                String expectedDate="2018-01-18"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(wednesdayCal,"1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "middleWeekDefault tuesday - 1 is monday"(){
            given:
                Date tuesday=dateFormat.parse("2018-01-16")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(tuesday)
            and:
                String expectedDate="2018-01-15"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(tuesdayCal,"-1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "skip non-working wednesday"(){
            given:
                Date tuesday=dateFormat.parse("2018-01-16")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(tuesday)
            and:
                addGivenYearlyNonWorking("2018-01-17,2018-03-25")
                testee.addDataSource(dsProps)
            and:
                String expectedDate="2018-01-18"
            when:
                Calendar result=testee.evaluate(tuesdayCal,"1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "properly load one-item lists"(){
            given:
                Date tuesday=dateFormat.parse("2018-01-16")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(tuesday)
            and:
                addGivenYearlyNonWorking("2018-01-17")
                testee.addDataSource(dsProps)
            and:
                String expectedDate="2018-01-18"
            when:
                Calendar result=testee.evaluate(tuesdayCal,"1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "easter-egg french version (CCMT legacy)"(){
            given:
                Date tuesday=dateFormat.parse("2018-01-16")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(tuesday)
            and:
                dsProps.remove(AddWorkingDay.KEY_NON_WORKING_DAYS)
                dsProps.setProperty(AddWorkingDay.KEY_JOURS_NON_OUVRES,"2018-01-17")
                testee.addDataSource(dsProps)
            and:
                String expectedDate="2018-01-18"
            when:
                Calendar result=testee.evaluate(tuesdayCal,"1")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
}


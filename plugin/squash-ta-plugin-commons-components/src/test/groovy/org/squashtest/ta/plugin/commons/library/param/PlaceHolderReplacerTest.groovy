/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param

import java.nio.charset.Charset;

import org.squashtest.ta.core.tools.io.BinaryData;

import spock.lang.Shared;
import spock.lang.Specification
import spock.lang.Unroll;

class PlaceHolderReplacerTest extends Specification {

	PlaceHolderReplacer testee
	BinaryData originalBinaryData
	Properties prop
	@Shared
	String propValue  = "Dummy value associate to key"
	@Shared
	String propValue2 = "Dummy value associate to key2\$\$"
	@Shared
	String propKey = "key.test"
	@Shared
	String propKey2 = "key.test2"
	@Shared
	String notExistingKey = "not.existing"
	
	def setup(){
		prop = new Properties()
		prop.put(propKey,propValue)
		prop.put(propKey2, propValue2)
		originalBinaryData = Mock()
	}
	
	@Unroll("When input data is #originalData, then output data should be #finalData")
	def "dd"(String originalData, String finalData){
	given:
		testee = new PlaceHolderReplacer(prop, Charset.forName("UTF-8"))
	and:
		originalBinaryData.toByteArray() >> originalData.getBytes()
	when:
		BinaryData res = testee.apply(originalBinaryData)
	then:
		new String(res.toByteArray()) == finalData
	where:
		originalData											|| 	finalData
		"test"													|| 	"test\n"
		"dummy\${"+propKey+"}dummy"								||	"dummy"+propValue+"dummy\n"
		"dummy\${"+propKey+"}dummy\${"+propKey2+"}end"			||	"dummy"+propValue+"dummy"+propValue2+"end\n"
		"dummy@\${"+propKey+"}@dummy"							||	"dummy\${"+propKey+"}dummy\n"
		"dummy@\${"+propKey+"}@dummy\${"+propKey2+"}end"		||	"dummy\${"+propKey+"}dummy"+propValue2+"end\n"
		"dummy\${"+notExistingKey+"}dummy"						||	"dummy\${"+notExistingKey+"}dummy\n"
		"dummy\${"+propKey+"}dummy\${"+notExistingKey+"}end"	||	"dummy"+propValue+"dummy\${"+notExistingKey+"}end\n"
	}
	
}

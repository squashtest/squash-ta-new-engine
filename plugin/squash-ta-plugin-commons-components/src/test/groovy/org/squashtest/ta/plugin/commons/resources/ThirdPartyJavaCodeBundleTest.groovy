/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources

import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.core.tools.io.Unzipper;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

import spock.lang.Specification

class ThirdPartyJavaCodeBundleTest extends Specification {

	def baseDir
	
	def setup(){
		baseDir=new Unzipper().unzipInTemp(getClass().getResourceAsStream("test-classes.zip"))
	}
	
	def cleanup(){
		new Unzipper().clean(baseDir)
	}
	
	
	def "should find expected classes"(){
		
		given :
                        def bundle = new ThirdPartyJavaCodeBundle(Collections.emptyList(), new File(baseDir,"test-classes"))
		when :
                    def res = bundle.getBundleClassNames()
			
		then :
			res.size()==1
                        res.iterator().next().equals("com.example.SimpleJupiterTest")
		
	}

}

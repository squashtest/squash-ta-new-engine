/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.resources

import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.core.tools.io.Unzipper;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

import spock.lang.Specification

class BundleResourceTest extends Specification {

	def baseDir
	
	def setup(){
		baseDir=new Unzipper().unzipInTemp(getClass().getResourceAsStream("/defaultSquashWorkspace.zip"))
		
	}
	
	def cleanup(){
		new Unzipper().clean(baseDir)
	}
	
	
	def "should say that the base directory is valid"(){
		
		given :
			def bundle = new BundleResource(baseDir,null)
		when :
			def res = bundle.isValidBaseDirectory()
			
		then :
			res == true
		
	}
	
	
	def "should say that the base directory is invalid because it's not a directory"(){
		
		given :
			
			def file = File.createTempFile("tester", "tester",TempDir.getExecutionTempDir());
		when :
			def bundle = new BundleResource(file,null)
		then :
			thrown(IllegalArgumentException)
		
	}
	
	def "should say that the base directory is invalid because doesn't exist"(){
		
		given :
			def file = new File(baseDir, "ahahaha")
			
		when : 
			def bundle = new BundleResource(file,null)
		then :
			thrown(IllegalArgumentException)
	}
	
	
	def "should say that the base directory is invalid because it's null"(){
		expect :
			false == new BundleResource().isValidBaseDirectory()
	}
	
	
	def "should say that the main file is valid because not specified yet"(){
		when:
			new BundleResource(baseDir, null)
		then:
			notThrown(IllegalArgumentException)
	}
	
	
	def "should say that the main file is valid because in the base directory"(){
		
		given :
			def mainFile = new File(baseDir, "defaultSquashWorkspace/src/squashTA/tests/toptest3.txt")
		when :
			new BundleResource(baseDir, mainFile)
		then :
			notThrown(IllegalArgumentException)
	}
	
	
	def "should say that main file is invalid because it doesn't exist"(){
		given :
			def mainFile = new File("azelrijapoierhapzioeurh")
		when : 
			new BundleResource(baseDir, mainFile)
		then :
			thrown(IllegalArgumentException)
	}
	
	def "should say that the main file is invalid because it's not a file"(){
		given :
			def mainFile = new File(".")
		when :
			new BundleResource(baseDir, mainFile)
		then :
			thrown(IllegalArgumentException)
	}
	
	def "should say that the main file is invalid because not in the base dir"(){
		
		given :
			def mainFile = File.createTempFile("tester", "tester", TempDir.getExecutionTempDir())
		when :
			new BundleResource(baseDir, mainFile)
		then :
			thrown(IllegalArgumentException)		
	}
	
	def "should return no main relative path because there is no main defined"(){
		
		expect :
			new BundleResource().getMainRelativePath() == null
		
	}
	
	def "should return a relative path because it's configured fully and correctly"(){
		
		given :
			def relPath =  "defaultSquashWorkspace/src/squashTA/tests/toptest3.txt"
			def bundle=new BundleResource(baseDir, new File(baseDir,relPath))		
		when :
			def path = bundle.getMainRelativePath()
		then :
			path.replaceAll("\\\\","/") == relPath
	
	}

	
	def "should construct a bundle correctly (1 arg)"(){
		when :
			def bundle = new BundleResource(baseDir, null)
		then :
			bundle.getBase() == baseDir
			bundle.getMain() == null
	}
	
	def "should construct a bundle correctly (2 args)"(){
		
		given :	
			def main = new File(baseDir, "defaultSquashWorkspace/src/squashTA/tests/toptest3.txt")
			
		when :
			def bundle = new BundleResource(baseDir, main)
		
		then :
			bundle.getMain() == main
			bundle.getBase() == baseDir
		
	}
	
	
	def "should rant because the basedir is invalid"(){
		when :
			new BundleResource(null, null)
		
		then :
			thrown IllegalArgumentException
	}
	
	def "should rant because the main is invalid"(){
		when :
			def file = File.createTempFile("tester", "tester", TempDir.getExecutionTempDir());
			new BundleResource(baseDir, file)
		
		then :
			thrown IllegalArgumentException;
			
	}
	
}

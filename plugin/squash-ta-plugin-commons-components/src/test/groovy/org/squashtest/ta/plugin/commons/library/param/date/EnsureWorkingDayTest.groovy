/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date

import java.text.SimpleDateFormat

class EnsureWorkingDayTest {
	def testee
        def dsProps
        def dateFormat
        
        def setup(){
            testee=new AddWorkingDay()
            dsProps=new Properties();
            dateFormat=new SimpleDateFormat("yyyy-MM-dd")
            dsProps.setProperty(AddWorkingDay.KEY_JOURS_NON_OUVRES,"")
        }
        
        def addGivenYearlyNonWorking(String dayList){
            dsProps.setProperty(AddWorkingDay.KEY_JOURS_NON_OUVRES,dayList)
        }
        
        def "Should throw on missing off day list key"(){
            given:
                dsProps.clear()
            when:
                testee.addDataSource(dsProps);
            then:
                thrown IllegalConfigurationException
        }
        
        def "default friday ensure working (AFTER) is same day"(){
            given:
                Date friday=dateFormat.parse("2018-01-19")
                Calendar fridayCal=new GregorianCalendar()
                fridayCal.setTime(friday)
            and:
                String expectedDate="2018-01-19"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(fridayCal,"AFTER")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "should throw if wrong parameter"(){
            given:
                Date friday=dateFormat.parse("2018-01-19")
                Calendar fridayCal=new GregorianCalendar()
                fridayCal.setTime(friday)
            and:
                String expectedDate="2018-01-19"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(fridayCal,"Gobbeldegook!")
            then:
                thrown java.lang.IllegalArgumentException
        }
        
        def "default friday ensure working (BEFORE) is same day"(){
            given:
                Date friday=dateFormat.parse("2018-01-19")
                Calendar fridayCal=new GregorianCalendar()
                fridayCal.setTime(friday)
            and:
                String expectedDate="2018-01-19"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(fridayCal,"BEFORE")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "Default saturday AFTER is monday"(){
            given:
                Date wednesday=dateFormat.parse("2018-01-20")
                Calendar wednesdayCal=new GregorianCalendar()
                wednesdayCal.setTime(wednesday)
            and:
                String expectedDate="2018-01-22"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(wednesdayCal,"AFTER")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        
        def "Default saturday BEFORE is friday"(){
            given:
                Date wednesday=dateFormat.parse("2018-01-20")
                Calendar wednesdayCal=new GregorianCalendar()
                wednesdayCal.setTime(wednesday)
            and:
                String expectedDate="2018-01-19"
            and:
                testee.addDataSource(dsProps)
            when:
                Calendar result=testee.evaluate(wednesdayCal,"BEFORE")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
        
        def "skip non-working wednesday AFTER to thursday"(){
            given:
                Date wednesday=dateFormat.parse("2018-01-17")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(wednesday)
            and:
                addGivenYearlyNonWorking("2018-01-17,2018-03-25")
                testee.addDataSource(dsProps)
            and:
                String expectedDate="2018-01-18"
            when:
                Calendar result=testee.evaluate(tuesdayCal,"AFTER")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
    
        def "rewind from non-working wednesday BEFORE to tuesday"(){
            given:
                Date wednesday=dateFormat.parse("2018-01-17")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(wednesday)
            and:
                addGivenYearlyNonWorking("2018-01-17,2018-03-25")
                testee.addDataSource(dsProps)
            and:
                String expectedDate="2018-01-16"
            when:
                Calendar result=testee.evaluate(tuesdayCal,"BEFORE")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
    
        def "properly load one-item lists"(){
            given:
                Date wednesday=dateFormat.parse("2018-01-17")
                Calendar tuesdayCal=new GregorianCalendar()
                tuesdayCal.setTime(wednesday)
            and:
                addGivenYearlyNonWorking("2018-01-17")
                testee.addDataSource(dsProps)
            and:
                String expectedDate="2018-01-18"
            when:
                Calendar result=testee.evaluate(tuesdayCal,"AFTER")
            then:
                String resultDate=dateFormat.format(result.getTime())
                resultDate.equals(expectedDate)
        }
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.converter

import java.lang.reflect.Method

import javax.tools.JavaCompiler
import javax.tools.ToolProvider

import org.slf4j.LoggerFactory
import org.squashtest.ta.core.tools.io.BinaryData
import org.squashtest.ta.core.tools.io.FileTree
import org.squashtest.ta.core.tools.io.SimpleLinesData
import org.squashtest.ta.core.tools.io.Unzipper
import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.plugin.commons.library.java.CompilationReport
import org.squashtest.ta.plugin.commons.library.java.CompilerConnector
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle

import spock.lang.Specification

class FileToJavaCodeBundleTest extends Specification{
	def base
	FileToJavaCodeBundle testee
	
//	def setup(){
//		base=new FileTree().createTempDirectory()
//		//the source code to compile is stored as a txt file in resources to avoid compilation ybt the IDE or maven
//		BinaryData source=new BinaryData(getClass().getResource("TestTargetJava.txt"))
//		//now we write it as a temp file with .java extension
//		source.write(new File(base,"TestTarget.java"))
//		
//		testee=new FileToJavaCodeBundle()
//		testee.COMPILER_CONNECTOR=new CompilerConnector()
//	}
//	
//	def cleanup(){
//		new FileTree().clean(base);
//	}
//	
//	def "should compile fine"(){
//		given:
//			def inputResource=new FileResource(new File(base,"TestTarget.java"))
//		when:
//			JavaCodeBundle codeBunde=testee.convert(inputResource)
//		then:
//			notThrown()
//	}
//	
//	def "class should be usable"(){
//		given:
//			def inputResource=new FileResource(new File(base,"TestTarget.java"))
//		when:
//			JavaCodeBundle codeBundle=testee.convert(inputResource)
//		then:
//			Class<?> compiledClass=codeBundle.getDedicatedClassloader().loadClass(codeBundle.getBundleClassNames().iterator().next())
//			Method addOne=compiledClass.getMethod("addOne", String.class)
//			Object instance=compiledClass.newInstance()
//			Integer.parseInt(addOne.invoke(instance, "9").toString())==10
//	}
//	
//	def "class should be loaded once per call to bundle getDedicatedClassLoader() (<=>isolated)"(){
//		given:
//			FileResource inputResource=new FileResource(new File(base,"TestTarget.java"))
//		and:
//			Class<?> generalClasspathClass
//			try{
//				generalClasspathClass=getClass().getClassLoader().loadClass("org.squashtest.ta.plugin.commons.converter.TestTarget")
//			}catch(ClassNotFoundException e){
//				generalClasspathClass=null
//			}
//		and:
//			JavaCodeBundle codeBundle=testee.convert(inputResource)
//		when:
//			Class<?> compiledClass=codeBundle.getDedicatedClassloader().loadClass(codeBundle.getBundleClassNames().iterator().next())
//			Class<?> compiledClass2=codeBundle.getDedicatedClassloader().loadClass(codeBundle.getBundleClassNames().iterator().next())
//		then:
//			!compiledClass.equals(generalClasspathClass)
//			!compiledClass2.equals(generalClasspathClass)
//			!compiledClass.equals(compiledClass2)
//	}
//	
//	def "testing java 1.4 support (using options, assuming sun JDK compiler, deactivates if -source 1.4 option unsupported)"(){
//		given:
//			def optionName="-source"
//			JavaCompiler compiler=ToolProvider.getSystemJavaCompiler()
//			//this test will only be active in the case of a Sun/Oracle javac compiler or one that supports -source 1.4
//			boolean sourceVersionJavacCompatible=(compiler.isSupportedOption(optionName)==1)
//			if(!sourceVersionJavacCompatible){
//				LoggerFactory.getLogger(getClass()).warn("Not a javac compatible compiler, source level 1.4 compliance not tested")
//			}
//		and:
//			File java1_4Base=new FileTree().createTempDirectory()
//			BinaryData java1_4_source=new BinaryData(getClass().getResource("TestTargetJava1_4.txt"))
//			java1_4_source.write(new File(java1_4Base,"TestTarget.java"))
//		and:
//			SimpleLinesData options=new SimpleLinesData([optionName,"1.4"])
//			File optionFile=File.createTempFile("options", ".txt", TempDir.getGroovyTempDir())
//			options.write(optionFile)
//			FileResource optionsResource=new FileResource(optionFile)
//			testee.addConfiguration([optionsResource])
//		and:
//			def inputResource=new FileResource(new File(java1_4Base,"TestTarget.java"))
//		when:
//			JavaCodeBundle codeBunde=null
//			if(sourceVersionJavacCompatible){
//				codeBunde=testee.convert(inputResource)
//			}			
//		then:
//			notThrown()
//		cleanup:
//			new FileTree().clean(java1_4Base)
//	}
//	
//	def "testing option passing"(){
//		given:
//			CompilerConnector connector=Mock()
//			testee.COMPILER_CONNECTOR=connector
//			CompilationReport report=Mock()
//			report.isSuccess()>>true
//			report.getCompiledClassNames()>>new HashSet<String>()
//			connector.compile(_, _, _)>>report
//		and:
//			String optionString="-source 1.4 unaryflag \"protected value\" other value"
//			File optionStringFile=File.createTempFile("unitTest", ".txt", TempDir.getGroovyTempDir());
//			new BinaryData(optionString.getBytes("utf-8")).write(optionStringFile)
//			testee.addConfiguration([new FileResource(optionStringFile)])
//		and:
//			FileResource inputResource=new FileResource(new File(base,"TestTarget.java"))
//		when:
//			testee.convert(inputResource)
//		then:
//			1*connector.compile(_,
//				{
//				it.containsAll(["-source","1.4","unaryflag","protected value","other","value"])
//				it.size()==6
//				}
//				,
//				 _)>>report
//	}
//	
//	def "should be able to load bundle resources from the resource tree"(){
//		given:
//			File bundleBase=new Unzipper().unzipInTemp(getClass().getResourceAsStream("/org/squashtest/ta/plugin/commons/library/java/srcStandardAvecResources.zip"))
//			FileResource bundleResource=new FileResource(bundleBase)
//		when:
//			JavaCodeBundle codeBundle=testee.convert(bundleResource)
//		then:
//			ClassLoader loader=codeBundle.getDedicatedClassloader()
//			def is=loader.getResourceAsStream("/resource1.txt")
//			is!=null
//			is.close()
//		cleanup:
//			new Unzipper().clean(bundleBase)
//			codeBundle.cleanUp()
//	}
//	
//	def "should be able to load bundle resources from the source code tree _discouraged but allowed_"(){
//		given:
//			File bundleBase=new Unzipper().unzipInTemp(getClass().getResourceAsStream("/org/squashtest/ta/plugin/commons/library/java/srcStandardAvecResources.zip"))
//			FileResource bundleResource=new FileResource(bundleBase)
//		when:
//			JavaCodeBundle codeBundle=testee.convert(bundleResource)
//		then:
//			ClassLoader loader=codeBundle.getDedicatedClassloader()
//			def is=loader.getResourceAsStream("/com/example/noise.Tofilter")
//			is!=null
//			is.close()
//		cleanup:
//			new Unzipper().clean(bundleBase)
//			codeBundle.cleanUp()
//	}
}

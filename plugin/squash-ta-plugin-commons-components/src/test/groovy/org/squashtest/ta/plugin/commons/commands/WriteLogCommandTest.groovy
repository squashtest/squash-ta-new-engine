/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.commands

import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BadDataException;

import spock.lang.Specification;

class WriteLogCommandTest extends Specification {

	WriteLogCommand testee=new WriteLogCommand()

	/** WFT !?! What part of Hell did that implementation-incestuous test slither from ? */
	//TODO: review this !
	def createMessage(String content){
		File msg=File.createTempFile("tmp",".txt")
		msg.deleteOnExit();
		FileWriter w=new FileWriter(msg);
		BufferedWriter bw=new BufferedWriter(w);
		bw.writeLine(content);
		bw.close();
		return msg;
	}
	
	def "test is ok in default scenario (1)"(){
		given:
		testee.message = createMessage("this is the message")
		def map = new HashMap<String, String>()
		map.put(testee.LOG_LEVEL, "DEBUG")
		testee.options = map
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		ok
	}
	
	def "test is ok in default scenario (2)"(){
		given:
		testee.message = createMessage("this is the message")
		def map = new HashMap<String, String>()
		map.put(testee.LOG_LEVEL, "INFO")
		testee.options = map
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		ok
	}
	
	def "test is ok in default scenario (3)"(){
		given:
		testee.message = createMessage("this is the message")
		def map = new HashMap<String, String>()
		map.put(testee.LOG_LEVEL, "WARN")
		testee.options = map
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		ok
	}
	
	def "test is ok in default scenario (4)"(){
		given:
		testee.message = createMessage("this is the message")
		def map = new HashMap<String, String>()
		map.put(testee.LOG_LEVEL, "ERROR")
		testee.options = map
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		ok
	}
	
	def "test is ok if log level is not indicated"(){
		given:
		testee.message = createMessage("this is the message")
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		ok
	}
	
	def "log levels ignore case"(){
		given:
		testee.message = createMessage("this is the message")
		def map = new HashMap<String, String>()
		map.put(testee.LOG_LEVEL, "error")
		testee.options = map
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		ok
	}
	
	def "fail if message empty and the end user is really high on getting hurt"(){
		given:
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/commands/emptyFile")
		FileResource file = new FileResource(new File(url.toURI()));
		testee.setResource(file)
                and:
                    def map = new HashMap<String, String>()
                    map.put(testee.LOG_LEVEL, "ERROR")
                and:
                //there goes the special case!
                    map.put(testee.RE_ENABLE_INSANE_LEGACY_CRASH_BEHAVIOR_ON_EMPTY_MESSAGES, "true")
                and:
                    testee.options = map
		when:
		testee.apply()

		then:
		BadDataException exception=thrown()
		exception.getMessage().contains("Cannot write an empty message into the logs.")
	}
	
        def "succeed if message is empty and user has not requested punishment"(){
            //NB: a warning will be issued before logging the empty message
            given:
		URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/commons/commands/emptyFile")
		FileResource file = new FileResource(new File(url.toURI()));
		testee.setResource(file)
                and:
                    def map = new HashMap<String, String>()
                    map.put(testee.LOG_LEVEL, "ERROR")
                    testee.options = map
		when:
		testee.apply()

		then:
                true
        }
    
	def "fail if unknown log level"(){
		given:
		testee.message = createMessage("this is the message")
		def map = new HashMap<String, String>()
		map.put(testee.LOG_LEVEL, "UNKNOWN")
		testee.options = map
		
		when:
		def ok = false
		testee.apply()
		ok = true
		then:
		BadDataException exception=thrown()
		exception.getMessage().contains("The Logger mode UNKNOWN is unknown")
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.java

import javax.tools.JavaCompiler
import javax.tools.JavaFileObject
import javax.tools.StandardJavaFileManager
import javax.tools.StandardLocation
import javax.tools.JavaCompiler.CompilationTask

import org.squashtest.ta.plugin.commons.library.java.CompilerConnector.ToolFactory

import spock.lang.Specification

class CompilerConnectorTest extends Specification {
	CompilerConnector testee
	JavaCompiler compiler
	StandardJavaFileManager fileManager
	
	def setup(){
		ToolFactory factory=Mock(CompilerConnector.ToolFactory)
		testee=new CompilerConnector()
		testee.toolFactory=factory
		
		compiler=Mock(JavaCompiler)
		factory.getJavaCompiler()>>compiler
		
		fileManager=Mock()
		fileManager.getLocation(StandardLocation.CLASS_PATH)>>[]
		compiler.getStandardFileManager(_,_,_)>>fileManager
	}
	
	def "should transmit options"(){
		given:
			def task=Mock(CompilationTask)
			task.call()>>true
		when:
			testee.compile(Collections.emptySet(), ["toto","toto2=yes"], new File("."))
		then:
			1*compiler.getTask(_,_,_,["toto","toto2=yes"],_,_)>>task
	}
	
	def "should transmit file list"(){
		given:
			def task=Mock(CompilationTask)
			task.call()>>true
		and:
			def sourceSet=new HashSet<File>([new File("toto.java")])
		and:
			def fileObjects=[Mock(JavaFileObject)]
			fileManager.getJavaFileObjects(new File("toto.java"))>>fileObjects
		when:
			testee.compile(sourceSet, [], new File("."))
		then:
			1*compiler.getTask(_,_,_,_,_,fileObjects)>>task
	}
}

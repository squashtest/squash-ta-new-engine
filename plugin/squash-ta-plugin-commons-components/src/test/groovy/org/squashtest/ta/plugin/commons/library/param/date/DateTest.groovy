/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.commons.library.param.date

import java.text.SimpleDateFormat
import org.squashtest.ta.plugin.commons.library.param.IllegalExpressionException

import spock.lang.Specification

class DateTest extends Specification {
    def testee
    def setup(){
        testee=new org.squashtest.ta.plugin.commons.library.param.date.Date()
    }
    
    def "Uses the given date as format"(){
        given:
            def properties=new Properties();
            properties.setProperty("date.prop","23/01/2013")
            testee.addDataSource(properties)
        and:
            def expectedDate=new SimpleDateFormat("yyyy-MM-dd").parse("2013-01-23")
        when:
            def result=testee.evaluate(null,"date.prop,dd/MM/yyyy")
        then:
            result.getTime().equals(expectedDate)
    }
}


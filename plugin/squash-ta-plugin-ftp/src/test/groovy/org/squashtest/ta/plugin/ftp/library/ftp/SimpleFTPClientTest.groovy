/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.library.ftp

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.ftp.library.ftp.SimpleFTPClient;
import org.squashtest.ta.framework.tools.TempDir;


import spock.lang.Specification
import spock.lang.Unroll;

class SimpleFTPClientTest extends Specification {

	
	def simpleClient = new SimpleFTPClient()
	def client = Mock(FTPClient)
	def mockFile
	
	def setup(){
		simpleClient.client = client;
		mockFile = File.createTempFile("temp", "temp", TempDir.getExecutionTempDir())
	}
		
	// *************** static methods ******************
	
	@Unroll("should say that filetype #iFiletype support is #check")
	def "should check the filetype"(){
		
		expect :
			check == SimpleFTPClient.checkFileTypeAsInt(iFiletype)
		
		where :
			iFiletype				|		check
			FTP.ASCII_FILE_TYPE		|		true
			FTP.BINARY_FILE_TYPE	|		true
			FTP.EBCDIC_FILE_TYPE	|		false
			-1						|		false
		
	}
	

	@Unroll("should decode #sFiletype as #result from stringified value")
	def "should decode filetype from int"(){
		
		expect :
			result == SimpleFTPClient.tryFiletypeAsInt(sFiletype)
			
		where :
			sFiletype							|		result
			FTP.ASCII_FILE_TYPE.toString()		|		FTP.ASCII_FILE_TYPE
			FTP.BINARY_FILE_TYPE.toString()		|		FTP.BINARY_FILE_TYPE
			FTP.EBCDIC_FILE_TYPE.toString()		|		null
			"-1"								|		null
			" 2 "								|		FTP.BINARY_FILE_TYPE
		
	}
	
	@Unroll("should decode #sFiletype as #result from stringified name")
	def "should decode filetype from string"(){
		
		expect :
			result == SimpleFTPClient.tryFiletypeAsString(sFiletype)
			
		where :
			sFiletype		|		result	
			"ascii"			|		FTP.ASCII_FILE_TYPE
			"ASCII"			|		FTP.ASCII_FILE_TYPE
			" asCiI "		|		FTP.ASCII_FILE_TYPE
			"binary"		|		FTP.BINARY_FILE_TYPE
			"BINARY"		|		FTP.BINARY_FILE_TYPE
			" biNaRy "		|		FTP.BINARY_FILE_TYPE
			"random"		|		null
			
	}
	
	
	@Unroll("should decode #sFiletype as #result from stringified value or name")
	def "should decode filetype from any string"(){
		
		expect :
			result == SimpleFTPClient.decodeFileType(sFiletype)
		
		where :
			sFiletype							|		result
			FTP.ASCII_FILE_TYPE.toString()		|		FTP.ASCII_FILE_TYPE
			"binary"							|		FTP.BINARY_FILE_TYPE
			"ffffff"							|		null
			"10"								|		null
			
	}
	
	// **************************** client status methods *******************
	
	def "should preinit (1)"(){
		
		given :
			presetSimpleClientWith(null, null, null, null, 10, FTPClientConfig.SYST_UNIX)
			
		when :
			simpleClient.preInitialization()
			
		then :
			simpleClient.filetype == 10
			simpleClient.port == FTP.DEFAULT_PORT
			1 * client.configure({ it instanceof FTPClientConfig })
			
	}
	
	def "should preinit (2)"(){
		
		given :
			presetSimpleClientWith(null, 50000, null, null, null, null)
		
		when :
			simpleClient.preInitialization()
		
		
		then :
			simpleClient.filetype == FTP.ASCII_FILE_TYPE
			simpleClient.port == 50000
			0 * simpleClient.configure(_)
		
	}
	
	def "should connect to a specific port"(){
		given :
			presetSimpleClientWith("robert", 50000, null, null, null, null)
			
		when :
			simpleClient.connect()
			
		then :
			1 * client.connect("robert", 50000)
			1 * client.enterLocalPassiveMode()
		
	}
	
	def "should connect to the default port"(){		
		given :
			presetSimpleClientWith("robert", null, null, null, null, null)
			
		when :
			simpleClient.connect()
			
		then :
			1 * client.connect("robert")
			1 * client.enterLocalPassiveMode()
	}
	
	def "should login with empty password"(){
		given :
			presetSimpleClientWith(null, null, "mike", null, null, null)
		
		when :
			simpleClient.login()
		
		then :
			1 * client.login("mike", "") >> true
	}
	
	def "should login with empty username (and fail gracefully)"(){
		given :
			presetSimpleClientWith(null, null, null, "bob", null, null)
		
		when :
			simpleClient.login()
		
		then :
			notThrown (InstructionRuntimeException)	
			1 * client.login("", "bob") >> false
			simpleClient.initialized == false
			simpleClient.why instanceof InstructionRuntimeException
	}
	
	def "should post init"(){
		
		given :
			simpleClient.filetype=2
			client.printWorkingDirectory() >> "aaa"
			
		when :
			simpleClient.postInitialization()
		
		then :
			1 * client.setFileType(2)
			simpleClient.homeDir == "aaa"
		
	}
	
	def "should start the client"(){
		
		given :
			presetSimpleClientWith("robert", null, null, null, null, null)
		
		when :
			simpleClient.start()
		
		then :
			1 * client.connect("robert", FTP.DEFAULT_PORT)
			1 * client.login("", "") >> true
			simpleClient.port == FTP.DEFAULT_PORT
			simpleClient.filetype == FTP.ASCII_FILE_TYPE
	}
	
	def "should stop the client"(){
		
		when : 
			simpleClient.stop()
			
		then :
			1 * client.logout()
			1 * client.disconnect()
		
	}

	
	// *************** basic operations *********************************

	def "should upload a file"(){
		
		given :
			assumeConnectionAlive()
		when :
			simpleClient.synchroPutFile("remote", mockFile, null)
			
		then :
			1 * client.storeFile("remote", _) >> true
	}
	
	def "should fail to upload because the file doesn't exist"(){
		
		given :
			simpleClient.initialized = true
			client.setNoOp() >> true	
			
			def buggyFile = new File("C:\\____________")

		when :
			simpleClient.synchroPutFile("remote", buggyFile, null)
		
		then :
			thrown InstructionRuntimeException
		
	}
	
	
	def "should fail to uploadd because the operation terminated abnormally"(){
		
		given :
			assumeConnectionAlive()
			client.storeFile(_,_) >> false
		when :
			simpleClient.synchroPutFile("remote", mockFile, null)
		then :
			thrown InstructionRuntimeException;
		
	}
	
	def "should fail to upload because an exception occured while transfering"(){
		
		given :
			client.setNoOp() >> {throw new IOException()}	//that line will just test the call to connectionAliveOrDie
															//but wont be the cause of the exception caught below
			client.login(_,_) >> true
			client.storeFile(_,_) >> { throw new IOException()} 
			
		when :
			simpleClient.synchroPutFile("remote", mockFile, 2 )
			
		then :
			thrown InstructionRuntimeException
			
	}
	
	def "should download a file"(){
		
		given :
			assumeConnectionAlive()						
			client.retrieveFile(_, _) >> true
			
		when :
		
			simpleClient.synchroGetFile("remote", null)
	
		then :
			notThrown IOException	
		
	}
	
	def "should fail to download because the operation terminated abnormally"(){
		given :
			assumeConnectionAlive()		
			
			client.retrieveFile(_,_) >> false
			
		when :
			simpleClient.synchroGetFile("remote", null)
			
		then :
			thrown InstructionRuntimeException
	}
	
	def "should fail to download because an exception occured"(){
		
		given :
			assumeConnectionAlive()		
			client.retrieveFile(_,_) >> { throw new IOException() }
			
		when :
			simpleClient.synchroGetFile("remote", 2)
			
		then :
			thrown InstructionRuntimeException			
		 
	}
	
	def "should delete a remote file"(){
		
		given :
			assumeConnectionAlive()		
		when :
			simpleClient.deleteFile("remote", false)
			
		then :
			1 * client.deleteFile("remote") >> true			
	}
	
	def "should delete a remote directory"(){
		
		given :
			assumeConnectionAlive()		
			
		when :
			simpleClient.deleteFile("remote/", true)
			
		then :
			thrown InstructionRuntimeException
		
	}
	
	def "should fail to delete a file due to abnormal termination"(){
		
		given :
			assumeConnectionAlive()		
			
		when : 
			simpleClient.deleteFile("remote", false)
			
		then :
			1 * client.deleteFile("remote") >> false
			thrown InstructionRuntimeException
		
	}
	
	def "should fail to delete a file because an exception occured"(){
		
		given :
			assumeConnectionAlive()
		
		when :
			simpleClient.deleteFile("remote", false) 
		
		then :
			1 * client.deleteFile("remote") >> {throw new IOException()}
			thrown InstructionRuntimeException 
	}
	
	
	// *************** basic operation interface *************************************
	
	def "should upload a file through API interface"(){
		
		given :
			
			assumeConnectionAlive()
			assumeSuccessfulUpload()
		
		when :
			
			simpleClient.putFile("remote", mockFile)
			
		then :
			0 * client.setFileType(_)
		
	}
	
	def "should upload a file through another API interface"(){
		
		given :
			assumeConnectionAlive()
			assumeSuccessfulUpload()
		
		when :
			simpleClient.putFile("remote", mockFile, "ascii")
		
		then :
			notThrown IllegalConfigurationException
			2 * client.setFileType(FTP.ASCII_FILE_TYPE)		
	}
	
	def "should fail to upload through API due to configuration"(){
		
		when :
			simpleClient.putFile("remote", mockFile, "scrogneugneu")
		
		then :
			thrown IllegalConfigurationException
	}
	
	def "should download a file through the API interface"(){
		
		when :
			assumeConnectionAlive()
			assumeSuccessfulDownload()
		
		then :
			simpleClient.getFile("remote")
			
		then :
			0 * client.setFileType(_)
		
	}
	
	def "should download a file through another API interface"(){
		
		given :
			assumeConnectionAlive()
			assumeSuccessfulDownload()
			
		when :
			simpleClient.getFile("remote", "binary")
			
		then :
			notThrown IllegalConfigurationException
			1 * client.setFileType(FTP.BINARY_FILE_TYPE)
			
	}
	
	def "should fail to download through API due to configuration"(){
		
		when :
			simpleClient.getFile("remote", "scrogneugneu")
			
		then :
			thrown IllegalConfigurationException
	}
	
	
	// *********************************misc *********************
	
	def "should reset the client"(){
		
		given :
			client.login(_,_) >> true
			
		when :
			simpleClient.reset()
			
		then :
			simpleClient.initialized == true
		
	}
	
	
	// *************** test class scaffolding ***************************
	
	def presetSimpleClientWith(host, port, login, pass, filetype, system){
		
		simpleClient.setHostName(host)
		simpleClient.setUsername(login)
		simpleClient.setPassword(pass)
		simpleClient.setPort(port)
		simpleClient.setFiletype(filetype)
		simpleClient.setSystem(system)
		
	}
	
	def assumeConnectionAlive(){
		simpleClient.filetype = FTP.ASCII_FILE_TYPE
		simpleClient.initialized = true
		client.sendNoOp() >> true
	}
	
	def assumeSuccessfulUpload(){
		client.storeFile(_,_) >> true
	}

	def assumeSuccessfulDownload(){
		client.retrieveFile(_,_) >> true
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

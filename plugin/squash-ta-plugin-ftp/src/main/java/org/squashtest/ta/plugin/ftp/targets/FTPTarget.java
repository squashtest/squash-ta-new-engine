/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.targets;

import java.io.File;
import java.util.Properties;

import org.apache.commons.net.ftp.FTPClient;
import org.squashtest.ta.framework.annotations.TATarget;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.plugin.ftp.library.ftp.SimpleFTPClient;



@TATarget("ftp")
public class FTPTarget implements Target {

	private SimpleFTPClient client;
	
	public FTPTarget(){
		super();
	}
	
	public FTPTarget(SimpleFTPClient client){
		this.client=client;
	}
	
	@Override
	public boolean init() {
		return client.init();
	}

	@Override
	public void reset() {
		client.reset();
	}

	@Override
	public void cleanup() {
		client.cleanup();
	}

	@Override
	public Properties getConfiguration() {
		return client.getConfiguration();
	}
	
	public void putFile(String remotePath, File file){
		client.putFile(remotePath, file);
	}
	
	public void putFile(String remotePath, File file, String fileType){
		client.putFile(remotePath, file, fileType);
	}
        
        //folder adding
        public void putFolder(String remoteDirPath, String localDirResource, String remoteParentDirPath) {
                client.putFolder(remoteDirPath, localDirResource, remoteParentDirPath);
        }
	
	public File getFile(String remotePath){
		return client.getFile(remotePath);
	}
	
	public File getFile(String remotePath, String fileType){		
		return client.getFile(remotePath, fileType);
	}
	
        public File getFolder(String remoteParentDirPath, String remoteDirPath) {
                return client.getFolder(remoteParentDirPath, remoteDirPath);
        }
        
	public void deleteFile(String remotePath, boolean isDirectory, boolean fileIfDoesNotExist){
			client.deleteFile(remotePath, isDirectory, fileIfDoesNotExist);
	}

	public FTPClient getClient(){
		return client.getClient();
	}

        public void deleteFolder(String remoteDirPath, boolean failIfDoesNotExist) {
                        client.deleteFolder(remoteDirPath, failIfDoesNotExist);
        }

   
}

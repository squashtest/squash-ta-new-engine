/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import java.util.Map;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;



/**
 * <p><strong>Description : </strong>That command will put a given file on the given ftp. It doesn't return anything so you can ignore the result of {@link #apply()}.</p>
 * 
 * <p>NOTE : as for now creation of missing directories on-the-fly is not supported. If the remotename remove is a directory and the said directory is not empty 
 * an exception will be raised.</p>
 * 
 *  <p>	<strong>Configuration :</strong> 
 *   a FileResource which entries are comma separated pairs of {@literal <key:value>}
 *   (note that column ':' is the separator)
 *  </p>
 *  <div>
 *  Available options :
 *  <ul>
 *  	<li>remotepath : file path name, relative to the user home directory. Mandatory.</li>
 *  	<li>filetype : 'ascii' or 'binary', case insensitive. Optional.</li>
 *  </ul>
 *  </div>
 * 
 *  <div>
 *    <strong>DSL example :</strong>
 *    <pre>EXECUTE put WITH myresource ON myftp USING $(remotepath : existingfolder/newfile.txt, filetype : ascii) AS null</pre>
 *  </div>
 *
 * @author bsiri
 *
 */
@TACommand("put")
public class FTPPutFileCommand extends AbstractFTPCommand implements Command<FileResource, FTPTarget> {
	
	private FileResource resource;
	
        
	public FTPPutFileCommand(){
		super();
	}


	@Override
	public void setTarget(FTPTarget target) {
		this.ftp=target;
	}

	@Override
	public void setResource(FileResource resource) {
                this.resource=resource;
        }
        
	@Override
	public VoidResource apply() {
		Map<String, String> options = getOptions();
		
		String remotePath = options.get(REMOTE_NAME_OPTION);
		String filetype = getFileType();
		
		if (filetype==null){
			ftp.putFile(remotePath, resource.getFile());
		}else{
			ftp.putFile(remotePath, resource.getFile(), filetype);
		}
                
		return new VoidResource();
	}
}

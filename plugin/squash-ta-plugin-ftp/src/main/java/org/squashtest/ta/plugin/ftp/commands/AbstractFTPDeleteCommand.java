/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import java.util.Map;

import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;

/**
 * Common base class for the new ({@link FTPDeleteCommand}) and legacy ( {@link LegacyFTPDeleteCommand}) versions of the
 * FTP delete command.
 * 
 * @author edegenetais
 * 
 */
public abstract class AbstractFTPDeleteCommand extends AbstractFTPCommand {
	/**
	 * Implementation for the {@link org.squashtest.ta.framework.components.Command#apply()} 
         * method (only subclasses effectively implement the {@link org.squashtest.ta.framework.components.Command}
	 * interface).
	 */
	public VoidResource apply() {
		Map<String, String> options = getOptions();

		String remotePath = options.get(REMOTE_NAME_OPTION);

		String failIfNotExistString = options.get("failIfDoesNotExist");
		boolean failIfDoesNotExist = !"false".equalsIgnoreCase(failIfNotExistString);
		ftp.deleteFile(remotePath, isDirectory(remotePath), failIfDoesNotExist);

		return new VoidResource();
	}

	/**
	 * Implementation for the {@link org.squashtest.ta.framework.components.Command#setTarget(org.squashtest.ta.framework.components.Target) } 
         * method (only subclasses effectively implement the
	 * {@link org.squashtest.ta.framework.components.Command} interface).
	 */
	public void setTarget(FTPTarget target) {
		this.ftp = target;
	}

}
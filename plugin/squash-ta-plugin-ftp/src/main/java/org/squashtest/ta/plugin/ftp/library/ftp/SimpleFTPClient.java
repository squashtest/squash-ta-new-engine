/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.library.ftp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;

public class SimpleFTPClient {


	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFTPClient.class);
	
	private Properties configuration;	
	
	private FTPClient client;
	
	/* ****** config ********** */

	private String hostname = null;
	private String username = null;
	private String password = null;
	private Integer port = null;
	
	private Integer filetype = null;
        
        private static final String SERVER_RETURNED_CODE = "', server returned error code ";
        
        private static final String SERVER_REMOVING_FILE_ERROR = "ftp client : an error occurred while removing file '";
	
        private static final String SERVER_UPLOADING_FILE_ERROR = "ftp client : an error occurred while uploading file '";
        
	private String system = null;
	
	
	/* ****** init status ****** */
	
	private boolean initialized;
	private InstructionRuntimeException why;
	private String homeDir;
        

	public SimpleFTPClient(){
		super();
		initialized=false;
	}
	        

	public void setHostName(String hostName) {
		this.hostname = hostName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
	public void setDefaultPort(){
		this.port=FTP.DEFAULT_PORT;
	}


	public void setFiletype(Integer filetype) {
		this.filetype = filetype;
	}

	public void setDefaultFiletype(){
		this.filetype= FTP.ASCII_FILE_TYPE;
	}

	public 	void setSystem(String system) {
		this.system = system;
	}

	
	/**
	 * May fail init without crashing the app : a failing ftp is only a problem when you require it.  
	 * 
	 * 
         * @return 
	 */
	public boolean init() {		
		client = new FTPClient();		
		start();
		return initialized;
	}


	public void reset() {
		stop();
		start();
	}

	
	private void start(){
		try {
			
			initialized=true;
			
			preInitialization();
			
			connect();
			login();
			
			postInitialization();		
			
		} catch (SocketException e) {
			failWith("The FTP client could not connect to socket. Maybe the net interface is not down, or port "+port+" is not correct : ", e);
		} catch (IOException e) {
			failWith("ftp client : error during communication with the server", e);
		}		
	}


	
	private void stop(){
		try {
			client.logout();
			client.disconnect();
		} catch (IOException e) {
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn("ftp client : error occurred when logging out / disconnect", e);
			}
		}		
	}

	public void cleanup() {
		stop();
	}
	
	public void setConfiguration(Properties configuration){
		this.configuration = configuration;
	}

	public Properties getConfiguration() {
		return (Properties)configuration.clone();		
	}
	
	
	
	public FTPClient getClient(){
		return client;
	}
	

	
	//* ****************** public interface for common operations ****************** 
	
	public void putFile(String remotePath, File file){
		synchroPutFile(remotePath, file, null);
	}
	
	public void putFile(String remotePath, File file, String fileType){
		
		Integer iFiletype =  decodeFileType(fileType);
		
		if (iFiletype == null){
			throw logAndThrowIllegalConfiguration("ftp : unsupported file type "+fileType, null);
		}
		
		synchroPutFile(remotePath, file, iFiletype);
	}
	
        public void putFolder(String remoteDirPath, String localDirResource, String remoteParentDirPath)  {
            try {
                connectionAliveOrDie();
                if (client.cwd(remoteDirPath)!=550) {
                    throw logAndThrow("ftp client : The same name folder exists!. Please delete the old directory before upload a new one.", null);
                }
                //create the root folder
                createFolder(remoteDirPath);
                LOGGER.debug("Root folder created: "+ remoteDirPath);
                //fulfill the folder content
                synchroPutFolder(remoteDirPath, localDirResource, remoteParentDirPath);
            } catch (IOException ex) {
                throw logAndThrow("ftp client : an error occurred while uploading folder '"+remoteDirPath + SERVER_RETURNED_CODE + client.getReplyCode(), ex);
            }           
        }


	public File getFile(String remotePath){
		return synchroGetFile(remotePath, null);
	}
	
	
	public File getFile(String remotePath, String fileType){
		
		Integer iFiletype =  decodeFileType(fileType);
		
		if (iFiletype == null){
			throw logAndThrowIllegalConfiguration("ftp : unsupported file type "+fileType, null);
		}
		
		return synchroGetFile(remotePath, iFiletype);
	}
	
        public File getFolder(String parentDirPath,String currentDirPath) {
            
                String downloaded;
                try {
                    File saveDirPath = File.createTempFile("ftp.client", "temp", TempDir.getExecutionTempDir());
                    downloaded = saveDirPath.getParent().replace('\\', '/')+"/dlDir";
                    //the download folder is created via FTP API tp the defined "downloaded" path
                    synchroGetFolder(parentDirPath, currentDirPath, downloaded);
                }catch (IOException e) {
			throw logAndThrow("ftp client : an error occurred while downloading the folder :"
                                +parentDirPath+"/"+currentDirPath + SERVER_RETURNED_CODE + client.getReplyCode(), e);
		}
           
                //load the downloaded file to the test resources
                return new File(downloaded);
        }
	/**
	 * Deletes a remote file. In this default version, the attempt to delete a non-existing file or directory triggers an exception.
	 * @param remotePath
	 *            the path of the file to delete on the remote system.
	 * @param isDirectory
	 *            set to <code>true</code> if the remote target is a directory.
	 */
	public synchronized void deleteFile(String remotePath, boolean isDirectory){
		deleteFile(remotePath, isDirectory, true);//by default, we fail
	}

	/**
	 * Deletes a remote file or directory.
	 * 
	 * @param remotePath
	 *            the path of the file to delete on the remote system.
	 * @param isDirectory
	 *            set to <code>true</code> if the remote target is a directory.
	 * @param failIfDoesNotExist
	 *            if <code>true</code> the all failures trigger exceptions, if
	 *            false, then attempts to delete non-existing files are silently
	 *            failed.
	 */
	public synchronized void deleteFile(String remotePath, boolean isDirectory, boolean failIfDoesNotExist){
		
		connectionAliveOrDie();
		
		boolean completed ;
		
		try{
			if (isDirectory){
                            // use 'deleteFolder' to remove directory instead
                            completed = false;
                            throw logAndThrow("ftp client : use 'deleteFolder' command to remove directory instead", null);
			} else{
                            completed = client.deleteFile(remotePath);
			}
			
			if (throwForFailure(failIfDoesNotExist, completed)){
				throw logAndThrow(SERVER_REMOVING_FILE_ERROR + remotePath + SERVER_RETURNED_CODE + client.getReplyCode(), null);
			}
			
		}catch(IOException ex){
			throw logAndThrow(SERVER_REMOVING_FILE_ERROR + remotePath + SERVER_RETURNED_CODE + client.getReplyCode(), ex);
		}
			
	}

	/**
	 * Decide if we must throw an exception to report delete failure
	 * @param failIfDoesNotExist
	 * @param completed
	 * @return
	 */
	private boolean throwForFailure(boolean failIfDoesNotExist,
			boolean completed) {
		boolean throwForFailure;
		if(completed){
			throwForFailure=false;
		}else{
                    throwForFailure = failIfDoesNotExist || client.getReplyCode()!=550; /*
                     * we throw if the file existed or if failIfDoesNotExist=true
                     * (meaning we had to report non-existing files)
                     */
		}
		return throwForFailure;
	}

	
	// ********************** private code corresponding to actual operations *****************
	
	
	private synchronized void synchroPutFile(String remotePath, File file, Integer fileType){
		
		connectionAliveOrDie();
		
		InputStream input=null;
		
		try {
			input = new FileInputStream(file);
			
			uploadFromStream(remotePath, fileType, input);			
			
		} 
		catch (FileNotFoundException e) {
			throw logAndThrow("ftp client : could not open file '"+file.getPath()+"'", e);
		} 
		catch (IOException e) {
			throw logAndThrow(SERVER_UPLOADING_FILE_ERROR + remotePath + SERVER_RETURNED_CODE + client.getReplyCode(), e);
		}
		finally{
			closeStream(input);
		}
		
	}

	private void uploadFromStream(String remotePath, Integer fileType,
			InputStream input) throws IOException{
		
		try{
			if (fileType!=null) { 
				client.setFileType(fileType); //set the specified file type	 
			}	
				
			navigateHome();
			// Add the parents directory creation if they not already exist. [Feat 864]
			// This replacement allow the use of '\' and '/' in the path given in the property file  
			String path = remotePath.replace('\\', '/');
			createDir(path);
			navigateHome();
			
			boolean success = client.storeFile(path, input);

			if (! success){
				throw logAndThrow(SERVER_UPLOADING_FILE_ERROR + remotePath + SERVER_RETURNED_CODE + client.getReplyCode(), null);
			}
		}
		finally{			
			if (fileType!=null) { 
				client.setFileType(this.filetype); //reset the configuration file type 
			}						
		}
		
	}
	
	
	private void createDir( String remotePath) throws IOException
	{
		String[] directories = remotePath.split("/");
		for (int i = 0; i < directories.length-1; i++) {
			String directory = directories[i];
			if(!directory.isEmpty() && !client.changeWorkingDirectory(directory)){
				boolean success = client.makeDirectory(directory);
				if (! success){
					throw logAndThrow("ftp client : an error occurred while creating one of the the parent directory of the file to upload '"+directory+
							SERVER_RETURNED_CODE + client.getReplyCode(), null);
				}
				client.changeWorkingDirectory(directory);
			}
		}
	}
	
	
	private synchronized File synchroGetFile(String remotePath, Integer fileType){
		
		connectionAliveOrDie();		
		
		File local;
		FileOutputStream out=null;
		
		try {

			local = File.createTempFile("ftp.client", "temp", TempDir.getExecutionTempDir());
			out = new FileOutputStream(local);
			
			downloadToStream(remotePath, fileType, out);
			
		} 
		catch (IOException e) {
			throw logAndThrow("ftp client : an error occurred while downloading the file :", e);
		}
		finally {
			closeStream(out);
		}
		
		return local;
		
	}


	private void downloadToStream(String remotePath, Integer fileType,
			FileOutputStream out) throws IOException{
		try {
			
			if (fileType!=null) { 
				client.setFileType(fileType); //set the overidden file type	
			}	
			
			navigateHome();
			boolean success = client.retrieveFile(remotePath, out);
			
			if (! success){
				throw logAndThrow("ftp client : an error occurred while downloading file '"+remotePath+"', server returned error "+client.getReplyCode(), null);				
			}
			
		} 
		finally{
			if (fileType!=null) { 
				client.setFileType(this.filetype);    //restore the default file type	 
			}	
		}
	}


	// ****************************** argument parsing ************** 
	
	public static Integer decodeFileType(String rawFiletype){
		
		if (rawFiletype==null){
			return null;
		}
		
		Integer result = tryFiletypeAsInt(rawFiletype);
		if (result == null){
			result = tryFiletypeAsString(rawFiletype);
		}
		
		return result;		
	}
	
	/*
	 * returns null if designated an unsupported file type
	 * 
	 */
	private static Integer tryFiletypeAsString(String raw){
		Integer intFileType = null;
		if ("ascii".equalsIgnoreCase(raw.trim())){
			intFileType = FTP.ASCII_FILE_TYPE;
		}else if ("binary".equalsIgnoreCase(raw.trim())){
			intFileType = FTP.BINARY_FILE_TYPE;
		}	
		return intFileType;
	}

	
	/*
	 * Returns null if was not a valid int, or indicated an unsupported file type.
	 * 
	 * @param raw
	 * @return
	 */
	private static  Integer tryFiletypeAsInt(String raw){
		Integer result=null;
		try{
			result = Integer.decode(raw.trim());
		}catch(NumberFormatException ex){
			// well, we'll try as String later
		}
		
		if ((result!=null) && (checkFileTypeAsInt(result))){
			return result;
		}else{
			return null;
		}
	}
	
	
	private static boolean checkFileTypeAsInt(Integer fileType){
		return ((fileType == FTP.ASCII_FILE_TYPE) || (fileType == FTP.BINARY_FILE_TYPE));
	}
	
	
	//*************************** micro methods because SONAR complained too loud ******
	
	private void preInitialization(){
		
		if (filetype==null){
			setDefaultFiletype();		
		}
		if (port==null){
			setDefaultPort();		
		}
		
		if (system!=null){
			FTPClientConfig config = new FTPClientConfig(system);
			client.configure(config);
		}		
	}
		
	private void connect() throws SocketException, IOException{
		if (port!=null){
			client.connect(hostname, port);
		}else{
			client.connect(hostname);
		}		
		client.enterLocalPassiveMode();
	}
	
	private void login() throws IOException{
		boolean successfulLogin;
		
		String pass = (password!=null) ? password : ""; 
		String user = (username!=null) ? username : "";
		successfulLogin = client.login(user, pass);
	
		if (! successfulLogin){
			int reply = client.getReplyCode();
			StringBuilder errorMessage = new StringBuilder("FTP client : could not login to the server, perhaps the credentials were rejected. ");
			if (password!=null || username!=null){
				errorMessage.append("Check that your username/password configuration is correct. ");
			}
			errorMessage.append("Failed with reply code : ").append(reply);	
			failWith(errorMessage.toString(), null);
		}		
	}

	
	
	private void postInitialization() throws IOException{
		client.setFileType(filetype);
		homeDir = client.printWorkingDirectory();
	}

	
	private void closeStream(OutputStream out){
		try{
			if (out!=null){
				out.close();				
			}
		} catch (IOException e) {
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn("ftp client : download complete, however an error" +
							" occurred after operation completion. Proceeding anyway.",e);
			}
		}	
	}
	
	private void closeStream(InputStream in){
		try{
			if (in!=null){
				in.close();				
			}
		} catch (IOException e) {
			if (LOGGER.isWarnEnabled()){
				LOGGER.warn("ftp client : upload complete, however an error occurred after " +
							"operation completion. Proceeding anyway.",e);
			}
		}	
	}
	
	/* ************************************** other useful things ***********************/
	
	private void navigateHome() throws IOException{
		client.changeWorkingDirectory(homeDir);
	}
	
	private void connectionAliveOrDie(){
		boolean shouldRestart=false;
		
		if (initialized){
			try{
				boolean completed = client.sendNoOp();
				if (! completed) {
					shouldRestart=true;
				}
			}catch(IOException ex){
                                LOGGER.debug("Connection should be restarted. Connection is initialized but an error occured while using client.",ex);
				shouldRestart=true;
			}
		}else{
			shouldRestart=true;
		}
		
		if (shouldRestart){
			start();
		}

		if (! initialized){
			throw why;
		}
	}

	
	private void failWith(String message, Exception why){
		if (LOGGER.isErrorEnabled()){
			LOGGER.error(message,why);
		}
		initialized=false;
		this.why = new InstructionRuntimeException(message, why);
	}
	
	
	private InstructionRuntimeException logAndThrow(String message, Exception orig){
		if (LOGGER.isErrorEnabled()){
			LOGGER.error(message, orig);
		}
		return new InstructionRuntimeException(message, orig);
	}

	
	private IllegalConfigurationException logAndThrowIllegalConfiguration(String message, Exception orig){
		if (LOGGER.isErrorEnabled()){
			LOGGER.error(message, orig);
		}
		return new IllegalConfigurationException(message, orig);
	}

    private void createFolder(String remoteFileorFolderPath) throws IOException {
        if (client.cwd(remoteFileorFolderPath)==550) {
            boolean success = client.makeDirectory(remoteFileorFolderPath);
                
            if (! success){
                throw logAndThrow("ftp client : an error occurred while uploading folder '"+remoteFileorFolderPath + SERVER_RETURNED_CODE + client.getReplyCode(), null);
            }
        } else {
            LOGGER.debug("A folder with this path : '" + remoteFileorFolderPath + "' exists already.");
        }
        
    }

    private void synchroPutFolder(String remoteDirPath, String localDirResource, String remoteParentDirPath) throws IOException {
        connectionAliveOrDie();
        String newRemoteDirPath = remoteDirPath.replace('\\', '/');
               
        File localDir = new File(localDirResource);
        File[] subFiles = localDir.listFiles();
        if (subFiles != null && subFiles.length >0) {

            for (File file:subFiles) {
                String name = file.getName();
                String remoteFileorFolderPath = ("".equals(remoteParentDirPath)) ? (newRemoteDirPath + name) 
                                                 :     (newRemoteDirPath + remoteParentDirPath + name);
                
                checkIfFolderOrFileForUpload(file, remoteFileorFolderPath, remoteParentDirPath, newRemoteDirPath, name);
                
            }
        }
    }

    private boolean putFileToFolder(String remoteFilePath, File file) throws IOException {
        connectionAliveOrDie();
        
        InputStream input = null;
        
        try {
            input = new FileInputStream(file);
            String fileName = file.getName();
            String fileExtension = fileName.substring(fileName.lastIndexOf(".")+1);
            if (isACSIIType(fileExtension)) {
                client.setFileType(FTP.ASCII_FILE_TYPE);
            } else {
                client.setFileType(FTP.BINARY_FILE_TYPE);
            }
                           
            return client.storeFile(remoteFilePath, input);
        }
        catch (FileNotFoundException e) {
                throw logAndThrow("ftp client : could not open file '"+file.getPath()+"'", e);
        } 
        catch (IOException e) {
                throw logAndThrow(SERVER_UPLOADING_FILE_ERROR + remoteFilePath + SERVER_RETURNED_CODE + client.getReplyCode(), e);
        }
        finally{
                closeStream(input);
        }
       
                
    }

    private void synchroGetFolder(String parentDirPath, String currentDirPath, String saveDir) throws IOException {
        connectionAliveOrDie();
        String dirToList = parentDirPath;
        currentDirPath = currentDirPath.replace('\\', '/');
        saveDir = saveDir.replace('\\', '/');
        
        //verify if the current folder is no longer empty as start
        if (!"".equals(currentDirPath)) {
            dirToList += "/" + currentDirPath;
        }
        
        FTPFile[] subFiles = client.mlistDir(dirToList);
        
        if (subFiles != null && subFiles.length >0) {
            //the current FTP client is NOT empty
            for (FTPFile file: subFiles) {
                String currentName = file.getName();
                if (!".".equals(currentName) && !"..".equals(currentName)) {
                    // means it is not the parent directory or the directory itself
                                        
                    String newPath =  (saveDir + "/" + currentName);
                    
                    //check if the current is file or folder
                    checkIfFolderOrFileForDownload(file, newPath, saveDir, dirToList, currentName);
                }
            }
        } else {
            //the request folder is empty
            LOGGER.info("ftp client : the requested folder for downloading is empty. No action is executed.");
        }
        
       
    }

    private boolean downloadSingleFile(String relativeFilePath, String newFolderPath) throws IOException {
        File downloadFile = new File(newFolderPath);
        File parentDir = downloadFile.getParentFile();
        
        if (!parentDir.exists()) {
            parentDir.mkdir();
        }
        
        try (OutputStream out = new BufferedOutputStream(new FileOutputStream(downloadFile))) {
            String relativeFilePathExtension = relativeFilePath.substring(relativeFilePath.lastIndexOf(".")+1);
            if (isACSIIType(relativeFilePathExtension)) {
                client.setFileType(FTP.ASCII_FILE_TYPE);
            } else {
                //check here
                client.enterLocalPassiveMode();
                client.setFileType(FTP.BINARY_FILE_TYPE);
                client.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            }
            
            return client.retrieveFile(relativeFilePath, out);
        }catch (IOException e) {
            throw logAndThrow("ftp client : an error occurred while downloading file '"+newFolderPath+"/"+relativeFilePath + SERVER_RETURNED_CODE + client.getReplyCode(), e);
        }
    }

    public void deleteFolder(String remotePath, boolean failIfDoesNotExist) {
        try {
                connectionAliveOrDie();
                
                synchroDeleteFolder(remotePath, "", failIfDoesNotExist);
                
        } catch (IOException ex) {
                throw logAndThrow("ftp client : an error occurred while removing folder '"+remotePath + SERVER_RETURNED_CODE + client.getReplyCode(), ex);
        }        
    }

    private boolean synchroDeleteFolder(String parentDirPath, String currentDirPath, boolean failIfDoesNotExist) throws IOException {
        
        boolean successDeletion;
        
        String dirToList = (!"".equals(currentDirPath)) ? (parentDirPath+"/"+currentDirPath)
                : (parentDirPath); 
        
        FTPFile[] subFiles = client.mlistDir(dirToList);
             
        //if the folder is NOT empty
        if (subFiles != null && subFiles.length >0) {
            for (FTPFile fileOrFolder : subFiles) {
                String currentName = fileOrFolder.getName();
                if ((!".".equals(currentName))&&(!"..".equals(currentName))){
                    // means it is not the parent directory or the directory itself
                    String relativeFilePath = ("".equals(currentDirPath)) ? (parentDirPath + "/" + currentName)
                            :(parentDirPath + "/" + currentDirPath + "/" + currentName);
                                                      
                    //check if the current is file or folder
                    successDeletion = checkIfFolderOrFileForDeletion(fileOrFolder, dirToList, currentName, failIfDoesNotExist, relativeFilePath);
                }
            }
            
            //finally remove the directory itself now is empty
            successDeletion = deleteFolderItself(dirToList, failIfDoesNotExist);
            
        } else {
            //if the folder is empty
            successDeletion = deleteFolderItself(dirToList, failIfDoesNotExist);
        }
        
        return successDeletion;
    }

    private void checkIfFolderOrFileForDownload(FTPFile file, String newFolderPath, String saveDir, String dirToList, String currentName) throws IOException {
        if (file.isDirectory()){
            //create directory in saveDir
            File newDir = new File(newFolderPath);
            boolean createFolder = newDir.mkdirs();
            if (!createFolder) {
                throw logAndThrow("ftp client : an error occurred while downloading folder '"+newFolderPath + SERVER_RETURNED_CODE + client.getReplyCode(), null);
            } else {
                LOGGER.debug("Directory downloaded: "+ newFolderPath);
            }
            //recursively download its subfolders and files
            String newSaveDir = saveDir + newFolderPath.substring(newFolderPath.lastIndexOf("/"), newFolderPath.length()); 
            synchroGetFolder(dirToList, currentName, newSaveDir);
        } else {
            //download file
            boolean createFile = downloadSingleFile(dirToList+"/"+currentName, newFolderPath);
            if (createFile) {
                LOGGER.debug("File downloaded: "+ newFolderPath);
            }
        }
    }

    private void checkIfFolderOrFileForUpload(File file, String remoteFileorFolderPath, String remoteParentDirPath,
                            String remoteDirPath, String name) throws IOException {
        if (file.isFile()) {
            //upload the file
            LOGGER.debug("Uploading file: "+ remoteFileorFolderPath);
            boolean uploadFile = putFileToFolder(remoteFileorFolderPath, file);
            if (! uploadFile) {
                throw logAndThrow(SERVER_UPLOADING_FILE_ERROR + remoteFileorFolderPath + SERVER_RETURNED_CODE + client.getReplyCode(), null);
            }
        } else {
            //create directory on the server
            LOGGER.debug("Creating folder: "+ remoteFileorFolderPath);
            createFolder(remoteFileorFolderPath);
            //upload the sub directory
            String newParent = ("".equals(remoteParentDirPath)) ? (name+ "/") :(remoteParentDirPath + name + "/") ;
            synchroPutFolder(remoteDirPath, file.getAbsolutePath(), newParent);
        }
    }

    private boolean checkIfFolderOrFileForDeletion(FTPFile fileOrFolder, String dirToList, String currentName, boolean failIfDoesNotExist, String relativeFilePath) throws IOException {
        if (fileOrFolder.isDirectory()){
            //recursively remove the sub directory
            return synchroDeleteFolder(dirToList, currentName, failIfDoesNotExist);
        } else {
            //delete the file
            if (client.deleteFile(relativeFilePath)) {
                LOGGER.debug("File deleted: "+ relativeFilePath);
                return true;
            } else {
                throw logAndThrow("ftp client : an error occurred while deleting file '"+relativeFilePath + SERVER_RETURNED_CODE + client.getReplyCode(), null);
            }
        }
    }

    private boolean deleteFolderItself(String dirToList, boolean failIfDoesNotExist) throws IOException {
        boolean successDeletion = client.removeDirectory(dirToList);
        
        if (successDeletion) {
            LOGGER.debug("Folder deleted: "+ dirToList);
        } else {
            if (throwForFailure(failIfDoesNotExist, successDeletion)){
				throw logAndThrow(SERVER_REMOVING_FILE_ERROR+dirToList + SERVER_RETURNED_CODE + client.getReplyCode(), null);
            }
        }
        return successDeletion;
    }

    private boolean isACSIIType(String fileExtension) {
        for (ASCIITextFileType fileType : ASCIITextFileType.values()) {
            if (fileType.getName().equals(fileExtension)){
                LOGGER.debug("This file type is an ASCII text filetype.");
                return true;
            }
        }
        LOGGER.debug("This file type is NOT an ASCII text filetype.");
        return false;
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.targets;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.core.templates.FileBasedCreator;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.TATargetCreator;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.plugin.commons.library.ShebangCheck;
import org.squashtest.ta.plugin.ftp.library.ftp.SimpleFTPClient;
import org.squashtest.ta.plugin.ftp.library.ftp.SimpleFTPClientFactory;


/**
 * <p>Will parse a properties file that will configure and create accordingly a FTP client. The static 
 * attributes in the upper section of that file describe the available keys in the configuration file.
 * If you wonder what they mean, then either you don't need them, either you need to refer to
 * {@link org.apache.commons.net.ftp.FTPClient}, 
 * {@link org.apache.commons.net.ftp.FTP} 
 * or {@link org.apache.commons.net.ftp.FTPClientConfig} for more details.</p>
 * 
 * <p>The current FTPTargets won't support connection through proxy.</p>
 * 
 * <div>Supported configuration is :
 * <ul>
 * 	<li> 'squashtest.ta.ftp.host' 		: supply the host name (mandatory)</li>
 * 	<li> 'squashtest.ta.ftp.username' 	: the username to log to (mandatory)</li>
 * 	<li> 'squashtest.ta.ftp.password' 	: the corresponding password (mandatory)</li>
 *      <li> 'squashtest.ta.ftp.port' 		: a different port than default</li>
 *      <li> 'squashtest.ta.ftp.filetype'	: the default file type used for each transfert 
 *                  when not specified otherwise. Currently supported : ascii or binary</li>
 *      <li> 'squashtest.ta.ftp.system' 	: the host system type. 
 *                  Currently supported : unix, vms, windows, os/2, os/400, as/400, mvs, l8, netware, macos</li>
 * </ul>
 * </div>
 * 
 * @author bsiri
 *
 */
@TATargetCreator("target.creator.ftp")
public class FTPTargetCreator extends FileBasedCreator implements TargetCreator<FTPTarget> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FTPTargetCreator.class);

	private static final ShebangCheck SHEBANG_CHECK = new ShebangCheck("ftp");
	
	private PropertiesBasedCreatorHelper helper = new PropertiesBasedCreatorHelper();
	
	private SimpleFTPClientFactory factory = new SimpleFTPClientFactory();
	
	public FTPTargetCreator(){
		helper.setKeysRegExp(SimpleFTPClientFactory.FTP_KEYS_REGEXP);
	}
	
	
	@Override
	public boolean canInstantiate(URL propertiesFile) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Testing eligibility of "+propertiesFile+" as FTP configuration URL.");
		}
		boolean isFtpConfiguration = false;
		try {
			//if there is shebang #!ftp (or no shebang at all, for retrocompatibility)
			isFtpConfiguration = isFTPConfiguration(propertiesFile);

		} catch (IOException ioe) {
			throw new BrokenTestException("Cannot access transmitted target definition URL",ioe);
		}	
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(propertiesFile+" is"+(isFtpConfiguration?" ":" not ")+"eligible.");
		}
		return isFtpConfiguration;
	}
	


	@Override
	public FTPTarget createTarget(URL propertiesFile) {
		try {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Creating target from configuration URL '"+propertiesFile.toExternalForm()+"'");
		}
		
		File file = getFileOrFail(propertiesFile);
		Properties properties;
		
		properties = helper.getEffectiveProperties(file);
		
		SimpleFTPClient client = factory.createClient(properties);
		
		Properties anonymised = helper.anonymize(properties, 1, 
												SimpleFTPClientFactory.FTP_LOGIN_KEY, 
												SimpleFTPClientFactory.FTP_PASSWORD_KEY);
		
		
		client.setConfiguration(anonymised);
		
		return new FTPTarget(client);
		} catch (IOException e) {
			throw new BrokenTestException("FTP target cannot be instantiated. Could not read target file configuration '"+propertiesFile+"'.", e);
		} catch (URISyntaxException e) {
			throw new BrokenTestException("FTP target cannot be instantiated. Definition URL '"+propertiesFile+"' was no valid URI", e);
		}
	}
	
	private boolean isFTPConfiguration(final URL propertiesFile) throws IOException{
		boolean hasFTPshebang = SHEBANG_CHECK.hasShebang(propertiesFile);
		boolean shebangOk = !SHEBANG_CHECK.hasAnyShebang(propertiesFile, FTPTargetCreator.class.getSimpleName()) || hasFTPshebang;

		boolean isFtpConfiguration = false;
		if (containsCorrectKeys(propertiesFile) && shebangOk){
			isFtpConfiguration = true;				
		}else if (hasFTPshebang){
			//we warn only if the target does have the good shebang (if it is meant to be another type of target, we must not give false alarm)
			LOGGER.error("FTP target '"+propertiesFile+"' cannot be instantiated. The configuration file should supply at least the following property: '"+SimpleFTPClientFactory.FTP_HOST_KEY+"'.");
		}
		return isFtpConfiguration;
	}
	
	private boolean containsCorrectKeys(final URL propertiesFile){
		final File file = getFileOrNull(propertiesFile);
		boolean isFTPConfigurationType;
		if (file==null){
			isFTPConfigurationType = false;
		}else{
			try {
				Properties properties = helper.getEffectiveProperties(file);
				PropertiesKeySet keySet = new PropertiesKeySet(properties);
				//the properties must contains the host key, and its value must not be null
				if (keySet.contains(SimpleFTPClientFactory.FTP_HOST_KEY)){
					isFTPConfigurationType = !properties.get(SimpleFTPClientFactory.FTP_HOST_KEY).toString().isEmpty();
				}else{
					isFTPConfigurationType = false;
				}
			} catch (IOException e) {
				LOGGER.warn("Could not read file {}.",file.getAbsolutePath(),e);
				isFTPConfigurationType = false;
			}
		}	
		return isFTPConfigurationType;
	}


}

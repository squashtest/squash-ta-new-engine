/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.library.ftp;

/**
 * This enum is to list all file extensions that will pass through ASCII_FILE_TYPE mode of FTP processes
 * 
 * @author qtran
 */
public enum ASCIITextFileType {
    ADA ("ada"),
    ASM ("asm"),
    ASP ("asp"),
    BAS ("bas"),
    BAT ("bat"),
    C ("c"),
    CFM ("cfm"),
    CGI ("cgi"),
    CMD ("cmd"),
    CONF ("conf"),
    CPP ("cpp"),
    CSH ("csh"),
    CSS ("css"),
    CSV ("csv"),
    DEF ("def"),
    DHTML ("dhtml"),
    DIC ("dic"),
    DIF ("dif"),
    DIZ ("diz"),
    DTD ("dtd"),
    F77 ("f77"),
    F90 ("f90"),
    F95 ("f95"),
    FORT ("for"),
    H ("h"),
    HPP ("hpp"),
    HTM ("htm"),
    HTML ("html"),
    ICS ("ics"),
    IDL ("idl"),
    INC ("inc"),
    INI ("ini"),
    JAVA ("java"),
    JDL ("jdl"),
    JS ("js"),
    JSP ("jsp"),
    LDIF ("ldif"),
    LIST ("list"),
    LOG ("log"),
    LSP ("lsp"),
    LUA ("lua"),
    M4 ("m4"),
    MAK ("mak"),
    MF ("mf"),
    MK ("mk"),
    OUT ("out"),
    PAS ("pas"),
    PATCH ("patch"),
    PEM ("pem"),
    PHP ("php"),
    PHTML ("phtml"),
    PL ("pl"),
    PO ("po"),
    PS ("ps"),
    PY ("py"),
    RTF ("rtf"),
    SH ("sh"),
    SHTML ("shtml"),
    SQL ("sql"),
    SRC ("src"),
    TCL ("tcl"),
    TEMPLATE ("template"),
    TEX ("tex"),
    TK ("tk"),
    TPL ("tpl"),
    TSV ("tsv"),
    TXT ("txt"),
    VBS ("vbs"),
    WSDL ("wsdl"),
    XHTML ("xhtml"),
    XML ("xml"),
    XRC ("xrc"),
    XSD ("xsd"),
    XSL ("xsl"),
    FIRST("1st");
    
    private String name = "";

    //Constructeur
    ASCIITextFileType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    
}

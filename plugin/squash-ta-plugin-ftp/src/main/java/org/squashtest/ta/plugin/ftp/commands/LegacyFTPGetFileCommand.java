/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.tools.ComponentRepresentation;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;

/**
 * <p><strong>Description : </strong>That command will fetch a resource from a given ftp. It doesn't need any Resource as input so the FileResource type argument exists
 * because it has to. The result of the file transfer will be returned as a {@link FileResource}, once {@link #apply()} has terminated successfully.</p>
 * 
 * <p>NOTE : since the input resource is irrelevant, you may pass any resource, the command won't care.</p>
 * 
 * 
 *  <p>
 *   <strong>Configuration</strong> : a FileResource which entries are comma separated pairs of {@literal <key:value>}
 *    (note that column ':' is the separator)
 * </p>
 *  <div>
 *  Available options :
 *  <ul>
 *  	<li>remotepath : file path name, relative to the user home directory. Mandatory.</li>
 *  	<li>filetype : 'ascii' or 'binary', case insensitive. Optional.</li>
 *  </ul>
 *  </div>
 * 
 *  <div>
 *   <strong>DSL example :</strong>
 *   <pre>EXECUTE get WITH $() ON myftp USING $(remotepath : existingfolder/newfile.txt, filetype : ascii) AS my.downloaded.resource</pre>
 *  </div>
 * 
 * 
 * 
 * @author bsiri
 * @deprecated Since Squash-TA-Framework 1.11 : Use instead {@link FTPGetFileCommand} using built-in {@link org.squashtest.ta.framework.components.VoidResource} as input parameter. 
 */
@Deprecated
@TACommand("get")
public class LegacyFTPGetFileCommand extends AbstractFTPGetFileCommand implements Command<FileResource, FTPTarget> {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(LegacyFTPGetFileCommand.class);
	@Override
	//to assure retrocompatibility
	public void setResource(FileResource resource) {
        LOGGER.warn("DEPRECTATED : Using legacy '{}' with ignored FileResource as input (WITH clause). Please consider using the version using the built-in '{void}' VoidResource as input. ",new ComponentRepresentation(this).toString());
	}
	
	//this may seem useless, but is required for correct command OUTPUT type detection
	@Override
	public FileResource apply() {
		return super.apply();
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.ftp.exceptions.FTPFileDownloadException;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;

public abstract class AbstractFTPCommand {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFTPCommand.class);
	
	protected FTPTarget ftp;

	protected static final String REMOTE_NAME_OPTION = "remotepath";
	protected static final String FILE_TYPE_OPTION = "filetype";
	protected static final String PROPERTY_ROOT = "squashtest.ta.ftp.";
	private static final String ASCII = "ascii";
	
	protected Collection<Resource<?>> configuration = new LinkedList<Resource<?>>();

	/************ Override methods from Command, same for all the FTP classes
        * @param configuration ************/

	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	public void cleanUp() {
		// nothing special
	}

	/************ Other shared methods
        * @return  ************/

	protected Map<String, String> getOptions() {
		Map<String, String> options = null;

		for (Resource<?> resource : configuration) {
			if (FileResource.class.isAssignableFrom(resource.getClass())) {
				File fileR = ((FileResource) resource).getFile();
				options = readConf(fileR);
			}
		}
		if ((options == null) || (!(options.containsKey(REMOTE_NAME_OPTION)))) {
			throw new FTPFileDownloadException("FTP file download : missing configuration. You must specify at least what is the remote file name (option '"
					+ REMOTE_NAME_OPTION + "')");
		}
		return options;
	}

	/**
	 * @param file : the configuration file to read
	 * @return a Map of the file type and path
	 */
	protected Map<String, String> readConf(File file) {
		try {
			return OptionsReader.BASIC_READER.getOptions(file);
		} catch (IOException | IllegalArgumentException ex) {
			throw new FTPFileDownloadException("FTP file download : an error occurred while reading the configuration : ", ex);
		}
	}

	/**
	 * Tests if a path is a directory path or an actual file path
	 * @param fileName : the path to test
	 * @return true if the path given is one of a directory, wrong if it is one of a file
	 */
	protected boolean isDirectory(String fileName) {
		return fileName.endsWith("/");
	}
	
	/**
	 * Return the parametrized filetype
	 * @return in order:
	 * - the file type given in the USING clause
	 * - OR ELSE the file type given in the target
	 * - OR ELSE null
	 */
	protected String getFileType() {
		Map<String, String> options = getOptions();
		String filetype = options.get(FILE_TYPE_OPTION);
		if (filetype == null){
			filetype = ftp.getConfiguration().getProperty(PROPERTY_ROOT+FILE_TYPE_OPTION);
		}
		//before returning it, we put a warning message if the filetype is ASCII (see Issue 1960)
		if (filetype != null && ASCII.equalsIgnoreCase(filetype)){
			
			LOGGER.warn("The FTP transfer mode is set to \"ascii\". If you try to upload or download a binary file, it might be corrupted in the process.");
		}
		return filetype;
	}
        
        /**
         * This method is used in sub-classes to handle input path ending slash
         * @param remoteDirPath
         * @return ending slashed modified remoteDirPath
         */
        protected String removeEndSlash(String remoteDirPath) {
            remoteDirPath = remoteDirPath.substring(0, remoteDirPath.length()-1);

            if (remoteDirPath.endsWith("/")) {
                remoteDirPath = removeEndSlash(remoteDirPath);
            } 

            return remoteDirPath;
        }
}

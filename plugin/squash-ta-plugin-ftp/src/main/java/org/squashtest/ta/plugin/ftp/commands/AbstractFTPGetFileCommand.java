/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import java.io.File;
import java.util.Map;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;

/**
 * Common base class for the new ({@link FTPGetFileCommand}) and legacy 
 * ( {@link LegacyFTPGetFileCommand}) versions of the FTP
 * get command.
 * 
 * @author edegenetais
 * 
 */
@TACommand("get")
public abstract class AbstractFTPGetFileCommand extends AbstractFTPCommand {
	/**
	 * Implementation for the {@link org.squashtest.ta.framework.components.Command#setTarget(org.squashtest.ta.framework.components.Target)} method
         * (only subclasses effectively implement the {@link org.squashtest.ta.framework.components.Command} interface).
         * @param target
	 */
	public void setTarget(FTPTarget target) {
		this.ftp = target;
	}

	/**
	 * Implementation for the {@link org.squashtest.ta.framework.components.Command#apply()} method 
         * (only subclasses effectively implement the {@link org.squashtest.ta.framework.components.Command}
	 * interface).
         * @return 
	 */
	public FileResource apply() {
		Map<String, String> options = getOptions();

		String remotePath = options.get(REMOTE_NAME_OPTION);
		String filetype = getFileType();

		File downloaded;
		if (filetype == null) {
			downloaded = ftp.getFile(remotePath);
		} else {
			downloaded = ftp.getFile(remotePath, filetype);
		}

		return new FileResource(downloaded);
	}
}

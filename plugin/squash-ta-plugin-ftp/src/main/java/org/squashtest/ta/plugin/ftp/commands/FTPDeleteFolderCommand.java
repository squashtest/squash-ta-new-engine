/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import java.util.Map;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.VoidResource;
import static org.squashtest.ta.plugin.ftp.commands.AbstractFTPCommand.REMOTE_NAME_OPTION;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;

/**
 * <p><strong>Description : </strong>That command will delete a remote folder resource from a given ftp. It doesn't need any Resource as input so the DirectoryResource type argument exists
 * because it has to. It doesn't return anything so you can ignore the result of {@link #apply()}.</p>
 * 
 * <p>NOTE : since the input resource is irrelevant, you may pass any resource, the command won't care.</p>
 * 
 * 
 *  <p>
 *  <strong>Configuration</strong> : 
 *  a DirectoryResource which entries are comma separated pairs of {@literal <key:value>}
 *   (note that column ':' is the separator)
 *  </p>
 *  <div>
 *  Available options :
 *  
 *  <ul>
 *  	<li>remotepath : folder path name, relative to the user home directory. Mandatory.</li>
 *  </ul>
 *  </div>
 * 
 *  <div>
 *    <strong>DSL example :</strong>
 *    <pre>EXECUTE deleteFolder WITH {void} ON myftp USING $(remotepath : existingfolder/) AS null</pre>
 *  </div>
 * 
 * 
 * 
 * @author qtran
 *
 */

@TACommand("deleteFolder")
public class FTPDeleteFolderCommand extends AbstractFTPCommand implements Command<VoidResource, FTPTarget> {

        @Override
	public void setResource(VoidResource resource) {
            //As no input Resource is required, this section is empty.
	}

	// this may seem useless, but is required for correct command OUTPUT type detection
	@Override
	public VoidResource apply() {
		Map<String, String> options = getOptions();
                
		String remoteDirPath = options.get(REMOTE_NAME_OPTION);
                
                String failIfNotExistString = options.get("failIfDoesNotExist");
		boolean failIfDoesNotExist = !"false".equalsIgnoreCase(failIfNotExistString);
                
                //replace all \\ by /
                remoteDirPath = remoteDirPath.replace('\\', '/');
                //Remove all // and add 1 to ensure the input path ends with ONLY one slash
                remoteDirPath = (remoteDirPath.endsWith("/")) ? (removeEndSlash(remoteDirPath)+"/")
                        : (remoteDirPath+"/");
		ftp.deleteFolder(remoteDirPath, failIfDoesNotExist);
                return new VoidResource();
	}

        @Override
        public void setTarget(FTPTarget target) {
            this.ftp = target;
        }

}

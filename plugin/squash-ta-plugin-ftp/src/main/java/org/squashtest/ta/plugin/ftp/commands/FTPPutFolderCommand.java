/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import java.util.Map;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.commons.resources.DirectoryResource;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;





/**
 * <p><strong>Description : </strong>That command will put a given folder on the given ftp. It doesn't return anything so you can ignore the result of {@link #apply()}.</p>
 * 
 *  
 *  <p>	<strong>Configuration :</strong> 
 *   a FolderResource which entries are comma separated pairs of {@literal <key:value>}
 *   (note that column ':' is the separator)
 *  </p>
 *  <div>
 *  Available option :
 *  <ul>
 *  	<li>remotepath : folder path name, relative to the user home directory. Mandatory.</li>
 *  </ul>
 *  </div>
 * 
 *  <div>
 *    <strong>DSL example :</strong>
 *    <pre>EXECUTE putFolder WITH myresource ON myftp USING $(remotepath : path/to/uploadedFolder/) AS null</pre>
 *  </div>
 *
 * @author qtran
 *
 */
@TACommand("putFolder")
public class FTPPutFolderCommand extends AbstractFTPCommand implements Command<DirectoryResource, FTPTarget> {
	
	private DirectoryResource resource;
	
	        
	public FTPPutFolderCommand(){
		super();
	}

	@Override
	public void setTarget(FTPTarget target) {
		this.ftp=target;
	}

	@Override
        public void setResource(DirectoryResource resource) {
                this.resource=resource;
        }
        
	@Override
	public VoidResource apply() {
		Map<String, String> options = getOptions();
		
		String remoteDirPath = options.get(REMOTE_NAME_OPTION);
		                
                //a folder upload is called, replace all \\ by /
                remoteDirPath = remoteDirPath.replace('\\', '/');
                //Remove all // and add 1 to ensure the input path ends with ONLY one slash
                remoteDirPath = (remoteDirPath.endsWith("/")) ? (removeEndSlash(remoteDirPath)+"/")
                        : (remoteDirPath+"/");
                
                ftp.putFolder(remoteDirPath, resource.getDirectory().getAbsolutePath(),"");
		return new VoidResource();
	}
    
}

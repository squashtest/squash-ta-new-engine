/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ftp.commands;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.ftp.targets.FTPTarget;


/**
 * <p><strong>Description : </strong>That command will delete a remote resource. It doesn't need any Resource as input so the FileResource type argument exists
 * because it has to. It doesn't return anything so you can ignore the result of {@link #apply()}.</p>
 * 
 * <p>NOTE : since the input resource is irrelevant, you may pass any resource, the command won't care.</p>
 * <p>NOTE (again) : as for now the deletion of a directory is NOT recursive. If the file to remove is a directory and the said directory is not empty 
 * an exception will be raised.</p>
 * 
 * 
 *  <p>
 *  <strong>Configuration</strong> : 
 *  a FileResource which entries are comma separated pairs of {@literal <key:value>}
 *  (note that column ':' is the separator)
 *  </p>
 *  <div>
 *  Available options : 
 *  <ul>
 *  	<li>remotepath : file path name to remove, relative to the user home directory. If it ends with a '/' it will be assumed to be a directory, 
 *  		otherwise it will be treated as a file. Mandatory.</li>
 *  </ul>
 *  </div>
 * 
 *  <div>
 *     <strong>DSL example :</strong>
 *     <pre>EXECUTE delete WITH $() ON myftp USING $(remotepath : existingfolder/) AS null</pre>
 *  </div>
 *  <div>
 *     <strong>DSL example :</strong>
 *     <pre>EXECUTE delete WITH $() ON myftp USING $(remotepath : existingfile) AS null</pre>
 *  </div>
 * 
 * 
 * @author bsiri
 *
 */

@TACommand("delete")
public class FTPDeleteCommand extends AbstractFTPDeleteCommand implements Command<VoidResource, FTPTarget> {
	
	public FTPDeleteCommand(){
		super();
	}

	@Override
	public void setResource(VoidResource resource) {
            //noop
	}
	
	//this may seem useless, but is required for correct command OUTPUT type detection
	@Override
	public VoidResource apply() {
		return super.apply();
	}
}

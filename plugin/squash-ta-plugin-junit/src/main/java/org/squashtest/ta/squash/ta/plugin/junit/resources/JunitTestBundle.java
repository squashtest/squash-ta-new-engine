/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.resources;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.UUID;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle;
import org.squashtest.ta.plugin.commons.resources.InitSilencingLoggerFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.JunitBundleContext;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Bundle;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.ContextException;

/**
 * Resource type used by Junit5 related components.
 * @author edegenetais
 */
@TAResource("script.junit5")
public class JunitTestBundle implements Resource<JunitTestBundle>, Bundle {
    private static final org.slf4j.Logger LOGGER = InitSilencingLoggerFactory.getLogger(JunitTestBundle.class);
    
    private static final String JUNIT_5_CONTEXT_IMPL_CLASS_NAME = "org.squashtest.ta.squash.ta.plugin.junit.library.context.Junit5ContextImpl";

    private final String uuid = UUID.randomUUID().toString();

    //Until further notice, this resource type will be a delegate, not a subtype.
    private JavaCodeBundle codeBundle;

    public JunitTestBundle() {/*noarg constructor for Spring*/}

    public JunitTestBundle(JavaCodeBundle codeBundle) {
        this.codeBundle = codeBundle.copy();
    }

    @Override
    public Set<String> getBundleClasses() {
        return this.codeBundle.getBundleClassNames();
    }

    @Override
    public ClassLoader getDedicatedClassLoader() {
        return new JunitClassLoader(this.uuid, codeBundle.getDedicatedClassloader());
    }

    @Override
    public JunitTestBundle copy() {
        /** We don't copy our resource here, since the constructor we use does it. */
        return new JunitTestBundle(codeBundle);
    }

    @Override
    public void cleanUp() {
        codeBundle.cleanUp();
    }

    public JunitBundleContext getContext() {
        try {
            final Class<JunitBundleContext> contextImplClass = (Class<JunitBundleContext>)getDedicatedClassLoader().loadClass(JUNIT_5_CONTEXT_IMPL_CLASS_NAME);
            Constructor<JunitBundleContext> constructor=contextImplClass.getConstructor(Bundle.class);
            return constructor.newInstance(this);
        } catch (NoSuchMethodException      |
                 SecurityException          |
                 InvocationTargetException  |
                 InstantiationException     |
                 ClassNotFoundException     |
                 IllegalAccessException     |
                /*
                 * this might seem strange, but as we are performing ClassPath extending at runtime, 
                 * this particular Error may result from a runtime error and should not kill the whole engine.
                 */
                 NoClassDefFoundError 
                ex) {
            throw new ContextException("Failed to bootstrap junit 5 bundle context", ex);
        }
    }

    @Override
    public String getId() {
        return uuid;
    }

    public boolean hasThisClassInBundle(String className) {
        
        try {
            codeBundle.getDedicatedClassloader().loadClass(className);
        } catch (ClassNotFoundException ex) {
            LOGGER.debug("Woops ! Could not find {}", className, ex);
            return false;
        }
        return true;
    }

}

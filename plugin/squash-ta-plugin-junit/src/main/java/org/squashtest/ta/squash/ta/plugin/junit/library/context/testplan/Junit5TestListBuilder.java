/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import org.junit.platform.engine.TestSource;
import org.junit.platform.engine.support.descriptor.ClassSource;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;
import org.squashtest.ta.squash.ta.plugin.junit.library.context.JunitTestDescriptorImpl;

/**
 * This implementation of {@link Junit5TestPlanVisitorListener}
 * builds a list of available {@link TestDescriptor}s as the 
 * {@link Junit5TestPlanVisitor} do its visit. 
 * 
 * @author fgautier
 */
public class Junit5TestListBuilder implements Junit5TestPlanVisitorListener {

    /**
     * Class related messages logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Junit5TestListBuilder.class);
    
    /**
     * Holds the visited {@link TestPointer}s. 
     */
    private List<TestDescriptor> visitedTests;
    
    /**
     * Holds a stack of {@link org.junit.platform.engine.TestDescriptor.Type#CONTAINER} {@link TestIdentifier}
     * to retain informations about the context of a {@link org.junit.platform.engine.TestDescriptor.Type#TEST} visited
     * {@link TestIdentifier}.
     * 
     * Indeed keeping only the current container of a visited test might not suffice as in some cases the lasse name of the 
     * test method is strored in its grand-parent's {@link org.junit.platform.engine.support.descriptor.ClassSource}, 
     * 
     * c.f. open JUnit issue #737 'Provide reliable way to get class (class name) of executed test method' https://github.com/junit-team/junit5/issues/737
     */
    private Deque<TestIdentifier> elders; 
    
   
    @Override
    public void notifyVisitStart(TestPlan plan) {
        LOGGER.debug("Starting testlist build from test plan {}", plan);
        visitedTests = new ArrayList<>();
        elders=new ArrayDeque<>();
    }
    
    @Override
    public void notifyContainer(TestIdentifier container) { 
        LOGGER.debug("Storing current container {}", container.getDisplayName());
        elders.push(container);
    }

    @Override
    public void notifyContainerEnd(TestIdentifier container) {
        if (!elders.isEmpty()) {
            TestIdentifier currentParent = elders.pop();
            if (!currentParent.equals(container)) {
                throw new IllegalStateException("Current Stack of elders is unbalanced. Trying to pop '" + container.getUniqueId() + "' while current parent is '" + currentParent.getUniqueId() + "'.");
            }
            LOGGER.debug("End of container {}. poping it out of the elder stack", currentParent.getUniqueId());
        } else {
            throw new IllegalStateException("Current Stack of elders is unbalanced. Trying to pop '" + container.getUniqueId() + "' out of an emty stack");
        }
    }

    @Override
    public void notifyTest(TestIdentifier test) {
        LOGGER.debug("Treating test {}", test.getDisplayName());
        String testMethodName = test.getDisplayName();
        
        String qualifiedClassName = findClassName(test);
        
        TestDescriptor testPointer = new JunitTestDescriptorImpl(qualifiedClassName, testMethodName, test.getUniqueId());
        visitedTests.add(testPointer);
    }
    
    @Override
    public void notifyHybridTestAndContainer(TestIdentifier identifier) {
        throw new UnsupportedOperationException("let me think about it..."); 
    }

    @Override
    public void notifyVisitEnd() {
        if(LOGGER.isDebugEnabled()) {
            StringBuilder messageBuilder = new StringBuilder("TestList construction is finished. Found tests:\n");
            for (TestDescriptor visitedTest : visitedTests) {
                messageBuilder.append("\tTest ")
                        .append(visitedTest.displayName())
                        .append("' from class ")
                        .append(visitedTest.className())
                        .append("\n");
            }
            LOGGER.debug(messageBuilder.toString());
        }
    }
    
    /**
     * Get the visited tests as a list of {@link TestDescriptor}s once a visit is complete.
     * 
     * @return an unmodifiable list of visited tests.
     */
    public List<TestDescriptor> getVisitedTests() {
        return Collections.unmodifiableList(visitedTests);
    }

    public void serializeListToFile(File outputFile) {
        // we don't use that event, so we silently ignore it. Any question ?
    }

    private String findClassName(TestIdentifier test) {
        String className = null;
        ArrayDeque<TestIdentifier> workingStorage = new ArrayDeque<>();

        while (className == null && !elders.isEmpty()) {
            TestIdentifier currentParrent = elders.pop();
            workingStorage.push(currentParrent);
            Optional<TestSource> parentSource = currentParrent.getSource();
            if (parentSource.isPresent()) {
                try {
                    ClassSource clazzSource = (ClassSource) parentSource.get();
                    Class<?> clazz = clazzSource.getJavaClass();
                    className = clazz.getCanonicalName();
                } catch (ClassCastException cce) {
                    if(LOGGER.isTraceEnabled()){//To give a chance at seeing this without cluttering logs, we'll only display the cause exception cce in trace level. 
                        LOGGER.trace("Current ancester '" + currentParrent.getDisplayName() + "' has no ClassSource as TestSource, going deeper...", cce);
                    }else{
                        LOGGER.debug("Current ancester '" + currentParrent.getDisplayName() + "' has no ClassSource as TestSource, going deeper...", cce.getMessage());
                    }
                }
            }
        }

        if (className == null) {
            throw new IllegalStateException("Stack is empty. Impossible to find class for test '" + test.getUniqueId() + "'. Please ensure that you classe notifyContainer first.");
        }

        while (!workingStorage.isEmpty()) {
            elders.push(workingStorage.pop());
        }

        return className;
    }
}

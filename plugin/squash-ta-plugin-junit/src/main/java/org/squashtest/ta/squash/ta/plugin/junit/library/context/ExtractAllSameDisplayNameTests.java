/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context;

import java.util.ArrayList;
import java.util.List;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;
import org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan.Junit5TestDisplayNameFilter;
import org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan.Junit5TestPlanVisitor;

/**
 *
 * @author edegenetais
 */
class ExtractAllSameDisplayNameTests implements Junit5ContextImpl.Operation<TestDescriptor, List<TestDescriptor>> {

    @Override
    public List<TestDescriptor> perform(TestDescriptor criterion) {
        // 1 - Request available tests for this (qualified) class
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request().selectors(DiscoverySelectors.selectClass(criterion.className())).build();
        // 2 - Explore
        Launcher explorer = LauncherFactory.create();
        TestPlan plan = explorer.discover(request);
        // 3 - Retain only test, the display name of which is the one specified. I love it, when a plan comes together !
        final Junit5TestDisplayNameFilter filter = new Junit5TestDisplayNameFilter(criterion.displayName());
        Junit5TestPlanVisitor planVisitor = new Junit5TestPlanVisitor(filter);
        planVisitor.visit(plan);
        // 4 - Translate for the outside world.
        final ArrayList<TestDescriptor> result = new ArrayList<>(filter.getFilteredTest().size());
        for (TestIdentifier testId : filter.getFilteredTest()) {
            result.add(new JunitTestDescriptorImpl(criterion.className(), criterion.displayName(), testId.getUniqueId()));
        }
        return result;
    }

}

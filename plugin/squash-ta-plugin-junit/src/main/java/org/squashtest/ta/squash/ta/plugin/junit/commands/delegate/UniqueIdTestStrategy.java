/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.commands.delegate;

import java.util.List;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.squash.ta.plugin.junit.commands.ExecuteJunit5Bundle;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.JunitBundleContext;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;

/**
 * This {@link ExecutionStrategy} implementation accepts and execute Junit Test
 * base on a configuration providing the full Junit Unique Id of the test.
 *
 * the configuration resource should be an option formatted {@link org.squashtest.ta.framework.components.FileResource}
 * containing the following key : <b>uniqueId</b>.
 *
 * @author fgautier
 */
public class UniqueIdTestStrategy extends ExecutionStrategy {

    /**
     * Key pointing to Junit unique ID value in a the corresponding command.
     * 
     * TODO A proper decision where to put the const keys should be made.
     */
    public static final String UNIQUE_ID = "uniqueId";

    /**
     * Std ctor. 
     * 
     * @param client : The client using this strategy. Used mostly to parametrise logs.
     */
    UniqueIdTestStrategy(ExecuteJunit5Bundle client) {
        super(client);
    }

    @Override
    public boolean accept(List<Resource<?>> configuration) {
        final String targetTestUID = confHelper().extractOptionValue(configuration, UNIQUE_ID);
        return targetTestUID != null;
    }

    @Override
    public Result executeImpl(List<Resource<?>> configuration, JunitBundleContext context) {
        final String targetTestUID = confHelper().extractOptionValue(configuration, UNIQUE_ID);
        return launchSingleTargetTest(context, targetTestUID);
    }

    /**
     * Helper method launching a single Junit Test based on its Unique Id.
     *
     * @param context : Where to find the test.
     * @param targetTestUID : The Unique Id of the test to execute.
     *
     * @return the {@link Result} of the execution.
     */
    protected Result launchSingleTargetTest(JunitBundleContext context, final String targetTestUID) {
        Result r = context.launch(targetTestUID);
        if (r.getTestStatus().isError()) {
            throw new InstructionRuntimeException("Junit test execution for " + targetTestUID + " failed on error : " + r.getCaughtError().getMessage(), r.getCaughtError());
        }
        return r;
    }
}

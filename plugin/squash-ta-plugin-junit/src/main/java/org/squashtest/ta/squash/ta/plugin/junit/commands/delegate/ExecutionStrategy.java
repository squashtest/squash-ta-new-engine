/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.commands.delegate;

import java.util.List;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.squash.ta.plugin.junit.commands.ExecuteJunit5Bundle;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.JunitBundleContext;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;

/**
 * This helper class execute Junit5 Tests based on a configuration. A
 * configuration is a {@link List} of {@link  Resource}.
 *
 * Each implementation should declare weather or not they accept a
 * configuration. And if so, execute the desired tests based on the
 * configuration.
 *
 * @author fgautier
 * 
 * The package visibility of the constructor is intended. As only the factory 
 * should be able to buuild such things.
 */
public abstract class ExecutionStrategy {

    /**
     * This helper is used to extract the value of a option file key in a
     * collection of configuration resources.
     */
    private final ConfigurationExtractor confHelper;
    
    /**
     * Standard constructor with package visiblity. 
     * 
     * @param client : The client using this strategy. Used mostly to parametrise logs.
     */
    ExecutionStrategy(ExecuteJunit5Bundle client) {
        confHelper = new ConfigurationExtractor(client);
    }

    /**
     * Does the {@link ExecutionStrategy} accept the configuration ?
     *
     * @param configuration : The collection of resources to crawl.
     *
     * @return true if configuration fits, false otherwise.
     */
    public abstract boolean accept(List<Resource<?>> configuration);

    /**
     * Execute the desired tests (in the provided context) based on the
     * configuration.
     *
     * @param configuration : the collecion of resources where to find the
     * definition of the test to execute.
     * @param context : where to find the implemented Junit test.
     *
     * @return the {@link Result} of the execution.
     */
    public Result execute(List<Resource<?>> configuration, JunitBundleContext context) {
        if (!accept(configuration)) {
            throw new InstructionRuntimeException("Internal error : " + this.getClass().getSimpleName()
                    + " cannot execute tests with configuration " + configuration.toString()
                    + ". Please ensure to call accept first.");
        }
        return executeImpl(configuration, context);
    }

    /**
     * This is the actual behavior of execution held by the
     * {@link ExecutionStrategy}.
     *
     * @param configuration : the collecion of resources where to find the
     * definition of the test to execute.
     * 
     * @param context : where to find the implemented Junit test.
     * 
     * @return the {@link Result} of the execution.
     */
    protected abstract Result executeImpl(List<Resource<?>> configuration, JunitBundleContext context);

    /**
     * Get the  helper that is used to extract the value of a option file key in a
     * collection of configuration resources.
     * 
     * @return the helper
     */
    protected ConfigurationExtractor confHelper() {
        return confHelper;
    }
}

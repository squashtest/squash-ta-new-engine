/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.JunitBundleContext;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Bundle;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.ContextException;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;

/**
 * This class is intended to be re-loaded from the bundle to bootstrap
 * a new classloading branch with the bundle's classpath.
 * @author edegenetais
 */
public class Junit5ContextImpl implements JunitBundleContext{

    private static final Logger LOGGER = LoggerFactory.getLogger(Junit5ContextImpl.class);
    
    private Bundle bundle;

    public Junit5ContextImpl(Bundle bundle) {
        LOGGER.debug("Creating context for bundle {}",bundle.getId());
        this.bundle = bundle;
    }
    
    @Override
    public Result launch(String uniqueId) {
        return doInClassloadingFirewall(new Junit5UniqueIdExecution(), uniqueId);
    }
    
    @Override
    public Result launch(List<String> uniqueIds) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TestDescriptor> listAvailableTests() {
        return doInClassloadingFirewall(new ListAvailableJunit5Tests(bundle));
    }

    @Override
    public List<TestDescriptor> getTestsOfMethod(TestDescriptor criterion) {
        return doInClassloadingFirewall(new ExtractAllSameDisplayNameTests(), criterion);
    }
    
    /**
     * This method is used to run code inside the classloading firewall, with the
     * bundle classpath.
     * @param <InputType> type of the input parameter of the operation.
     * @param <ReturnType> type of the result of the operation.
     * @param operation the code to run, packaged in a functrional interface.
     * @param input the input parameter for the code to run.
     * @return the operation result.
     * @see #doInClassloadingFirewall(org.squashtest.ta.squash.ta.plugin.junit.library.context.Junit5ContextImpl.Operation) 
     * @see Junit5ContextImpl.Operation
     */
    protected <InputType,ReturnType> ReturnType doInClassloadingFirewall(Operation<InputType,ReturnType> operation, InputType input){
        ClassLoader contextCL=Thread.currentThread().getContextClassLoader();
        try{
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            LOGGER.trace("Thread switched to bundle {} classloading context.",bundle.getId());
            
            return operation.perform(input);
            
        }catch(NoClassDefFoundError e){
            throw new ContextException("Bundle "+bundle.getId()+"failed to load class",e);
        }finally{
            Thread.currentThread().setContextClassLoader(contextCL);
            LOGGER.trace("Thread switched back from bundle {} context to framework classloading context.",bundle.getId());
        }
    }
    /**
     * This method is used to run code inside the classloading firewall, with the
     * bundle classpath. This overloaded version is specialized for operations without parameters.
     * @param <ReturnType> the return type of the operation.
     * @param operation the code to run, packaged in a functional interface.
     * @return the operation result.
     * @see #doInClassloadingFirewall(org.squashtest.ta.squash.ta.plugin.junit.library.context.Junit5ContextImpl.Operation, java.lang.Object) 
     * @see Junit5ContextImpl.Operation
     */
    protected <ReturnType> ReturnType doInClassloadingFirewall(Operation<Void,ReturnType> operation){
        return doInClassloadingFirewall(operation, null);
    }

    /**
     * Fcuntional interface to package code for execution inside the classloading firewall,
     * with the bundle's classpath.
     * @param <InputType> type of the operation's input parameter.
     * @param <ReturnType> type of the operation's retrun parameter.
     */
    protected interface Operation<InputType,ReturnType>{
        ReturnType perform(InputType t);
    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.resources;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;

/**
 * This resource holds the result of any Junit5 command.
 * @author edegenetais
 */
@TAResource("result.junit5")
public class JunitResult implements Resource<JunitResult>{
    private Result executionData;

    public JunitResult(){/* This default constructor exists only to make Spring happy. */}
    
    public JunitResult(Result junit5Result) {
        this.executionData = junit5Result;
    }

    public Result getExecutionData() {
        return executionData;
    }

    @Override
    public JunitResult copy() {
        return new JunitResult(executionData);
    }

    @Override
    public void cleanUp() {
        //noop
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan;

import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;

/**
 * This Interface receives events from a {@link Junit5TestPlanVisitor} as it visits 
 * a TestPlan. 
 * 
 * Each implementation should handle three kind of events : 
 * <ul>
 *  <li>{@link org.junit.platform.engine.TestDescriptor.Type#CONTAINER} node visit related event</li>
 *  <li>{@link org.junit.platform.engine.TestDescriptor.Type#TEST} node visit related event</li>
 *  <li>{@link org.junit.platform.engine.TestDescriptor.Type#CONTAINER_AND_TEST} node visit related event</li>
 * </ul>
 * 
 * @author fgautier
 */
public interface Junit5TestPlanVisitorListener {
    
    /**
     * This event signals the visit start of the given {@link TestPlan}. 
     * 
     * @param plan the visited plan.
     */
    void notifyVisitStart(TestPlan plan);
    
    /**
     * This event signals the visit of a {@link org.junit.platform.engine.TestDescriptor.Type#CONTAINER} node. 
     * 
     * @param container the visied node.
     */
    void notifyContainer(TestIdentifier container);
    
    /**
     * This event signals the visit of a {@link org.junit.platform.engine.TestDescriptor.Type#CONTAINER} node. 
     * 
     * @param container the visied node.
     */
    void notifyContainerEnd(TestIdentifier container);
    
    /**
     * This event signals the visit of a {@link org.junit.platform.engine.TestDescriptor.Type#TEST} node. 
     * 
     * @param test the visied node.
     */
    void notifyTest(TestIdentifier test);
    
    /**
     * This event signals the visit of a {@link org.junit.platform.engine.TestDescriptor.Type#CONTAINER_AND_TEST} node. 
     * 
     * @param hybridIdentifier the visied node.
     */
    void notifyHybridTestAndContainer(TestIdentifier hybridIdentifier);

    /**
     * This event signals the end of the visit. 
     */ 
    void notifyVisitEnd();
}

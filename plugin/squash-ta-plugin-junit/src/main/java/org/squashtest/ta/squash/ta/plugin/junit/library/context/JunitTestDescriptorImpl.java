/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;

/**
 *
 * @author edegenetais
 */
public class JunitTestDescriptorImpl implements TestDescriptor{
    private String className;
    private String displayName;
    private String uniqueId;
    private List<Map<String, List<String>>> metadataList = new LinkedList<>();

    public JunitTestDescriptorImpl(String className, String displayName, String uniqueId) {
        this.className = className;
        this.displayName = displayName;
        this.uniqueId = uniqueId;
    }

    public JunitTestDescriptorImpl(String className, String displayName, String uniqueId, List<Map<String, List<String>>> methodMetadataList) {
        this.className = className;
        this.displayName = displayName;
        this.uniqueId = uniqueId;
        this.metadataList = methodMetadataList;
    }

    @Override
    public String className() {
        return className;
    }

    @Override
    public String displayName() {
        return displayName;
    }

    @Override
    public String uniqueId() {
        return uniqueId;
    }

    @Override
    public List<Map<String, List<String>>> metadataList() {
        return metadataList;
    }
    
}

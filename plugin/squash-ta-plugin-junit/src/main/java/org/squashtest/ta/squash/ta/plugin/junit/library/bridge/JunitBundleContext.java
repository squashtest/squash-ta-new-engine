/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.bridge;

import java.util.List;

/**
 * This class defines the interface of a Junit bundle context that wraps the junit5
 * tests and engine in a classloading firewall to allow dynamic adaptation
 * to the project's configuration.
 * 
 * @author ericdegenetais
 */
public interface JunitBundleContext {
    /**
     * List all available tests in this bundle.
     * @return the discovered tests.
     */
    List<TestDescriptor> listAvailableTests();
    /**
     * List all unique tests sharing a qualified method name.
     * @param criterion this descriptor defines the qualified test name which will be looked up.
     * If a {@link TestDescriptor#uniqueId()} is defined, it will be ignored.
     * @return the list of all tests that are defined by the same display name in the same method.
     */
    List<TestDescriptor> getTestsOfMethod(TestDescriptor criterion);
    /**
     * Launch a test specified by its uniqueId.
     * @param uniqueId the id.
     * @return the result of the test run.
     */
    Result launch(String uniqueId);
    
    /**
     * Launch a series of tests specified by their respetive uniqueIds.
     * @param uniqueIds the list of ids.
     * @return the result of the test run.
     */
    Result launch(List<String> uniqueIds);
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.resources;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.squashtest.ta.local.process.library.process.ClasspathException;
import org.squashtest.ta.local.process.library.process.ClasspathManager;
import org.squashtest.ta.plugin.commons.library.InfrastructureAwareClassLoader;
import org.squashtest.ta.plugin.commons.resources.FrameworkCLInfrastructureAwareTuning;
import org.squashtest.ta.plugin.commons.resources.SKFEngineContextCLInfrastructureAwareTuning;
import org.squashtest.ta.plugin.commons.resources.InitSilencingLoggerFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.ContextException;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.JunitBundleContext;

/**
 *
 * @author edegenetais
 */
class JunitClassLoader extends InfrastructureAwareClassLoader {

    private static final Logger LOGGER = InitSilencingLoggerFactory.getLogger(JunitClassLoader.class);
    
    private static final String JUNIT_LAUNCHER_CLASS_NAME = "org.junit.platform.launcher.core.LauncherFactory";
    private static final String MSG_FALLBACK_ON_BUNDLE_CP = "JunitClassLoader : Loading {} from the bundle CP as I didn't find it myself!";
    private Set<FrameworkCLInfrastructureAwareTuning> islandTunerStrategies=new HashSet<>();
 
    private String bundleUuid;
    private final ClassLoader bundleLoader;
    
    public JunitClassLoader(String bundleUuid, ClassLoader bundleLoader) {
        super(computeClasspathURLset(bundleLoader,
                "org.junit.platform.launcher.TestExecutionListener",
                "org.junit.platform.engine.EngineDiscoveryRequest",
                "org.junit.platform.commons.util.PreconditionViolationException",JUNIT_LAUNCHER_CLASS_NAME) 
        );

        this.bundleUuid=bundleUuid;
        this.bundleLoader=bundleLoader;
        islandTunerStrategies.add(new SKFEngineContextCLInfrastructureAwareTuning());
        islandTunerStrategies.add(new CucumberUnderSpringBootCLIslandStrategy(this));
    }

    public String bundleUuid(){
        return bundleUuid;
    }
    
    private static final URL[] computeClasspathURLset(ClassLoader bundleLoader,String... dependencyClassNames) {
      try {
        InitSilencingLoggerFactory.switchOnInitPhase();
        final Set<URL> classCProotURLSet = new HashSet<>();
        classCProotURLSet.add(getClassCProotURL(JunitTestBundle.class));
        final ClassLoader frameworkCL = JunitClassLoader.class.getClassLoader();
        
        for(String dependencyClassName:dependencyClassNames){
            addDefaultDependencyFromFrameworkCP(bundleLoader, dependencyClassName, frameworkCL, classCProotURLSet);
        }
        
        addDefaultEngineJarsIfNeeded(bundleLoader, classCProotURLSet, frameworkCL);
        
        return classCProotURLSet.toArray(new URL[classCProotURLSet.size()]);
      } finally {
        InitSilencingLoggerFactory.switchOffInitPhase();
      }
    }
    
    @Override
    public void loggerLoadClass(String className, ClassNotFoundException e) {
        try {
        InitSilencingLoggerFactory.switchOnInitPhase();
            if (LOGGER.isTraceEnabled()) {
                LOGGER.error("JunitClassLoader: Woops ! Could not find {}", className, e);
            } else {
                LOGGER.error("JunitClassLoader: Woops ! Could not find {}. For full stack trace, please switch to TRACE mode.", className);
            }
        } finally {
            InitSilencingLoggerFactory.switchOffInitPhase();
        }
    }

    @Override
    public void loggerFindClass(String className) {
        LOGGER.trace("JunitClassLoader: Looking up {}", className);
    }
    
    @Override
    public void loggerFindClassNotFound(String className, ClassNotFoundException e) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.debug("JunitClassLoader: NoclassDefFoundException while looking for {}", className, e);
        } else {
            LOGGER.debug("JunitClassLoader: NoclassDefFoundException while looking for {}. For full stack trace, please switch to TRACE mode.", className);
        }
    }
        
    @Override
    public void loggerFrameworkInfrastructureAwareLoadingStrategyLoadingClass(String className) {
        LOGGER.debug("JunitClassLoader : Trying to load {}", className);
    }

    @Override
    public void loggerFrameworkInfrastructureAwareLoadingStrategyIsInfrastructure(String className) {
        LOGGER.debug("JunitClassLoader : Loading {} from the framework classloader as it is from the bridge package or infrastructure.", className);
    }
    
    @Override
    public void loggerFrameworkInfrastructureAwareLoadingStrategySuccessfullyLoaded(String className) {
        LOGGER.trace("JunitClassLoader : Successfully loaded {}", className);
    }
    
    
    @Override
    public void loggerGetResourceInit(String name) {
        LOGGER.debug("JunitClassLoader : Getting resource URL for {}",name);
    }

    @Override
    public void loggerGetResourceFound(String name) {
        LOGGER.trace("JunitClassLoader : Found resource {} at {}",name);
    } 
    
    @Override
    public void loggerGetResourceAsStreamInit(String name) {
        LOGGER.trace("JunitClassLoader : Opening input stream from resource {}", name);
    }

    @Override
    public void loggerGetResourceAsStreamFailure(String name, IOException ex) {
        LOGGER.warn("JunitClassLoader : Failed to open resource Stream for {}", name, ex);
    }

        @Override
    public void loggerGetResourcesInit(String name) {
        LOGGER.debug("JunitClassLoader : Getting all resources matching name {}", name);
    }

    @Override
    public void loggerGetResourcesFound(List<URL> resourceURLs) {
        LOGGER.trace("JunitClassLoader : Found {} matching resources :\n{}", resourceURLs.size(), resourceURLs);
    }

    private static void addDefaultEngineJarsIfNeeded(ClassLoader bundleLoader1, final Set<URL> classCProotURLSet, final ClassLoader frameworkCL) {
        ClassLoader contextCL=Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(bundleLoader1);
            Class<?> launcherFactoryClass = getLauncherFactoryClassForEngineTest(bundleLoader1, classCProotURLSet);
            Method createMethod=launcherFactoryClass.getMethod("create");
            createMethod.invoke(null);
        }catch(ClassNotFoundException e){
            addDefaultEngineJars(classCProotURLSet, frameworkCL, e);
        }catch(
                NoSuchMethodException   |
                        IllegalAccessException  |
                        InvocationTargetException
                        e){
            LOGGER.warn("JunitClassLoader: Bundle junit5 not functional, trying to fix it by adding default engine jars.",e);
            addDefaultEngineJars(classCProotURLSet, frameworkCL, e);
        }finally{
            Thread.currentThread().setContextClassLoader(contextCL);
        }
    }

    private static Class<?> getLauncherFactoryClassForEngineTest(ClassLoader bundleLoader1, final Set<URL> classCProotURLSet) throws ClassNotFoundException {
        Class<?> launcherFactoryClass;
        try{
            launcherFactoryClass = bundleLoader1.loadClass(JUNIT_LAUNCHER_CLASS_NAME);
        }catch(ClassNotFoundException e){
            if(LOGGER.isTraceEnabled()){//add the exception in trace mode, not in debug mode
                LOGGER.debug("JunitClassLoader: LauncherFactory or dependencies not found in bundle, trying with additional classpath for default dependencies.",e);
            }else{
                LOGGER.debug("JunitClassLoader: LauncherFactory or dependencies not found in bundle, trying with additional classpath for default dependencies. Set this logger to level trace to get the stacktrace.");
            }
            launcherFactoryClass = new URLClassLoader(classCProotURLSet.toArray(new URL[classCProotURLSet.size()]), bundleLoader1).loadClass(JUNIT_LAUNCHER_CLASS_NAME);
        }
        return launcherFactoryClass;
    }
    
    private static void addDefaultEngineJars(final Set<URL> classCProotURLSet, final ClassLoader frameworkCL, Exception e) {
        LOGGER.debug("JunitClassLoader: Junit launcher core not in bundle classpath, we'll bet it's a junit4 context and add the default vintage engine to classpath");
        try{
            classCProotURLSet.add(getClassCProotURL(frameworkCL.loadClass("org.junit.vintage.engine.VintageTestEngine")));
            classCProotURLSet.add(getClassCProotURL(frameworkCL.loadClass("org.opentest4j.MultipleFailuresError")));
        }catch(ClassNotFoundException dtce){
            dtce.addSuppressed(e);
            throw new ContextException("JunitClassLoader: Failed to inject engine classpath roots from the framework", dtce);
        }
    }

    private static void addDefaultDependencyFromFrameworkCP(ClassLoader bundleLoader1, String dependencyClassName, final ClassLoader frameworkCL, final Set<URL> classCProotURLSet) throws ContextException {
        try {
            bundleLoader1.loadClass(dependencyClassName);
        }catch (ClassNotFoundException ex) {
            LOGGER.debug("JunitClassLoader: Dependency class {} not found in bundle, trying to get it from plugin classpath.",dependencyClassName,ex);
            try {
                Class<?> dependencyClass=frameworkCL.loadClass(dependencyClassName);
                final URL dependencyClassCProotURL = getClassCProotURL(dependencyClass);
                classCProotURLSet.add(dependencyClassCProotURL);
                LOGGER.trace("JunitClassLoader: Got {} from {}, adding it to the junit5 context classpath",dependencyClassName,dependencyClassCProotURL);
            } catch (ClassNotFoundException finalCLfailure) {
                finalCLfailure.addSuppressed(ex);
                throw new ContextException("JunitClassLoader: Dependency class "+dependencyClassName+" not found ! There's something rotten in the realm of Denmark!", finalCLfailure);
            }
        }
    }

    private static URL getClassCProotURL(final Class<?> targetClass) {
        try {
            return new ClasspathManager().getClassCProotURL(targetClass);
        } catch (ClasspathException ex) {
            throw new ContextException("JunitClassLoader: Failed to recover the plugin jar URL.",ex);
        }
    }
        
     @Override   
     /**
     * To avoid schizoïd snafus, we exclude some 'infrastructure' classes from classloading isolation.
     * 
     * @param className the name of the target class.
     * @return <code>true</code> if it is infrastructure (classes for which we want classloading to remain in order to ensure consistent
     * communication with the outer (engine) world), <code>false</code> for any other class.
     * )
     */
    public boolean isInfrastructureClass(String className) {
        String[] infrastructureClassPrefixes=new String[]{
            JunitBundleContext.class.getPackage().getName(),//this must be common to both worlds for linkage consistency
            "java.lang",                                    //redefining Java is bad !
            "org.slf4j"                                     //logging should'nt be split-brain!
        };
        for(String prefix:infrastructureClassPrefixes){
            if(className.startsWith(prefix)){
                return true;
            }
        }
        //We will for now suppose that all referenced strategies apply - so that if even one tags a class as infrastructure we follow it.
        for(FrameworkCLInfrastructureAwareTuning tuner:islandTunerStrategies){
            if(tuner.isInfrastructure(className)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public Class<?> isNotInfrastructureClassLoading(String className) throws ClassNotFoundException {
        LOGGER.debug("JunitClassLoader: Loading {} myself as it is not bridge-related.", className);
        Class<?> theClass;
        try {
            theClass = super.urlClassLoaderLoadClass(className);

        } catch (ClassNotFoundException cnfe) {

            if (LOGGER.isTraceEnabled()) {//give a chance to see the stacktrace, but avoid cluttering debug mode logs
                LOGGER.trace(MSG_FALLBACK_ON_BUNDLE_CP, className, cnfe);
            } else {
                LOGGER.debug(MSG_FALLBACK_ON_BUNDLE_CP, className);
            }
            theClass = this.bundleLoader.loadClass(className);
        }
        return theClass;
    }
    
    @Override    
    public List<URL> addResourceURLs(String name) throws IOException {
        List<URL> resourceURLs = new ArrayList<>();

        resourceURLs.addAll(Collections.list(super.urlClassLoaderFindResourcse(name)));
        resourceURLs.addAll(Collections.list(bundleLoader.getResources(name)));
        
        return resourceURLs;
    }

    @Override
    public URL urlResourceIsNullStrategy (String name) {
        LOGGER.trace("JunitClassLoader: Not found in my base package, looking in bundle");
        return bundleLoader.getResource(name);
    }



}

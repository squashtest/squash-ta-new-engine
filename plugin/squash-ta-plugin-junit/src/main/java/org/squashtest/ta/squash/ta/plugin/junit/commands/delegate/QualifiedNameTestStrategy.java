/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.commands.delegate;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.squash.ta.plugin.junit.commands.ExecuteJunit5Bundle;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.JunitBundleContext;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;
import org.squashtest.ta.squash.ta.plugin.junit.library.context.JunitTestDescriptorImpl;

/**
 * This {@link ExecutionStrategy} implementation accepts and execute Junit Test
 * base on a configuration providing the qualified display name of the test to
 * execute.
 *
 * the configuration resource should be an option formatted {@link org.squashtest.ta.framework.components.FileResource}
 * containing the following keys :
 * <ul>
 * <li>qualifiedClassName</li>
 * <li>displayName</li>
 * </ul>
 *
 * Both these keys are mandatory.
 * 
 * @author fgautier
 */
public class QualifiedNameTestStrategy extends ExecutionStrategy {

    /**
     * Logger used to log class related messages. 
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(QualifiedNameTestStrategy.class);
    
    /**
     * key used to specify the qualified class name of the class where the Junit test(s) is implemented.
     */
    private static final String QUALIFIED_CLASS = "qualifiedClassName";
    /**
     * Key used to specify the display name of the Junit test(s).
     */
    private static final String DISPLAY_NAME = "displayName";
    
    /**
     * Strategy used when only one test is slected. 
     */
    private final UniqueIdTestStrategy uniqueTestDelegate;
    
    /**
     * Logger used to log the client command related messages;
     */
    private final Logger clientLogger;
    
    /**
     * Std ctor. 
     * 
     * @param client : The client using this strategy. Used mostly to parametrise logs.
     * @param uniqueTestDelegate : The delegate used in the case of a single Junit test. 
     */
    QualifiedNameTestStrategy(ExecuteJunit5Bundle client, UniqueIdTestStrategy uniqueTestDelegate) {
        super(client);
        this.uniqueTestDelegate=uniqueTestDelegate;
        clientLogger=LoggerFactory.getLogger(client.getClass());
    }

    @Override
    public boolean accept(List<Resource<?>> configuration) {
        final String targetTestClassName = confHelper().extractOptionValue(configuration, QUALIFIED_CLASS);
        final String targetTestDisplayName = confHelper().extractOptionValue(configuration, DISPLAY_NAME);

        return targetTestClassName != null && targetTestDisplayName != null;
    }

    @Override
    protected Result executeImpl(List<Resource<?>> configuration, JunitBundleContext context) {
        final String targetTestClassName = confHelper().extractOptionValue(configuration, QUALIFIED_CLASS);
        final String targetTestDisplayName = confHelper().extractOptionValue(configuration, DISPLAY_NAME);

        List<TestDescriptor> foundTests = context.getTestsOfMethod(new JunitTestDescriptorImpl(targetTestClassName, targetTestDisplayName, null));

        // Java 8 could be handy here with stream stuffs... 
        List<String> targetedTestsUIDs = new ArrayList<>();
        for (TestDescriptor test : foundTests) {
            if (test.uniqueId() != null) {
                targetedTestsUIDs.add(test.uniqueId());
            }
        }

        if (targetedTestsUIDs.isEmpty()) {
            final String message = "No implemented Junit Test with corresponding qualified name " + targetTestClassName + "." + targetTestDisplayName + " was discovered.";
            clientLogger.warn(message);
            throw new IllegalConfigurationException(message);
        } else if (targetedTestsUIDs.size() == 1) {
            final String targetTestUID = targetedTestsUIDs.get(0);
            LOGGER.debug("Found a unique Test with UID {}. Delegating launch to UID Strategy.", targetTestUID);
            return uniqueTestDelegate.launchSingleTargetTest(context, targetTestUID);
        } else {
            LOGGER.debug("Found {} tests to launch.", targetedTestsUIDs.size());
            throw new InstructionRuntimeException("Launch of several tests with same qualified name is not YET supported. Found " + targetedTestsUIDs.size() + " tests with qualified name " + targetTestClassName + "." + targetTestDisplayName);
            /*
             * Once ContextImpl will implement the multiple Ids launch replace the above line by :  context.launch(targetedTestsUIDs);
             * To do so a specific "Junit5ContextImpl.Operation" should be implemented to producing a ClusterResource of Junit Results. 
             */
        }

    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.commands.delegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.tools.ComponentRepresentation;
import org.squashtest.ta.squash.ta.plugin.junit.commands.ExecuteJunit5Bundle;

/**
 * This Builder class enables one to select the correct
 * {@link ExecutionStrategy} according to a given configuration.
 *
 * @author fgautier
 */
public class StrategyFactory {

    /**
     * Holds the available strategies.
     */
    private final List<ExecutionStrategy> availableStrategies;

    /**
     * The client of the different delegate strategies.
     */
    private final ExecuteJunit5Bundle delegateClient;

    /**
     * The looger of the client.
     */
    private final Logger delegateClientLogger;

    /**
     * Standard constructor. You should specify the client the delegates will be
     * constructed for.
     *
     * @param delegateClient : The client that will use the delegates;
     */
    public StrategyFactory(ExecuteJunit5Bundle delegateClient) {
        this.delegateClient = delegateClient;
        this.delegateClientLogger = LoggerFactory.getLogger(delegateClient.getClass());

        List<ExecutionStrategy> newStrategies = new ArrayList<>();
        UniqueIdTestStrategy uniqueStrategy = new UniqueIdTestStrategy(delegateClient);
        QualifiedNameTestStrategy nameStrategy = new QualifiedNameTestStrategy(delegateClient, uniqueStrategy);
        newStrategies.add(uniqueStrategy);
        newStrategies.add(nameStrategy);
        availableStrategies = Collections.unmodifiableList(newStrategies);

    }

    /**
     * Select an {@link ExecutionStrategy} given a configuration.
     *
     * @param configuration : the configuration.
     *
     * @return the constructed strategy.
     *
     */
    public ExecutionStrategy buildStrategy(List<Resource<?>> configuration) {
        List<ExecutionStrategy> acceptedStrategies = new ArrayList<>();

        for (ExecutionStrategy availableStrategy : availableStrategies) {
            if (availableStrategy.accept(configuration)) {
                acceptedStrategies.add(availableStrategy);
            }
        }

        if (acceptedStrategies.isEmpty()) {
            final String message = "Missing mandatory configuration for " + new ComponentRepresentation(delegateClient) + ". Usage : " + ExecuteJunit5Bundle.USAGE;
            delegateClientLogger.warn(message);
            throw new IllegalConfigurationException(message);
        } else if (acceptedStrategies.size() > 1) {
            final String message = "Ambiguous configuration for " + new ComponentRepresentation(delegateClient) + ". Usage : " + ExecuteJunit5Bundle.USAGE;
            delegateClientLogger.warn(message);
            throw new IllegalConfigurationException(message);
        } else {
            return acceptedStrategies.get(0);
        }
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context;

import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;

/**
 *
 * @author edegenetais
 */
class Junit5UniqueIdExecution implements Junit5ContextImpl.Operation<String, Result> {

    @Override
    public Result perform(String uniqueId) {
        final Junit5ResultImpl resultImpl = new Junit5ResultImpl(uniqueId);
        LauncherDiscoveryRequestBuilder requestBuilder = LauncherDiscoveryRequestBuilder.request();
        LauncherDiscoveryRequest r = requestBuilder.selectors(DiscoverySelectors.selectUniqueId(uniqueId)).build();
        Launcher l = LauncherFactory.create();
        l.registerTestExecutionListeners(resultImpl);
        l.execute(r);
        return resultImpl;
    }

}

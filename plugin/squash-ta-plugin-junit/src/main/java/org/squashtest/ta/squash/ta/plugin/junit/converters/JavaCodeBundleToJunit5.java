/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.converters;

import java.util.Collection;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitTestBundle;

/**
 * This converter produces {@link JunitTestBundle} resources from {@link JavaCodeBundle}.
 * @author edegenetais
 */
@TAResourceConverter("structured")
public class JavaCodeBundleToJunit5 implements ResourceConverter<JavaCodeBundle, JunitTestBundle>{    

    @Override
    public float rateRelevance(JavaCodeBundle input) {
        return 0.5f;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        new ConfigurationExtractor(this).expectNoConfiguration(configuration);
    }

    @Override
    public JunitTestBundle convert(JavaCodeBundle resource) {
        return new JunitTestBundle(resource.copy());
    }

    @Override
    public void cleanUp() {
        //noop - for now !
    }
}

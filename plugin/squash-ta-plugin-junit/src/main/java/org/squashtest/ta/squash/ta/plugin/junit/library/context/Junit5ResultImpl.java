/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context;

import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.slf4j.Logger;
import org.squashtest.ta.plugin.commons.resources.InitSilencingLoggerFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;


/**
 *
 * @author edegenetais
 */
public class Junit5ResultImpl implements Result,TestExecutionListener{
    
    private static final Logger LOGGER=InitSilencingLoggerFactory.getLogger(Junit5ResultImpl.class);
    
    /** UID of the test we wanted to execute */
    private final String initialTargetUID;
    
    private final Class<?> junit5FailureClass;
    private final Class<?> junit4FailureClass;
    private final Class<?> assertionErrorClass;
    
    private Status status=Status.NOT_RUN;
    private Throwable error;
    
    public Junit5ResultImpl(String initialTargetUID) {
      try{
        InitSilencingLoggerFactory.switchOnInitPhase();
        this.initialTargetUID = initialTargetUID;
        junit5FailureClass=loadClassIfAvailable("org.opentest4j.AssertionFailedError");
        junit4FailureClass=loadClassIfAvailable("org.junit.ComparisonFailure");
        assertionErrorClass=loadClassIfAvailable("java.lang.AssertionError");
      } finally {
        InitSilencingLoggerFactory.switchOffInitPhase();
      }
    }

    private Class<?> loadClassIfAvailable(String className) {
        Class<?> targetClass;
        try {
            targetClass=Class.forName(className);
        } catch (ClassNotFoundException ex) {
            LOGGER.debug("Junit probably not in the classpath : failed to load AssertionFailedError",ex);
            targetClass=null;
        }
        return targetClass;
    }
    
    @Override
    public void testPlanExecutionStarted(TestPlan testPlan) {
        LOGGER.debug("We started something!");
        status=Status.NOT_FOUND;
    }

    @Override
    public void testPlanExecutionFinished(TestPlan testPlan) {
        LOGGER.debug("Test plan done.");
        switch(status){
            case RUNNING:
            case NOT_RUN:
                LOGGER.debug("Oops : the test apparently hung, stuck at status {}!",status);
                status=Status.ERROR;
                error=new IllegalStateException("Execution of test "+initialTargetUID+" never completed");
                break;
            case NOT_FOUND:
                status=Status.ERROR;
                error=new IllegalArgumentException("Test not found : "+initialTargetUID);
                break;
            default:
                LOGGER.debug("Received end of test plan message. The final status is {}.",status);
                break;
        }
    }
    
    @Override
    public void executionStarted(TestIdentifier testIdentifier) {
        if(initialTargetUID.equals(testIdentifier.getUniqueId())){
            LOGGER.debug("Target test {}(UID:{}) started",testIdentifier.getDisplayName(),testIdentifier.getUniqueId());
        }else{
            LOGGER.debug("Test {}(UID:{}) started.",testIdentifier.getDisplayName(),testIdentifier.getUniqueId());
        }
    }

    @Override
    public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
        LOGGER.debug("Test {} finished with result {}",testIdentifier.getUniqueId(),testExecutionResult.toString());
        if(this.initialTargetUID.equals(testIdentifier.getUniqueId())){
            if(testExecutionResult.getStatus().equals(TestExecutionResult.Status.SUCCESSFUL)){
                status=Status.SUCCESS;
            }else if(isFunctionalFailure(testExecutionResult)){
                status=Status.FAIL;
                if(testExecutionResult.getThrowable().isPresent()){
                    error=testExecutionResult.getThrowable().get();
                }
            }else{
                status=Status.ERROR;
                error=testExecutionResult.getThrowable().get();
            }
        }else if(testExecutionResult.getStatus().equals(TestExecutionResult.Status.FAILED)){
            /*
             * If previous tests fail for whatever reason, 
             * it is precondition failure (these tests where dragged in the plan because of dependencies), 
             * thus error (technical problem) not fail (actual different from expected).
             * The target test is expected to be the last test to run.
             */
            status=Status.ERROR;
            if(testExecutionResult.getThrowable().isPresent()){
                error=testExecutionResult.getThrowable().get();
            }
        }
    }

    private boolean isFunctionalFailure(TestExecutionResult testExecutionResult) {
        if(testExecutionResult.getThrowable().isPresent()){
            boolean isJunit5Failure;
            if(this.junit5FailureClass==null){
                isJunit5Failure=false;//cannot be junit5 failure, as junit5 is not loaded
            }else{
                isJunit5Failure=this.junit5FailureClass.isAssignableFrom(testExecutionResult.getThrowable().get().getClass());
            }
            boolean isJunit4Failure;
            if(this.junit4FailureClass==null){
                isJunit4Failure=false;
            }else{
                isJunit4Failure=this.junit4FailureClass.isAssignableFrom(testExecutionResult.getThrowable().get().getClass())
                        ||
                        this.assertionErrorClass.isAssignableFrom(testExecutionResult.getThrowable().get().getClass());
            }
            return isJunit4Failure||isJunit5Failure;
        }else{
            return false;
        }
    }

    @Override
    public Status getTestStatus() {
        return status;
    }

    @Override
    public Throwable getCaughtError() {
        return error;
    }
}

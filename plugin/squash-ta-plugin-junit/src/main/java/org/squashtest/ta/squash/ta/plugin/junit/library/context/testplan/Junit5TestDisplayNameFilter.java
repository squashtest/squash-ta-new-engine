/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This {@link Junit5TestPlanVisitorListener} will retain only tests the display name of which 
 * equals the targeted one. 
 * 
 * @author fgautier
 */
public class Junit5TestDisplayNameFilter implements Junit5TestPlanVisitorListener {

    /**
     * Class related messages logge.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Junit5TestDisplayNameFilter.class);
    
    /**
     * Defines the targeted display name.
     */
    private final String targetedDisplayName;

    /**
     * Holds the maching {@link TestIdentifier}s.
     */
    private List<TestIdentifier> filteredTests;
    
    public Junit5TestDisplayNameFilter(String targetedDisplayName) {
        this.targetedDisplayName = targetedDisplayName;
    }
    
    @Override
    public void notifyVisitStart(TestPlan plan) {
        LOGGER.debug("Filtering of test plan '{}'.", plan);
        filteredTests = new ArrayList<>(); 
    }
    
    @Override
    public void notifyContainer(TestIdentifier container) { 
        /* NO-OP */
        LOGGER.debug("Discarding container '{}'.", container.getDisplayName());
    }

    @Override
    public void notifyContainerEnd(TestIdentifier container) {
        /* NO-OP */
        LOGGER.debug("End of container '{}'.", container.getDisplayName());
    }
    
    @Override
    public void notifyTest(TestIdentifier test) {
        LOGGER.debug("Treating test {}.", test.getDisplayName());
        if(targetedDisplayName.equals(test.getDisplayName())) {
            LOGGER.debug("Test '{}' matches display name '{}'.", test.getUniqueId(), targetedDisplayName);
            filteredTests.add(test);
        }
    }
    
    @Override
    public void notifyHybridTestAndContainer(TestIdentifier identifier) {
        throw new UnsupportedOperationException("let me think about it."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void notifyVisitEnd() { 
        if(LOGGER.isDebugEnabled()) {
            StringBuilder messageBuilder = new StringBuilder("Filtering is done. Found tests:\n");
            for (TestIdentifier filteredTest : filteredTests) {
                messageBuilder.append("test id :")
                        .append(filteredTest.getUniqueId())
                        .append("\n");
            }
            LOGGER.debug(messageBuilder.toString());
        }
    }
    
    /**
     * Get the retained tests. 
     * 
     * @return an unmodifiable list of filtered {@link TestIdentifier}s.
     */
    public List<TestIdentifier> getFilteredTest() {
        return Collections.unmodifiableList(this.filteredTests);
    }

}

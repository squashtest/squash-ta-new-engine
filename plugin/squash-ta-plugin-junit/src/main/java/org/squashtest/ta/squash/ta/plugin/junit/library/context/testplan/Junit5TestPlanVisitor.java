/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;

/**
 * Helper class whose only concern is to correctly visit a {@link TestPlan}. During 
 * its visit it will signal its collection of {@link Junit5TestPlanVisitorListener}s 
 * for each visited {@link TestIdentifier} accordingly, i.e. :  
 * <ul>
 *  <li>if identifier.isTest() : signals a test.</li>
 *  <li>if identifier.isContainer() : signals a test container.</li>
 *  <li>if both identifier.isTest() and identifier.isContainer() : signals a dual test and test container node.</li>
 * </ul>
 * 
 * NB : this class originated from the JunitRunner exploitation layer, 
 * but was moved to the plugin to void direct handling of the Junit5 elements 
 * by exploitation and keep the isolated junit5 classloading context within framework code.
 * 
 * @author fgautier
 */
public class Junit5TestPlanVisitor {
    
    /**
     * Holds the different listeners that will be signaled during the visit. 
     */
    private final List<Junit5TestPlanVisitorListener> listeners;

    /**
     * Constructs a {@link Junit5TestPlanVisitor} that will signal the given {@link Junit5TestPlanVisitorListener}
     * during the visit. 
     * 
     * @param listeners the signals receivers.  
     */
    public Junit5TestPlanVisitor(Junit5TestPlanVisitorListener ... listeners) {
        this.listeners = Collections.unmodifiableList(Arrays.asList(listeners));
    }
    
    /**
     * Visits recursively the given {@link TestPlan}. The listeners are signaled accordingly 
     * each time a Test or a Test Container is visited.
     * 
     * @param plan the {@link TestPlan} to visit. 
     */
    public void visit(TestPlan plan) {
        signalStart(plan);
        
        // Start the visit with roots 
        visitIdentifiers(plan.getRoots(), plan);
        
        signalEnd();
    }
    
    /**
     * Visit a {@link TestPlan} starting (sequentially) from a set of {@link TestIdentifier}.
     * 
     * @param identifiers are the starting points.
     * @param plan the {@link TestPlan} to visit.
     */
    private void visitIdentifiers(Set<TestIdentifier> identifiers, TestPlan plan) {
        for (TestIdentifier identifier : identifiers) {
            visitIdentifier(identifier, plan);
        }
    }
    
    /**
     * Visit a {@link TestPlan} starting from a (known) {@link TestIdentifier}.
     * 
     * @param identifier the starting point.
     * @param plan the {@link TestPlan} to visit.
     */
    private void visitIdentifier(TestIdentifier identifier,TestPlan plan) {
        if(identifier.isTest() && identifier.isContainer()) {
            signalTestAndContainer(identifier);
            visitIdentifiers(plan.getChildren(identifier), plan);
        } else if (identifier.isContainer()) {
            signalContainer(identifier);
            visitIdentifiers(plan.getChildren(identifier), plan);
            signalContainerEnd(identifier);
        } else if (identifier.isTest()) {
            signalTest(identifier);
        }
    }

    /**
     * Signals all the listeners that TestPlan visit just started. 
     * 
     * @param plan the visited plan.
     */
    private void signalStart(TestPlan plan) {
        for (Junit5TestPlanVisitorListener listener : listeners) {
            listener.notifyVisitStart(plan);
        }
    }
    
    /**
     * Signals all the listeners that a JUnit Test has been visited. 
     * 
     * @param test the visited test node
     */
    private void signalTest(TestIdentifier test) {
        for (Junit5TestPlanVisitorListener listener : listeners) {
            listener.notifyTest(test);
        }
    }
    
    /**
     * Signals all the listeners that a JUnit Test Container has been visited. 
     * 
     * @param container the visited test node
     */
    private void signalContainer(TestIdentifier container) {
        for (Junit5TestPlanVisitorListener listener : listeners) {
            listener.notifyContainer(container);
        }
    }
    
    /**
     * Signals all the listeners that a JUnit Test Container visit is finished. 
     * 
     * @param container the visited test node
     */
    private void signalContainerEnd(TestIdentifier container) {
        for (Junit5TestPlanVisitorListener listener : listeners) {
            listener.notifyContainerEnd(container);
        }
    }
    
    /**
     * Signals all the listeners that an hybride node (both Test and TestContainer) has been visited. 
     * 
     * @param hybridIdentifier the visited test node
     */
    private void signalTestAndContainer(TestIdentifier hybridIdentifier) {
        for (Junit5TestPlanVisitorListener listener : listeners) {
            listener.notifyHybridTestAndContainer(hybridIdentifier);
        }
    }
    
    /**
     * Signals all the listeners that visit has finished...
     */
    private void signalEnd() {
        for (Junit5TestPlanVisitorListener listener : listeners) {
            listener.notifyVisitEnd();
        }
    }
}

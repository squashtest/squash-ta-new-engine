/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.squash.ta.plugin.junit.commands.delegate.ExecutionStrategy;
import org.squashtest.ta.squash.ta.plugin.junit.commands.delegate.StrategyFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Result;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitResult;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitTestBundle;

/**
 * Command to execute any Junit test using the junit5 framework.
 * Usage : 
 * 
 * <b>EXECUTE excute WITH junit5Bundle ON {void} USING configuration</b>
 * 
 * This command is parametrized using a <b>mandatory</b> option-formatted {@link FileResource}, 
 * the content of which can be set in two (mutually exclusive) ways : 
 * 
 * <ul>
 * <li>Directly set the Unique Id of the test to execute using the <b>"uniqueId"</b> key.</li>
 * <li>Find the test(s) to execute with its qualified display name using the following keys
 *   <ul>
 *     <li>"qualifiedClassName"</li>
 *     <li>"displayName"</li>
 *   </ul>
 * </li>
 * </ul>
 * 
 * Note that the configuration is mandatory. The command will fail if none (correct) 
 * is provided. 
 * 
 * Also note that these two possible configuration contents are mutually exclusive. Meaning 
 * that one has to choose how one wants to select the test(s) to execute. Either using its
 * unique Id or using its qualified display name, but not both. If both are provided the command 
 * will fail. 
 * 
 * Finally note that in the case of the the qualified display name test(s) bears a conditional "s". 
 * Indeed in this scenario one cannot guarantee the uniqueness of a display name (even qualified). 
 * 
 * 
 * @author edegenetais
 */
@TACommand("execute")
public class ExecuteJunit5Bundle implements Command<JunitTestBundle, VoidTarget> {

    /**
     * Proper usage of the command. For logging and error throwing puposes.
     */
    public static final String USAGE = "This command is parametrized using a mandatory option-formatted FileResource,"
            + "the content of which can be set in two (mutually exclusive) ways :\n"
            + "\t- Set Unique Id of the test to execute using the \"uniqueId\" key.\n"
            + "\t- Specify the test(s) to execute with its qualified display name using the \"qualifiedClassName\" and \"displayName\" keys.";

    private final List<Resource<?>> configuration = new ArrayList<>();
    
    private JunitTestBundle input;

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        this.configuration.addAll(configuration);
    }

    @Override
    public void setTarget(VoidTarget target) {
        //noop : void target is nothing but a placeholder.
    }

    @Override
    public void setResource(JunitTestBundle resource) {
        this.input = resource;
    }

    @Override
    public JunitResult apply() {
        ExecutionStrategy executionStrategy = new StrategyFactory(this).buildStrategy(configuration);
        Result r = executionStrategy.execute(configuration, input.getContext());
        return new JunitResult(r);
    }

    @Override
    public void cleanUp() {
        //noop - yet
    }
}

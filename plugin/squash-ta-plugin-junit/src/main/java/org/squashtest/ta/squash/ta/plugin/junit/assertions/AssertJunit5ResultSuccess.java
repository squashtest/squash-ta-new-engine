/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.assertions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitResult;

/**
 * {@link UnaryAssertion} verifying that a {@link JunitResult} is a success.
 * @author fgautier
 */
@TAUnaryAssertion("success")
public class AssertJunit5ResultSuccess implements UnaryAssertion<JunitResult>{
    
    private JunitResult testedResult; 
    
    @Override
    public void setActualResult(JunitResult actual) {
        testedResult=actual;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        new ConfigurationExtractor(this).expectNoConfiguration(configuration);
    }

    @Override
    public void test() {
        if(!testedResult.getExecutionData().getTestStatus().isSuccess()){
            final String failureMessage;
            final List<ResourceAndContext> errorRac = new ArrayList<>();
            
            final Throwable caughtError = testedResult.getExecutionData().getCaughtError();
            if(caughtError==null){
                failureMessage="no message";
            }else{
                if(caughtError.getMessage()==null) {
                    failureMessage="no message";
                } else {
                    failureMessage=caughtError.getMessage();
                }
                
                addStackTraceToRac(caughtError, errorRac);
            }
            
            throw new AssertionFailedException("Junit test failed : "+failureMessage, testedResult, errorRac);
        }
    }

    private void addStackTraceToRac(Throwable caughtError, List<ResourceAndContext> errorRac) {
        File stackTraceReport = createTempFile();
        
        if (stackTraceReport != null) {
            try (   OutputStream fos = new FileOutputStream(stackTraceReport);
                    PrintStream ps = new PrintStream(fos);
                    ){
                caughtError.printStackTrace(ps);

                ResourceAndContext assertionFailureContext = new ResourceAndContext();
                assertionFailureContext.setResource(new FileResource(stackTraceReport).copy());
                assertionFailureContext.setMetadata(new ExecutionReportResourceMetadata(AssertJunit5ResultSuccess.class,
                                                                                        new Properties(),
                                                                                        FileResource.class,
                                                                                        "test-stacktrace.txt"));
                errorRac.add(assertionFailureContext);
            } catch (IOException ioex) {
                LoggerFactory.getLogger(AssertJunit5ResultSuccess.class).warn("Technical failure while appending Junit error statck trace. Report will be missing", ioex);
            }
        }
    }

    private File createTempFile() {
        File stackTraceReport = null;
        try {
            stackTraceReport = File.createTempFile("junit-error-stack", ".txt", TempDir.getExecutionTempDir());
            stackTraceReport.deleteOnExit();
        } catch (IOException ioex) {
            LoggerFactory.getLogger(AssertJunit5ResultSuccess.class).warn("Technical failure while creating Junit error statck trace report file support. Report will be missing", ioex);
        }
        return stackTraceReport;
    }


    
    
}

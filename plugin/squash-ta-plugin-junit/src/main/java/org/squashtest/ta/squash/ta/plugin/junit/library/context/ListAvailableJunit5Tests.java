/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.library.context;

import java.util.List;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.Bundle;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;
import org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan.Junit5TestListBuilder;
import org.squashtest.ta.squash.ta.plugin.junit.library.context.testplan.Junit5TestPlanVisitor;

/**
 *
 * @author edegenetais
 */
class ListAvailableJunit5Tests implements Junit5ContextImpl.Operation<Void, List<TestDescriptor>> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ListAvailableJunit5Tests.class);
    
    private Bundle bundle;

    public ListAvailableJunit5Tests(Bundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public List<TestDescriptor> perform(Void t) {
        Launcher l = LauncherFactory.create();
        LauncherDiscoveryRequestBuilder requestBuilder = LauncherDiscoveryRequestBuilder.request();
        LOGGER.debug("Bundle has {} classes ", bundle.getBundleClasses().size());
        for (String name : bundle.getBundleClasses()) {
            LOGGER.trace("Referencing class {} from bundle {}.", name, bundle);
            requestBuilder.selectors(DiscoverySelectors.selectClass(name));
        }
        TestPlan tp = l.discover(requestBuilder.build());
        final Junit5TestListBuilder listBuilder = new Junit5TestListBuilder();
        new Junit5TestPlanVisitor(listBuilder).visit(tp);
        return listBuilder.getVisitedTests();
    }

}

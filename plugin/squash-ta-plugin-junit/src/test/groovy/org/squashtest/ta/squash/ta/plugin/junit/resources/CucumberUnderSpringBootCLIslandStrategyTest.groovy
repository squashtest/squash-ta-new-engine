/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.plugin.junit.resources

import spock.lang.Specification;

class CucumberUnderSpringBootCLIslandStrategyTest extends Specification{
    CucumberUnderSpringBootCLIslandStrategy testee
    
    def "If not springboot, nothing is infrastructure"(){
        given:
            ClassLoader island=new ClassLoader(getClass().getClassLoader()){
                public Class<?> loadClass(String name){
                    if("org.springframework.boot.SpringApplication".equals(name)){
                        throw new ClassNotFoundException("There is no SpringBoot");
                    }else{
                        return super.loadClass(name);
                    }
                }
            }
        and:
            testee=new CucumberUnderSpringBootCLIslandStrategy(island);
        when:
            def isInfra=testee.isInfrastructure("cucumber.runtime.java.spring.SpringFactory")
        then:
            isInfra==false
    }
    
    def "If not the SpringFactory, then not infrastructure without SpringBoot"(){
        given:
            ClassLoader island=new ClassLoader(getClass().getClassLoader()){
                public Class<?> loadClass(String name){
                    if("org.springframework.boot.SpringApplication".equals(name)){
                        throw new ClassNotFoundException("There is no SpringBoot");
                    }else{
                        return super.loadClass(name);
                    }
                }
            }
        and:
            testee=new CucumberUnderSpringBootCLIslandStrategy(island);
        when:
            def isInfra=testee.isInfrastructure("org.squashtest.ta.squash.ta.plugin.junit.resources.JunitClassLoader")
        then:
            isInfra==false
    }
    
    def "If springboot, SpringFactory is infrastructure"(){
        given:
            testee=new CucumberUnderSpringBootCLIslandStrategy(getClass().getClassLoader())
        when:
            def isInfra=testee.isInfrastructure("cucumber.runtime.java.spring.SpringFactory")
       then:
            isInfra
    }
    
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.converters;

import static org.junit.Assert.*

import java.util.Collection;

import org.apache.commons.collections.collection.CompositeCollection
import org.squashtest.ta.core.tools.io.BinaryData
import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.soapui.converters.FileToSoapUI
import org.squashtest.ta.plugin.soapui.resources.SoapUIScript

import spock.lang.Specification

class FileToSoapUITest extends Specification {
	def converter = new FileToSoapUI();
	

	def "the soapUI file must be correctly formated"(){
	
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/soapui/converters/correct-soapui-project.xml")
			FileResource myResource = new FileResource(new File(url.toURI()))
		when :
			SoapUIScript mySoapUIScript = converter.convert(myResource)
			
		then :
			mySoapUIScript != null
			notThrown(BadDataException) 
	}
	
	def "the soapUI file makes the validation crash"(){
		
			given :
				URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/soapui/converters/incorrect-soapui-project.xml")
				FileResource myResource = new FileResource(new File(url.toURI()))
			when :
				SoapUIScript mySoapUIScript = converter.convert(myResource)
				
			then :
				thrown(BadDataException)
		}
	
	def "the soapUI bundle without configuration is not tested"(){
		
			given :
				URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/soapui/converters/")
				FileResource myResource = new FileResource(new File(url.toURI()))
			when :
				SoapUIScript mySoapUIScript = converter.convert(myResource)
				
			then :
				mySoapUIScript != null
				notThrown(BadDataException)
		}
	
	def "the soapUI bundle with configuration must be correctly formated"(){
		
			given :
				URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/soapui/converters/")
				FileResource myResource = new FileResource(new File(url.toURI()))
			and :
				BinaryData targetTestSuite=new BinaryData("soapui.project.path:correct-soapui-project.xml".bytes)
				File tempDeploy=File.createTempFile("testTarget", ".properties", TempDir.getExecutionTempDir())
				targetTestSuite.write(tempDeploy)
				FileResource testTarget=new FileResource(tempDeploy)
				Collection<Resource<?>> conf = new ArrayList<Resource<?>>()
				conf.add(testTarget);
				converter.addConfiguration(conf)
			when :
				SoapUIScript mySoapUIScript = converter.convert(myResource)
				
			then :
				mySoapUIScript != null
				notThrown(BadDataException)
		}
	
	def "the soapUI bundle makes the validation crashed"(){
		
			given :
				URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/soapui/converters/")
				FileResource myResource = new FileResource(new File(url.toURI()))
			and :
				BinaryData targetTestSuite=new BinaryData("soapui.project.path:incorrect-soapui-project.xml".bytes)
				File tempDeploy=File.createTempFile("testTarget", ".properties", TempDir.getExecutionTempDir())
				targetTestSuite.write(tempDeploy)
				FileResource testTarget=new FileResource(tempDeploy)
				Collection<Resource<?>> conf = new ArrayList<Resource<?>>()
				conf.add(testTarget);
				converter.addConfiguration(conf)
			when :
				SoapUIScript mySoapUIScript = converter.convert(myResource)
				
			then :
				thrown(BadDataException)
		}
}

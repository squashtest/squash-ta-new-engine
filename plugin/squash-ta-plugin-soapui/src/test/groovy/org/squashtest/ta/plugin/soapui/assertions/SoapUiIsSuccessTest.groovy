/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.assertions

import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.soapui.assertions.SoapUIIsSuccess
import org.squashtest.ta.plugin.soapui.resources.SoapUIResult

import spock.lang.Specification

import com.eviware.soapui.model.testsuite.TestRunner.Status

class SoapUiIsSuccessTest extends Specification {
	SoapUIIsSuccess testee;
	def setup(){
		testee=new SoapUIIsSuccess()
	}
	
	def "if success, pass through silently"(){
		given:
			Map<String, Status> testStatusByName=["1::ben":Status.FINISHED,"1::nuts":Status.FINISHED]
			Map<String,String> testMessagesByName=[:]
		and:
			SoapUIResult actual=new SoapUIResult(testStatusByName,testMessagesByName)
			testee.setActualResult(actual)
		when:
			testee.test()
		then:
			noExceptionThrown()
	}
	
	def "if failure, AssertionFailureException expected"(){
		given:
			Map<String, Status> testStatusByName=["1::ben":Status.FAILED,"1::nuts":Status.FINISHED]
			Map<String,String> testMessagesByName=["1::ben":"grilled","1::nuts":"spicy"]
		and:
			SoapUIResult actual=new SoapUIResult(testStatusByName,testMessagesByName)
			testee.setActualResult(actual)
		when:
			testee.test()
		then:
			thrown(AssertionFailedException)
	}
}

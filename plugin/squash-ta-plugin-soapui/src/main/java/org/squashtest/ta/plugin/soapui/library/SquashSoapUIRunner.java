/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.library;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.process.StreamMuncher;
import org.squashtest.ta.plugin.commons.library.java.JavaSystemProcessConnector;
import org.squashtest.ta.plugin.soapui.resources.SoapUIResult;

import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.support.SoapUIException;

/**
 * This class is derived from the sahi test suite runner. Used by the SoapUI components to
 * run SoapUI test suites.
 * 
 * @author Agnes
 * @author edegenetais
 * @author kdrifi
 */
public class SquashSoapUIRunner extends JavaSystemProcessConnector implements SoapUiProcessCommunication {
    
	private static final Logger LOGGER = LoggerFactory.getLogger(SquashSoapUIRunner.class);

	private Properties config;
	private Properties target;

	private List<String> testSuites;
	private List<String> testCases;

	public SquashSoapUIRunner(Properties config, Properties target) {
		this(config);
		this.target = target;
	}

	public SquashSoapUIRunner(Properties config) {
		this.config = config;
		testSuites = buildTestList(SoapUiProcessExecutor.SOAPUI_TESTSUITE);
		testCases = buildTestList(SoapUiProcessExecutor.SOAPUI_TESTCASE);
	}

	/**
	 * Runs a SoapUI test file The test file contains the definition of the
	 * endpoints the tests suite to execute the test cases to execute
	 * 
	 * @param soapUIFile
	 * @throws IOException
	 */
	public SoapUIResult run(File soapUIFile, Properties configuration)
			throws XmlException, IOException, SoapUIException {
		logSoapUIConf(soapUIFile);

		// NB: the class name is transmitted as a String to avoid loading it and
		// triggering the SoapUi related logging snafu...
		ArrayList<String> arguments = buildSoapuiProcessArguements(soapUIFile);

		List<String> command = createCommand(arguments,
				"org.squashtest.ta.plugin.soapui.library.SoapUiProcessExecutor");
		ProcessBuilder processBuilder = new ProcessBuilder(command);
		processBuilder.directory(new File(".").getAbsoluteFile());
		Process p = processBuilder.start();

		Map<String, Status> testStatusesByName = new HashMap<String, Status>();
		Map<String, String> testMessagesByName = new HashMap<String, String>();

		PipeListener statusPrintListener = new PipeListener(p, testStatusesByName, testMessagesByName);
		statusPrintListener.start();
		StreamMuncher errTail = new StreamMuncher("SoapUI stderr", p.getErrorStream(), 25600);
		errTail.start();

		int processReturn;
		try {
			processReturn = p.waitFor();
		} catch (InterruptedException e) {
			LOGGER.debug("Interrupted while waiting for the SoapUI external process",e);
			processReturn = 5;
		}
		errTail.requestStop();

		switch (processReturn) {
		case INPUT_FILE_IO_ERROR:
			throw new IOException("I/O error in the soapUI process:\n" + errTail.getStreamContent());
		case WORKSPACE_XML_EXCEPTION:
			throw new XmlException("Trouble loading the SoapUI workspace:\n" + errTail.getStreamContent());
		case WORKSPACE_SOAPUI_EXCEPTION:
			throw new SoapUIException("Trouble loading the SoapUI workspace:\n" + errTail.getStreamContent());
		case BAD_DATA_EXCEPTION:
			throw new BadDataException("Bad data given to the instruction: \n" + errTail.getStreamContent());
		case 0:
			// this is the OK case

			/*
			 * if a test case wasn't present in a test suite, we need to
			 * manually rise an exception (SoapUI runs nothing, but consider it
			 * to be ok)
			 */
			compareWithExpected(testStatusesByName);
			break;
		default:
			throw new InstructionRuntimeException(
					"SoapUI process failed with code " + processReturn + "\n" + errTail.getStreamContent());
		}

		return new SoapUIResult(testStatusesByName, testMessagesByName);
	}

	private ArrayList<String> buildSoapuiProcessArguements(File soapUIFile) throws IOException {
		ArrayList<String> arguments = new ArrayList<String>();

		// Saving soapui temp file and get his absolute path.
		arguments.add(soapUIFile.getAbsolutePath());

		// Saving soapui configuration temp file file and get his absolute path.
		File configTemp = File.createTempFile("soapui_temp", ".properties", TempDir.getExecutionTempDir());
		FileWriter writerConfig = new FileWriter(configTemp);
		config.store(writerConfig, "Temporary");
		writerConfig.close();
		arguments.add(configTemp.getAbsolutePath());

		// Saving target temp file and get his absolute path.
		if (target != null) {
			File targetTemp = File.createTempFile("soapuiTarget_temp", ".properties", TempDir.getExecutionTempDir());
			FileWriter writerTarget = new FileWriter(targetTemp);
			target.store(writerTarget, "Temporary");
			writerTarget.close();
			arguments.add(targetTemp.getAbsolutePath());
		}
		
		return arguments;
	}

	private void compareWithExpected(Map<String, Status> testStatusesByName) {
		int expectedExecutionNumber = testSuites.size() * testCases.size();
		if (testStatusesByName.size() != expectedExecutionNumber) {
			String diffReport = buildDiffReport(testStatusesByName);
			if (!diffReport.isEmpty()) {
				throw new BadDataException("Following SoapUI test(s) could not be found:" + diffReport);
			}
		}
	}

	private String buildDiffReport(Map<String, Status> testStatusesByName) {
		StringBuilder diffReportBuilder = new StringBuilder();
		for (String testSuite : testSuites) {
			for (String testCase : testCases) {
				if (!testStatusesByName.keySet().contains(testSuite + "::" + testCase)) {
					diffReportBuilder
							.append("\n- '" + testSuite + "' does not contain any testcase named '" + testCase + "'");
				}
			}
		}
		return diffReportBuilder.toString();
	}

	private List<String> buildTestList(String soapuiTestsuite) {
		List<String> testList = new ArrayList<String>();
		String property = config.getProperty(soapuiTestsuite);
		if (property != null) {
			String[] testTab = property.split(",");
			for (int i = 0; i < testTab.length; i++) {
				testList.add(testTab[i].trim());
			}
		}
		return testList;
	}

	/**
	 * @param pSoapUIFile
	 * @param pSoapUITestSuite
	 * @param pSoapUITestCase
	 */
	private void logSoapUIConf(File soapUIFile) {
		LOGGER.debug("SoapUI Test File = {}", soapUIFile.getName());
		LOGGER.debug("SoapUI Test Suite to test = {}", config.get(SoapUiProcessExecutor.SOAPUI_TESTSUITE));
		LOGGER.debug("SoapUI Test Case to test = {}", config.get(SoapUiProcessExecutor.SOAPUI_TESTCASE));
	}
}

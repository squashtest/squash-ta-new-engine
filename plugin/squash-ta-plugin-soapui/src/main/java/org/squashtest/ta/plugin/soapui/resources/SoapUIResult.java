/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.resources;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.tools.TempDir;

import com.eviware.soapui.model.testsuite.TestRunner.Status;

/**
 * Resource holding the result of a SoapUI test suite. Contents:
 * <ol>
 * <li>Status flag ({@link GeneralStatus})</li>
 * </ol>
 * 
 * @author fgaillard
 * 
 */
@TAResource("result.soapui")
public class SoapUIResult implements Resource<SoapUIResult> {
	
	private Map<String, Status>testStatusesByName = new HashMap<String, Status>();
	
	private Map<String, String>testMessagesByName = new HashMap<String, String>();
		
	/** Execution status of the SopaUI suite. */
	private GeneralStatus soapUITestsStatus;

	private File executionReport;

	/** Noarg constructor for Spring enumeration. */
	public SoapUIResult() {}
	
	public SoapUIResult(Map<String, Status> testStatusesByName, Map<String, String>testMessagesByName){
		try{
			this.testStatusesByName = testStatusesByName;
			this.testMessagesByName = testMessagesByName; 
			soapUITestsStatus = findGeneralStatus();
			executionReport = constructExecutionReport();
		} catch (IOException ioe){
			throw new InstructionRuntimeException("SoapUI Test failed at result File construction", ioe);
		}
	}
	
	/**
	 * @return the execution status of the SoapUI test suite.
	 */
	public GeneralStatus getSoapUITestsStatus() {
		return soapUITestsStatus;
	}
	
	/**
	 * returns the execution report reconstructed from the messages received from SoapUI
	 * @return
	 */
	public File getExecutionReport(){
		return executionReport;
	}
	
	public List<String> getFailures(){
		List<String> testFailures = new ArrayList<String>();
		
		Set<String> names = testStatusesByName.keySet();
		for (String name : names) {
			if (testStatusesByName.get(name) != Status.FINISHED){
				String testSuite = name.split("::")[0];
				String testCase = name.split("::")[1];
				testFailures.add("Test case '" + testCase + "' from test suite '" + testSuite + "' has status : " + testStatusesByName.get(name));
			}
		}
		
		return testFailures;
	}

	@Override
	public SoapUIResult copy() {
		return new SoapUIResult(testStatusesByName, testMessagesByName);
	}

	@Override
	public void cleanUp() {
		//GC only for now, however we might need to think about it!
	}
	
	private GeneralStatus findGeneralStatus(){
		boolean allOK = true;
		Set<String> keys = testStatusesByName.keySet();
		for (String name : keys) {
			if (testStatusesByName.get(name) != Status.FINISHED){
				allOK = false;
			}
		}
		if (allOK){
			return GeneralStatus.SUCCESS;
		} else {
			return GeneralStatus.FAIL;
		}
	}
	
	private File constructExecutionReport() throws IOException {
		File tempFile = File.createTempFile("SoapUI_Result", ".txt", TempDir.getExecutionTempDir());
		List<String>dataLines=new ArrayList<String>();
		Set<String> names = testMessagesByName.keySet();
		for (String name : names) {
			dataLines.add(testMessagesByName.get(name));
			dataLines.add("");
			dataLines.add("");
			dataLines.add("--------------------------------------------");
			dataLines.add("");
			dataLines.add("");
		}
		SimpleLinesData fileData=new SimpleLinesData(dataLines);
		fileData.write(tempFile);
		return tempFile;
	}
}

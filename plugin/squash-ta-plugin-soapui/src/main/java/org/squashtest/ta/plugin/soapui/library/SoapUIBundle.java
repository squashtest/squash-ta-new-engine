/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.library;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.library.BundleComponent;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class can deals with SoapUI bundle, to make which differences needs to
 * be made between normal files and directory
 * 
 * @author cruhlmann
 *
 */
public abstract class SoapUIBundle extends BundleComponent {

	protected static final String PATH_KEY = "soapui.project.path";

	protected static final String CONF_ERROR_MESSAGE = "Wrongly configured SoapUI File\n";

	protected SoapUIBundle() {
	}

	protected SoapUIBundle(char optionsSeparator) {
		super(optionsSeparator);
	}

	/**
	 * Transform a configuration into a Properties, and look for the main file
	 * path in case of bundle
	 * 
	 * @param configuration
	 *            The configuration to transform, given in the USING clause
	 * @return Properties
	 */
	protected Properties addConf(Collection<Resource<?>> configuration) {
		Properties confProperties = new Properties();
		for (Resource<?> confElement : configuration) {
			if (confElement instanceof PropertiesResource) {
				confProperties = addPropertiesConfigurationSource(confProperties, confElement);
			} else if (confElement instanceof FileResource) {
				confProperties = addFileConfigurationSource(confProperties, confElement);
			} else {
				getLogger().warn("Unrecognized configuration resource will be ignored (type: "
						+ confElement.getClass().getName() + ")");
			}
		}
		return confProperties;
	}

	protected void checkXMLFileConformity(File file) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
			};

			saxParser.parse(file, handler);
		} catch (SAXException se) {
			throw new BadDataException(CONF_ERROR_MESSAGE, se);
		} catch (IOException ioe) {
			throw new BadDataException(CONF_ERROR_MESSAGE, ioe);
		} catch (ParserConfigurationException pce) {
			throw new BadDataException(CONF_ERROR_MESSAGE, pce);
		}
	}

	protected abstract Logger getLogger();

	private Properties addFileConfigurationSource(Properties confProperties, Resource<?> confElement) {
		try {
			FileResource confFile = (FileResource) confElement;
			if (isOptions(confFile.getFile())) {
				Map<String, String> optionsMap = getOptions(confFile.getFile());
				for (Entry<String, String> entry : optionsMap.entrySet()) {
					confProperties.setProperty(entry.getKey(), entry.getValue());
				}
			} else {
				throw new IllegalConfigurationException("Configuration resource " + confFile.getFile()
						+ " is not a valid ; separated options resource.");
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("Failed to open configuration resource.", e);
		}
		return confProperties;
	}

	private Properties addPropertiesConfigurationSource(Properties confProperties, Resource<?> confElement) {
		Properties properties = ((PropertiesResource) confElement).getProperties();
		Set<Object> keys = properties.keySet();
		for (Object object : keys) {
			confProperties.setProperty((String) object, properties.getProperty((String) object));
		}
		return confProperties;
	}
}

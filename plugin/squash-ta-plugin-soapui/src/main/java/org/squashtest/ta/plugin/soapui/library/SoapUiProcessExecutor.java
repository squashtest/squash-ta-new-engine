/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.library;

import com.eviware.soapui.SoapUI;
import org.squashtest.ta.plugin.commons.library.java.LoggingFixer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.xmlbeans.XmlException;
import org.squashtest.ta.framework.exception.BadDataException;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.model.support.PropertiesMap;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.model.testsuite.TestStep;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.model.testsuite.TestStepResult.TestStepStatus;
import com.eviware.soapui.model.testsuite.TestSuite;
import com.eviware.soapui.settings.HttpSettings;
import com.eviware.soapui.support.SoapUIException;
import org.slf4j.Logger;

/**
 * Entry point for the SoapUi external process.
 * 
 * @author edegenetais
 * 
 */
public class SoapUiProcessExecutor implements SoapUiProcessCommunication {
	private static final String DEFAULT_SOAPUI_TESTSUITE = "";

	private static final String DEFAULT_SOAPUI_TESTCASE = "";

	public static final String SOAPUI_TESTSUITE = "soapui.test.suites";

	public static final String SOAPUI_TESTCASE = "soapui.test.cases";
        
        private static final String LINE_SEPARATOR_PROPERTY = "line.separator";

	private String soapUITestSuite;

	private String soapUITestCase;

	private Map<String, Status> testStatusesByName = new HashMap<String, Status>();

	private Map<String, String> testMessagesByName = new HashMap<String, String>();

	private static final String LF = System.getProperty(LINE_SEPARATOR_PROPERTY);

        private static final Logger LOGGER=new LoggingFixer().getLogger(SoapUiProcessExecutor.class);
        
	/** Package accessible for testability purpose. */
	static class WsdlProjectFactory {
		public WsdlProject getWsdlProject(File soapUIFile) throws XmlException, IOException, SoapUIException {
			URI uri = soapUIFile.toURI();
			URL url = uri.toURL();
			String path = url.getProtocol() + "://" + url.getPath();
			return new WsdlProject(path);
		}
	}

	private WsdlProjectFactory wsdlProjectFactory = new WsdlProjectFactory();

	public SoapUiProcessExecutor(Properties soapUiConfig) {
		soapUITestSuite = soapUiConfig.getProperty(SOAPUI_TESTSUITE, DEFAULT_SOAPUI_TESTSUITE);
		soapUITestCase = soapUiConfig.getProperty(SOAPUI_TESTCASE, DEFAULT_SOAPUI_TESTCASE);
	}

	public void run(File soapUiFile, Properties target, Properties config)
			throws IOException, XmlException, SoapUIException {
		WsdlProject soapUIProject = wsdlProjectFactory.getWsdlProject(soapUiFile);
                //set Http reponse compression to false in order to avoid XML parsing issue
                SoapUI.getSettings().setBoolean(HttpSettings.RESPONSE_COMPRESSION, false);
		if (soapUIProject.getTestSuiteList().isEmpty()) {
			throw new SoapUIException("No test suite found. Please check SoapUI file " + soapUiFile.getAbsolutePath());
		}
		List<TestSuite> testSuites = retrieveWantedTestSuites(soapUIProject);

		executeTestSuites(soapUIProject, testSuites, target, config);

	}

	private void executeTestSuites(WsdlProject soapUIProject, List<TestSuite> testSuites, Properties target,
			Properties config) {
		for (TestSuite testSuite : testSuites) {
			List<TestCase> testCases = retrieveWantedTestCases(soapUIProject, testSuite);
			executeTestCases(testSuite.getName(), testCases, target, config);
		}
	}

	private void executeTestCases(String testSuiteName, List<TestCase> testCases, Properties target,
			Properties config) {
		for (TestCase testCase : testCases) { 
			if (target != null) {
				Enumeration<?> propertyNames = target.propertyNames();
				while (propertyNames.hasMoreElements()) {
					String key = (String) propertyNames.nextElement();
					testCase.setPropertyValue(key, target.getProperty(key));
				}
			}
			
			Enumeration<?> configPropertyNames = config.propertyNames();
			while (configPropertyNames.hasMoreElements()) {
				String key = (String) configPropertyNames.nextElement();
				testCase.setPropertyValue(key, config.getProperty(key));
			}
			
			TestCaseRunner runner = testCase.run(new PropertiesMap(), false);
			if (Status.FINISHED != runner.getStatus()) {
				testStatusesByName.put(testSuiteName + "::" + testCase.getName(), runner.getStatus());
				testMessagesByName.put(testSuiteName + "::" + testCase.getName(), createErrorMessage(runner));
			} else {
				testStatusesByName.put(testSuiteName + "::" + testCase.getName(), runner.getStatus());
			}
		}
	}

	private List<TestSuite> retrieveWantedTestSuites(WsdlProject soapUIProject) {
		List<TestSuite> testSuites;

		if ("".equals(soapUITestSuite)) {
			testSuites = soapUIProject.getTestSuiteList();
		} else {
			testSuites = new ArrayList<TestSuite>();
			String[] testSuitesTab = soapUITestSuite.split(",");
			for (int i = 0; i < testSuitesTab.length; i++) {
				testSuites.add(testExistingTestSuite(soapUIProject, testSuitesTab[i].trim()));
			}
		}

		return testSuites;
	}

	private TestSuite testExistingTestSuite(WsdlProject soapUIProject, String testSuiteName) {
		TestSuite testSuite = soapUIProject.getTestSuiteByName(testSuiteName);
		if (testSuite == null) {
			// We show the existing names of test suites in the project
			StringBuffer msg = new StringBuffer(
					"Test suite '" + testSuiteName + "' not found. Valid test suite names are:");
			List<TestSuite> testSuites = soapUIProject.getTestSuiteList();
			for (TestSuite suite : testSuites) {
				String message = System.getProperty(LINE_SEPARATOR_PROPERTY) + "- " + suite.getName();
				msg.append(message);
			}
			throw new BadDataException(msg.toString());
		}
		return testSuite;
	}

	private List<TestCase> retrieveWantedTestCases(WsdlProject soapUIProject, TestSuite testSuite) {
		List<TestCase> testCases;

		if ("".equals(soapUITestCase)) {
			testCases = testSuite.getTestCaseList();
		} else {
			testCases = new ArrayList<TestCase>();
			String[] testCasesTab = soapUITestCase.split(",");
			for (int i = 0; i < testCasesTab.length; i++) {
				TestCase testCase = testExistingTestCase(soapUIProject, testSuite.getName(), testCasesTab[i].trim());
				if (testCase != null) {
					testCases.add(testCase);
				}
			}
		}

		return testCases;
	}

	private TestCase testExistingTestCase(WsdlProject soapUIProject, String testSuiteName, String testCaseName) {
		TestSuite testSuite = soapUIProject.getTestSuiteByName(testSuiteName);
		TestCase testCase = testSuite.getTestCaseByName(testCaseName);
		if (testCase == null) {
			// We then check if the testCase exists in a different testSuite of
			// the project
			List<TestSuite> testSuites = soapUIProject.getTestSuiteList();
			for (TestSuite testSuite2 : testSuites) {
				if (!testSuiteName.equals(testSuite2.getName()) && testCase == null) {
					// If the testCase is still null, we try to find it in
					// another testSuite than the one given
					testCase = testSuite2.getTestCaseByName(testCaseName);
				}
			}
			if (testCase == null) {
				// If the testCase is not found in any of the testSuites, we
				// raise an exception
				StringBuffer msg = new StringBuffer("Test case '" + testCaseName
						+ "' not found in any testSuite. Valid test case names for the test suite '" + testSuiteName
						+ "' are:");
				List<TestCase> testCases = testSuite.getTestCaseList();
				for (TestCase testCase2 : testCases) {
					String message = System.getProperty(LINE_SEPARATOR_PROPERTY) + "- " + testCase2.getName();
					msg.append(message);
				}
				throw new BadDataException(msg.toString());
			} else {
				// If the testCase is present in another testSuite, we return
				// null, we don't want it here
				return null;
			}
		}
		return testCase;
	}

	private String createErrorMessage(TestCaseRunner runner) {

		StringBuffer msg = new StringBuffer("TestCase '" + runner.getTestCase().getName() + "' of TestSuite '"
				+ runner.getTestCase().getTestSuite().getName() + "' : " + runner.getStatus());

		List<TestStepResult> testStepResults = runner.getResults();

		for (TestStepResult testStepResult : testStepResults) {

			// Step that failed
			if (TestStepStatus.OK != testStepResult.getStatus()) {
				msg.append(createStepErrorMessage(testStepResult));
			}

			// Response
			if (testStepResult instanceof WsdlTestRequestStepResult) {
				WsdlTestRequestStepResult wsdlTestRequestStepResult = (WsdlTestRequestStepResult) testStepResult;
				String message = LF + LF + "--> Response was :" + LF + wsdlTestRequestStepResult.getResponseContent();
				msg.append(message);
			}

		}
		return msg.toString();
	}

	public void emmittResult() {
		for (Entry<String, Status> entry : testStatusesByName.entrySet()) {
			System.out.println("STATUS");
			System.out.println(entry.getValue().name());
			System.out.println(entry.getKey());
		}
		for (Entry<String, String> entry : testMessagesByName.entrySet()) {
			System.out.println("MESSAGE");
			String[] lines = entry.getValue().split("\n");
			for (String line : lines) {
				if ("ESCAPE:".equals(line) || "ENDMESSAGE:".equals(line)) {
					System.out.println("ESCAPE:" + line);
				} else {
					System.out.println(line);
				}
			}
			System.out.println("ENDMESSAGE:" + entry.getKey());
		}
	}

	public static void main(String[] args) {
		File soapUiFile = new File(args[0]);
		File configFile = new File(args[1]);

		// Handles cases with or without target
		File targetFile = null;
		if (args.length == 3) {
			targetFile = new File(args[2]);
		}

		Properties soapUiConfig = new Properties();
		Properties soapUiTarget = new Properties();

		InputStream configStream = null;
		InputStream configStreamTarget = null;

		try {
			configStream = new FileInputStream(configFile);
			soapUiConfig.load(configStream);

			if (targetFile != null) {
				configStreamTarget = new FileInputStream(targetFile);
				soapUiTarget.load(configStreamTarget);
			}

			SoapUiProcessExecutor executor = new SoapUiProcessExecutor(soapUiConfig);
			executor.run(soapUiFile, soapUiTarget, soapUiConfig);

			executor.emmittResult();

			System.out.println("End of SoapUi execution process");

			System.exit(0);// this should not be necessary, however the process
							// appears to go on indefinitely for some reason...

		} catch (IOException ioe) {
			LOGGER.error("SoapUIProcessExecutor I/O error.", ioe);
			System.exit(INPUT_FILE_IO_ERROR);
		} catch (XmlException e) {
			LOGGER.error("SoapUIProcessExecutor Xml error.",e);
			System.exit(WORKSPACE_XML_EXCEPTION);
		} catch (SoapUIException e) {
			LOGGER.error("SoapUIProcessExecutor SoapUI error.",e);
			System.exit(WORKSPACE_SOAPUI_EXCEPTION);
		} catch (BadDataException e) {
			LOGGER.error("SoapUIProcessExecutor got bad data from calling TA test.",e);
			System.exit(BAD_DATA_EXCEPTION);
		} catch (RuntimeException e) {
			LOGGER.error("SoapUIProcessExecutor unrecognized error.",e);
			System.exit(1);
		}
		try {
			configStream.close();
		} catch (IOException ioe) {
			LOGGER.error("SoapUIProcessExecutor filed to close configuration file.",ioe);
		}
	}

	private Object createStepErrorMessage(TestStepResult testStepResult) {
		TestStep step = testStepResult.getTestStep();
		StringBuffer msg = new StringBuffer();
		String message = LF + LF + "--> Step '" + step.getName() + "' : " + testStepResult.getStatus() + " : ";
		msg.append(message);
		String[] messages = testStepResult.getMessages();
		int len = messages.length;
		if (len > 0) {
			String msg0 = LF + "- " + messages[0];
			msg.append(msg0);
			for (int i = 1; i < len; i++) {
				String msgi = LF + "- " + messages[i];
				msg.append(msgi);
			}
		}
		return msg.toString();
	}
}

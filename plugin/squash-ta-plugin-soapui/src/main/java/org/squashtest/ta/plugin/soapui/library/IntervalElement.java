/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.library;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eviware.soapui.model.testsuite.TestRunner.Status;
import org.squashtest.ta.plugin.commons.library.java.Element;
import org.squashtest.ta.plugin.commons.library.java.LoggingFixerProtocol;

class IntervalElement implements Element<Status>{

	private static final Logger LOGGER = LoggerFactory.getLogger(IntervalElement.class);
        private static final LoggingFixerProtocol LOGGING_FIXER=new LoggingFixerProtocol();
        
	@Override
	public Element add(String input, Map<String, Status> statusMap,
			Map<String, String> messageMap) {
		Element el;
		if("STATUS".equals(input)){
			el=new StatusElement();
			LOGGER.debug("Serialized test status on the pipe.");
		}else if("MESSAGE".equals(input)){
			el=new MessageElement();
			LOGGER.debug("Serialized test message on the pipe.");
                }else if(LOGGING_FIXER.accept(input)){
                        el=LOGGING_FIXER.getPipeElement(this);
		}else{
			LOGGER.debug("SoapUI message: '"+input+"'");
			el=this;
		}
		return el;
	}

    
	
}
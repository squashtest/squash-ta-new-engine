/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.converters;

import java.io.File;
import java.util.Collection;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.plugin.soapui.library.SoapUIBundle;
import org.squashtest.ta.plugin.soapui.resources.SoapUIScript;

/**
 * File To XML Converter
 * Converts a File entry (XML File obviously) into an XMLResource Type 
 * (encapsulated XML File which proves that the XML is "well formed")
 * <p> The file must a correct XML implementation<p>
 * 
 * @author fgaillard
 *
 */
@TAResourceConverter("structured")
public class FileToSoapUI extends SoapUIBundle implements ResourceConverter<FileResource, SoapUIScript> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileToSoapUI.class);
	
	private Properties confProperties = new Properties();
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public FileToSoapUI(){
            //default constructor
        }
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.confProperties = addConf(configuration);
	}

	@Override
	public SoapUIScript convert(FileResource resource) {
		
		String pathFound = null;
		
		if (confProperties.containsKey(PATH_KEY)){
			pathFound = (String) confProperties.get(PATH_KEY);
		}
		
		checkXMLConformity(resource.getFile(), pathFound);
		
		SoapUIScript script = new SoapUIScript(resource.getFile());
		
		if (pathFound != null){
			script.setMainFilePath(pathFound);
		}
		return script;
	}

	@Override
	public void cleanUp() {
		//noop
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
	
	/** will test a file to see if it is valid XML
	 * @param file The file to test
	 */
	private void checkXMLConformity(File file, String mainFilePath) {
		if (file.isDirectory()){
			//if this is a directory and no path was given through the using clause, we don't do anything
			if (mainFilePath != null){
				//if a path is given, we check its file conformity
				checkXMLFileConformity(new File (file, mainFilePath));
			}
		}else{
			//if this is not a directory, we check it itself
			checkXMLFileConformity(file);
		}			
	}
}

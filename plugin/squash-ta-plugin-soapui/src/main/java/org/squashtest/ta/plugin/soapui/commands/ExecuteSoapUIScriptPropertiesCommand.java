/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.soapui.library.SoapUIBundle;
import org.squashtest.ta.plugin.soapui.library.SquashSoapUIRunner;
import org.squashtest.ta.plugin.soapui.resources.SoapUIResult;
import org.squashtest.ta.plugin.soapui.resources.SoapUIScript;
import org.squashtest.ta.plugin.soapui.targets.SoapUITarget;

import com.eviware.soapui.support.SoapUIException;

/**
 * This command executes a SoapUI xml script given as input resource ({@link SoapUIScript}) with a SoapUITarget
 * <p><strong>Returns : </strong> {@link #apply()} returns a {@link SoapUIResult}</p>
 * 
 * @author kdrifi
 *
 */
@TACommand("execute")
public class ExecuteSoapUIScriptPropertiesCommand extends SoapUIBundle implements Command<SoapUIScript, SoapUITarget> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExecuteSoapUIScriptCommand.class);

	static class SoapUIRunnerFactory{
		public SquashSoapUIRunner getRunner(Properties configuration, Properties target){
			return new SquashSoapUIRunner(configuration, target);
		}
	}
	
	private SoapUIRunnerFactory runnerFactory = new SoapUIRunnerFactory();
		
	private SoapUIScript soapUIResource;

	private SoapUITarget target;
	
	private Properties confProperties;
	
	private Properties targetProperties;
	
	/** Noarg constructor for Spring enumeration. */
	public ExecuteSoapUIScriptPropertiesCommand(){
		super(';');//we use ';' as options separator because here configuration keys are comma-separated lists
	}
	
	/**
	 * Two configuration formats here:
	 * 	- properties resource to get configuration from properties files
	 *  - file resource to get configuration from inline definition (as ; separated options because each option value is a comma separated name list).
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {	
		this.confProperties = addConf(configuration);
		String pathFound = (String) confProperties.get(PATH_KEY);
		if (pathFound != null){
			checkXMLFileConformity(new File (soapUIResource.getSoapUIFile(), pathFound));
			soapUIResource.setMainFilePath(pathFound);
		}
	}

	@Override
	public void setResource(SoapUIScript resource) {
		soapUIResource=resource;
	}

	@Override
	public SoapUIResult apply() {
		targetProperties = target.getConfiguration();
		SquashSoapUIRunner runner = runnerFactory.getRunner(confProperties, target.getConfiguration());
		try{
			File soapUIFile = getCorrectFile(soapUIResource.getSoapUIFile());
			return runner.run(soapUIFile, target.getConfiguration());
		} catch(SoapUIException suie){
			throw new InstructionRuntimeException("SoapUI Test failed", suie);
		} catch (IOException ioe) {
			throw new InstructionRuntimeException("SoapUI test failed from IO problem.", ioe);
		} catch (XmlException xmle) {
			throw new InstructionRuntimeException("SoapUI test failed from XML exception.", xmle);
		}
	}

	@Override
	public void cleanUp() {
            //noop
	}

	@Override
	public void setTarget(SoapUITarget target) {
		this.target = target;
		this.targetProperties = target.getConfiguration();
	}

        public Properties getTargetProperties() {
            return targetProperties;
        }
        
	@Override
	protected Logger getLogger() {
		return LOGGER;
	} 	
	
	/** will return the good file to run with (either itself, or
	 * the one given in configuration)
	 * @param soapUiFile the SoapFile or Directory given as Resource
	 * @return the File to run with
	 */
	private File getCorrectFile(File soapUiFile) {
		File correctFile;
		if (soapUiFile.isDirectory()){
			String mainFilePath = soapUIResource.getMainFilePath();
			if (mainFilePath == null){
				throw new BadDataException(CONF_ERROR_MESSAGE + "As you have loaded a directory, you must give the path of the XML workspace in configuration.");
			}else{
				correctFile = new File(soapUiFile, mainFilePath);
			}
		}else{
			correctFile = soapUiFile;
		}
		return correctFile;
	}
}

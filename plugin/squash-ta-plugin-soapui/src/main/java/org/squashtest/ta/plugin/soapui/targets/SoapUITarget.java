/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.soapui.targets;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.squashtest.ta.framework.annotations.TATarget;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;

/**
 * <p>
 * SoapUI application target. This manages all the parameters and properties
 * needed for a SoapUI test.
 * </p>
 * 
 * @author kdrifi
 *
 */
@TATarget("soapui")
public class SoapUITarget implements Target {

	static final String NAME_SPACE = "squashtest.ta.soapui";

	static final String ENDPOINT_URL_KEY = NAME_SPACE + ".endpoint.url";
	static final String PROXY_HOST_KEY = NAME_SPACE + ".proxy.host";
	static final String PROXY_PORT_KEY = NAME_SPACE + ".proxy.port";
	static final String PROXY_HOST_EXCLUDES = NAME_SPACE + ".proxy.excludes";

	private static final String HTTPS_PROTOCOL = NAME_SPACE + ".https";

	private Properties configuration;

	private URL url;
	private Integer proxyPort;
	private String proxyHost;
	private List<String> proxyHostExcludes;

	private boolean https;

	/** noarg constructor for Spring enumeration. */
	public SoapUITarget() {
	}

	/**
	 * Constructor for a valid object.
	 * 
	 * @param configuration
	 */
	public SoapUITarget(Properties configuration) {
		this.configuration = configuration;
		try {
			url = new URL(configuration.getProperty(ENDPOINT_URL_KEY));
			https = (HTTPS_PROTOCOL.equals(url.getProtocol()));
			proxyHost = configuration.getProperty(PROXY_HOST_KEY);
			// TODO : Configure urls to exclude when using proxy
			// proxyHostExcludes =
			// Arrays.asList(configuration.getProperty(PROXY_HOST_EXCLUDES).split(","));

			String pPort = configuration.getProperty(PROXY_PORT_KEY);
			if (pPort != null) {
				proxyPort = Integer.parseInt(pPort.trim());
			}
		} catch (MalformedURLException e) {
			throw new IllegalConfigurationException("SoaUITarget: endpoint.url is not valid", e);
		}
	}

	@Override
	public boolean init() {
		// noop
		return true;
	}

	@Override
	public void reset() {
		// TODO
	}

	@Override
	public void cleanup() {
		// TODO
	}

	@Override
	public Properties getConfiguration() {
		Properties returndProperties = new Properties();
		returndProperties.putAll(configuration);
		return returndProperties;
	}

	/**
	 * Get the URL from this target.
	 * 
	 * @return
	 */
	public URL getUrl() {
		return url;
	}

	public Integer getProxyPort() {
		return proxyPort;
	}

	public String getProxyHost() {
		return proxyHost;
	}

        public boolean isHttps() {
                return https;
        }
        
	public List<String> getProxyHostExcludes() {
		return proxyHostExcludes;
	}

	public boolean usesProxy() {
		return ((proxyHost != null) && (proxyPort != null));
	}

	public boolean usesProxyExcludes() {
		return (proxyHostExcludes != null);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.assertions

import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.selenium.assertions.SeleniumIsSuccess
import org.squashtest.ta.plugin.selenium.commands.ExecuteJavaSeleniumTwoCommand
import org.squashtest.ta.plugin.selenium.library.SurefireReportExporter
import org.squashtest.ta.plugin.selenium.resources.SeleniumTwoJavaTest

import spock.lang.Specification

class SeleniumIsSuccessTest extends Specification {

	def "Assertion should fail"(){
		
		given :
			
			def mainClass = Class.forName("org.squashtest.ta.plugin.selenium.stuffs.SampleJUnit3")
		
			def res = Mock(SeleniumTwoJavaTest)
			res.getMainClass() >> mainClass
			
			def command = new ExecuteJavaSeleniumTwoCommand()
			def success = new SeleniumIsSuccess()
			
			command.setResource(res)
			def result = command.apply()
			
		when :
			success.setActualResult(result)
			success.test()
		then :
			thrown(AssertionFailedException)
	}
	
	def "Assertion should generate a report"(){
		
		given :
			
			def mainClass = Class.forName("org.squashtest.ta.plugin.selenium.stuffs.SampleJUnit3")
			
			def res = Mock(SeleniumTwoJavaTest)
			res.getMainClass() >> mainClass
			
			def command = new ExecuteJavaSeleniumTwoCommand()
			def success = new SeleniumIsSuccess()
			
			command.setResource(res)
			def result = command.apply()
			def report = SurefireReportExporter.getInstance()
			
		when :
			File reportFile = report.generateReport(result)
			String[] tab = reportFile.getPath().split("\\.")
			String extension = tab[tab.length -1]
			String stringReport = reportFile.text
			String[] tabReport = stringReport.split(/<|>/)
			
		then :
			reportFile != null
			extension.equals("xml")
			tabReport[5] == "testcase time=\"0.003\" classname=\"org.squashtest.ta.plugin.selenium.stuffs.SampleJUnit3\" name=\"testOops\""
			tabReport[7] == "error message=\"expected:&lt;Howdy [Mike] !&gt; but was:&lt;Howdy [Bob] !&gt;\" type=\"junit.framework.ComparisonFailure\""
	}
}

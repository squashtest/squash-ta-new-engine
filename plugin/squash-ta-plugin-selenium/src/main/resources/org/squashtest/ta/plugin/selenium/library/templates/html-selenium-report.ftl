<#--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Execution Selenium report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="../../../public/stylesheets/knacss.css" media="all" />
    <link rel="icon" type="image/png" href="../../../public/images/logo_squash_favicone.ico" />
    <link rel="shortcut icon" type="image/x-icon" href="../../../public/images/logo_squash_favicone.ico" />
</head>

<body class="ml2 mr2 mt1">

    <div id="header" class="row">
        <div class="col w20">
            <img class="left" src="../../../public/images/logo_squash.png" alt="Logo Squash" />
        </div>
        <div class="col-mid txtcenter">
            <h1>Selenium report</h1>
        </div>
    </div>
    
    <div id="content">
        <div id="summary"> 
                
            <div class="info">
                <span>Execution date:&nbsp;</span>${timeNow}
                <br/>
            </div>
        
            <table class="w100 striped">
                <thead>
                    <tr class="table-header">
                        <th>Test</th>
                        <th class="w10">Method (s)</th>
                        <th class="w10">Time (ms)</th>
                    </tr>
                </thead>
                
                <tbody>
                    <td>
                        <#list failures as failure>
                            ${failure.getDescription().getClassName()}
                            <#break>
                        </#list>    
                    </td>
                    <td>
                        <#global cpt = 0/>
                        <#global methodName = ""/>                        
                        <#list failures as failure>
                            <#if methodName != failure.getDescription().getMethodName()>
                                <a href="#method${cpt}">${failure.getDescription().getMethodName()}</a>
                                <#global methodName = failure.getDescription().getMethodName()/>  
                                <#global cpt = cpt+1/>
                            </#if>
                        </#list>                    
                    </td>
                    <td>
                        ${result.getResult().getRunTime()}
                    </td>                        
                </tbody>                    
            </table>
            
            <div class="ecosystem"> 
          
                <div class="ecosystem-header">
                    <h2>
                        <#list failures as failure>
                            ${failure.getDescription().getClassName()}
                            <#break>
                        </#list> 
                    </h2>
                </div>
            
                <div class="ecosystem-body">   
            
                    <#global cpt = 0/>
                    <#global methodName = ""/>
                
                    <#list failures as failure>
                 
                        <#if methodName != failure.getDescription().getMethodName()>

                            <#if cpt != 0 >            
                                </div> <#-- END DIV SCRIPT -->         
                            </#if>

                            <div class="script">  
                                <div class="script-header">
                                    <h3 id="method${cpt}">${failure.getDescription().getMethodName()}</h3> 
                                    <#global methodName = failure.getDescription().getMethodName()/>                           
                                </div>
                                 
                            <@failureList failure=failure cpt=cpt/>
                                                
                        <#else>                                   
                            <@failureList failure=failure cpt=cpt/>
                            
                        </#if>    
                    </#list>
 
                            </div> <#-- END DIV SCRIPT -->
       
                </div> <#-- END DIV ECOSYSTEM-BODY --> 
            </div> <#-- END DIV ECOSYSTEM -->        
                  
        </div> <#-- END DIV SUMMARY -->                
    </div> <#-- END DIV CONTENT -->
    
    <div class="footer txtcenter">
        <p>Generated by Squash Keyword Framework Selenium plugin&copy; on ${timeNow}</p>
    </div>
    <script type="text/javascript" src="../../../public/javascripts/dropdown.js" ></script>
</body>
</html>


<#macro dropdown>
    <div id="toggle${cpt}-expand" class="col-mid toggle-button">
        <a class="toggleClass" name="toggle${cpt}"><img src="../../../public/images/expand.png"/></a>
    </div>
    <div id="toggle${cpt}-collapse" class="desktop-hidden toggle-button">
        <a class="toggleClass" name="toggle${cpt}"><img src="../../../public/images/collapse.png"/></a>
    </div>
</#macro>

<#macro failureList failure cpt>
    <div class="script-content">
        ${failure.getException()}
    </div>                            
    <div class="stack"> 
        <div class="row">
            <@dropdown/>
            <div class="col">                
                <table class="w100 striped">
                    <thead>
                        <tr class="table-header">
                            <th>Trace</th>
                        </tr>
                    </thead>
                </table>                
            </div>
        </div>
    
        <div class="script-content">
            <div id="toggle${cpt}" class="desktop-hidden small col trace">
                <pre>
                    ${failure.getTrace()}
                </pre>
            </div>
            <#global cpt = cpt+1/>
        </div>
    </div>  
</#macro>



/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.converters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle;
import org.squashtest.ta.plugin.selenium.library.JUnitTester;

public abstract class AbstractJavaBundleToJavaSeleniumConverter {
	
	protected String mainSeleniumClass;
	protected JUnitTester junitTester = JUnitTester.getInstance();
	
	protected static final String ERROR_MESSAGE = "Java Code Bundle does not have any Junit type class";
        
	protected abstract Logger getLogger();        
	
	public float rateRelevance(JavaCodeBundle input) {
		return 0.5f;
	}
	
	public void addConfiguration(Collection<Resource<?>> configuration) {
		// The main class of the selenium test suite can be passed through configuration
		try{
			for (Object object : configuration) {
				if (object instanceof FileResource) {
					FileResource fileConf = (FileResource) object;
					SimpleLinesData confLines=new SimpleLinesData(fileConf.getFile().getAbsolutePath());
					for(String conf:confLines.getLines()) {
						setMainSeleniumClass(conf);
					}
				}
			}
		} catch (FileNotFoundException fnfe) {
			throw new BadDataException("Configuration not found\n",fnfe);
		} catch (IOException ioe) {
			throw new BadDataException("Configuration not found\n",ioe);
		}
	}
	
	public void cleanUp() {
		// no cleanup necessary
	}
	
	protected void logError(String message){
		if (getLogger().isErrorEnabled()){
			getLogger().error(message);
		}
	}
	
	protected boolean containsJUnitClasses(JavaCodeBundle resource){
		boolean jUnit = false;
		ClassLoader bundleClassLoader = resource.getDedicatedClassloader();
		Set<String> classesInBundle = resource.getBundleClassNames();
		try {
			if (mainSeleniumClass != null){
				//first if the mainSeleniumClass has been defined we check to see if it is a Junit Type
				if (junitTester.isJunitClass(bundleClassLoader.loadClass(mainSeleniumClass))){
					jUnit = true;
				}
			} else {
				for (String className : classesInBundle) {
					Class<?> clazz = bundleClassLoader.loadClass(className);
					if (junitTester.isJunitClass(clazz)){
						jUnit = true;
					}
				}
			}
		} catch (ClassNotFoundException cnfe) {
			throw new BadDataException("Class not found", cnfe);
		}
		return jUnit;
	}
	
	private void setMainSeleniumClass(String mainSeleniumClass) {
		this.mainSeleniumClass = mainSeleniumClass;
	}
}

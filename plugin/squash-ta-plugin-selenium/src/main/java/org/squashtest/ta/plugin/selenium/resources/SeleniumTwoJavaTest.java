/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.selenium.library.JavaExecutableUnit;

@TAResource("script.java.selenium2")
public class SeleniumTwoJavaTest extends AbstractSeleniumJavaTest implements Resource<SeleniumTwoJavaTest>, JavaExecutableUnit {
	
	public SeleniumTwoJavaTest(){
		super();
	}
	
	public SeleniumTwoJavaTest(ClassLoader loader){
		super(loader);
	}
	
	public SeleniumTwoJavaTest(ClassLoader loader, String mainClassName){
		super(loader,mainClassName);
	}
		
	@Override
	public SeleniumTwoJavaTest copy() {
		if (mainClass != null){
			return new SeleniumTwoJavaTest(dedicatedClassLoader, mainClass.getName()); 
		}
		else{
			return new SeleniumTwoJavaTest(dedicatedClassLoader);
		}
	}

	@Override
	protected Logger getLogger() {
		return LoggerFactory.getLogger(SeleniumTwoJavaTest.class);
	}	
}

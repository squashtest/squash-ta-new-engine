/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.resources;

import org.slf4j.Logger;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

public abstract class AbstractSeleniumJavaTest {
	
	protected ClassLoader dedicatedClassLoader;
	protected Class<?> mainClass;
		
	protected AbstractSeleniumJavaTest() {
		super();
	}
	
	protected AbstractSeleniumJavaTest(ClassLoader loader){
		super();
		setClassLoader(loader);
	}
	
	protected AbstractSeleniumJavaTest(ClassLoader loader, String mainClassName){
		this(loader);
		setMainClass(mainClassName);
	}
        
	protected abstract Logger getLogger();        
	
	private InstructionRuntimeException logAndThrow(String message, Exception exception){
		if (getLogger().isErrorEnabled()){
			getLogger().error(message);
		}
		return new InstructionRuntimeException(message, exception);
		
	}
	
	public void cleanUp() {
		// noop
	}
	
	private void setClassLoader(ClassLoader loader){
		this.dedicatedClassLoader=loader;
	}
	
	/**
	 * will look for the class using the fully qualified name, and the dedicated classloader. So be sure to set the 
	 * dedicated class loader first.
	 * 
	 * @param fullClassName
	 */
	private void setMainClass(String fullClassName){
		try {
			this.mainClass=dedicatedClassLoader.loadClass(fullClassName);
		} catch (ClassNotFoundException e) {
			throw logAndThrow("selenium test : main test class '"+ fullClassName +"'not found.", e);
		}
	}
	
	// ********** from JavaExecutableUnit ********* 
	
	public Class<?> getMainClass(){
		return mainClass;
	}
	
	public ClassLoader getDedicatedClassloader() {
		return dedicatedClassLoader;
	}
}

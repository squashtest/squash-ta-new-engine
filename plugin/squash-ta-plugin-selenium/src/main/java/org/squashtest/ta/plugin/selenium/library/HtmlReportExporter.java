/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.library;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.runner.notification.Failure;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.selenium.resources.SeleniumResult;

import freemarker.template.Configuration;
import freemarker.template.Template;
import static org.squashtest.ta.commons.exporter.html.HtmlResultExporterBase.fillTempHTLMFile;


/**
 * A Class to generate html report selenium which uses freemarker (http://freemarker.org/)
 * 
 * @author kguilloux
 * 
 * **/
public class HtmlReportExporter {

	private static final HtmlReportExporter INSTANCE = new HtmlReportExporter();
        
        private List<Failure> failures = new ArrayList<Failure>();
        
        private HtmlReportExporter() {
            //don't instanciate
	}

	public static HtmlReportExporter getInstance() {
		return INSTANCE;
	}

	public File generateHtmlReport(SeleniumResult seleniumResult) throws IOException {

		// temporary file creation
		File tempHTMLFile = this.generateReport(seleniumResult);

		// Freemarker configuration object
		Configuration configuration = new Configuration();

		// Load template from source folder
		configuration.setClassForTemplateLoading(HtmlReportExporter.class, "templates");
		Template template = configuration.getTemplate("html-selenium-report.ftl");

		// Build the data-model
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", seleniumResult);
		
		failures = seleniumResult.getFailures();
		map.put("failures", failures);		
		Date today = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
		map.put("timeNow", dateFormat.format(today));		
		
		// File output
                fillTempHTLMFile(map,template, tempHTMLFile);
		return tempHTMLFile;
	}
	
	public File generateReport(SeleniumResult seleniumResult) throws IOException {
		return File.createTempFile("TEST-jUnitReport", "Test.html", TempDir.getExecutionTempDir());
	}
}

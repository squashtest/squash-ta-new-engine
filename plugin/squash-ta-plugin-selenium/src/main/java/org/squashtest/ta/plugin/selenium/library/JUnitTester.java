/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.library;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import junit.framework.TestCase;

public class JUnitTester {
    
    	private static final JUnitTester INSTANCE = new JUnitTester();
	
	/**
	 * empty constructor for Spring instanciation purposes
	 */
	public JUnitTester(){
            //default constructor
        }
	
        
	public static JUnitTester getInstance(){
		return INSTANCE;
	}
	
	public boolean isJunitClass(Class<?> clazz){
		boolean result = false;
		//we first check if the class implements the TestCase interface
		if (TestCase.class.isAssignableFrom(clazz)){
			result = true;
		}
		if (!result){
			//if not, we check if the class has the junit test annotations
			Method[] classMethods = clazz.getDeclaredMethods();
			for (Method method : classMethods) {
				Annotation[] methodAnnotations = method.getAnnotations();
				for (Annotation annotation : methodAnnotations) {	
					Package pack = annotation.annotationType().getPackage();
					if ("org.junit".equals(pack.getName())){
						result = true;
					}
				}
			}
		}
		return result;
	}
}

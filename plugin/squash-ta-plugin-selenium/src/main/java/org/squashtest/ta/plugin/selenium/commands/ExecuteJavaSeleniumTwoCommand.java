/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.commands;

import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import org.junit.runner.Result;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.plugin.commons.library.PrintStreamTee;
import org.squashtest.ta.plugin.selenium.library.StandardJUnitExecutor;
import org.squashtest.ta.plugin.selenium.resources.SeleniumResult;
import org.squashtest.ta.plugin.selenium.resources.SeleniumTwoJavaTest;


/**
 * <p><strong>Description : </strong>That command will simply run a Selenium 2 test. Actually, it mostly runs a JUnit Test but treats it 
 * as a Selenium Test. It doesn't need any Target, thus the target is VoidTarget.
 *  The result of the java test will be returned as a {@link SeleniumResult}.</p>
 *  
 *  <div><strong>Configuration</strong> : a FileResource which entries are comma separated pairs of {@literal <key:value>} (note that column ':' is the separator)
 *  Available options :
 *  <ul>
 *  	<li>mainclass : the full class name of the main class. Optional, if not supplied it will use on the one set in the Resource instead.</li>
 *  </ul>
 *  </div>
 * 
 *  <div>
 * <strong>DSL example :</strong>
 * <pre>{@literal EXECUTE execute WITH mytest.script.selenium USING $(mainclass : my.package.MyClass) AS my.selenium.result}</pre>
 * </div>
 * 
 * 
 * @author bsiri
 */

@TACommand("execute")
public class ExecuteJavaSeleniumTwoCommand extends AbstractExecuteJavaSeleniumCommand implements Command<SeleniumTwoJavaTest, VoidTarget>  {

	private StandardJUnitExecutor executor = new StandardJUnitExecutor();
	
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		executor.addConfiguration(configuration);
	}

	@Override
	public void setTarget(VoidTarget target) {
		executor.setTarget(target);
	}

	@Override
	public void setResource(SeleniumTwoJavaTest resource) {
		executor.setResource(resource);
	}

	@Override
	public SeleniumResult apply() {
		PrintStream out=System.out;
		PrintStreamTee outListener=new PrintStreamTee(out);
		Result result;
		final HashSet<File> attachements = new HashSet<File>();
		try{
		    System.setOut(outListener);
		    result=executor.apply();
		}finally{
                    System.setOut(out);
                    getAttachements(outListener, attachements);
                }
            
		return new SeleniumResult(result, attachements);
	}

	@Override
	public void cleanUp() {
		//nothing yet
	}


	
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.commons.helpers.AttachmentMangementHelper;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.selenium.library.HtmlReportExporter;
import org.squashtest.ta.plugin.selenium.resources.SeleniumResult;

/**
 * This assertion checks that a Selenium test succeeded. If the assertion fails.
 * The exception thrown contains the names of the failed selenium junit tests
 * @author fgaillard
 *
 */

@TAUnaryAssertion("success")
public class SeleniumIsSuccess implements UnaryAssertion<SeleniumResult>{

	private static final Logger LOGGER=LoggerFactory.getLogger(SeleniumIsSuccess.class);
	
	private SeleniumResult seleniumResult;
	
	private HtmlReportExporter htmlReportExporter = HtmlReportExporter.getInstance();
	
	@Override
	public void setActualResult(SeleniumResult actual) {
		this.seleniumResult=actual;
	}

	/**
	 * For this assertion, no configuration is necessary. Any injected resource will be ignored.
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public void test() throws AssertionFailedException {
		ArrayList<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
		try{
                    if(!seleniumResult.isSuccess()){
				
                            StringBuffer failedTestNames = buildFailureContext(context);
                                
				throw new AssertionFailedException("Selenium test failed. Following are the name of the tests that failed:\n" + failedTestNames.toString(), seleniumResult, context);
			}
		} catch (IOException ioe) {
			AssertionFailedException reportErrorException=new AssertionFailedException("Selenium test failed. Due to an error, the report could not be generated", seleniumResult, context);
			reportErrorException.initCause(ioe);//too sad Sonar shuns initCause()
			throw reportErrorException;//NOSONAR
		}
	}

    private StringBuffer buildFailureContext(ArrayList<ResourceAndContext> context) throws IOException {
        //We retrieve the Selenium tests that failed
        List<Failure> seleniumFailures = seleniumResult.getFailures();
        StringBuffer failedTestNames = new StringBuffer();
        for (Failure failure : seleniumFailures) {
            failedTestNames.append(failure.getTestHeader());
            failedTestNames.append("\n");
        }
        File htmlSeleniumReport = htmlReportExporter.generateHtmlReport(seleniumResult);        
        FileResource reportResource=new FileResource(htmlSeleniumReport).copy();
        ResourceAndContext reportContext=new ResourceAndContext();
        reportContext.setResource(reportResource);
        //we add the ".html" extension so that eclipse will recognise it as a Junit file
        reportContext.setMetadata(new ExecutionReportResourceMetadata(SeleniumIsSuccess.class,new Properties(),FileResource.class,"seleniumReport.html"));
        context.add(reportContext);
        if(seleniumResult.getAdditionalReport()!=null){
            ResourceAndContext additionalReportContext=new ResourceAndContext();
            additionalReportContext.setResource(new FileResource(seleniumResult.getAdditionalReport()).copy());
            additionalReportContext.setMetadata(new ExecutionReportResourceMetadata(SeleniumIsSuccess.class, new Properties(), FileResource.class, "detailedSeleniumReport"));
            context.add(additionalReportContext);
        }
        addAttachementReportContext(context);
        return failedTestNames;
    }

    private void addAttachementReportContext(ArrayList<ResourceAndContext> context) {
        //if any additional report, publish it too (eg: selenese HTML execution report)
        if(!seleniumResult.getAttachments().isEmpty()){
            for(File attachment:seleniumResult.getAttachments()){
                try {
                    AttachmentMangementHelper.injectAttachedFile(attachment, context);
                } catch (IOException ex) {
                    LOGGER.warn("Lost attachment {} due to I/O problem.",attachment.getAbsolutePath(),ex);
                }
            }
        }
    }
}

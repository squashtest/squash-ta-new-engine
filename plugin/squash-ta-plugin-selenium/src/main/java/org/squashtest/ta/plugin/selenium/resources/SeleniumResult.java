/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;


@TAResource("result.selenium")
public class SeleniumResult implements Resource<SeleniumResult>{
    
        private static final Logger LOGGER = LoggerFactory.getLogger(SeleniumResult.class);    
	
	private Result result;
        /** Addtional report (eg: Selenese report) */
        private File additionalReport;
	/** used to hold attachments */
	private Set<File> attachments=new HashSet<>();
	
	public SeleniumResult(){}
	
        /**
         * Use this if there is no report attachement.
         * @param result the execution result object.
         */
	public SeleniumResult(Result result){
		this.result=result;
	}

        /**
         * Use this if there is a single report attachment.
         * @param result the main report.
         * @param additionalReport the report attachment.
         */
	public SeleniumResult(Result result, File additionalReport){
		this.result=result;
		this.additionalReport=additionalReport;
	}
	
        public SeleniumResult(Result result, Set<File> attachments){
            try {
                this.result=result;
                File attachmentDir=FileTree.FILE_TREE.createTempDirectory("selenium", "attached");
                for(File attachement:attachments){
                    //la technique de copie ci-dessous vise à préserver les noms de fichiers pour utilisation dans l'assertion
                    File newPiece=new File(attachmentDir,attachement.getName());
                    
                    copyAttachmentKeepingName(attachement, newPiece);
                    this.attachments.add(newPiece);
                }
            } catch (IOException ex) {
                LOGGER.warn("Lost all attachements on error.", ex);
            }
        }

    private void copyAttachmentKeepingName(File attachement, File newPiece) {
        if(attachement.isFile()){
            try(
                    InputStream in=new FileInputStream(attachement);
                    OutputStream os=new FileOutputStream(newPiece);
                    )
            {
                IOUtils.copy(in, os);
            } catch (IOException ex) {
                LOGGER.warn("Lost attachment "+attachement.getAbsolutePath(),ex);
            }
        }else{
            if(newPiece.mkdir()){
                LOGGER.debug("Successfully created destination dir {}",newPiece.getAbsolutePath());
                for(File child:attachement.listFiles()){
                    File newChild=new File(newPiece,child.getName());
                    copyAttachmentKeepingName(child, newChild);
                }
            }else{
                LOGGER.warn("Failed to create attachment destination directory {}");
            }
        }
    }

        
	public void setResult(Result result){
		this.result=result;
	}
	
	public Result getResult(){
		return result;
	}

	@Override
	public SeleniumResult copy() {
		return new SeleniumResult(result,attachments);
	}

	@Override
	public void cleanUp() {
		if(additionalReport!=null){
                    deleteWithLogs(additionalReport, "Additional report");
                }
                for(File attachment:attachments){
                    deleteWithLogs(attachment, "Attachment");
                }
	}

        private void deleteWithLogs(File target, String fileType) {
            boolean deleted=target.delete();
            if(deleted){
                LOGGER.debug("{} file {} was cleaned up.", fileType, target.getAbsolutePath());
            }else{
                LOGGER.warn("Failed to delete {} file {}",fileType.toLowerCase(), target.getAbsolutePath());
            }
        }

	public boolean isSuccess(){
            return (result.getFailureCount() == 0);
	}
	
	public int getFailureCount(){
		return result.getFailureCount();
	}
	
	public List<Failure> getFailures(){
		return result.getFailures();
	}

	public String getMessage(int failureIndex){
		return result.getFailures().get(failureIndex).getMessage();
	}
	
	public List<String> getMessages(){
		List<String> messages = new LinkedList<String>();
	
		for (Failure fail : result.getFailures()){
			messages.add(fail.getMessage());
		}
		
		return messages;
	}
	
	public Throwable getException(int failureIndex) {
		return result.getFailures().get(0).getException();
	}
	
	public List<Throwable> getExceptions(){
		List<Throwable> exceptions = new LinkedList<Throwable>();
		
		for (Failure fail : result.getFailures()){
			exceptions.add(fail.getException());
		}
		
		return exceptions;
	}

	public File getAdditionalReport() {
            return additionalReport;
	}
        
        public Set<File> getAttachments(){
            return Collections.unmodifiableSet(attachments);
        }
}

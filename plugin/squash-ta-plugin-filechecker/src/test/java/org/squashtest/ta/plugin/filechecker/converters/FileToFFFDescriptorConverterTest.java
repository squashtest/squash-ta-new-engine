/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.converters;

import java.io.File;
import java.net.URL;

import org.junit.Test;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

public class FileToFFFDescriptorConverterTest {

	@Test
	public void testConvert() {
		// Successful test
		URL resourceUrl = Thread.currentThread().getContextClassLoader().getResource("filechecker/descriptors/FFF_txt_descriptor.xml");
		FileResource resource = new FileResource(new File(resourceUrl.getPath()));
		FileToFFFDescriptorConverter converters = new FileToFFFDescriptorConverter();
		converters.convert(resource);
	}
	
	@Test(expected = BadDataException.class)
	public void testConvert2() {
		// Test with a descriptor with missing schema
		URL resourceUrl = Thread.currentThread().getContextClassLoader().getResource("filechecker/descriptors/MissingSchema.xml");
		FileResource resource = new FileResource(new File(resourceUrl.getPath()));
		FileToFFFDescriptorConverter converters = new FileToFFFDescriptorConverter();
		converters.convert(resource);
	}
	
	@Test(expected = InstructionRuntimeException.class)
	public void testConvert3() {
		// Test with a not existing file descriptor 
		FileResource resource = new FileResource(new File("filechecker/descriptors/notExistFile.xml"));
		FileToFFFDescriptorConverter converters = new FileToFFFDescriptorConverter();
		converters.convert(resource);
	}
	
	@Test(expected = BadDataException.class)
	public void testConvert4() {
		// Test with a malformed descriptor file
		URL resourceUrl = Thread.currentThread().getContextClassLoader().getResource("filechecker/descriptors/Bad_FFF_txt_descriptor.xml");
		FileResource resource = new FileResource(new File(resourceUrl.getPath()));
		FileToFFFDescriptorConverter converters = new FileToFFFDescriptorConverter();
		converters.convert(resource);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.converters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Iterator;

import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.UnknownRecordException;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsBuilder;
import org.squashtest.ta.filechecker.library.bo.iface.Records;
import org.squashtest.ta.filechecker.library.dao.DatasourceFactory;
import org.squashtest.ta.filechecker.library.dao.IDatasource;
import org.squashtest.ta.filechecker.library.facade.RecordsBuilderFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.filechecker.resources.FFFDescriptorResource;
import org.squashtest.ta.plugin.filechecker.resources.FFFResource;

@TAResourceConverter("structured")
public class FileToFFFConverters implements
		ResourceConverter<FileResource, FFFResource> {

	private FFFrecordsTemplate template;

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		Iterator<Resource<?>> it = configuration.iterator();
		while (it.hasNext()) {
			Resource<?> resource = it.next();
			if( resource instanceof FFFDescriptorResource){
				if(template==null){
					FFFDescriptorResource descriptor = (FFFDescriptorResource)resource;
					template = descriptor.getTemplate();
				}else{
					throw new IllegalConfigurationException("A FFF descriptor resource has been provided more than once");
				}
			}
		}
		if(template==null)
		{
			throw new IllegalConfigurationException("A FFF descriptor resource should be provided in the USING close");
		}
	}

	@Override
	public FFFResource convert(FileResource resource) {
		if (template!=null){
			URI uri = resource.getFile().toURI();
                        StringBuffer buffer = template.getEncoding();
                        boolean isBinary = template.isBinary();
                        int bytesPerRecord = template.getBytesPerRecord();
			IDatasource datasource = DatasourceFactory.createDatasource(uri, isBinary, 
					bytesPerRecord, buffer);
                       
			try {
				AbstractRecordsBuilder builder = RecordsBuilderFactory.createBuilder(datasource, template);
				Records records = builder.buildRecords();
                                return new FFFResource( records );
			} catch (FileNotFoundException e) {
				throw new BadDataException("The file to convert is not readable (FileNotFound Exception)",e);
			} catch (UnknownRecordException e) {
				throw new BadDataException(e.getMessage(), e);
			} catch (IOException e) {
				throw new InstructionRuntimeException(e);
			}
		}
		else{
			throw new IllegalConfigurationException("No FFF descriptor resource has been provided");
		}
		
	}

	@Override
	public void cleanUp() {
		// No op
	}

}

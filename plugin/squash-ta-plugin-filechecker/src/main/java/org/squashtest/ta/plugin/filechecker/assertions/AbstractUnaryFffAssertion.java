/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.tools.ComponentRepresentation;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.commons.library.reporting.IdentityTranslation;
import org.squashtest.ta.plugin.commons.library.reporting.Translator;
import org.squashtest.ta.plugin.commons.resources.TranslationResource;

/**
 *
 * @author edegenetais
 */
public abstract class AbstractUnaryFffAssertion {
    private static final Logger LOGGER=LoggerFactory.getLogger(AbstractUnaryFffAssertion.class);
    
    private Translator translator;

    protected List<ResourceAndContext> createFailureContext(String report, String resourceName) {
        List<ResourceAndContext> context = new ArrayList<>(1);
        try {
            final File reportFile = File.createTempFile(resourceName, ".txt", TempDir.getExecutionTempDir());
            reportFile.deleteOnExit();
            new BinaryData(report.getBytes("utf-8")).write(reportFile);
            ResourceAndContext reportRAC = new ResourceAndContext(new FileResource(reportFile), new ExecutionReportResourceMetadata(FileNotContains.class, new Properties(), FileResource.class, "detectedForbiddenContext.txt"));
            context.add(reportRAC);
        } catch (IOException e) {
            LOGGER.warn("Failed to write report to disk, assertion failure will be reported without context.",e);
        }
        return context;
    }

    protected void pickTranslationResource(Resource<?> res) {
        if(translator==null){
            translator=((TranslationResource)res).getTranslator();
            LOGGER.debug("Picked up translation configuration {}({}) in {}",res,new ComponentRepresentation(res),new ComponentRepresentation((UnaryAssertion<?>)this));
        }else{
            LOGGER.warn("Ignoring conflicting translation configuration {}({}) as {} is already registered.",res,new ComponentRepresentation(res), getTranslator());
        }
    }
    
    /**
     * @return the translator
     */
    protected synchronized Translator getTranslator() {
        if(translator==null){
            translator=new IdentityTranslation();
        }
        return translator;
    }

    protected String translate(final String message1) {
        return getTranslator().translate(message1);
    }

    public void addConfiguration(Collection<Resource<?>> configuration) {
        for (Resource<?> res : configuration) {
            if (res instanceof TranslationResource) {
                pickTranslationResource(res);
            } else {
                LOGGER.warn("assertion {} ignored an unexpected configuration resources of type {}", new ComponentRepresentation((UnaryAssertion<?>)this), res.getClass());
            }
        }
    }
    
}

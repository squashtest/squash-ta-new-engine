/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.assertions;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 * This assertion searches a pattern inside a FileResource.
 * If the pattern is found, the assertion fails.
 * 
 * <div>
 * <strong>DSL example :</strong>
 * <pre>ASSERT $(hello world) DOES not.contain THE $(ping)</pre>
 * </div>
 * 
 * @author cruhlmann
 */
@TABinaryAssertion("not.contain")
public class FileNotContains extends AbstractFileSearcherAssertion implements BinaryAssertion<FileResource, FileResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileNotContains.class); 
        
	@Override
	public void test() throws BinaryAssertionFailedException {
		try {
			if (!actualContainsExpected()){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Not found '"+getRegex()+"' in the file.");
				}
			}else{
				String message = translate("The pattern '"+getRegex()+"' was found in the file.");
                                LOGGER.warn(message);
                                List<ResourceAndContext> context=createFailureContext(message, "fileNotContains");
                                
				throw new BinaryAssertionFailedException(message, expectedPattern, actual, context);
			}
		} catch (IOException e) {
                    String message =  "I/O Problem while checking if '"+getRegex()+"' was absent from file.";
                    LOGGER.warn(message,e);
                    throw new InstructionRuntimeException(message,e);
		}	
	}


}

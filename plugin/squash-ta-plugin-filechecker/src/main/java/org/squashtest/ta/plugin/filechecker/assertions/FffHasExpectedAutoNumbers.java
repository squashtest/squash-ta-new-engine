/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.assertions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.Records;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.plugin.filechecker.resources.FFFResource;

@TAUnaryAssertion("expected.autonumbers")
public class FffHasExpectedAutoNumbers extends AbstractUnaryFffAssertion implements UnaryAssertion<FFFResource> {

        private static final Logger LOGGER = LoggerFactory.getLogger(FffHasExpectedAutoNumbers.class);
        
        private FFFResource records;
	
	@Override
	public void setActualResult(FFFResource actual) {
		records = actual;
	}

    @Override
    public void test() throws AssertionFailedException {

        try {
            Records recordClone = (Records) records.getRecords().clone();
            recordClone.resetSequences();
            recordClone.validateSequences();
        } catch (InvalidSyntaxException e) {
            final String message = translate(e.getMessage());            
            LOGGER.warn(message,e);
            throw new AssertionFailedException(message, records, createFailureContext(message, "missingAutonumbers"));
        }

    }

}

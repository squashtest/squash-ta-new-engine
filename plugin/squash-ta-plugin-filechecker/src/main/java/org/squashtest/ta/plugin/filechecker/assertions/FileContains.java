/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.assertions;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * This assertion searches a pattern inside a FileResource.
 * If the pattern is not found, the assertion fails.
 * 
 * <div>
 * <strong>DSL example :</strong>
 * <pre>ASSERT $(hello world) DOES contain THE $(hello)</pre>
 * </div>
 * 
 * @author cruhlmann
 */
@TABinaryAssertion("contain")
public class FileContains extends AbstractFileSearcherAssertion implements BinaryAssertion<FileResource, FileResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileContains.class);

	
	@Override
	public void test() throws BinaryAssertionFailedException {
			try {
				if (actualContainsExpected()){
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Found '"+getRegex()+"' in the file.");
					}
				}else{
					String message = translate("The pattern '"+getRegex()+"' was not found in the file.");
                                        LOGGER.warn(message);
					throw new BinaryAssertionFailedException(message, expectedPattern, actual, createFailureContext(message, "fileNotContains"));
				}
			} catch (IOException e) {
                            String message =  "I/O Problem while checking if file contained '"+getRegex()+"';";
                            LOGGER.warn(message,e);
                            throw new InstructionRuntimeException(message,e);
			}
		}
}

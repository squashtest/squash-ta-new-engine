/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.converters;

import java.io.IOException;
import java.util.Collection;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsTemplate;
import org.squashtest.ta.filechecker.library.facade.FileType;
import org.squashtest.ta.filechecker.library.facade.TemplateFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.filechecker.resources.FFFDescriptorResource;
import org.xml.sax.SAXException;

@TAResourceConverter("descriptor")
public class FileToFFFDescriptorConverter implements
		ResourceConverter<FileResource, FFFDescriptorResource> {
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
            new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public FFFDescriptorResource convert(FileResource resource) {
		try {
			AbstractRecordsTemplate template = TemplateFactory.createTemplate(resource.getFile().toURI());
                        if(template.getType().equals(FileType.FixedFieldFile)){
				return new FFFDescriptorResource((FFFrecordsTemplate)template);
			}else{
				throw new BadDataException("The descriptor file is not the descriptor of a Fixed Field File");
			}
		} catch (IOException e1) {
			throw new InstructionRuntimeException(e1);
		} catch (SAXException e1) {
			throw new BadDataException("Malformed descriptor file: "+e1.getMessage(), e1);
		} catch (InvalidDescriptorException e1) {
			throw new BadDataException("Malformed descriptor file: "+e1.getMessage(), e1);
		}

	}

	@Override
	public void cleanUp() {
		// No op
	}

}

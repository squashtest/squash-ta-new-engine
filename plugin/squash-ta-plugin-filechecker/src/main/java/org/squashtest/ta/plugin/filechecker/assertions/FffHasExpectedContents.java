/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.filechecker.assertions;

import java.util.Collection;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.utils.xpath.InvalidContentException;
import org.squashtest.ta.filechecker.library.utils.xpath.XpathAssertions;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.tools.ComponentRepresentation;
import org.squashtest.ta.plugin.commons.resources.TranslationResource;
import org.squashtest.ta.plugin.filechecker.resources.FFFQueriesResource;
import org.squashtest.ta.plugin.filechecker.resources.FFFResource;

@TAUnaryAssertion("expected.content")
public class FffHasExpectedContents extends AbstractUnaryFffAssertion implements UnaryAssertion<FFFResource> {
        
        private static final Logger LOGGER = LoggerFactory.getLogger(FffHasExpectedContents.class);
    
	private FFFResource records;
	
	private XpathAssertions assertions;
	
	@Override
	public void setActualResult(FFFResource actual) {
		records = actual;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		Iterator<Resource<?>> it = configuration.iterator();
		while (it.hasNext()) {
			Resource<?> resource = it.next();
			if( resource instanceof FFFQueriesResource){
				if(assertions==null){
					FFFQueriesResource queries = (FFFQueriesResource)resource;
					assertions = queries.getAssertions();
				}else{
					throw new IllegalConfigurationException("A FFF queries resource has been provided more than once");
				}
			}else if(resource instanceof TranslationResource){
                            pickTranslationResource(resource);
                        }else{
                            LoggerFactory.getLogger(FffHasExpectedContents.class).warn("Ignoring unexpected configuration resource {}",new ComponentRepresentation(resource));
                        }
		}
		if(assertions == null)
		{
			throw new IllegalConfigurationException("A FFF queries resource should be provided in the USING close");
		}
	}
	

	@Override
	public void test() throws AssertionFailedException {
		try {
			records.getRecords().validateSemantics(assertions.iterator());
		} catch (InvalidContentException e) {
                        final String message = translate(e.getMessage());
                        LOGGER.warn(message,e);
			throw new AssertionFailedException(message, records, createFailureContext(message, "missingContentReport"));
		}    

	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.tools.ComponentRepresentation;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

/**
 * This is the legacy version of the SFTP delete command, which uses an ignored,
 * bogus {@link FileResource} INPUT resource, as the deleted filename is passed
 * through configuration.
 * 
 * @author edegenetais.
 * @deprecated Since Squash-TA-Framework 1.11 : Use instead  {@link SFTPGetFileCommand} using built-in {@link org.squashtest.ta.framework.components.VoidResource} as input parameter. 
 */
@Deprecated
@TACommand("get")
public class LegacySFTPGetFileCommand extends AbstractSFTPGetFileCommand implements
		Command<FileResource, SSHTarget> {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(LegacySFTPGetFileCommand.class);
    
	//to assure retrocompatibility
    public void setResource(FileResource resource) {
        LOGGER.warn("DEPRECTATED : Using legacy '{}' with ignored FileResource as input (WITH clause). Please consider using the version using the built-in '{void}' VoidResource as input. ", new ComponentRepresentation(this).toString());
    }

    //this may seem useless, but is required for correct command OUTPUT type detection
	@Override
	public FileResource apply() {
		return super.apply();
	}
}
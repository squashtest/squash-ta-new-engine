/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh;

import java.io.File;
import java.io.IOException;
import java.util.List;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.FileMode;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.xfer.FileSystemFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.ssh.exceptions.SFTPFileDeleteException;
import org.squashtest.ta.plugin.ssh.exceptions.SFTPFileDownloadException;
import org.squashtest.ta.plugin.ssh.exceptions.SFTPFileUploadException;

// FIXME : this design has problems, as shown by the contorted code parts. This reeks of missing classes who should
// each take some part of the responsibilities assumed by this utility class.
public final class SFTPUtils {

        private static final Logger LOGGER = LoggerFactory.getLogger(SFTPUtils.class);
	private static final String SFTP_CLOSURE_ERROR = "problem with sftp closure!!!";
	private static final String SSH_CLOSURE_ERROR = "problem with ssh client closure!!!";

	
	private SFTPUtils() {}
	
    protected static synchronized File synchroGetFile(SquashSSHClient squashSSHClient, String remotePath) throws IOException {
            File result;
	    if (!squashSSHClient.sshClient.isConnected()){
	    	squashSSHClient.connect();
	    }
	    SFTPClient sftp = null;
	    try {
	    	squashSSHClient.authenticate();
	      File local = File.createTempFile("sftp.client", "temp", TempDir.getExecutionTempDir());
	      sftp = squashSSHClient.sshClient.newSFTPClient();
	      String remote=normalizeRemotePath(remotePath);
                final FileMode.Type remoteType = sftp.type(remote);
              switch(remoteType){
                  case REGULAR:
                      result = getRegularFile(remote, sftp, local);                              
                      break;
                  case DIRECTORY:
                      result = getDirectory(remote, local, sftp, remotePath);
                      break;
                  default:
                      throw new SFTPFileDownloadException("Unsupported remote file type "+remoteType+" cannot be downloaded.");
              }
	    }finally {
	    	  closeConnections(sftp, squashSSHClient.sshClient);
	    }
	    return result;
    }

    private static File getDirectory(String remote, File local, SFTPClient sftp, String remotePath) throws IOException, SFTPFileDownloadException {
        LOGGER.debug("{} is a remote directory. Replacing temp storage by a directory and proceeding.",remote);
        if(local.delete()){
            sftp.get(remote, new FileSystemFile(local));
            return new File(local,new File(remotePath).getName());
        }else{
            throw new SFTPFileDownloadException("Failed to replace the "+local.getAbsolutePath()+" local temp file by a directory.");
        }
    }

    private static File getRegularFile(String remote, SFTPClient sftp, File local) throws IOException {
        File result;
        LOGGER.debug("{} is a remote regular file. Proceeding.",remote);
        sftp.get(remote, new FileSystemFile(local));
        result=local;
        return result;
    }

    protected static synchronized void synchroPutFile(SquashSSHClient squashSSHClient, String remotePath, File file){
		SFTPClient sftp = null;
	    try {	
			if (!squashSSHClient.sshClient.isConnected()){
				squashSSHClient.connect();
			}

			squashSSHClient.authenticate();

			sftp = squashSSHClient.sshClient.newSFTPClient();
			  
			String remote = normalizeRemotePath(remotePath);
			
                        //we first check if the path given does not leads to an existing resource
                        //                                       sic ---^
                        if (sftp.statExistence(remote) != null){
                            throw new SFTPFileUploadException("As the file "+remotePath+" already exists, it could not be properly uploaded");
                        }
                        
			//if some of the directories in the given path doesn't exists, we created them
			String directory = createDirectories(remote, sftp);

                        /*
                        * We will first upload into a temporary location, then rename to the target remote name,
                        * so that the file will seem to appear atomically, 
                        * ensuring the remote side that files appear at once complete and ready for use.
                        */
                        doUploadInTemporaryLocation(sftp, file, directory, remotePath);
                        renameToTargetRemoteName(sftp, directory, file, remote, remotePath);
                        
	    } catch (IOException e) {
		     throw new SFTPFileUploadException("problem put "+remotePath, e);
	    } finally {
	    	  closeConnections(sftp, squashSSHClient.sshClient);
	    }
    }

    private static void renameToTargetRemoteName(SFTPClient sftp, String directory, File file, String remote, String remotePath) throws SFTPFileUploadException {
        //... then we modify it for the wanted name
        try{
            sftp.rename(directory+"/"+file.getName(), remote);
        }catch(IOException e){
            throw new SFTPFileUploadException("The uploaded temporary file "+file.getName()+" could not be properly renamed in "+remotePath+".", e);
        }
    }

    private static void doUploadInTemporaryLocation(SFTPClient sftp, File file, String directory, String remotePath) throws SFTPFileUploadException {
        //we first put the file with its original temporary name...
        try{
            sftp.put(file.getAbsolutePath(), directory);
        }catch(IOException e){
            throw new SFTPFileUploadException("A problem occurred during the uploading of "+remotePath+".", e);
        }
    }

    protected static synchronized void synchroDeleteFile(SquashSSHClient squashSSHClient, String remotePath, boolean directory,
			boolean failIfNotExist) {
	      SFTPClient sftp = null;
	    try {
	    	if (!squashSSHClient.sshClient.isConnected()){
	    		squashSSHClient.connect();
	    	}
	    	squashSSHClient.authenticate();
		     sftp = squashSSHClient.sshClient.newSFTPClient();
                     String remote=normalizeRemotePath(remotePath);
		     //we first check if the path given leads to an existing resource
		     if (sftp.statExistence(remote) != null){
		    	 if (directory){
		    		 removeDirectory(sftp, remote);
		    	 }else{
		    		 sftp.rm(remote);
		    	 }
		     }else if (failIfNotExist){
			     throw new SFTPFileDeleteException("The file "+remotePath+" could not be deleted as it doesn't exists.");
		     }
    	} catch (IOException e) {
		     throw new SFTPFileDeleteException("The file "+remotePath+" could not be deleted.", e);
    	} finally {
	    	  closeConnections(sftp, squashSSHClient.sshClient);
    	}
    }
    
    private static String createDirectories(String remote, SFTPClient sftp) {
		StringBuilder directory;
                if(remote.startsWith("/")){
                    directory = new StringBuilder();
                }else{
                    directory = new StringBuilder("./");
                }
		try{
			int lastSlashIndex = remote.lastIndexOf("/");
			if (lastSlashIndex > 0){
				directory.append(remote.substring(0, lastSlashIndex));
			}
			sftp.mkdirs(directory.toString());
		}catch(IOException e){
		     throw new SFTPFileUploadException("A problem happened when creating the directory(ies) "+directory, e);
		}
		return directory.toString();
    }

    //As sshj SFTPClient.rmdir() method is not recursive, here is a manual recursive method
    private static synchronized void removeDirectory(SFTPClient sftp, String remotePath) throws IOException {
		List<RemoteResourceInfo> resources = sftp.ls(remotePath);
		//if the directory is not empty, we delete each of its children
		for (RemoteResourceInfo resource : resources){
			String path = resource.getPath();
                        if (resource.isDirectory()){
				removeDirectory(sftp, path);
			}else {
				//we remove the file
				sftp.rm(path);
			}
		}
		sftp.rmdir(remotePath);
    }
	
    protected static String normalizeRemotePath(String remotePath) throws IOException, SFTPFileUploadException {
        // This replacement allow the use of '\' and '/' in the path given in the property file
        String remote = remotePath.replace("\\", "/");
        if(remote.startsWith("/")){
            LOGGER.debug("{} is an absolute path, rewritten as {}",remotePath,remote);
        }else{
            remote="./"+remote;
            LOGGER.debug("{} is a relative path, rewritten as {} to be relative to remote home.",remotePath,remote);
        }
        return remote;
    }
    
    private static void closeConnections(SFTPClient sftp, SSHClient sshClient) {
        SFTPFileUploadException toplevelException=null;
        try {
                if (sftp != null){
                        sftp.close();
                }
        } catch (IOException e) {
            toplevelException = new SFTPFileUploadException(SFTP_CLOSURE_ERROR, e);
        }
        
        try{
	    if (sshClient != null){
                sshClient.disconnect();
            }
	}catch(IOException e){
            SFTPFileUploadException clientCloseException=new SFTPFileUploadException(SSH_CLOSURE_ERROR, e);
            if(toplevelException==null){
                toplevelException=clientCloseException;
            }else{
                toplevelException.addSuppressed(clientCloseException);
            }
        }
        if(toplevelException!=null){
            throw toplevelException;
        }
    }
}

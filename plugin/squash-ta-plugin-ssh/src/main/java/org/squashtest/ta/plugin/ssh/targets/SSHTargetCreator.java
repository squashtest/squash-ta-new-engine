/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.targets;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.TATargetCreator;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.plugin.commons.library.ShebangCheck;

/**
 * Target creator for ssh targets.
 * 
 * @author edegenetais
 * @author k.drifi
 * 
 */
@TATargetCreator("target.creator.ssh")
public class SSHTargetCreator implements TargetCreator<SSHTarget> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SSHTargetCreator.class);
	private static final ShebangCheck SHEBANG_CHECK = new ShebangCheck("ssh");

	/**
	 * This object allows overriding the target properties with system
	 * properties (@see {@link System#getProperties()})
	 */
	private PropertiesBasedCreatorHelper propertiesHelper = new PropertiesBasedCreatorHelper();

	public SSHTargetCreator() {
		propertiesHelper.setKeysRegExp(SSHTarget.NAMESPACE.replaceAll("\\.", "\\.") + "\\..*");
	}

	@Override
	public boolean canInstantiate(URL propertiesFile) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Testing eligibility of " + propertiesFile + " as SSH configuration URL.");
		}
		boolean isSshConfiguration = false;
		try {
			if (isSSHConfigurationType(propertiesFile) && isCorrect(propertiesFile)) {
				isSshConfiguration = true;
			}
		} catch (IOException e) {
			if (LOGGER.isWarnEnabled()) {
				LOGGER.warn("I/O error while evaluating eligibility of " + propertiesFile.toExternalForm()
						+ " as ssh configuration.", e);
			}
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(propertiesFile + " is " + (isSshConfiguration ? " " : "not ") + "eligible.");
		}
		return isSshConfiguration;
	}

	private boolean isSSHConfigurationType(URL propertiesFile) throws IOException {
		return "file".equals(propertiesFile.getProtocol()) && SHEBANG_CHECK.hasShebang(propertiesFile);
	}

	private boolean isCorrect(URL propertiesFile) {
		boolean correct;
		try {
			Properties configuration = load(propertiesFile);
			List<String> missingMandatoryKeys = searchMandatoryKey(configuration);
			if (!missingMandatoryKeys.isEmpty()) {
				correct = false;
				StringBuilder builder = new StringBuilder("Detected missing mandatory key(s): ");
				Iterator<String> it = missingMandatoryKeys.iterator();
				while (it.hasNext()) {
					String string = (String) it.next();
					builder.append(string);
					if (it.hasNext()) {
						builder.append(", ");
					} else {
						builder.append(". ");
					}
				}
				builder.append("(");
				builder.append(propertiesFile);
				builder.append(")");
				LOGGER.warn(builder.toString());
			} else {
				correct = "true".equalsIgnoreCase(configuration.getProperty(SSHTarget.PUBLICAUTH_KEY))
						? isCorrectKeyTarget(configuration) : isCorrectPasswordTarget(configuration);
			}
		} catch (URISyntaxException e) {
			correct = false;
			LOGGER.warn("Detected malformed URL while evaluating correctness of " + propertiesFile, e);
		} catch (IOException e) {
			correct = false;
			LOGGER.warn("Could not read target configuration file", e);
		}
		return correct;
	}

	private boolean isCorrectPasswordTarget(Properties configuration) {
		LOGGER.info("SSHTarget is interpreted as a password authenticated one, " + SSHTarget.PUBLICAUTH_KEY
				+ " is set to false or not available in the target property file.");
		if (!configuration.containsKey(SSHTarget.PASSWORD_KEY)) {
			LOGGER.warn("Detected missing mandatory key: " + SSHTarget.PASSWORD_KEY);
			return false;
		} else
			return true;
	}

	private boolean isCorrectKeyTarget(Properties configuration) {
		LOGGER.info("SSHTarget is interpreted as a key authenticated one, " + SSHTarget.PUBLICAUTH_KEY
				+ " is set to true.");
		if (!configuration.containsKey(SSHTarget.PASSPHRASE_KEY)) {
		 LOGGER.warn("Detected missing mandatory key: " + SSHTarget.PASSPHRASE_KEY);
		 return false;
		 } else if (!configuration.containsKey(SSHTarget.KEYFILEPATH_KEY)){
			 LOGGER.warn(SSHTarget.PUBLICAUTH_KEY + " is set to true and " +
			 SSHTarget.KEYFILEPATH_KEY
			 + " is missing, key file must be provided as a ressource when executing a SSH or SFTP command.");
			 return true;
		 } else
			 return true;
	}

	/**
	 * 
	 * @param configuration
	 *            The target configuration file
	 * @return The list of mandatory key not found in the configuration file
	 */
	private List<String> searchMandatoryKey(Properties configuration) {
		List<String> notFoundKeys = new ArrayList<String>();
		for (String searchedKey : SSHTarget.MANDATORY_KEYS) {
			if (!configuration.containsKey(searchedKey)) {
				notFoundKeys.add(searchedKey);
			}
		}
		return notFoundKeys;
	}

	@Override
	public SSHTarget createTarget(URL propertiesFile) {
		LOGGER.debug("trying to instanciate SSH target from " + propertiesFile);
		try {
			if (isCorrect(propertiesFile)) {
				Properties configuration = load(propertiesFile);
				return new SSHTarget(configuration);
			} else {
				throw new BrokenTestException(propertiesFile + " is not a valid SSH target configuration file");
			}
		} catch (URISyntaxException e) {
			throw new BrokenTestException(
					"SSHTarget configuration loading failed for " + propertiesFile.toExternalForm(), e);
		} catch (IOException e) {
			throw new BrokenTestException(
					"SSHTarget configuration loading failed for " + propertiesFile.toExternalForm(), e);
		}
	}

	private Properties load(URL propertiesFile) throws URISyntaxException, IOException {
		File file = new File(propertiesFile.toURI());
		return propertiesHelper.getEffectiveProperties(file);
	}

}
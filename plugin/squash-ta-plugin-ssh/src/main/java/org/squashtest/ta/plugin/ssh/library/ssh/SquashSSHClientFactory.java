/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh;

public class SquashSSHClientFactory {
	public static final String PASSWORD_AUTH_CLIENT = "Password Authenticated Client";
	public static final String KEY_AUTH_CLIENT = "Key Authenticated Client";

	public SquashSSHClient getSquashSSHClient(String clientType, String hostname, Integer port, String username,
			String password, String keyfilepath) {
            
		SquashSSHClient client = null;
                
                if (PASSWORD_AUTH_CLIENT.equals(clientType)) {
                    
                    client = new PasswordSquashSSHClient(hostname, port, username, password, keyfilepath);
                    
                } else if (KEY_AUTH_CLIENT.equals(clientType)) {
                    
                    client = new KeySquashSSHClient(hostname, port, username, password, keyfilepath);
                    
                }

		return client;
	}

}

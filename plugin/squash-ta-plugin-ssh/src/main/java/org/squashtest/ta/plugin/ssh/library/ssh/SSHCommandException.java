/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh;

import org.slf4j.helpers.MessageFormatter;

/**
 * Exception thrown when an error occurs during an SSH command execution.
 * Ported from CATS.
 * 
 * @author amd
 * @author edegenetais
 */
@SuppressWarnings("serial")
public class SSHCommandException extends RuntimeException {

    /**
     * Creates a new instance of {@link SSHCommandException}
     */
    public SSHCommandException() {
        super();
    }

    /**
     * Creates a new instance of {@link SSHCommandException}
     * 
     * @param pCause Parent exception of this exception.
     */
    public SSHCommandException(final Throwable pCause) {
        super(pCause);
    }

    /**
     * Creates a new instance of {@link SSHCommandException}
     * 
     * @param pMsg Error message ({} will be replaced by params).
     * @param pParams Parameters of the error message
     */
    public SSHCommandException(final String pMsg, final Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage());
    }

    /**
     * Creates a new instance of {@link SSHCommandException}
     * 
     * @param pCause Parent exception of this exception.
     * @param pMsg Error message ({} will be replaced by params).
     * @param pParams Parameters of the error message
     */
    public SSHCommandException(final Throwable pCause, final String pMsg, final Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage(), pCause);
    }

}

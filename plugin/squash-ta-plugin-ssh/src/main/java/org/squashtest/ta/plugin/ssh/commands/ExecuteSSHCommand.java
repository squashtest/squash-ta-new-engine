/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import java.io.IOException;
import java.util.Collection;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.local.process.library.shell.ShellOptionReader;
import org.squashtest.ta.plugin.local.process.resources.CommandLineResource;
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource;
import org.squashtest.ta.plugin.ssh.library.ssh.SquashSSHClient;
import org.squashtest.ta.plugin.ssh.resources.PrivateKeyResource;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

/**
 * Command to execute a command line over SSH.
 * @author edegenetais
 *
 */
@TACommand("execute")
public class ExecuteSSHCommand implements
		Command<CommandLineResource, SSHTarget> {
	
	private SSHTarget target;
	private CommandLineResource commandLine;
	private ShellOptionReader optionReader=new ShellOptionReader("SSHexecuteCommand");
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Resource<?> resource : configuration){
			if (PrivateKeyResource.class.isAssignableFrom(resource.getClass())) {
				PrivateKeyResource privateKey = ((PrivateKeyResource) resource);
				this.target.setKeyFilePath(privateKey.getPath());	
			}
		}
		optionReader.addConfiguration(configuration);
	}

	@Override
	public void setTarget(SSHTarget target) {
		this.target=target;
	}

	@Override
	public void setResource(CommandLineResource resource) {
		this.commandLine=resource;
	}

	@Override
	public ShellResultResource apply() {
		ShellResultResource result;
		SquashSSHClient client=target.getClient();
               
		String command=commandLine.getPosixCommand();
		Integer timeout=optionReader.getTimeout();
		if(timeout==null){
			timeout=commandLine.getTimeout();
		}
		ShellResult output;
		try {
			output = client.runSSHCommand(command,timeout);
		} catch (IOException e) {
			throw new InstructionRuntimeException("The execution of the following command with SSH failed: '"+command+"'.", e);
		}
		result=new ShellResultResource(output);
		return result;
	}

	@Override
	public void cleanUp() {
		//noop: no resource held
	}

}

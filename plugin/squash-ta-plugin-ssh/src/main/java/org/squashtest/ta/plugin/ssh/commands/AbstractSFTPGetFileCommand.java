/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.ssh.exceptions.SFTPFileDownloadException;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

/**
 * Main logic of the SFTP "get" command, which exists in two flavours, new ({@link SFTPGetFileCommand}) and legacy (
 * {@link LegacySFTPGetFileCommand}).
 * 
 * @author edegenetais
 * 
 */
@TACommand("get")
public abstract class AbstractSFTPGetFileCommand extends AbstractSFTPCommand {

	public AbstractSFTPGetFileCommand() {
		this.configuration = new LinkedList<Resource<?>>();
	}
	 
    /**
     * Implementation for the {@link org.squashtest.ta.framework.components.Command#setTarget(org.squashtest.ta.framework.components.Target)} method
     * (only subclasses effectively implement the {@link org.squashtest.ta.framework.components.Command} interface.
     */
	public void setTarget(SSHTarget target) {
		this.target = target;
	}

    /**
     * Implementation for the {@link org.squashtest.ta.framework.components.Command#apply()} method
     * (only subclasses effectively implement the {@link org.squashtest.ta.framework.components.Command}
	 * interface).
     */
	public FileResource apply() {
		Map<String, String> options = getOptions();

		String remotePath = options.get(remoteNameOption);
		try {
			File downloaded = target.getClient().getFile(remotePath);
			return new FileResource(downloaded);
		} catch (IOException e) {
			throw new SFTPFileDownloadException("The file " + remotePath + " was not found", e);
		}
	}
}
/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh;

import net.schmizz.sshj.transport.TransportException;
import net.schmizz.sshj.userauth.UserAuthException;

public class PasswordSquashSSHClient extends SquashSSHClient {

	public PasswordSquashSSHClient(String hostname, Integer port, String username, String password, String keyfilepath) {
		super(hostname, port, username, password, keyfilepath);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void authenticate() throws UserAuthException, TransportException {
		// if already authenticated, sshj throws a useless SSHExecption with no clue.
		if (!sshClient.isAuthenticated()) {
			sshClient.authPassword(username, password);
		}
	}

	@Override
	protected void init() {
		// Maybe someday this will cause a bug.
	}

}

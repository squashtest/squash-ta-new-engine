/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import java.util.Map;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

@TACommand("put")
public class SFTPPutFileCommand extends AbstractSFTPCommand implements
		Command<FileResource, SSHTarget> {

	private FileResource resource;

	@Override
	public void setTarget(SSHTarget target) {
		this.target = target;
	}

	@Override
	public void setResource(FileResource resource) {
		this.resource = resource;
	}

	@Override
	public VoidResource apply() {

		Map<String, String> options = getOptions();

		String remotePath = options.get(remoteNameOption);

		target.getClient().putFile(remotePath, resource.getFile());

		return new VoidResource();
	}
}

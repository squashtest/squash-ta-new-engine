/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

/**
 * <p>
 * <strong>Description : </strong>That command will fetch a resource from a
 * given sftp. It doesn't need any Resource as input so the FileResource type
 * argument exists because it has to. The result of the file transfer will be
 * returned as a {@link FileResource}, once {@link #apply()} has terminated
 * successfully.
 * </p>
 * 
 * <p>
 * NOTE : since the input resource is irrelevant, you may pass any resource, the
 * command won't care.
 * </p>
 * 
 * 
 * <p>
 * <strong>Configuration</strong> : a FileResource which entries are comma
 * separated pairs of {@literal <key:value>} (note that column ':' is the separator)
 * </p>
 * <div>
 * Available options :
 * <ul>
 * <li>remotepath : file path name, relative to the user home directory.
 * Mandatory.</li>
 * </ul>
 * </div>
 * 
 * <div>
 * <strong>DSL example :</strong>
 * <pre>
 * EXECUTE get WITH $() ON mysftp USING $(remotepath : existingfolder/newfile.txt) AS my.downloaded.resource
 * </pre>
 * </div>
 * 
 * 
 * 
 * @author cruhlmann
 * 
 */

@TACommand("get")
public class SFTPGetFileCommand extends AbstractSFTPGetFileCommand implements
		Command<VoidResource, SSHTarget> {

	@Override
	public void setResource(VoidResource resource) {
            //noop
	}
	
	//this may seem useless, but is required for correct command OUTPUT type detection
	@Override
	public FileResource apply() {
		return super.apply();
	}
}
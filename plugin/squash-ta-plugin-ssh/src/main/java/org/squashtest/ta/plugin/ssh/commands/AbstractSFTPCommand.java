/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.ssh.exceptions.SFTPFileDownloadException;
import org.squashtest.ta.plugin.ssh.resources.PrivateKeyResource;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

public abstract class AbstractSFTPCommand {

	protected static String remoteNameOption = "remotepath";
	protected Collection<Resource<?>> configuration = new LinkedList<Resource<?>>();
	protected SSHTarget target;

	/************
	 * Override methods from Command, same for all the FTP classes
	 ************/

	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Resource<?> resource : configuration) {
			if (PrivateKeyResource.class.isAssignableFrom(resource.getClass())) {
				PrivateKeyResource privateKey = ((PrivateKeyResource) resource);
				this.target.setKeyFilePath(privateKey.getPath());
			}
		}
		this.configuration.addAll(configuration);
	}

	public void cleanUp() {
		// Nothing special
	}

	/************ Other shared methods ************/

	protected Map<String, String> getOptions() {
		Map<String, String> options = null;

		for (Resource<?> resource : configuration) {
			if (FileResource.class.isAssignableFrom(resource.getClass())) {
				File fileR = ((FileResource) resource).getFile();
				options = readConf(fileR);
			}
		}
		if ((options == null) || (!(options.containsKey(remoteNameOption)))) {
			throw new SFTPFileDownloadException(
					"SFTP file download: missing configuration. You must specify at least what is the remote file name (option '"
							+ remoteNameOption + "')");
		}
		return options;
	}

	/**
	 * @param file
	 *            : the configuration file to read
	 * @return a Map of the file type and path
	 */
	protected Map<String, String> readConf(File file) {
		try {
			return OptionsReader.BASIC_READER.getOptions(file);
		} catch (IOException ex) {
			throw new SFTPFileDownloadException(
					"SFTP file download : an error occurred while reading the configuration : ", ex);
		} catch (IllegalArgumentException ex) {
			throw new SFTPFileDownloadException(
					"SFTP configuration error: an error occurred while reading the configuration, " + ex.getMessage()
							+ ". Please check the file " + file.getName() + " or your USING clause.",
					ex);
		}
	}

	/**
	 * Tests if a path is a directory path or an actual file path
	 * 
	 * @param fileName
	 *            : the path to test
	 * @return true if the path given is one of a directory, wrong if it is one
	 *         of a file
	 */
	protected boolean isDirectory(String fileName) {
		return fileName.endsWith("/");
	}
}

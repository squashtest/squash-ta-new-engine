/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.targets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.PropertiesUtils;
import org.squashtest.ta.framework.annotations.TATarget;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.plugin.ssh.library.ssh.SSHConfigurationException;
import org.squashtest.ta.plugin.ssh.library.ssh.SquashSSHClient;
import org.squashtest.ta.plugin.ssh.library.ssh.SquashSSHClientFactory;

@TATarget("ssh.target")
public class SSHTarget implements Target {

	public static final String NAMESPACE = "squashtest.ta.ssh";

	public static final String PASSWORD_KEY = NAMESPACE + ".password";

	public static final String USERNAME_KEY = NAMESPACE + ".username";

	public static final String PORT_KEY = NAMESPACE + ".port";

	public static final String HOSTNAME_KEY = NAMESPACE + ".hostname";

	public static final String KEYFILEPATH_KEY = NAMESPACE + ".keyfilepath";

	public static final String PASSPHRASE_KEY = NAMESPACE + ".passphrase";
	
	public static final String PUBLICAUTH_KEY = NAMESPACE + ".public.auth";

	private static final Logger LOGGER = LoggerFactory.getLogger(SSHTarget.class);

	private List<SquashSSHClient> clients = new LinkedList<SquashSSHClient>();

	private String hostname;
	private Integer port;
	private String username;
	private String password;
	private String passphrase;
	private String keyfilepath;
	private Boolean publicAuth;

	private SquashSSHClientFactory clientFactory = new SquashSSHClientFactory();

	public static final List<String> MANDATORY_KEYS;

	static {
		List<String> mandaroryKey = new ArrayList<String>(3);
		mandaroryKey.add(HOSTNAME_KEY);
		mandaroryKey.add(USERNAME_KEY);
		MANDATORY_KEYS = mandaroryKey;
	}

	/** noarg constructor for Spring */
	public SSHTarget() {
	}

	/**
         * This constructor allows full initialization from a configuration file.
	 * @param configuration
	 */
	public SSHTarget(Properties configuration) {
		this.hostname = configuration.getProperty(HOSTNAME_KEY);
		this.port = new PropertiesUtils().getIntegerValue(configuration, PORT_KEY);
		this.username = configuration.getProperty(USERNAME_KEY);
		this.password = configuration.getProperty(PASSWORD_KEY);
		this.keyfilepath = configuration.getProperty(KEYFILEPATH_KEY);
		this.passphrase = configuration.getProperty(PASSPHRASE_KEY);
		this.publicAuth = "true".equalsIgnoreCase(configuration.getProperty(PUBLICAUTH_KEY));
	}

	@Override
	public boolean init() {
		SquashSSHClient client = null;
		if (publicAuth) {
			client = clientFactory.getSquashSSHClient(SquashSSHClientFactory.KEY_AUTH_CLIENT, hostname, port, username,
					passphrase, keyfilepath);
		} else {
			client = clientFactory.getSquashSSHClient(SquashSSHClientFactory.PASSWORD_AUTH_CLIENT, hostname, port,
					username, password, keyfilepath);
		}

		try {
			client.connect();
			return true;
		} catch (IOException e) {
			throw new SSHConfigurationException(e, "Client connection failed.", new Object[] {});
		}
	}

	public boolean initAndAuthenticate() {
		SquashSSHClient client = null;
		if (publicAuth) {
			client = clientFactory.getSquashSSHClient(SquashSSHClientFactory.KEY_AUTH_CLIENT, hostname, port, username,
					passphrase, keyfilepath);
		} else {
			client = clientFactory.getSquashSSHClient(SquashSSHClientFactory.PASSWORD_AUTH_CLIENT, hostname, port,
					username, password, keyfilepath);
		}

		try {
			client.connect();
			client.authenticate();
			synchronized (clients) {
				clients.add(client);
			}
			return true;
		} catch (IOException e) {
			throw new SSHConfigurationException(e, "Client connection failed.", new Object[] {});
		}
	}

	@Override
	public void reset() {
		synchronized (clients) {
			cleanup();
			initAndAuthenticate();
		}
	}

	@Override
	public void cleanup() {
		synchronized (clients) {
			for (SquashSSHClient client : clients) {
				try {
					client.dispose();
				} catch (IOException e) {
                                        String message="Failed to dispose an SSH connection to host " + hostname
							+ (port == null ? "" : ":" + port.toString());
					LOGGER.warn(message,e);
				}
			}
			clients.clear();
		}
	}

	public SquashSSHClient getClient() {
		synchronized (clients) {
			if (clients.isEmpty()) {
				initAndAuthenticate();
			}
			return clients.remove(0);
		}
	}

	@Override
	public Properties getConfiguration() {
		Properties pro = new Properties();
		pro.setProperty(HOSTNAME_KEY, hostname);
		pro.setProperty(USERNAME_KEY, username);
		if (port != null)
			pro.setProperty(PORT_KEY, port.toString());
		if (password != null)
			pro.setProperty(PASSWORD_KEY, password);
		if (passphrase != null && publicAuth)
			pro.setProperty(PASSPHRASE_KEY, passphrase);
		if (publicAuth.equals(true))
			pro.setProperty(PUBLICAUTH_KEY, "true");
		else
			pro.setProperty(PUBLICAUTH_KEY, "false");
		if (keyfilepath != null && publicAuth)
			pro.setProperty(KEYFILEPATH_KEY, keyfilepath);
				
		return pro;
	}

	public void setKeyFilePath(String keyfilepath) {
		this.keyfilepath = keyfilepath;
	}
}

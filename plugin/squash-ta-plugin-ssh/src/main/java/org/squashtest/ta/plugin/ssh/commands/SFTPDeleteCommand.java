/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.plugin.ssh.targets.SSHTarget;

/**
 * This command deletes a file through SFTP with the new configuration based on a {@link VoidResource} INPUT, as the
 * filename is passed through configuration. For the implementation of the old way with an ignored, bogus
 * {@link org.squashtest.ta.framework.components.FileResource}, please see {@link LegacySFTPDeleteCommand}.
 * 
 */
@TACommand("delete")
public class SFTPDeleteCommand extends AbstractSFTPDeleteCommand implements Command<VoidResource, SSHTarget> {

	@Override
	public void setResource(VoidResource resource) {
            //noop
	}

	//this may seem useless, but is required for correct command OUTPUT type detection
	@Override
	public VoidResource apply() {
		return super.apply();
	}
}

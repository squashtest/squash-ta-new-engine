/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.local.process.library.shell.ShellResult;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Command;
import net.schmizz.sshj.transport.TransportException;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;
import net.schmizz.sshj.userauth.UserAuthException;

/**
 * SSH Client code ported from CATS.
 * This class will become the SSH client, and only the SSH client.
 * Its other responsibilities will be splitted to other classes.
 * @author edegenetais
 * @author Agnes
 * @author kdrifi
 */
public abstract class SquashSSHClient {

	protected static final Logger LOGGER = LoggerFactory.getLogger(SquashSSHClient.class);
    
    protected HostKeyVerifier hostKeyVerifier= new DefaultHostKeyVerifier();
    
    protected String hostname;
    
    protected Integer port;
    
	protected String username;
    
    protected String password;
    
    protected String keyfilepath;
    
    protected SSHClient sshClient=new SSHClient();
    
    /**
     * Constructor with complete initialization.
	 * @param hostname
	 * @param port
	 * @param username
	 * @param password
	 * @param keyfilepath
	 */
	public SquashSSHClient(String hostname, Integer port, String username,
			String password, String keyfilepath) {
		this.hostname = hostname;
		this.port = port;
		this.username = username;
		this.password = password;
		this.keyfilepath = keyfilepath;
		sshClient.addHostKeyVerifier(hostKeyVerifier);
		try {
			init();
		} catch (IOException e) {
			LOGGER.warn("Target configuration is invalid : unless you use the infamous (and deprecated) in-test key injection feature, tests will probably fail!",e);
		}
	}
    
	protected abstract void init() throws IOException;		

	/**
	 * Reset host key verifier.
	 * @param hostKeyVerifier the new host key verifier to set.
	 * @throws IOException iction of the previous client, if it existed and was connected.
	 */
	public void setHostKeyVerifier(HostKeyVerifier hostKeyVerifier) throws IOException {
		this.hostKeyVerifier = hostKeyVerifier;
                
		if(sshClient!=null && sshClient.isConnected()){
		
			sshClient.disconnect();
		}
	}
	
    /**
     * Runs a SSH command, reading the connection parameters from the specified
     * configuration file.
     * 
     * @param command Command to execute
     * @param timeOut process killing timeout (ms).
     * @throws IOException
     */
    public ShellResult runSSHCommand(String command,Integer timeOut)
                    throws IOException {
    	ShellResult executionResult;
        if(!sshClient.isConnected()){
        	connect();
        }
		try {
			authenticate();
		    final Session session = sshClient.startSession();
		    try {
		        final Command cmd = session.exec(command);
		        if(timeOut==null){
		        	LOGGER.debug("Applying default timeout.");
		        	cmd.join(5, TimeUnit.SECONDS);
		        }else{
		        	LOGGER.debug("Applying specified timeout: {} ms.",timeOut);
		        	cmd.join(timeOut, TimeUnit.MILLISECONDS);
		        }
		        Integer exitStatus = cmd.getExitStatus();
		        if (exitStatus == null) {
		            throw new SSHCommandException("Could not retrieve exit status for command {}", command);
		        }
		        String outputStream = IOUtils.readFully(cmd.getInputStream()).toString();
		        String errorStream = IOUtils.readFully(cmd.getErrorStream()).toString();
		        executionResult=new ShellResult(exitStatus, outputStream, errorStream, command);
		        LOGGER.debug("Exit code={}",exitStatus);
		        LOGGER.debug("Output stream = \n{}",outputStream);
		        LOGGER.debug("Error stream = \n{}",errorStream);
		    } finally {
		        session.close();
		    }
		} finally {
		    sshClient.disconnect();
		}
		return executionResult;
    }

    /**
     * Connect the ssh client to host.
     * @throws IOException thrown when SSH i/o fails.
     */
	public void connect() throws IOException {
        if(port==null){
            sshClient.connect(hostname);
        } else {
            sshClient.connect(hostname, port);
        }
    }

	 /**
     * Disconnect the ssh client.
     * @throws IOException thrown when SSH i/o fails.
     */
	public void dispose() throws IOException{
		if(sshClient.isConnected()){
			sshClient.disconnect();
		}
	}

	public void setKeyFilePath(String keyFilePath){
		this.keyfilepath = keyFilePath;
	}
	
    public abstract void authenticate() throws UserAuthException,
			TransportException, IOException;
    
    /************** SFTP methods **************/
    
	 /**
     * SFTP Method.
     * Get a file on SFTP, via SSH protocol.
     * @param remotePath : path of the file to get on the SSH server
     * @return the local file, copy in a temp directory
     * @throws IOException : thrown when SSH i/o fails.
     */
    public File getFile(String remotePath) throws IOException {
	    return SFTPUtils.synchroGetFile(this, remotePath);
	  }

	/**
     * SFTP Method.
     * Put a file on SFTP, via SSH protocol.
     * @param remotePath : the path where the file will be put (directory and file name)
     * @param file : the local file to put
	 */
	public void putFile(String remotePath, File file) {
		SFTPUtils.synchroPutFile(this, remotePath, file);
	}

	/**
     * SFTP Method.
     * Delete a file or a directory on SFTP, via SSH protocol.
     * @param remotePath : the resource to delete
     * @param directory : true if directory, false if file
     * @param failIfDoesNotExist : param given in using clause
     */
	public void deleteFile(String remotePath, boolean directory,
			boolean failIfDoesNotExist) {
		SFTPUtils.synchroDeleteFile(this, remotePath, directory, failIfDoesNotExist);
	}
}

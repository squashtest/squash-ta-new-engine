/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.commands

import java.util.Collection;

import org.squashtest.ta.core.tools.io.SimpleLinesData
import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.ssh.exceptions.SFTPFileDownloadException
import org.squashtest.ta.plugin.ssh.library.ssh.SquashSSHClient
import org.squashtest.ta.plugin.ssh.targets.SSHTarget

import spock.lang.Specification

class AbstractSFTPCommandTest extends Specification {
	
	//as we cannot instantiate an abstract class, we will use one of its children
	SFTPDeleteCommand testee
	
	def setup(){
		testee=new SFTPDeleteCommand()
	}
		
	def "fail to recuperation configurate if no configuration"(){
		when:
			testee.getOptions()
		then:
			thrown(SFTPFileDownloadException)
	}
	
	def "succeed to build configuration"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/sftp/sftpConf")
			File file = new File(url.toURI())
		when:
			def res = testee.readConf(file)
		then:
			res instanceof Map<String, String>
			res.get("remotepath") == "phantom.txt"
			res.get("failIfDoesNotExist") == "false"
	}
	
	def "succeed to recuperate configuration"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/sftp/sftpConf")		
			FileResource fileResource = new FileResource(new File(url.toURI()))
			Collection<Resource<?>> configuration = new ArrayList<Resource<?>>()
			configuration.add(fileResource)
			testee.addConfiguration(configuration)
		when:
			def res = testee.getOptions()
		then:
			res instanceof Map<String, String>
			res.get("remotepath") == "phantom.txt"
			res.get("failIfDoesNotExist") == "false"			
	}
}

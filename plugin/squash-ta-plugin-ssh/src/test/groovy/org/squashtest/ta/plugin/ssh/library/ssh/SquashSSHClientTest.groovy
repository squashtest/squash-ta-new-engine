/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh

import java.util.concurrent.TimeUnit

import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.connection.channel.direct.Session
import net.schmizz.sshj.connection.channel.direct.Session.Command

import org.squashtest.ta.local.process.library.shell.ShellResult;

import spock.lang.Specification

class SquashSSHClientTest extends Specification{
	
	SquashSSHClient testee
	SSHClient mockSSHclient
	SquashSSHClientFactory clientFactory = new SquashSSHClientFactory()
	
	def setup(){
		testee=clientFactory.getSquashSSHClient(SquashSSHClientFactory.PASSWORD_AUTH_CLIENT,"",0,"","", "")
		mockSSHclient=Mock(SSHClient)
		testee.sshClient=mockSSHclient
	}
	
	def "transmit 0 exit value"(){
		given:
			Command cmd=Mock()
			cmd.getExitStatus()>>0
			cmd.getInputStream()>>new ByteArrayInputStream("".getBytes())
			cmd.getErrorStream()>>new ByteArrayInputStream("".getBytes())
		and:
			Session session=Mock()
			session.exec(_)>>cmd
		and:
			mockSSHclient.isConnected()>>true
			mockSSHclient.isAuthenticated()>>true
			mockSSHclient.startSession()>>session
		when:
			ShellResult result=testee.runSSHCommand("",null)
		then:
			result.getExitValue()==0
	}
	
	def "transmit non-zero exit value"(){
		given:
			Command cmd=Mock()
			cmd.getExitStatus()>>-235
			cmd.getInputStream()>>new ByteArrayInputStream("".getBytes())
			cmd.getErrorStream()>>new ByteArrayInputStream("".getBytes())
		and:
			Session session=Mock()
			session.exec(_)>>cmd
		and:
			mockSSHclient.isConnected()>>true
			mockSSHclient.isAuthenticated()>>true
			mockSSHclient.startSession()>>session
		when:
			ShellResult result=testee.runSSHCommand("",null)
		then:
			result.getExitValue()==-235
	}
	
	def "transmit stdout"(){
		given:
			Command cmd=Mock()
			cmd.getExitStatus()>>0
			cmd.getInputStream()>>new ByteArrayInputStream("Helloworld!".getBytes())
			cmd.getErrorStream()>>new ByteArrayInputStream("".getBytes())
		and:
			Session session=Mock()
			session.exec(_)>>cmd
		and:
			mockSSHclient.isConnected()>>true
			mockSSHclient.isAuthenticated()>>true
			mockSSHclient.startSession()>>session
		when:
			ShellResult result=testee.runSSHCommand("",null)
		then:
			"Helloworld!".equals(result.getStdout())
	}
	
	def "transmit stderr"(){
		given:
			Command cmd=Mock()
			cmd.getExitStatus()>>0
			cmd.getInputStream()>>new ByteArrayInputStream("".getBytes())
			cmd.getErrorStream()>>new ByteArrayInputStream("Goodbye, and thanks for all the fish!".getBytes())
		and:
			Session session=Mock()
			session.exec(_)>>cmd
		and:
			mockSSHclient.isConnected()>>true
			mockSSHclient.isAuthenticated()>>true
			mockSSHclient.startSession()>>session
		when:
			ShellResult result=testee.runSSHCommand("",null)
		then:
			"Goodbye, and thanks for all the fish!".equals(result.getStderr())
	}
	
	def "throw if no exit status"(){
	given:
		Command cmd=Mock()
		cmd.getExitStatus()>>null
		cmd.getInputStream()>>new ByteArrayInputStream("".getBytes())
		cmd.getErrorStream()>>new ByteArrayInputStream("Goodbye, and thanks for all the fish!".getBytes())
	and:
		Session session=Mock()
		session.exec(_)>>cmd
	and:
		mockSSHclient.isConnected()>>true
		mockSSHclient.isAuthenticated()>>true
		mockSSHclient.startSession()>>session
	when:
		ShellResult result=testee.runSSHCommand("",null)
	then:
		thrown(SSHCommandException)
	}
	
	def "should transmit required timeout, if any"(){
	given:
		Command cmd=Mock()
		cmd.getExitStatus()>>0
		cmd.getInputStream()>>new ByteArrayInputStream("".getBytes())
		cmd.getErrorStream()>>new ByteArrayInputStream("".getBytes())
	and:
		Session session=Mock()
		session.exec(_)>>cmd
	and:
		mockSSHclient.isConnected()>>true
		mockSSHclient.isAuthenticated()>>true
		mockSSHclient.startSession()>>session
	when:
		ShellResult result=testee.runSSHCommand("",35000)
	then:
		1 * cmd.join(35000, TimeUnit.MILLISECONDS)
	}
}

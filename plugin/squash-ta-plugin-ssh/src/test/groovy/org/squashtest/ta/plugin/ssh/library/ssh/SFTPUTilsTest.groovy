/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.library.ssh

import java.util.concurrent.TimeUnit

import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.connection.channel.direct.Session
import net.schmizz.sshj.connection.channel.direct.Session.Command
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.sftp.FileMode;
import spock.lang.Specification

class SFTPUTilsTest extends Specification{
	
	SquashSSHClient testee
	SSHClient mockSSHclient
	SquashSSHClientFactory clientFactory = new SquashSSHClientFactory()
	
	def setup(){
		testee=clientFactory.getSquashSSHClient(SquashSSHClientFactory.PASSWORD_AUTH_CLIENT,"",0,"","", "")
		mockSSHclient=Mock()
		testee.sshClient=mockSSHclient
	}
	
	def "should get file"(){
		given:
			Session session=Mock()
			SFTPClient sftpClient=Mock()
		and:
			mockSSHclient.isConnected() >> true
			mockSSHclient.newSFTPClient() >> sftpClient
                        sftpClient.type(_) >> FileMode.Type.REGULAR
		when:
			def result=SFTPUtils.synchroGetFile(testee,"remotepath")
		then:
			result.getName().contains("sftp.client")
			notThrown(Exception)
			
	}
	
	def "should put file"(){
		given:
			Session session=Mock()
			SFTPClient sftpClient=Mock()
			File file=Mock()
		and:
			mockSSHclient.isConnected() >> true
			mockSSHclient.newSFTPClient() >> sftpClient
		when:
			def res="ko"
			SFTPUtils.synchroPutFile(testee,"remotepath", file)
			res="ok"
		then:
			res == "ok"
			notThrown(Exception)
	}
	
	def "should delete file"(){
		given:
			Session session=Mock()
			SFTPClient sftpClient=Mock()
			File file=Mock()
		and:
			mockSSHclient.isConnected() >> true
			mockSSHclient.newSFTPClient() >> sftpClient
		when:
			def res="ko"
			SFTPUtils.synchroDeleteFile(testee,"remotepath", false, false)
			res="ok"
		then:
			res == "ok"
			notThrown(Exception)
	}
}

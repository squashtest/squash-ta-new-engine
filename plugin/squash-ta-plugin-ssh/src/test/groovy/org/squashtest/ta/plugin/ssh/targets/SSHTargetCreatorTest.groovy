/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.ssh.targets

import net.schmizz.sshj.SSHClient;

import org.squashtest.ta.plugin.ssh.library.ssh.SquashSSHClient;

import spock.lang.Specification;

class SSHTargetCreatorTest extends Specification {
	
	SSHTargetCreator testee
	
	def setup(){
		testee=new SSHTargetCreator()
	}
	
	def "can instantiate correct properties file"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp")
		when:
			def res = testee.canInstantiate(url)
			def resTarget = testee.createTarget(url)
		then:
			res == true
			resTarget.getConfiguration().get("squashtest.ta.ssh.hostname") == "squash-int"
			resTarget.getConfiguration().get("squashtest.ta.ssh.username") == "tester"
			resTarget.getConfiguration().get("squashtest.ta.ssh.password") == "tester"
	}
	
	def "can create target"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_wrong")
		when:
			def res = testee.createTarget(url)
		then:
			res instanceof SSHTarget
			res.getConfiguration().get("squashtest.ta.ssh.hostname") == "squash-int"
			res.getConfiguration().get("squashtest.ta.ssh.username") == "tester"
			res.getConfiguration().get("squashtest.ta.ssh.password") == "tester"
	}

	def "can create key target"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_key")
		when:
			def res = testee.createTarget(url)
		then:
			res instanceof SSHTarget
			res.getConfiguration().get("squashtest.ta.ssh.hostname") == "squash-int"
			res.getConfiguration().get("squashtest.ta.ssh.username") == "tester"
			res.getConfiguration().get("squashtest.ta.ssh.passphrase") == "tester"
			res.getConfiguration().get("squashtest.ta.ssh.public.auth") == "true"
			res.getConfiguration().get("squashtest.ta.ssh.keyfilepath") == "my/Path/To/File"
	}

	def "cannot instantiate correct properties file with no shebang"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_wrong")
		when:
			def res = testee.canInstantiate(url)
		then:
			res == false
	}
	
	def "cannot instantiate correct properties file with missing hostname"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_missing_hostname")
		when:
			def res = testee.canInstantiate(url)
		then:
			res == false
	}
	
	def "cannot instantiate correct properties file with missing username"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_missing_username")
		when:
			def res = testee.canInstantiate(url)
		then:
			res == false
	}
	
	def "cannot instantiate correct password properties file with missing password"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_missing_password")
		when:
			def res = testee.canInstantiate(url)
		then:
			res == false
	}
	
	def "cannot instantiate correct key properties file with missing passphrase"(){
		given:
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/ssh/resources/conf/sshProp_missing_passphrase")
		when:
			def res = testee.canInstantiate(url)
		then:
			res == false
	}
	
	// TODO Create tests ti check warn/info logs in each cases
}

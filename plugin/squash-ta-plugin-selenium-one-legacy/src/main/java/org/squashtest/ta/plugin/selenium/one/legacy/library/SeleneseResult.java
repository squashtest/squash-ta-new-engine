/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.library;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.internal.AssumptionViolatedException;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.selenium.one.legacy.resources.SeleniumHtmlTestSuite;
import org.squashtest.ta.plugin.selenium.resources.SeleniumResult;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Holder for the direct result of the selenese execution.
 * This is <strong>not</strong> a {@link org.squashtest.ta.framework.components.Resource} 
 * implementation.
 * @author edegenetais
 *
 */
public class SeleneseResult extends ShellResult {
	
	private static final String NBSP_REPLACEMENT = " ";

	private static final String NBSP_ENTITY = "&nbsp;";

	private static final Logger LOGGER = LoggerFactory.getLogger(SeleneseResult.class);

	@SuppressWarnings("serial")
	private static final class SeleneseTestFailed extends AssumptionViolatedException {
		public SeleneseTestFailed() {
			super("Selenese test execution failed. See attached report");
		}
	}

	private static final String CLASS_VALUE_TEST_FAILED = "status_failed";

	private static final String CLASS_VALUE_TEST_SUCCEEDED = "status_passed";

	private static final String SUITE_TABLE_PARSING_QUERY = "//table[@id='suiteTable']/tbody/tr";

	private static final String CLASS_ATTRIBUTE_NAME = "class";
	
	private static final String PARSING_FAIL = "Failed to parse HTML selenium execution report";

	private File reportLocation;
	
	private File suiteLocation;
	
	public SeleneseResult(int exitValue, String stdout, String stderr,
			String command, File reportLocation, File suiteLocation) {
		super(exitValue, stdout, stderr, command);
		this.reportLocation=reportLocation;
		this.suiteLocation=suiteLocation;
	}
	
	/**
	 * @return location of the selenium HTML report file.
	 */
	public File getReportLocation() {
		return reportLocation;
	}

	/**
	 * @return location of the selenium HTML test suite file.
	 */
	public File getSuiteLocation() {
		return suiteLocation;
	}
	
	public SeleniumResult toSeleniumResult(){
		try {
			//initializing junit result
			Result junitResult=new Result();
			RunListener rl=junitResult.createListener();
			rl.testRunStarted(Description.createSuiteDescription(suiteLocation.getName(), new Annotation[]{}));
			
			//strip file from some unrecognized entities
			File strippedFile = stripNonXmlEntities();
			
			//extract test list with names and statuses
			XPath xpathEngine=XPathFactory.newInstance().newXPath();
			NodeList nodeList=(NodeList)xpathEngine.evaluate(SUITE_TABLE_PARSING_QUERY, new InputSource(strippedFile.toURI().toString()), XPathConstants.NODESET);
			int index=0;
			Node tr=nodeList.item(index);
			while(tr!=null){
				extractTestResultFromTableLine(rl,
						xpathEngine, tr);
				tr=nodeList.item(++index);
			}
			
			//close result building process
			rl.testRunFinished(junitResult);
			return new SeleniumResult(junitResult,reportLocation);
		} catch (XPathExpressionException e) {
			throw new InstructionRuntimeException(PARSING_FAIL, e);
		} catch (Exception e) {
			throw new InstructionRuntimeException(PARSING_FAIL, e);
		}
	}

	private void extractTestResultFromTableLine(RunListener rl,
			XPath xpathEngine, Node tr) throws XPathExpressionException,
			Exception {
		NamedNodeMap attributes=tr.getAttributes();
		Node classNode=attributes.getNamedItem(CLASS_ATTRIBUTE_NAME);
		String classValue=classNode.getTextContent().trim();
		String testName = xpathEngine.evaluate("./td/a", tr);
		if(CLASS_VALUE_TEST_FAILED.equals(classValue)){
			Description testDescription = Description.createTestDescription(SeleniumHtmlTestSuite.class, testName);
			rl.testStarted(testDescription);
			Failure failure = new Failure(testDescription, new SeleneseTestFailed());
			rl.testAssumptionFailure(failure);
			rl.testFailure(failure);
			rl.testFinished(testDescription);
		}else if(CLASS_VALUE_TEST_SUCCEEDED.equals(classValue)){
			Description testDescription = Description.createTestDescription(SeleniumHtmlTestSuite.class, testName);
			rl.testStarted(testDescription);
			rl.testFinished(testDescription);
		}else{
			LOGGER.debug("skipped suite table line with class "+classValue);
		}
	}

	private File stripNonXmlEntities() throws IOException {
		File strippedFile;
		SimpleLinesData reportData=new SimpleLinesData(reportLocation.getAbsolutePath());
		strippedFile=File.createTempFile("seleneseReport", ".trimmed", TempDir.getExecutionTempDir());
		List<String> strippedXhtml=new ArrayList<String>(reportData.getLines().size());
		for(String line:reportData.getLines()){
			line=line.replace(NBSP_ENTITY, NBSP_REPLACEMENT);
			strippedXhtml.add(line);
		}
		new SimpleLinesData(strippedXhtml).write(strippedFile);
		return strippedFile;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle;
import org.squashtest.ta.plugin.selenium.converters.AbstractJavaBundleToJavaSeleniumConverter;
import org.squashtest.ta.plugin.selenium.one.legacy.resources.SeleniumOneJavaTest;

@TAResourceConverter("script")
public class JavaBundleToJavaSelenium1Converter extends AbstractJavaBundleToJavaSeleniumConverter implements ResourceConverter<JavaCodeBundle, SeleniumOneJavaTest> {
	
	/**
	 * empty constructor for Spring instanciation purposes
	 */
	public JavaBundleToJavaSelenium1Converter(){
            //default constructor
        }

	@Override
	public SeleniumOneJavaTest convert(JavaCodeBundle resource) {
		//We test if the code Bundle (compiled classes) have at least one class of Junit type
		boolean jUnit = containsJUnitClasses(resource);
		if (jUnit){
			ClassLoader bundleClassLoader = resource.getDedicatedClassloader();
			if (mainSeleniumClass != null){
				return new SeleniumOneJavaTest(bundleClassLoader, mainSeleniumClass);
			} else {
				return new SeleniumOneJavaTest(bundleClassLoader);
			}
		} else {
			logError(ERROR_MESSAGE);
			throw new BadDataException(ERROR_MESSAGE);
		}
	}

	@Override
	protected Logger getLogger() {
		return LoggerFactory.getLogger(JavaBundleToJavaSelenium1Converter.class);
	}

}


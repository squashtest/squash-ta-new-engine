/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.selenium.library.JavaExecutableUnit;
import org.squashtest.ta.plugin.selenium.resources.AbstractSeleniumJavaTest;


@TAResource("script.java.selenium1")
public class SeleniumOneJavaTest extends AbstractSeleniumJavaTest implements Resource<SeleniumOneJavaTest>, JavaExecutableUnit {
	
	/** Noarg constructor for Spring enumeration. */
	public SeleniumOneJavaTest() {
		super();
	}
	
	public SeleniumOneJavaTest(ClassLoader loader){
		super(loader);
	}
	
	public SeleniumOneJavaTest(ClassLoader loader, String mainClassName){
		super(loader,mainClassName);
	}
	
	@Override
	public SeleniumOneJavaTest copy() {
		if (mainClass != null){
			return new SeleniumOneJavaTest(dedicatedClassLoader, mainClass.getName()); 
		}
		else{
			return new SeleniumOneJavaTest(dedicatedClassLoader);
		}
	}

	@Override
	protected Logger getLogger() {
		return LoggerFactory.getLogger(SeleniumOneJavaTest.class);
	}
}

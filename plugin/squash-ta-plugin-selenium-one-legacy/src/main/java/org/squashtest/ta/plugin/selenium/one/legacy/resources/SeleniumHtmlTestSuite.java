/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.resources;

import java.io.File;
import java.io.IOException;

import org.slf4j.LoggerFactory;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.commons.resources.AbstractBundle;

@TAResource("script.html.selenium")
public class SeleniumHtmlTestSuite extends AbstractBundle implements Resource<SeleniumHtmlTestSuite>{

	private boolean ownsBundleTree;
	private String browserString;
	
	/** Noarg constructor for Spring */
	public SeleniumHtmlTestSuite() {}
	
	private SeleniumHtmlTestSuite(SeleniumHtmlTestSuite orig){
		super(orig.getBase(),orig.getMain());
		ownsBundleTree=orig.ownsBundleTree;
	}
	
	public SeleniumHtmlTestSuite(File base, File main,boolean ownsBase, String browserString){
		super(base,main);
		ownsBundleTree=ownsBase;
		this.browserString=browserString;
	}
	
	@Override
	public SeleniumHtmlTestSuite copy() {
		return new SeleniumHtmlTestSuite(this);
	}

	@Override
	public void cleanUp() {
		if(ownsBundleTree){
			try {
				FILE_TREE.clean(getBase());
			} catch (IOException e) {
				LoggerFactory.getLogger(SeleniumHtmlTestSuite.class).warn("Resource cleanup failed for resource "+getBase().getAbsolutePath(), e);
			}
		}
	}

	public String getBrowserString() {
		return browserString;
	}

}

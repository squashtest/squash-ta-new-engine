/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.converters;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import org.squashtest.ta.core.library.properties.PropertiesKeySet;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.selenium.one.legacy.library.SeleniumServerManager;
import org.squashtest.ta.plugin.selenium.one.legacy.resources.SeleniumConfiguration;

/**
 * Converter to create a Selenium Server configuration from file.
 * @author edegenetais
 *
 */

@TAResourceConverter("structured")
public class FileToSeleniumConfiguration implements
		ResourceConverter<FileResource, SeleniumConfiguration> {

	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public SeleniumConfiguration convert(FileResource resource) {
		try {
			Properties prop=new Properties();
			prop.load(resource.openStream());
			PropertiesKeySet keySet=new PropertiesKeySet(prop);
			if(keySet.contains(SeleniumServerManager.SELENIUM_MARKER_KEY)){
				return new SeleniumConfiguration(prop);
			}else{
				throw new BadDataException("Resource "+resource.getFile()+" is not a Squash TA Selenium Server configuration resource.");
			}
		} catch (IOException e) {
			throw new BadDataException("Cannot load selenium configuration data from resource.", e);
		}
	}

	@Override
	public void cleanUp() {
		//noop: only memory held
	}

}

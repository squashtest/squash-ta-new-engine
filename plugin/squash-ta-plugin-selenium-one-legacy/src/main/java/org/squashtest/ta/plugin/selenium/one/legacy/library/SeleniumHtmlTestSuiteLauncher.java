/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.library;

import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.process.ProcessHandle;

/**
 * Launcher for Selenium HTML test suites.
 * @author edegenetais
 *
 */
public class SeleniumHtmlTestSuiteLauncher extends SeleniumServerManager {
    
        private static final Logger LOGGER = LoggerFactory.getLogger(SeleniumHtmlTestSuiteLauncher.class);

	private static final String STDOUT_DISCLAIMER_MESSAGE = "<stdout is monitored through the selenium.server logger>";

	private static final String HTMLSUITE_PARM = "-htmlsuite";
	
	public static final String SELENIUM_BROWSER_STRING_KEY=SELENIUM_MARKER_KEY+".browser";
	
	private File suiteLocation;
	
	/**
	 * Creates a SeleniumHtmlTestSuiteLauncher for a given test suite.
	 * @param suiteLocation
	 */
	public SeleniumHtmlTestSuiteLauncher(File suiteLocation) {
		this.suiteLocation = suiteLocation;
	}

	/**
	 * Launches an HTML test suite.
	 * @param sutUrl URL for the SUT to apply the test suite to.
	 * @param browserString the name under which the browser is known to the Selenium proxy
	 * @throws IOException in case java executable cannot be opened, or the report file cannot be created.
	 */
	public SeleneseResult launchProcess(URL sutUrl, String browserString) throws IOException{
			
			//setting server port on a free TCP port
			int port = getAvailablePort();
			Properties serverConfiguration= new Properties();
			serverConfiguration.setProperty(SELENIUM_PORT_KEY, Integer.toString(port));
			setServerProperties(serverConfiguration);
			
			//creating empty report file (will be filled by the execution process)
			File reportFile=File.createTempFile("selenese", "report.html", TempDir.getExecutionTempDir());
			
			/*building command line arguments*/
			List<String> commandLineArgs=buildCommandLineArgumentList(sutUrl, suiteLocation,
					browserString,reportFile);
			
			ProcessHandle handle=startProxyWith(commandLineArgs);
			
			while(handle.isProcessAlive()){
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					//Don't care
				}
			}
			
			int returnValue;
			Integer processExitCode = handle.returnValue();
			if(processExitCode==null){
				returnValue=2;
			}else{
				returnValue=processExitCode.intValue();
			}
			
			return new SeleneseResult(
					returnValue, 
					STDOUT_DISCLAIMER_MESSAGE, 
					handle.getErrorStream(), 
					buildCommandStringForDisplay(commandLineArgs, "java"), 
					reportFile,suiteLocation
					);
	}

	private List<String> buildCommandLineArgumentList(URL sutUrl, File suiteLocation,
			String browserString, File reportLocation) throws IOException {
		List<String> commandArgs=new ArrayList<String>();
		commandArgs.addAll(buildCommand()); //JVM arguments
		
		commandArgs.add(HTMLSUITE_PARM); //switch for HTML suite execution mode
		
		commandArgs.add(browserString); //choice of the browser for test execution
		
		commandArgs.add(sutUrl.toExternalForm()); //adding starting SUT URL
		
		commandArgs.add(suiteLocation.getAbsolutePath()); //adding main suite HTML file location
		
		commandArgs.add(reportLocation.getAbsolutePath()); //specifying report file location
		
		return commandArgs;
	}

	private int getAvailablePort() {
		int port=1025;
		boolean searching=true;
		while(searching && port<=20000){
			try {
				ServerSocket socket=new ServerSocket(port);
				socket.close();
				searching=false;
			} catch (BindException e) {
                                LOGGER.debug("Could not bind to port {}. Trying next one.",port,e);
				port++;
			}catch(IOException ioe){
				throw new InstructionRuntimeException("I/O error while looking for an available port for the Selenium server.",ioe);
			}
		}
		if(searching){
			throw new InstructionRuntimeException("Found no available port between 1025 and 20000");
		}
		return port;
	}
	
	@Override
	protected void treatPollingException(Exception pollingException) {
		throw new InstructionRuntimeException("Lost communication with Selenium proxy.",pollingException);
	}

	@Override
	protected void treatPollerTimeout() {
		LoggerFactory.getLogger(SeleniumHtmlTestSuiteLauncher.class).debug("Selenium server failed to start within the given time. Test may fail");
	}

	@Override
	protected void mournDeadProcess(String errorStreamContent) {
		//ignore this, all the better if the suite completed so quick!
	}

	@Override
	protected File getWorkingDirectory() {
		return suiteLocation.getAbsoluteFile().getParentFile();
	}

	@Override
	protected int getStderrRecordLength() {
		return 20000;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.one.legacy.library;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.PropertiesUtils;
import org.squashtest.ta.local.process.library.process.ProcessHandle;
import org.squashtest.ta.local.process.library.process.ProcessLogger;
import org.squashtest.ta.plugin.commons.library.java.JavaSystemProcessConnector;


public abstract class SeleniumServerManager extends JavaSystemProcessConnector{

	private static final Logger LOGGER=LoggerFactory.getLogger(SeleniumServerManager.class);
	
	public static final String SELENIUM_MARKER_KEY = "squashtest.ta.selenium";
	public static final String SELENIUM_PORT_KEY = "squashtest.ta.selenium.port";
	public static final String SELENIUM_KEY_PATTERN = Pattern.quote(SELENIUM_MARKER_KEY) + ".*";
	
	private static final String SELENIUM_2_21_0_MAIN_CLASS = "org.openqa.selenium.server.SeleniumServer";
	
	private Properties serverProperties;
	
	protected ProcessHandle startProxyWith(List<String> command) throws IOException {
		
		ProcessBuilder procBuilder=new ProcessBuilder(command);
		procBuilder.directory(getWorkingDirectory());
		
		LOGGER.debug("Starting at "+new SimpleDateFormat("HH:mm:ss").format(new Date()));
		Process serverProcess = procBuilder.start();
		ProcessHandle handle=new ProcessHandle("Selenium server",serverProcess, true,true, getStderrRecordLength());
		
		new ProcessLogger("seleniumServer", serverProcess).start();
		
		// now we must check that the server is actually running			
		          SeleniumProxyPoller poller = new SeleniumProxyPoller(serverProperties);
		
		LOGGER.debug("Selenium server : checking proxy status");
		poller.testProxy();
		
		if (! poller.isServerReady())
		{
			diagnoseAndProceed(poller,handle);
		}
		else
		{	
			LOGGER.info("Selenium server : proxy ready for use");
		}
		return handle;
	}

	protected abstract int getStderrRecordLength();

	protected List<String> buildCommand() {
	
		List<String> arguments = new LinkedList<String>();
	
		// options now, except 'interactive'
		Properties effectiveProperties = PropertiesUtils.staticGetStrippedProperties(serverProperties, SELENIUM_MARKER_KEY + ".");
	
		@SuppressWarnings("unchecked") //as we say in France: "Pourquoi tant de haine, Sun?"
		Enumeration<String> keys = (Enumeration<String>) effectiveProperties.propertyNames();
		
		while (keys.hasMoreElements()) {
	
			String key = keys.nextElement();
	
			if (key.matches("interactive")) {
				continue;
			}	
			arguments.add("-"+key);
			arguments.add(effectiveProperties.getProperty(key));	
		}
	
		List<String> command=createCommand(arguments, SELENIUM_2_21_0_MAIN_CLASS);
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(buildCommandStringForDisplay(command, "Generated command line: "));
		}		
		return command;
	}

	/**
	 * Build a displayable version of the command line string for logs/error reporting.
	 * @param command the command line elements.
	 * @param heading heading to display before the actual instructions.
	 * @return a String to display the command line.
	 */
	protected String buildCommandStringForDisplay(List<String> command, String heading) {
		StringBuilder commandLineStringBuilder=new StringBuilder(heading);
		for(String element:command){
			commandLineStringBuilder.append(element).append(" ");
		}
		return commandLineStringBuilder.toString().trim();
	}
	
	protected void diagnoseAndProceed(SeleniumProxyPoller seleniumProxyPoller, ProcessHandle handle) {
		if (handle.isProcessAlive())
		{
			if (seleniumProxyPoller.timedOut()) {
				treatPollerTimeout();
			} else if (seleniumProxyPoller.gotException()) {
				treatPollingException(seleniumProxyPoller.getException());
			}
		}else{
			mournDeadProcess(handle.getErrorStream());
		}
	}
	
	protected abstract void treatPollingException(Exception pollingException) ;

	protected abstract void treatPollerTimeout(); 

	protected abstract void mournDeadProcess(String errorStreamContent);

	/**
	 * Specifies the desired working directory for the selenium server process.
	 */
	protected abstract File getWorkingDirectory();
	
	protected void setServerProperties(Properties serverProperties) {
		this.serverProperties = serverProperties;
	}

	protected Properties getConfiguration(){
		return new Properties(serverProperties);
	}
}
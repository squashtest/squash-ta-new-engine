/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.selenium.converters

import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.exception.BadDataException
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle
import org.squashtest.ta.plugin.selenium.one.legacy.converters.JavaBundleToJavaSelenium1Converter
import org.squashtest.ta.plugin.selenium.one.legacy.resources.SeleniumOneJavaTest
import org.squashtest.ta.framework.tools.TempDir;

import spock.lang.Specification

class JavaBundleToJavaSelenium1ConverterTest extends Specification {
	
	def "the JavaBundle Must have a main Class that is a JUnit3 Class"(){	
		given :
			def mainClassName = "org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit3Class"
		and :
			def mainClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/RuntimeJUnit3Class.class")
		and :
			ClassLoader classloader = prepareClassLoader(mainClassName, mainClassBytes)
		and :
			def otherClassName = "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"
		and :
			def otherClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/NonJunitClass.class")
		and : 
			ClassLoader classloader2 = prepareClassLoader(otherClassName, otherClassBytes, classloader)
			JavaCodeBundle myJavaCodeBundle = Mock()
			myJavaCodeBundle.getDedicatedClassloader() >> classloader2
			myJavaCodeBundle.getBundleClassNames() >> ["org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit3Class", "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"]
			JavaBundleToJavaSelenium1Converter converter = new JavaBundleToJavaSelenium1Converter()
		when :
			File tempFile = File.createTempFile("groovyTest",".test", TempDir.getExecutionTempDir())
			tempFile.write("org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit3Class")
			FileResource fr = new FileResource(tempFile)
			List<Resource> conf = new ArrayList<Resource>()
			conf.add(fr);
			converter.addConfiguration(conf)
			SeleniumOneJavaTest selenium1 = converter.convert(myJavaCodeBundle)
		then :
			notThrown(BadDataException)
	}
	
	def "the JavaBundle Must have at least one Class that is a JUnit3 Class"(){
		given :
			def mainClassName = "org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit3Class"
		and :
			def mainClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/RuntimeJUnit3Class.class")
		and :
			ClassLoader classloader = prepareClassLoader(mainClassName, mainClassBytes)
		and :
			def otherClassName = "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"
		and :
			def otherClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/NonJunitClass.class")
		and : 
			ClassLoader classloader2 = prepareClassLoader(otherClassName, otherClassBytes, classloader)
			JavaCodeBundle myJavaCodeBundle = Mock()
			myJavaCodeBundle.getDedicatedClassloader() >> classloader2
			myJavaCodeBundle.getBundleClassNames() >> ["org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit3Class", "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"]
			JavaBundleToJavaSelenium1Converter converter = new JavaBundleToJavaSelenium1Converter()
		when :
			SeleniumOneJavaTest selenium1 = converter.convert(myJavaCodeBundle)
		then :
			notThrown(BadDataException)
	}
	
	def "the JavaBundle Must have a main Class that is a JUnit4 Class"(){
		given :
			def mainClassName = "org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit4Class"
		and :
			def mainClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/RuntimeJUnit4Class.class")
		and :
			ClassLoader classloader = prepareClassLoader(mainClassName, mainClassBytes)
		and :
			def otherClassName = "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"
		and :
			def otherClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/NonJunitClass.class")
		and :
			ClassLoader classloader2 = prepareClassLoader(otherClassName, otherClassBytes, classloader)
			JavaCodeBundle myJavaCodeBundle = Mock()
			myJavaCodeBundle.getDedicatedClassloader() >> classloader2
			myJavaCodeBundle.getBundleClassNames() >> ["org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit4Class", "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"]
			JavaBundleToJavaSelenium1Converter converter = new JavaBundleToJavaSelenium1Converter()
		when :
			File tempFile = File.createTempFile("groovyTest",".test", TempDir.getExecutionTempDir())
			tempFile.write("org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit4Class")
			FileResource fr = new FileResource(tempFile)
			List<Resource> conf = new ArrayList<Resource>()
			conf.add(fr);
			converter.addConfiguration(conf)
			SeleniumOneJavaTest selenium1 = converter.convert(myJavaCodeBundle)
		then :
			notThrown(BadDataException)
	}
	
	def "the JavaBundle Must have at least one Class that is a JUnit4 Class"(){
		given :
			def mainClassName = "org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit4Class"
		and :
			def mainClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/RuntimeJUnit4Class.class")
		and :
			ClassLoader classloader = prepareClassLoader(mainClassName, mainClassBytes)
		and :
			def otherClassName = "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"
		and :
			def otherClassBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/NonJunitClass.class")
		and :
			ClassLoader classloader2 = prepareClassLoader(otherClassName, otherClassBytes, classloader)
			JavaCodeBundle myJavaCodeBundle = Mock()
			myJavaCodeBundle.getDedicatedClassloader() >> classloader2
			myJavaCodeBundle.getBundleClassNames() >> ["org.squashtest.ta.plugin.selenium.stuffs.RuntimeJUnit4Class", "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"]
			JavaBundleToJavaSelenium1Converter converter = new JavaBundleToJavaSelenium1Converter()
		when :
			SeleniumOneJavaTest selenium1 = converter.convert(myJavaCodeBundle)
		then :
			notThrown(BadDataException)
	}
	
	def "conversion should fail if no class is of JUnit Type"(){
		given :
			def className = "org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"
		and :
			def classBytes = loadBytes("runtime-classes/org/squashtest/ta/plugin/selenium/stuffs/NonJunitClass.class")
		and :
			ClassLoader classloader = prepareClassLoader(className, classBytes)
			JavaCodeBundle myJavaCodeBundle = Mock()
			myJavaCodeBundle.getDedicatedClassloader() >> classloader
			myJavaCodeBundle.getBundleClassNames() >> ["org.squashtest.ta.plugin.selenium.stuffs.NonJunitClass"]
			JavaBundleToJavaSelenium1Converter converter = new JavaBundleToJavaSelenium1Converter()
		when :
			SeleniumOneJavaTest selenium1 = converter.convert(myJavaCodeBundle)
		then :
			thrown(BadDataException)
	}
	
	def loadBytes(String fileName){
		def URL bytesURL = this.getClass().getClassLoader().getResource(fileName);
	
		return new File(bytesURL.toURI()).getBytes();
	}
	
	def prepareClassLoader(className, classBytes){
		
		def loader = new ClassLoader(this.getClass().getClassLoader()){
			public Class<?> addClass(String name, byte[] bytes, int off, int len){
				return super.defineClass(name, bytes, off, len);
			}
		};
	
		def debug = loader.addClass(className, classBytes, 0, classBytes.length)
		
		return loader;
	}
	
	def prepareClassLoader(className, classBytes, classLoader){
	
		def debug = classLoader.addClass(className, classBytes, 0, classBytes.length)
		
		return classLoader;
	}
}

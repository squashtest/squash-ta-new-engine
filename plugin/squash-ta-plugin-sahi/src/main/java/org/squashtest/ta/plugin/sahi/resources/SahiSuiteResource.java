/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.resources;

import java.io.File;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.commons.resources.AbstractBundle;

/**
 * This resource encapsulates a file as a Sahi test suite.
 * @author edegenetais
 *
 */

@TAResource("script.sahi")
public class SahiSuiteResource extends AbstractBundle implements Resource<SahiSuiteResource> {
	
	/** Noarg constructor for Spring enumeration. */
	public SahiSuiteResource() {}
	
	private SahiSuiteResource(SahiSuiteResource orig){
		super(orig.getBase(),orig.getMain());
	}
	
	public SahiSuiteResource(File base, File mainFile) {
		super(base,mainFile);
	}
	
	@Override
	public SahiSuiteResource copy() {
		return new SahiSuiteResource(this);
	}

	@Override
	public void cleanUp() {
		//noop (GC will be enough)
	}

}

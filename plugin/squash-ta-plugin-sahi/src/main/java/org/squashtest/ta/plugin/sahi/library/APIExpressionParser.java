/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.library;

import java.io.File;
import java.io.IOException;

import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.library.param.Expression;
import org.squashtest.ta.plugin.commons.library.param.ExpressionParser;
import org.squashtest.ta.plugin.commons.library.param.LitteralExpression;

/**
 * Parser to substitute property values in the API javascript file template.
 * @author edegenetais
 *
 */
public class APIExpressionParser implements ExpressionParser {
	
	private static final String BUNDLE_BASE_PREOPERTY_NAME = "bundleBase";
	private File bundleBase;
	
	public APIExpressionParser(File bundleBase) {
		this.bundleBase=bundleBase;		
	}
	@Override
	public boolean accept(String expression) {
		return BUNDLE_BASE_PREOPERTY_NAME.equals(expression);
	}

	@Override
	public Expression parse(String expression) {
		try {
		if(BUNDLE_BASE_PREOPERTY_NAME.equals(expression)){
				return new LitteralExpression((bundleBase.getCanonicalPath()+File.separatorChar).replace("\\", "\\\\"));
		}else{
			return null;
		}
		} catch (IOException e) {
			throw new InstructionRuntimeException("Sahi API expression resolution failed on IO.",e);
		}
	}

}

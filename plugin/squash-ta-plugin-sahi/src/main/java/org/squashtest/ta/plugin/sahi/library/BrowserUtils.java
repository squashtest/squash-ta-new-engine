/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.library;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.sf.sahi.util.BrowserType;
import net.sf.sahi.util.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class was based on net.sf.sahi.client.Browser. It permits to execute
 * command on the sahi proxy.
 * 
 * @author adurand/cruhlmann
 * @author bfranchet
 * 
 */
public class BrowserUtils {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BrowserUtils.class);

	private String sessionId = null;
	private String host = "localhost";
	private int port = 9999;
	private static final String BROWSER_TYPE_TAG_NAME = "browserType";

	/**
	 * Contructor
	 * 
	 * @param host
	 *            : Sahy proxy host
	 * @param port
	 *            : Sahy proxy port
	 */
	public BrowserUtils(String host, int port) {
		this.host = host;
		this.port = port;
		sessionId = Utils.generateId();
	}

	/**
	 * This method returns the list of the browser available for the sahi proxy.
	 * Each element of the list is the browserType to use for the configuration
	 * of an instruction
	 * 
	 * @return list of available browser
	 */
	public List<String> availableBrowser() {
		QueryStringBuilder qs = new QueryStringBuilder();
		qs.add("fileName", "config/browser_types.xml");
		byte[] response = execCommand("ConfigureUI", "readFile", qs);
		return loadBrowserTypes(response);
	}

	private byte[] execCommand(String clazz, String command,
			QueryStringBuilder qs) {
		String proxyURL = getProxyURL(clazz, command, qs);
		final byte[] table = Utils.readURL(proxyURL);
		if (table == null) {
			final String msg = "Sahi proxi is not available at URL : " + proxyURL
					+ ". Maybe Sahi proxy is down or you provide a bad URL";
			LOGGER.error(msg);
			throw new InstructionRuntimeException(msg);
		}
		return table;

	}

	private String getProxyURL(String clazz, String command, QueryStringBuilder queryStringBuilder) {
		QueryStringBuilder queryBuilder = queryStringBuilder;
		if (queryBuilder == null){
			queryBuilder = new QueryStringBuilder();
		}
		queryBuilder.add("sahisid", sessionId);
		return "http://" + host + ":" + port + "/_s_/dyn/" + clazz + "_"
				+ command + queryBuilder.toString();
	}
	
	private List<String> loadBrowserTypes(byte[] response) {
		List<String> browserList = new ArrayList<String>();
		DocumentBuilder parser;
		try (final ByteArrayInputStream is = new ByteArrayInputStream(response)) {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = parser.parse(is);
			final Element root = document.getDocumentElement();
			final NodeList childNodes = root
					.getElementsByTagName(BROWSER_TYPE_TAG_NAME);
			for (int i = 0; i < childNodes.getLength(); i++) {
				Element el = (Element) childNodes.item(i);
				BrowserType browserType = new BrowserType(el);
				browserList.add(browserType.name().trim());
			}
		} catch (SAXException e) {
			LOGGER.warn("Unable to parse response {}, browser list will be returned empty.", response, e);
		} catch (IOException e) {
			LOGGER.warn("There is something rotten in the realm of Danemark. I/O exception occured while reading from in memory response {}. Browser list will be returned empty.", response, e);
		} catch (ParserConfigurationException e) {
			LOGGER.warn("There is something rotten in the realm of Danemark. Response parser is ill-configured. Browser list will be returned empty.", e);
		}
		return browserList;
	}
	

	static class QueryStringBuilder {
		
		private Map<String, String> map = new LinkedHashMap<String, String>();

		public QueryStringBuilder add(String key, String value) {
			map.put(key, value);
			return this;
		}

		public String toString() {
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append("?");
			boolean start = true;
			Set<String> keySet = map.keySet();
			for (Iterator<String> iterator = keySet.iterator(); iterator
					.hasNext();) {
				if (!start){
					stringBuffer.append("&");
				}
				start = false;
				String key = iterator.next();
				String value = map.get(key);
				stringBuffer.append(key);
				stringBuffer.append("=");
				try {
					value = URLEncoder.encode(value, "utf-8");
				} catch (UnsupportedEncodingException e) {
					LOGGER.warn("There is something rotten in the realm of Danemark. Unable to encode value {} in UTF-8.",value, e);
				}
				stringBuffer.append(value);
			}
			return stringBuffer.toString();
		}
	}
}
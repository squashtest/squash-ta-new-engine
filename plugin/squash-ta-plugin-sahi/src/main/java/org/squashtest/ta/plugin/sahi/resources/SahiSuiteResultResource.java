/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.resources;

import java.io.File;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.test.result.GeneralStatus;

/**
 * Resource holding the result of a Sahi test suite. Contents:
 * <ol>
 * <li>Status flag ({@link GeneralStatus})</li>
 * <li>Execution report (file transmitted from Sahi)</li>
 * </ol>
 * 
 * @author edegenetais
 * 
 */

@TAResource("result.sahi")
public class SahiSuiteResultResource implements Resource<SahiSuiteResultResource>{
	/** Execution status of the Sahi suite. */
	private GeneralStatus suiteStatus;

	private File executionReport;

	/** Noarg constructor for Spring enumeration. */
	public SahiSuiteResultResource() {}
	
	public SahiSuiteResultResource(GeneralStatus status, File report){
		this.suiteStatus=status;
		this.executionReport=report;
	}
	
	/**
	 * @return the execution status of the sahi test suite.
	 */
	public GeneralStatus getSuiteStatus() {
		return suiteStatus;
	}
	
	/**
	 * 
	 * @return
	 */
	public File getExecutionReport(){
		return executionReport;
	}

	@Override
	public SahiSuiteResultResource copy() {
		return new SahiSuiteResultResource(suiteStatus,executionReport);
	}

	@Override
	public void cleanUp() {
		//GC only for now, however we might need to think about it!
	}
}

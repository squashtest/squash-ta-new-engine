/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.library;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import net.sf.sahi.ant.Report;
import net.sf.sahi.test.TestRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.plugin.sahi.resources.SahiSuiteResultResource;

/**
 * Port from CATS of the Sahi test suite runner. Used by the sahi components to
 * run sahi test suites.
 * 
 * @author Agnes
 * @author edegenetais
 */
public class SquashSahiRunner {
        //NOSONAR : 
        //we specifically target LO1 IP for replacement, because this specific IP causes some browsers to misbehave,
        //and there is no friggin' point in making this configurable
        private static final String LO_1_IPV4 = "127.0.0.1";//NOSONAR
        
	private static final String DEFAULT_SAHI_PROXY_PORT = "9999";

	private static final String DEFAULT_SAHI_PROXY_HOST = "localhost";

	private static final String DEFAULT_TARGET_URL_PROTOCOL = "default";

	private static final String DEFAULT_TIMEOUT = "60000";
	
	private static final String BROWSER_TYPE = "browserType";

	private static final String HTML_REPORT = "html";
	
	private static final String TIMEOUT = "timeout";

	private static final String ENDPOINT_URL = "endpoint.url";

	private static final String REPORT_FORMAT = "report.format";

	private static final String SAHI_THREAD_NB = "sahi.thread.nb";

	private static final String SAHI_PROXY_PORT = "sahi.proxy.port";

	private static final String SAHI_PROXY_HOST = "sahi.proxy.host";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SquashSahiRunner.class);

	private String sahiHost;

	private String sahiPort;

	private String threadNumber;

	private String reportFormat;

	private String baseUrl;

	private String browserType;

	private BrowserUtils browserUtil;
	
	private Integer timeout;

	/** Package accessible for testability purpose. */
	static class TestRunnerFactory {
		public TestRunner getRunner(String suitePath, String browserType,
				String targetUrl, String proxyHost, String proxyPort,
				String threadNumber) {
			return new TestRunner(suitePath, browserType, targetUrl, proxyHost,
					proxyPort, threadNumber);
		}
	}
	
	private TestRunnerFactory runnerFactory = new TestRunnerFactory();

	public SquashSahiRunner(Properties config) {
		sahiHost = (config
				.getProperty(SAHI_PROXY_HOST, DEFAULT_SAHI_PROXY_HOST)).trim();
		sahiPort = (config
				.getProperty(SAHI_PROXY_PORT, DEFAULT_SAHI_PROXY_PORT)).trim();
		threadNumber = (config.getProperty(SAHI_THREAD_NB, "1")).trim();
		reportFormat = (config.getProperty(REPORT_FORMAT, HTML_REPORT)).trim();
		browserType = (config.getProperty(BROWSER_TYPE, "")).trim();
		baseUrl = config.getProperty(ENDPOINT_URL);
		browserUtil = new BrowserUtils(sahiHost, Integer.parseInt(sahiPort));
		timeout = Integer.parseInt((config.getProperty(TIMEOUT, DEFAULT_TIMEOUT)).trim());
	}

	/**
	 * Runs a Sahi suite against the specified browser, starting at the base
	 * URL.
	 * 
	 * @param suiteFile the sahi suite.
	 * @param targetUrl the base URL of the web application/site to test
	 * @return the result of the Sahi test suite.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public SahiSuiteResultResource run(File suiteFile, URL targetUrl)
			throws IOException, InterruptedException {

		if ("".equals(browserType)) {
			throw new IllegalConfigurationException("Browser type is missing.");
		}

		String targetUrlString = buildUrl(targetUrl);

		logSahiConf();

		checkIfBrowserExists();

		return executeSahiTest(suiteFile, targetUrlString);
	}

	private String buildUrl(URL targetUrl) {
		String targetUrlString;
		if (DEFAULT_TARGET_URL_PROTOCOL.equals(targetUrl.getProtocol())) {
			/*
			 * Manage the legacy case where a default URL is provided. Please
			 * note that this behavior is DEPRECATED, and that new tests should
			 * provide baseURL through the targetURL parameter (comes from a
			 * WebTarget instance at the squashTA test level)
			 */
			targetUrlString = baseUrl;
		} else {
			targetUrlString = targetUrl.toExternalForm();
			baseUrl = targetUrlString;
		}
		return targetUrlString;
	}

	private void checkIfBrowserExists() {
		List<String> availableBrowsers = browserUtil.availableBrowser();
		if (!availableBrowsers.contains(browserType)) {
			throw new IllegalConfigurationException(
					"The browserType ("
							+ browserType
							+ ") cannot be opened. Ensure that:\n"
							+ "- the property 'browserType' from the Squash-TA configuration file matches the name given to the browser in the Sahi configuration file. (Available browser types in the Sahi proxy are : "
							+ availableBrowsers.toString() + ")\n"
							+ "- the browser is properly configured");
		}
	}

	private SahiSuiteResultResource executeSahiTest(File suiteFile,
			String targetUrlString) throws IOException, InterruptedException {
		
		// change of host : "127.0.0.1" by "localhost" to resolve problems with browsers like internetExplorer (Issue : 2613)
		if (LO_1_IPV4.equals(new URL(targetUrlString).getHost())){
			targetUrlString= targetUrlString.replaceFirst(LO_1_IPV4, DEFAULT_SAHI_PROXY_HOST);
		}
		final TestRunner testRunner = runnerFactory.getRunner(
				suiteFile.getAbsolutePath(), browserType, targetUrlString,
				sahiHost, sahiPort, threadNumber);
		File reportFile = FILE_TREE.createTempDirectory("squash_sahi", "."
				+ reportFormat);
		testRunner.addReport(new Report(reportFormat, reportFile
				.getCanonicalPath()));
		
		Future<String> future = Executors.newCachedThreadPool().submit(new BlockingMethodCallable(testRunner));
		
		String statusString = "";
		
		try {
			statusString = future.get(timeout, TimeUnit.MILLISECONDS); 
		} catch (TimeoutException ex) {
			throw new InstructionRuntimeException("Sahi script execution didn't finish within "+timeout+" ms.", ex);
		} catch (InterruptedException e) {
			throw new InstructionRuntimeException("Sahi script execution was interrupted.", e);
		} catch (ExecutionException e) {
			throw new InstructionRuntimeException("Sahi script execution was aborted.", e);
		} finally {
			//The task execution is then canceled
		   future.cancel(true);
		}
		
				
		GeneralStatus status = "SUCCESS".equals(statusString) ? GeneralStatus.SUCCESS
				: GeneralStatus.FAIL;
		return new SahiSuiteResultResource(status, reportFile);
	}

	/**
	 * @param pSahiHost
	 * @param pSahiPort
	 * @param pThreadNumber
	 * @param pReportDir
	 * @param pScriptDir
	 * @param pReportFormats
	 * @param pBrowserType
	 * @param pBaseUrl
	 * @param pSuiteUrl
	 */
	private void logSahiConf() {
		LOGGER.debug("Sahi proxy host = {}", sahiHost);
		LOGGER.debug("Sahi proxy port = {}", sahiPort);
		LOGGER.debug("Sahi thread number = {}", threadNumber);
		LOGGER.debug("Sahi report format(s) = {}", reportFormat);
		LOGGER.debug("Base URL of the webapp under testing = {}", baseUrl);
	}
	
	/** Method accessible for testability purpose. */
	public Integer getTimeOut(){
		return timeout;
	}
	
	private static class BlockingMethodCallable implements Callable<String> {
		
		private TestRunner testRunner;
		
		public BlockingMethodCallable(TestRunner testRunner) {
			this.testRunner = testRunner;
		}

		@Override
		public String call() throws IOException, InterruptedException {
			return testRunner.execute();
		}

	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.library

import org.squashtest.ta.plugin.sahi.library.BrowserUtils.QueryStringBuilder;

import spock.lang.Specification;

class BrowserUtilsTest extends Specification {
	
	BrowserUtils testee
	def host
	def port
	
	def setup(){
		host = "localhost"
		port = 9999
		testee = new BrowserUtils(host, port)
	}
	
	def "the proxy URL should be built"(){
		given :
			URL url = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/properties/db.properties")
			QueryStringBuilder qs = new QueryStringBuilder();
			qs.add("keyQS", "valueQS");
		when :
			def res = testee.getProxyURL("clazz", "command", qs)
		then :
			//the URL must begin as followed
			res.contains("http://localhost:9999/_s_/dyn/clazz_command?keyQS=valueQS")
	}
}

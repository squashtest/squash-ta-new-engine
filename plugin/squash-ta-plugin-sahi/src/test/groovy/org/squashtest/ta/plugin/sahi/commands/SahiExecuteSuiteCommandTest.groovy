/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.commands

import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException
import org.squashtest.ta.plugin.commons.targets.WebTarget
import org.squashtest.ta.plugin.sahi.library.BasicSahiCommandHelper
import org.squashtest.ta.plugin.sahi.resources.SahiSuiteResource
import org.squashtest.ta.framework.tools.TempDir;

import spock.lang.Specification

class SahiExecuteSuiteCommandTest extends Specification{
	SahiExecuteSuiteCommand command
	BasicSahiCommandHelper helper
	SahiSuiteResource suite
	
	def setup(){
		command = new SahiExecuteSuiteCommand()
		helper = Mock(BasicSahiCommandHelper)
		
		command.helper = helper;
		
		suite=Mock()
		suite.getBase() >> new File(".")
		command.setResource(suite)
	}
	
	def "should take valid options file into account"(){

		given:
			FileResource commandConf = mockValidOptions()
		and:
			command.addConfiguration([commandConf])
		and:
			suite.getMain() >> null //we want no main configured in the suite
		when:
			command.apply()
		then:
			1 * helper.setSuite(new File("tests/main.sah"))			
	}
	
	def "should take suite main into account when no options"(){
		given :
			suite.getMain() >> new File("tests/main.sah")
		when :
			command.apply()
		then :
			notThrown(IllegalConfigurationException)
			1 * helper.setSuite(new File("tests/main.sah"))
	}
	
	def "options mainpath should override suite main"(){
		given :
			FileResource commandConf = mockValidOptions()
		and:
			command.addConfiguration([commandConf])
		and:
			suite.getMain() >> new File("tests/suiteMain.sah")
		when :
			command.apply()
		then :
			1 * helper.setSuite(new File("tests/main.sah"))
	}
	
	def "should ignore mainpath from properties resource (Sahi engine config)"(){
		
		given :
			FileResource commandConf = mockMainPathInProperties()
		and:
			command.addConfiguration([commandConf])
		and:
			suite.getMain() >> null //we want no main configured in the suite
		when :
			command.apply()
		then :
			thrown(IllegalConfigurationException)
	}
	
	def "should sort properties and options into engine config and main suite definition, respectively"(){
		
		given :
		
			def conf = [
					mockValidPropertiesFile(),
					mockValidOptions(),
					mockValidPropertiesResource()
					]
		and :
			command.addConfiguration(conf)
		when :
			command.apply()
		then :
			1 * helper.readPropertiesFrom(conf[0])
			1 * helper.readPropertiesFrom(conf[2])
			1 * helper.setSuite(new File("tests/main.sah"))
	}
	
	def "should execute the command"(){
		
		given :
			WebTarget target = Mock()
		and :
			def conf = [ mockValidOptions(), mockValidPropertiesFile() ]
			command.addConfiguration(conf)
		
		when :
			command.setTarget(target)
			command.apply()
		then :
		
			1 * helper.setSuite(_)
			1 * helper.setTarget(target)
			1 * helper.readPropertiesFrom(conf[1])
			1 * helper.apply()
	}
	
	/* utilities */
	
	def mockValidOptions(){
		FileResource res = Mock()
		
		def valid = File.createTempFile("tester", "tester", TempDir.getExecutionTempDir())
		
		valid.withWriter{ it.writeLine("name : mike, buddy : robert, mainpath : tests/main.sah") }
		
		res.getFile() >> valid
		
		return res;
		
	}
	
	def mockMainPathInProperties(){
		FileResource res = Mock()
		
		File valid = File.createTempFile("tester", "tester", TempDir.getExecutionTempDir())
		
		valid.withWriter{ it.writeLine("noise = noise\nmainpath = robert") }
		
		res.getFile() >> valid
		
		return res;
		
	}
	
	def mockValidPropertiesFile(){
		def res = Mock(FileResource)
		
		def valid = File.createTempFile("tester", "tester", TempDir.getExecutionTempDir())
		
		valid.withWriter{ it.writeLine("name = mike\nbuddy = robert") }
		
		res.getFile() >> valid
		
		return res;
		
	}
	
	def mockValidPropertiesResource(){
		def res = Mock(PropertiesResource)
		
		def ppts = new Properties()
		
		ppts.put("browserType", "firefox")
		ppts.put("random", "random")
		
		res.getProperties();
		
		return res;
	}
	
}

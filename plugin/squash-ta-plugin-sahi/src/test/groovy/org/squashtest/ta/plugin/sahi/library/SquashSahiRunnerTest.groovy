/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahi.library

import net.sf.sahi.test.TestRunner;

import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.plugin.sahi.library.SquashSahiRunner;
import org.squashtest.ta.plugin.sahi.library.SquashSahiRunner.TestRunnerFactory

import spock.lang.Specification

class SquashSahiRunnerTest extends Specification {
	def testee
	
	def setup(){
	}
	
	def "should take parameters into account"(){
		given:
			Properties config=new Properties()
			InputStream configStream=getClass().getResourceAsStream("parmTest.properties")
			config.load(configStream)
		and:
			def runnerFactory=Mock(TestRunnerFactory)
			def runner=Mock(TestRunner)
		and:
			testee=new SquashSahiRunner(config)
			testee.runnerFactory=runnerFactory
		and:
			def dummySuite=new File("./dummy")
			def targetUrl="http://www.kernel.org"
		when:
			testee.executeSahiTest(dummySuite,targetUrl)
		then:
			1 * runnerFactory.getRunner(dummySuite.getAbsolutePath(),"firefox",targetUrl,"sahi.squashtest.org","123","2") >> runner 
			1 * runner.addReport({"fancycustom".equals(it.getType())})
	}

	def "should provide default values for proxy, threads & timeout"(){
		given:
			Properties config=new Properties()
			config.setProperty("browserType", "firefox")
		and:
			def runnerFactory=Mock(TestRunnerFactory)
			def runner=Mock(TestRunner)
			runnerFactory.getRunner(_,_,_,_,_,_)>>runner 
		and:
			testee=new SquashSahiRunner(config)
			testee.runnerFactory=runnerFactory
		and:
			def dummySuite=new File("./dummy")
			def targetUrl="http://www.kernel.org"
		when:
			testee.executeSahiTest(dummySuite,targetUrl)
		then:
			1 * runnerFactory.getRunner(dummySuite.getAbsolutePath(),"firefox",targetUrl,"localhost","9999","1") >> runner
			1 * runner.addReport({"html".equals(it.getType())})
			testee.getTimeOut() == 60000;
	}

	def "should throw IllegalConfiguration if no browser type"(){
		given:
		Properties config=new Properties()
	and:
		def runnerFactory=Mock(TestRunnerFactory)
		def runner=Mock(TestRunner)
		runnerFactory.getRunner(_,_,_,_,_,_)>>runner
	and:
		testee=new SquashSahiRunner(config)
		testee.runnerFactory=runnerFactory
	and:
		def dummySuite=new File("./dummy")
		def targetUrl="http://www.kernel.org"
	when:
		testee.run(dummySuite,new URL(targetUrl))
	then:
		thrown IllegalConfigurationException
	}
	
	def "should throw IllegalConfiguration if wrong browser type"(){
		given:
			Properties config=new Properties()
			config.setProperty("browserType", "unknownBrowser")
	and:
		def runnerFactory=Mock(TestRunnerFactory)
		def runner=Mock(TestRunner)
		runnerFactory.getRunner(_,_,_,_,_,_)>>runner
		def browser=Mock(BrowserUtils)
		browser.availableBrowser()>>new ArrayList<String>()
	and:
		testee=new SquashSahiRunner(config)
		testee.runnerFactory=runnerFactory
		testee.browserUtil=browser
	when:
		testee.checkIfBrowserExists()
	then:
		thrown IllegalConfigurationException
	}
	
	def "should trim given parameters"(){
		given:
			Properties config=new Properties()
			InputStream configStream=getClass().getResourceAsStream("sahiTrim.properties")
			config.load(configStream)
		and:
			def runnerFactory=Mock(TestRunnerFactory)
			def runner=Mock(TestRunner)
		and:
			testee=new SquashSahiRunner(config)
			testee.runnerFactory=runnerFactory
		and:
			def dummySuite=new File("./dummy")
			def targetUrl="http://www.kernel.org"
		when:
			testee.executeSahiTest(dummySuite,targetUrl)
		then:
			1 * runnerFactory.getRunner(dummySuite.getAbsolutePath(),"firefox",targetUrl,"sahi.squashtest.org","123","2") >> runner
			1 * runner.addReport({"fancycustom".equals(it.getType())})
			testee.getTimeOut() == 20000;
	}
}

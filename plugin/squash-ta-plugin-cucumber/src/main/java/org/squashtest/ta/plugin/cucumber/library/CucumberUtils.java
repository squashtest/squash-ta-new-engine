/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library;

import cucumber.api.CucumberOptions;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.Map;

/**
 *
 * @author cjuillard
 */
public class CucumberUtils {

    private CucumberUtils() {
    }

    public static String toStringCucumberOptions(CucumberOptions cucumberOptions) {
        StringBuilder out = new StringBuilder("Cucumber CucumberOptions: \n");
        out.append("isDry: ").append(cucumberOptions.dryRun()).append("\n");
        out.append("isStrict: ").append(cucumberOptions.strict()).append("\n");

        out.append("Features: \n");
        for (String feature : cucumberOptions.features()) {
            out.append("\t").append(feature).append("\n");
        }

        out.append("Glue: \n");
        for (String glue : cucumberOptions.glue()) {
            out.append("\t").append(glue).append("\n");
        }

        out.append("JunitOptions: \n");
        for (String option : cucumberOptions.junit()) {
            out.append("\t").append(option).append("\n");
        }

        out.append("TagFilters: \n");
        for (String tag : cucumberOptions.tags()) {
            out.append("\t").append(tag).append("\n");
        }

        out.append("plugin: \n");
        for (String option : cucumberOptions.plugin()) {
            out.append("\t").append(option).append("\n");
        }

        return out.toString();
    }

    public static Object modifyCucumberOptions(Annotation annotation, String key, Object newValue) {
        Object handler = Proxy.getInvocationHandler(annotation);
        Field field;
        try {
            field = handler.getClass().getDeclaredField("memberValues");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(e);
        }
        field.setAccessible(true);
        Map<String, Object> memberValues;
        try {
            memberValues = (Map<String, Object>) field.get(handler);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Object oldValue = memberValues.get(key);
        if (oldValue == null || oldValue.getClass() != newValue.getClass()) {
            throw new IllegalArgumentException();
        }
        memberValues.put(key, newValue);
        return oldValue;
    }

    enum CucumberOptionsKeys {
        FEATURES("features"),
        STRICT("strict"),
        DRYRUN("dryRun"),
        TAGS("tags"),
        PLUGIN("plugin");

        // Members :
        private final String keyName;

        CucumberOptionsKeys(String key) {
            this.keyName = key;
        }

        public String getKeyName() {
            return keyName;
        }
    }

}

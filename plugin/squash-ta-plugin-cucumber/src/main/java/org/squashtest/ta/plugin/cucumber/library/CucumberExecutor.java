/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library;

import cucumber.api.CucumberOptions;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.squashtest.ta.core.tools.ExceptionLogger;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

import org.junit.runner.JUnitCore;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.cucumber.library.CucumberUtils.CucumberOptionsKeys;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;

public class CucumberExecutor {

    private static final ExceptionLogger logger = new ExceptionLogger(CucumberExecutor.class, IllegalConfigurationException.class);
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CucumberExecutor.class);
    private JavaGherkinExecutableUnit javaTest;
    private Collection<Resource<?>> configuration = new ArrayList<Resource<?>>();
    private Class classCucumberRunner = null;
    private static final String CUCUMBER_JUNIT_RUNNER = Conf.RUNNER_CLASS;
    private static final String SRC_TEST_RESOURCES_PREFIX = Conf.SRC_TEST_RESOURCES;
    private static final String TF_TMP_TARGET = Conf.TF_TMP_TARGET; // for runner
    private static final String TF_TMP_CUC = Conf.TF_TMP_CUC;
    private static final String CUC_JSON = Conf.CUC_JSON;
    private boolean isJSON_OK = true;
    private String workingDir = System.getProperty("user.dir");
    private boolean isHTMLGherkin = false;

    public CucumberResult apply(boolean isDryRun, Properties testParams) {
        Result junitResult = applyJunit(isDryRun, testParams);

        //save additionnal reporting 
        File destination = saveCucumberJSON();

        CucumberResult cucumberResult = new CucumberResult(javaTest, junitResult, destination);

        traceJunitResult(junitResult);

        if (isDryRun) {
            filterCucumberResultIsRunable(junitResult);
        } else {
            filterCucumberResultIsSuccess(junitResult);
        }
        return cucumberResult;
    }

    public Result applyJunit(boolean isDryRun, Properties testParams) {
        List<String> features = javaTest.getFeatures();
        List<String> formatedFeatures = new ArrayList<String>();
        for (String test : features) {
            test = test.replace(SRC_TEST_RESOURCES_PREFIX, "");  //oups....
            test = "classpath:" + test;
            formatedFeatures.add(test);
        }
        String[] tabFeatures = new String[formatedFeatures.size()];
        formatedFeatures.toArray(tabFeatures);
        try {
            ClassLoader clazzLoader = javaTest.getDedicatedClassloader();
            if (classCucumberRunner == null) {
                classCucumberRunner = clazzLoader.loadClass(CUCUMBER_JUNIT_RUNNER);
            }
        } catch (ClassNotFoundException ex) {
            throw logger.errAndThrow("Runner for cucumber: class not found. Unable to process features", ex);
        }
        CucumberOptions cucumberOptions = (CucumberOptions) classCucumberRunner.getAnnotation(CucumberOptions.class);
        //override cucumber Options
        CucumberUtils.modifyCucumberOptions(cucumberOptions, CucumberOptionsKeys.FEATURES.getKeyName(), tabFeatures);
        CucumberUtils.modifyCucumberOptions(cucumberOptions, CucumberOptionsKeys.DRYRUN.getKeyName(), isDryRun);
        if(testParams != null){
            String dsName = testParams.getProperty("DSNAME");
            if (StringUtils.isNotEmpty(dsName)) {
                String[] tags = {transformAsTagName(dsName)};
                CucumberUtils.modifyCucumberOptions(cucumberOptions, CucumberOptionsKeys.TAGS.getKeyName(), tags);
            }
        }

        String pathTest = features.get(0).replace(SRC_TEST_RESOURCES_PREFIX, "");

        int index = (isHTMLGherkin ? 3 : 2);
        String[] plugin = new String[index];

        plugin[0] = "pretty";
        plugin[1] = "json:" + TF_TMP_CUC + "/" + CUC_JSON; // pour traitement par TA"    
        if (isHTMLGherkin) {
            plugin[2] = "json:" + TF_TMP_TARGET + "/" + pathTest + "/" + CUC_JSON;
        }
        CucumberUtils.modifyCucumberOptions(cucumberOptions, CucumberOptionsKeys.PLUGIN.getKeyName(), plugin);

        if (LOGGER.isDebugEnabled()) {
            cucumberOptions = (CucumberOptions) classCucumberRunner.getAnnotation(CucumberOptions.class);
            LOGGER.debug("CucumberOptions applies: ");
            LOGGER.debug(CucumberUtils.toStringCucumberOptions(cucumberOptions));
        }
        JUnitCore junit = new JUnitCore();
        return junit.run(classCucumberRunner);        
    }

    private void filterCucumberResultIsSuccess(Result result) {
        for (Failure f : result.getFailures()) {
            if (f.getException().getClass().toString().toLowerCase().contains("cucumber.runtime")) {
                throw new InstructionRuntimeException(f.getMessage(), f.getException());
            }
            if (isRuntimeException(f.getException().getClass())) {
                throw new InstructionRuntimeException(f.getMessage(), f.getException());
            }
        }
        errorIfEmptyJSON();
    }

    private void filterCucumberResultIsRunable(Result result) {
        for (Failure f : result.getFailures()) {
            if (!f.getException().getClass().toString().startsWith("class cucumber.runtime.junit.UndefinedThrowable")) {
                throw new InstructionRuntimeException(f.getMessage(), f.getException());
            }
        }

        //no error now check JSON empty
        errorIfEmptyJSON();
    }

    private File saveCucumberJSON() {
        File destination = null;
        File source = null;
        try {
            File workingDirFile = new File(workingDir, TF_TMP_CUC);
            source = new File(workingDirFile, CUC_JSON);
            if (source.exists() && (source.length() > 2)) {
                File dir = FileTree.FILE_TREE.createTempDirectory("cucumber", "attached");
                destination = new File(dir, source.getName());
                FileUtils.moveFile(source, destination);
                LOGGER.debug("save existing " + Conf.CUC_JSON + " in: " + destination.getAbsolutePath());
                isJSON_OK = true; //.

            } else {
                Logger.getLogger(CucumberExecutor.class.getName()).log(Level.SEVERE, "unable to find " + Conf.CUC_JSON + " or empty file");
                isJSON_OK = false;
            }
            source.delete();
            workingDirFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(CucumberExecutor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return destination;
    }

    private boolean isRuntimeException(Class clazz) {
        while (clazz != null) {
            if ("java.lang.RuntimeException".equals(clazz.getName())) {
                return true;
            }
            clazz = clazz.getSuperclass();
        }
        return false;
    }

    public void addConfiguration(Collection<Resource<?>> configuration) {
        for(Resource<?> configurationResource : configuration){
            if(!(configurationResource instanceof PropertiesResource)){
                this.isHTMLGherkin = true;
                break;
            }
        }
        this.configuration.addAll(configuration);
    }

    public void setTarget(VoidTarget target) {
        //no target 
    }

    public void setResource(JavaGherkinExecutableUnit resource) {
        this.javaTest = resource;
    }

    private void traceJunitResult(Result result) {
        LOGGER.debug(" \n*** JUNIT RESULT ***** ");
        LOGGER.debug("result.wasSuccessful " + result.wasSuccessful());
        LOGGER.debug("result.getRunCount: " + result.getRunCount());
        LOGGER.debug(" result.getIgnoreCount" + result.getIgnoreCount());
        LOGGER.debug(" result.getFailureCount " + result.getFailureCount());
        LOGGER.debug(" --");
        List<Failure> failures = result.getFailures();
        for (Failure failure : failures) {
            LOGGER.debug(" \n **** Description: " + failure.getDescription());
            LOGGER.debug("\n **** TestHeader: " + failure.getTestHeader());
            LOGGER.debug("\n **** Message failure: " + failure.getMessage());
            LOGGER.debug("\n **** Exception: " + failure.getException().getMessage());
            LOGGER.debug("\n **** Trace" + failure.getTrace());
            Throwable cause = failure.getException();
            Class exceptionClass = failure.getException().getClass();
            LOGGER.debug(" exceptionClass:  " + exceptionClass);
            LOGGER.debug(" exceptionClass:  " + exceptionClass.getName());
            List<StackTraceElement> traces = Arrays.asList(cause.getStackTrace());
            LOGGER.debug(" cause:  ");
            for (StackTraceElement trace : traces) {
                LOGGER.debug(" \t\t line " + trace.getLineNumber());
                LOGGER.debug(" \t\t" + trace.getMethodName());
                LOGGER.debug(" \t\t" + trace.getFileName());
                LOGGER.debug(" \t\t" + trace.getClassName());
            }
        }
    }

    private void errorIfEmptyJSON() {
        if (!isJSON_OK) {
            Exception except = new BadDataException("'" + Conf.CUC_JSON
                    + "' file contains no scenario. Possible cause: scenario outline without dataset or empty file");
            throw new InstructionRuntimeException(except.getMessage(), except);
        }
    }

    private String transformAsTagName(String datasetName){
        String datasetNameWithNoSpace = StringUtils.replace(datasetName, " ", "_");
        return new StringBuilder(datasetNameWithNoSpace).insert(0, "@").toString();
    }
}

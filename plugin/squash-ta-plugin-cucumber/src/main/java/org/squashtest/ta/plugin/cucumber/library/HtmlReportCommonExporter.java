/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library;

import freemarker.template.Template;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import static org.squashtest.ta.commons.exporter.html.HtmlResultExporterBase.fillTempHTLMFile;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;


/**
 *
 * @author cjuillard
 */
public abstract class HtmlReportCommonExporter {

    public File commonGenerateHtmlReport(Map<String, Object> map, CucumberResult cucumberResult, Template template, File tempHTMLFile) throws IOException {

        Date today = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
        map.put("timeNow", dateFormat.format(today));
        String uri = cucumberResult.getUri();
        map.put("uri", uri);
        
        // File output
        fillTempHTLMFile(map,template, tempHTMLFile);
         return tempHTMLFile;  
    }
}

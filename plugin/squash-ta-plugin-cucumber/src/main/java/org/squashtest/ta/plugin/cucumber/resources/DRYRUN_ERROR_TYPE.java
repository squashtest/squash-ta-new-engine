/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.resources;

/**
 *
 * @author cjuillard
 */
public enum DRYRUN_ERROR_TYPE {

    NOT_IMPLEMENTED("STEP NOT IMPLEMENTED"),
    EMPTY_METHOD("STEP IMPLEMENTED WITH AN EMPTY JAVA METHOD"),
    EXECPTION_METHOD("STEP IMPLEMENTED WITH JAVA METHOD REDUCE TO 'throw new cucumber.api.PendingException()'");

    private String name = "";

    //Constructeur
    DRYRUN_ERROR_TYPE(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

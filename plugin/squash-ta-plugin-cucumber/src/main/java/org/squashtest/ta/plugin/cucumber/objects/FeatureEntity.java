/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.objects;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.squashtest.ta.plugin.cucumber.json.JsonUtil;
import org.squashtest.ta.plugin.cucumber.library.Conf;

/**
 * Object representing a feature file
 *
 * @author cjuillard
 */
public class FeatureEntity {

    private static final String KEYWORD = "Feature: ";

    private static final String KEY_URI = "uri";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_ID = "id";
    private static final String KEY_ELEMENTS = "elements";

    private static final String STR_URI = "\turi: ";
    private static final String STR_DESCRIPTION = "\tdescription: ";

    private String name;
    private final String description;
    private final String id;
    private final String uri;
//	private /* String[] */String tags; // TODO ...
    private List<ScenarioEntity> scenari = new ArrayList<ScenarioEntity>();

    public FeatureEntity(JSONObject object) {
        assert object != null : "Feature::constructor arg object must not be null";
        uri = JsonUtil.getStringFromKey(object, KEY_URI);
        name = JsonUtil.getStringFromKey(object, KEY_NAME);
        description = JsonUtil.getStringFromKey(object, KEY_DESCRIPTION);
        id = JsonUtil.getStringFromKey(object, KEY_ID);
        JSONArray array = (JSONArray) object.get(KEY_ELEMENTS);
        if (array != null) {
            int scenariNb = array.size();
            JSONObject jsonObject = null;
            for (int i = 0; i < scenariNb; i++) {
                jsonObject = (JSONObject) array.get(i);
                if (jsonObject != null) {
                    ScenarioEntity entity = new ScenarioEntity(jsonObject);
                    scenari.add(entity);
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(Conf.CUC_JSON + " result parsing: \n");
        out.append(KEYWORD);
        out.append(name);
        out.append(" \n ");

        out.append(STR_URI);
        out.append(uri);
        out.append("\n");

        out.append(STR_DESCRIPTION);
        out.append(description);
        out.append("\n\n");

        for (ScenarioEntity scenario : scenari) {
            out.append(scenario.toString());
        }

        return out.toString();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getUri() {
        return uri;
    }

    public List<ScenarioEntity> getScenari() {
        return scenari;
    }

    public static String getKEYWORD() {
        return KEYWORD;
    }

    public void setName(String name) {
        this.name = name;
    }  
}

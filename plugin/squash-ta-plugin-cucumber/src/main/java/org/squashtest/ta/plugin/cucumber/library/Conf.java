/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library;

/**
 * Sahred configuration for TF cucumber plugin and TF gherkin runner
 *
 * @author cjuillard
 */
public class Conf {

    public static final String FEATURE_EXTENTSION = ".feature";
    public static final String SEP = "/";

    public static final String TEST = "test";
    public static final String MAIN = "main";
    public static final String MAIN_JAVA = "main/java";
    public static final String MAIN_RESOURCES = "main/resources";
    public static final String TEST_JAVA = "test/java";
    public static final String TEST_RESOURCES = "test/resources";
    public static final String SRC = "src";
    public static final String RESOURCES = "resources/";
    public static final String SRC_TEST_RESOURCES = "src/test" + SEP + RESOURCES;
    public static final String JAVA = "java/";

    public static final String FOLDER_RUNNER = "ta_runner";
    public static final String FOLDER_BLANK_TEST = "ta_feature";
    public static final String FOLDER_BLANK_TEST_IMPL = "ta_stepsdef";

    public static final String SHORT_RUNNER = "RunCucumberProjectTest";
    public static final String SHORT_BLANK_TEST_IMPL = "TAforCheckUnicityStepsImpl.java";
    public static final String SHORT_BLANK_TEST = "checkUnicityStepsImpl.feature";

    public static final String JAVA_RUNNER_FOLDER = JAVA + FOLDER_RUNNER; //java/ta_runner
    public static final String RESOURCES_BLANKTEST_FOLDER = RESOURCES + FOLDER_BLANK_TEST; // resources/ta_feature
    public static final String JAVA_BLANKTEST_IMPL_FOLDER = JAVA + FOLDER_BLANK_TEST_IMPL; //java/ta_stepsdef

    public static final String RUNNER_CLASS = FOLDER_RUNNER + "." + SHORT_RUNNER;	//ta_runner.RunCucumberProjectTest (class to load)
    public static final String RUNNER_ABS_NAME = FOLDER_RUNNER + SEP + SHORT_RUNNER + ".java";	//ta_runner/RunCucumberProjectTest.java

    public static final String BLANK_TEST = FOLDER_BLANK_TEST + SEP + SHORT_BLANK_TEST; //ta_feature/checkUnicityStepsImpl.feature
    public static final String CUC_FULLPATH_BLANK_TEST = RESOURCES + BLANK_TEST; //resources/ta_feature/checkUnicityStepsImpl.feature
    public static final String BLANK_TEST_IMPL = FOLDER_BLANK_TEST_IMPL + SEP + SHORT_BLANK_TEST_IMPL; //ta_stepsdef/TAforCheckUnicityStepsImpl.java

    public static final String TF_TMP_CUC = "target/reportCucumber";
    public static final String TF_TMP_TARGET = "target/squashTF/reportCucumber"; // for runner
    //warning: cucumner.json used in .tfl files
    public static final String CUC_JSON = "cucumber.json";

    private Conf() {
        //don't instanciate
    }

}

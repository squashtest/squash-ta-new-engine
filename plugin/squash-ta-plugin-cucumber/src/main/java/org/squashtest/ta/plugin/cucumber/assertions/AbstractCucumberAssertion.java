/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.plugin.commons.helpers.AttachmentMangementHelper;
import org.squashtest.ta.plugin.cucumber.json.CucumberJSONParser;
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity;
import org.squashtest.ta.plugin.cucumber.objects.ScenarioEntity;
import org.squashtest.ta.plugin.cucumber.objects.StepEntity;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;

/**
 * Common elements for TaUnaryAssertion after cucumber execution (run or dryrun)
 *
 * @author cjuillard
 */
public abstract class AbstractCucumberAssertion {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCucumberAssertion.class);
    protected CucumberResult cucumberResult;

    protected FeatureEntity feature;

    protected void addAttachementReportContext(ArrayList<ResourceAndContext> context) {

        if (!cucumberResult.getAttachments().isEmpty()) {
            LOGGER.debug("Adding '" + cucumberResult.getAttachments().size() + "' additional execution reports");
            for (File attachment : cucumberResult.getAttachments()) {
                try {
                    AttachmentMangementHelper.injectAttachedFile(attachment, context);
                } catch (IOException ex) {
                    LOGGER.error("Lost attachment {}", attachment.getAbsolutePath(), ex);
                }
            }
        }
    }

    public void loadFeatureResult(File cucumberJSON) {
        List<FeatureEntity> features = CucumberJSONParser.parse(cucumberJSON.getAbsolutePath());
        if (features.size() > 1) {
            throw new IllegalArgumentException("I must be only one feature for one test. Not yet implemented '");
        }
        feature = features.get(0);
    }

    protected Set<String> retrieveLineFromStepName(String stepName) {
        Set<String> response = new HashSet<String>();
        if (feature == null) {
            return response;
        }
        for (ScenarioEntity scenario : feature.getScenari()) {
            for (StepEntity step : scenario.getSteps()) {
                if (step.getName().equals(stepName)) { //equalsIgnoreCase
                    response.add(Long.toString(step.getLine()));
                }
            }
        }
        return response;
    }
}

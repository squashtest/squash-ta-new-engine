/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.cucumber.library.JavaGherkinExecutableUnit;

@TAResource("result.cucumber")
/**
 * TAResource: result of a cucumber Test with attachments, CucumberJSON
 */
public class CucumberResult implements Resource<CucumberResult> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberResult.class);

    private Result result;

    private File cucumberlReport;

    private final Set<File> attachments = new HashSet<>();

    private JavaGherkinExecutableUnit javaTest;

    //filled on assertion for DryRun
    private Set<String> formatedMessages = new HashSet<String>();

    public CucumberResult() {
    }

    public CucumberResult(JavaGherkinExecutableUnit javaTest, Result result, File cucumberlReport, Set<File> attachments, Set<String> formatedMessages) {
        this.result = result;
        this.cucumberlReport = cucumberlReport;
        this.javaTest = javaTest;
        this.attachments.addAll(attachments);
        this.formatedMessages.addAll(formatedMessages);
    }

    /**
     * Use this if there is no report attachement.
     *
     * @param result the execution result object.
     */
    public CucumberResult(Result result) {
        this.result = result;
    }

    /**
     * Use this if there is a single report attachment.
     *
     * @param javaTest
     * @param result the main report.
     * @param additionalReport the report attachment.
     */
    public CucumberResult(JavaGherkinExecutableUnit javaTest, Result result, File additionalReport) {
        this.result = result;
        this.cucumberlReport = additionalReport;
        this.javaTest = javaTest;

    }

    public void setAttachements(Set<File> attachments) {

        try {
            File attachmentDir = FileTree.FILE_TREE.createTempDirectory("cucumber", "attached");

            for (File attachement : attachments) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("CucumberResult::setAttachements for '" + attachement.getName() + "'");
                }
                File newPiece = new File(attachmentDir, attachement.getName());

                copyAttachmentKeepingName(attachement, newPiece);
                this.attachments.add(newPiece);
            }
        } catch (IOException ex) {
            LOGGER.error("CucumberResult: Lost all attachements on error.", ex);
        }
    }

    private void copyAttachmentKeepingName(File attachement, File newPiece) {
        if (attachement.isFile()) {
            try (
                    InputStream in = new FileInputStream(attachement);
                    OutputStream os = new FileOutputStream(newPiece);) {
                IOUtils.copy(in, os);
            } catch (IOException ex) {
                LOGGER.error("Lost attachment " + attachement.getAbsolutePath(), ex);
            }
        } else {
            if (newPiece.mkdir()) {
                LOGGER.debug("Successfully created destination dir {}", newPiece.getAbsolutePath());
                for (File child : attachement.listFiles()) {
                    File newChild = new File(newPiece, child.getName());
                    copyAttachmentKeepingName(child, newChild);
                }
            } else {
                LOGGER.warn("Failed to create attachment destination directory {}");
            }
        }
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public CucumberResult copy() {
        return new CucumberResult(javaTest, result, cucumberlReport, attachments, formatedMessages);
    }

    @Override
    public void cleanUp() {
        if (cucumberlReport != null) {
            deleteWithLogs(cucumberlReport, "Additional report");
            
        }
        for (File attachment : attachments) {
            deleteWithLogs(attachment, "Attachment");
        }
    }

    private void deleteWithLogs(File target, String fileType) {
        boolean deleted = target.delete();
        if (deleted) {
            LOGGER.debug("{} file {} was cleaned up.", fileType, target.getAbsolutePath());
        } else {
           LOGGER.warn("Failed to delete {} file {}", fileType.toLowerCase(), target.getAbsolutePath());
        }
    }

    public boolean isSuccess() {
        return (result.getFailureCount() == 0);
    }

    public int getFailureCount() {
        return result.getFailureCount();
    }

    public List<Failure> getFailures() {
        return result.getFailures();
    }

    public String getMessage(int failureIndex) {
        return result.getFailures().get(failureIndex).getMessage();
    }

    public List<String> getMessages() {
        List<String> messages = new LinkedList<String>();

        for (Failure fail : result.getFailures()) {
            messages.add(fail.getMessage());
        }

        return messages;
    }

    public Throwable getException() {
        return result.getFailures().get(0).getException();
    }

    public List<Throwable> getExceptions() {
        List<Throwable> exceptions = new LinkedList<Throwable>();

        for (Failure fail : result.getFailures()) {
            exceptions.add(fail.getException());
        }

        return exceptions;
    }

    public File getAdditionalReport() {
        return cucumberlReport;
    }

    public Set<File> getAttachments() {
        return Collections.unmodifiableSet(attachments);
    }

    public void setAdditionalReport(File cucumberlReport) {
        this.cucumberlReport = cucumberlReport;
    }

    public Set<String> getBundleClasses() {
        return javaTest.getClassNames();
    }

    public JavaCodeBundleGherkin getBundle() {
        return javaTest.getBundle();
    }

    public ClassLoader getBundleClassLoader() {
        return javaTest.getDedicatedClassloader();
    }

    public String getUri() {
        return javaTest.getFeatures().get(0);
    }

    public Set<String> getFormatedMessages() {
        return formatedMessages;
    }

    public void setFormatedMessages(Set<String> formatedMessages) {
        this.formatedMessages = formatedMessages;
    }

    public JavaGherkinExecutableUnit getJavaTest() {
        return javaTest;
    }

}

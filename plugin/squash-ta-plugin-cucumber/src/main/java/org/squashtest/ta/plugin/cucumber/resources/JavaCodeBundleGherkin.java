/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.resources;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.cucumber.exception.TACucumberException;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;

/**
 * TAResource: Java bundle for cucumber execution
 *
 * @author cjuillard
 */
@TAResource("gherkin.java")
public class JavaCodeBundleGherkin implements Resource<JavaCodeBundleGherkin> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaCodeBundleGherkin.class);
    private File bundleBase;

    private Set<String> classNames;

    private String runnerName;

    /**
     * noarg constructor for Spring enumeration.
     */
    public JavaCodeBundleGherkin() {
    }

    public JavaCodeBundleGherkin(File directoryLocation, String runnerName, Set<String> classNames) {
        directoryLocation.canRead();
        bundleBase = directoryLocation;
        this.runnerName = runnerName;
        this.classNames = Collections.unmodifiableSet(classNames);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("List of classes in gherkin bundle: ");
            for (String clazz : classNames) {
                LOGGER.debug(clazz);
            }
        }
    }

    public ClassLoader getDedicatedClassloader() {
        URL[] classLoaderUrls = null;
        try {
            classLoaderUrls = new URL[]{bundleBase.toURI().toURL()};
        } catch (MalformedURLException ex) {
            throw new TACucumberException("JavaCodeBundleGherkin::getDedicatedClassloader => MalformedURLException", ex);

        }
        URLClassLoader urlClassLoader = new URLClassLoader(classLoaderUrls, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(urlClassLoader);
        return urlClassLoader;
    }

    public Set<String> getBundleClassNames() {
        return this.classNames;
    }

    @Override
    public JavaCodeBundleGherkin copy() {
        return new JavaCodeBundleGherkin(bundleBase, runnerName, classNames);
    }

    @Override
    public void cleanUp() {
   
        try {
            if (bundleBase.exists() && bundleBase.isDirectory()) {
                FILE_TREE.clean(bundleBase);
                LOGGER.debug("JavaCodeBundleGherkin::cleanUp success! ");
            }
        } catch (IOException e) {
            
            if(LOGGER.isDebugEnabled()){//add the exception in debug mode, not in warn/info mode
                 LOGGER.warn("JavaCodeBundleGherkin::cleanup Failed to clean up java Gherkin code bundle base "
                    + bundleBase.getAbsolutePath(), e);
            }else{
               LOGGER.warn("JavaCodeBundleGherkin::cleanup Failed to clean up java Gherkin code bundle base (Set this logger to level debug to get the stacktrace): "
                    + bundleBase.getAbsolutePath(), e.getMessage());
            }
           
        }
    }

    public File getFileBundleBase() {
        return bundleBase;
    }
}

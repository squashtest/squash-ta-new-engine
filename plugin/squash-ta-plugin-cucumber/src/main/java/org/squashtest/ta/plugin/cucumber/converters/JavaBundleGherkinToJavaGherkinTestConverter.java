/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.converters;

import java.util.Collection;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest;
import org.squashtest.ta.plugin.cucumber.resources.JavaCodeBundleGherkin;

/**
 *
 * @author cjuillard
 */
@TAResourceConverter("gherkin.script")
public class JavaBundleGherkinToJavaGherkinTestConverter extends AbstractJavaBundleGherkinToJavaGherkinConverter implements ResourceConverter<JavaCodeBundleGherkin, CucumberJavaTest> {

    /**
     * empty constructor for Spring instanciation purposes
     */
    public JavaBundleGherkinToJavaGherkinTestConverter() {
        //empty constructor for Spring instanciation purposes
    }

    @Override
    public CucumberJavaTest convert(JavaCodeBundleGherkin resource) {
        CucumberJavaTest cucumberTest = null;
        bundleClassLoader = resource.getDedicatedClassloader();
        if (!features.isEmpty()) {
            cucumberTest = new CucumberJavaTest(resource, features);
        } else {
            throw new BadDataException("\n No feature provided. ", null);
        }
        //Not creating test if not existing feature or not feature file
        checkFeatureFile();
        return cucumberTest;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        super.addConfiguration(configuration);
    }

    @Override
    public float rateRelevance(JavaCodeBundleGherkin input) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

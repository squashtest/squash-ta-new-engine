/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.converters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.cucumber.library.Conf;
import org.squashtest.ta.plugin.cucumber.resources.JavaCodeBundleGherkin;

/**
 *
 * @author cjuillard
 */
public abstract class AbstractJavaBundleGherkinToJavaGherkinConverter {

    protected List<String> features = new ArrayList<>();

    protected String customRunner;

    protected ClassLoader bundleClassLoader;

    protected Set<String> classNames;

    protected JavaCodeBundleGherkin bundle;

    public float rateRelevance() {
        return 0.5f;
    }

    public void addConfiguration(Collection<Resource<?>> configuration) {
        // In implementation script TF test, add (a) feature(s) through configuration
        try {
            for (Object object : configuration) {
                if (object instanceof FileResource) {
                    addFeature(object);
                }
            }
        } catch (FileNotFoundException fnfe) {
            throw new BadDataException("Configuration not found\n", fnfe);
        } catch (IOException ioe) {
            throw new BadDataException("Configuration not found\n", ioe);
        }
    }

    public void cleanUp() {
        // no cleanup necessary
    }

    protected boolean isFeatureExtension(String fileName) {
        return fileName.toLowerCase().endsWith(Conf.FEATURE_EXTENTSION);
    }

    protected void checkFeatureFile() {
        for (String feature : features) {
            String srcTestResources = Conf.SRC_TEST_RESOURCES;
            int index = feature.lastIndexOf(srcTestResources);
            if (index >= 0) {
                feature = feature.substring(index + srcTestResources.length());
            }
            if (bundleClassLoader.getResource(feature) == null) {
                throw new BadDataException("\n '" + feature + "' does not exist in bundle", null);
            }
            if (!isFeatureExtension(feature)) {
                throw new BadDataException("\n '" + feature + "' does not have '" + Conf.FEATURE_EXTENTSION +"' extension", null);
            }
        }
    }

    private void addFeature(Object object) throws IOException {
        FileResource fileConf = (FileResource) object;
        SimpleLinesData confLines = new SimpleLinesData(fileConf.getFile().getAbsolutePath());
        for (String conf : confLines.getLines()) {
            features.add(conf);
        }
    }

}

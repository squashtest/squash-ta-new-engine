/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.cucumber.library.Conf;
import org.squashtest.ta.plugin.cucumber.library.HtmlReportGherkinExecuteDetailsFailuresExporter;

/**
 * TAUnaryAssertion for cucumber framework excecution
 *
 * @author cjuillard
 */
@TAUnaryAssertion("success")
public class CucumberIsSucess extends AbstractCucumberAssertion implements UnaryAssertion<CucumberResult> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberIsSucess.class);
    protected HtmlReportGherkinExecuteDetailsFailuresExporter htmlExecuteReportExporter = HtmlReportGherkinExecuteDetailsFailuresExporter.getInstance();

    @Override
    public void setActualResult(CucumberResult actual) {
        this.cucumberResult = actual;

    }

    /**
     * For this assertion, no configuration is necessary. Any injected resource
     * will be ignored.
     */
    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        if (LOGGER.isWarnEnabled() && !configuration.isEmpty()) {
            LOGGER.warn("Ignoring " + configuration.size() + " useless configuration resources (none is expected)");
        }

    }

    @Override
    public void test() throws AssertionFailedException {
        ArrayList<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
        try {
            loadFeatureResult(cucumberResult.getAdditionalReport());
            if (!cucumberResult.isSuccess()) {
                StringBuilder failedTestNames = buildExecutionFailureContext(context);
                throw new AssertionFailedException("scenario(Feature):" + failedTestNames.toString() + String.join(",;-", cucumberResult.getMessages()),
                        cucumberResult, context);
            }
        } catch (IOException ioe) {
            AssertionFailedException reportErrorException
                    = new AssertionFailedException("Test failed. Due to an error, the report could not be generated", cucumberResult, context);
            reportErrorException.initCause(ioe);
            throw reportErrorException;
        }
    }

    /**
     * Add context for cucumber failures excecution. Build additional HTML
     * report.
     *
     * @param context
     * @return
     * @throws IOException
     */
    protected StringBuilder buildExecutionFailureContext(ArrayList<ResourceAndContext> context) throws IOException {

        List<Failure> cucumberFailures = cucumberResult.getFailures();
        StringBuilder failedTestNames = new StringBuilder();
        for (Failure failure : cucumberFailures) {
            failedTestNames.append(failure.getTestHeader());
            failedTestNames.append("\n");
        }

        File htmlCucumberReport = null;
        htmlCucumberReport = htmlExecuteReportExporter.generateHtmlGherkinDetailsReport(cucumberResult);
        FileResource reportResource = new FileResource(htmlCucumberReport).copy();
        ResourceAndContext reportContext = new ResourceAndContext();
        reportContext.setResource(reportResource);
        //we add the ".html" extension so that eclipse will recognise it as a Junit file
        reportContext.setMetadata(new ExecutionReportResourceMetadata(this.getClass(), new Properties(), FileResource.class, "Details.html"));
        context.add(reportContext);

        if (cucumberResult.getAdditionalReport() != null) {
            LOGGER.debug("buildFailureContext : additional report: " + cucumberResult.getAdditionalReport().getAbsolutePath());
            ResourceAndContext additionalReportContext = new ResourceAndContext();
            additionalReportContext.setResource(new FileResource(cucumberResult.getAdditionalReport()).copy());
            additionalReportContext.setMetadata(new ExecutionReportResourceMetadata(CucumberIsSucess.class, new Properties(), FileResource.class, Conf.CUC_JSON));
            context.add(additionalReportContext);
        }
        addAttachementReportContext(context);
        return failedTestNames;
    }
}

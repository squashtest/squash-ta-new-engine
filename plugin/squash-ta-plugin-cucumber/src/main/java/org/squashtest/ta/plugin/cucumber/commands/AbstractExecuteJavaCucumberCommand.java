/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.commands;

import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.plugin.commons.library.PrintStreamTee;
import org.squashtest.ta.plugin.cucumber.library.CucumberExecutor;
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;

public abstract class AbstractExecuteJavaCucumberCommand{

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExecuteJavaCucumberCommand.class);
    protected CucumberExecutor executor = new CucumberExecutor();

    public void addingConfiguration(Collection<Resource<?>> configuration) {
        executor.addConfiguration(configuration);
    }

    public void settingTarget(VoidTarget target) {
        executor.setTarget(target);
    }

    public void settingResource(CucumberJavaTest resource) {
        executor.setResource(resource);
    }

    protected void getAttachements(PrintStreamTee outListener, final HashSet<File> attachements) {
        final String outContent = outListener.getContent();
        Pattern regex = Pattern.compile("\\[\\[ATTACHMENT--\\|([^\\]]*)\\]\\]");
        Matcher attachmentMatcher = regex.matcher(outContent);
        while (attachmentMatcher.find()) {
            final String name = attachmentMatcher.group(1);
            LOGGER.debug("Found file: {}", name);
            File attachement = new File(name);
            attachements.add(attachement);
        }
        LOGGER.debug("Stdout:\n{}", outContent);
    }

    protected final CucumberResult applyCucumber(boolean isDryRun, Properties testParams) {
        PrintStream out = System.out;
        PrintStreamTee outListener = new PrintStreamTee(out);
        final HashSet<File> attachements = new HashSet<File>();
        CucumberResult cucumberResult = null;
        try {
            System.setOut(outListener);
            cucumberResult = executor.apply(isDryRun, testParams);
            cucumberResult.setAttachements(attachements);
        } finally {
           System.setOut(out);
            getAttachements(outListener, attachements);
        }
        return cucumberResult;
    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions;

import java.io.File;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.CodeIterator;
import javassist.bytecode.ConstPool;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.Mnemonic;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;

import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.cucumber.library.Conf;
import org.squashtest.ta.plugin.cucumber.library.HtmlReportDryrunDetailExporter;
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity;
import org.squashtest.ta.plugin.cucumber.objects.ScenarioEntity;
import org.squashtest.ta.plugin.cucumber.objects.StepEntity;
import org.squashtest.ta.plugin.cucumber.resources.DRYRUN_ERROR_TYPE;

/**
 * TAUnaryAssertion for cucumber framework dryrun execution
 *
 * @author cjuillard
 */
@TAUnaryAssertion("runable")
public class CucumberIsRunable extends AbstractCucumberAssertion implements UnaryAssertion<CucumberResult> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberIsRunable.class);
    private static final int PENDING_EXCEPTION_MIN_SIZE = 8;
    private static final int PENDING_EXCEPTION_MAX_SIZE = 10;
    private static final String CODE_CUCUMBER_EXCEPT = "newdupinvokespecialathrow";
    private static final String CODE_CUCUMBER_EXCEPT_2 = "newdupldcinvokespecialathrow";
    private static final String PENDING_EXCEPTION = "cucumber/api/PendingException";
    //pour formattage du msg de l'exception (affich�)
    private static final String MSG_EXCEPT_FOUND = "found : ";
    private static final String MSG_EXCEPT_STEP_NOT_IMPLEMENTED = " steps not implemented ";
    private static final String MSG_EXCEPT_EMPTY_METHOD = "empty(s) method(s) for step(s) line(s)  ";
    private static final String MSG_EXCEPT_PENDING_EXCEPTION = " assumed method(s) with code limited to cucumber.pendingException:  ";

    private final Set<String> linesEmptyMethod = new HashSet<String>();
    private final HashMap<String, HashSet<Long>> emptyMethods = new HashMap<String, HashSet<Long>>();
    private final Set<String> linesPendingExceptionMethod = new HashSet<String>();
    private final HashMap<String, HashSet<Long>> methodsWithCucumberPendingException = new HashMap<String, HashSet<Long>>();
    //reporting
    private final Set<String> formatedMessages = new HashSet<String>();
    private final ArrayList<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
    protected HtmlReportDryrunDetailExporter htmlReportExporter = HtmlReportDryrunDetailExporter.getInstance();
    // for processing
    private ClassPool pool;
    private HashSet<Long> lineList = null;

    @Override
    public void setActualResult(CucumberResult actual) {
        this.cucumberResult = actual;
    }

    /**
     * For this assertion, no configuration is necessary. Any injected resource
     * will be ignored.
     */
    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        if (LOGGER.isWarnEnabled() && !configuration.isEmpty()) {
            LOGGER.warn("Ignoring " + configuration.size() + " useless configuration resources (none is expected)");
        }
    }

    @Override
    public void test() throws AssertionFailedException {
        pool = getClassPool();
        int notImplentedCountInt = 0;
        loadFeatureResult(cucumberResult.getAdditionalReport());
        if (cucumberResult.getJavaTest().isAdditionalDryRun()) {
            additionalChecks(feature);
        }
        try {
            //on peut avoir des failures pour cucumber.runtime.junit.UndefinedThrowable (steps non implement� mais normalement rien d'autre
            String linesNotImplemented = "on line(s): ";
            for (Failure failure : cucumberResult.getResult().getFailures()) {
                if (!formatedMessages.contains(failure.getMessage() + " : " + failure.getException().getClass().toString())) { //proteger par le HasSet mais n�cessaire pour le compteur...
                    //recherche de la ligne dans la feature
                    String stepName = failure.getMessage();
                    LOGGER.debug("stepName => " + stepName);
                    stepName = stepName.substring(stepName.indexOf("\"") + 1, stepName.lastIndexOf("\""));
                    Set<String> lines = retrieveLineFromStepName(stepName);  //on a toutes les lignes ou on trouve la m�thode mais on a bien qu'un seul formatedMessages
                    formatedMessages.add(failure.getMessage() + " # On line(s): " + String.join("-", lines) + " # " + failure.getException().getClass().toString());
                    notImplentedCountInt += 1;
                    linesNotImplemented += String.join("-", lines) + " # ";
                }
            }

            if (!cucumberResult.isSuccess() || !emptyMethods.isEmpty() || !methodsWithCucumberPendingException.isEmpty()) {
                buildFailureContextIsRunnable(context);
                StringBuilder out = new StringBuilder();
                out.append(MSG_EXCEPT_FOUND); //  \n non compris en HTML , <br> remplac� par &lt;br&gt; et non interpr�t� comme balise HTLM ....

                //not implemented Step (cucumber DryRun)
                if (notImplentedCountInt > 0) {
                    out.append(notImplentedCountInt);
                    out.append(MSG_EXCEPT_STEP_NOT_IMPLEMENTED)
                            .append(linesNotImplemented)
                            .append("; ");
                }
                //emptyMethods
                if (!emptyMethods.isEmpty()) {
                    out.append(emptyMethods.size());
                    out.append(MSG_EXCEPT_EMPTY_METHOD);
                    out.append(linesEmptyMethod)
                            .append("; ");
                }

                //suspicious method with throw cucumber.pending.exception
                if (!methodsWithCucumberPendingException.isEmpty()) {
                    out.append(methodsWithCucumberPendingException.size());
                    out.append(MSG_EXCEPT_PENDING_EXCEPTION);
                    out.append(linesPendingExceptionMethod);
                }
                throw new AssertionFailedException(out.toString(), cucumberResult, context);
            }

        } catch (IOException ioe) {
            AssertionFailedException reportErrorException = new AssertionFailedException("Test failed. Due to an error, the report could not be generated", cucumberResult, context);
            reportErrorException.initCause(ioe);
            throw reportErrorException;
        }
    }

    protected void buildFailureContextIsRunnable(ArrayList<ResourceAndContext> context) throws IOException {
        cucumberResult.setFormatedMessages(formatedMessages);
        File htmlCucumberReport = htmlReportExporter.generateHtmlReport(cucumberResult);
        FileResource reportResource = new FileResource(htmlCucumberReport).copy();
        ResourceAndContext reportContext = new ResourceAndContext();
        reportContext.setResource(reportResource);
        //we add the ".html" extension so that eclipse will recognise it as a Junit file
        reportContext.setMetadata(new ExecutionReportResourceMetadata(this.getClass(), new Properties(), FileResource.class, "Details.html"));
        context.add(reportContext);

        if (cucumberResult.getAdditionalReport() != null) {
            LOGGER.debug("buildFailureContextIsRunnable : additional report: " + cucumberResult.getAdditionalReport().getAbsolutePath());
            ResourceAndContext additionalReportContext = new ResourceAndContext();
            additionalReportContext.setResource(new FileResource(cucumberResult.getAdditionalReport()).copy());
            additionalReportContext.setMetadata(new ExecutionReportResourceMetadata(CucumberIsRunable.class, new Properties(), FileResource.class, Conf.CUC_JSON));
            context.add(additionalReportContext);
        }
        addAttachementReportContext(context);
    }

    private void additionalChecks(FeatureEntity feature) {
        LOGGER.debug("starting additionalChecks over Cucumber DryRun");
        String uri = feature.getUri();
        assert uri != null : "uri est null";
        String scenarioName = ""; //false-positive see line 170 
        List<ScenarioEntity> scenari = feature.getScenari();
        //Recherche dans les scenari , les steps pour lesquels il existe une location dans 'match'
        for (ScenarioEntity scenario : scenari) {
            scenarioName = scenario.getName();
            List<StepEntity> steps = scenario.getSteps();
            for (StepEntity step : steps) {
                if (step.getMatch().isLocationExist()) {
                    checkIfEmptyOrPendingException(step, scenarioName);
                }
            }
        }
    }

    private CtMethod retrieveBinaryMethod(String clazz, String method, ClassPool pool) {
        CtClass cc = null;
        try {
            cc = pool.get(clazz);
            List<CtMethod> methods = Arrays.asList(cc.getMethods());

            for (CtMethod ctMethod : methods) {
                if (ctMethod.getName().equals(method)) {
                    LOGGER.debug("Retrieved in pool: " + clazz + "." + method + ", isEmpty => " + ctMethod.isEmpty());
                    return ctMethod;
                }
            }
        } catch (NotFoundException e) {
            LOGGER.error("'" + clazz + "." + method + "' was not found in class pool. Stacktrace: " + e);
        }
        return null;
    }

    private ClassPool getClassPool() {
        if (pool == null) {
            pool = ClassPool.getDefault();
            try {
                pool.insertClassPath(cucumberResult.getBundle().getFileBundleBase().getAbsolutePath());
            } catch (NotFoundException ex) {
                LOGGER.error("AbstractCucumberAssertion: unable to get ClassPool: " + ex);
            }
        }
        return pool;
    }

    /*
        dans le cucumberJson on a juste le nom de la classe qui patch, pas son path => on doit le rechercher ..
        En cas de doublon (path different) , il faudra v�rifier qu'il y a l'annotation attendue et/ou la m�thode chercher  et/ou la bonne signature...
     */
    /**
     * Retrieve full path of a metoh Not yet implemented: duplicate name or
     * overload
     *
     * @param wanted
     * @return
     */
    private synchronized String findClassWithPath(String wanted) {
        String found = wanted;
        Set<String> classes = cucumberResult.getBundleClasses();
        for (String clazz : classes) {
            if (clazz.endsWith(wanted)) {
                found = clazz;
            }
        }
        return found;
    }

    /**
     * check if step exist and is not an empty implementation
     */
    private void checkIfEmptyOrPendingException(StepEntity step, String scenarioName) {
        Long line = -1L;
        String location = step.getMatch().getLocation();
        line = step.getLine();
        String clazz = step.getMatch().getLocationClass();
        String method = step.getMatch().getLocationMethod();
        clazz = findClassWithPath(clazz);

        CtMethod ctMethod = retrieveBinaryMethod(clazz, method, pool);
        if (ctMethod == null) {
            return;
        }

        if (ctMethod.isEmpty()) {
            LOGGER.debug("found empty method => '" + method + "' from class:  '" + clazz + "'");
            linesEmptyMethod.add(Long.toString(line));
            formatedMessages.add("Empty Implementation: '" + clazz + "." + method
                    + "' (" + location + ") for scenario '" + scenarioName + "', StepLine: " + line
                    + " StepName: '" + step.getName() + "'.");
            addLineToList(emptyMethods, location, line);

        } else {
            //check signature of "throw new cucumber.api.PendingException();"
            if (lookForCucumberPendingException(ctMethod)) {
                LOGGER.debug("suspicious  throw new cucumber.api.PendingException in '" + method + "' from class:  '" + clazz + "'");
                linesPendingExceptionMethod.add(Long.toString(line));
                formatedMessages.add("suspicious  throw new cucumber.api.PendingException in '" + clazz + "." + method
                        + "' (" + location + ") for scenario '" + scenarioName + "', StepLine: " + line
                        + " StepName: '" + step.getName() + "'.");
                addLineToList(methodsWithCucumberPendingException, location, line);
            }
        }
    }

    private boolean lookForCucumberPendingException(CtMethod ctMethod) {
        LOGGER.debug("looking for CucumberPending Exception");
        boolean response = false;
        MethodInfo methodInfo = ctMethod.getMethodInfo();
        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
        int codeLenght = codeAttribute.getCodeLength();
        if ((codeLenght != PENDING_EXCEPTION_MIN_SIZE)
                && (codeLenght != PENDING_EXCEPTION_MAX_SIZE)) {
            LOGGER.debug("codeLenght = " + codeLenght + " so bypass looking for Pending Exception");
            return response;
        }

        if (!isCucumberExceptionInClasseNames(codeAttribute)) {
            return response;
        }

        CodeIterator iterator = codeAttribute.iterator();
        String signature = "";
        while (iterator.hasNext()) {
            try {
                int index = iterator.next();
                int op = iterator.byteAt(index);
                signature += Mnemonic.OPCODE[op];
            } catch (BadBytecode ex) {
                LOGGER.error("Error in reading code of method '" + ctMethod.getName() + "'. " + ex);
            }
            if (signature.equalsIgnoreCase(CODE_CUCUMBER_EXCEPT) || signature.equalsIgnoreCase(CODE_CUCUMBER_EXCEPT_2)) {
                response = true;
            }
        }
        return response;
    }

    private void addLineToList(HashMap<String, HashSet<Long>> list, String location, Long line) {
        lineList = list.get(location);
        if (lineList == null) {
            HashSet<Long> newList = new HashSet<Long>();
            newList.add(line);
            list.put(location, newList);
        } else {
            lineList.add(line);
        }
    }

    private boolean isCucumberExceptionInClasseNames(CodeAttribute codeAttribute) {
        ConstPool constPoolMethod = codeAttribute.getConstPool();
        Set<String> classesNames = constPoolMethod.getClassNames();
        return classesNames.contains(PENDING_EXCEPTION);
    }

    /**
     * Called by runner for HTML reporting specific to Gherkin Tests
     * Parse failure message to retrieve empty/throw on dryrun goal
     * @param message
     * @param keySentence
     * @param type
     * @param mapDryrunError
     * @return 
     */
    private static String extractDryrunErrorType(String message, String keySentence, DRYRUN_ERROR_TYPE type, Map<Long, DRYRUN_ERROR_TYPE> mapDryrunError) {
        int pos = message.indexOf(keySentence);
        String partialMsg = message.substring(0, pos);
        int nbError = Integer.parseInt(partialMsg.trim());
        int fin = ((message.indexOf(";") == -1) ? message.length() : message.indexOf(";"));
        String msg = message.substring(message.indexOf("[") + 1, fin - 1);
        msg = msg.replaceAll(" ", "");
        List<String> emptyLine = Arrays.asList(msg.split(","));
        List<Long> listeMethodeVide = new ArrayList<Long>();
        for (String line:emptyLine) {
            listeMethodeVide.add(Long.parseLong(line));           
        }
        if (nbError > 0) {
            for (Long n : listeMethodeVide) {
                mapDryrunError.put(n, type);
            }
        }
        return ((message.length() > (fin + 1)) ? message.substring(fin + 1) : "");

    }

    /**
     * Called by Gherkin runner for HTML exporting
     * @param msg
     * @param mapDryrunError 
     */
    public static void parseMessageErrorDryrun(String msg, Map<Long, DRYRUN_ERROR_TYPE> mapDryrunError) {
        int rLength = MSG_EXCEPT_FOUND.length();
        msg = msg.substring(rLength);
        int iIndex = msg.indexOf(MSG_EXCEPT_STEP_NOT_IMPLEMENTED);
        boolean isEmptyMethod = msg.contains(MSG_EXCEPT_EMPTY_METHOD);
        boolean isPending = msg.contains(MSG_EXCEPT_PENDING_EXCEPTION);

        if (iIndex != -1) {
            int fin = ((msg.indexOf(";") == -1) ? msg.length() : msg.indexOf(";"));
            msg = msg.substring(fin + 1);
        }
        if (isEmptyMethod) {
            msg = extractDryrunErrorType(msg, MSG_EXCEPT_EMPTY_METHOD, DRYRUN_ERROR_TYPE.EMPTY_METHOD, mapDryrunError);
        }
        if (isPending) {
            extractDryrunErrorType(msg, MSG_EXCEPT_PENDING_EXCEPTION, DRYRUN_ERROR_TYPE.EXECPTION_METHOD, mapDryrunError);
        }
    }
}

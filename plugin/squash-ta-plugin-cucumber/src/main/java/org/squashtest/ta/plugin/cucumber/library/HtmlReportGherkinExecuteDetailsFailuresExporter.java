/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.runner.notification.Failure;
import org.squashtest.ta.framework.tools.TempDir;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;

/**
 * Class to generate specific html report for failures on Cucumber Tests Use
 * freemarker: http://freemarker.org/
 *
 */
public class HtmlReportGherkinExecuteDetailsFailuresExporter extends HtmlReportCommonExporter {

    private static final HtmlReportGherkinExecuteDetailsFailuresExporter INSTANCE = new HtmlReportGherkinExecuteDetailsFailuresExporter();

    private List<Failure> failures = new ArrayList<Failure>();

    private HtmlReportGherkinExecuteDetailsFailuresExporter() {
    }

    public static HtmlReportGherkinExecuteDetailsFailuresExporter getInstance() {
        return INSTANCE;
    }

    public File generateHtmlGherkinDetailsReport(CucumberResult cucumberResult) throws IOException {

        // temporary file creation
        File tempHTMLFile = this.generateReport();

        // Freemarker configuration object
        Configuration configuration = new Configuration();

        // Load template from source folder
        configuration.setClassForTemplateLoading(HtmlReportGherkinExecuteDetailsFailuresExporter.class, "templates");
        Template template = configuration.getTemplate("html-gherkin-report.ftl");

        // Build the data-model
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("result", cucumberResult);

        failures = cucumberResult.getFailures();
        map.put("failures", failures);
        return commonGenerateHtmlReport(map, cucumberResult, template, tempHTMLFile);
    }

    public File generateReport() throws IOException {
        return File.createTempFile("TEST-jUnitReport", "Test.html", TempDir.getExecutionTempDir());
    }
}

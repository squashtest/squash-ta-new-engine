/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.plugin.cucumber.library.JavaGherkinExecutableUnit;

@TAResource("gherkin.java.cucumber")
/**
 * TAResource: Cucumber Java Test object link between bundle and feature file to
 * process
 */
public class CucumberJavaTest implements Resource<CucumberJavaTest>, JavaGherkinExecutableUnit {

    private List<String> features = new ArrayList<String>();

    private String runner = null;

    JavaCodeBundleGherkin bundle;

    private boolean additionalDryRunCheck;

    private ClassLoader dedicatedClassLoader;

    public CucumberJavaTest() {
        super();
    }

    public CucumberJavaTest(ClassLoader loader) {
        dedicatedClassLoader = loader;
    }

    public CucumberJavaTest(JavaCodeBundleGherkin bundle) {
        this.bundle = bundle;
        dedicatedClassLoader = bundle.getDedicatedClassloader();
    }

    public CucumberJavaTest(ClassLoader loader, String runner) {
        dedicatedClassLoader = loader;
        this.runner = runner;
    }

    public CucumberJavaTest(ClassLoader loader, List<String> features) {
        dedicatedClassLoader = loader;
        this.features = features;
    }

    public CucumberJavaTest(JavaCodeBundleGherkin bundle, List<String> features) {
        this.bundle = bundle;
        dedicatedClassLoader = bundle.getDedicatedClassloader();
        this.features = features;
    }

    public CucumberJavaTest(ClassLoader loader, List<String> features, String runner) {
        dedicatedClassLoader = loader;
        this.features = features;
        this.runner = runner;
    }

    public void setFeatures(List<String> features) {
        this.features = features;
    }

    @Override
    public List<String> getFeatures() {
        return features;
    }

    @Override
    public CucumberJavaTest copy() {
        if (features.isEmpty()) {
            if (runner == null) {
                return new CucumberJavaTest(dedicatedClassLoader);
            } else {
                return new CucumberJavaTest(dedicatedClassLoader, runner);
            }
        } else {
            if (runner == null) {
                return new CucumberJavaTest(dedicatedClassLoader, features);
            } else {
                return new CucumberJavaTest(dedicatedClassLoader, features, runner);
            }
        }
    }

    @Override
    public void cleanUp() {
        // no action
    }

    @Override
    public ClassLoader getDedicatedClassloader() {
        return dedicatedClassLoader;
    }

    @Override
    public String getRunnerName() {
        return runner;
    }

    @Override
    public JavaCodeBundleGherkin getBundle() {
        return bundle;
    }

    @Override
    public Set<String> getClassNames() {
        return bundle.getBundleClassNames();
    }

    public boolean isAdditionalDryRunCheck() {
        return additionalDryRunCheck;
    }

    public void setAdditionalDryRunCheck(boolean additionalDryRunCheck) {
        this.additionalDryRunCheck = additionalDryRunCheck;
    }

    @Override
    public boolean isAdditionalDryRun() {
        return additionalDryRunCheck;
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.converters;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.converter.CommonFileToJavaCodeBundle;
import org.squashtest.ta.plugin.commons.library.java.CompilationReport;
import org.squashtest.ta.plugin.commons.library.java.CompilerConnector;
import org.squashtest.ta.plugin.commons.library.java.SourceEnumerator;
import org.squashtest.ta.plugin.cucumber.exception.TACucumberException;
import org.squashtest.ta.plugin.cucumber.library.Conf;
import org.squashtest.ta.plugin.cucumber.resources.JavaCodeBundleGherkin;

/**
 * SquashTA Converter. - Convert from loaded java project to bundle. Add to the
 * bundle a default Cuumber runner and a blank test
 *
 * @author cjuillard
 */
@TAResourceConverter("compile.noflat")
public class FileToJavaCodeBundleGherkin extends CommonFileToJavaCodeBundle implements ResourceConverter<FileResource, JavaCodeBundleGherkin> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileToJavaCodeBundleGherkin.class);

    private static final CompilerConnector compilerConnector = new CompilerConnector(true);

    private final List<String> options = new ArrayList<String>();

    @Override
    public float rateRelevance(FileResource input) {
        return 0.5f;
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {

        for (Resource<?> element : configuration) {
            if (element instanceof FileResource) {
                addFileResource((FileResource) element);
            } else {
                LOGGER.warn("Only resources of type file are supported in configuration. Ignored.");
            }
        }
    }

    @Override
    public JavaCodeBundleGherkin convert(FileResource sourceTree) {

        LOGGER.debug("JavaCodeBundleGherkin sourceTree : " + sourceTree.getFile().getAbsolutePath());
        File sourceTreeLoc = sourceTree.getFile();
        ClassLoader classLoader = getClass().getClassLoader();

        URL runnerSourceUrl = classLoader.getResource(Conf.RUNNER_ABS_NAME);
        URL checkFeatureUrl = classLoader.getResource(Conf.BLANK_TEST);
        URL featureImplURL = classLoader.getResource(Conf.BLANK_TEST_IMPL);

        boolean isOK = new File(sourceTreeLoc, Conf.JAVA_RUNNER_FOLDER).mkdirs();
        LOGGER.debug("adding folder " + Conf.JAVA_RUNNER_FOLDER + " to bundle isOK: " + isOK);
        boolean isOK2 = new File(sourceTreeLoc, Conf.RESOURCES_BLANKTEST_FOLDER).mkdirs();
        LOGGER.debug("adding folder " + Conf.RESOURCES_BLANKTEST_FOLDER + " to bundle isOK:" + isOK2);
        boolean isOK3 = new File(sourceTreeLoc, Conf.JAVA_BLANKTEST_IMPL_FOLDER).mkdirs();
        LOGGER.debug("adding folder " + Conf.JAVA_BLANKTEST_IMPL_FOLDER + " to bundle isOK:" + isOK3);

        File destRunner = new File(sourceTreeLoc, Conf.JAVA + Conf.RUNNER_ABS_NAME);
        File destFeature = new File(sourceTreeLoc, Conf.RESOURCES + Conf.BLANK_TEST);
        File destFeatureImpl = new File(sourceTreeLoc, Conf.JAVA + Conf.BLANK_TEST_IMPL);

        try {
            FileUtils.copyURLToFile(runnerSourceUrl, destRunner);
            FileUtils.copyURLToFile(checkFeatureUrl, destFeature);
            FileUtils.copyURLToFile(featureImplURL, destFeatureImpl);
        } catch (IOException ex) {
            throw new TACucumberException("Cucumber-ta-plugin: unable to load/copy embedded runner or files for setup ecosystem phase", ex);
        }

        mergeDirectories(sourceTreeLoc, Conf.JAVA, Conf.MAIN_JAVA);
        mergeDirectories(sourceTreeLoc, Conf.RESOURCES, Conf.MAIN_RESOURCES);
        mergeDirectories(sourceTreeLoc, Conf.JAVA, Conf.TEST_JAVA);
        mergeDirectories(sourceTreeLoc, Conf.RESOURCES, Conf.TEST_RESOURCES);

        SourceEnumerator enumerator = new SourceEnumerator(sourceTree.getFile());
        try {
            File bundleBase = FILE_TREE.createTempDirectory("bundle", ".basedir");
            CompilationReport compileReport = compilerConnector.compile(enumerator.getJavaSourceFiles(), options,
                    bundleBase);
            if (compileReport.isSuccess()) {
                return createBundle(enumerator, compileReport, bundleBase);
            } else {
                if (enumerator.isBundleEmpty() || enumerator.isWrongExtensionNameFile() || enumerator.isJavaIsNotPresent()) {
                    LOGGER.error("It must have java sources in a java subdirectory in resources directory");
                    LOGGER.error("enumerator.isBundleEmpty() " + enumerator.isBundleEmpty());
                    LOGGER.error("enumerator.isWrongExtensionNameFile() " + enumerator.isWrongExtensionNameFile());
                    LOGGER.error("Cucumber enumerator.isJavaIsNotPresent() " + enumerator.isJavaIsNotPresent());

                    throw new BadDataException("Java code compilation failed:\n" + compileReport.getCompilerMessages());
                } else {
                    throw new BadDataException("Java code compilation failed:\n" + compileReport.getCompilerMessages());
                }
            }
        } catch (IOException e) {
            throw new InstructionRuntimeException("JavaCodeBundle compile failed during I/O operation.", e);
        }

    }

    @Override
    public void cleanUp() {
        // noop
    }

    private void addFileResource(FileResource element) {
        try {
             LOGGER.debug("FileToJavaCodeBundleGherkin::addFileResource reading the compilation options ...");
            SimpleLinesData instruction= new SimpleLinesData((element).getFile()
                    .getAbsolutePath());
            String path = instruction.getLines().get(0);
            SimpleLinesData elementContent= new SimpleLinesData(path);
            for (String optionDefinition : elementContent.getLines()) {
                LOGGER.debug(".. add option: " + optionDefinition);
                addOptionDefinition(optionDefinition, options);
            }
        } catch (IOException e) {
            throw new InstructionRuntimeException("Failed to load configuration file", e);
        }
    }

    private JavaCodeBundleGherkin createBundle(SourceEnumerator enumerator, CompilationReport compileReport, File bundleBase) throws IOException {
        for (File resourceFile : enumerator.getResourceFiles()) {
            String resourceName = enumerator.getResourceRelativePath(resourceFile).replaceAll("\\\\", "/");
            File resourceDestination;
            resourceDestination = new File(bundleBase, resourceName);
            LOGGER.debug("JavaCodeBundleGherkin resourceDestination " + resourceDestination.getAbsolutePath() + " "
                    + resourceDestination.getName());
            File resourceParent = resourceDestination.getParentFile();
            checkOrCreateParent(resourceParent);
            BinaryData resourceData = new BinaryData(resourceFile);
            resourceData.write(resourceDestination);
        }
        LOGGER.debug("compileReport.getCompiledClassNames: " + String.join(" :-:", compileReport.getCompiledClassNames()));
        return new JavaCodeBundleGherkin(bundleBase, null, compileReport.getCompiledClassNames());

    }

    private void checkOrCreateParent(File resourceParent) {
        if (resourceParent != null && !resourceParent.exists()) {
            boolean created = resourceParent.mkdirs();
            if (!created) {
                throw new InstructionRuntimeException(
                        "Failed to create resource destination in java code bundle: "
                        + resourceParent.getAbsolutePath());
            }
        }
    }

    public static void mergeDirectories(File sourceDir, File targetDir) {

        try {
            FileUtils.copyDirectory(sourceDir, targetDir);
        } catch (IOException ex) {
            LOGGER.error("FileToJavaCodeBundleGherkin CONVERTER Error in mergeDirectories", ex);
        }
    }

    public static void mergeDirectories(File sourceTreeLoc, String srcTarget, String srcSource) {
        File target = new File(sourceTreeLoc, srcTarget);
        File folderToMOve = new File(sourceTreeLoc, srcSource);
        if (folderToMOve.exists() && folderToMOve.isDirectory()) {
            mergeDirectories(folderToMOve, target);
        } else {
            LOGGER.debug("FileToJavaCodeBundleGherkin CONVERTER: noe src/ " + srcSource + " folder to merge");
        }
    }
}

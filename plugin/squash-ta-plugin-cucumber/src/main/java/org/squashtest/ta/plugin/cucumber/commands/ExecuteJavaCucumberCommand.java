/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.commands;

import java.util.Collection;
import java.util.Properties;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest;

/**
 *
 * @author cjuillard
 */
@TACommand("execute")
public class ExecuteJavaCucumberCommand extends AbstractExecuteJavaCucumberCommand implements Command<CucumberJavaTest, VoidTarget> {
    private Properties testParms;

    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        super.addingConfiguration(configuration);
        if(!configuration.isEmpty()){
            final ConfigurationExtractor configurationExtractor = new ConfigurationExtractor(this);
            ConfigurationExtractor.ExtractionResult<Properties> propertiesExtraction=configurationExtractor.extractOptionalPropertiesResourceFromConfiguration(testParms, configuration);
            testParms=propertiesExtraction.getExtractedData();
        }
    }

    @Override
    public void setTarget(VoidTarget target) {
        super.settingTarget(target);
    }

    @Override
    public void setResource(CucumberJavaTest resource) {
        super.settingResource(resource);
    }

    @Override
    public CucumberResult apply() {

        return applyCucumber(false, testParms);
    }

    @Override
    public void cleanUp() {
        //nothing yet 
    }

}

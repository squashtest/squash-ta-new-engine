/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.json;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.plugin.cucumber.exception.TACucumberException;
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity;

/**
 * Parser for cucumber xml cucumber reporting
 *
 * @author cjuillard
 */
public class CucumberJSONParser {

    private static String encoding = null;

    private CucumberJSONParser() {
    }

    public static List<FeatureEntity> parse(String jsonFile) {
        List<FeatureEntity> features = new ArrayList<FeatureEntity>();
        BufferedReader reader = null;

        try {
            if (encoding != null) {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(jsonFile), encoding));
            } else {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(jsonFile)));
            }
            JSONParser jsonParser = new JSONParser();
            JSONArray array = (JSONArray) jsonParser.parse(reader);
            if (array != null) {
                parseFeatureArray(array, features);
            }

        } catch (FileNotFoundException e1) {
            throw new TACucumberException("Cucumber-ta-plugin: unable to find cucumber framework xml execution reporting ", e1);
        } catch (IOException | ParseException e) {
            throw new TACucumberException("Cucumber-ta-plugin: IO or parser error in accessing cucumber xml reporting", e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                Logger LOGGER = LoggerFactory.getLogger(CucumberJSONParser.class);
                LOGGER.warn("IO Error in parsing cucumberJSON. StackTrace: " + e);
            }

        }
        return features;
    }

    private static void parseFeatureArray(JSONArray array, List<FeatureEntity> features) {
        JSONObject jsonObject = null;
        int featureNumber = array.size();
        for (int i = 0; i < featureNumber; i++) {
            jsonObject = (JSONObject) array.get(i);
            if (jsonObject != null) {
                FeatureEntity entity = new FeatureEntity(jsonObject);
                checkName(entity);
                features.add(entity);
            }
        }
    }

    public static String getEncoding() {
        return encoding;
    }

    public static void setEncoding(String encoding) {
        CucumberJSONParser.encoding = encoding;
    }

    public static void checkName(FeatureEntity entity) {
        if ((entity != null) && (entity.getName().isEmpty())) {
            entity.setName(FeatureEntity.getKEYWORD());
        }
    }
}

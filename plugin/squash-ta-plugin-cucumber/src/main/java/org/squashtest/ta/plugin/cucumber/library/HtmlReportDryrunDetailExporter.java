/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.tools.TempDir;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult;

/**
 * A Class to generate html detail report with freemarker
 * Specific HTML report to DryRun Gherkin tests (Cucumber DryRun)
 * (http://freemarker.org/)
 *
 */
public class HtmlReportDryrunDetailExporter extends HtmlReportCommonExporter{

  
    private static final HtmlReportDryrunDetailExporter INSTANCE = new HtmlReportDryrunDetailExporter();
    private static final Logger LOGGER = LoggerFactory.getLogger(HtmlReportDryrunDetailExporter.class);
    private List<String> messages = new ArrayList<String>();
    
      private HtmlReportDryrunDetailExporter() {
    }
    
    public static HtmlReportDryrunDetailExporter getInstance() {
        return INSTANCE;
    }

    public File generateHtmlReport(CucumberResult cucumberResult) throws IOException {

        // temporary file creation
        File tempHTMLFile = this.generateReport();

        // Freemarker configuration object
        Configuration configuration = new Configuration();

        // Load template from source folder
        configuration.setClassForTemplateLoading(HtmlReportDryrunDetailExporter.class, "templates");
        Template template = configuration.getTemplate("html-gherkin-dryrun-report.ftl");

        // Build the data-model
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("result", cucumberResult);
        messages = new ArrayList<String>(cucumberResult.getFormatedMessages());
        LOGGER.debug("HtmlReportDryrunDetailExporter::generateHtmlReport message with: " + String.join("',-;'", messages));
        map.put("messages", messages);
        return commonGenerateHtmlReport(map, cucumberResult, template, tempHTMLFile);
    
    }

    public File generateReport() throws IOException {
        return File.createTempFile("TEST-jUnitReport", "Test.html", TempDir.getExecutionTempDir());
    }
}

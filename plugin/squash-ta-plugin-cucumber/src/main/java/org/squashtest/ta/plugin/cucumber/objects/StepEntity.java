/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.objects;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.squashtest.ta.plugin.cucumber.json.JsonUtil;

/**
 * Object representing step of a sceanrio of a feature file
 *
 * @author cjuillard
 */
public class StepEntity {

    private static final String KEY_NAME = "name";
    private static final String KEY_LINE = "line";
    private static final String KEY_KEYWORD = "keyword";  //GIVEN, WHEN, ...
    private static final String KEY_MATCH = "match";		//JSONObject
    private static final String KEY_MATCH_LOCATION = "location";
    private static final String KEY_MATCH_ARGUMENTS = "arguments";
    private static final String KEY_RESULTS = "result";   //array

    private String name;
    /*
     * Step line in feature file
     */
    private Long line;
    private String keyword;
    /*
     * Object for Step Implementation and arguments used if executed
     */
    private MatchEntity match;
    /*
     * Object for result of the step execution (status and duration)
     */
    private ResultEntity result;

    public StepEntity(JSONObject object) {
        name = JsonUtil.getStringFromKey(object, KEY_NAME);
        line = (Long) object.get(KEY_LINE);
        line = (line == null ? -1 : line);
        keyword = JsonUtil.getStringFromKey(object, KEY_KEYWORD);
        //Method Matches
        JSONObject obj = (JSONObject) object.get(KEY_MATCH);
        if (obj != null) {
            String location = JsonUtil.getStringFromKey(obj, KEY_MATCH_LOCATION);
            match = new MatchEntity(location,
                    (JSONArray) obj.get(KEY_MATCH_ARGUMENTS));
        } else {
            match = new MatchEntity();
        }

        obj = (JSONObject) object.get(KEY_RESULTS);
        if (obj != null) {
            result = new ResultEntity(obj);
        } else {
            result = new ResultEntity();
        }
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append(result.getStatus()).append(" # ");
        out.append(line.toString()).append(":");
        out.append(keyword).append("->");
        out.append(name).append(" # ");
        out.append(match.getLocation().isEmpty() ? "No location found" : match.getLocation())
                .append(" #  args:").append(match.getArgumentsInLine());
        if (result.isErrorMessageExist()) {
         out.append(result.getStatus()).append(" \n\t\t errorMessage -> ");
         out.append(result.getErrorMessage());
        }
        out.append("\n");
        return out.toString();
    }

    public String getName() {
        return name;
    }

    public Long getLine() {
        return line;
    }

    public String getKeyword() {
        return keyword;
    }

    public MatchEntity getMatch() {
        return match;
    }

    public ResultEntity getResult() {
        return result;
    }

}

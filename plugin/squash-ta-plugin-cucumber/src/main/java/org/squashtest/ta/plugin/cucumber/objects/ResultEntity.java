/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.objects;

import org.json.simple.JSONObject;
import org.squashtest.ta.plugin.cucumber.json.JsonUtil;

/**
 * Object representing the result of a step of a feature file processed with
 * cucumber
 *
 * @author cjuillard
 */
public class ResultEntity {

    private static final String KEY_RESULTS_DURATION = "duration";
    private static final String KEY_RESULTS_STATUS = "status";
    private static final String KEY_ERROR_MESSAGE = "error_message";

    private Long duration = -1L;
    private String status = "";
    private boolean resultExist = false;
    private boolean errorMessageExist = false;
    private String errorMessage = "";

    /*
	 * Constructor called when no ResultExist
     */
    public ResultEntity() {
    }

    public ResultEntity(JSONObject object) {
        this.duration = (Long) object.get(KEY_RESULTS_DURATION);
        this.duration = (duration == null ? -1L : duration);
        status = JsonUtil.getStringFromKey(object, KEY_RESULTS_STATUS);
        resultExist = true;
        errorMessage = (String) object.get(KEY_ERROR_MESSAGE);
      
        errorMessageExist = ((errorMessage== null) ? false : true);
        if (errorMessageExist) {

            errorMessage = errorMessage.replaceAll("✽", "*");
        }
        else {
            errorMessage = "No stackTrace available";
        }
    }

    public Long getDuration() {
        return duration;
    }

    public String getStatus() {
        return status;
    }

    public boolean isResultExist() {
        return resultExist;
    }

    public boolean isErrorMessageExist() {
        return errorMessageExist;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}

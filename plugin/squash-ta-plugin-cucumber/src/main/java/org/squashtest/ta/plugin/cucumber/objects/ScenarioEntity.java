/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.objects;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.cucumber.json.JsonUtil;

/**
 * Object representing an executed block from feature file ie either a
 * background, either a scenario, or a dataset from a scenario outline
 *
 * @author cjuillard
 */
public final class ScenarioEntity {

    // Keys in JSON File
    private static final String KEY_TYPE = "type";
    private static final String KEY_KEYWORD = "keyword";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_ID = "id";
    private static final String KEYS_STEPS = "steps";

    private static final String STR_DESCRIPTION = "\tdescription: ";

    private String type; // Background or Scenario 
    private boolean isSeveral = false; //scenario outline or background several executed
    private String keyword; // BackGround or Scenario or Scenario Outline.  Language dependent
    private String name;
    private String description;
    private String id; // id (FormatedFeatureName,FormatedScenarioName;;dataSet number)
    private String stDataSetNb;
    private int dataSetNumber;
    private int totalDatasetOfScenarioOutline; // number of Dataset ran for the scenario Outline
    private /* String[] */ String tags = "";
    private List<StepEntity> steps = new ArrayList<StepEntity>();

    public ScenarioEntity(JSONObject object) {
        type = JsonUtil.getStringFromKey(object, KEY_TYPE);
        keyword = JsonUtil.getStringFromKey(object, KEY_KEYWORD);
        name = JsonUtil.getStringFromKey(object, KEY_NAME);

        description = JsonUtil.getStringFromKey(object, KEY_DESCRIPTION);
        id = JsonUtil.getStringFromKey(object, KEY_ID);
        updateNameAndId();

        JSONArray array = (JSONArray) object.get(KEYS_STEPS);
        if (array != null) {
            int nbsteps = array.size();
            JSONObject jsonObject = null;
            for (int i = 0; i < nbsteps; i++) {
                jsonObject = (JSONObject) array.get(i);
                StepEntity entity = new StepEntity(jsonObject);
                steps.add(entity);
            }
        }

        JSONArray tagsJSON = (JSONArray) object.get(keyword);
        if (tagsJSON != null) {
            tags = getTags(tagsJSON);
        }
    }

    public final String getTags(JSONArray array) {
        int arraySize = array.size();
        JSONObject jsonObject = null;
        StringBuilder tmp = new StringBuilder();
        while (arraySize > 0) {
            jsonObject = (JSONObject) array.get(arraySize - 1);
            tmp.append((String) jsonObject.get(KEY_NAME)).append(" ");
            arraySize -= 1;
        }
        return tmp.toString();
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(type);
        out.append(" (").append(keyword).append(")");
        out.append(" - id = ").append(id).append("\n");
        out.append(" - tags:");
        out.append(tags);
        out.append("; dataset: ").append(stDataSetNb).append("=").append(dataSetNumber); //debug.. -> null=-1 if no dataset
        out.append("; name = ").append(name);
        out.append("\n");

        out.append(STR_DESCRIPTION);
        out.append(description);
        out.append("\nSTEPS:\n");

        for (StepEntity step : steps) {
            out.append(step.toString());
        }
        out.append("\n");
        return out.toString();
    }

    public String getKeyword() {
        return keyword;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getStDataSetNb() {
        return stDataSetNb;
    }

    public int getDataSetNumber() {
        return dataSetNumber;
    }

    public String getTags() {
        return tags;
    }

    public List<StepEntity> getSteps() {
        return steps;
    }

    public int getTotalDatasetOfScenarioOutline() {
        return totalDatasetOfScenarioOutline;
    }

    public void setTotalDatasetOfScenarioOutline(int totalDatasetOfScenarioOutline) {
        this.totalDatasetOfScenarioOutline = totalDatasetOfScenarioOutline;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsSeveral() {
        return isSeveral;
    }

    public void setIsSeveral(boolean isSeveral) {
        this.isSeveral = isSeveral;
    }

    public String getType() {
        return type;
    }

    public void setStDataSetNb(String stDataSetNb) {
        this.stDataSetNb = stDataSetNb;
    }

    public void setDataSetNumber(int dataSetNumber) {
        this.dataSetNumber = dataSetNumber;
    }

    /**
     * Warning: ID don't exist for backgroud, so valid only for scenario or
     * scenario outline
     */
    private void updateNameAndId() {
        if ((name.isEmpty())) {
            setName(keyword);
        }
       
        int index = id.lastIndexOf(";;");
        if (index > 0) {
            stDataSetNb = id.substring(id.lastIndexOf(";;") + 2);
            try {
                dataSetNumber = Integer.parseInt(stDataSetNb);
            } catch (NumberFormatException e) {
                throw new BadDataException("Error in parsing cucumber.json: Bad scenario's iteration string '" + stDataSetNb + "'"
                        + ", cannot be convert in integer", e);
            }
            isSeveral = true;
        }
    }
}

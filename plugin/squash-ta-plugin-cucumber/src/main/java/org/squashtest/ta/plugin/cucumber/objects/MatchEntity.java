/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.objects;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Object representing the match of a step of a feature file
 *
 * @author cjuillard
 */
public class MatchEntity {

    private static final String KEY_MATCH_ARGUMENTS_VAL = "val";
    /*
     * format fullClassName.methodeName without signature (no surcharge (yet) supported)
     * empty if not exist
     */
    private String location = "";
    private boolean locationExist = false;
    private String locationClass = "";
    private String locationMethod = "";
    /*
	 * arguments passed during execution. Formated in a single String
     */
    private String argumentsInLine = "";
    private boolean argumentsExist = false;
    /*
	 * arguments passed during execution. List.
     */
    private List<String> arguments = new ArrayList<String>();

    /*
	 * Constructor called when no Match exists in JSON All fields are set with
	 * default values
     */
    public MatchEntity() {
    }

    /*
	 * Constructor when ' Match' exists in JSON
     */
    public MatchEntity(String location, JSONArray jsonArguments) {
        super();
        this.location = location;
        int index = location.indexOf("(");
        if (index > 0) {
            location = location.substring(0, index);
            index = location.lastIndexOf(".");
            locationClass = location.substring(0, index);
            locationMethod = location.substring(index + 1, location.length());
            locationExist = true;
        }
        if (jsonArguments != null) {
            int nbsteps = jsonArguments.size();
            for (int i = 0; i < nbsteps; i++) {
                readArguments((JSONObject) jsonArguments.get(i));
            }
        }
    }

    public String getLocation() {
        return location;
    }

    public List<String> getArguments() {
        return arguments;
    }

    public String getArgumentsInLine() {
        return argumentsInLine;
    }

    public boolean isLocationExist() {
        return locationExist;
    }

    public boolean isArgumentsExist() {
        return argumentsExist;
    }

    public String getLocationClass() {
        return locationClass;
    }

    public String getLocationMethod() {
        return locationMethod;
    }

    private void readArguments(JSONObject jsonObject) {
        String arg;
        if (jsonObject != null) {
            arg = (String) jsonObject.get(KEY_MATCH_ARGUMENTS_VAL);
            if (arg != null) {
                argumentsExist = true;
                arguments.add(arg);
                argumentsInLine = argumentsInLine.concat("'" + arg + "'; ");
            }
        }
    }
}

 Feature: T3: Is it Friday Yet - 1Step no define?
  No Exception on undefine Step

  Scenario Outline: Today is or is not Friday
    Given today is "<day>"
    When no existing(matching) method for this step
    Then I should be told "<answer>"

  Examples:
    | day | answer |
    | Friday | TGIF |
    | Sunday | Nope |
    | anything else! | Nope |
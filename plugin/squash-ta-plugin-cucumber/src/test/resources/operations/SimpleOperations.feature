 Feature: T1: Is Sum of a and b?
  check sum

  @WithDataset
  Scenario Outline: add numbers
    Given first member is <firstNumber>
    Given second member is <secondNumber>
    Then sum should be <result>

  Examples:
    | firstNumber | secondNumber | result | 
    |1|1|2|
    |2|2|6|
    |2|3|5|
    
    @WithoutDataset
    Scenario: multiply numbers
    Given first factor is 2
    Given second factor is 5
    Then product should be 10
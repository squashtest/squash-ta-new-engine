/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.cucumber.commands.ExecuteDryrunCucumberCommand
import org.squashtest.ta.plugin.cucumber.commands.ExecuteJavaCucumberCommand
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult
import org.junit.runner.Description
import org.junit.Ignore;

import spock.lang.Specification


class DryRunCommandTest extends Specification {

    URLClassLoader urlClassLoader
    def listeFeature
    ExecuteDryrunCucumberCommand cmdDryRun = new ExecuteDryrunCucumberCommand()
    CucumberIsRunable cucumberIsRunable = new CucumberIsRunable()
    CucumberResult result 
    
    def "Checking command Dryrun on features "(){
		
        setup : 
        setUrlClassLoader()
                            
        when :
        // 1 scenario. 3 datasets. All steps are defined. 1 step is KO at execution but no impact for DryRun
        executeDryRun("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature")
        then :                 
        assert (result.getUri() == listeFeature[0])                      
        assert (result.getResult().wasSuccessful() == true)  
        assert( result.getResult().getIgnoreCount() == 0)
        assert (result.getResult().getRunCount() == 3)
        // execute and not dryrun
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(0).getResult().getStatus() == "skipped")
        assert(!cucumberIsRunable.feature.getScenari().get(0).getSteps().get(0).getMatch().getLocation().isEmpty())
           
        when :
        // 1 scenario. 3 datasets. Step[1] are not defined
        executeDryRun("squash/T3_is_it_friday_3steps_3datasets_NoMatchingMethodStepWhen.feature")
             
        then :
        assert (result.getUri() == listeFeature[0])                       
        assert (result.getResult().wasSuccessful() == false)  
        assert (result.getResult().getRunCount() == 3)
        assert( result.getResult().getIgnoreCount() == 0)
        assert (result.getResult().getFailureCount() == 3)           
        // same failure for the 3 datasets
        assert (result.getResult().getFailures().get(0).getException().toString() == result.getResult().getFailures().get(1).getException().toString())
        assert (result.getResult().getFailures().get(2).getException().toString() == result.getResult().getFailures().get(1).getException().toString())
        assert (result.getResult().getFailures().get(0).getException().toString() == "cucumber.runtime.junit.UndefinedThrowable: The step \"no existing(matching) method for this step\" is undefined")                                                                               
        //step 1 not implemented
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(1).getResult().getStatus() == "undefined")
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(1).getResult().getStatus() == 
            cucumberIsRunable.feature.getScenari().get(1).getSteps().get(1).getResult().getStatus())
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(1).getResult().getStatus() == 
            cucumberIsRunable.feature.getScenari().get(2).getSteps().get(1).getResult().getStatus())                                
        //so location is empty
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(1).getMatch().getLocation().isEmpty())
        assert(cucumberIsRunable.feature.getScenari().get(1).getSteps().get(1).getMatch().getLocation().isEmpty())
        assert(cucumberIsRunable.feature.getScenari().get(2).getSteps().get(1).getMatch().getLocation().isEmpty())
        //but step 0 is defined
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(0).getResult().getStatus() == "skipped")
        assert(cucumberIsRunable.feature.getScenari().get(0).getSteps().get(0).getMatch().getLocation() == "StepdefsIsItFriday.today_is(String)")
                        
        
    }	

    def "setUrlClassLoader" () {
        URL[] bytesURL = this.getClass().getClassLoader().getResource("");
        this.urlClassLoader = new URLClassLoader(bytesURL, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(this.urlClassLoader);       
    }
	
    def "executeDryRun" (String script) {                 
        listeFeature = [script]
        CucumberJavaTest test = new CucumberJavaTest(urlClassLoader, listeFeature)
        cmdDryRun.setResource(test)
        result = cmdDryRun.apply()
        File json = result.getAdditionalReport()               
        cucumberIsRunable.loadFeatureResult(json)
    } 
}



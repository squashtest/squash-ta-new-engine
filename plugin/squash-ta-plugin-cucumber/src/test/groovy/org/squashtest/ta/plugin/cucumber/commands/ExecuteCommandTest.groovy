/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.cucumber.commands.ExecuteJavaCucumberCommand
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult
import org.junit.runner.Description
import org.junit.Ignore;

import spock.lang.Specification


class ExecuteCommandTest extends Specification {

    URLClassLoader urlClassLoader
    def listeFeature
    ExecuteJavaCucumberCommand cmdExe = new ExecuteJavaCucumberCommand()
    CucumberIsSucess cucumberIsSucess = new CucumberIsSucess()
    CucumberResult result 
    
    def "Executing features  "(){
		
        setup : 
        setUrlClassLoader()
                            
        when :
        //  1 scenario. All steps are defined. 3 datasets. All dataSets in sucess
        executeCucumber ("squash/T1_is_it_friday_3steps_3datasets.feature")
      
        then :                 
        assert (result.getUri() == listeFeature[0])                      
        assert (result.getResult().wasSuccessful() == true)  
        assert( result.getResult().getIgnoreCount() == 0)
        assert (result.getResult().getRunCount() == 3)
        // execute and not dryrun
        assert(cucumberIsSucess.feature.getScenari().get(0).getSteps().get(0).getResult().getStatus() == "passed")
           
        when :
        //  1 scenario. All steps are defined. 3 datasets. Success for DataSet[0,1], failed for third dataset on one step
        executeCucumber ("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature")
                          
        then :
        assert (result.getUri() == listeFeature[0])                       
        assert (result.getResult().wasSuccessful() == false)  
        assert (result.getResult().getRunCount() == 3)
        assert( result.getResult().getIgnoreCount() == 0)
        assert (result.getResult().getFailureCount() == 1)               
        assert (result.getResult().getFailures().get(0).getException().toString() == "org.junit.ComparisonFailure: expected:<[TGIF]> but was:<[Nope]>")                                                                                         
        //first dataset OK
        assert(cucumberIsSucess.feature.getScenari().get(0).getSteps().get(0).getResult().getStatus() == "passed")
        //third dataset step 3 KO
        assert(cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getResult().getStatus() == "failed")
    }
        
    def "setUrlClassLoader" () {
        URL[] bytesURL = this.getClass().getClassLoader().getResource("");
        this.urlClassLoader = new URLClassLoader(bytesURL, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(this.urlClassLoader);       
    }
	
    def "executeCucumber" (String script) {                 
        listeFeature = [script]
        CucumberJavaTest test = new CucumberJavaTest(urlClassLoader, listeFeature)
        cmdExe.setResource(test)
        result = cmdExe.apply()
        File json = result.getAdditionalReport()                 
        cucumberIsSucess.loadFeatureResult(json)
    } 
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions

import org.junit.Ignore
import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity
import org.squashtest.ta.plugin.cucumber.objects.ScenarioEntity

import spock.lang.Specification


class ParserCucumberJsonTest extends Specification {

	def "checking cucumberJSON parsing"(){
		
		setup : 
			File jsonResult = new File("src/test/resources/CucumberJSON/cucumberDryRun.json")
                         
                        CucumberIsSucess cucumberIsSucess = new CucumberIsSucess()
                       
		
		when :
			cucumberIsSucess.loadFeatureResult(jsonResult)
     
		then :
                        assert (cucumberIsSucess.feature != null)
                        assert (cucumberIsSucess.feature.getName() == "T7 complexeOperation: ?")
                        assert (cucumberIsSucess.feature.getUri() == "mypackage/subfolder/T7ComplexeOperations.feature")
                        assert (cucumberIsSucess.feature.getDescription() == "  ...not important")                        
                        assert(cucumberIsSucess.feature.getScenari().size() == 5)
                        
                        assert(cucumberIsSucess.feature.getScenari().get(0).getKeyword() == "Scenario Outline")
                        assert (cucumberIsSucess.feature.getScenari().get(0).getSteps().get(0).getKeyword()
                                     == cucumberIsSucess.feature.getScenari().get(1).getSteps().get(0).getKeyword())
                        assert (cucumberIsSucess.feature.getScenari().get(1).getSteps().get(0).getKeyword() == cucumberIsSucess.feature.getScenari().get(2).getSteps().get(0).getKeyword())
                        assert (cucumberIsSucess.feature.getScenari().get(0).getSteps().get(0).getKeyword() ==  "Given ")
                        assert (cucumberIsSucess.feature.getScenari().get(0).getSteps().get(1).getKeyword() ==  "Given ")
                        assert (cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getKeyword() ==  "Then ")
                         
                        assert (cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getResult().resultExist == true)
                        assert (cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getResult().status == "skipped")
                        assert (cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getResult().getStatus() == "skipped")
                        assert (cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getMatch().getLocation() == "StepDefSubFolder.complexe_operation_gives(Integer)")
                        assert (cucumberIsSucess.feature.getScenari().get(2).getSteps().get(2).getMatch().getArguments().get(0) == "5")
                        
                        assert (cucumberIsSucess.feature.getScenari().get(3).getName() ==  "divide numbers")
                        assert (cucumberIsSucess.feature.getScenari().get(3).getKeyword() ==  "Scenario")
                        assert (cucumberIsSucess.feature.getScenari().get(3).getDescription() ==  "")
	}
        
        def "checking updateNameAndId from ScenarioEntity"() {
            setup : 
			File jsonResult = new File("src/test/resources/CucumberJSON/background-cucumber.json")
                         
                        CucumberIsSucess cucumberIsSucess = new CucumberIsSucess()
                       
		
		when :
			cucumberIsSucess.loadFeatureResult(jsonResult)
     
		then :
                        assert (cucumberIsSucess.feature != null)
                        assert (cucumberIsSucess.feature.getName() == "SQTA 107 Utilisation d\u0027un contexte")                   
                        assert(cucumberIsSucess.feature.getScenari().size() == 10)
                        ScenarioEntity scenario = cucumberIsSucess.feature.getScenari().get(0);
                        assert(scenario.getType() ==  "background");
        }
}


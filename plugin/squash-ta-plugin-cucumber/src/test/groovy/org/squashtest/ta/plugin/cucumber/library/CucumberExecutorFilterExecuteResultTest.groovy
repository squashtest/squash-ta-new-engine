/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions

import org.squashtest.ta.plugin.cucumber.library.CucumberExecutor
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult

import spock.lang.Specification

class CucumberExecutorFilterExecuteResultTest extends Specification {

    URLClassLoader urlClassLoader
    CucumberExecutor executor
    CucumberJavaTest test
    def listeFeature
    CucumberResult result
         
         
    def "Executor: Checking filters on Junit Result"(){
		
        setup : 
        setUrlClassLoader()
                            
        when :
        //  feature OK
        prepareExecuteCucumber("squash/T1_is_it_friday_3steps_3datasets.feature")       
        result = executor.apply(false, null) //not DryRun
        then :      
        assert (result.getResult().wasSuccessful() == true)  
        
        when :
        //  1 dataSet KO
        prepareExecuteCucumber("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature")       
        result = executor.apply(false, null) //not DryRun
        then :      
        assert (result.getResult().wasSuccessful() == false)  
        
        when :
        //  Divide by zero
        prepareExecuteCucumber("squash/T5_is_it_friday_3steps_Exception_divideBy0.feature")   
        then :      
        try {
            result = executor.apply(false, null)
            assert ( 1 == 2)
        } catch(Exception ex) {
            assert (ex.getClass().toString()).toLowerCase().contains(
                            "org.squashtest.ta.framework.exception.instructionruntimeexception")
            assert (ex.getCause() instanceof ArithmeticException)
            assert (ex.getMessage().contains(ex.getCause().getMessage()))
        }  
        
        when :
        prepareExecuteCucumber("squash/MalformedFeature.feature")   
        then :      
        try {
            result = executor.apply(false, null)
            assert ( 1 == 2)
        } catch(Exception ex) {
            assert (ex.getClass().toString()).toLowerCase().contains(
                            "org.squashtest.ta.framework.exception.instructionruntimeexception")
        }  
        
        when :
        prepareExecuteCucumber("squash/T3_is_it_friday_3steps_3datasets_NoMatchingMethodStepWhen.feature")   
        then :      
        try {
            result = executor.apply(false, null)
            assert ( 1 == 2)
        } catch(Exception ex) {
            assert (ex.getClass().toString()).toLowerCase().contains(
                            "org.squashtest.ta.framework.exception.instructionruntimeexception")
        }  
        
        when :
        prepareExecuteCucumber("squash/T6_NullPointerException.feature")   
        then :      
        try {
            result = executor.apply(false, null)
            assert ( 1 == 2)
        } catch(Exception ex) {
            assert (ex.getClass().toString()).toLowerCase().contains(
                            "org.squashtest.ta.framework.exception.instructionruntimeexception")
        }  
    }

    
    
    def "setUrlClassLoader" () {
        URL[] bytesURL = this.getClass().getClassLoader().getResource("");
        this.urlClassLoader = new URLClassLoader(bytesURL, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(this.urlClassLoader);       
    }
    
    def "prepareExecuteCucumber" (String script) {
        listeFeature = [script]
        CucumberJavaTest test = new CucumberJavaTest(urlClassLoader, listeFeature)
        executor = new CucumberExecutor() //reset JSON_OK
        executor.setResource(test)
    } 
}


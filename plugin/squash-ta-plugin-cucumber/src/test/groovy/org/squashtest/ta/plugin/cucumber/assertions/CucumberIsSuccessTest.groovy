/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.cucumber.commands.ExecuteJavaCucumberCommand
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult
import org.junit.runner.Description
import org.junit.Ignore;

import spock.lang.Specification


class CucumberIsSuccessTest extends Specification {

    URLClassLoader urlClassLoader
    ExecuteJavaCucumberCommand cmdExe = new ExecuteJavaCucumberCommand()
    def listeFeature
    CucumberResult result
    CucumberIsSucess cucumberIsSucess = new CucumberIsSucess()
    
    def "Checking IsSuccess assertion"(){
		
        setup : 
        setUrlClassLoader()
     
        when :
        //  1 scenario. All steps are defined. 3 datasets. All dataSets in sucess
        execute("squash/T1_is_it_friday_3steps_3datasets.feature")
     
        then :               
        // OK Cucumber/JUNIT
        assert (result.getResult().wasSuccessful() == true)  
        //no AssertionFailedException 
        cucumberIsSucess.test()
              
        when :
        //  1 scenario. All steps are defined. 3 datasets. All dataSets in sucess
        execute("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature")
      
        then :               
        // KO Cucumber/JUNIT
        assert (result.getResult().wasSuccessful() == false)  
        //TF generates AssertionFailedException 
        try {
            cucumberIsSucess.test()
            assert( 1 == 2 )
        } catch(AssertionFailedException ex) {
            // if not, not catched ...
            assert ( (ex.getClass().toString()).toLowerCase().contains(
                            "org.squashtest.ta.framework.exception.AssertionFailedException".toLowerCase()))
            String msg = ex.getMessage().toLowerCase()
            assert (msg.contains ("expected:<[TGIF]> but was:<[Nope]>".toLowerCase()))
            assert (msg.contains("T2: Is it Friday Yet?".toLowerCase())) //feature Name
        }

    }

    def "setUrlClassLoader" () {
        URL[] bytesURL = this.getClass().getClassLoader().getResource("");
        this.urlClassLoader = new URLClassLoader(bytesURL, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(this.urlClassLoader);       
    }
    
    def "execute" (String script) {
        listeFeature = [script]
        CucumberJavaTest test = new CucumberJavaTest(urlClassLoader, listeFeature)
        cmdExe.setResource(test)
        result = cmdExe.apply()
        cucumberIsSucess.setActualResult(result)
    } 
}

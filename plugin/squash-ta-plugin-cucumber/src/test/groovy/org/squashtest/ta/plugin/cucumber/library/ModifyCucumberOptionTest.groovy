/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.library

import cucumber.api.CucumberOptions
import spock.lang.Specification

class ModifyCucumberOptionTest extends Specification {

    def "modify cucumberOptions of TF runner"() {
		
        setup : 
        URL[] bytesURL = this.getClass().getClassLoader().getResource("")
        URLClassLoader urlClassLoader = new URLClassLoader(bytesURL, Thread.currentThread().getContextClassLoader())
        Thread.currentThread().setContextClassLoader(urlClassLoader)
                      
        Class runner =  urlClassLoader.loadClass("ta_runner.RunCucumberProjectTest")
        CucumberOptions cucumberOptions = (CucumberOptions) runner.getAnnotation(CucumberOptions.class)
        String [] tabFeatures = ["myfolder/test.feature"]
        String plugin1 = "json:target/squashTF/reportCucumber/" + tabFeatures[0] + "/cucumber.json"
        String[] tabPlugins = new String[3]
        tabPlugins[0] = "pretty"
        tabPlugins[1] = plugin1
        tabPlugins[2] = "json:target/reportCucumber/cucumber.json" // pour traitement par TA
        
        when :
        CucumberUtils.modifyCucumberOptions(cucumberOptions, "features", tabFeatures)
        CucumberUtils.modifyCucumberOptions(cucumberOptions, "plugin", tabPlugins)
        CucumberUtils.modifyCucumberOptions(cucumberOptions, "dryRun", false)
        cucumberOptions = (CucumberOptions) runner.getAnnotation(CucumberOptions.class)
                          
        then :                    
        assert(!cucumberOptions.dryRun())
        assert(cucumberOptions.strict())
        assert (cucumberOptions.features() == tabFeatures)                       
        assert (cucumberOptions.glue() == ["classpath:"])
        assert (cucumberOptions.plugin()[0] == "pretty")    
        assert (cucumberOptions.plugin()[1] == plugin1)
        assert (cucumberOptions.plugin()[2] == "json:target/reportCucumber/cucumber.json")

        when :
        tabFeatures = ["anotherFolder/subfolder/anotherTest.feature"]
        plugin1 = "json:target/squashTF/reportCucumber/" + tabFeatures[0] + "/cucumber.json"
        tabPlugins[1] = plugin1
        CucumberUtils.modifyCucumberOptions(cucumberOptions, "features", tabFeatures)
        CucumberUtils.modifyCucumberOptions(cucumberOptions, "dryRun", true)
        CucumberUtils.modifyCucumberOptions(cucumberOptions, "plugin", tabPlugins)
        cucumberOptions = (CucumberOptions) runner.getAnnotation(CucumberOptions.class)
                     
        then :                                       
        assert(cucumberOptions.dryRun())
        assert(cucumberOptions.strict())
        assert (cucumberOptions.features() == tabFeatures) 
        assert (cucumberOptions.glue() == ["classpath:"])
        assert (cucumberOptions.plugin()[0] == "pretty")
        assert (cucumberOptions.plugin()[1] == plugin1)
        assert (cucumberOptions.plugin()[2] == "json:target/reportCucumber/cucumber.json")
    }
}

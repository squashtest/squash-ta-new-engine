/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
import javassist.ClassPool
import javassist.CtMethod
import org.squashtest.ta.plugin.cucumber.assertions.CucumberIsRunable
import spock.lang.Specification
import java.lang.reflect.Method;

class CucumberIsRunnableOptionsTest extends Specification {
    
    CucumberIsRunable cucumberIsRunable = new CucumberIsRunable()
    ClassPool pool
    CtMethod gherkinImplementationMethode
      
    def "Checking TF framework gherkin Dryrun Options" () {
        
        setup:
        pool = ClassPool.getDefault();
        Method retrieveBinaryMethod = CucumberIsRunable.class.getDeclaredMethod("retrieveBinaryMethod", 
            String.class, String.class, ClassPool.class)
        retrieveBinaryMethod.setAccessible(true)
        Method lookForCucumberPendingException = CucumberIsRunable.class.getDeclaredMethod("lookForCucumberPendingException", 
            CtMethod.class)
	lookForCucumberPendingException.setAccessible(true)	
                            
        when: 
        //non existing method, must not be found
        boolean notFound = false
        try {
            gherkinImplementationMethode = (CtMethod) retrieveBinaryMethod.invoke(cucumberIsRunable,"mypackage.IDONTEXIST","no_matter" , pool);
            if (gherkinImplementationMethode == null) 
            {
                notFound = true
            }
        } catch (Exception e) {
            notFound = true
        }
        then:
        assert (notFound == true)
                
        when: 
        //case existing nor empty nor PendingException step  
        try {
            gherkinImplementationMethode = (CtMethod) retrieveBinaryMethod.invoke(cucumberIsRunable,"mypackage.StepdefsIsItFriday","i_ask_whether_is_s_Friday_yet" , pool);
        } catch (Exception e) {
            assert("Exception thrown in'CucumberIsRunnableOptionsTest', " 
                + " TEST  can't be executed for method 'mypackage.StepdefsIsItFriday.i_ask_whether_is_s_Friday_yet'. \n" + e.printStackTrace() == "")
        }
        then:
        assert (((boolean) lookForCucumberPendingException.invoke(cucumberIsRunable,gherkinImplementationMethode)) == false)

                            
        when: 
        //case empty method (T7)
        try {
            gherkinImplementationMethode = (CtMethod) retrieveBinaryMethod.invoke(cucumberIsRunable,"mypackage.subfolder.StepDefSubFolder","divide_should_be" , pool);
        } catch (Exception e) {
            assert("Exception thrown in'CucumberIsRunnableOptionsTest', " 
                + " TEST  can't be executed for T7 feature case empty method \n" + e.printStackTrace() == "")
        }
        then:
        assert (gherkinImplementationMethode.isEmpty())
        assert (((boolean) lookForCucumberPendingException.invoke(cucumberIsRunable,gherkinImplementationMethode)) == false)
                
        when: 
        //hrow new cucumber.api.PendingException (T7)
        try {
            gherkinImplementationMethode = (CtMethod) retrieveBinaryMethod.invoke(cucumberIsRunable,"mypackage.subfolder.StepDefSubFolder","complexe_operation_gives" , pool);
        } catch (Exception e) {
            assert("Exception thrown in'CucumberIsRunnableOptionsTest', " 
                + " TEST  can't be executed for T7 feature case cucumber.api.PendingException. \n" + e.printStackTrace() == "")
        }
        then:
        assert (!gherkinImplementationMethode.isEmpty())
        assert (((boolean) lookForCucumberPendingException.invoke(cucumberIsRunable,gherkinImplementationMethode)) == true)
    }
}

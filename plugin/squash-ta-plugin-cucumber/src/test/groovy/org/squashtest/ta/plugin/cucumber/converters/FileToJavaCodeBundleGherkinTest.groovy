/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.converters


import org.squashtest.ta.framework.components.FileResource
import org.apache.commons.io.FileUtils
import org.junit.Ignore
import org.squashtest.ta.plugin.cucumber.resources.JavaCodeBundleGherkin
import spock.lang.Specification


class FileToJavaCodeBundleGherkinTest extends Specification {

    def "checking embedded components deployed on bundle"(){
		
        setup : 
        String userDir = System.getProperty("user.dir");
        File src = new File(userDir,"src/test/resources/src-classes/gherkin-project")
        File target = new File(userDir,"src/test/resources/src-classes/todelete")
        if (target.exists() && target.isDirectory()) {FileUtils.forceDelete(target)}
             
        FileUtils.copyDirectory(src,target)
                  
        FileToJavaCodeBundleGherkin converter = new FileToJavaCodeBundleGherkin()
                       
        FileResource sourceTree = new FileResource(target)
        JavaCodeBundleGherkin bundle = converter.convert(sourceTree)
        File base = bundle.getFileBundleBase();
                      
        when :
        File tfFile = new File(base, "ta_feature/checkUnicityStepsImpl.feature");
        then :                                       
        assert (tfFile.exists())
        assert (tfFile.isFile())
        when :
        tfFile = new File(base, "ta_runner/RunCucumberProjectTest.class");
        then :                                       
        assert (tfFile.exists())
        assert (tfFile.isFile())
        when :
        tfFile = new File(base, "ta_stepsdef/TAforCheckUnicityStepsImpl.class");
        then :                                       
        assert (tfFile.exists())
        assert (tfFile.isFile())
        when :
        tfFile = new File(base, "data/IsItFriday.class");
        then :                                       
        assert (tfFile.exists())
        assert (tfFile.isFile())   
        when :
        tfFile = new File(base, "mypackage");
        then :                                       
        assert (tfFile.exists())
        assert (tfFile.isDirectory())   //no flat
        when :
        tfFile = new File(base, "mypackage/StepdefsIsItFriday.class");
        then :                                       
        assert (tfFile.exists())
        assert (tfFile.isFile()) 
        cleanup :
        if (target.exists() && target.isDirectory()) {FileUtils.forceDelete(target)}
    }
}

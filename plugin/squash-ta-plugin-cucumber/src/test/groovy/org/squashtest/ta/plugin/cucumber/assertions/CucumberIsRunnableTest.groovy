/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.cucumber.assertions

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import groovy.mock.interceptor.MockFor
import org.squashtest.ta.framework.exception.AssertionFailedException
import org.squashtest.ta.plugin.cucumber.commands.ExecuteDryrunCucumberCommand
import org.squashtest.ta.plugin.cucumber.library.CucumberExecutor
import org.squashtest.ta.plugin.cucumber.objects.FeatureEntity
import org.squashtest.ta.plugin.cucumber.resources.CucumberJavaTest
import org.squashtest.ta.plugin.cucumber.resources.CucumberResult
import org.squashtest.ta.plugin.cucumber.resources.JavaCodeBundleGherkin
import org.junit.runner.Description
import org.junit.runner.notification.Failure
import org.junit.runner.Result
import org.junit.Ignore;

import spock.lang.Specification


class CucumberIsRunnableTest extends Specification {

    URLClassLoader urlClassLoader
    ExecuteDryrunCucumberCommand cmdExe = new ExecuteDryrunCucumberCommand()
    def listeFeature
    CucumberResult result
    CucumberIsRunable cucumberIsRunable = new CucumberIsRunable()
    JavaCodeBundleGherkin bundle
    Result junitRes
    List<Failure> failures

   
    def "Checking IsRunnable assertion"(){
		
        setup : 
        setUrlClassLoader()
        setBundle()
       
        when :
        //  1 scenario. All steps are defined. 3 datasets. All dataSets in sucess
        go("squash/T1_is_it_friday_3steps_3datasets.feature", true)  
        then :               
        // OK Cucumber/JUNIT
        assert (result.getResult().wasSuccessful() == true)  
        //no AssertionFailedException 
        cucumberIsRunable.test()
              
        when :
        //  1 scenario. All steps are defined. KO only if feature is executing
        go("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature", true)
        junitRes = result.getResult() // Junit        
        then :
        assert (junitRes.wasSuccessful() == true)  
        cucumberIsRunable.test()
         
        when :
        //  1 scenario. 3 datasets. 1 setp not implemented
        go("squash/T3_is_it_friday_3steps_3datasets_NoMatchingMethodStepWhen.feature", true)
        junitRes = result.getResult()
        failures = junitRes.getFailures()
        then :               
        assert (junitRes.wasSuccessful() == false)  
        assert (failures.get(0).getDescription().toString().contains("Today is or is not Friday(T3: Is it Friday Yet - 1Step no define?)"))
        //3 datasets for same step undefined
        assert (failures.get(0).getException().getClass().getName().contains("cucumber.runtime.junit.UndefinedThrowable")) 
        assert (failures.get(1).getException().getClass().getName().contains("cucumber.runtime.junit.UndefinedThrowable"))
        assert (failures.get(2).getException().getClass().getName().contains("cucumber.runtime.junit.UndefinedThrowable"))
        try {
            cucumberIsRunable.test()
            assert( 1 == 2 )
        } catch(Exception ex) {
            assert ( (ex.getClass().toString()).toLowerCase().contains(
                            "org.squashtest.ta.framework.exception.AssertionFailedException".toLowerCase()))
            String msg = ex.getMessage().toLowerCase()
            assert (msg.contains ("steps not implemented on line".toLowerCase()))
        }
    }
	
    //   *********************************************************
    def "setUrlClassLoader" () {
        File resourcesDirectory = new File("src/test/resources")
        URL[] bytesURL = new URL[1];
        bytesURL[0] = resourcesDirectory.toURI().toURL();
        this.urlClassLoader = new URLClassLoader(bytesURL, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(this.urlClassLoader);       
    }
    
    def "go" (String script, boolean withDryrunOptions) {
        listeFeature = [script]
        CucumberJavaTest test = new CucumberJavaTest(bundle, listeFeature)
        test.setAdditionalDryRunCheck(withDryrunOptions) 
        cmdExe.setResource(test)
        result = cmdExe.apply()
        cucumberIsRunable.setActualResult(result)
    } 
    
    def "setBundle" () {
        File file = new File("src/test/resources");
        Set<String> bof = new HashSet<String>()         
        bof.add("data.IsItFriday");
        bof.add("mypackage.MyException");
        bof.add("mypackage.StepdefsIsItFriday");
        bof.add("mypackage.StepsWithRunTimeException");
        bof.add("mypackage.subfolder/StepDefSubFolder");
        bof.add("operations.OperationsSteps");
        bof.add("ta_runner.RunCucumberProjectTest");
        bof.add("ta_stepsdef.TAforCheckUnicityStepsImpl");
        bundle =  new JavaCodeBundleGherkin(file, null,bof)
    }
    
}

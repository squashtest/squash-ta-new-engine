/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.converters

import org.squashtest.ta.core.tools.io.SimpleLinesData
import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.exception.BadDataException
import org.squashtest.ta.plugin.local.process.resources.CommandLineResource
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.shell.PosixTestAdapterInjector;
import org.squashtest.ta.local.process.library.shell.PlatformTestAdapter;

import spock.lang.Specification

class FileToShellQueryTest extends Specification {
	FileToShellQuery testee
	File file
	FileResource resource
        
	def setup(){
		testee=new FileToShellQuery()
		file=File.createTempFile("die", ".dead", TempDir.getExecutionTempDir())
		file.delete()
		resource=new FileResource(file)       
	}
        
        def cleanup(){
            new PlatformTestAdapter().reset()
        }
        
	def "Should yell if non-existant file"(){
		when:
			testee.convert(resource)
		then:
			thrown(BadDataException)
	}
	
	def "Should yell if directory"(){
		given:
			file.mkdir()
		when:
			testee.convert(resource)
		then:
			thrown(BadDataException)
	}
	
	def "should create command"(){
		given:
                        new PosixTestAdapterInjector().apply()
                and:
			SimpleLinesData command=new SimpleLinesData(["ls -la /home/tester"])
			command.write(file)
		when:
			CommandLineResource resource=testee.convert(resource)
		then:
                        def expected="ls -la /home/tester;".equals(resource.getLocalCommand())
	}
}

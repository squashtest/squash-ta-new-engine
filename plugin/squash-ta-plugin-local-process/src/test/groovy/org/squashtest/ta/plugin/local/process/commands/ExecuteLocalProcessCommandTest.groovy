/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.commands

import org.squashtest.ta.core.tools.io.SimpleLinesData
import org.squashtest.ta.local.process.library.process.LocalProcessClient;
import org.squashtest.ta.local.process.library.shell.NeutralTestExecutionPlatformAdapter;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.local.process.command.ExecuteLocalProcessCommand
import org.squashtest.ta.plugin.local.process.resources.CommandLineResource
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource
import org.squashtest.ta.framework.tools.TempDir;

import spock.lang.Specification

class ExecuteLocalProcessCommandTest extends Specification {
	ExecuteLocalProcessCommand testee
	File cf
	
	def setup(){
                new NeutralTestExecutionPlatformAdapter().apply();
		testee=new ExecuteLocalProcessCommand()
		cf=File.createTempFile("cmd", ".txt", TempDir.getExecutionTempDir())
		new SimpleLinesData(["ping"]).write(cf)
	}

        def cleanup(){
            new NeutralTestExecutionPlatformAdapter().reset();
        }
    
	def "should execute said command"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		when:
			def result = testee.apply()
		then:
			result instanceof ShellResultResource
		cleanup:
			cf.delete()
	}
	
	def "should transmit 0 exitValue"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			LocalProcessClient mockClient = Mock(LocalProcessClient)
			testee.setClient(mockClient)
			mockClient.runLocalProcessCommand(_,_,_,_) >> new ShellResult(0, "", "","ls -la /home/tester")
		when:
			def result=testee.apply()
		then:
			result.getResult().getExitValue() == 0
		cleanup:
			cf.delete()
	}
	
	def "should transmit non-zero exitValue"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			LocalProcessClient mockClient = Mock(LocalProcessClient)
			testee.setClient(mockClient)
			mockClient.runLocalProcessCommand(_,_,_,_)>> new ShellResult(-5, "", "","ls -la /home/tester")
		when:
			ShellResultResource result=testee.apply()
		then:
			result.getResult().getExitValue()==-5
		cleanup:
			cf.delete()
	}
	
	def "should transmit stdout"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			LocalProcessClient mockClient = Mock(LocalProcessClient)
			testee.setClient(mockClient)
			mockClient.runLocalProcessCommand(_,_,_,_)>> new ShellResult(0, "gotcha!", "","ls -la /home/tester")
		when:
			ShellResultResource result=testee.apply()
		then:
			"gotcha!".equals(result.getResult().getStdout())
		cleanup:
			cf.delete()
	}
	
	def "should transmit stderr"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
		and:
			LocalProcessClient mockClient = Mock(LocalProcessClient)
			testee.setClient(mockClient)
			mockClient.runLocalProcessCommand(_,_,_,_)>> new ShellResult(0, "", "Come get some!","ls -la /home/tester")
		when:
			ShellResultResource result=testee.apply()
		then:
			"Come get some!".equals(result.getResult().getStderr())
		cleanup:
			cf.delete()
	}
	
	def "should transmit timeout spec"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,1500)
			testee.setResource(cl)
			LocalProcessClient mockClient = Mock(LocalProcessClient)
			testee.setClient(mockClient)
		when:
			ShellResultResource result=testee.apply()
		then:
			1 * mockClient.runLocalProcessCommand(cl.getLocalCommand(),_,1500,_)>> new ShellResult(0, "", "Come get some!","ls -la /home/tester")
		cleanup:
			cf.delete()
	}
	
	def "should build default timeout"(){
		given:
			CommandLineResource cl=new CommandLineResource(cf,null)
			testee.setResource(cl)
			LocalProcessClient mockClient = Mock(LocalProcessClient)
			testee.setClient(mockClient)
		when:
			ShellResultResource result=testee.apply()
		then:
			1 * mockClient.runLocalProcessCommand(cl.getLocalCommand(),_,5000,_)>> new ShellResult(0, "", "Come get some!","ls -la /home/tester")
		cleanup:
			cf.delete()
	}
}

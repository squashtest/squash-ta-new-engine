/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.resources

import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.tools.TempDir;


import spock.lang.Specification;
import org.squashtest.ta.local.process.library.shell.PosixTestAdapterInjector;
import org.squashtest.ta.local.process.library.shell.WindowsTestAdapterInjector;
import org.squashtest.ta.local.process.library.shell.PlatformTestAdapter;

class CommandLineResourceTest extends Specification {
	CommandLineResource testee
	File file
	
	def setup(){
		file=File.createTempFile("cmd", ".txt", TempDir.getExecutionTempDir())
	}
	
	def "Must be able to read single line - POSIX"(){
		given:
                        new PosixTestAdapterInjector().apply()
                and:
			BinaryData data=new BinaryData(getClass().getResource("shellCommand/POSIX/shellCommandTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester;".equals(content)
	}
        
        def "Must be able to read single line - Windows"(){
		given:
                        new WindowsTestAdapterInjector().apply()
                and:
			BinaryData data=new BinaryData(getClass().getResource("shellCommand/windows/shellCommandTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
	
	def "Must be able to read first non-comment line - POSIX"(){
		given:
                        new PosixTestAdapterInjector().apply()
                and:
			BinaryData data=new BinaryData(getClass().getResource("shellCommand/POSIX/shellCommandCommentTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
        
        def "Must be able to read first non-comment line - Windows"(){
		given:
                        new WindowsTestAdapterInjector().apply()
                and:
			BinaryData data=new BinaryData(getClass().getResource("shellCommand/windows/shellCommandCommentTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
	
	def "Must be able to ignore comments in the same line - POSIX"(){
		given:
                        new PosixTestAdapterInjector().apply()
                and:
			BinaryData data=new BinaryData(getClass().getResource("shellCommand/POSIX/shellCommandSameLineComment"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
        
        def "Windows doesn't ignore comments in the same line, so we shouldn't - Windows"(){
		given:
                        new WindowsTestAdapterInjector().apply()
                and:
			BinaryData data=new BinaryData(getClass().getResource("shellCommand/windows/shellCommandSameLineComment"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester REM comment which must NOT be ignored".equals(content)
	}
	
	def "Read multiline shell - POSIX"(){
		given:
		        new PosixTestAdapterInjector().apply()
                and:
                        BinaryData data=new BinaryData(getClass().getResource("shellCommand/POSIX/shellCommandMultilineTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester;echo \"this is second line and should be after semicolumn\"".equals(content)
	}
        
	def "Read multiline shell - Windows"(){
		given:
		        new WindowsTestAdapterInjector().apply()
                and:
                        BinaryData data=new BinaryData(getClass().getResource("shellCommand/windows/shellCommandMultilineTest"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester && echo \"this is second line and should be after statement separator\"".equals(content)
	}
        
	def "Ignore blank lines - POSIX"(){
		given:
		        new PosixTestAdapterInjector().apply()
                and:
                        BinaryData data=new BinaryData(getClass().getResource("shellCommand/POSIX/shellCommandBlankLines"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
	def "Ignore blank lines - Windows"(){
		given:
		        new WindowsTestAdapterInjector().apply()
                and:
                        BinaryData data=new BinaryData(getClass().getResource("shellCommand/windows/shellCommandBlankLines"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester".equals(content)
	}
        
	def "Trim the commands - POSIX"(){
		given:
		        new PosixTestAdapterInjector().apply()
                and:
                        BinaryData data=new BinaryData(getClass().getResource("shellCommand/POSIX/shellCommandTrim"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester;echo \"this is second line and should be after semicolumn\"".equals(content)
	}	
	def "Trim the commands - Windows"(){
		given:
		        new WindowsTestAdapterInjector().apply()
                and:
                        BinaryData data=new BinaryData(getClass().getResource("shellCommand/windows/shellCommandTrim"))
			data.write(file)
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			"ls -la /home/tester && echo \"this is second line and should be after semicolumn\"".equals(content)
	}
        
	def "should hang on empty"(){
		given:
			testee=new CommandLineResource(file,null)
		when:
			String content=testee.getLocalCommand()
		then:
			thrown(BadDataException)
	}
	
	def cleanup(){
                new PlatformTestAdapter().reset()
		file.delete()
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.library.shell

import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.local.process.library.shell.ExitCodeCheck

import spock.lang.Specification;

class ExitCodeCheckTest extends Specification{
	ExitCodeCheck testee
	
	def "default should accept result with 0 exit code"(){
		given:
			testee=new ExitCodeCheck()
		and:
			ShellResult actual=Mock()
			actual.getExitValue()>>0
		when:
			boolean success=testee.check(actual)
		then:
			success==true
	}
	
	def "default should reject result with positive non-zero exit code"(){
		given:
			testee=new ExitCodeCheck()
		and:
			ShellResult actual=Mock()
			actual.getExitValue()>>1
		when:
			boolean success=testee.check(actual)
		then:
			success==false
	}
	
	def "default should reject result with negative non-zero exit code"(){
		given:
			testee=new ExitCodeCheck()
		and:
			ShellResult actual=Mock()
			actual.getExitValue()>>-1
		when:
			boolean success=testee.check(actual)
		then:
			success==false
	}
	
	def "default should accept result with expected exit code"(){
		given:
			testee=new ExitCodeCheck(1)
		and:
			ShellResult actual=Mock()
			actual.getExitValue()>>1
		when:
			boolean success=testee.check(actual)
		then:
			success==true
	}
	
	def "default should reject result with unexpected exit code"(){
		given:
			testee=new ExitCodeCheck(-3)
		and:
			ShellResult actual=Mock()
			actual.getExitValue()>>1
		when:
			boolean success=testee.check(actual)
		then:
			success==false
	}
	
	def "default should reject result with unexpected success code"(){
		given:
			testee=new ExitCodeCheck(-3)
		and:
			ShellResult actual=Mock()
			actual.getExitValue()>>0
		when:
			boolean success=testee.check(actual)
		then:
			success==false
	}
}

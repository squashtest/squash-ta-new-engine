/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.targets;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.core.templates.FileBasedCreator;
import org.squashtest.ta.core.tools.PropertiesBasedCreatorHelper;
import org.squashtest.ta.framework.annotations.TATarget;
import org.squashtest.ta.framework.annotations.TATargetCreator;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.plugin.commons.library.ShebangCheck;

/**
 * Factory for {@link LocalStorageDirectoryTarget}. For a properties file to be eligible it HAS to have a correct Shebang.
 * This factory uses the {@link TATarget} annotation value "localdisk" of the {@link LocalStorageDirectoryTarget} as Shebang targeted value.
 * 
 * A valid configuration properties file should look like : 
 * <pre><code>
 * #!localdisk
 * squashtest.ta.localdisk.base=/a/path/to/a/writable/directory/on/local/filesystem
 * </code></pre>
 * @author fgautier shamelessly reaping work done by edegenetais. Implemented slight modification for the Shebang to be the name of  LocalStorageDirectoryTarget target type. 
 * @author edegenetais the mind behind it.
 */
@TATargetCreator("localdisk.creator")
public class LocalStorageDirectoryTargetCreator extends FileBasedCreator implements TargetCreator<LocalStorageDirectoryTarget>{

    /**
     * Well a logger used to log class related messages...
     * It has a funny name because the class FileBasedCreator from which this class inherits
     * defines a ***PROTECTED and STATIC***  Logger called LOGGER.
     */
    private static final Logger CLASS_LOGGER = LoggerFactory.getLogger(LocalStorageDirectoryTargetCreator.class);
    
    /**
     * This Shebang checker uses the value of the  {@link TATarget} annotation "localdisk" as target value.
     */
    private static final ShebangCheck TARGET_TYPE_CHECK = new ShebangCheck(LocalStorageDirectoryTarget.class);

    /**
     * The regex used to test for eligibilty of preperties files as a LocalStorgageTaget configuration. 
     */
    private static final String SQUASHTEST_TA_LOCALDISK_KEY_REGEX = "squashtest\\.ta\\.localdisk\\..*";
    /**
     * The key used to test the eligibilty of a properties files as a LocalStorgageTaget configuration. 
     * 
     * Please note the "package" visiblity in order to define only once this key and to be 
     * able to share it with the target for it to do its dirty business.
     */
    static final String LOCALDISK_BASE_KEY = "squashtest.ta.localdisk.base";

    /**
     * The optionnal key used to set the behavior of the target concerning sub directory creation. 
     * 
     * Please note the "package" visiblity in order to define only once this key and to be 
     * able to share it with the target for it to do its dirty business.
     */
    static final String DIR_CREATION_BEHAVIOR_KEY ="squashtest.ta.localdisk.create.dir";
    
    /**
     * Provides utlity methods to deal with properties files. 
     */
    private static final PropertiesBasedCreatorHelper PROPERTIES_HELPER = new PropertiesBasedCreatorHelper();

    static {
        /* Proper initialisation of the helper */
        PROPERTIES_HELPER.setKeysRegExp(SQUASHTEST_TA_LOCALDISK_KEY_REGEX);
    }
    
    /**
     * used to check wheter the value of the property is empty or not.
     */
    private static final String EMPTY_STR = "";    
    @Override
    public boolean canInstantiate(URL targetDefinitionUrl) {
        CLASS_LOGGER.debug("Testing eligibilty of {} as a local repository configuration file.", targetDefinitionUrl);
        boolean isLocalRepository=false;
        try {
            //Testing if the file has correct shebang #!localdisk
            if (TARGET_TYPE_CHECK.hasShebang(targetDefinitionUrl)) {
                Properties prop = PROPERTIES_HELPER.getEffectiveProperties(getFileOrFail(targetDefinitionUrl));
                if(prop.containsKey(LOCALDISK_BASE_KEY) && !EMPTY_STR.equals(prop.get(LOCALDISK_BASE_KEY).toString())) {
                    isLocalRepository=true;
                } else {
                    CLASS_LOGGER.warn("Target definition {} is marked as localdisk target definition but lacks the '{}' property, thus isn't usable.",targetDefinitionUrl,LOCALDISK_BASE_KEY);
                }
            } else {
                CLASS_LOGGER.debug("Target definition {} lacks shebang #!{}, thus isn't eligible as a local repository configuration file.", targetDefinitionUrl, TARGET_TYPE_CHECK.getShebang());
            }
        } catch (IOException | URISyntaxException e) {
            CLASS_LOGGER.warn("Cannot access transmitted tager definition URL.");
            throw new BrokenTestException("Cannot access transmitted tager definition URL.",e);
        }
        return isLocalRepository;
    }


    @Override
    public LocalStorageDirectoryTarget createTarget(URL configUrl) {
        CLASS_LOGGER.debug("Creating target from configuration URL {}",configUrl);
        try {
            File configFile = getFileOrFail(configUrl);
            Properties cfg = PROPERTIES_HELPER.getEffectiveProperties(configFile);
            return new LocalStorageDirectoryTarget(cfg);
            
        } catch (URISyntaxException e) {
            final String message = "Local output directory target cannot be created. Definition URL " + configUrl.toExternalForm() + " was not valid URI";
            CLASS_LOGGER.warn(message);
            throw new BrokenTestException(message,e);
        } catch (IOException e) {
            final String message = "I/O error. Local output directory target cannot be created.";
            CLASS_LOGGER.warn(message);
            throw new BrokenTestException(message,e);
        }

    }
    
}

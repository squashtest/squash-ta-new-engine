/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.library.shell;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * Option file reader for shell components.
 * 
 * @author edegenetais
 * 
 */
public class ShellOptionReader extends OptionsReader {

	private static final String STREAMLENGTH_KEY = "streamlength";
	private static final String TIMEOUT_KEY = "timeout";
	private static final Set<String> SUPPORTED_OPTIONS = new HashSet<String>(Arrays.asList(new String[] { TIMEOUT_KEY,
			STREAMLENGTH_KEY }));

	private final Logger logger;

	private Integer streamlength;
	private Integer timeout;

	/**
	 * Constructor.
	 * 
	 * @param callerName
	 *            name of the caller for logging.
	 */
	public ShellOptionReader(String callerName) {
		logger = LoggerFactory.getLogger(ShellOptionReader.class.getName() + "." + callerName);
	}

	@Override
	protected Set<String> supportedKeys(Map<String, String> source) {
		HashSet<String> options = new HashSet<String>(SUPPORTED_OPTIONS);
		options.retainAll(source.keySet());
		return options;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

	/**
	 * Add configuration to the data of this configuration object.
	 * 
	 * @param configuration
	 */
	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Resource<?> element : configuration) {
			//if (timeout == null) {
				if (element instanceof FileResource) {
					addFileResource(element);
				} else {
					logger.warn("Ignoring unsupported non FileResource configuration element.");
				}
			/*} else {
				logger.warn("Ignoring configuration excess resource as time out is already configured at " + timeout);
			}*/
		}
	}

	private void addFileResource(Resource<?> element) {
		FileResource conf = (FileResource) element;
		File confFile = conf.getFile();
		try {
			if (isOptions(confFile)) {
				Map<String, String> options = getFilteredOptions(confFile);
				
				String timeoutString = options.get(TIMEOUT_KEY);
				if (timeoutString != null) {
					timeout = Integer.parseInt(timeoutString);
					if (timeout.compareTo(0) <= 0) {
						throw new IllegalConfigurationException("Negative or null value, " + timeoutString
								+ ", is not a valid timeout specification");
					}
				}
				
				String streamlengthString = options.get(STREAMLENGTH_KEY);
				if (streamlengthString != null) {		
					// "full" sends all stream
					if ("FULL".equalsIgnoreCase(streamlengthString)) {						
						streamlength = Integer.parseInt("-1");
					} else {
						streamlength = Integer.parseInt(streamlengthString);
						if (streamlength.compareTo(0) <= 0) {
							throw new IllegalConfigurationException("Negative or null value, " + streamlengthString
									+ ", is not a valid stream length specification");
						}
					}
				}
			} else {
				logger.warn("Ignoring unsupported configuration file "
						+ (confFile == null ? "null" : confFile.getAbsolutePath()));
			}
		} catch (IllegalArgumentException e) {
			throw new IllegalConfigurationException("Illegal file->query.shell converter configuration element "
					+ confFile, e);
		} catch (IOException e) {
			throw new InstructionRuntimeException("Failed to access file->query.shell converter configuration element "
					+ confFile, e);
		}
	}

	/**
	 * @return the configured timeout.
	 */
	public Integer getTimeout() {
		return timeout;
	}

	/**
	 * @return the configured streamlength.
	 */
	public Integer getStreamlength() {
		return streamlength;
	} 
}

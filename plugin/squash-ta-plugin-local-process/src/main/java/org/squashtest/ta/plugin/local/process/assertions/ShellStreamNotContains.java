/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.assertions;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.plugin.local.process.library.shell.BinaryShellReport;
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource;

/**
 * This assertions is meant to prove that a given pattern did NOT accur in one
 * of the output streams (stdin or stdout) of a shell process.
 * If the given pattern (regex) is found, the assertion fails.
 * @author edegenetais
 * 
 */
@TABinaryAssertion("not.contain")
public class ShellStreamNotContains extends AbstractShellStreamSearcherAssertion implements BinaryAssertion<ShellResultResource, FileResource>{
	private static final Logger LOGGER = LoggerFactory.getLogger(ShellStreamNotContains.class);

	public ShellStreamNotContains() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void test() throws BinaryAssertionFailedException{
		String streamData=loadConfiguration();
		
		//in either case, we build the ShellReport for the record
		BinaryShellReport shellReport = buildBinaryReport(streamData);
		
		//load the pattern
		Pattern pattern=compilePattern();

		if(pattern.matcher(streamData).find()){
			reportAssertionFailure(shellReport, pattern.pattern());
		}else{
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Found '"+pattern.pattern()+"' in "+getTargetStream()+" of "+getActual().getCommand());
			}
		}
	}

	@Override
	protected String getFailureMessage(String regex) {
		return "Failure pattern '"+regex+"' was found.";
	}
}

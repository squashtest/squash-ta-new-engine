/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.converters;

import java.io.File;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.plugin.local.process.library.shell.ShellOptionReader;
import org.squashtest.ta.plugin.local.process.resources.CommandLineResource;

/**
 * This is the converter to create shell command line resources from file resources.
 * Implemented checks: the file resource is a readable regular file.
 * @author edegenetais
 *
 */
@TAResourceConverter("query")
public class FileToShellQuery implements ResourceConverter<FileResource, CommandLineResource>{

	private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineResource.class);
	private ShellOptionReader shellOptions = new ShellOptionReader("query_shell_converter");
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		shellOptions.addConfiguration(configuration);
	}

	@Override
	public CommandLineResource convert(FileResource resource) {
		File sourceFile=resource.getFile();
		String fileAsString = sourceFile==null?"null":sourceFile.getAbsolutePath();
		if(sourceFile!=null && sourceFile.canRead()){
			if(sourceFile.isFile()){
				Integer timeout = shellOptions.getTimeout();
				Integer streamlength = shellOptions.getStreamlength();
					
				LOGGER.debug(
						"Created commandline from file "
								+ sourceFile.getAbsolutePath()
								+ " with timeout "
								+ (timeout == null ? "unspecified" : timeout
										.toString())
								+ " with stream length "
								+ (streamlength == null ? "unspecified" : streamlength.toString()));
				
				return new CommandLineResource(sourceFile,timeout,streamlength);
			}else{
				throw new BadDataException(fileAsString+" isn't a regular file.");
			}
		}else{
			throw new BadDataException(fileAsString+" is not readable.");
		}
	}

	@Override
	public void cleanUp() {
		//noop: is a 
	}

}

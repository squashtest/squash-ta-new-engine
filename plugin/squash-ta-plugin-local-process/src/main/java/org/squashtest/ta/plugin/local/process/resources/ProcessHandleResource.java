/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.resources;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.local.process.library.process.ProcessHandle;

/**
 * This resource type is a mere handle for a process. It only allows process
 * termination after use (typically as part of the 
 * {@link org.squashtest.ta.framework.test.definition.Environment} teardown
 * process.
 * 
 * @author edegenetais
 * 
 */
@TAResource("process")
public class ProcessHandleResource implements Resource<ProcessHandleResource> {
	/** The referenced process. */
	private ProcessHandle process;

	/* noarg constructor for Spring enumeration*/
	public ProcessHandleResource() {}
	
	public ProcessHandleResource(ProcessHandle handle) {
		this.process = handle;
	}

	@Override
	public ProcessHandleResource copy() {
		return new ProcessHandleResource(process);
	}

	@Override
	public void cleanUp() {
		process.killProcess();
	}

	public ProcessHandle getProcessHandle() {
		return process;
	}

}

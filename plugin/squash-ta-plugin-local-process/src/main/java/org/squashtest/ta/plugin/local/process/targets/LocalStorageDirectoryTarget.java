/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.targets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TATarget;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * This target represents a Directory with read and write permissions on a local
 * filesystem. This kind of target has to be used wisely since usually TA-Host ≠
 * SUT ! Don't forget to clean your mess at then end.
 *
 * As stated on the factory creating it, a LocalStorageDirectoryTarget is
 * defined by properties files with a content similar to :
 * <pre><code>
 * #!localdisk
 * squashtest.ta.localdisk.base=/a/path/to/a/writable/directory/on/local/filesystem
 * </code></pre> Please note that the Shebang is the value of the
 * {@link TATarget} annotion of this class.
 *
 * @author fgautier shamelessly reaping work done by edegenetais.
 * @author edegenetais the mind behind it.
 */
@TATarget("localdisk")
public class LocalStorageDirectoryTarget implements Target {

    /**
     * A logger to log class related messages.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalStorageDirectoryTarget.class);

    /**
     * Should contain all the inforamtion about the local directory this targets
     * should point to.
     */
    private Properties configuration;
    /**
     * The local directory this target points to.
     */
    private File base;

    /**
     * Cause of the petential initialisation failure of the target.
     */
    private String cause;

    /**
     * This boolean defines the behavior of this target on wether creating or
     * not sub directories.
     *
     * By default the target won't allow for sub-directories creation;
     */
    private boolean createSubDir = false;

    /**
     * Default contructor for spring enumeration only.
     */
    public LocalStorageDirectoryTarget() {
    }

    /**
     * Constructor using an eligible configuration.
     */
    LocalStorageDirectoryTarget(Properties configuration) {
        this.configuration = configuration;
    }

    @Override
    public boolean init() {
        String baseCandidatePath = this.configuration.getProperty(LocalStorageDirectoryTargetCreator.LOCALDISK_BASE_KEY);
        File baseCandidate = new File(baseCandidatePath);
        boolean isValidBase = true;
        if (!baseCandidate.exists()) {
            cause = baseCandidatePath + " does not exist on local filesystem.";
            isValidBase = false;
        }
        if (isValidBase && !baseCandidate.isDirectory()) {
            cause = baseCandidatePath + " does not point to a directory.";
            isValidBase = false;
        }
        if (isValidBase && !baseCandidate.canWrite()) {
            cause = baseCandidatePath + " is not writable. Please check permissions";
            isValidBase = false;
        }
        if (isValidBase) {
            base = baseCandidate;
        } else {
            /*
             * No failure (exception) here because the target may be unused by the selected tests.
             * We issue a warning, however, for configuration troubleshooting !
             */
            LOGGER.warn("Invalid target : {}", cause);
        }

        String rawDirCreationBehavior = this.configuration.getProperty(LocalStorageDirectoryTargetCreator.DIR_CREATION_BEHAVIOR_KEY);
        if (rawDirCreationBehavior != null) {
            createSubDir = Boolean.parseBoolean(rawDirCreationBehavior);
        }

        return isValidBase;
    }

    @Override
    public void reset() { /*NOPE: We keep those files its the whole point*/ }

    @Override
    public void cleanup() { /*NOPE*/ }

    @Override
    public Properties getConfiguration() {
        // Configuration copy for decoupling. 
        return new Properties(configuration);
    }

    /**
     * Write the source file in base with the specified name, and optionally
     * relative path.
     *
     * @param source the source file.
     * @param destinationPath relative destination path.
     */
    public void write(File source, File destinationPath) {
        this.write(source, destinationPath, this.createSubDir);
    }

    /**
     * Write the source file in base with the specified name, and optionally
     * relative path. If createParentDir is set to true, missing parent 
     * directories are automatically created.
     *
     * @param source the source file.
     * @param destinationPath relative destination path.
     * @param createParentDir Set to true if one wants auto creation of parent directories;
     */
    public void write(File source, File destinationPath, boolean createParentDir) {
        if (base == null) {
            /*
             * Error reporting is postponed to this stage because a broken
             * target should only impact tests that actually use it.
             */
            throw new IllegalConfigurationException(cause);
        }

        if (source.isDirectory()) {
            throw new InstructionRuntimeException("Writing of a tree is not supported, files only.");
        }

        // Trying to avoid sandbox breaches
        File destination = getDestFileDescriptorOrFailIfUnsafe(destinationPath);

        // Creating parent directory if allowed and if need be
        createParentDirectory(destination, createParentDir);

        try (
                InputStream input = new FileInputStream(source);
                OutputStream output = new FileOutputStream(destination);) {
            IOUtils.copy(input, output);
        } catch (IOException e) {
            String message = "Failed to dump file " + source.getAbsolutePath() + " to destination " + destination.getAbsolutePath()+". ";
            if(destination.getParentFile() != null && !destination.getParentFile().exists()) {
                message+="Parent directory "+destination.getParentFile()+" does not exist. You may wan't to create it or use auto creation mecanism of either taget or command.";
            }
            throw new InstructionRuntimeException(message, e);
        }
    }
    
    /**
     * Return Base directory of the target.
     *
     * @return a {@link File} pointing to the base directory.
     */
    public File getBase() {
        return base;
    }

    /**
     * This private helper method constructs the full destination file
     * descriptor using destination Path relative to the "base" directory
     * pointed by the this target.
     *
     * In order to avoid sandbox breaches the destination path is sanitized :
     * <ul>
     * <li>First the relative destination path is checked to ensure that it is
     * indeed a relative one.</li>
     * <li>Second using the canonical path of the file "basePath+relativePath"
     * we check that it is indeed a descendant of the base directory.</li>
     * </ul>
     *
     * @param destinationPath the relative path of the output file with respect
     * to the base directory pointed by this target.
     * @return The {@link File} descriptor pointing to the fully sanitized
     * destination path.
     * @throws {@link BadDataException} If one of the above condition is not
     * met, or if any I/O probleme occured while checking the canonical path
     * (permissions).
     */
    private File getDestFileDescriptorOrFailIfUnsafe(File destinationPath) {
        if (destinationPath.isAbsolute()) {
            throw new BadDataException("Destination path " + destinationPath + " is absolute. Destination path must me specified relative to base location.");
        }

        File destination = new File(this.base, destinationPath.getPath());

        try {
            final String canonicalDestinationPath = destination.getCanonicalPath();
            final String baseAbsolutePath = base.getAbsolutePath();
            if (canonicalDestinationPath.startsWith(baseAbsolutePath)) {
                LOGGER.debug("Relative path {} is safe. No sandbox breach detected.", destination);
            } else {
                final String message = "Relative path " + destinationPath + " breaches sandbox. Full constructed absolute path " + canonicalDestinationPath + " is not a descendant of base directory " + baseAbsolutePath + ".";
                LOGGER.warn(message);
                throw new BadDataException(message);
            }
        } catch (IOException e) {
            throw new BadDataException("Failed to get canonicial path description of destination " + destination.getAbsolutePath() + ". Please check for permissions.",e);
        }
        return destination;
    }
    
    /**
     * This private helper method creates file's parent directory(ies) if allowed and if need be.
     * 
     * @param destination : The file for wich parent directory should be created
     * @param createParentDir : Should parent directory(ies) be created ?
     */
    private void createParentDirectory(File destination, boolean createParentDir) {
        // Creating parent directories if need be
        if (createParentDir) {
            File parent = destination.getParentFile();
            if (parent != null && !parent.exists() && !parent.mkdirs()) {
                throw new InstructionRuntimeException("Failed to create parent directory(ies) for file "+destination.getAbsolutePath()+".");
            }
        } else {
            LOGGER.debug("Creation of parent directories is disabled. Some I/O problem may occur if trying to write in a non-existent sub-directory.");
        }
    }
}

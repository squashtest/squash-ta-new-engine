/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.command;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidResource;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.ComponentRepresentation;
import org.squashtest.ta.plugin.local.process.targets.LocalStorageDirectoryTarget;

/**
 * This commands dumps the content of a {@link FileResource} on a local filesystem.  
 * 
 * <b>WARNING</b> : This has to be used wisely since usually TA-Host ≠ SUT ! Don't forget to clean your
 * mess at then end. 
 * 
 * The output relative path is given via a configuration {@link  Resource} (Clause USING), hence it is mandatory.
 * 
 * The  behavior of this command concerning the creation of non existent parent directories is twofold :
 * <ul>
 * <li> Use the {@link LocalStorageDirectoryTarget} behavior. In this case a mendatory {@link SimpleLinesData} formated {@link FileResource} 
 * is used as configuration in order to specify the output file name.
 * 
 * DSL Usage example : 
 * <pre><code>
 * EXECUTE dump ON my.local.directory WITH my.resource.file USING $(outputfilename.txt) AS $()
 * </code></pre> 
 * </li>
 * <li> Override the target settings and specify wether or not directories should be created. In this case a mandatory {@link  Properties} is 
 * used as a configuration in order to specify first the output file name and second the creation behavior.The property file should posse the folowing
 * two key :
 *     <ul>
 *         <li><b>output.relative.path</b> : the relavive path of the output file on the local filsystem</li>
 *         <li><b>create.missing.dir</b> : if set to "true" (case insensitive) the paretn directories will be created, else it won't.</li>
 *     </ul>
 *
 * DSL Usage example : 
 * <pre><code>
 * DEFINE $(output.relative.path=parent/outputfilename.txt\n create.missing.dir=true) AS output.conf.file
 * CONVERT output.conf.file TO properties(structured) AS output.conf.prop
 * EXECUTE dump ON my.local.directory WITH my.resource.file USING $output.conf.prop AS $()
 * </code></pre> 
 * </li>
 * </ul>
 * 
 * Those two mecanisms are in mutual exclusion. The user should choose wich mecanism he wants to configure it's command, since otherwise this could lead to dramatic 
 * incidents. Dramatic incidents are the path to killing penguins ! Dramatic incidents lead to anger. Anger leads to hate. Hate leads to suffering. Be cool with penguins 
 * and avoid the mess. Here we help the user and choose the conservative way, that is we throw an {@link IllegalConfigurationException} if one tries to configure 
 * twice (or more) this command.
 * 
 * @author fgautier shamelessly reaping work done by edegenetais.
 * @author edegenetais the mind behind it.
 */
@TACommand("dump")
public class DumpToDisk implements Command<FileResource, LocalStorageDirectoryTarget>{
    
    /**
     * Well ... a logger used to log class related messages...
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DumpToDisk.class);

    /**
     * If the command is used with a PropertiesResource configuration this key contains
     * the relative path of the output file. In that case this key is mandatory.
     */
    private static final String OUTPUT_PATH_KEY = "output.relative.path";
    /**
     * If the command is used with a PropertiesResource configuration this key points 
     * to the behavior to be adopted with missing parent directories. In that case the
     * key is optional.
     */
    private static final String CREATE_MISSING_DIR_KEY = "create.missing.dir";

    /**
     * Name of the file where the content will be dumped.
     */
    private String fileName;
    /**
     * Resource whose content is to be dumped.
     */
    private FileResource resourceToDump;
    /**
     * Target pointing to the (base) directory where the content of the resource is to be dumped.
     */
    private LocalStorageDirectoryTarget outputDirectory;
    
    /**
     * To avoid multiple configuration. 
     */
    private boolean isAlreadyConfigured =false;
    
    /**
     * This strategy controls the behavior of the command regarding parent directories creation. who said intelligent design ? 
     */
    private CreateMissingDirectoryStrategy creationStrategy = CreateMissingDirectoryStrategy.DEFAULT;
    
    /**
     * This exctractor functor-like instance is used to pulls configuration from a PropertiesResource configuration.
     */
    private final ConfigurationExtractor propertiesResourceConfExctractor = new PropertiesResourceConfExctractor(this);

    /**
     * This exctractor functor-like instance is used to pulls configuration from a {@link FileResource} configuration.
     */
    private final ConfigurationExtractor fileResourceConfExtractor = new FileResourceConfExtractor(this);
    
    /*
     * Since this command can be configured in two ways which ca have opposite beavior results, One has to be extra carefull when dealing with configuration resource. 
     * Here we choose the conservative way and throw an Illegal configuration exception if we try to configure twice (or more) this command.
     * c.f. treatSpecificConfigurationResource.
     */
    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        for (Resource<?> confRes : configuration) {
            treatConfigurationResource(confRes);
        }

    }
    
    @Override
    public void setTarget(LocalStorageDirectoryTarget target) {
        this.outputDirectory=target;
    }

    @Override
    public void setResource(FileResource resource) {
        this.resourceToDump=resource;
    }

    @Override
    public VoidResource apply() {
        File relativePath = new File(fileName);
        if(relativePath.isAbsolute()) {
            throw new IllegalConfigurationException("File name "+fileName+" should be relative to the base path, but is absolute");
        }
        logConfiguration();
        File source=resourceToDump.getFile();
        creationStrategy.writeToDirectory(outputDirectory, source, relativePath);
        return new VoidResource();
    }
    
    @Override
    public void cleanUp() { /*NOPE*/ }

    /****** Private implementation ******/
    
    /*** Configuration handle using functor like strategy pattern ***/
    /**
     * Choose the correct treatment for a given configuration resource.
     */
    private void treatConfigurationResource(Resource<?> confRes){
        if (confRes instanceof FileResource) {
            treatSpecificConfigurationResource(confRes, this.fileResourceConfExtractor);
        } else if (confRes instanceof PropertiesResource) {
            treatSpecificConfigurationResource(confRes, this.propertiesResourceConfExctractor);
        } else {
            LOGGER.warn("Ignoring unexpected configuration resource {}.", new ComponentRepresentation(confRes));
        }
    }
     
    /**
     * Private interface used to deal with the configuration of the command. The goal
     * of the implementations of this interface is to provide a mechanism to exctract
     * config from a certain type of Resource.
     * 
     * This has to be used as a kind of functor in a strattegy like pattern.
     */
    private static interface ConfigurationExtractor {
        /**
         * This in the function contained by the functor. It should be able
         * to exctract configuration from a resource. 
         * @param confRes : The resource from which the configuration is to be extracted.
         */
        void extract(Resource<?> confRes);
    }
    /**
     * Utility method used to extract configuration from a resource. The behavior is given by the second functor like argument.
     * 
     * Since this command can be configured in two ways which ca have opposite beavior results, One has to be extra carefull when dealing with configuration resource. 
     * Here we choose the conservative way and throw an Illegal configuration exception if we try to configure twice (or more) this command.
     */
    private void treatSpecificConfigurationResource(Resource<?> confRes, ConfigurationExtractor configurationHelper) {
        if(!isAlreadyConfigured) {
            configurationHelper.extract(confRes);
            isAlreadyConfigured=true;
        } else {
             final String message = "Confliciting configuration resource "+confRes+". "+new ComponentRepresentation(this)+" is already set: Output path "+fileName+", Directory creation strategy "+creationStrategy+".";
             throw new IllegalConfigurationException(message);
        }
        
    }
    
    /**
     * This exctractor pulls configuration from a PropertiesResource configuration.
     */
    private static final class PropertiesResourceConfExctractor implements ConfigurationExtractor {

        /** 
         * The instance of the command being configured.
         */
        private final DumpToDisk commandToSet;
        
        /** 
         * Fully parametrized constructor.
         * @param commandToSet : The instance of the {@link DumpToDisk} command to configure.
         */
        public PropertiesResourceConfExctractor(DumpToDisk commandToSet) {
            this.commandToSet=commandToSet;
        }
        
        @Override
        public void extract(Resource<?> confRes) {
            Properties prop = ((PropertiesResource) confRes).getProperties();
            if(prop.containsKey(OUTPUT_PATH_KEY)){
                commandToSet.fileName=prop.getProperty(OUTPUT_PATH_KEY);
                if("".equals(commandToSet.fileName)) {
                    throw new IllegalConfigurationException("PropertiesResource "+confRes+" : empty output file name");
                }
                if(prop.containsKey(DumpToDisk.CREATE_MISSING_DIR_KEY)) {
                    final boolean createMissingDir=Boolean.parseBoolean(prop.getProperty(DumpToDisk.CREATE_MISSING_DIR_KEY));
                    commandToSet.creationStrategy = (createMissingDir) ? CreateMissingDirectoryStrategy.TRUE : CreateMissingDirectoryStrategy.FALSE;
                } else {
                    commandToSet.creationStrategy = CreateMissingDirectoryStrategy.DEFAULT;
                }
            } else {
                final String message = "Unexpected configuration properties resource "+confRes+". A valid configuration resource should contain key "+DumpToDisk.OUTPUT_PATH_KEY+".";
                throw new IllegalConfigurationException(message);
            }
        }
    }
    /**
     * This exctractor pulls configuration from a PropertiesResource configuration.
     */
    private static final class FileResourceConfExtractor implements ConfigurationExtractor {

        /** 
         * The instance of the command being configured.
         */
        private final DumpToDisk commandToSet;
        
        /** 
         * Fully parametrized constructor.
         * @param commandToSet : The instance of the {@link DumpToDisk} command to configure.
         */
        public FileResourceConfExtractor(DumpToDisk commandToSet) {
            this.commandToSet=commandToSet;
        }

        @Override
        public void extract(Resource<?> confRes) {
             FileResource fileResource = (FileResource) confRes;
            try {
                SimpleLinesData lines = new SimpleLinesData(fileResource.getFile().getAbsolutePath());
                if (lines.getLines().isEmpty()) {
                    throw new IllegalConfigurationException("Resource " + fileResource.getFile().getAbsolutePath() + " : empty output file name");
                } else {
                    commandToSet.fileName = lines.getLines().get(0);
                    DumpToDisk.LOGGER.debug("File will be dumped to path {} relative to the target local repository", commandToSet.fileName);
                    if (lines.getLines().size() > 1) {
                        DumpToDisk.LOGGER.warn("Ignoring {} extraneous lines in filename.", (lines.getLines().size() - 1));
                    }
                    commandToSet.creationStrategy = CreateMissingDirectoryStrategy.DEFAULT;
                }
            } catch (IOException e) {
                throw new InstructionRuntimeException("Error while reading the configuration resource " + fileResource.getFile() == null ? "{null}" : fileResource.getFile().getAbsolutePath(), e);
            }
        }
    }

    /*** Directory creation behavior ***/
    
    /**
     * This enum enumerates the possible stratgies when dealing with missing parent directories:
     * <ul>
     * <li>DEFAULT : Uses the target behavior.</li>
     * <li>TRUE : Creates if need be and possible missing directories.</li>
     * <li>FALSE : Do not insert new directories.</li>
     * </ul>
     */
    private static enum CreateMissingDirectoryStrategy {
        DEFAULT,TRUE,FALSE;
        
        public void writeToDirectory(LocalStorageDirectoryTarget directory, File source, File relativePath) {
            switch(this) {
                case TRUE : 
                    directory.write(source, relativePath, true);
                    break;
                case FALSE : 
                    directory.write(source, relativePath,false);
                    break;
                case DEFAULT :
                default:
                    directory.write(source, relativePath);
                    break;
            }
        }
    }

    /*** Utilities ***/
    
    /**
     * Logs, if debug is enabled, the configuration of the current instance of the command.
     */
    private void logConfiguration() {
        if(LOGGER.isDebugEnabled()) {
            final String message = "Using "+new ComponentRepresentation(this)+" parametrised as :  Output -> '"+fileName+"', Parent directory creation behavior -> '"+creationStrategy+"'.";
            LOGGER.debug(message);
        }
    }
}

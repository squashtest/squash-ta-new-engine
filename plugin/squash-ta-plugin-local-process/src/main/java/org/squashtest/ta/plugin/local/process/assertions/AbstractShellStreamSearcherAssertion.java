/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.assertions;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.regex.Pattern;

import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.shell.OutputStream;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.local.process.library.shell.BinaryShellReport;
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource;

public abstract class AbstractShellStreamSearcherAssertion extends
		AbstractShellResultAssertion{

	public static final String DEFAULT_SHELL_ENCODING = "UTF-8";
	private ShellResult actual;
	private FileResource expectedPattern;
	private OutputStream targetStream;

	public void setActualResult(ShellResultResource actual) {
		this.actual = actual.getResult();
	}

	public void setExpectedResult(FileResource expected) {
		this.expectedPattern = expected;
	}

	public void addConfiguration(Collection<Resource<?>> configuration) {
		for (Resource<?> resource : configuration) {
			if (resource instanceof FileResource) {
				FileResource parameter = (FileResource) resource;
				String absolutePath = parameter.getFile().getAbsolutePath();
				if (targetStream == null) {

					try {
						SimpleLinesData data = new SimpleLinesData(absolutePath);
						if (data.getLines().isEmpty()) {
							throw new IllegalConfigurationException(
									"Empty configuration element");
						}
						targetStream = OutputStream.valueOf(data.getLines()
								.get(0));
					} catch (IOException e) {
						throw new InstructionRuntimeException(
								"Failed to read configuration element "
										+ absolutePath, e);
					}
				} else {
					throw new IllegalConfigurationException(
							"Target stream is set more than once (first set to "
									+ targetStream + " then reset by "
									+ absolutePath);
				}
			}
		}
	}

	protected String loadConfiguration() {
		String streamData;
		if (targetStream == null) {
			throw new IllegalConfigurationException(
					"Target stream was not specified for pattern lookup.");
		}
		switch (targetStream) {
		case out:
			streamData = actual.getStdout();
			break;
		case err:
			streamData = actual.getStderr();
			break;
		default:
			throw new IllegalConfigurationException(
					targetStream
							+ " is not a supported stream for the result.shell contains assertion.");
		}
		return streamData;
	}

	protected void reportAssertionFailure(BinaryShellReport shellReport, String regex) {
		throw new BinaryAssertionFailedException(getFailureMessage(regex),
				shellReport.getExpectedReport(), shellReport.getActualReport(), shellReport.getContext());
	}

	protected Pattern compilePattern(){
		try {
			BinaryData patternData = new BinaryData(expectedPattern
					.getFile());
			String regex = new String(patternData.toByteArray(),
					DEFAULT_SHELL_ENCODING);
			return Pattern.compile(regex);
		} catch (IOException e) {
			throw new InstructionRuntimeException(
					"Failed to load expected pattern.", e);
		}
	}

	protected abstract String getFailureMessage(String regex);

	protected ShellResult getActual() {
		return actual;
	}

	protected OutputStream getTargetStream() {
		return targetStream;
	}
	
	protected BinaryShellReport buildBinaryReport (String streamData){
		BinaryShellReport shellReport = new BinaryShellReport();
		shellReport.setContext(buildShellContext(actual));
		shellReport.setExpectedReport(expectedPattern.copy());
		try {
			File streamFile = File.createTempFile("streamAssert", ".actual", TempDir.getExecutionTempDir());
			BinaryData streamBytes = new BinaryData(
					streamData.getBytes(DEFAULT_SHELL_ENCODING));
			streamBytes.write(streamFile);
			Resource<?> actualReport = new FileResource(streamFile);
			shellReport.setActualReport(actualReport);
		} catch (IOException e) {
			LoggerFactory.getLogger(AbstractShellStreamSearcherAssertion.class)
					.warn("Failed while building failure context", e);

		}
		return shellReport;
	}
}
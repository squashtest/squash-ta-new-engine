/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.assertions;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.plugin.local.process.library.shell.ExitCodeCheck;
import org.squashtest.ta.plugin.local.process.library.shell.UnaryShellReport;
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource;

/**
 * This assertion checks that an expected error code has occurred. This is for error use cases.
 * @author edegenetais
 *
 */
@TABinaryAssertion("failure")
public class ShellIsExpectedFailure extends AbstractShellResultAssertion implements BinaryAssertion<ShellResultResource, FileResource>{

	private static final Logger LOGGER = LoggerFactory.getLogger(ShellIsExpectedFailure.class);
	
	private ShellResultResource actual;
	private FileResource expected;

	/**
	 * For this assertion, no configuration is necessary. Any injected resource will be ignored.
	 */
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public void setActualResult(ShellResultResource actual) {
		this.actual=actual;
	}

	@Override
	public void setExpectedResult(FileResource expected) {
		this.expected=expected;
	}

	@Override
	public void test() throws BinaryAssertionFailedException {
		
		//in either case, we build the report for the record
		UnaryShellReport shellReport = buildUnaryReport(actual.getResult());
		try {
			BinaryData expectedCodeData=new BinaryData(expected.getFile());
			String expectedCodeString=new String(expectedCodeData.toByteArray(),"UTF-8");
			int expectedCode=Integer.parseInt(expectedCodeString);
			if(new ExitCodeCheck(expectedCode).check(actual.getResult())){
				LOGGER.debug("Expected failure code "+expectedCodeString+" was found.");
			}else{
				Resource<?> actualCodeResource;
				try {
					File actualFile = dumpExitCodeToFile(actual.getResult());
					actualCodeResource = new FileResource(actualFile);
				} catch (IOException ioe) {
					LOGGER.warn("Unable to create exit code reporting file", ioe);
					actualCodeResource = actual;// fallback: actual if we can't
												// extract exit code
				}


				throw new AssertionFailedException(
						"Shell command should have failed with code "+expectedCodeString+" but actual code was "+actual.getResult().getExitValue()+errorMessageDetails, actualCodeResource,
						shellReport.getContext());
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("Failed to load expected exit code value.", e);
		}catch(NumberFormatException e){
			throw new IllegalConfigurationException("Failed to load expected exit code value.", e);
		}
	}
	
	
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.resources;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.local.process.library.shell.Platform;
import org.squashtest.ta.local.process.library.shell.PosixPlatform;

/**
 * This resource holds a single shell command to be executed.
 * @author edegenetais
 *
 */
@TAResource("query.shell")
public class CommandLineResource implements Resource<CommandLineResource> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineResource.class);
        
        
        
	private File commandFile;
	private Integer timeout;
	private Integer streamlength;
	
	/** Noarg constructor for Spring enumeration. */
	public CommandLineResource() {}
	
	/**
	 * Full initialization constructor.
	 * @param commandFile
	 * @param timeout
	 * @param streamlength
	 */
	public CommandLineResource(File commandFile,Integer timeout, Integer streamlength) {
		this.commandFile = commandFile;
		this.timeout=timeout;
		this.streamlength=streamlength;
	}
	
	public CommandLineResource(File commandFile,Integer timeout) {
		this.commandFile = commandFile;
		this.timeout=timeout;
	}

	@Override
	public CommandLineResource copy() {
		return new CommandLineResource(commandFile,timeout,streamlength);
	}

	/**
	 * Get the command line content preped for execution in *REAL* POSIX environment.
	 * @return the content of the command line.
	 */
	public String getPosixCommand(){
                return prepareCommandForPlatform(new PosixPlatform());
	}

        /**
         * Prepare command for execution on a given platform.
         * TODO : redesign this. All this jumping through hoops to prepare resource content belongs in the converter,
         * or a library class shared between several converters and/or commands.
         * see issue 
         * @param platform the targezt platform for execution.
         * @return
         * @throws BadDataException
         * @throws InstructionRuntimeException 
         */
        private String prepareCommandForPlatform(final Platform platform) throws BadDataException, InstructionRuntimeException {
        try {
            SimpleLinesData content = new SimpleLinesData(commandFile.getAbsolutePath());
            StringBuffer command = new StringBuffer("");
            if(content.getLines().isEmpty()){
                throw new BadDataException("Empty command file at " + commandFile.getAbsolutePath());
            } else {
                Iterator<String> lineIterator=content.getLines().iterator();
                //The first line is treated differently : if it is the only one, no separator needeed.
                if(lineIterator.hasNext()){
                    platform.addStatementFromLine(lineIterator, command);
                }
                //next lines - if any - will be prepended with a separator.
                final String platformStatementSeparator = platform.getPlatformStatementSeparator();
                final int separatorLength = platformStatementSeparator.length();
                while(lineIterator.hasNext()){
                    if(command.length()>=separatorLength && !command.substring(command.length()-separatorLength).equals(platformStatementSeparator)){
                        command.append(platformStatementSeparator);
                    }
                    platform.addStatementFromLine(lineIterator, command);
                }
            }
            
            LOGGER.debug("Command: {}",command);
            return command.toString();
        } catch (IOException e) {
            throw new InstructionRuntimeException("Command content fetching failed.", e);
        }
    }

	
	/**
	 * Get the command line content, but without ";" at the end.
	 * @return the content of the command line.
	 */
	public String getLocalCommand() {
		return prepareCommandForPlatform(Platform.getLocalPlatform());
	}
	
        
	@Override
	public void cleanUp() {
		//noop: this file is managed by the initial resource from which it was taken.
	}

	/**
	 * @return the configured timeout for this commandline.
	 */
	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * @return the configured streamlength for this commandline.
	 */
	public Integer getStreamlength(){
		return streamlength;
	}
	
	public void setStreamlength(Integer streamlength){
		this.streamlength = streamlength;
	}
}

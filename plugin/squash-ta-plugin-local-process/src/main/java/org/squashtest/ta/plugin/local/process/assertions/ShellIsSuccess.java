/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.assertions;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.local.process.library.shell.ExitCodeCheck;
import org.squashtest.ta.plugin.local.process.library.shell.UnaryShellReport;
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource;

/**
 * This assertion checks that the shell execution returned the
 * <strong>success</strong> (0) exit code. No configuration needed.
 * 
 * @author edegenetais
 * 
 */
@TAUnaryAssertion("success")
public class ShellIsSuccess extends AbstractShellResultAssertion implements UnaryAssertion<ShellResultResource> {

	/** Logger for the class. */
	static final Logger LOGGER = LoggerFactory
			.getLogger(ShellIsSuccess.class);

	private static ExitCodeCheck SUCCESS_CHECK = new ExitCodeCheck();

	private ShellResultResource actual;

	@Override
	public void setActualResult(ShellResultResource actual) {
		this.actual = actual;
	}

	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public void test() throws AssertionFailedException {
		ShellResult shellResult = actual.getResult();
		
		//in either case, we build the report for the record
		UnaryShellReport shellReport = buildUnaryReport(shellResult);
		
		if (SUCCESS_CHECK.check(shellResult)) {
			LOGGER.debug("Shell success assertion passed.");
		} else {

			Resource<?> actualCodeResource;
			try {
				File actualFile = dumpExitCodeToFile(shellResult);
				actualCodeResource = new FileResource(actualFile);
			} catch (IOException ioe) {
				LOGGER.warn("Unable to create exit code reporting file", ioe);
				actualCodeResource = actual;// fallback: actual if we can't
											// extract exit code
			}

			throw new AssertionFailedException(
					"Shell command should have been successful but failed with code "+shellResult.getExitValue()+errorMessageDetails, actualCodeResource,
					shellReport.getContext());
		}
	}

}

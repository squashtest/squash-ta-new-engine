/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.assertions;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.local.process.library.shell.UnaryShellReport;

/**
 * Base class for {@link org.squashtest.ta.framework.components.UnaryAssertion}s and {@link org.squashtest.ta.framework.components.BinaryAssertion}s
 * that operate on shell results.  
 * @author edegenetais
 *
 */
public abstract class AbstractShellResultAssertion {

	private static final Logger LOGGER=LoggerFactory.getLogger(AbstractShellResultAssertion.class);
	
	protected static String errorMessageDetails = "\nPlease refer to your application documentation as for the meaning of this exit code. You may also check the content of the standard I/O streams (attached).";

	protected List<ResourceAndContext> buildShellContext(ShellResult shellResult) {
		List<ResourceAndContext> context = new ArrayList<ResourceAndContext>();
		try {
	
			context.add(buildContextElement(shellResult.getCommand(), ".command",
					"commandLine"));
			
			context.add(buildContextElement(shellResult.getStdout(), ".stream", "stdout"));
			
			context.add(buildContextElement(shellResult.getStderr(), ".stream", "stderr"));
	
		} catch (IOException ioe) {// can't abort failure reporting merely
									// because we can't give the context
			LOGGER.warn(
					"Unable to create context for success assertion failure.",
					ioe);
		}
		return context;
	}

	private ResourceAndContext buildContextElement(String content, String fileSuffix, String resourceName)
			throws IOException, UnsupportedEncodingException {
				File commandFile = File.createTempFile("shell", fileSuffix, TempDir.getExecutionTempDir());
				BinaryData commandData = new BinaryData(content
						.getBytes("UTF-8"));
				commandData.write(commandFile);
				ResourceAndContext commandLineContext = new ResourceAndContext();
				commandLineContext.setResource( new FileResource(commandFile));
				commandLineContext.setMetadata( new ExecutionReportResourceMetadata(
						getClass(), new Properties(), FileResource.class, resourceName));
				return commandLineContext;
	}

	protected File dumpExitCodeToFile(ShellResult shellResult)
			throws UnsupportedEncodingException, IOException {
		File actualFile = File.createTempFile("shell", ".exitCode", TempDir.getExecutionTempDir());
		String codeString = Integer.toString(shellResult.getExitValue());
		BinaryData exitCodeData = new BinaryData(codeString.getBytes("UTF-8"));
		exitCodeData.write(actualFile);
		return actualFile;
	}
	
	protected UnaryShellReport buildUnaryReport (ShellResult  shellResult){
		UnaryShellReport shellReport = new UnaryShellReport();
		shellReport.setContext(buildShellContext(shellResult));
		return shellReport;
	}
}
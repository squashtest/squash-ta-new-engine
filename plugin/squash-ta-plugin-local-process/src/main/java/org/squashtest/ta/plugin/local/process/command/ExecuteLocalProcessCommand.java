/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.local.process.command;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.concurrent.TimeoutException;

import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.VoidTarget;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.local.process.library.process.LocalProcessClient;
import org.squashtest.ta.local.process.library.shell.Platform;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.plugin.local.process.library.shell.ShellOptionReader;
import org.squashtest.ta.plugin.local.process.resources.CommandLineResource;
import org.squashtest.ta.plugin.local.process.resources.ShellResultResource;

/**
 * This command can execute a command or a script on the local system.
 * Syntax is: 
 * <pre>{@literal EXECUTE local WITH <Res:query.shell> [USING] $(timeout:<timeout>, streamlength:<streamlength>)] AS <Res:result.shell>}</pre>
 * 
 * @author cruhlmann
 *
 */

@TACommand("local")
public class ExecuteLocalProcessCommand implements Command<CommandLineResource, VoidTarget> {
	
	//default timeout is 5s
	private static final Integer DEFAULT_TIMEOUT = 5000;
	
	private static final Integer DEFAULT_STREAMLENGTH = 4096;
	
	private CommandLineResource commandLine;
	
	private ShellOptionReader shellOptions = new ShellOptionReader("query_shell_command");

	
	private LocalProcessClient client;
	
	public ExecuteLocalProcessCommand(){
		client = new LocalProcessClient();
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		shellOptions.addConfiguration(configuration);
	}

	@Override
	public void setTarget(VoidTarget localSystem) {
		//noop
	}

	@Override
	public void setResource(CommandLineResource commandLine) {
		this.commandLine = commandLine;			
	}

	@Override
	public ShellResultResource apply() {
		ShellResultResource result = null;
        try {
        	//the EXECUTE timeout override the CONVERT timeout
        	if (shellOptions.getTimeout() != null){
        		commandLine.setTimeout(shellOptions.getTimeout());
        	}
        	if (shellOptions.getStreamlength() != null){
        		commandLine.setStreamlength(shellOptions.getStreamlength());
        	}
        	
        	Integer timeout = commandLine.getTimeout()!=null?commandLine.getTimeout():DEFAULT_TIMEOUT;
        	
        	Integer streamLength = commandLine.getStreamlength() != null ? commandLine.getStreamlength():DEFAULT_STREAMLENGTH;
                    
                final String localCommand = commandLine.getLocalCommand();
        	
                String commandForProcess=Platform.getLocalPlatform().addShellSpawnCommandIfNeeded(localCommand);
                
                ShellResult executionResult= client.runLocalProcessCommand(commandForProcess, null, timeout, streamLength);

            result = new ShellResultResource(executionResult);
            
        } catch (IOException e) {
        	throw new InstructionRuntimeException("The execution of the command failed with the following message:\n"+e.getMessage(), e);
        } catch (InterruptedException e) {
        	throw new InstructionRuntimeException("The execution of the command was interrupted.", e);
        } catch (TimeoutException e) {
			throw new InstructionRuntimeException(e);
		} catch (ParseException e) {
			throw new InstructionRuntimeException("The execution of the command failed due a malformed command", e);
		}
		return result;
	}

	@Override
	public void cleanUp() {
		// noop	
	}

	//testing purpose only
	@SuppressWarnings("unused")
	private void setClient(LocalProcessClient client) {
		this.client = client;
	}
	
	
	
	
}

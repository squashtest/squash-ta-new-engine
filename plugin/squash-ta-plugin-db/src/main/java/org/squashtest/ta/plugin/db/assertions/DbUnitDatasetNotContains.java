/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.db.assertions;

import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.IDataSet;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.plugin.db.library.dbunit.FilteredStructureDataSet;
import org.squashtest.ta.plugin.db.library.dbunit.assertion.FailureHandlerExtension;
import org.squashtest.ta.plugin.db.resources.DbUnitDatasetResource;

/**
 * Binary assertion that checks that effective dataset contains expected
 * dataset.
 *
 * @author edegenetais
 *
 */
@TABinaryAssertion("not.contain")
public class DbUnitDatasetNotContains extends DbUnitDatasetGerenalContains implements
        BinaryAssertion<DbUnitDatasetResource, DbUnitDatasetResource> {

    @Override
    protected boolean compareSortedTables(FailureHandlerExtension myHandler, List<String> notFoundTables, final String[] tableNamesExpected, final String[] tableNamesActual, IDataSet pExpected, FilteredStructureDataSet actualView) throws DatabaseUnitException {
            
            for (String tableExpected : tableNamesExpected) {
                notFoundTables = notFound(tableExpected, tableNamesActual, notFoundTables);
                if (notFoundTables.isEmpty()
                        && performTableCompare(tableExpected, pExpected, actualView, myHandler)) {
                    //continue the loop
                } else {
                    return true;
                }
            }

            throwAssertionFailure(myHandler.getMap(), notFoundTables,
                    "The first dataset contain the second one.");
            return false;
    }
}

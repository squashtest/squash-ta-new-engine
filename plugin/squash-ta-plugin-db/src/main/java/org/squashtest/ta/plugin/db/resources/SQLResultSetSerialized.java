/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.db.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * File based SQL Query resource implementation.
 *
 * @author cjuillard
 *
 */
@TAResource("result.sql")
public class SQLResultSetSerialized extends SQLResultSet {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SQLResultSetSerialized.class);

    private CachedRowSet queryResultSet = null;

    private String objectSerializedFile;

    public SQLResultSetSerialized() {
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public SQLResultSetSerialized(ResultSet resultSet) {

        queryResultSet = convertToCachedResulSet(resultSet);
        objectSerializedFile = "ResulSet" + this.hashCode() + ".SquashTA.tmp";
        serialize();
    }

    @Override
    public SQLResultSetSerialized copy() {
        return new SQLResultSetSerialized(queryResultSet);
    }

    @Override
    public void cleanUp() {
        if (queryResultSet == null) {
            deserialize();
        }
        try {

            queryResultSet.close();
            queryResultSet = null;

        } catch (SQLException sqle) {
            throw new InstructionRuntimeException("The closing of the result set caused the following exception : ", sqle);
        }

    }

    @Override
    public List<String> getColumnNames() {
        if (queryResultSet == null) {
            deserialize();
        }
        return super.getColumnNames();
    }

    @Override
    public ResultSet getResultSet() {
        if (queryResultSet == null) {
            deserialize();
        }
        return queryResultSet;
    }

    @Override
    protected void finalize() throws Throwable {//NOSONAR - in this case we have other logic to ensure files are removed BUT, 
                                                //          best effort to remove stale files if there is a hole in the net is worth the finalize delay,
                                                //          espacially while knowing that we use this class for BIG datasets.
        File file = new File(objectSerializedFile);
        if(file.exists()){
            LOGGER.warn("There's some hole in the resource management net : the {} data file exists, while it should have been erased before.",file.getAbsolutePath());
        }
        if(!file.delete()){
            LOGGER.warn("Failed to delete {}, there is something rotten in the realm of Denmark!",file.getAbsolutePath());
            file.deleteOnExit();
        }
        super.finalize();
    }

    private void serialize() {
        LOGGER.info("Serializing..the ResulSet in {}",objectSerializedFile);
        if (queryResultSet != null) {
            FileOutputStream fichier;
            ObjectOutputStream serializer;

            try {
                fichier = new FileOutputStream(objectSerializedFile);
                serializer = new ObjectOutputStream(fichier);
                serializer.writeObject(queryResultSet);
                serializer.flush();
                serializer.close();
                queryResultSet.close();
                queryResultSet = null;
            } catch (FileNotFoundException ex) {
                LOGGER.error("Error in serialization CahedResultSet: File: {} not found/created",objectSerializedFile, ex);
            } catch (IOException e) {
                //oups... TODO
                /* err ... TODO what, exactly ? */
                LOGGER.error("Error in serialization CahedResultSet", e);
            } catch (SQLException ex) {
                LOGGER.error("SQL Error in serialization CahedResultSet", ex);
            }
        } else {
            LOGGER.warn("SQLResultSetSerialized serialize is called but object seems serialized or does not exist");
        }
    }


    private void deserialize() {

        if (queryResultSet == null) {
            LOGGER.info("Deserializing..ReslutSet from {}",objectSerializedFile);
            FileInputStream fichier = null;
            File file = new File(objectSerializedFile);
            ObjectInputStream deserializer = null;

            try {
                fichier = new FileInputStream(file);
                deserializer = new ObjectInputStream(fichier);
                queryResultSet = (CachedRowSet) deserializer.readObject();
                deserializer.close();
            } catch (FileNotFoundException ex) {
                LOGGER.error("File: {} not found (serialized ResulSet) ",objectSerializedFile, ex);
            } catch (IOException | ClassNotFoundException ex) {
                LOGGER.error("deserializer error", ex);
            } finally {
                file.delete();
            }
        } else {
            LOGGER.warn("SQLResultSetSerialized deserialize is call but object seems not serialized");
        }
    }

}

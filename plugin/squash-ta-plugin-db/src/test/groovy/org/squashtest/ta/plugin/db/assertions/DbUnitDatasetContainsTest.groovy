/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
*     This file is part of the Squashtest platform.
*     Copyright (C) 2011 - 2011 Squashtest TA, Squashtest.org
*
*     See the NOTICE file distributed with this work for additional
*     information regarding copyright ownership.
*
*     This is free software: you can redistribute it and/or modify
*     it under the terms of the GNU Lesser General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     this software is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General Public License
*     along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.squashtest.ta.plugin.db.assertions

import org.dbunit.assertion.DbComparisonFailure;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.NoSuchTableException;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.filter.IColumnFilter;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.filter.DefaultTableFilter;
import org.squashtest.ta.plugin.db.assertions.DbUnitDatasetContains;
import org.squashtest.ta.plugin.db.converter.XmlToDataset
import org.squashtest.ta.plugin.db.library.dbunit.ByTableIncludeExcludeColumnFilter
import org.squashtest.ta.plugin.db.library.dbunit.FilteredStructureDataSet
import org.squashtest.ta.plugin.db.resources.DbUnitDatasetResource;
import org.squashtest.ta.plugin.db.resources.DbUnitFilterResource;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.test.result.ResourceAndContext
import org.squashtest.ta.plugin.commons.resources.XMLResource

import spock.lang.Specification

class DbUnitDatasetContainsTest extends Specification {
	DbUnitDatasetContains testee
	URL expectedUrl
        URL actualUrl
        def myExpectedXMLResource
        def myActualXMLResource
        XmlToDataset datasetConverter
	IDataSet expected
        IDataSet actual
        
	def setup(){
            testee=new DbUnitDatasetContains()
                
            expectedUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_expected.xml")
            File expectedFile = new File(expectedUrl.toURI())
            myExpectedXMLResource = Mock(XMLResource)
            myExpectedXMLResource.getXMLFile() >> expectedFile
            datasetConverter = new XmlToDataset()
            expected = datasetConverter.convert(myExpectedXMLResource).getDataset()
                
 	}
	
	def "if less tables in actual, dbunit exception"(){
		given:
                        actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_1.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
                and:
                        testee.setExpectedResult(new DbUnitDatasetResource(expected,true))
                        testee.setActualResult(new DbUnitDatasetResource(actual,true))
		when:
			testee.test()	    
                then:
			BinaryAssertionFailedException exception=thrown()
                        //ok
	}
	
        def "only unfiltered tables are looked for"(){
		given:
			actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_3.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
                        
                and:
                        def tableFilter=Mock(ITableFilter)
			tableFilter.getTableNames(_)>>["SECOND_TABLE","EMPTY_TABLE"]
                        
			def ITableIterator filterIterator=Mock(ITableIterator)
			filterIterator.next()>>>[true,true,false]
			filterIterator.getTable()>>>[expected.getTable("SECOND_TABLE"), expected.getTable("EMPTY_TABLE")]
			filterIterator.getTableMetaData()>>>[expected.getTableMetaData("SECOND_TABLE"), expected.getTableMetaData("EMPTY_TABLE")]
			tableFilter.iterator(expected,_)>>filterIterator
        
                        expected=new FilteredStructureDataSet(expected, tableFilter, null);
		and:
			testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
			testee.setActualResult(new DbUnitDatasetResource(actual,false))
		when:
			def ok=false
			testee.test()
			ok=true
		then:
			ok
        }
        
        def "if less columns, dbunit exception"(){
		given:
                        actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_2.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
                and:
                        testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
                        testee.setActualResult(new DbUnitDatasetResource(actual,false))
                when:
                        testee.test()
                then:
                        BinaryAssertionFailedException exception=thrown()
	}
        
        def "only unfiltered columns are looked for"(){
		given:
			actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_6.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
		and:
                        def col0 = expected.getTableMetaData("TEST_TABLE").getColumns()[0]
                        def col1 = expected.getTableMetaData("TEST_TABLE").getColumns()[1]
                        def col2 = expected.getTableMetaData("TEST_TABLE").getColumns()[2]
                         
                        IColumnFilter columnFilter=Mock(IColumnFilter)
			columnFilter.accept(_,col0)>>false
                        columnFilter.accept(_,col1)>>true
                        columnFilter.accept(_,col2)>>true
                        
                        def tableFilter=Mock(ITableFilter)
			tableFilter.getTableNames(_)>>["TEST_TABLE"]
			def ITableIterator filterIterator=Mock(ITableIterator)
			filterIterator.next()>>>[true,false]
			filterIterator.getTable()>>>[expected.getTable("TEST_TABLE")]
			filterIterator.getTableMetaData()>>>[expected.getTableMetaData("TEST_TABLE")]
			tableFilter.iterator(expected,_)>>filterIterator
        
                        expected=new FilteredStructureDataSet(expected, tableFilter, columnFilter);
		and:
			testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
			testee.setActualResult(new DbUnitDatasetResource(actual,false))
		when:
			def ok=false
			testee.test()
			ok=true
		then:
			ok
	}
        
        def "if same content, ok"(){
		given:
			actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_expected.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
		and:
			testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
			testee.setActualResult(new DbUnitDatasetResource(actual,false))
		when:
			def ok=false
			testee.test()
			ok=true
		then:
			ok
	}

        def "if different content, FAILURE, not ERROR"(){
		given:
			actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_3.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
		and:
			testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
			testee.setActualResult(new DbUnitDatasetResource(actual,false))
		when:
			testee.test()
		then:
			BinaryAssertionFailedException failure=thrown()
			//expected: report
			failure.getFailureContext()!=null
			failure.getFailureContext().size()==3
			failure.getFailureContext().get(0).resource instanceof FileResource
	}
        
        def "if missing lines, FAILURE, not ERROR"(){
                given:
                        actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_4.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
                and:
                        testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
                        testee.setActualResult(new DbUnitDatasetResource(actual,false))
                when:
                        testee.test()
                then:
                        BinaryAssertionFailedException failure=thrown()
        }
        
        def "fix[894]: should report missing line when one of the lines in expected does not exist in actual and expected less lines than actual"(){
		given:
			actualUrl = getClass().getClassLoader().getResource("org/squashtest/ta/plugin/db/converter/dataset_contain_actual_5.xml")
                        File actualFile = new File(actualUrl.toURI())
                        myActualXMLResource = Mock(XMLResource)
                        myActualXMLResource.getXMLFile() >> actualFile
                        datasetConverter = new XmlToDataset()
                        actual = datasetConverter.convert(myActualXMLResource).getDataset()
		and:
			testee.setExpectedResult(new DbUnitDatasetResource(expected,false))
                        testee.setActualResult(new DbUnitDatasetResource(actual,false))
		when:
			testee.test()
		then:
			BinaryAssertionFailedException e=thrown()
	}
}
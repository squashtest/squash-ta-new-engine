/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.db.converter;

import static org.junit.Assert.*

import org.squashtest.ta.api.test.toolkit.ResourceExtractorSpockBase;
import org.squashtest.ta.framework.components.FileResource
import org.squashtest.ta.framework.components.Resource
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.plugin.db.converter.FileToSQLScript;
import org.squashtest.ta.plugin.db.resources.SQLScript;

import spock.lang.Specification

class FileToSQLScriptTest extends ResourceExtractorSpockBase {

	def "the script file must be correctly converted"(){
	
		given :
                        String input = "/org/squashtest/ta/plugin/db/converter/FileToSQLScriptResource.sql";
                        File file = createFile(input);
			FileResource myResource = new FileResource(file);
			FileToSQLScript converter = new FileToSQLScript();
                        
		when :
			SQLScript myScript = converter.convert(myResource);
			
		then :
			myScript.getBatchAsString().trim().equals("DROP PROCEDURE IF EXISTS `some_procedure`;       ###    CREATE PROCEDURE `greetings` BEGIN 	dbms_output.put_line('Hello World!'); END;   ###  DROP PROCEDURE IF EXISTS `some_procedure`;"); 
	}
        
        def "the script file must be correctly converted with new delimiter"(){
	
		given :
			String input = "/org/squashtest/ta/plugin/db/converter/FileToSQLScriptResource.sql";
			File file = createFile(input);
			FileResource myResource = new FileResource(file)
                        
                        String conInput = "/org/squashtest/ta/plugin/db/options/NEW-DELIMITER.opts";
                        File confFile = createFile(conInput);
			FileResource myConfResource = new FileResource(confFile)
                        List<Resource<?>> configuration = new ArrayList<Resource<?>>()
                        configuration.add(myConfResource)
			FileToSQLScript converter = new FileToSQLScript()
                        converter.addConfiguration(configuration)
		when :
			SQLScript myScript = converter.convert(myResource);
			
		then :
			myScript.getBatchAsString().trim().equals("DROP PROCEDURE IF EXISTS `some_procedure`;         @@ CREATE PROCEDURE `greetings` BEGIN 	dbms_output.put_line('Hello World!'); END; @@   DROP PROCEDURE IF EXISTS `some_procedure`;"); 
	}
}
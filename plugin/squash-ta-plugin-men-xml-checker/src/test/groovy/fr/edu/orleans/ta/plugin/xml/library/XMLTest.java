/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurAbstract;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurFactory;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurXmlJaxbImpl;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.RapportValidationXSD;
import fr.edu.orleans.ta.plugin.xml.resources.XSDResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

public class XMLTest extends ResourceExtractorTestBase{
        private static final Logger LOGGER=LoggerFactory.getLogger(XMLTest.class);
	/**
	 * 
	 * Le changement d'ordre des balises (noeuds fils) produit un résultat
	 * positif avec Jaxb et négatif avec XmlUnit
	 * 
	 * @throws SAXException
	 * @throws IOException
	 */
	
	@Test
	public void testOrdreBalise() throws SAXException, IOException {

		File resourceFile1 = createFile("/books/books1.xml");
		File resourceFile2 = createFile("/books/books1ReverseOrder.xml");
		File xsdFile = createFile("/books/books.xsd");

		ComparateurFactory comparateurFactory = new ComparateurFactory();

		// Jaxb2
		ComparateurXmlJaxbImpl comparateurJAXB = (ComparateurXmlJaxbImpl) comparateurFactory
				.getComparateur(ComparateurFactory.COMPARATEUR_JAXB);
		comparateurJAXB.setContextPath("generated");
		comparateurJAXB.setJaxbCodeBundlePath("src/test/resources/jaxb");
		comparateurJAXB.setXsdFilePath("src/test/resources/books/books.xsd");
		
		XSDResource xsdResource = new XSDResource(xsdFile);
		comparateurJAXB.setXsdFile(xsdResource.getXSDFile());
		

		assertTrue(
				"Les fichiers sont différents",
				comparateurJAXB.fichiersSimilaires(resourceFile1,resourceFile2)
                );
		// XmlUnit
		ComparateurAbstract comparateurXMLUNIT = comparateurFactory
				.getComparateur(ComparateurFactory.COMPARATEUR_XMLUNIT);
		
                assertTrue(
				"Les fichiers sont différents",
				comparateurXMLUNIT.fichiersSimilaires(resourceFile1,resourceFile2)
                );

	}
	
	/**
	 * 
	 * Test des différences retrouvées par XMLUnit
	 * Deux fichiers visiblement différents sont comparés
	 * 
	 * @throws SAXException
	 * @throws IOException
	 */
	@Test
	public void testDifferences() throws SAXException, IOException {

		File resourceFile1 = createFile("/books/books.xml");
		File resourceFile2 = createFile("/books/booksKO.xml");

		ComparateurFactory comparateurFactory = new ComparateurFactory();

		// XmlUnit
		ComparateurAbstract comparateurXMLUNIT = comparateurFactory
				.getComparateur(ComparateurFactory.COMPARATEUR_XMLUNIT);
		assertTrue(
				"Les fichiers ne sont pas différents",
				!comparateurXMLUNIT.fichiersSimilaires(
						resourceFile1,
						resourceFile2)
                );
		LOGGER.info("Message comparateur XMLUNIT:\n{}",comparateurXMLUNIT.getMessage());
		
		assertTrue("Pas de différences XMLUNIT", !comparateurXMLUNIT.getMessage().equals(""));

	}
	
	
	
	@Test
	public void testXSDValidationOK() throws SAXException, IOException {
		File resourceFile1 = createFile("/books/books.xml");
		File resourceFile2 = createFile("/books/booksReverseOrder.xml");
		File xsdFile = createFile("/books/books.xsd");

		ComparateurFactory comparateurFactory = new ComparateurFactory();

		// Jaxb2
		ComparateurAbstract comparateurJAXB = comparateurFactory
				.getComparateur(ComparateurFactory.COMPARATEUR_JAXB);

		comparateurJAXB.isXmlFileValid(xsdFile,
				resourceFile2);
		
		// XmlUnit
		ComparateurAbstract comparateurXMLUNIT = comparateurFactory
				.getComparateur(ComparateurFactory.COMPARATEUR_XMLUNIT);
		
		RapportValidationXSD rapport=comparateurXMLUNIT.isXmlFileValid(xsdFile,
				resourceFile1);
		assertTrue(rapport.documentIsValid());
	}
	
	@Test
	public void testXSDValidationKO() throws SAXException, IOException {
		File resourceFile1 = createFile("/books/booksInvalide.xml");
		File xsdFile = createFile("/books/books.xsd");

		ComparateurFactory comparateurFactory = new ComparateurFactory();

		// Jaxb2
		ComparateurAbstract comparateurJAXB = comparateurFactory
				.getComparateur(ComparateurFactory.COMPARATEUR_JAXB);

		RapportValidationXSD rapport=comparateurJAXB.isXmlFileValid(xsdFile,
				resourceFile1);
		assertFalse(rapport.documentIsValid());
	}

}

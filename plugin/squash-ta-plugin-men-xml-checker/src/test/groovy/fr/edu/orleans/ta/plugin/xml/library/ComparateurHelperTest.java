/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library;

import java.io.File;

import org.junit.Test;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurHelper;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

public class ComparateurHelperTest extends ResourceExtractorTestBase{

	@Test
	public void test() throws Exception {
		File jaxbOutputDir = 
                        FILE_TREE.createTempDirectory();
		
                File XSDFile = createFile("/books/books.xsd");
		
		new ComparateurHelper().jaxbCodeGen(jaxbOutputDir.getAbsolutePath(), "generated", XSDFile);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.ErreurXSD;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.RapportValidationXSD;

final class RapportTardifErrorHandler implements
		ErrorHandler {
	
	private List<ErreurXSD> erreurs=new ArrayList<ErreurXSD>();
	
	public RapportValidationXSD getRapport(){
		return new RapportValidationXSD(erreurs);
	}
	
	@Override
	public void warning(SAXParseException exception) throws SAXException {
		LoggerFactory.getLogger(RapportTardifErrorHandler.class).warn("XSD warning", exception);
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		LoggerFactory.getLogger(RapportTardifErrorHandler.class).debug("XSD fatal error", exception);
		erreurs.add(new ErreurXSD(exception.getLineNumber(), exception.getColumnNumber(), exception.getMessage()));
		throw exception;
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		LoggerFactory.getLogger(RapportTardifErrorHandler.class).debug("XSD error", exception);
		erreurs.add(new ErreurXSD(exception.getLineNumber(), exception.getColumnNumber(), exception.getMessage()));
	}
}
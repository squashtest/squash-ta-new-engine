/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class DefaultXsdReportingFilter implements XSDReportingFilter{

	public void writeReport(RapportValidationXSD rapportValidation,
			File targetXml, File xsdFile,File destination) throws IOException {
		try(
				FileOutputStream fis=new FileOutputStream(destination);
				OutputStreamWriter osw=new OutputStreamWriter(fis, "utf-8");
				BufferedWriter reportWriter=new BufferedWriter(osw);
				){
			reportWriter.write("<html><head><meta charset=\"UTF-8\"/></head><body>");
			reportWriter.write("<span>Liste des violations:<span>");
			reportWriter.newLine();
			reportWriter.write("<table><tr><th>Ligne</th><th>Colonne</th><th>Message</th><tr>");
			reportWriter.newLine();
			for(ErreurXSD violation:rapportValidation.violations()){
				reportWriter.write("<tr>");
				reportWriter.append("<td>").append(Integer.toString(violation.getLine())).append("</td>");
				reportWriter.append("<td>").append(Integer.toString(violation.getColumn())).append("</td>");
				reportWriter.append("<td>").append(violation.getMessage()).append("</td>");
				reportWriter.write("</tr>");
				reportWriter.newLine();
			}
			reportWriter.write("</table></body></html>");
			reportWriter.close();//close here to make sure write errors are detected before injecting the report in the failure context.
		}
	}

}

/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur;

import com.sun.tools.xjc.BadCommandLineException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.FileUtils;
import org.jvnet.jaxb2_commons.lang.InterfaceEqualsStrategy;
import org.xml.sax.SAXException;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.plugin.commons.converter.FileToJavaCodeBundle;
import org.squashtest.ta.plugin.commons.resources.JavaCodeBundle;

public class ComparateurXmlJaxbImpl extends ComparateurAbstract {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ComparateurXmlJaxbImpl.class);
        
        ComparateurHelper comparatorHelper=new ComparateurHelper();
        
	private File xsdFile;

	public File getXsdFile() {
		return xsdFile;
	}

	public void setXsdFile(File xsdFile) {
		this.xsdFile = xsdFile;
	}

	public boolean fichiersSimilaires(File pCtrlFichier, File pOutFichier)
			throws SAXException, IOException {
		boolean resultat;
		File bundleBase = FILE_TREE.createTempDirectory();

		try {		
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Contexte de JAXB récupéré : " + getContextPath());
			}
				
			// Compilation des classes de binding Jaxb
			
			
			File java = new File(bundleBase.getAbsolutePath()+"/java");
			java.mkdir();
			
			// Now get the XSD from bean configuration
			
			comparatorHelper.jaxbCodeGen(bundleBase.getAbsolutePath() + "/java", getContextPath(), getXsdFile());
	        
			FileResource bundleResource=new FileResource(bundleBase);
			
			FileToJavaCodeBundle ftjcb=new FileToJavaCodeBundle();
			
			JavaCodeBundle codeBundle=ftjcb.convert(bundleResource);
			
			Iterator it = codeBundle.getBundleClassNames().iterator();
			while (it.hasNext()) {
				String className = (String)it.next();
				LOGGER.debug("Loading class : " + className );
				codeBundle.getDedicatedClassloader().loadClass(className);
			}

	        // Pour charger les classes de binding, Jaxb a besoin du classloader 
			// qui a servi à la compilation
			JAXBContext jc = JAXBContext.newInstance(getContextPath(),codeBundle.getDedicatedClassloader());
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			Object source = unmarshaller.unmarshal(pOutFichier);
			Object control = unmarshaller.unmarshal(pCtrlFichier);

			resultat = InterfaceEqualsStrategy.INSTANCE.equals(null, null, source,
					control);
			FileUtils.deleteDirectory(bundleBase);
		
			
			return resultat;
			
		} catch (
                        BadCommandLineException |
                        ClassNotFoundException  |
                        JAXBException           e
                        ) {
			throw new ComparateurXmlException(
					"Erreur survenue lors de la comparaison des fichiers xml.",
					e);
                }finally{
			FileUtils.deleteDirectory(bundleBase);
		}

	}
}

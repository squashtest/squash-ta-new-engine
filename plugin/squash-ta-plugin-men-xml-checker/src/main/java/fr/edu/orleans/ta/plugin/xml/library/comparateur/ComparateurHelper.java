/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import com.sun.codemodel.JCodeModel;
import com.sun.tools.xjc.AbortException;
import com.sun.tools.xjc.BadCommandLineException;
import com.sun.tools.xjc.ConsoleErrorReporter;
import com.sun.tools.xjc.ErrorReceiver;
import com.sun.tools.xjc.Language;
import com.sun.tools.xjc.ModelLoader;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.XJCListener;
import com.sun.tools.xjc.api.SchemaCompiler;
import com.sun.tools.xjc.api.XJC;
import com.sun.tools.xjc.model.Model;
import com.sun.tools.xjc.util.ErrorReceiverFilter;
import com.sun.tools.xjc.util.NullStream;
import java.io.FileNotFoundException;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.LoggerPrintWriter;

public class ComparateurHelper {
        private static final String XCJ_LISTENER_LOGGER_NAME = ComparateurHelper.class.getPackage().getName()+".xjcListener";
        
	public void jaxbCodeGen(String outputDir, String jaxbContext,
			File schemaFile) throws BadCommandLineException {

		// Setup schema compiler
		SchemaCompiler sc = XJC.createSchemaCompiler();
		sc.forcePackageName(jaxbContext);

		// Setup SAX InputSource
		InputSource is = new InputSource(schemaFile.toURI().toString());
                
		final PrintStream out = buildLoggerPrintWriter(LoggerPrintWriter.Level.DEBUG);
		final PrintStream status = buildLoggerPrintWriter(LoggerPrintWriter.Level.ERROR);

		class Listener extends XJCListener {
			ConsoleErrorReporter cer = new ConsoleErrorReporter(
					out == null ? new PrintStream(new NullStream()) : out);

			public void generatedFile(String fileName, int count, int total) {
				message(fileName);
			}

			public void message(String msg) {
				if (status != null)
					status.println(msg);
			}

			public void error(SAXParseException exception) {
				cer.error(exception);
			}

			public void fatalError(SAXParseException exception) {
				cer.fatalError(exception);
			}

			public void warning(SAXParseException exception) {
				cer.warning(exception);
			}

			public void info(SAXParseException exception) {
				cer.info(exception);
			}
		}
		
		final Listener listener = new Listener();

                ErrorReceiver receiver = new ErrorReceiverFilter(listener) {
                    public void info(SAXParseException exception) {
                            super.info(exception);
                    }
                    public void warning(SAXParseException exception) {
                            super.warning(exception);
                    }
                    @Override
                    public void pollAbort() throws AbortException {
                        if(listener.isCanceled())
                            throw new AbortException();
                    }
                };

		Options options = new Options();
		options.addGrammar(is);
		options.defaultPackage = jaxbContext;
		options.setSchemaLanguage(Language.XMLSCHEMA);
		options.targetDir = new File(outputDir);
		options.parseArguments(new String[] { "-Xequals", "-XtoString",
				"-XhashCode" });
		Model model = ModelLoader.load(options, new JCodeModel(), receiver);

		if (model == null) {
			throw new ComparateurXmlException("Mauvais parsing du schema");
		}

		// Parse & build
		model.generateCode(options, null);
		try {
			model.codeModel.build(new File(outputDir));
		} catch (IOException e) {
			throw new ComparateurXmlException("Failed to build model code for the comparator", e);
		}

	}

        private PrintStream buildLoggerPrintWriter(LoggerPrintWriter.Level loggingLevel) {
            PrintStream outStream=null;
            try {
                outStream = new LoggerPrintWriter(XCJ_LISTENER_LOGGER_NAME, loggingLevel);
            } catch (FileNotFoundException ex) {
                LoggerFactory.getLogger(ComparateurHelper.class).warn("Failed to setup print writer logger, reporting will be lost",ex);
            }
            return outStream;
        }
    

	public void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

}

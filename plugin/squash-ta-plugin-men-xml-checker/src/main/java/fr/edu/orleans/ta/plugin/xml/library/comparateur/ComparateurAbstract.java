/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.RapportValidationXSD;

/**
 * @author ebelenfant
 *
 */
public abstract class ComparateurAbstract {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ComparateurAbstract.class);
	private String contextPath;
	private String jaxbCodeBundlePath;
	private String xsdFilePath;
	private String message;

	
	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getXsdFilePath() {
		return xsdFilePath;
	}


	public void setXsdFilePath(String xsdFilePath) {
		this.xsdFilePath = xsdFilePath;
	}


	public abstract boolean fichiersSimilaires(File pCtrlFichier, File pOutFichier) throws SAXException, IOException;
	
		
	public String getJaxbCodeBundlePath() {
		return jaxbCodeBundlePath;
	}


	public void setJaxbCodeBundlePath(String jaxbCodeBundlePath) {
		this.jaxbCodeBundlePath = jaxbCodeBundlePath;
	}


	public String getContextPath() {
		return contextPath;
	}


	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	/**
	 * Valide un fichier XML en fonction du schéma indiqué.
	 * @param xsdFile le schéma XSD à utiliser pour la validation.
	 * @param xmlFile le fichier XML à valider.
	 * @return
	 * @throws IOException
	 */
	public static RapportValidationXSD isXmlFileValid(File xsdFile, File xmlFile) throws IOException {
		RapportTardifErrorHandler errorHandler=new RapportTardifErrorHandler();
		Schema schema;
		try {
			SchemaFactory factory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			schema = factory.newSchema(xsdFile);
		}catch(Exception e){//should this happen, it must be reported as a technical problem, and not caught and reported as a VALID XML FILE (yuck!)
			throw new IllegalArgumentException("Invalid XSD file.", e);
		}
		try {
			Validator validator = schema.newValidator();
			validator.setErrorHandler(errorHandler);
			
			validator.validate(new StreamSource(xmlFile));

			return errorHandler.getRapport();
			
		} catch (SAXException e) {
			LOGGER.error("Problème de structure fatal durant la validation XSD",e);
			return errorHandler.getRapport();
		}

	}

}

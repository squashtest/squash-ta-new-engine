/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting;

import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Difference;

public class DefaultXmlComparisonReportingFilter implements XmlComparisonReportingFilter{
	private static final String EOL = "\n";
	private Map<String,String> header=Collections.emptyMap();
	@Override
	public String buildReport(DetailedDiff report, File expectedXml, File actualXml) {
		
		StringBuilder message=new StringBuilder();
		if(header.containsKey(ACTUAL_NAME_OPTION_KEY)){
			String expectedName = header.get(EXPECTED_NAME_OPTION_KEY);
			message.append("EXPECTED: ").append(expectedName).append(EOL);
			message.append("ACTUAL: ").append(header.get(ACTUAL_NAME_OPTION_KEY)).append(EOL);
		}
		message.append("COMPARATEUR: ").append(header.get(COMPARATEUR_OPTION_KEY)).append(EOL);
		
		@SuppressWarnings("unchecked")
		List<Difference> allDifferences = report.getAllDifferences();
		Iterator<Difference> differences = allDifferences.iterator();

		if(allDifferences.isEmpty()){
			message.append("Pas de différences.");
		}else if(allDifferences.size()==1){
			message.append("1 différence : \n");
		}else{
			message.append(allDifferences.size()).append(" differences : \n");
		}
		

		while (differences.hasNext()) {
			Difference difference = differences.next();
			message.append(difference.toString()).append("\n");
		}

		return message.toString();
	}

	@Override
	public void setHeader(Map<String, String> data) {
		this.header=data;
	}

	@Override
	public String getFormatSpecificFileExtension() {
		return "txt";
	}

}

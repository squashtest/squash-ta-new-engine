/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.examples.RecursiveElementNameAndTextQualifier;
import org.xml.sax.SAXException;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.DefaultXmlComparisonReportingFilter;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.XmlComparisonReportingFilter;

public class ComparateurXmlXmlUnitImpl extends ComparateurAbstract {

	private XmlComparisonReportingFilter reportingFilter;
	
	public ComparateurXmlXmlUnitImpl() {
		setMessage("");
	}

	public void setReportingFilter(XmlComparisonReportingFilter filter){
		this.reportingFilter=filter;
	}
	
	@Override
	public boolean fichiersSimilaires(File pCtrlFichier, File pOutFichier)
			throws SAXException, IOException {
		if(reportingFilter==null){
			reportingFilter=new DefaultXmlComparisonReportingFilter();
		}
		BufferedReader ctrlReader = new BufferedReader(new FileReader(
				pCtrlFichier));
		BufferedReader outReader = new BufferedReader(new FileReader(
				pOutFichier));
		
		// OPTIONS XMLUNIT
		XMLUnit.setCompareUnmatched(true);
		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setNormalize(true);
		XMLUnit.setNormalizeWhitespace(true);
		XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true);

		Diff myDiff = new Diff(ctrlReader, outReader);
		////////////////////////////////////////////////////////////////////////
		// On utilise un qualifier spécifique pour identifier les fragments XML
		// comparables deux à deux :
		// - le qualifier est récursif, il fonctionne quelquesoit la profondeur
		//   de l'arbre XML
		// - deux balises de meme nom et portant le même texte sont comparables
		////////////////////////////////////////////////////////////////////////
		myDiff.overrideElementQualifier(new RecursiveElementNameAndTextQualifier());

		DetailedDiff myDetDiff = new DetailedDiff(myDiff);
		
		setMessage(reportingFilter.buildReport(myDetDiff,pCtrlFichier,pOutFichier));

		return myDiff.similar();
	}

}

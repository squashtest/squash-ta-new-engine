/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Copyright (C) 2014, DSI - Académie d'Orléans-Tours, All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.converter;

import java.io.IOException;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.squashtest.ta.framework.annotations.TAResourceConverter;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.ResourceConverter;
import org.squashtest.ta.framework.exception.BadDataException;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.edu.orleans.ta.plugin.xml.resources.XSDResource;
import org.squashtest.ta.framework.tools.ConfigurationExtractor;

/**
 * File To XSD Converter
 * 
 * @author ebelenfant
 *
 */
@TAResourceConverter("structured")
public class FileToXSD implements ResourceConverter<FileResource, XSDResource> {

	public FileToXSD(){/*default constructor for Spring enumeration*/}
	
	@Override
	public float rateRelevance(FileResource input) {
		return 0.5f;
	}
	
	@Override
	public void addConfiguration(Collection<Resource<?>> configuration) {
		new ConfigurationExtractor(this).expectNoConfiguration(configuration);
	}

	@Override
	public XSDResource convert(FileResource resource) {
		XSDResource resultResource = null;
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
		 
			DefaultHandler handler = new DefaultHandler() {};
			
			saxParser.parse(resource.getFile(), handler);
		} catch (
                            SAXException | 
                            IOException |
                            ParserConfigurationException ex
                        ) {
			throw new BadDataException("Wrongly configured XML File\n",ex);
		}
		resultResource = new XSDResource(resource.getFile());
		return resultResource;
	}

	@Override
	public void cleanUp() {
            //noop : all in memory - so far !
	}
}

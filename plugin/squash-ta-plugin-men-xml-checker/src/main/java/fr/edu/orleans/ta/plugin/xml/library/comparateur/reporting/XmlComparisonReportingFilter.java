/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting;

import java.io.File;
import java.util.Map;

import org.custommonkey.xmlunit.DetailedDiff;

/**
 * Interface defining a transform from raw comparison report 
 * to user readable report.
 * @author edegenetais
 */
public interface XmlComparisonReportingFilter {
	static final String EXPECTED_NAME_OPTION_KEY = "expectedName";
	static final String ACTUAL_NAME_OPTION_KEY = "actualName";
	static final String COMPARATEUR_OPTION_KEY = "comparateur";
	
	void setHeader(Map<String,String> data);
	String buildReport(DetailedDiff report, File expectedXml, File actualXml);
	String getFormatSpecificFileExtension();
}

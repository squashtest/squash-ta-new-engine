/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.resources;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.XmlComparisonReportingFilter;

@TAResource("xml.comparison.reporting.filter")
public class XmlComparisonReportingFilterResource implements Resource<XmlComparisonReportingFilterResource>{
	
	private XmlComparisonReportingFilter filter;
	/** Pour Spring */
	public XmlComparisonReportingFilterResource(){}
	public XmlComparisonReportingFilterResource(XmlComparisonReportingFilter filter){
		this.filter=filter;
	}
	
	@Override
	public void cleanUp() {
		//noop - as yet!
	}

	@Override
	public XmlComparisonReportingFilterResource copy() {
		return new XmlComparisonReportingFilterResource(filter);
	}

	public XmlComparisonReportingFilter getFilter(){
		return filter;
	}
}

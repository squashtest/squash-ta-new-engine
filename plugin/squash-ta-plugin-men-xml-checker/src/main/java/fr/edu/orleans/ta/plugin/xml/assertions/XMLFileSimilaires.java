/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.OptionsReader;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.BinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.PropertiesResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.commons.resources.XMLResource;
import org.xml.sax.SAXException;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurAbstract;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurFactory;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurXmlJaxbImpl;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurXmlXmlUnitImpl;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.DefaultXmlComparisonReportingFilter;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.XmlComparisonReportingFilter;
import fr.edu.orleans.ta.plugin.xml.resources.XSDReportingFilterResource;
import fr.edu.orleans.ta.plugin.xml.resources.XSDResource;
import fr.edu.orleans.ta.plugin.xml.resources.XmlComparisonReportingFilterResource;
import org.squashtest.ta.framework.exception.BrokenTestException;

/**
 * @author ebelenfant
 * 
 */

@TABinaryAssertion("similaire")
public class XMLFileSimilaires implements
		BinaryAssertion<XMLResource, XMLResource> {
	/** End of Line */
	
	private static final String DEFAULT_EXPECTED_NAME = "Anonymous resource";
	
	protected XMLResource actual;
	protected XMLResource expected;
	protected XSDResource xsdResource;

	protected String typeComparateur = null;
	protected String jaxbContextPath = null;
	protected String jaxbCodeBundlePath = null;

	
	protected static final String XSD_FILE_OPTIONS = "xsd";
	protected static final String TYPE_COMPARATEUR_OPTIONS = "comparateur";
	protected static final String JAXB_CONTEXT_PATH_OPTION = "jaxbContextPath";
	protected static final String JAXB_CODE_BUNDLE_OPTIONS = "jaxbCodeBundlePath";
	
	protected Collection<Resource<?>> configuration = new LinkedList<Resource<?>>();
	
	private XmlComparisonReportingFilter filter;
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(XMLFileSimilaires.class);

	public void setActualResult(XMLResource actual) {
		this.actual = actual;
	}

	public void setExpectedResult(XMLResource expected) {
		this.expected = expected;
	}
	
	public void setXsdResource(XSDResource xsdResource) {
		this.xsdResource = xsdResource;
	}


	public void addConfiguration(Collection<Resource<?>> configuration) {
		this.configuration.addAll(configuration);
	}

	protected Map<String, String> getOptions() {
		Map<String, String> options = new HashMap<String, String>();

		for (Resource<?> resource : configuration) {
			if (FileResource.class.isAssignableFrom(resource.getClass())) {
				File fileR = ((FileResource) resource).getFile();
				LOGGER.debug("Treating options configuration resource at {}",fileR);
				options.putAll(readConf(fileR));
			}else if(PropertiesResource.class.isAssignableFrom(resource.getClass())){
				Properties p=((PropertiesResource)resource).getProperties();
				for(String key:p.stringPropertyNames()){
					options.put(key, p.getProperty(key));
				}
				LOGGER.debug("Treating memory properties configuration resource");
			}else if (XSDResource.class.isAssignableFrom(resource.getClass())) {
				setXsdResource((XSDResource)resource);
				LOGGER.debug("Getting XSD reference.");
			}else if(XmlComparisonReportingFilterResource.class.isAssignableFrom(resource.getClass())){
				this.filter=((XmlComparisonReportingFilterResource)resource).getFilter();
				LOGGER.debug("Injecting custom reporting filter of type {}",resource.getClass().getName());
			}else if(resource instanceof XSDReportingFilterResource){
				/** The mixup is too easy to overlook. Need to get a bit drastic here! */
				throw new IllegalConfigurationException(
						"You mixed up resource types and gave an "+XSDReportingFilterResource.class.getSimpleName()+" reporting filter"+
				" where an "+XmlComparisonReportingFilterResource.class.getSimpleName()+" instance was expected."
				);
			}else{
				LOGGER.warn("Ignoring unexpected configuration resource {}",resource);
			}
		}
		return options;
	}

	protected Map<String, String> readConf(File file) {

		try {
			return OptionsReader.BASIC_READER.getOptions(file);
		} catch (IOException ex) {
			String message = "Cannot load comparator options : "+ex.getMessage();
                        throw new InstructionRuntimeException(message, ex);
		}catch (IllegalArgumentException ex) {
			String message = "Invalid comparator options : "+ex.getMessage();
                        throw new BrokenTestException(message, ex);
		}
	}

	protected ComparateurAbstract getComparateur() {

                parseConfigurationOptions();
		
		ComparateurFactory comparateurFactory = new ComparateurFactory();
		ComparateurAbstract comparateur;
                
                checkComparatorOptionsValidity();
                
		if ("jaxb".equalsIgnoreCase(typeComparateur)) {
                        
			ComparateurXmlJaxbImpl comparateurJaxb = (ComparateurXmlJaxbImpl) comparateurFactory
					.getComparateur(ComparateurFactory.COMPARATEUR_JAXB);
			comparateurJaxb.setContextPath(jaxbContextPath);
			comparateurJaxb.setJaxbCodeBundlePath(jaxbCodeBundlePath);
			comparateurJaxb.setXsdFile(xsdResource.getXSDFile());
			comparateur = comparateurJaxb;

		} else if (("xmlunit".equalsIgnoreCase(typeComparateur))) {
			ComparateurXmlXmlUnitImpl comparateurXmlUnit = (ComparateurXmlXmlUnitImpl) comparateurFactory
					.getComparateur(ComparateurFactory.COMPARATEUR_XMLUNIT);
			comparateurXmlUnit.setReportingFilter(filter);
			comparateur = comparateurXmlUnit;
		} else {
			String message = "Problème dans l'option du comparateur XML : "
					+ typeComparateur
					+ ". Les valeurs possibles sont : jaxb, xmlunit.";
			
					throw new BinaryAssertionFailedException(message, expected, actual,
					null);
		}

		return comparateur;
	}

        private void parseConfigurationOptions() {
            Map<String, String> options = getOptions();

            if (options != null) {
                if (options.containsKey(TYPE_COMPARATEUR_OPTIONS)) {
                    typeComparateur = options.get(TYPE_COMPARATEUR_OPTIONS);
                } else {
                    typeComparateur = "jaxb";
                }
                if (options.containsKey(JAXB_CONTEXT_PATH_OPTION)) {
                    jaxbContextPath = options.get(JAXB_CONTEXT_PATH_OPTION);
                } else {
                    jaxbContextPath = "generated";
                }
                if (options.containsKey(JAXB_CODE_BUNDLE_OPTIONS)) {
                    jaxbCodeBundlePath = options.get(JAXB_CODE_BUNDLE_OPTIONS);
                } else {
                    jaxbCodeBundlePath = "src/squashTA/resources/jaxb";
                }

            } else {
                typeComparateur = "jaxb";
                jaxbContextPath = "generated";
                jaxbCodeBundlePath = "src/squashTA/resources/jaxb";
            }

            LOGGER.debug("Type comparateur : {}",typeComparateur);
            LOGGER.debug("Jaxb context path : {}",jaxbContextPath);
            LOGGER.debug("Jaxb code bundle path : {}",jaxbCodeBundlePath);

        }

        /**
         * This checks comparator options validity, throwing {@link IllegalConfigurationException}
         * if it is invalid.
         */
        private void checkComparatorOptionsValidity() {
            if(xsdResource==null && "jaxb".equalsIgnoreCase(typeComparateur)){
                throw new IllegalConfigurationException("The jaxb comparator type needs an XSD schema definition, but you provided none. Please switch to xmlunit comparator, or provide the required schema file.");
            }
        }

	private List<ResourceAndContext> buildFailureContext(String diff) {
		
		List<ResourceAndContext> failureContext=new ArrayList<ResourceAndContext>();
		
		ResourceMetadata expectedMetadata=new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, "expectedXml.xml");
		ResourceAndContext expectedCtx=new ResourceAndContext(
				//ici copie pour s'assurer de la survie de la ressource au moment du nettoyage de fin des tests
				new FileResource(expected.getXMLFile()).copy(),
				expectedMetadata
				);
		failureContext.add(expectedCtx);
		
		ResourceMetadata actualMetadata=new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, "actualXml.xml");
		ResourceAndContext actualCtx=new ResourceAndContext(
				//ici copie pour s'assurer de la survie de la ressource au moment du nettoyage de fin des tests
				new FileResource(actual.getXMLFile()).copy(),
				actualMetadata
				);
		failureContext.add(actualCtx);
		
		try {
			SimpleLinesData diffLines=new SimpleLinesData(diff.getBytes());
			File diffFile = File.createTempFile("xml_compare", ".diff", TempDir.getExecutionTempDir());
			diffLines.write(diffFile.getPath(),"utf-8");
			ResourceMetadata diffMetadata=new ExecutionReportResourceMetadata(getClass(), new Properties(), FileResource.class, "diff."+filter.getFormatSpecificFileExtension());
			ResourceAndContext diffCtx=new ResourceAndContext(new FileResource(diffFile).copy(), diffMetadata);
			failureContext.add(diffCtx);
		} catch (IOException e) {
			LOGGER.warn("Failed to create xml diff file", e);
		}
		
		return failureContext;
	}

	public void test() throws BinaryAssertionFailedException {
		try {
			if(filter==null){
				filter=new DefaultXmlComparisonReportingFilter();
			}
			
			ComparateurAbstract comparateur = getComparateur();
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Comparateur récupéré : " + comparateur.toString());
				LOGGER.debug("Contexte JAXB : " + comparateur.getContextPath());
				LOGGER.debug("Jaxb code bundle path : " + jaxbCodeBundlePath);
			}

			//doit être fait à ce moment là sinon les données ne seront pas peuplées à temps pour la génération du rapport.
			Map<String, String> options=getOptions();
			Map<String,String> headerFields=new HashMap<String, String>(options);
			headerFields.put(XmlComparisonReportingFilter.COMPARATEUR_OPTION_KEY, typeComparateur);
			if(headerFields.containsKey(XmlComparisonReportingFilter.ACTUAL_NAME_OPTION_KEY) && ! headerFields.containsKey(XmlComparisonReportingFilter.EXPECTED_NAME_OPTION_KEY)){
				headerFields.put(XmlComparisonReportingFilter.EXPECTED_NAME_OPTION_KEY, DEFAULT_EXPECTED_NAME);
			}
			filter.setHeader(headerFields);
			
			if (comparateur.fichiersSimilaires(expected.getXMLFile(),actual.getXMLFile()
					)) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Les fichiers comparés ont des contenus similaires (comparateur : "
							+ typeComparateur + ").");
				}
			} else {
				
				StringBuilder message = buildDiffReport(comparateur);
				List<ResourceAndContext> failureContext = buildFailureContext(message.toString());
				message.insert(0,"Les contenus des fichiers sont différents\n");
				throw new BinaryAssertionFailedException(message.toString(), expected,
						actual, failureContext);
			}
		} catch (IOException e) {
			throw new InstructionRuntimeException("Problem with I/O while comparing XML files.",e);
		} catch (SAXException e) {
                    final BinaryAssertionFailedException binaryAssertionFailedException = new BinaryAssertionFailedException(
                            "Problem with SAX parsing : " + e.getMessage(), expected,
                            actual, buildFailureContext("Comparison is impossible if XML is not valid."));
                    binaryAssertionFailedException.initCause(e);
			throw binaryAssertionFailedException;
		}
	}

	protected StringBuilder buildDiffReport(ComparateurAbstract comparateur) {
		
		StringBuilder message = new StringBuilder();
		
		message.append(comparateur.getMessage());
		return message;
	}

}

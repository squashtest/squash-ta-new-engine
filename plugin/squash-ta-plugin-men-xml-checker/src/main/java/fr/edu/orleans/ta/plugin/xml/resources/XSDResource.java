/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.resources;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.squashtest.ta.core.tools.io.FileTree.FILE_TREE;
import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;

/**
 * XSD resource implementation.
 * @author ebelenfant
 *
 */
@TAResource("xsd")
public class XSDResource implements Resource<XSDResource> {

	private static final Logger LOGGER=LoggerFactory.getLogger(XSDResource.class);
	
	private File xsdFile;
	
	/**
	 * Default constructor for Spring enumeration only.
	 */
	public XSDResource(){}
	
	public XSDResource(File file){
		xsdFile = file;
	}
	
	//@Override
	public XSDResource copy() {
		try {
			File newFile = FILE_TREE.createTempCopyDestination(xsdFile);
			FileUtils.copyFile(xsdFile, newFile);
			return new XSDResource(newFile);
		} catch (IOException e) {
			throw new InstructionRuntimeException("XSD (resource) : could not copy file path '"+xsdFile.getPath()+"', an error occured :", e);
		}
	}

	//@Override
	public void cleanUp() {
		boolean deleted=xsdFile.delete();
		if(!deleted && xsdFile.exists()){
                        LOGGER.warn("Failed to delete "+xsdFile.getAbsolutePath());
		}
	}

	public File getXSDFile() {
		return xsdFile;
	}
}

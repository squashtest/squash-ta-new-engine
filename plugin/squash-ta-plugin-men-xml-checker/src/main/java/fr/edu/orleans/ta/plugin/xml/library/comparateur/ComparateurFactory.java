/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Copyright (C) 2014, DSI - Académie d'Orléans-Tours, All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur;

/**
 * Cette classe sert à fabriquer des comparateurs de fichiers
 * 
 * @author ebelenfant
 * 
 */
public class ComparateurFactory {

	public static final String COMPARATEUR_JAXB = "fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurXmlJaxbImpl";
	public static final String COMPARATEUR_XMLUNIT = "fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurXmlXmlUnitImpl";

	public ComparateurAbstract getComparateur(String classComparateur){
            try {
                return (ComparateurAbstract) Class.forName(classComparateur).newInstance();
            } catch (
                    ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException e
                    ) {
                throw new ComparateurXmlException("Impossible de charger la classe " + classComparateur + " passée en paramètre", e);
            }
	}
}

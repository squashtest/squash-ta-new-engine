/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.resources;

import org.squashtest.ta.framework.annotations.TAResource;
import org.squashtest.ta.framework.components.Resource;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.XSDReportingFilter;

@TAResource("xsd.reporting.filter")
public class XSDReportingFilterResource implements Resource<XSDReportingFilterResource>{
	
	private XSDReportingFilter filter;
	
	/** To make spring happy */
	public XSDReportingFilterResource() {}

	public XSDReportingFilterResource(XSDReportingFilter filter){
		this.filter=filter;
	}
	
	@Override
	public void cleanUp() {
		//noop. Memory object.
	}

	@Override
	public XSDReportingFilterResource copy() {
		return new XSDReportingFilterResource(filter);
	}
	
	public XSDReportingFilter getFilter(){
		return filter;
	}
}

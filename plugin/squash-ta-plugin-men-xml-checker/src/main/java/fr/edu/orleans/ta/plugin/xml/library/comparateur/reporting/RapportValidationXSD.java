/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * Late-reporting mode error handler. This handler allows validation to go on 
 * as long as no fatal condition is detected. If the document is well formed, 
 * analysis should be complete, allowing to report all violations, 
 * not just the first detected violation.
 * </p><p>
 * If the XML document is not well formed, validation will be partial 
 * (only the beginning of the document up to the first detected structural anomaly).
 * </p>
 * <div>
 * <p>Please note that:</p>
 * <ul>
 * <li>violations might contain some noise when the document is too far 
 * from the structure described by the XSD</li>
 * <li>some violations may be missed if parent elements are not recognizable 
 * (only the violation reporting the parent as an unknown type 
 * will be issued in this case)</li>
 * </ul>
 * </div>
 * 
 * @author edegenetais
 *
 */
public class RapportValidationXSD {
	
	private List<ErreurXSD> failures=new ArrayList<ErreurXSD>();
	
	public RapportValidationXSD(List<ErreurXSD> errors) {
		failures.addAll(errors);
	}
	
	/**
	 * Returns an unmodifiable collection of errors..
	 * @return
	 */
	public Collection<ErreurXSD> violations(){
		return Collections.unmodifiableCollection(failures);
	}
	
	/**
	 * Main result: is the document valid.
	 * @return
	 */
	public boolean documentIsValid(){
		return failures.isEmpty();
	}
}

/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Initial Copyright (C) 2014, DSI - Académie d'Orléans-Tours,
 *     Modified by Henix (C) 2016 - 2020,
 *     All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package fr.edu.orleans.ta.plugin.xml.assertions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TAUnaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.components.UnaryAssertion;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.exception.IllegalConfigurationException;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.ResourceMetadata;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.plugin.commons.helpers.ExecutionReportResourceMetadata;
import org.squashtest.ta.plugin.commons.resources.XMLResource;

import fr.edu.orleans.ta.plugin.xml.library.comparateur.ComparateurAbstract;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.DefaultXsdReportingFilter;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.RapportValidationXSD;
import fr.edu.orleans.ta.plugin.xml.library.comparateur.reporting.XSDReportingFilter;
import fr.edu.orleans.ta.plugin.xml.resources.XSDReportingFilterResource;
import fr.edu.orleans.ta.plugin.xml.resources.XmlComparisonReportingFilterResource;

/**
 * @author ebelenfant
 * 
 * Cette classe vérifie qu'un fichier XML est valide
 * à l'aide d'un fichier XSD fourni par configuration (USING ...)
 * 
 */

@TAUnaryAssertion("valid")
public class XMLFileIsValid implements UnaryAssertion<XMLResource> {

	private static final Logger LOGGER = LoggerFactory.getLogger(XMLFileIsValid.class);
	private File xsdFile;
	private File xmlFile;
	private XSDReportingFilter filtreReportingCfg;
	
	public void setActualResult(XMLResource actual) {
		xmlFile = actual.getXMLFile();
	}

	public void addConfiguration(Collection<Resource<?>> configuration) {
		Iterator<Resource<?>> it = configuration.iterator();
		while (it.hasNext()) {
			Resource<?> resource = it.next();
			if (resource instanceof FileResource) {
				if (xsdFile == null) {
					FileResource fileResource = (FileResource) resource;
					xsdFile = fileResource.getFile();
					LOGGER.debug("Took validation XSD in.");
				} else {
					throw new IllegalConfigurationException(
							"Un seul fichier XSD peut être fourni pour la validation.");
				}
			}else if(resource instanceof XSDReportingFilterResource){
				LOGGER.debug("Got custom reporting filter of type {}.",resource.getClass().getName());
				if(filtreReportingCfg==null){
					filtreReportingCfg=((XSDReportingFilterResource)resource).getFilter();
					LOGGER.debug("Took filter in.");
				}else{
					throw new IllegalConfigurationException(
							"Un seul filtre de message peut être fourni.");
				}
			}else if(resource instanceof XmlComparisonReportingFilterResource){
				/** The mixup is too easy to overlook. Need to get a bit drastic here! */
				throw new IllegalConfigurationException(
						"You mixed up resource types and gave an "+XmlComparisonReportingFilterResource.class.getSimpleName()+" reporting filter"+
				" where an "+XSDReportingFilterResource.class.getSimpleName()+" instance was expected."
				);
			}else{
				LOGGER.warn("Ignoring unexpected configuration resource {}",resource);
			}
		}
	}

	public void test() throws AssertionFailedException {
		try{
			RapportValidationXSD rapportValidation=ComparateurAbstract.isXmlFileValid(xsdFile, xmlFile);
			if(!rapportValidation.documentIsValid()){
				List<ResourceAndContext> contexte=new ArrayList<ResourceAndContext>();
				
				addResourceToContext(contexte,xmlFile, "xmlDocument");
				
				addResourceToContext(contexte, xsdFile, "xsdSchema");
				
				genererRapportValidationXSD(rapportValidation, contexte);
				
				throw new AssertionFailedException("Le document ne respecte pas le schéma fourni", new FileResource(xmlFile).copy(), contexte);
			}
		} catch (IOException e) {
			throw new BadDataException(
					"Problème dans la validation du fichier XML (IOException)" + e.getMessage(),
					e);
		}
	}

	protected void genererRapportValidationXSD(
			RapportValidationXSD rapportValidation,			
			List<ResourceAndContext> contexte) throws IOException {
		try{
			File reportFile=File.createTempFile("xsdValidation", ".txt", TempDir.getExecutionTempDir());
			reportFile.deleteOnExit();
			XSDReportingFilter reportingFilter=this.filtreReportingCfg;
			if(reportingFilter==null){
				reportingFilter=new DefaultXsdReportingFilter();
			}
			reportingFilter.writeReport(rapportValidation, xmlFile,xsdFile,reportFile);
			addResourceToContext(contexte, reportFile, "xsdValidationReport.html");
		}catch(IOException e){
			LOGGER.warn("Erreur lors de l'écriture du rapport d'analyse, rapport indisponible.",e);
		}
	}

	protected void addResourceToContext(List<ResourceAndContext> contexte, File file, String resourceName) {
		FileResource document=new FileResource(file);
		ResourceMetadata documentMetadata=new ExecutionReportResourceMetadata(XMLFileIsValid.class,new Properties(),FileResource.class,resourceName);
		ResourceAndContext documentRAC=new ResourceAndContext(document.copy(), documentMetadata);
		contexte.add(documentRAC);
	}

}

/**
 *     This file is part of xml file comparison plugin within Squash-ta framework,
 *     Copyright (C) 2014, DSI - Académie d'Orléans-Tours, All rights reserved.
 *
 *     This library is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU Lesser General Public
 *
 *     License as published by the Free Software Foundation; either
 *     version 3.0 of the License, or (at your option) any later version.
 *
 *     This library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *     See the GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this library.  If not, see <http://www.gnu.org/licenses />.
 */
package org.jvnet.jaxb2_commons.lang;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

public class InterfaceEqualsStrategy extends DefaultEqualsStrategy {
    public static final EqualsStrategy INSTANCE = new InterfaceEqualsStrategy();
     
       protected boolean equalsInternal(ObjectLocator leftLocator, ObjectLocator rightLocator, Object lhs, Object rhs)
       {
         if ((lhs instanceof JAXBElement) && (rhs instanceof JAXBElement)) {
           JAXBElement left = (JAXBElement)lhs;
           JAXBElement right = (JAXBElement)rhs;
           return equalsInternal(leftLocator, rightLocator, left, right); }
         if ((lhs instanceof List) && (rhs instanceof List)) {
           List left = (List)lhs;
           List right = (List)rhs;
           return equalsInternal(leftLocator, rightLocator, left, right);
         }
         return super.equalsInternal(leftLocator, rightLocator, lhs, rhs);
       }
     
       protected boolean equalsInternal(ObjectLocator leftLocator, ObjectLocator rightLocator, List<?> left, List<?> right)
       {
           boolean contain = false;
           int indexR = 0;
           int indexL = 0;
           
           if(left.size() != right.size()){
               return false;
           }
           
           for(Object o1 : left){
               contain = false;
               for(Object o2 : right){
                   if(equals(LocatorUtils.item(leftLocator, indexL, o1), LocatorUtils.item(rightLocator, indexR, o2), o1, o2)){
                       contain = true;
                   }
                   indexR++;
               }
               if(!contain){
                   return false;
               }
               indexL++;
           }
           
           for(Object o2 : right){
               contain = false;
               for(Object o1 : left){
                   if(equals(LocatorUtils.item(leftLocator, indexL, o1), LocatorUtils.item(rightLocator, indexR, o2), o1, o2)){
                       contain = true;
                   }
                   indexL++;
               }
               if(!contain){
                   return false;
               }
               indexR++;
           }
           
           return contain;
           
       }
     
       protected boolean equalsInternal(ObjectLocator leftLocator, ObjectLocator rightLocator, JAXBElement<?> left, JAXBElement<?> right)
       {
         return ((equals(LocatorUtils.property(leftLocator, "name", left.getName()), LocatorUtils.property(rightLocator, "name", right.getName()), left.getName(), right.getName())) && (equals(LocatorUtils.property(leftLocator, "value", left.getValue()), LocatorUtils.property(rightLocator, "name", right.getValue()), left.getValue(), right.getValue())));
       }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.plugin.sahiproconnection.commands;

import org.squashtest.ta.core.tools.ClasspathInjector;
import java.io.File;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.annotations.TACommand;
import org.squashtest.ta.framework.components.Command;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.plugin.commons.targets.WebTarget;
import org.squashtest.ta.plugin.sahi.commands.SahiExecuteSuiteCommand;
import org.squashtest.ta.plugin.sahi.resources.SahiSuiteResource;
import org.squashtest.ta.plugin.sahi.resources.SahiSuiteResultResource;

/**
 * This decorator adds the <code>sahi.pro.install.dir</code> property value check
 * to the {@link org.squashtest.ta.plugin.sahi.commands.SahiExecuteSuiteCommand} command class
 * that defines the <code>execute</code> TA DSL command for sahi bundles.
 * It also tries to use this property to inject the SahiPro jars into the Squash TA component classloader,
 * and to add diagnosis messages to some failure cases.
 * 
 * @author edegenetais
 */
@TACommand("execute")
public class SahiProExecuteCommandDecorator implements Command<SahiSuiteResource, WebTarget>{

    private static final Logger LOGGER = LoggerFactory.getLogger(SahiProExecuteCommandDecorator.class);
    
    private static final String SAHI_PRO_LOCATION_PROPERTY_KEY = "sahi.pro.install.dir";
    
    WebTarget t;
    SahiSuiteResource s;
    
    SahiExecuteSuiteCommand delegate=new SahiExecuteSuiteCommand();
    
    @Override
    public void addConfiguration(Collection<Resource<?>> configuration) {
        delegate.addConfiguration(configuration);
    }
    
    
    @Override
    public void setTarget(WebTarget target) {
        this.t=target;
        
    }

    @Override
    public void setResource(SahiSuiteResource resource) {
        this.s=resource;
    }

    @Override
    public SahiSuiteResultResource apply() {
        
        final String sahiJarPath = System.getProperty(SAHI_PRO_LOCATION_PROPERTY_KEY) + "/lib/sahi.jar";
        final String antSahiJarPath = System.getProperty(SAHI_PRO_LOCATION_PROPERTY_KEY) + "/lib/ant-sahi.jar";
        
        boolean classPathInjected;
        if(new File(sahiJarPath).canRead() && new File(antSahiJarPath).canRead()){
            classPathInjected=checkAndSetSahiProClasspathEntries(sahiJarPath, antSahiJarPath);
        }else{
            classPathInjected=false;
        }
        
        try{
            delegate.setTarget(this.t);
            delegate.setResource(s);
            return (SahiSuiteResultResource) delegate.apply();
        }catch(NoClassDefFoundError err){
            if(classPathInjected){
                /*
                 * the jars have supposedly been properly injected, so this may well be a genuine LinkageError, 
                 * let's not emit misleading diagnosis messages...
                 */
                throw err;
            }else{
                //check if the sahi configuration was correct, tgghrow if not (adding the NoClassDefFoundError as suppressed exception)
                checkSahiProLocationKey(err);
                //here we know that we attempted classppath injection but failed, so we advise about manually setting the sahi jars in the engine classpath.
                throw new InstructionRuntimeException(
                        "A NoClassDefFoundError error failed Sahi test execution. You may want to add the following jars to your classpath and try again :\n"
                                +sahiJarPath+"\n"
                                +antSahiJarPath+"\n"+
                        "If you are using the regular Squash TA mojo, add the following to classpath dependencies : \n "
                        +"<dependency>\n"+
                        "  <groupId>sahipro</groupId>\n"+
                        "  <artifactId>sahi</artifactId>\n"+
                        "  <scope>system</scope>\n"+
                        "  <systemPath>"+sahiJarPath+"</systemPath>\n"+
                        "</dependency>\n"
                        +"<dependency>\n"+
                        "  <groupId>sahipro</groupId>\n"+
                        "  <artifactId>ant-sahi</artifactId>\n"+
                        "  <scope>system</scope>\n"+
                        "  <systemPath>"+antSahiJarPath+"</systemPath>\n"+
                        "</dependency>\n",

                        err
                );
            }
        }
    }

    protected void checkSahiProLocationKey(Throwable suppressed) throws BrokenTestException {
        
        final String sahiProLocationPropertyValue = System.getProperty(SAHI_PRO_LOCATION_PROPERTY_KEY);
        LOGGER.debug("The {} property is set to {}",SAHI_PRO_LOCATION_PROPERTY_KEY, sahiProLocationPropertyValue);
        if(sahiProLocationPropertyValue==null){
            final BrokenTestException brokenTestException = new BrokenTestException("The "+SAHI_PRO_LOCATION_PROPERTY_KEY+" java system property is not set, preventing classloading from your SahiPro installation! Please set the property to the base directory of your SahiPro installation.");
            brokenTestException.addSuppressed(suppressed);
            throw brokenTestException;
        }
        String invalidSahiLocationMessage="";
        if(!new File(sahiProLocationPropertyValue+"/lib/sahi.jar").canRead()){
            invalidSahiLocationMessage+="sahi.jar";
        }
        if(!new File(sahiProLocationPropertyValue+"/lib/ant-sahi.jar").canRead()){
            if(invalidSahiLocationMessage.length()>0){
                invalidSahiLocationMessage+=";";
            }
            invalidSahiLocationMessage+="ant-sahi.jar";
        }
        if(invalidSahiLocationMessage.length()>0){
            final BrokenTestException brokenTestException = new BrokenTestException("The "+SAHI_PRO_LOCATION_PROPERTY_KEY+" property value "+System.getProperty(SAHI_PRO_LOCATION_PROPERTY_KEY)+" does not point to a usable SahiPro installation. The following files are missing or unreadable: "+invalidSahiLocationMessage);
            brokenTestException.addSuppressed(suppressed);
            throw brokenTestException;
        }
        
    }
    
    protected boolean checkAndSetSahiProClasspathEntries(String sahiJarPath, String antSahiJarPath) throws BrokenTestException {
        return new ClasspathInjector().classPathInjector(getClass(), "SahiPro", sahiJarPath, antSahiJarPath);
    }
    

    @Override
    public void cleanUp() {
        delegate.cleanUp();
    }
    
}
